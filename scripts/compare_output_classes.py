import numpy as np
import os
import argparse


parser = argparse.ArgumentParser(description='Computes the accuracy of the circuit simulated with verilator')
parser.add_argument('--correct_classes_file', '-c', required=True, dest='correct_classes_file',
                   type=str,
                   help="The path to the binary file containing the correct classes")
parser.add_argument('--csv_file', '-v', required=True, dest='csv_file',
                   type=str,
                   help="The path of the generated csv file for the last layer of the network")
parser.add_argument('--num_classes', '-n', dest='num_classes', type=int, help="The number of possible classes", default=1001)
args = parser.parse_args()
if not os.path.isfile(args.correct_classes_file):
	print("ERROR: correct_classes_file is not valid!")

if not os.path.isfile(args.csv_file):
	print("ERROR: csv_file is not valid!")


correct_classes = list(np.fromfile(args.correct_classes_file, np.int16))

top_classes = []
top_vals = []
NUM_CLASSES=args.num_classes

with open(args.csv_file) as in_file:
	first_line = True
	line_counter = 0
	image_counter = 0
	pic_vals = []
	classes = []

	for line in in_file:
		if first_line:
			first_line = False
		else:
			pic_vals.append(int(line))
			classes.append(line_counter)
			line_counter += 1
			if line_counter >= NUM_CLASSES:
				zipped_list = [list(x) for x in zip(classes, pic_vals)]
				zipped_list.sort(key=lambda tup: tup[1])
				zipped_list = zipped_list[-5:]
				top_classes.append([x[0] for x in zipped_list])
				top_vals.append([x[1] for x in zipped_list])
				line_counter = 0
				image_counter += 1
				pic_vals = []
				classes = []


top_1_correct = 0
top_5_correct = 0
for image in range(len(top_classes)):
	cur_classes = top_classes[image][::-1]
	if cur_classes[0] == correct_classes[image]:
		top_1_correct += 1
	if correct_classes[image] in cur_classes:
		top_5_correct += 1

	print("Image " + str(image) + ":", cur_classes, "Correct class:", correct_classes[image])


print("Sim top-1:", top_1_correct/float(len(top_classes))*100)
print("Sim top-5:", top_5_correct/float(len(top_classes))*100)
