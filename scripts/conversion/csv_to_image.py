import sys
sys.path.insert(1, './')

from tf_graph_to_scnn import get_image_from_3d_activation
from matplotlib import pyplot as plt
import seaborn as sns
import numpy as np
import compare_output_distributions as cod
import imageio
import os
import re
import argparse



parser = argparse.ArgumentParser(description='Generate Activation Images and Comparisons to TensorFlow')
parser.add_argument('files', metavar='filepath', type=str, nargs='+',
                   help='a list of .csv files to process')
parser.add_argument('--relative_path_to_tf_outputs', dest='tf_path',
                   type=str,
                   help='Specify a path from which to load tensorflow output CSVs of the same name as the HPIPE outputs')
parser.add_argument("--image_number", type=int, default=0, help="Specify which activation number you would like to process")
parser.add_argument("--margin", type=int, default=150, help="Specify how much margin to allow before complaining about differences between tensorflow and the hardware.  Default is 150.")
parser.add_argument("--both_image_number", action="store_const", const=True, default=False, help="By default the TensorFlow image number is always 0 while image_number controls the simulation CSV image index.  Adding this flag makes it so that both use the image number flag to set their image index.")
args = parser.parse_args()

tensorflow_image_index = 0
if args.both_image_number:
	tensorflow_image_index = args.image_number

dump_tensorflow = args.tf_path is not None
image = args.image_number
files = args.files

print(args.tf_path)

for f in files:
	(directory, filename) = os.path.split(f)
	
	is_npy = False
	if f[-3:] == "csv":
		with open(f, "r") as fh:
			values = []
			counter = 0
			for l in fh:
				values.append(l)
				counter += 1
				if counter > 150000:
					break
			values = [[v for v in l.split(",")] for l in values]
			for l in values:
				try:
					float(l[-1])
				except Exception as e:
					if "x" not in l[-1]:
						del l[-1]
		print(f)
		print(values[0][0])
		if len(values) == 0:
			print("No contents in " + f)
			continue
		channels = int(values[0][0])
		print(channels)
		del values[0]
		if len(values) == 0:
			print("No contents in " + f)
			continue

	elif f[-3:] == "npy":
		values = np.load(f)
		channels = values.shape[-1]
		height = values.shape[1]
		width = values.shape[2]
		is_npy = True
	else:
		print(f"File {f} is not of type .npy or .csv")
		sys.exit(1)

	if dump_tensorflow:
		tf_path = directory + "/" + args.tf_path + "/"
		with open(tf_path + filename, "r") as fh:
			golden_values = [[float(v) for v in l.split(",")] for l in fh]
		channels = int(golden_values[0][0])
		del golden_values[0]
	#golden_values = values


	if dump_tensorflow:
		length = len(golden_values[0])
	else:
		length = len(values[0])
	max_height = length

	if not is_npy:
		if len(values[-1]) != length:
			del values[-1]
		try:
			for i,l in enumerate(values):
				for j,v in enumerate(l):
					if "x" in v:
						values[i][j] = 0.0
					else:
						try:
							values[i][j] = float(v)
						except Exception as e:
							if i == (len(values) - 1):
								values[i][j] = 0.0
							else:
								raise e

		except Exception as e:
			print("Couldn't convert '" + str(values[i][j] + "' to float at (i,j) (" + str(i) + "," + str(j) + "), skipping " + f + "..."))
			continue
			#raise e
					
	if dump_tensorflow:
		for i,(vl,gl) in enumerate(zip(values, golden_values)):
			breaking = False
			for j,(v,g) in enumerate(zip(vl,gl)):
				if abs(v - g) > args.margin:
					print("Expected " + str(g) + " got " + str(v) + " in line " + str(i) + " at position " + str(j))
					breaking = True
					break
			if breaking:
				break

	"""
	np_values = np.array(values)
	np_golden = np.array(golden_values)
	np_golden = np_golden[:np_values.shape[0]]
	values = np_values - np_golden
	values = list(values)
	"""
	#values = golden_values

	print("Generating " + filename)

	if not is_npy:
		if not dump_tensorflow:
			height = len(values) // channels
			height = min(height, max_height)
			width = len(values[0])
			height = width
		else:
			height = len(golden_values) // channels
			height = min(height, max_height)
			width = len(golden_values[0])
			height = width
		print(height)
		print(width)

		values = np.array(values[image*channels * height+0:(image+1)*channels*height+0])
		print(values.shape)
		values = np.reshape(values, [1, height, channels, width])
		values = np.transpose(values, [0, 1, 3, 2]).astype(float)
	a = np.reshape(values, [height, width, channels])
	#values.shape[2] - values.shape[1]
	#values = np.pad(values, [[0,0], [0, 0], [0,0], [0, 0]], mode="constant", constant_values=0)
	
	im = get_image_from_3d_activation(values)

	no_ext = filename[:-4]
	imageio.imwrite(directory + "/" + no_ext + ".png", im)
	#continue

	if not dump_tensorflow:
		continue

	no_ext = filename[:-4]
	golden_values = np.array(golden_values[tensorflow_image_index * channels * height :(tensorflow_image_index + 1) * channels * height])
	golden_values = np.reshape(golden_values, [1, height, channels, width])
	golden_values = np.transpose(golden_values, [0, 1, 3, 2]).astype(float)
	ga = np.reshape(golden_values, [height, width, channels])

	gim = get_image_from_3d_activation(golden_values)

	imageio.imwrite(tf_path + no_ext + ".png", gim)

	#continue
	#plt.imshow(im)
	#plt.show()


	a = np.transpose(a, [2,0,1])
	#a = np.mean(a, (1,2), keepdims=True)
	ga = np.transpose(ga, [2,0,1])
	#ga = np.mean(ga, (1,2), keepdims=True)
	vplot_data = cod.get_violinplot_data({
		"Accelerator": a,
		"TensorFlow": ga})
	ax = sns.violinplot(data=vplot_data, hue="Generator", 
		x="Output Channel", y="Values", split=False, inner="quartile")
	fig = plt.gcf()
	dpi = fig.get_dpi()
	fig.set_size_inches(min(60*channels/64, (2**16-1)/dpi),10)
	fig.savefig(directory + "/" + no_ext + "_distributions.pdf", bbox_inches="tight")
	plt.clf()


