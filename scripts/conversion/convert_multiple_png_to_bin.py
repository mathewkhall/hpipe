# This script converts a png image to a binary input file that can be used for verilator
# It has only been tested with images of size 224x224
# The images have to have names as 0.png, 1.png,...
import struct
import math
import numpy as np
from PIL import Image

num = 10 #Select the number of images to be added and converted to bin
IMAGE_NAME = '/media/mabdelfattah3929/Data/hpipe/HPIPE_GIT/hpipe/generated_files/binary_inputs/images/'
OUTPUT_NAME = '/media/mabdelfattah3929/Data/hpipe/HPIPE_GIT/hpipe/generated_files/binary_inputs/images/bin/'+str(num)+'_3_512.bin'

#Open image and convert to 8-bit unsigned int
image = []
for i in range(num):
	image.append(Image.open(IMAGE_NAME+str(i)+".png"))
	image[i] = np.array(image[i],  dtype=np.uint8)

#Variable definitions for determining number of pixels per transfer
image_width = image[0].shape[0]
image_height = image_width
interface_width = 512
bits_per_pixel = 8
pixels_per_transfer = interface_width // bits_per_pixel
transfers_per_w = math.ceil(image_width / float(pixels_per_transfer))

with open(OUTPUT_NAME, "wb") as fh:
	for i in range(num):	
		for row in range(image[i].shape[0]):
			for channel in range(image[i].shape[2]):
				cur_row = image[i][row,:,channel]
				#Order needs to be reversed for each transfer
				for transfer_num in range(transfers_per_w):
					start_index = transfer_num*pixels_per_transfer 
					end_index = transfer_num*pixels_per_transfer + pixels_per_transfer
					cur_pixels = np.flip(cur_row[start_index:end_index])
					if len(cur_pixels) < pixels_per_transfer:
						cur_pixels = [0] * (pixels_per_transfer - len(cur_pixels)) + list(cur_pixels)
					cur_struct = struct.pack('>' + str(len(cur_pixels)) +'B', *cur_pixels)
					fh.write(cur_struct)
			
			

			

