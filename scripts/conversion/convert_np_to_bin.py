# This script converts a png csv file to a bin

import struct
import math
import numpy as np
from PIL import Image
import argparse
import csv 
import os
import cv2

input_file = 'relu6_10.csv'
output_file = 'many_inputs_1.bin'
convert_image = False

with open(input_file,newline='') as csvfile:
	values=csv.reader(csvfile,delimiter=',')
	parameters = list(values)

w = 28
h = 28
c = 256
precision_in = 16
interface_width = 256
all_vals = np.array(np.hstack(parameters))
image = all_vals[1:(w*h*c)+1]
image = np.array(image).astype(np.int16)

dec_image =image

remainder_padding = int((np.ceil(w*precision_in/interface_width) - (w*precision_in/interface_width))*precision_in)
num_transfers = int((w*h*c*precision_in)/interface_width)

if((w*precision_in)%interface_width != 0):
	padded_image = []
	for i in range(w*h*c):
		if(i%w == 0):
			for j in range(remainder_padding): #This 8 will be the remainder of the line that was not complete (4- (56*16/256) ) * 16 --> the 4 is ceil(56*16/265)
				padded_image.append(np.zeros(1,dtype = np.int16))
		padded_image.append(dec_image[i])
	dec_image = np.array(padded_image).astype(np.int16)
	num_transfers = int(((w+remainder_padding)*h*c*precision_in)/interface_width)



with open(output_file, "wb") as fh:
	for i in range(num_transfers):
		start_index = i*precision_in
		end_index = i*precision_in + precision_in
		cur_pixels = np.flip(dec_image[start_index:end_index])
		cur_struct = struct.pack('<' + str(int(interface_width/precision_in)) +'h', *cur_pixels)
		fh.write(cur_struct)



if(convert_image):

	# This script converts a png image to a binary input file that can be used for verilator
	# It has only been tested with images of size 224x224



	parser = argparse.ArgumentParser(description='Convert a png to a bin input')
	parser.add_argument('--input_png', required=True, dest='input_png',
	                   type=str,
	                   help='Specify the path of the input png image. Note that this script has only been tested with images of size 224x224')
	parser.add_argument('--output_bin', required=True, dest='output_bin',
	                   type=str,
	                   help='Desired output bin path')
	parser.add_argument('--interface_width', dest='interface_width', type=int,
	        default=256, help='Interface width of input in bits (default=256)')
	args = parser.parse_args()

	#Open image and convert to 8-bit unsigned int
	image = Image.open(args.input_png)
	image = np.array(image,  dtype=np.int16)

	#Variable definitions for determining number of pixels per transfer
	image_width = image.shape[0]
	image_height = image_width
	interface_width = args.interface_width
	bits_per_pixel = 16
	pixels_per_transfer = interface_width // bits_per_pixel
	transfers_per_w = math.ceil(image_width / float(pixels_per_transfer))

	with open(args.output_bin, "wb") as fh:
		for row in range(image.shape[0]):
			for channel in range(image.shape[2]):
				cur_row = image[row,:,channel]
				#Order needs to be reversed for each transfer
				for transfer_num in range(transfers_per_w):
					start_index = transfer_num*pixels_per_transfer 
					end_index = transfer_num*pixels_per_transfer + pixels_per_transfer
					cur_pixels = np.flip(cur_row[start_index:end_index])
					cur_struct = struct.pack('<' + str(pixels_per_transfer) +'h', *cur_pixels)
					fh.write(cur_struct)
			
			

			




os.system("rm /media/mabdelfattah3929/Data/hpipe/HPIPE_GIT/hpipe/generated_files/binary_inputs/many_inputs_1.bin")
os.system("mv /media/mabdelfattah3929/Data/hpipe/HPIPE_GIT/hpipe/many_inputs_1.bin /media/mabdelfattah3929/Data/hpipe/HPIPE_GIT/hpipe/generated_files/binary_inputs/")
"""

dec_image = np.array(dec_image,dtype=np.int16).reshape((56,56,64))
image = dec_image
#Variable definitions for determining number of pixels per transfer
image_width = image.shape[0]
image_height = image_width
interface_width = 256
bits_per_pixel = 16
pixels_per_transfer = interface_width // bits_per_pixel
print(pixels_per_transfer)
transfers_per_w = math.ceil(image_width / float(pixels_per_transfer))

with open(output_file, "wb") as fh:
	for row in range(image.shape[0]):
		for channel in range(image.shape[2]):
			cur_row = image[row,:,channel]
			#Order needs to be reversed for each transfer
			for transfer_num in range(transfers_per_w):
				start_index = transfer_num*pixels_per_transfer 
				end_index = transfer_num*pixels_per_transfer + pixels_per_transfer
				print("start",start_index)
				print("end", end_index)
				cur_pixels = np.flip(cur_row[start_index:end_index])
				print(len(cur_pixels))
				cur_struct = struct.pack('>' + str(pixels_per_transfer) +'h', *cur_pixels)
				print(*cur_pixels,'\n')
				fh.write(cur_struct)
"""