# Simple script that calculates the network GOPS
# The input file should have the format where each line is as follows:
# layer name, kernel shape, output shape
# 
# For example:
# import/MobilenetV1/Logits/Conv2d_1c_1x1/Conv2D (1, 1, 1024, 1001) (5, 1, 1, 1001)
# 
# This can be done in HPIPE with a line like the following in GraphBuilder.py:
# print(n.tf_op.name,  n.get_kernel_shape(True), n.get_output_shape())
#
# Make sure to remove any nodes used for training first (e.g. Batch norm)

input_file = "/home/marius/tmp_flops"

total_gops = 0
import math

with open(input_file, 'r') as in_file:
    for line in in_file:

        line = line.split('(')
        
        layer_name = line[0].split("/")
        layer_name = "/".join(layer_name[2:])

        kernel_shape = line[-2].replace(')', '').replace(' ','')
        output_shape = line[-1].replace(')', '').replace('\n','').replace(' ','')

        kernel_shape = kernel_shape.split(",")
        output_shape = output_shape.split(",")

        # Skip anything that doesn't have a kernel as we only count MACs
        if kernel_shape[0] == '': continue

        kernel_shape = [int(i) for i in kernel_shape]
        output_shape = [int(i) for i in output_shape]

        kernel_ops = math.prod(kernel_shape)
        # Make sure not to double count output channels. First number in shape is example images which should also be skipped
        output_ops = math.prod(output_shape[1:3])
  
    
        total_gops += (kernel_ops*output_ops)
        print(layer_name, "kernel shape: ", kernel_shape, "output shape: ", output_shape[1:], "GOPS: ", float(kernel_ops*output_ops)/1000000000.0)


# Need to multiply by 2 as each MAC counts as 2 ops
total_gops = (float(total_gops)/1000000000.0) * 2.0
print("TOTAL GOPS:", total_gops)
