from time import sleep
import os
import re
from shutil import copyfile
model_dir = "/home/mat/drive1/sparse_resnet_imagenet_s85_fast_prune"
output_dir = "/home/mat/drive1/different_sparsity_resnet_models"
capture_highest_model_number = re.compile("model\.ckpt-(\d+)\.")
for i in range(10):
	files = os.listdir(model_dir)
	model_files = [f for f in files if "model.ckpt" in f]
	highest_number = 0
	for s in model_files:
		current_number = int(capture_highest_model_number.match(s).group(1))
		if current_number > highest_number:
			highest_number = current_number
	os.mkdir(output_dir + "/" + str(highest_number))
	files_to_copy = [f for f in model_files if ("model.ckpt-" + str(highest_number)) in f]
	for f in files_to_copy:
		copyfile(model_dir + "/" + f, output_dir + "/" + str(highest_number) + "/" + f)
	sleep(2500)
