import os
import json
import copy

# Define Chip Parameters, for now MX


MX_MAX_DSP = 3960
MX_MAX_MEM = 6840
MX_MAX_DSP_PERCENT = 0.9
MX_MAX_MEM_PERCENT = 0.95
TARGET_DSP_PERCENT = 0.7

# Define Default algorithm to be u_shape_move
PARTITION_ALGORITHM = 'naive'

# Define the path to dump the instances and other files
MULTI_CHIP_CIRCUIT_PATH = "generated_files/circuits/multi_chip"

os.system("rm default_precisions.annotations")
#Normal Flow with Single Chip
def run_single_chip():
	print("Running with a single chip ... \n")
	os.system("python3 -m hpipe.LayerComponents --param_file_path quartus_params.json --partitioning=True \n")


# Read the Normal HPIPE Params file to check if "multi_chip" paramter is set for more than 1
params_file = "quartus_params.json"
if os.path.isfile(params_file):
	with open(params_file, "r") as fh:
		default_json = json.load(fh)
	if "chip_count" not in default_json:
		run_single_chip()
		exit(0)
	if default_json["chip_count"] == 1:
		run_single_chip()
		exit(0)
else:
	run_single_chip()
	exit(0)


def prepend_line(file_name, line):
	""" Insert given string as a new line at the beginning of a file """
	# define name of temporary dummy file
	dummy_file = file_name + '.bak'
	# open original file in read mode and dummy file in write mode
	with open(file_name, 'r') as read_obj, open(dummy_file, 'w') as write_obj:
		# Write given line to the dummy file
		write_obj.write(line + '\n')
		# Read lines from original file one by one and append them to the dummy file
		for line in read_obj:
			write_obj.write(line)
	# remove original file
	os.remove(file_name)
	# Rename dummy file as the original file
	os.rename(dummy_file, file_name)



# At this point, we know that multi chip should be called

# |||||||||||||||||||||||||||||||| Stage 1 ||||||||||||||||||||||||||||||||
#Figure out the Hardware Setup (#chps, dsps/chip, mem/chip, allowed_dsp/chip, allowed_mem/chip, hpipe_dsp_target)


# There are two modes for the setup, 1) custom, 2) auto
# "custom" assumes a file with the config exists in the directory 'partition_params.json'
# "auto" generates automatic preset values for the different parameters

chip_count = default_json["chip_count"]
if "setup_mode" not in default_json:
	setup_mode = 'auto'
else :
	setup_mode = default_json["setup_mode"]


sample_partition_config = {
    "number_of_fpgas": 2,  
	#"The percent specifies the maximum allowed DSPs and M20Ks to be placed on a each chip. Individual percentages overwrite these "
    "target_dsp_percent": 0.9,
    "target_m20k_percent": 0.95,   

    #How much DSPs you want to generate HPIPE with. For example, if 2 chips and each has 100 DSPs you specify that to be 1 (100%)
	#and you will call hpipe to run with 200 DSPs and then in the partitioning the parameter "target_dsp_percent" will be used to 
	#determine the max allowed DSP on every chip 
    "dsp_percentage": 0.85,

    #"Optional - Maximum operating freuqency on the FPGAs (MHz)"
    "fmax": 400,
    "number_of_switches": 0,
    #"The Algorithm specifies which algorithm should be used. Can be [naive,u_shape,u_shape_move]"
    "algorithm": "naive",

    # "The types of connectivity available are [point_to_point,star,2d_torus]"
    "connectivity": "point_to_point",
    "hops": [
        {
            "id": 0,
            #"Type could be f-->fpga or s-->switch"
            "type": "f",
            "dsps": 5700,
            "m20k": 11000,
			#"The percent specifies the maximum allowed DSPs and M20Ks to be placed on this chip",
            "target_dsp_percent": 0.9,
            "target_m20k_percent": 0.95
        },
        {
            "id": 1,
            "type": "f",
            "dsps": 5700,
            "m20k": 11000,
            "target_dsp_percent": 0.9,
            "target_m20k_percent": 0.95
        }
    ],
    "links": [
        {
            "id": 0,

            #"Type could be f-f , f-s , s-s",
            "type": "f-f",
            "source": 0,
            "destination": 1,
            "bandwidth": 100
        }
    ],
    "filename": "mobilenet_v1",
    "intermediate_graph": MULTI_CHIP_CIRCUIT_PATH+"/"+str(int(chip_count))+".txt",
    "dump_numbers_file_path": "null",
    "dump_numbers": -1
}

# If setup mode is custom, read the partition_params from "partition_params.json" and make sure the #chips matches chip_count
partition_params_file = 'partition_params.json'
if setup_mode == 'custom':
	if not os.path.isfile(partition_params_file):
		print("Error: Partition Params file doesn't exist, please create the file or use mode=auto in the quartus_params.json")
		exit(1)
	else:
		with open(partition_params_file, "r") as fh:
			partition_json = json.load(fh)
		if partition_json["number_of_fpgas"] != chip_count:
			print("Error: Miss Match in chip count between quartus_params.json and partition_params.json")
			exit(1)

# If setup mode is auto, create a partition_params json based on the number of chips and pre-set percentages for MX device
else:
	partition_json = copy.deepcopy(sample_partition_config)
	partition_json["number_of_fpgas"] = chip_count
	partition_json["target_dsp_percent"] = MX_MAX_DSP
	partition_json["target_m20k_percent"] = MX_MAX_MEM 
	partition_json["hops"] = []
	partition_json["algorithm"] = PARTITION_ALGORITHM
	partition_json["dsp_percentage"] = MX_MAX_DSP_PERCENT 
	for ii in range(chip_count):
		node = {}
		node["id"] = ii
		node["type"] = "f"
		node["dsps"]  = MX_MAX_DSP
		node["m20k"] = MX_MAX_MEM
		node["target_dsp_percent"]= MX_MAX_DSP_PERCENT
		node["target_m20k_percent"] = MX_MAX_MEM_PERCENT
		partition_json["hops"].append(node)


	#For point to point I will just connect every two consecitive FPGAs
	partition_json["links"] = []
	for ii in range(chip_count-1):
		link = {}
		link["id"] = ii
		link["type"] = "f-f"
		link["source"]  = ii
		link["destination"] = ii+1
		link["bandwidth"]= 100
		partition_json["links"].append(link)
	with open(partition_params_file, "w") as fh:
		json.dump(partition_json, fh, indent=4)

#  |||||||||||||||||||||||||||||||| Stage 2 ||||||||||||||||||||||||||||||||
# Call the Partitioning Algorithm to generate the start and end points of every piece

# Prepare the Output directory
os.system("mkdir -p " + MULTI_CHIP_CIRCUIT_PATH)

# Call HPIPE for partitioning 
dsp_target = int(chip_count * MX_MAX_DSP * TARGET_DSP_PERCENT)
os.system("python3 -m hpipe.LayerComponents --param_file_path quartus_params.json --dsp_target="+str(dsp_target)+" --partitioning=True" +'\n')
# The file containing the cut points will be named "multi_chip_config.txt"
multi_chip_config_file = MULTI_CHIP_CIRCUIT_PATH+'/multi_chip_config.txt'
if os.path.isfile(multi_chip_config_file):
	fh = open(multi_chip_config_file) 
	lines = fh.readlines()
	fh.close()
else:
    print("Error: multi_chip_config_file doesn't exist, Partitioning might have failed, check  partitioning_log.txt")
    exit(1)

# IF the partitioning Failed, the multi_chip_config file will have the word "Failed"
if len(lines) == 1:
	if lines[0].split()[0] == 'Failed':
		print("Error: Partitioning Failed. Can not enter stage 3 and enerate Verilog")
		exit(1)


# |||||||||||||||||||||||||||||||| Stage 3 ||||||||||||||||||||||||||||||||
# If stage 1 and 2 are successful move to stage 3 and generate the multiple HPIPE pieces
print("Running with " + str(len(lines)) + " chips ... \n")
for i in range(len(lines)):
	json_new = copy.deepcopy(default_json)
	json_new["top_level_module_type"] = "npu_wrapper"
	print(lines[i].split(','))
	# First Piece
	if i ==0:
		print("creating first hpipe instance  \n")
		json_new["build_until"] = lines[i].split(',')[1].split()[0]

		json_new["generated_circuit_dir"] = MULTI_CHIP_CIRCUIT_PATH + "/hpipe_"+str(i+1)
		json_new["propagate_outputs_to_top"] = True
		json_file_name = "hpipe_"+str(i+1)+".json"
		json_new["instance_location"]  = "input"
		with open(json_file_name,'w') as f_json:
			json.dump(json_new,f_json, indent=4)

	# Remaining Pieces
	else:
		print("creating hpipe instance # " + str(i+1) +  "\n")
		json_new["build_until"] = lines[i].split(',')[1].split()[0]
		json_new["build_from"] = lines[i].split(',')[0]
		if i != len(lines)-1: #Intermediate chips
			json_new["propagate_outputs_to_top"] = True
			json_new["instance_location"]  = "intermediate"
		else: # Last chip will have output buffer
			json_new["propagate_outputs_to_top"] = False
			json_new["build_until"] = None
			json_new["instance_location"]  = "output"
			

		json_new["generated_circuit_dir"] = MULTI_CHIP_CIRCUIT_PATH + "/hpipe_"+str(i+1)
		json_file_name = "hpipe_"+str(i+1)+".json"
		with open(json_file_name,'w') as f_json:
			json.dump(json_new,f_json, indent=4)
	fff = open("generated_files/circuits/multi_chip/hpipe_piece_num.txt",'w')
	fff.write(str(i))
	fff.close()
	os.system("python3 -m hpipe.LayerComponents --param_file_path " + json_file_name +'  --dsp_target='+str(dsp_target) +'\n')
	#generate the include_list file 
	os.system("./build_utils/make_quartus_include_list.sh " + json_new["generated_circuit_dir"] + " " + json_new["generated_circuit_dir"])
	#Generate the params_file that carries the number of OC for the output FIFO Any instance other than the last one
	#Generate the file
	fifo_param_path = json_new["generated_circuit_dir"] + '/CIRCUIT/npu_fifo_params.sv'
	f_params = open(fifo_param_path,'w')
	f_params.write('`ifndef NPU_FIFO_PARAMS \n')
	f_params.write('`define NPU_FIFO_PARAMS \n')
	f_params.write('\n')
	f_params.write('parameter NPU_OUT_FIFO_DEPTH = ' + str(int(lines[i].split(',')[2].split()[0])*2) + '; \n')
	f_params.write('parameter NPU_OUT_FIFO_FULL = ' + str(int(lines[i].split(',')[2].split()[0])-1) + '; \n')
	f_params.write('\n')
	f_params.write('`endif \n')
	f_params.close()

	# Convert the include file to .sv

	#Add the file to the include_list
	include_file = json_new["generated_circuit_dir"] + '/include_list.v'
	include_file_sv = json_new["generated_circuit_dir"] + '/include_list.sv'
	os.system("mv " + include_file + ' ' + include_file_sv)
	include_file_sv = json_new["generated_circuit_dir"] + '/include_list.sv'
	new_line = "`include"+' "CIRCUIT/npu_fifo_params.sv"'
	prepend_line(include_file_sv,new_line)

	new_line = "`include"+' "CIRCUIT/generic_component_0.sv"'
	prepend_line(include_file_sv,new_line)

	new_line = "`include"+' "CIRCUIT/pll_reconfig_control.v"'
	prepend_line(include_file_sv,new_line)

############################### Succes #######################################
print("Partitioning Successfull ... Refer to generated_circuit/circuits/mlti_chip for more reports/logs \n")