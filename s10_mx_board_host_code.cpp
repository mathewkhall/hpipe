// (C) 2017-2018 Intel Corporation.
//
// Intel, the Intel logo, Intel, MegaCore, NIOS II, Quartus and TalkBack words
// and logos are trademarks of Intel Corporation or its subsidiaries in the
// U.S. and/or other countries. Other marks and brands may be claimed as the
// property of others. See Trademarks on intel.com for full list of Intel
// trademarks or the Trademarks & Brands Names Database (if Intel) or see
// www.intel.com/legal (if Altera). Your use of Intel Corporation's design
// tools, logic functions and other software and tools, and its AMPP partner
// logic functions, and any output files any of the foregoing (including
// device programming or simulation files), and any associated documentation
// or information are expressly subject to the terms and conditions of the
// Altera Program License Subscription Agreement, Intel MegaCore Function
// License Agreement, or other applicable license agreement, including,
// without limitation, that your use is for the sole purpose of programming
// logic devices manufactured by Intel and sold by Intel or its authorized
// distributors. Please refer to the applicable agreement for further details.

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <limits>
#include <time.h>
#include <termios.h>
#include <ctime>
#include <system_error>
#include <cerrno>
#include <stdexcept>
#include <fstream>
#include <cstring>
#include <string>
#include <chrono>
#include<math.h>
#include "intel_fpga_pcie_api.hpp"
#include "intel_fpga_pcie_link_test.hpp"

#define PLL_BASE_ADDR                   (0x10000)
#define INPUT_BUFFER_ADDR       		(0x70000)
#define OUTPUT_BUFFER_ADDR       		(0x40000)
#define STATUS_REGISTER_BASE_ADDR       (0x100000)
#define STATUS_REGISTER_ADDR            (0x100000 + 0x40 * 9)

uint32_t OUTPUT_HEIGHT = 1, OUTPUT_WIDTH = 1, OUTPUT_CHANNELS = 1001, OUTPUT_BITS_PER_ACT = 16;
uint32_t INPUT_HEIGHT = 224, INPUT_WIDTH = 224, INPUT_CHANNELS = 3, INPUT_BITS_PER_ACT = 8;

using namespace std;

static void dma_mode(intel_fpga_pcie_dev *dev);
static void dma_run(intel_fpga_pcie_dev *dev, bool run_rd, bool run_wr,
                    bool run_simul, unsigned int num_dw,
                    unsigned int num_desc);

bool GET_OUTPUT_SHAPE(intel_fpga_pcie_dev *dev)
{
  bool result;
  uint32_t tmp;
  result = dev->read32(0,reinterpret_cast<void *>(STATUS_REGISTER_BASE_ADDR+64*8),&tmp);
  result = dev->read32(0,reinterpret_cast<void *>(STATUS_REGISTER_BASE_ADDR+ 0),&OUTPUT_HEIGHT);
  result = dev->read32(0,reinterpret_cast<void *>(STATUS_REGISTER_BASE_ADDR+ 1*64),&OUTPUT_WIDTH);
  result = dev->read32(0,reinterpret_cast<void *>(STATUS_REGISTER_BASE_ADDR+ 2*64),&OUTPUT_CHANNELS);
  result = dev->read32(0,reinterpret_cast<void *>(STATUS_REGISTER_BASE_ADDR+ 3*64),&OUTPUT_BITS_PER_ACT);
  result = dev->read32(0,reinterpret_cast<void *>(STATUS_REGISTER_BASE_ADDR+ 4*64),&INPUT_HEIGHT);
  result = dev->read32(0,reinterpret_cast<void *>(STATUS_REGISTER_BASE_ADDR+ 5*64),&INPUT_WIDTH);
  result = dev->read32(0,reinterpret_cast<void *>(STATUS_REGISTER_BASE_ADDR+ 6*64),&INPUT_CHANNELS);
  result = dev->read32(0,reinterpret_cast<void *>(STATUS_REGISTER_BASE_ADDR+ 7*64),&INPUT_BITS_PER_ACT);

  printf("Output Shape: %d %d %d %d\n", OUTPUT_HEIGHT, OUTPUT_WIDTH, OUTPUT_CHANNELS, OUTPUT_BITS_PER_ACT);
  printf("Input Shape: %d %d %d %d\n", INPUT_HEIGHT, INPUT_WIDTH, INPUT_CHANNELS, INPUT_BITS_PER_ACT);
  uint32_t status;
  result = dev->read32(0,reinterpret_cast<void *>(STATUS_REGISTER_ADDR),&status);
  printf("HPIPE STATUS: 0x%08X\n", status);
  result = dev->read32(0,reinterpret_cast<void *>(STATUS_REGISTER_BASE_ADDR+ 64*8),&tmp);
  return result;
}

bool config_pll(intel_fpga_pcie_dev *dev)
{
    uint32_t config_id;
    uint32_t result;
    cout << "Enter id " << endl;
    cin  >> config_id;
    result = dev->write32(0,reinterpret_cast<void *>(PLL_BASE_ADDR), config_id);

    uint32_t waitrequest = 1;
    while(waitrequest == 1)
        result = dev->read32(0,reinterpret_cast<void *>(PLL_BASE_ADDR),&waitrequest);
    if(result == 1)
        cout << "Configuration Passed" << endl;
    return result;
}

/*static void do_rd(intel_fpga_pcie_dev *dev)
{
    uint64_t addr;
    uint32_t read_data;
    int result;
    std::ios_base::fmtflags f(cout.flags()); // Record initial flags

    cout << hex << showbase << setfill(' ') << internal;
    cin >> hex;

    cout << "Enter address to read, in hex:\n";
    cout << "> " << flush;
    cin >> addr;
    cin.clear();
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    cout << "Reading from BDF " << dev->get_dev();
    cout << " BAR " << dev->get_bar() << " offset " << addr;
    cout << ".." << endl;

    result = dev->read32(0,reinterpret_cast<void *>(addr), &read_data);
    if (result == 1) {
        cout << "Read " << read_data << endl;
    } else {
        cout << "Read failed!" << endl;
    }
    cout.flags(f); // Restore initial flags
}*/

int main(void)
{
    intel_fpga_pcie_dev *dev;
    uint16_t bdf = 0;
    int bar = -1;
    std::ios_base::fmtflags f(cout.flags()); // Record initial flags
    
    try {

        try {
            dev = new intel_fpga_pcie_dev(bdf,bar);
        } catch (const std::exception& ex) {
            cout << "Invalid BDF or BAR!" << endl;
            throw;
        }
        cout << hex << showbase;
        cout << "Opened a handle to BAR " << dev->get_bar();
        cout << " of a device with BDF " << dev->get_dev() << endl;
        //Just do one dma
        dma_mode(dev);
    } catch (std::exception& ex) {
        cout.flags(f); // Restore initial flags
        cout << ex.what() << endl;
        return -1;
    }
    
    cout.flags(f); // Restore initial flags
}



static void dma_mode(intel_fpga_pcie_dev *dev)
{
    int result;
    bool run_rd = true;
    bool run_wr = true;
    bool run_simul = true;
    unsigned int num_dw = 2048;
    unsigned int num_desc = 1;
	cout << "Got here" << endl;
    result = dev->use_cmd(true);
    if (result == 0) {
        cout << "Could not switch to CMD use mode!" << endl;
        return;
    }
    
    dma_run(dev, run_rd, run_wr, run_simul, num_dw, num_desc);


    result = dev->use_cmd(false);
    if (result == 0) {
        cout << "Could not switch back from CMD use mode!" << endl;
        return;
    }
}





static void dma_run(intel_fpga_pcie_dev *dev, bool run_rd, bool run_wr,
                    bool run_simul, unsigned int num_dw,
                    unsigned int num_desc)
{
    int result;
    void *mmap_addr;
    uint32_t *pImages[10];
    uint32_t * kdata;
    unsigned int alloc_size =  1024 * 1024;
    // Obtain kernel memory.
    result = dev->set_kmem_size(alloc_size);
    if (result != 1) {
        cout << "Could not get kernel memory!" << endl;
        return;
    }
    mmap_addr = dev->kmem_mmap(alloc_size, 0);
    if (mmap_addr == MAP_FAILED) {
        cout << "Could not get mmap kernel memory!" << endl;
        return;
    }
    kdata = reinterpret_cast<uint32_t *>(mmap_addr);
	


    //Read Output Shape 
    bool success = GET_OUTPUT_SHAPE(dev);
    if(!success)
    {
        cout << "Failed to read status reg \n";
        exit(1);
    }
    //Set the PLL Frequency
    config_pll(dev);

    // Read Bin Files


    FILE *ptr;
    
    int image_bytes = 172032;
    int total_read_images = 50000;
    int image_words = image_bytes / 4;
    int output_bytes = 2048;  //1056 16-bit values (32*512-bit)
    int images_per_file_first_9 = 5001;
    int images_per_file_last_one = 4991;
    char file_path[256];
    for (int image = 1; image < 10; ++image) {
        sprintf(file_path, "../../imagenet_val/imagenet_val_512_%d.bin", image);
        ptr = fopen(file_path,"rb");
        pImages[image-1] = (uint32_t *) malloc(images_per_file_first_9*image_bytes);
        cout << fread(pImages[image-1],images_per_file_first_9*image_bytes,1,ptr) << endl;
        fclose(ptr); 
    }
    //Load last batch
    sprintf(file_path, "../../imagenet_val/imagenet_val_512_%d.bin", 10);
    ptr = fopen(file_path,"rb");
    pImages[9] = (uint32_t *) malloc(images_per_file_last_one*image_bytes);
    cout << fread(pImages[9],images_per_file_last_one*image_bytes,1,ptr) << endl;
    
    fclose(ptr);  
    std::ofstream outdata;
    outdata.open("output.bin", std::ofstream::binary);

    std::chrono::time_point<std::chrono::system_clock> read_times[50000];
    std::chrono::time_point<std::chrono::system_clock> start_image_times[50000];
    int written_images = 0;
    int read_images = 0;
    uint32_t status;
    int same_status_count = 0;
    std::chrono::duration<double> elapsed(0);
    std::chrono::duration<double> sum_of_write_latencies(0);
    std::chrono::duration<double> sum_of_read_latencies(0);
    std::chrono::duration<double> sum_of_latencies(0);
    //std::chrono::duration<double> start_write(0);
   // std::chrono::duration<double> start_read(0);

    int read_idx=0;
    int start_image_idx=0;
    int end_image_idx=0;
    auto start = std::chrono::system_clock::now();
    //std::memcpy(kdata,images_ptr ,image_bytes);
    for(int image = 0;image<10;image++){
        written_images = 0;
        read_images = 0;
        if(image != 9)
            total_read_images = 5001;
        else
            total_read_images = 4991;
        int total_written_images = total_read_images;
        while(read_images < total_read_images) {
            // Keep reading status register;
            same_status_count = 0;
            status = 0;
        	while (status == 0 && same_status_count < 1000000 ) {
        		result = dev->read32(0,reinterpret_cast<void *>(STATUS_REGISTER_ADDR),&status);
        		++same_status_count;
        	}
        	if (same_status_count >= 1000000) {
        			printf("Status = 0x%08X\n", status);
        		}

               //DMA Write
           if ((written_images < total_written_images) && (status & 0x1)) {
                start_image_times[start_image_idx] = std::chrono::system_clock::now();;
                std::memcpy(kdata,pImages[image] + (written_images * image_words) ,image_bytes);
                result = dev->dma_queue_write(INPUT_BUFFER_ADDR, image_bytes, 0);
                if (result == 0) {
                    cout << "Could not queue DMA write! Aborting DMA.." << endl;
                    break;
                }
                result = dev->dma_send_write();
                sum_of_write_latencies += std::chrono::system_clock::now() - start_image_times[start_image_idx];
                start_image_idx++;
               // cout << "time " << elapsed.count() << endl;
               
        		if (result != 0) {
        			//cout << "Successfully Wrote data through DMA \n";
                    written_images ++;
                    
                    
        		}
        		else {
        			cout << "Failed to Write Data through DMA \n";
        		}
            }
            
            //DMA Read
            if(status & 0x2){
                   read_times[read_idx] = std::chrono::system_clock::now();
        		  result = dev->dma_queue_read(OUTPUT_BUFFER_ADDR, output_bytes, 0);
                    if (result == 0) {
                        cout << "Could not queue DMA read! Aborting DMA.." << endl;
                        break;
                    }

                    result = dev->dma_send_read();
                    sum_of_latencies += (std::chrono::system_clock::now() - start_image_times[end_image_idx++]);
                    sum_of_read_latencies += std::chrono::system_clock::now() - read_times[read_idx];
                    read_idx++;
                    if (result != 0) {
            		//cout << "Read Data successfully" << endl;
                        outdata.write(reinterpret_cast<char *>(kdata), static_cast<int>(output_bytes));
                        read_images++;
            	    }
            	   else {
            		cout << "Failed to do a DMA read \n";
            	    }
        	}
            
        }
        cout << "Finished batch # " << image << endl;
    }
    elapsed = (std::chrono::system_clock::now() - start);


    auto average_latency = sum_of_latencies.count() / 50000.;
    std::cout << "Total time for 50K images  is " << (elapsed.count()) << endl;
    std::cout << "Throughput is " << floor(50000/(elapsed.count())) << " images/s" <<endl;
    std::cout << "Average latency: " << average_latency << " s\n";
    std::cout << "Average Read Latency: " << (sum_of_read_latencies.count() / 50000.0) << "\n";
    std::cout << "Average Write Latency: " << (sum_of_write_latencies.count() / 50000.0) << "\n";
    result = dev->kmem_munmap(mmap_addr,alloc_size);
    if (result != 1) 
        cout << "Could not unmap Kernel memory! \n";
    outdata.close();
    cout << "Ending Operation Bye ... \n";
}


