#!/bin/bash
dir=$1
path=$2
sed -r "s|.*/CIRCUIT/|CIRCUIT/|" $path/verilog_paths.txt | grep -vP '(/quartus_top_0.v)' | awk '{print "`include \""$1"\""}' > $path/include_list.v
sed -r "s|.*/CIRCUIT/|CIRCUIT/|" $path/verilog_paths.txt | grep -vP '(/quartus_top_0.v)' | awk '{print "`include \""$1"\""}' > $path/include_list.sv
