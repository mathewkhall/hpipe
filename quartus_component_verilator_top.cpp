#include "Vhpipe_0.h"
#include "verilated.h"
#ifdef DUMP
#include "verilated_fst_c.h"
#endif
#include <iostream>
#include <fstream>

//#define MEM_SIZE                        (32*5376) //                                                                                                                     
//#define OUT_MEM_SIZE                    (2*16*50176) //

#define MEM_SIZE                        (32*5376) //                                                                                                                     
#define OUT_MEM_SIZE                    (2*16*50176) //
#define CHANNELS_OUT                    (1001)
#define WIDTH_OUT                       (1)
#define SIM_RUNTIME                     (100000000000)
#define INTERFACE_WIDTH  				(256)

double INTERFACE_CLOCK_PERIOD = 4.0*100; //250 MHz
//double INTERFACE_CLOCK_PERIOD = (100.0/15.0)*100.0; //150 MHz
double INTERFACE_PULSE_WIDTH = INTERFACE_CLOCK_PERIOD / 2.0;
//double INTERNAL_CLOCK_PERIOD = 2.5*100; //400 MHz
double INTERNAL_CLOCK_PERIOD = (10.0/3.0)*100.0; //300 MHz
double INTERNAL_PULSE_WIDTH = INTERNAL_CLOCK_PERIOD / 2.0;
double INTERNAL_CLOCK_PERIOD_2X = INTERNAL_CLOCK_PERIOD / 2.0;
double INTERNAL_PULSE_WIDTH_2X = INTERNAL_CLOCK_PERIOD_2X / 2.0;

double interface_time = 0.0;
double internal_time = 0.0;
double main_time = 0.0;       // Current simulation time
// This is a 64-bit integer to reduce wrap over issues and
// allow modulus.  You can also use a double, if you wish.

#ifdef DUMP
	VerilatedFstC* tfp;
#endif

double sc_time_stamp () {       // Called by $time in Verilog
    return main_time / 100.0;           // converts to double, to match
                                // what SystemC does
}

void eval_dump(Vhpipe_0* top) {
#ifdef DUMP
	tfp->dump(main_time);
#endif
	top->eval();
#ifdef DUMP
	tfp->dump(main_time);
#endif
}

void tick_internal_until(Vhpipe_0* top, double until) {
	while (internal_time < until) {
		top->sys_clock = !top->sys_clock;
		//top->sys_clock_2x = !top->sys_clock_2x;
		main_time = internal_time;
		eval_dump(top);
		internal_time += INTERNAL_PULSE_WIDTH;
	}
}

// This will run one full period of the interface clock,
// and will run the internal clock as many ticks as happen
// within that period
void tick(Vhpipe_0* top) {
	tick_internal_until(top, interface_time);
	top->io_clock = !top->io_clock;
	main_time = interface_time;
	eval_dump(top);
	interface_time += INTERFACE_PULSE_WIDTH;
	tick_internal_until(top, interface_time);
	top->io_clock = !top->io_clock;
	main_time = interface_time;
	eval_dump(top);
	interface_time += INTERFACE_PULSE_WIDTH;
}

void write_image(Vhpipe_0* top, char *image, int size) {
	top->system_builder_component_0_valid = 1;
	top->system_builder_component_0_chipselect = 1;
	int num_bytes = INTERFACE_WIDTH / 8;
	int write_count = 0;
	int write_address = 0;
	int repeat_line_write_counter = 0;
	for (int written_count = 0; written_count < size; written_count += num_bytes) {
		for (int j = 0; j < num_bytes; ++j) {
			((char*)top->system_builder_component_0_data)[j] = *image;
			++image;
		}
		top->system_builder_component_0_addr = write_address++;
		tick(top);
		++write_count;
		++repeat_line_write_counter;
		if (repeat_line_write_counter == 100) {
			// This models the strange behaviour from the PCIe controller
			// where we get the same address written multiple times.
			image -= num_bytes;
			--write_count;
			--write_address;
			repeat_line_write_counter = 0;
			written_count -= num_bytes;
		}
	}
	top->system_builder_component_0_valid = 0;
	top->system_builder_component_0_chipselect = 0;
	printf("Write count: %d\n", write_count);
}

void read_image(Vhpipe_0* top, char *image, int size) {
	top->system_builder_component_0_valid2 = 1;
	top->system_builder_component_0_chipselect2 = 1;
	top->system_builder_component_0_addr2 = 0;
	int num_bytes = INTERFACE_WIDTH / 8;
	const int read_image_pipe_delay = 9;
	for (int read_delay = 0; read_delay < (read_image_pipe_delay-1); ++read_delay) {
		tick(top);
		++top->system_builder_component_0_addr2;
	}
	for (int read_count = 0; read_count < size; read_count += num_bytes) {
		tick(top);
		for (int j = 0; j < num_bytes; ++j) {
			*image = ((char *)top->pcie_output_data)[j];
			++image;
		}
		++top->system_builder_component_0_addr2;
	}
	top->system_builder_component_0_valid2 = 0;
	top->system_builder_component_0_chipselect2 = 0;
}

int run_until_status_not_equal(Vhpipe_0* top, int other) {
	int status = other;
	while (status == other && main_time < SIM_RUNTIME) {
		top->system_builder_component_0_addr3 = 9;
		tick(top);
		status = top->status_register_pcie_read;
	}
	// the PCIe interface polls with some latency, so we will
	// model it as a fixed latency after we have found that the
	// status actually changed
	for (int i = 0; i < 923; ++i) {
		top->system_builder_component_0_addr3 = 9;
		tick(top);
		status = top->status_register_pcie_read;
	}
	return status;
}

int ceil(float in) {
	if (((float)((int)in)) < in) {
		return (int)in + 1;
	}
	return (int)in;
}

void compute_transfer_info(int h, int w, int c, int b, int transfer_bits, int* tensor_bytes, int *final_transfer_padding) {
	const int width_bits = w * b;
	bool is_deserialized = true;
	if (width_bits >= transfer_bits) {
		is_deserialized = false;
	}
	const int lines_per_width = ceil(width_bits/(float)transfer_bits);
	const int acts_per_line = transfer_bits/width_bits;
	const int acts_per_line_serialized = transfer_bits / b;
	*final_transfer_padding = lines_per_width * acts_per_line_serialized - w;
	std::cout << "Lines per width: " << lines_per_width << ". Acts per line: " << acts_per_line << ". Width: " << w << std::endl;
	int total_lines;
	if (is_deserialized) {
		total_lines = ceil(c*h/(float)acts_per_line);
	}
	else {
		total_lines = c*h*lines_per_width;
	}
	*tensor_bytes = total_lines * (transfer_bits / 8);
}

void reset(Vhpipe_0* top) {
	for (int i = 0; i < 923; ++i) {
		tick(top);
	}
	top->system_builder_component_0_addr3 = 8;
	tick(top);
}

int main(int argc, char** argv, char** env) {


	int written_count = 0;
	int processing_count = 0;
	int TOTAL_READ_COUNT = 10;
	int TOTAL_WRITE_COUNT = 10;

	char *pWrite;
	int *pIntWrite;
	char *pRead;
	int *pIntRead;
	char szError[256];
	std::ofstream outdata;
	outdata.open("generated_files/layer_images/verilog/verilator_read_out.bin", std::ofstream::binary);

	Verilated::commandArgs(argc, argv);
	Verilated::traceEverOn(true);

#ifdef DUMP
	tfp = new VerilatedFstC;
#endif
	Vhpipe_0* top = new Vhpipe_0;
	//top->sys_clock_2x = 0;
	top->sys_clock = 0;
	top->io_clock = 0;
	top->system_builder_component_0_reset = 1;
	top->system_builder_component_0_valid = 0;
	top->system_builder_component_0_valid2 = 0;
	top->system_builder_component_0_addr = 0;
	top->system_builder_component_0_addr2 = 0;
	top->system_builder_component_0_chipselect = 0;
	top->system_builder_component_0_chipselect2 = 0;
#ifdef DUMP
	top->trace(tfp, 99);
	tfp->open("sim.fst");
#endif
	std::cout << "Beginning hpipe simulation" << std::endl;
//	while (!Verilated::gotFinish()) {

	for (int i = 0; i < 4; ++i) {
		tick(top);
	}
	top->system_builder_component_0_reset = 0;

	int hi, wi, ci, bi;
	int ho, wo, co, bo;
	int remaining_pipe_delay = 9;
	int write_index = 0;
	int read_index = 0;
	bool all_read = false;
	while (read_index < 8) {
		if (write_index < 8) {
			top->system_builder_component_0_addr3 = write_index++;
		}
		tick(top);
		if (remaining_pipe_delay == 0) {
			if (read_index == 0) {
				ho = top->status_register_pcie_read;
			}
			else if (read_index == 1) {
				wo = top->status_register_pcie_read;
			}
			else if (read_index == 2) {
				co = top->status_register_pcie_read;
			}
			else if (read_index == 3) {
				bo = top->status_register_pcie_read;
			}
			else if (read_index == 4) {
				hi = top->status_register_pcie_read;
			}
			else if (read_index == 5) {
				wi = top->status_register_pcie_read;
			}
			else if (read_index == 6) {
				ci = top->status_register_pcie_read;
			}
			else if (read_index == 7) {
				bi = top->status_register_pcie_read;
			}
			++read_index;
		}
		else {
			--remaining_pipe_delay;
		}
	}
    outdata.write((char*)&co, sizeof(co));
    outdata.write((char*)&ho, sizeof(ho));
    outdata.write((char*)&wo, sizeof(wo));
    outdata.write((char*)&bo, sizeof(bo));
	printf("Input shape: %d %d %d %d\n", hi, wi, ci, bi);
	printf("Output shape: %d %d %d %d\n", ho, wo, co, bo);
	int nOutSize;
	int nTestSize;
	int inTransferPadding, outTransferPadding;
	compute_transfer_info(ho, wo, co, bo, INTERFACE_WIDTH, &nOutSize, &outTransferPadding);
	compute_transfer_info(hi, wi, ci, bi, INTERFACE_WIDTH, &nTestSize, &inTransferPadding);
	reset(top);
	printf("Input Buffer Bytes: %d\n", nTestSize);
	printf("Input Buffer Transfers: %d\n", nTestSize / (INTERFACE_WIDTH/8));
	printf("Output Buffer Bytes: %d\n", nOutSize);
	printf("Output Buffer Transfers: %d\n", nOutSize / (INTERFACE_WIDTH/8));
	printf("Output Transfer Padding: %d\n", outTransferPadding);
	pRead = (char *) malloc(nOutSize);
	pIntRead = (int *)pRead;
	pWrite = (char *) malloc(nTestSize*TOTAL_WRITE_COUNT);
	pIntWrite = (int *)pWrite;
	std::ifstream is ("generated_files/binary_inputs/many_inputs_1.bin", std::ifstream::binary);
	if (is.is_open()) {
		is.read(pWrite, nTestSize*TOTAL_WRITE_COUNT);
		is.close();
	}
	else {
		printf("Could not open "
		"generated_files/binary_inputs/many_inputs_1.bin."
		"\nWe use this file as input to the simulation.\n"
		"Please check that it exists.\n");
		exit(1);
	}


	int read_count = 0;
	int write_count = 0;
	int status = 0;
	int should_clear_status = 0;
	while (read_count < TOTAL_READ_COUNT && main_time < SIM_RUNTIME) {
		should_clear_status = 0;
		status = run_until_status_not_equal(top, status);
		printf("Status: %01X\n", status);
		if (status & 0x1 && write_count < TOTAL_WRITE_COUNT) {
			write_image(top, pWrite + (nTestSize * write_count), nTestSize);
			++write_count;
			should_clear_status = 1;
		}
		if (should_clear_status) {
			status = 0;
			run_until_status_not_equal(top, status);
		}
		
		if (status & 0x2) {
			int read_padding = 0;
			should_clear_status = 1;
			printf("About to read %d bytes\n", nOutSize);
			read_image(top, (char *)pRead, nOutSize);
			outdata.write((char *)pRead, nOutSize);
			printf("Read %d bytes\n", nOutSize);
			++read_count;
			if (0) {
		    for (int i = 0; (i + read_padding) < nOutSize; i+=1) {
		      if ((i % (wo*1)) != 0) {
		        outdata << ", ";
		      }
		      outdata << (int)(*((signed char *)(pRead + i + read_padding)));
		      if ((i % (wo*1)) == ((wo*1) - 1)) {
		        outdata << "\n";
		        read_padding += outTransferPadding;
		      }

		    }
			}
		}
		if (should_clear_status)
			status = 0;
	}
#ifdef DUMP
	tfp->close();
#endif
	top->final();
	delete top;
    outdata.close();
	exit(0);
}
