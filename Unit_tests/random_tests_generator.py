import sys
import csv
import numpy as np
import time
import random
import os

os.system('rm large_tests_set1_parameters.csv')
out_f = open('large_tests_set1_parameters.csv','w')
out_f.write('input_w,input_h,input_c,kernel_w,kernel_h,output_c,act_int,act_frac,wght_int,wght_frac,out_int,out_frac,stride_x,stride_y,padding_type,module_name,low_range_act,high_range_act,sparsity_level,channel_splits \n')

verified_modules = ["Conv2d","Sigmoid","Swish","ReLU"]
Padding_type = ["VALID","SAME"]
low_range = ['-1','0']
number_tests = 10


for i in range(number_tests):
	input_w = str(np.random.randint(1,225))
	input_h = input_w
	input_c = str(np.random.randint(3,128))
	kernel_w =str(np.random.randint(1,9))
	kernel_h =kernel_w
	output_c =str(np.random.randint(3,128))

	int_input = str(np.random.randint(1,4))
	frac_input= str(np.random.randint(3,10))

	int_weight = str(np.random.randint(1,4))
	frac_weight = str(np.random.randint(3,10))

	int_output = str(np.random.randint(4,6))
	frac_output = str(np.random.randint(10,15))

	x_stride = str(np.random.randint(1,4))
	y_stride = x_stride

	padding = random.choices(Padding_type,k=1)
	module = random.choices(verified_modules,weights=(95,1.25,1.25,1.25),k=1)
	low_range_act = random.choices(low_range,weights=(20,80))
	sparsity_level = str(np.random.uniform(0,0.95))
	n_splits = str(np.random.randint(1,40))

	out_f.write(input_w+','+input_h+','+input_c+','+kernel_w+','+kernel_h+','+output_c+
		','+int_input+','+frac_input+','+int_weight+','+frac_weight+','+int_output+','+frac_output+','+
		x_stride+','+y_stride+','+padding[0]+','+module[0]+','+low_range_act[0]+','+str(1)+','+ sparsity_level + ',' + n_splits +'\n')