// PCIE_FUNDAMENTAL.cpp : Defines the entry point for the console application.
//
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include "PCIE.h"
#include <iostream>
#include <fstream>
#include <chrono>
#include <ctime>
#include <thread>
#include <string.h>

#include <boost/python/module.hpp>
#include <boost/python/def.hpp>

//SET TO FALSE TO RUN ON FPGA
bool testing_mode = true;
bool perf_mode = false; //Turns off all print statements

#define DEMO_PCIE_USER_BAR			PCIE_BAR4
#define DEMO_PCIE_IO_LED_ADDR		0x4000010
#define DEMO_PCIE_IO_BUTTON_ADDR	0x4000020
#define DEMO_PCIE_MEM_ADDR			0x00000000
#define DEMO_PCIE_MEM_READ_ADDR			0x00000000
#define STATUS_REGISTER_BASE_ADDR       0x4000000
#define STATUS_REGISTER_ADDR            (0x4000000 + 0x4 * 9)
uint32_t OUTPUT_HEIGHT = 1, OUTPUT_WIDTH = 1, OUTPUT_CHANNELS = 1001, OUTPUT_BITS_PER_ACT = 16;
uint32_t INPUT_HEIGHT = 224, INPUT_WIDTH = 224, INPUT_CHANNELS = 3, INPUT_BITS_PER_ACT = 8;

#define PLL_BASE_ADDR 0x06000000

//#define MEM_SIZE			(512*1024) //512KB
#define MEM_SIZE			(32*5376) //
#define BASE_ADDRESS_JUMP               (32*5376) //
#define OUT_MEM_SIZE			(2*16*50176) //512KB
typedef enum {
  MENU_LED = 0, MENU_BUTTON, MENU_DMA_MEMORY, MENU_GET_OUTPUT_SHAPE, MENU_READ_UNTIL_BUFFER_CLEAR, MENU_DMA_FIFO, MENU_TEST_JUST_MEMORY, MENU_READ_FMAX, MENU_CONFIGURE_PLL, MENU_PRINT_PERIODIC_READINGS, MENU_QUIT = 99
} MENU_ID;

void UI_ShowMenu(void)
{
	printf("==============================\r\n");
	printf("[%d]: Led control\r\n", MENU_LED);
	printf("[%d]: Button Status Read\r\n", MENU_BUTTON);
	printf("[%d]: DMA Memory Test\r\n", MENU_DMA_MEMORY);
	printf("[%d]: Get Output Shape\r\n", MENU_GET_OUTPUT_SHAPE);
	printf("[%d]: Read Until Buffer Clear\r\n", MENU_READ_UNTIL_BUFFER_CLEAR);
	printf("[%d]: TEST_JUST_MEMORY\r\n", MENU_TEST_JUST_MEMORY);
	printf("[%d]: READ_FMAX\r\n", MENU_READ_FMAX);
	printf("[%d]: PROGRAM_PLL\r\n", MENU_CONFIGURE_PLL);
	printf("[%d]: PRINT_PERIODIC_READINGS\r\n", MENU_PRINT_PERIODIC_READINGS);
	printf("[%d]: Quit\r\n", MENU_QUIT);
	printf("Plesae input your selection:");
}

int UI_UserSelect(void)
{
	int nSel;
	scanf("%d", &nSel);
	return nSel;
}


bool configure_pll(PCIE_HANDLE hPCIe, unsigned int config_id){
  bool pass;
  pass = PCIE_Write32(hPCIe, DEMO_PCIE_USER_BAR, PLL_BASE_ADDR, config_id);
  uint32_t waitrequest = 1;
  while(waitrequest == 1){
    pass = PCIE_Read32(hPCIe, DEMO_PCIE_USER_BAR, PLL_BASE_ADDR, &waitrequest);
  }
  return pass;
}


bool print_periodic_readings(PCIE_HANDLE hPCIe) {
  bool bPass;
  char pRead[32];
  long previous = 0;
  const PCIE_LOCAL_ADDRESS read_base = DEMO_PCIE_MEM_READ_ADDR;
  for (int i = 0; i < 10; ++i) {
    bPass = PCIE_DmaRead(hPCIe, read_base, pRead, 32);
    std::cout << (*(long *) &pRead[0]) - previous << "\n";
    previous = (*(long *) &pRead[0]);
    std::this_thread::sleep_for (std::chrono::seconds(1));
  }
  return bPass;
}

bool GET_OUTPUT_SHAPE(PCIE_HANDLE hPCIe)
{
  bool bPass;
  uint32_t tmp;
  bPass = PCIE_Read32(hPCIe, DEMO_PCIE_USER_BAR, STATUS_REGISTER_BASE_ADDR+4*8, &tmp);
  bPass = PCIE_Read32(hPCIe, DEMO_PCIE_USER_BAR, STATUS_REGISTER_BASE_ADDR + 0, &OUTPUT_HEIGHT);
  bPass = PCIE_Read32(hPCIe, DEMO_PCIE_USER_BAR, STATUS_REGISTER_BASE_ADDR + 4, &OUTPUT_WIDTH);
  bPass = PCIE_Read32(hPCIe, DEMO_PCIE_USER_BAR, STATUS_REGISTER_BASE_ADDR + 8, &OUTPUT_CHANNELS);
  bPass = PCIE_Read32(hPCIe, DEMO_PCIE_USER_BAR, STATUS_REGISTER_BASE_ADDR + 12, &OUTPUT_BITS_PER_ACT);
  bPass = PCIE_Read32(hPCIe, DEMO_PCIE_USER_BAR, STATUS_REGISTER_BASE_ADDR + 16, &INPUT_HEIGHT);
  bPass = PCIE_Read32(hPCIe, DEMO_PCIE_USER_BAR, STATUS_REGISTER_BASE_ADDR + 20, &INPUT_WIDTH);
  bPass = PCIE_Read32(hPCIe, DEMO_PCIE_USER_BAR, STATUS_REGISTER_BASE_ADDR + 24, &INPUT_CHANNELS);
  bPass = PCIE_Read32(hPCIe, DEMO_PCIE_USER_BAR, STATUS_REGISTER_BASE_ADDR + 28, &INPUT_BITS_PER_ACT);

  if(!perf_mode) printf("Output Shape: %d %d %d %d\n", OUTPUT_HEIGHT, OUTPUT_WIDTH, OUTPUT_CHANNELS, OUTPUT_BITS_PER_ACT);
  if(!perf_mode) printf("Input Shape: %d %d %d %d\n", INPUT_HEIGHT, INPUT_WIDTH, INPUT_CHANNELS, INPUT_BITS_PER_ACT);
  uint32_t status;
  bPass = PCIE_Read32(hPCIe, DEMO_PCIE_USER_BAR, STATUS_REGISTER_ADDR, &status);
  if(!perf_mode) printf("HPIPE STATUS: 0x%08X\n", status);
  bPass = PCIE_Read32(hPCIe, DEMO_PCIE_USER_BAR, STATUS_REGISTER_BASE_ADDR+4*8, &tmp);
  return bPass;
}

bool READ_FMAX(PCIE_HANDLE hPCIe)
{
  bool bPass;
  uint32_t tmp;
  uint32_t avg;
  for (int i = 520, b = 1; i <= 1000; i+=20, b += 1) {
    avg = 0;
    for (int j = 0; j < 100; ++j) {
      bPass = PCIE_Read32(hPCIe, DEMO_PCIE_USER_BAR, STATUS_REGISTER_BASE_ADDR, &tmp);
      avg += tmp;
    }
    printf("%d %d %f\n", i-20, tmp, 250 * ((float)avg)/(800.0));
    bPass = PCIE_Write32(hPCIe, DEMO_PCIE_USER_BAR, 0, (uint32_t)b);
    for (int j = 0; j < 10000; ++j) {
      printf("\r");
    }
  }
  avg = 0;
  for (int j = 0; j < 100; ++j) {
    bPass = PCIE_Read32(hPCIe, DEMO_PCIE_USER_BAR, STATUS_REGISTER_BASE_ADDR, &tmp);
    avg += tmp;
  }
  bPass = PCIE_Read32(hPCIe, DEMO_PCIE_USER_BAR, STATUS_REGISTER_BASE_ADDR, &tmp);
  printf("%d %f\n", tmp, 250 * ((float)avg)/(800.0));
  return bPass;
}

bool TEST_LED(PCIE_HANDLE hPCIe)
{
	bool bPass;
	int Mask;

	printf("Please input led conrol mask:");
	scanf("%d", &Mask);

	bPass = PCIE_Write32(hPCIe, DEMO_PCIE_USER_BAR, DEMO_PCIE_IO_LED_ADDR,
		(uint32_t) Mask);
	if (bPass)
		printf("Led control success, mask=%xh\r\n", Mask);
	else
		printf("Led conrol failed\r\n");

	return bPass;
}

bool TEST_BUTTON(PCIE_HANDLE hPCIe)
{
	bool bPass = true;
	uint32_t Status;

	bPass = PCIE_Read32(hPCIe, DEMO_PCIE_USER_BAR, DEMO_PCIE_IO_BUTTON_ADDR,
		&Status);
	if (bPass)
		printf("Button status mask:=%xh\r\n", Status);
	else
		printf("Failed to read button status\r\n");

	return bPass;
}

char PAT_GEN(int nIndex)
{
	char Data;
	//Data = nIndex & 0xFF;
	Data = 0xFF;
	return Data;
}

char* fake_pcie_buffer;
bool fake_pcie_buffer_allocated = false;

void fake_write(char* pWrite, int nTestSize){
	
	printf("FAKE WRITE\n");
	
	if (!fake_pcie_buffer_allocated) fake_pcie_buffer = (char *) malloc(nTestSize);
	
	memcpy(fake_pcie_buffer, pWrite, nTestSize);
	
}

bool images_loaded = false;
bool out_buffer_allocated = false;
char * pImages;
char *pRead;

void load_images_file(const char* path,  long *size_ref) {
  std::ifstream in(path, std::ifstream::binary);
  in.seekg(0, in.end);
  long size = static_cast<long>(in.tellg());
  in.seekg(0, in.beg);
  
	//long size = 7526450176;
	 if(!perf_mode) printf("Size %ld\n", size);

  *size_ref = size;
  pImages = (char *) malloc(size);
	if(!(pImages)) printf("ERROR: failed to allocate input buffer of size %d\n", size);
  in.read(pImages, size);
  in.close();
	images_loaded = true;
}

bool READ_UNTIL_BUFFER_CLEAR(PCIE_HANDLE hPCIe)
{
    GET_OUTPUT_SHAPE(hPCIe);
	bool bPass = true;
	int i;
	const int nTestSize = MEM_SIZE;
	const int OUTPUTS_PER_READ = 256 / 16;
	int reads_per_line = OUTPUT_WIDTH / (OUTPUTS_PER_READ);
	if ((OUTPUT_WIDTH % OUTPUTS_PER_READ) != 0) {
	  ++reads_per_line;
	}
	int padding_to_skip = (OUTPUTS_PER_READ - (OUTPUT_WIDTH % OUTPUTS_PER_READ)) * 2;
	if (OUTPUT_WIDTH < OUTPUTS_PER_READ) {
	  padding_to_skip = 0;
	}
	const int BYTES_PER_READ = 256 / 8;
	float partial_reads = (((float)OUTPUT_CHANNELS) / reads_per_line);
	int reads = (int) partial_reads;
	if (((float)((int)partial_reads)) < partial_reads) {
		++reads;
	}
	const int nOutSize = OUTPUT_HEIGHT * reads * BYTES_PER_READ;
	const PCIE_LOCAL_ADDRESS LocalAddr = DEMO_PCIE_MEM_ADDR;
	const PCIE_LOCAL_ADDRESS read_base = DEMO_PCIE_MEM_READ_ADDR;
	uint32_t status;
	char *pRead;
	char szError[256];
	int base_address = BASE_ADDRESS_JUMP;
	std::ofstream outdata;
	//char *message = "hello world";

	//pWrite = (char *) malloc(nTestSize);
	pRead = (char *) malloc(nOutSize);
	if (!pRead) {
		bPass = false;
		sprintf(szError, "DMA Memory:malloc failed\r\n");
	}
	
	bPass = PCIE_Read32(hPCIe, DEMO_PCIE_USER_BAR, STATUS_REGISTER_ADDR, &status);
	while ((status&0x1)) {
	  bPass = PCIE_Read32(hPCIe, DEMO_PCIE_USER_BAR, STATUS_REGISTER_ADDR, &status);
	  bPass = PCIE_DmaRead(hPCIe, read_base, pRead, nOutSize);
	  if (!bPass) {
	    sprintf(szError, "DMA Memory:PCIE_DmaRead failed\r\n");
	  	break;
	  }
	}

	if (pRead)
		free(pRead);

	if (!bPass)
		printf("%s", szError);
	else
		 if(!perf_mode)printf("DMA-Memory (Size = %d byes) pass\r\n", nTestSize);

	return bPass;

}

int hpipe_ceil(float in) {
	if (((float)((int)in)) < in) {
		return (int)in + 1;
	}
	return (int)in;
}

void compute_transfer_info(int h, int w, int c, int b, int transfer_bits, int* tensor_bytes, int *final_transfer_padding) {
	const int width_bits = w * b;
	bool is_deserialized = true;
	if (width_bits >= transfer_bits) {
		is_deserialized = false;
	}
	const int lines_per_width = hpipe_ceil(width_bits/(float)transfer_bits);
	const int acts_per_line = transfer_bits/width_bits;
	const int acts_per_line_serialized = transfer_bits / b;
	*final_transfer_padding = lines_per_width * acts_per_line_serialized - w;
	int total_lines;
	if (is_deserialized) {
		total_lines = hpipe_ceil(c*h/(float)acts_per_line);
	}
	else {
		total_lines = c*h*lines_per_width;
	}
	*tensor_bytes = total_lines * (transfer_bits / 8);
}

bool TEST_DMA_MEMORY(PCIE_HANDLE hPCIe, unsigned image_id)
{
    if (!testing_mode) GET_OUTPUT_SHAPE(hPCIe);
	bool bPass = true;
	int i;
	const int OUTPUTS_PER_READ = 256 / 16;
	int reads_per_line = OUTPUT_WIDTH / (OUTPUTS_PER_READ);
	if ((OUTPUT_WIDTH % OUTPUTS_PER_READ) != 0) {
	  ++reads_per_line;
	}
	int padding_to_skip = (OUTPUTS_PER_READ - (OUTPUT_WIDTH % OUTPUTS_PER_READ)) * 2;
	if (OUTPUT_WIDTH < OUTPUTS_PER_READ) {
	  padding_to_skip = 0;
	}
	if (testing_mode) printf("Padding to skip: %d\n", padding_to_skip);
	const int interface_width = 256;
	int nOutSize;
	int nTestSize;
	int inTransferPadding, outTransferPadding;
	int ho = OUTPUT_HEIGHT, wo = OUTPUT_WIDTH, co = OUTPUT_CHANNELS, bo = OUTPUT_BITS_PER_ACT;
	int hi = INPUT_HEIGHT, wi = INPUT_WIDTH, ci = INPUT_CHANNELS, bi = INPUT_BITS_PER_ACT;
	compute_transfer_info(ho, wo, co, bo, interface_width, &nOutSize, &outTransferPadding);
	compute_transfer_info(hi, wi, ci, bi, interface_width, &nTestSize, &inTransferPadding);
	if (testing_mode) printf("Output Shape: %d %d %d %d\n", ho, wo, co, bo);
	if (testing_mode) printf("Input Shape: %d %d %d %d\n", INPUT_HEIGHT, INPUT_WIDTH, INPUT_CHANNELS, INPUT_BITS_PER_ACT);
	if (testing_mode) printf("nOutSize: %d\n", nOutSize);
	if (testing_mode) printf("nTestSize: %d\n", nTestSize);
	const PCIE_LOCAL_ADDRESS LocalAddr = DEMO_PCIE_MEM_ADDR;
	const PCIE_LOCAL_ADDRESS read_base = DEMO_PCIE_MEM_READ_ADDR;
	uint32_t status;
	char *pWrite;

	long images_size;
	
	char szError[256];
	char file_path[256];
	int base_address = BASE_ADDRESS_JUMP;
	std::chrono::time_point<std::chrono::system_clock> start_times[8];
	int write_index = 0;
	int read_index = 0;
	//std::ifstream outdata;
	//outdata.open("from_pcie.bin", std::ifstream::binary);

	std::ofstream outdata;
	outdata.open("from_pcie.bin", std::ofstream::binary);
	outdata.write((char*)&co, 4);
	outdata.write((char*)&ho, 4);
	outdata.write((char*)&wo, 4);
	outdata.write((char*)&bo, 4);
	//char *message = "hello world";
	int total_image_count = 50000;

	sprintf(file_path, "many_inputs.bin");
	//sprintf(file_path, "/home/stanmari/MLperf_imagenet/many_inputs.bin");
	if (images_loaded == false) load_images_file(file_path,  &(images_size));

	if (!out_buffer_allocated) {
		pRead = (char *) malloc(nOutSize);
		out_buffer_allocated = true;
	}
	
	if (!pRead){
		printf("ERROR: pRead malloc faile\n");
		return 0;
	}

	//for (int test_number = 0; test_number < 100; ++test_number) {
	int test_read_images = 0;
	std::chrono::duration<double> sum_of_latencies(0);
	std::chrono::duration<double> sum_of_write_latencies(0);
	std::chrono::duration<double> sum_of_read_latencies(0);
	auto total_start_time = std::chrono::system_clock::now();
	long total_status_checks = 0;
	double time_between_outputs[50000];
	int total_read_images = 0;
	std::chrono::time_point<std::chrono::system_clock> last_output_time = std::chrono::system_clock::now();

	 int image = 0;
	pWrite = pImages; 
	if (!pWrite || !pRead) {
		bPass = false;
		sprintf(szError, "DMA Memory:malloc failed\r\n");
	}
	
	int written_images = 0;
	int read_images = 0;
	int print_count = 0;
	int image_count = (int)(images_size / (long)nTestSize);
	
	if (!testing_mode){
		  int same_status_count = 0;
		  status = 0;
		  while (status == 0 && same_status_count < 1000000) {
				++total_status_checks;
			bPass = PCIE_Read32(hPCIe, DEMO_PCIE_USER_BAR, STATUS_REGISTER_ADDR, &status);
			++same_status_count;
		  }
		  if (same_status_count >= 1000000) {
			printf("Status = 0x%08X\n", status);
		  }
	} else {
		status = 1;
	}
	
		//Calculate correct image id
		long offset = (long) nTestSize * (long)(image_id);
		pWrite = pWrite + offset;
		if (testing_mode) printf("Base 0x%x pWrite 0x%x offset %ll\n",  pImages, pWrite, offset);

	  // write test pattern
	  if (bPass && (status & 0x1)) {
	    start_times[write_index] = std::chrono::system_clock::now();
	    if (!testing_mode) bPass = PCIE_DmaWrite(hPCIe, LocalAddr, pWrite, nTestSize);
		else fake_write(pWrite, nTestSize);
	    sum_of_write_latencies += std::chrono::system_clock::now() - start_times[write_index];
	    ++write_index;
	    if (write_index > 7)
	      write_index = 0;

	    if (!bPass)
	      sprintf(szError, "DMA Memory:PCIE_DmaWrite failed\r\n");
	  }
	

	
	if (!testing_mode){
		  int same_status_count = 0;
		  status = 0;
		  while (!(status & 0x2) && same_status_count < 1000000) {
				++total_status_checks;
			bPass = PCIE_Read32(hPCIe, DEMO_PCIE_USER_BAR, STATUS_REGISTER_ADDR, &status);
			++same_status_count;
		  }
		  if (same_status_count >= 1000000) {
			printf("Status = 0x%08X\n", status);
		  }
	} else {
		status = 0x2;
	}

	  //bPass = PCIE_Read32(hPCIe, DEMO_PCIE_USER_BAR, STATUS_REGISTER_ADDR, &status);
	  //printf("Status after write: 0x%08X\n", status);
	  // read back test pattern and verify
	  if (bPass && (status & 0x2)) {
	    auto before_read_time = std::chrono::system_clock::now();
	    if (!testing_mode) bPass = PCIE_DmaRead(hPCIe, read_base, pRead, nOutSize);
	    time_between_outputs[total_read_images++] = (std::chrono::system_clock::now() - last_output_time).count();
	    last_output_time = std::chrono::system_clock::now();
	    sum_of_latencies += (std::chrono::system_clock::now() - start_times[read_index++]);
	    sum_of_read_latencies += std::chrono::system_clock::now() - before_read_time;
	    if (read_index > 7)
	      read_index = 0;
	    ++read_images;
	    ++test_read_images;
	    ++print_count;
	    if (!bPass) {
	      sprintf(szError, "DMA Memory:PCIE_DmaRead failed\r\n");
	    } else {

	      outdata.write(pRead, nOutSize);
	      if (0) {

		  int within_line_count = 0;
		  for (i = 0; i < nOutSize; i+=1) {
		    if (within_line_count != 0) {
		      outdata << ", ";
		    }
		    outdata << (int)*((signed char *)(pRead + i));
		    ++within_line_count;
		    if ((i+1) == nOutSize/*within_line_count == wo && ((i+1) < nOutSize)*/) {
		      //outdata << "\n";
		      //i += outTransferPadding;
		      within_line_count = 0;
		    }
		  
		  }
		  outdata << "\n";
	      }
//#endif
	      if (print_count == 10) {
		 if(!perf_mode) printf("Done %d/50000\r", test_read_images);
		fflush(stdout);
		print_count = 0;
	      }
	    }
	  }

	  uint32_t tmp;
	
/*
	auto average_latency = sum_of_latencies.count() / 50000.;
	auto total_end_time = std::chrono::system_clock::now();
	std::chrono::duration<double> diff = total_end_time-total_start_time;
	//std::cout << "Total time to process 50k images: " << diff.count() << " s\n";
	//std::cout << "Average latency: " << average_latency << " s\n";
	//std::cout << "Average Status Checks: " << (total_status_checks / 50000.0) << "\n";
	//std::cout << "Average Read Latency: " << (sum_of_read_latencies.count() / 50000.0) << "\n";
	//std::cout << "Average Write Latency: " << (sum_of_write_latencies.count() / 50000.0) << "\n";
	std::ofstream between_output_times;
	between_output_times.open("between_output_times.csv");
	for (int i = 0; i < 50000; ++i) {
	  between_output_times << time_between_outputs[i] << "\n";
	}
	between_output_times.close();

	//printf("\n%d\n", test_number);
	//} // test number
	//printf("\n");
	//outdata.close();

*/


	if (!bPass)
		printf("%s", szError);
	else
		 if(!perf_mode) printf("DMA-Memory (Size = %d byes) pass\r\n", nTestSize);

	return image_id; //Dummy return for now
}

bool CONFIGURE_PLL(PCIE_HANDLE hPCIe)
{
  int nsel;
  printf("PLL Config index: ");
  nsel = UI_UserSelect();
  return configure_pll(hPCIe, nsel);
}

bool TEST_JUST_MEMORY(PCIE_HANDLE hPCIe)
{
  int input_data[8192];
  int output_data[8192];
  bool bPass = true;
  bPass = PCIE_DmaWrite(hPCIe, DEMO_PCIE_MEM_ADDR, (char *) input_data, 8192*4);
  bPass = PCIE_DmaRead(hPCIe, DEMO_PCIE_MEM_READ_ADDR, (char *) output_data, 8192*4);
  for (int i = 0; i < 8192; i+=8) {
    printf("%d %d\n", output_data[i+1], output_data[i]);
  }

  return bPass;
}

//Main function for mlperf
int hpipe_main(unsigned image_id)
{
    	void *lib_handle;
	PCIE_HANDLE hPCIE;
	bool bQuit = false;
	int nSel;

	 if(!perf_mode) printf("\n== HPIPE Driver: Image %u ==\r\n", image_id);

	if(!testing_mode) lib_handle = PCIE_Load();
	if (!testing_mode && !lib_handle) {
		printf("PCIE_Load failed!\r\n");
		return 0;
	}
	
	unsigned prediction;

	
	if (!testing_mode) hPCIE = PCIE_Open(DEFAULT_PCIE_VID, DEFAULT_PCIE_DID, 0);
	
	if (!testing_mode && !hPCIE) {
		printf("PCIE_Open failed\r\n");
	} else {
		prediction = TEST_DMA_MEMORY(hPCIE, image_id);
		if (!testing_mode) PCIE_Close(hPCIE);

	}

	if (!testing_mode) PCIE_Unload(lib_handle);
	
	
	return prediction;
}

//Boost function for linking up with mlperf
BOOST_PYTHON_MODULE(hpipe_ext)
{
   	using namespace boost::python;
    def("hpipe_main", hpipe_main);
}


//Not used for mlperf
int main(int argc, char* argv[])
{
	void *lib_handle;
	PCIE_HANDLE hPCIE;
	bool bQuit = false;
	int nSel;

	printf("== Terasic: PCIe Demo Program ==\r\n");

	lib_handle = PCIE_Load();
	if (!lib_handle) {
		printf("PCIE_Load failed!\r\n");
		return 0;
	}

	hPCIE = PCIE_Open(DEFAULT_PCIE_VID, DEFAULT_PCIE_DID, 0);
	if (!hPCIE) {
		printf("PCIE_Open failed\r\n");
	} else {
		while (!bQuit) {
			UI_ShowMenu();
			nSel = UI_UserSelect();
			switch (nSel) {
			case MENU_LED:
				TEST_LED(hPCIE);
				break;
			case MENU_BUTTON:
				TEST_BUTTON(hPCIE);
				break;
			case MENU_DMA_MEMORY:
				TEST_DMA_MEMORY(hPCIE, 0);
				break;
			case MENU_GET_OUTPUT_SHAPE:
				GET_OUTPUT_SHAPE(hPCIE);
				break;
			case MENU_READ_UNTIL_BUFFER_CLEAR:
				READ_UNTIL_BUFFER_CLEAR(hPCIE);
				break;
			case MENU_TEST_JUST_MEMORY:
				TEST_JUST_MEMORY(hPCIE);
				break;
			case MENU_READ_FMAX:
				READ_FMAX(hPCIE);
				break;
			case MENU_CONFIGURE_PLL:
			        CONFIGURE_PLL(hPCIE);
			        break;
			case MENU_PRINT_PERIODIC_READINGS:
			        print_periodic_readings(hPCIE);
				break;
			case MENU_QUIT:
				bQuit = true;
				printf("Bye!\r\n");
				break;
			default:
				printf("Invalid selection\r\n");
			} // switch

		} // while

		PCIE_Close(hPCIE);

	}

	PCIE_Unload(lib_handle);
	return 0;

}

