import numpy as np
import math
import sys
from matplotlib import pyplot as plt
#classes = list(np.fromfile("generated_files/binary_inputs/classes.bin", np.int16))

def load_bin_predicions(fh):
	read_int = lambda: np.frombuffer(fh.read(4), dtype=np.uint32)[0]
	#print(np.unpackbits(np.frombuffer(fh.read(4*4), dtype=np.ubyte)))

	#sys.exit(0)
	dims = {n : read_int() for n in ["c", "h", "w"]}

	pcie_transfer_bits = 256
	pcie_transfer_bytes = pcie_transfer_bits // 8

	bits = read_int()
	#print(dims)
	#print(bits)
	values_per_transfer = pcie_transfer_bits // bits

	output_size = np.prod(list(dims.values()))
	#print(output_size)
	fractional_transfers_per_output = output_size / values_per_transfer

	integer_transfers_per_output = int(fractional_transfers_per_output)
	total_transfers_per_output = math.ceil(fractional_transfers_per_output)
	fraction_of_normal_transfer_in_final_transfer = fractional_transfers_per_output - integer_transfers_per_output

	values_in_final_transfer = int(fraction_of_normal_transfer_in_final_transfer * values_per_transfer + 0.5)
	bytes_for_final_transfer = math.ceil(values_in_final_transfer * bits / 8)
	final_transfer_padding = pcie_transfer_bytes - bytes_for_final_transfer

	bytes_per_output = total_transfers_per_output * pcie_transfer_bytes #integer_transfers_per_output * pcie_transfer_bytes + bytes_for_final_transfer

	data = np.frombuffer(fh.read(), dtype=np.ubyte)
	#data = data[:int(output_size*2)]
	#data = data.reshape([-1, total_transfers_per_output, pcie_transfer_bytes])
	#data = np.pad(data, [[0,0], [0, final_transfer_padding]], mode="constant", constant_values=0)
	#data = data.reshape([-1, total_transfers_per_output, pcie_transfer_bytes])
	data = np.unpackbits(data)
	data = np.reshape(data, [-1,8])
	data = np.flip(data, -1)
	data = data.reshape([-1, total_transfers_per_output * pcie_transfer_bits])
	data = data.reshape([-1, total_transfers_per_output, pcie_transfer_bits])
	#print(data[0,0,:])
	transfer_bits = values_per_transfer * bits
	data = data[:,:,:transfer_bits]
	data = data.reshape([-1, total_transfers_per_output, values_per_transfer, bits])
	closest_dtype_bits = 2 ** math.ceil(math.log(bits, 2))
	dtype_bit_difference = closest_dtype_bits - bits
	if dtype_bit_difference > 0:
		sign_extend = np.tile(data[:,:,:, bits-1:bits], [1,1,1,dtype_bit_difference])
		data = np.concatenate([data, sign_extend], axis=-1)
	
	data = np.reshape(data, [-1,8])
	data = np.flip(data, -1)
	data = np.packbits(data, axis=-1)
	#print(data[0:19])
	data = data.tobytes()
	closest_dtype = np.byte
	if closest_dtype_bits == 16:
		closest_dtype = np.short
	if closest_dtype_bits == 32:
		closest_dtype = np.int
	data = np.frombuffer(data, dtype=closest_dtype)
	data = data.reshape([-1, values_per_transfer * total_transfers_per_output])
	data = data[:, :output_size]
	#data = data.reshape([-1] + [dims[k] for k in ["h", "c", "w"]])
	#data = np.transpose(data, [0, 2,1,3])
	#predictions = data[0:1]
	predictions = data.reshape([-1, output_size])
	return predictions

#with open("generated_files/layer_images/verilog/verilator_read_out.bin", "rb") as fh:
#	predictions = load_bin_predicions(fh)

#with open("mobilenet_v2_from_pcie_2.bin", "rb") as fh:
#	predictions = load_bin_predicions(fh)
