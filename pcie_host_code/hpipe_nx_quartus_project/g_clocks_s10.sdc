#**************************************************************
# Create Clock
#**************************************************************
create_clock -period 3.103 -name Gck0 Gck0
create_clock -period 3.103 -name Gck1 Gck1
create_clock -period 3.103 -name Gck2 Gck2
create_clock -period 8.000 -name Gck3 Gck3
##create_clock -period   -name Gck4 Gck4
if {$PROC1C == 1} {
create_clock -period 40.000 -name Gck_pll Gck_pll
create_clock -period 8.000 -name Gck_Osc Gck_Osc
}
create_clock -period 26.667 -name clk2 [get_registers $clk2_reg]
if {$PROC1C == 0} {
create_clock -period 5.000 -name ext_clk ext_clk
}
#**************************************************************
# Set user pll SDC variables
#**************************************************************
set g_user_pll_ref_clk if_hpipe|$g_user_pll_ref_clk_
set g_user_pll_n_cnt_clk if_hpipe|$g_user_pll_n_cnt_clk_
set g_user_pll_clk0 if_hpipe|$g_user_pll_clk0_
set g_user_pll_clk1 if_hpipe|$g_user_pll_clk1_

set n_cnt_clk if_hpipe|$n_cnt_clk_name
set clk0 if_hpipe|$clk0_name
set clk if_hpipe|$clk_name
#**************************************************************
# Create Generated Clock
#**************************************************************
create_generated_clock -name $n_cnt_clk -source $g_user_pll_ref_clk -divide_by 5 -multiply_by 1 -duty_cycle 50.00 $g_user_pll_n_cnt_clk -add
create_generated_clock -name $clk0 -source $g_user_pll_n_cnt_clk -divide_by 2 -multiply_by 24 -duty_cycle 50.00 $g_user_pll_clk0 -add
create_generated_clock -name $clk -source $g_user_pll_n_cnt_clk -divide_by 1 -multiply_by 24 -duty_cycle 50.00 $g_user_pll_clk1 -add
#**************************************************************
# Set False Path
#**************************************************************
# CLK0
set_false_path -from [get_clocks $clk0] -to [get_clocks $lb_clock]
set_false_path -from [get_clocks $clk0] -to [get_clocks $pld_clk]
set_false_path -from [get_clocks $clk0] -to [get_clocks $clk]
set_false_path -from [get_clocks $clk0] -to [get_clocks $clk_125m]
set_false_path -from [get_clocks $pld_clk] -to [get_clocks $clk0]
set_false_path -from [get_clocks $lb_clock] -to [get_clocks $clk0]
set_false_path -from [get_clocks $clk_125m] -to [get_clocks $clk0]
# CLK
set_false_path -from [get_clocks $clk] -to [get_clocks $lb_clock]
set_false_path -from [get_clocks $clk] -to [get_clocks $clk0]
set_false_path -from [get_clocks $clk] -to [get_clocks $clk_125m]
set_false_path -from [get_clocks $lb_clock] -to [get_clocks $clk]
set_false_path -from [get_clocks $clk_125m] -to [get_clocks $clk]
# CLK2
set_false_path -from [get_clocks $lb_clock] -to [get_clocks clk2]
set_false_path -from [get_clocks clk2] -to [get_clocks $lb_clock]
