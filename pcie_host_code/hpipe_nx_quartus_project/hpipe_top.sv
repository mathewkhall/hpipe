 
//*********************************************************************
//*  module  hpipe_top                                                *
//*  Created    :  Sat Dec 04 19:32:13 2021                           *
//*********************************************************************



//Note that this will currently only work for the Imagenet dataset as there are hard coded values for the input/output address counters
//Otherwise we would need some sort of initialization FSM to compute these, or we would need to have HPIPE generate these values for the top-level module to read 

module  hpipe_top
(
     input                          clrn,                                              // 0: global reset 
     input                          lclk,                                              // Clock
     input                          fast_clock,                                        // Clock
	  input									fast_clock_2x,													//2x fast_clock
     input          [31:0]          addr_wr,                                           // Internal bus: Write address
     input          [31:0]          addr_rd,                                           // Internal bus: Read address
     output         [255:0]         data_mem_rd,                                       // Internal bus: Read data
     input          [255:0]         data_mem_wr,                                       // Internal bus: Write data
     output logic                   mem_ready_wr,                                      // Internal Write bus: 1 - ready, 0 - add wait state. 
     output                         mem_ready_rd,                                      // Internal Read bus: 1 - ready, 0 - add wait state
	  output         [31:0]           user_dreq, 														// DMA control - assert low to enable DMA operation, high to stop DMA (for each DMA channel).
     input                          l_eos,                                             // 1 - end of stream (EOS) flag of the last DMA transfer to user logic
     input          [31:0]          l_be,                                              // represent enable only for the remaining bytes at the EOS
     input                          sel_hpipe_input_mem,                               
     input                          sel_hpipe_output_mem,                              
     input                          sel_hpipe_misc_regs_wr,                            
     input                          sel_hpipe_misc_regs_rd,                            
     output         [31:0]          hpipe_status_reg,                                  
     output      [31:0]          debug_read_registers[10],                             
     input       [31:0]          debug_write_registers[5]                              
);

//Note: The DMA writes are executed using a DMA List structure in the C++ driver, by leveraging the DREQ signal when no more space is available. All
// the images are sent in one burst, and this increases throughput significantly (up to 50% sometimes). 
// When we attempted to replicate the structure for DMA reads, HPIPE ended hanging, with no appreciable advantage in throughput ins any way. 
// For that reason, DMA reads are done one by one, without use of the DREQ signal to throttle the communication

//0 = regular clock (300 MHz) , 1 = 2x clock for timing estimate (600 MHz), 2 = PCIe clock for Signal Tap debug
logic [15:0] image_counter_write, image_counter_read;
parameter CLOCK_MODE = 0;

//Status reg is just the misc reg read value when the address is 0 (which it will be by defauly if we read the register from the driver)
wire [31:0] misc_reg_read_val;
assign hpipe_status_reg = misc_reg_addr_delay[1] == 4'd9 ? {image_counter_read, misc_reg_read_val[15:0]} : {image_counter_read, 16'd0};

//Parameters for computing number of output lines. Currently output sizes of 8-16 bits are supported
//These are computed by multiplying number of output classes (1001) by the bit size and then calculating the number of 256-bit reads needed (ceil function needs to be used)
localparam OUTPUT_LINES_8_BIT = 32;
localparam OUTPUT_LINES_10_BIT = 40;
localparam OUTPUT_LINES_12_BIT = 47;
localparam OUTPUT_LINES_14_BIT = 55;
localparam OUTPUT_LINES_16_BIT = 63;

//Need to read 9 extra values to hide the read latency since apparently there is no other way to implement a read latency with Gidel's IPs
localparam EXTRA_CYCLES = 9;

//Need to read the number of output bits from HPIPE and store it in a register
//To do this we just have a shift register for the read since there is a read delay of 1 cycle, we maintain it high for more since HPIPE is in different clock domain
reg [31:0] hpipe_num_output_bits;
reg [2:0] read_output_bits;
reg [1:0] sel_hpipe_misc_regs_rd_delay;
wire [3:0] alt_misc_reg_addr;
assign alt_misc_reg_addr = 4'd3;
reg [1:0][3:0]  misc_reg_addr_delay;
always_ff @(posedge lclk) begin
	if (!clrn) begin
		hpipe_num_output_bits <= 32'd0;
		read_output_bits <= 3'b001;
		sel_hpipe_misc_regs_rd_delay <= 2'd0;
		misc_reg_addr_delay[0] <= 4'd0;
		misc_reg_addr_delay[1] <= 4'd0;
	end else begin
		read_output_bits <= read_output_bits << 1;
		hpipe_num_output_bits <= (read_output_bits == 3'b100) ? misc_reg_read_val : hpipe_num_output_bits;
		sel_hpipe_misc_regs_rd_delay <= {sel_hpipe_misc_regs_rd_delay[0], sel_hpipe_misc_regs_rd};
		misc_reg_addr_delay[0] <= misc_reg_addr;
		misc_reg_addr_delay[1] <= misc_reg_addr_delay[0];
	end
end

//Select number of output lines for read counter
reg [7:0] max_output_mem_addr;
always_comb begin
	case(hpipe_num_output_bits) 
		32'd8 : max_output_mem_addr = OUTPUT_LINES_8_BIT-1;
		32'd10 : max_output_mem_addr = OUTPUT_LINES_10_BIT-1;
		32'd12 : max_output_mem_addr = OUTPUT_LINES_12_BIT-1;
		32'd14 : max_output_mem_addr = OUTPUT_LINES_14_BIT-1;
		32'd16 : max_output_mem_addr = OUTPUT_LINES_16_BIT-1;
		default : max_output_mem_addr = OUTPUT_LINES_8_BIT-1; //Ideally we should have some sort of error message in the SW driver because the resulting behaviour will be wrong for other values
	endcase
end

reg [19:0] input_mem_addr;

assign mem_ready_rd = (sel_hpipe_misc_regs_rd && !sel_hpipe_misc_regs_rd_delay[1]) ? 1'b0 : 1'b1; //Should always be 1 for DMA, otherwise we can have a delay
assign mem_ready_wr = 1'b1;

logic wr_dreq, rd_dreq;
//DMA channel 0 is write and channel 1 is read
//Note that writes have a 3 cycle delay before stopping after dreq=1 and reads have 4 cycles
assign user_dreq = {31'd0, wr_dreq};


//FSM for requesting new images in stream
        enum int unsigned
{
        S_DMA_WR_IDLE, 
        S_DMA_WR_WRITE,
		  S_DMA_WR_WAIT,
        S_DMA_WR_PAUSE,
		  S_DMA_WR_DONE
} dma_wr_state, dma_wr_nextstate;

// Clocked always block for making state registers
always_ff @ (posedge lclk) begin
        if (!clrn) dma_wr_state <= S_DMA_WR_IDLE;
        else dma_wr_state <= dma_wr_nextstate;
end

logic reset_input_counter;
always_comb begin
        // Set default values for signals here
        dma_wr_nextstate = dma_wr_state;
			wr_dreq = 1'b0;
			reset_input_counter = 1'b0;
        case (dma_wr_state)
                S_DMA_WR_IDLE: begin
								reset_input_counter = !sel_hpipe_input_mem;
                        if (sel_hpipe_input_mem) dma_wr_nextstate = S_DMA_WR_WRITE;
                end
                S_DMA_WR_WRITE: begin
						  //Dreq 3 cycles before for writes
                    if (input_mem_addr == (20'd4703 - 3)) dma_wr_nextstate = S_DMA_WR_WAIT;
                end
					 S_DMA_WR_WAIT: begin
						 //Wait for the DMA to finish (i.e. for the 3 cycle delay)
	               wr_dreq = 1'b1;
						if (input_mem_addr == (20'd4703)) dma_wr_nextstate = (hpipe_status_reg & 32'd1) ? S_DMA_WR_DONE : S_DMA_WR_PAUSE;					 
					 end
                S_DMA_WR_PAUSE: begin
						//Pause until HPIPE can take in another input
                  wr_dreq = 1'b1;
						if (hpipe_status_reg & 32'd1) dma_wr_nextstate = S_DMA_WR_DONE;
                end
					 S_DMA_WR_DONE: begin
						wr_dreq = 1'b1;
						reset_input_counter = 1'b1;
						dma_wr_nextstate = S_DMA_WR_IDLE;
					 end
        endcase
end





logic [3:0] misc_reg_addr;

//Logic for selecing the misc register address
//Note that the top level module will sometimes need to override the address
//Otherwise it should default to 0 if not using one of the read or write selects so that the status register can be properly read
always_comb begin
if (read_output_bits != 0) begin	
	misc_reg_addr = alt_misc_reg_addr;
end else begin
	if (sel_hpipe_misc_regs_wr) begin
		misc_reg_addr = addr_wr[5:2]; //lower bits of address aren't used since it's byte addressable and we do 4-byte (32 bit) writes/reads
	end else if (sel_hpipe_misc_regs_rd) begin
		misc_reg_addr = addr_rd[5:2];
	end else begin
		misc_reg_addr = 4'd9; //Status register is 9
	end
end
end


wire [255:0] pcie_output_data;
assign data_mem_rd = sel_hpipe_misc_regs_rd_delay[1] ? {224'd0, misc_reg_read_val} : pcie_output_data;

//Generate addresses for hpipe input and output mem because apparently the driver doesn't generating addresses past 16

reg [31:0] last_read_addr, last_write_addr;
reg [7:0] output_mem_addr;

always_ff @(posedge lclk) begin
	if(!clrn) begin
		input_mem_addr <= 20'd0;
		output_mem_addr <= 6'd0;
		last_read_addr <= 32'd0;
		last_write_addr <= 32'd0;
	end
	else begin
		if (reset_input_counter) begin
			input_mem_addr <= 20'd0;
			last_write_addr <= 32'd0;
		end else if(sel_hpipe_input_mem && addr_wr != last_write_addr) begin
			//Have to save last read/write addresses just in case there are duplicate writes to the same address
			last_write_addr <= addr_wr;
			input_mem_addr <= (input_mem_addr == 20'd4703)? 20'd0 : input_mem_addr + 20'd1;
		end else begin
			input_mem_addr <= input_mem_addr;
		end
		
		if(sel_hpipe_output_mem) begin
			last_read_addr <= addr_rd; //Don't need this anymore
			output_mem_addr <= (output_mem_addr == (max_output_mem_addr + EXTRA_CYCLES))? 8'd0 : output_mem_addr + 8'd1;
		end else begin 
			output_mem_addr <= output_mem_addr; 
		end
	end

end

logic sys_clock;
always_comb begin
	case(CLOCK_MODE)
		1 : sys_clock = fast_clock_2x;
		2 : sys_clock = lclk;
		default: sys_clock = fast_clock;
	endcase
end

   hpipe_0 hpipe_0_i(
         .system_builder_component_0_reset(!clrn), //Resets in HPIPE are active high so we need to invert
         .system_builder_component_0_addr(input_mem_addr), 
         .system_builder_component_0_data(data_mem_wr),
         .system_builder_component_0_valid(1'b1), //For the Gidel PCIe IP there is only a chipselect signal so we can tie this to 1
         .system_builder_component_0_chipselect(sel_hpipe_input_mem),
         .system_builder_component_0_addr2(output_mem_addr), 
         .system_builder_component_0_valid2(1'b1),
         .system_builder_component_0_chipselect2(sel_hpipe_output_mem),
         .system_builder_component_0_addr3(misc_reg_addr),
         .system_builder_component_0_valid3(1'b1),
         .system_builder_component_0_chipselect3(sel_hpipe_misc_regs_wr || sel_hpipe_misc_regs_rd),
			//Regular clock
         .sys_clock(sys_clock),
         .io_clock(lclk),
         .pcie_output_data(pcie_output_data),
         .status_register_pcie_read(misc_reg_read_val)
		//	.debug_read_registers(debug_read_registers[0:7]),
		//	.debug_write_registers(debug_write_registers)
   );
	
	logic [31:0] current_cycle_counter;
	logic [31:0] greatest_cycle_counter;
	logic [31:0] second_greatest_cycle_counter;
	logic [63:0] average_cycle_counter; //For 4096 images so we can right shift by 12
	logic [63:0] total_cycle_counter;
	
	logic large_cycle_count;
	logic enable_debug_regs;
	
	//Keeps 
	logic [15:0] next_image_counter_read;
	assign next_image_counter_read = image_counter_read + 16'b1;
	always_ff @(posedge lclk) begin
		if(!clrn) begin
			image_counter_read <= 16'd0;
		end else begin
			//Increment if we have read the last output line. Note that this value is artificially limited to 50,000
			if(sel_hpipe_output_mem && output_mem_addr == (max_output_mem_addr + EXTRA_CYCLES)) image_counter_read <= (next_image_counter_read == 16'd50001) ? 0 : next_image_counter_read;
		end
	end
	
	

	
	
	always_ff @(posedge lclk) begin
		if(!clrn) begin
			image_counter_write <= 16'd0;
			
			greatest_cycle_counter <= 32'd0;
			current_cycle_counter <= 32'd0;
			second_greatest_cycle_counter <= 32'd0;
			average_cycle_counter <= 64'd0;
			total_cycle_counter <= 64'd0;
			large_cycle_count <= 1'b0;
			enable_debug_regs <= 0;
		end
		else begin
			if (enable_debug_regs) begin
				if (sel_hpipe_input_mem && addr_wr != last_write_addr && input_mem_addr == 20'd4703) begin
					image_counter_write <=  image_counter_write + 1'b1;
					current_cycle_counter <= 32'd0;
					greatest_cycle_counter <= (current_cycle_counter > greatest_cycle_counter) ? current_cycle_counter : greatest_cycle_counter;
					second_greatest_cycle_counter <= (current_cycle_counter > greatest_cycle_counter) ? greatest_cycle_counter : ((current_cycle_counter > second_greatest_cycle_counter) ? current_cycle_counter : second_greatest_cycle_counter);
					average_cycle_counter <= (image_counter_write == 4096) ? average_cycle_counter >> 12 : average_cycle_counter + current_cycle_counter;		
				end
				else begin
					image_counter_write <= image_counter_write;
					current_cycle_counter <= current_cycle_counter + 1'b1;
				end
				total_cycle_counter <= total_cycle_counter + 1'b1;
				large_cycle_count <= 1'b0;
				
			end		
			enable_debug_regs <= sel_hpipe_input_mem ? 1'b1 : enable_debug_regs;
		
		end
	end
	
	
	//Debug registers
assign debug_read_registers[0] = {image_counter_read, image_counter_write};
assign debug_read_registers[1] = greatest_cycle_counter;
assign debug_read_registers[2] = second_greatest_cycle_counter;
assign debug_read_registers[3] = average_cycle_counter[31:0];
assign debug_read_registers[4] = total_cycle_counter[31:0];
assign debug_read_registers[5] = total_cycle_counter[63:32];
assign debug_read_registers[6] = large_cycle_count;
assign debug_read_registers[7] = 32'd0;

endmodule
