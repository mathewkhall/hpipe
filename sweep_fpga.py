#This is a script that runs the partitioner of HPIPE with different FPGA targets

import sys
import os
import time
import json
#########################################################################################################

#WHEN GENERATING RESULTS (DUMP=0 ) MAKE SURE YOU ADD THE U_SHAPE_MOVE

#########################################################################################################
#########################################################################################################

mx_dsp = 3960
mx_mem = 6847


gx_dsp = 5760
gx_mem = 11721


dump_numbers = 0 #If 1 run to store intermediate nodes' DSPs and M20Ks an

if dump_numbers == 1:
	partition_ready = 0
else:
	partition_ready = 1
mx_gx = 1   # 0 for MX and 1 for GX
folder_path = '/intermediate_files_oct_20/'


if mx_gx:
	chip_dsp = 5760
	chip_mem = 11721
	extra_path = 'GX'
else:
	chip_dsp = 3960
	chip_mem = 6847
	extra_path = 'MX'


intermediate_files_path = os.getcwd()+ folder_path + extra_path
intermediate_results_path = os.getcwd()+ folder_path +'results/'+ extra_path
current_dirr = os.getcwd()
FPGAs = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]
percentage = [1,0.95,0.9,0.85,0.8,0.75,0.7,0.65,0.6,0.55,0.5]
net_name = open('network_name.txt')
algorithms = ['u_shape_move']
planner_type = 'direct'# Can be ['direct','constrainted']
network_name = net_name.readlines()[0]
a = time.time()
num_chips = 0
target_dsp = 0

dsp_max = 0.9
mem_max = 0.9
net_name = open('network_name.txt')
network_name = net_name.readlines()[0]

#Generate the json file


default_params = {
	"//":"This is not a key but an indication that this text is just a comment",
	
	"number_of_fpgas":4,

	"//":"The percent specifies the maximum allowed DSPs and M20Ks to be placed on a each chip. Individual percentages overwrite these ",
	"target_dsp_percent":0.9,
	"target_m20k_percent":0.95,
	"dsp_percentage":1,
	
	"//":"Optional - Maximum operating freuqency on the FPGAs (MHz)",
	"fmax":400, 

	"number_of_switches":0,
	


	"//":"The Algorithm specifies which algorithm should be used. Can be [naive,u_shape,u_shape_move]",
	"algorithm": "u_shape_move",

	"//" : "The types of connectivity available are [point_to_point,star,2d_torus]",
	"connectivity":"point_to_point",

	"hops":[
		{
			"id":0,
			"//":"Type could be f-->fpga or s-->switch",
			"type":"f",
			"dsps":3960,
			"m20k":6847,
			"//":"The percent specifies the maximum allowed DSPs and M20Ks to be placed on this chip",
			"target_dsp_percent":0.9,
			"target_m20k_percent":0.95
			
		},
		{
			"id":1,
			"type":"f",
			"dsps":3960,
			"m20k":6847,
			"target_dsp_percent":0.9,
			"target_m20k_percent":0.95
		},
		{
			"id":2,
			"type":"f",
			"dsps":3960,
			"m20k":6847,
			"target_dsp_percent":0.9,
			"target_m20k_percent":0.95
		},
		{
			"id":3,
			"type":"f",
			"dsps":3960,
			"m20k":6847,
			"target_dsp_percent":0.9,
			"target_m20k_percent":0.95
		}
	],

	"links":[
		{
			"id":0,
			"//":"Type could be f-f , f-s , s-s",
			"type":"f-f", 
			"source":0,
			"destination":1,
			"//":"Bandwidth is in units of Gbit/s",
			"bandwidth":100
		},
		{
			"id":1,
			"type":"f-f",
			"source":1,
			"destination":2,
			"bandwidth":100
		},
		{
			"id":2,
			"type":"f-f",
			"source":2,
			"destination":3,
			"bandwidth":100
		}
	],

	"//":"Optional-For the output files that contain the results, specify a name to appear in the result files.",
	"//":"For example, if you specify filename = my_network you will have 4 output files that have my_network_ in them",
	"filename":network_name,

	"intermediate_graph" : "null",
	"dump_numbers_file_path": "null",
	"dump_numbers" : dump_numbers
}


for indexx,l in enumerate(algorithms):
	if dump_numbers == 1 :
		if indexx == 1 or indexx ==2:
			continue

	for j in percentage:
		dir_cur = intermediate_files_path+'/'+str(int(j*100))
		if dump_numbers == 1:
			os.system('mkdir '+ dir_cur)
		if dump_numbers == 0:
			f11 = open(default_params["filename"]+"_partition.txt",'a')
			f11.write("Running with percentage: " + str(j) +  ': =============================================================\n')
			f11.close()
			f11 = open(default_params["filename"]+"_partition_log.txt" ,'a')
			f11.write("Running with percentage: " + str(j) +  ' :=============================================================\n')
			f11.close()
			f11 = open(default_params["filename"]+"_partition_log_summary.txt",'a') 
			f11.write("Running with percentage: " + str(j) +  ': =============================================================\n')
			f11.close()	
			if j != 1 :
				f11 = open(default_params["filename"]+"_summary_results.txt",'a') 
				f11.write('\n')
				f11.close()
		for i in range(len(FPGAs)):
			
			if dump_numbers == 1:
				file_for_dump = dir_cur+'/_'+str(FPGAs[i])+'.txt'
				os.system('rm ' + file_for_dump)
				os.system('touch ' + file_for_dump)
				default_params["dump_numbers_file_path"] = file_for_dump
			else:
				fh_current_in = dir_cur + '/_' +str(FPGAs[i])+'.txt'	

			if dump_numbers == 0:
				f11 = open(default_params["filename"]+"_partition.txt",'a')
				f11.write("Running with chips: " + str(FPGAs[i]) +  ': =============================================================\n')
				f11.close()
				f11 = open(default_params["filename"]+"_partition_log.txt" ,'a')
				f11.write("Running with chips: " + str(FPGAs[i]) +  ' :=============================================================\n')
				f11.close()
				f11 = open(default_params["filename"]+"_partition_log_summary.txt",'a') 
				f11.write("Running with chips: " + str(FPGAs[i]) +  ': =============================================================\n')


			target_dsp = int(FPGAs[i] * chip_dsp * j * dsp_max)
			max_chip_mem = int(chip_mem * mem_max)
			max_chip_dsp =  int(chip_dsp * dsp_max)
			num_chips = FPGAs[i]
			os.system('rm default_precisions.annotations')
			os.system('echo Running with: ' + str(num_chips) + ' =============================================================')
			dsp_target = int(FPGAs[i] * j * chip_dsp * dsp_max)
			default_params["number_of_fpgas"] = num_chips
			default_params["target_dsp_percent"] = dsp_max
			default_params["target_m20k_percent"] = mem_max 
			if dump_numbers == 0:
				default_params["intermediate_graph"] = fh_current_in
			default_params["hops"] = []
			default_params["algorithm"] = l
			default_params["dsp_percentage"] = j 
			for ii in range(num_chips):
				node = {}
				node["id"] = ii
				node["type"] = "f"
				node["dsps"]  = chip_dsp
				node["m20k"] = chip_mem
				node["target_dsp_percent"]= dsp_max
				node["target_m20k_percent"] = mem_max
				default_params["hops"].append(node)


			#For point to point I will just connect every two consecitive FPGAs
			default_params["links"] = []
			for ii in range(num_chips-1):
				link = {}
				link["id"] = ii
				link["type"] = "f-f"
				link["source"]  = ii
				link["destination"] = ii+1
				link["bandwidth"]= 100
				default_params["links"].append(link)

			params_file = 'partition_params.json'
			with open(params_file, "w") as fh:
				json.dump(default_params, fh, indent=4)

			os.system('python3 -m hpipe.LayerComponents --param_file_path quartus_params.json  --partitioning=True ' +  ' --dsp_target '+ str(target_dsp) + ' --partition_ready='+str(partition_ready))

	if dump_numbers == 0:
		dir_for_copy = intermediate_results_path + '/ungroup/' + l + '/' 
		os.system('cp ' + default_params["filename"]+"_partition.txt " + dir_for_copy)
		os.system('cp ' + default_params["filename"]+"_partition_log.txt " + dir_for_copy)
		os.system('cp ' + default_params["filename"]+"_partition_log_summary.txt " + dir_for_copy)
		os.system('cp ' + default_params["filename"]+"_summary_results.txt " + dir_for_copy)

		os.system('rm ' + default_params["filename"]+"_partition.txt " )
		os.system('rm ' + default_params["filename"]+"_partition_log.txt ")
		os.system('rm ' + default_params["filename"]+"_partition_log_summary.txt " )
		os.system('rm ' + default_params["filename"]+"_summary_results.txt " )
print("Whole run took: " + str(time.time() - a))