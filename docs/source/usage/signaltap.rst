.. _signaltap:
Debugging The FPGA With Signal Tap
========================================

This guide will explain how to debug the FPGA design with Signal Tap if there are issues on the FPGA that do not come up while simulating. Note that you can right click and open images in a new tab if you need to zoom in.

Project Setup
----------------------------
Before you can set up Signal Tap, you must generate a HPIPE circuit and move it over to PChenry or Wintermute where the HPIPE projects are. Both the regular HPIPE project or the NX HPIPE project can be used. For example, you can copy the NX project from ``/scratch/shared/stanmari/hpipe_nx_project`` to your own scratch directory and then place your ``quartus_circuit`` in the same directory (not in the project directory). You can then open ``hpipe_nx_project/hpipe.qpf`` in Quartus and the project should be ready to use.

You will then need to run "Analysis and Synthesis" in Quartus (or a full compile), otherwise Signal Tap will not be able to find any of your ports or registers.

Adding Signals To Signal Tap
----------------------------
Once Synthesis is finished you can open Signal Tap by going to ``Tools -> Signal Tap Logic Analyzer``. If this is the first time setting up Signal Tap for the given project you can select ``default`` and click create. Now that the Signal Tap window is open, we can add some signals. Double click in the middle where it says: ``"Double-click to add nodes"``. In the window that comes up, you can specify the name of the signals you are looking for. For now we will leave it as a wildcard (*) to display all signals. For the Filter we can leave it as ``"Signal Tap: pre-synthesis"`` as it shows the signals similar to the names we have in our Verilog files. Next click on the "..." beside the ``Look in`` box and navigate down to the module you want to add signals from. For example in the screenshot below we are adding signals from the first conv module:

.. image:: /_static/Signal_Tap_Empty.png
	:alt: Empty Signal Tap Window

Once you have selected the module you want click "OK". Make sure to unselect ``Include subentities`` if you are using a wildcard for the name, otherwise you will get thousands of signals. Now click "Search" and you should have some signals pop up in the ``Matching Nodes`` window. Expand the hierarchy and add the signals you want using the ``>`` button, similar to the following screenshot:

.. image:: /_static/Signal_Tap_Added_Nodes.png
	:alt: A couple of added nodes in signal tap

Once finished you can click ``Insert`` and then ``Close`` and your signals should be added to the window. Now you need to select one or more signals to be the trigger signal. This is the signal that will tell Signal Tap to save all the other signal values to memory. Since we have a limited amount of memory, Signal Tap will continuously record all signal values into memory until the trigger signal is activated. After that, it will save a window of values into memory for you to view (a bit before the trigger and a lot after). By default all signals are selected as a ``Trigger Enable``. We want to deselect everything but the one we want. In the example below we want to monitor the convolution inputs/outputs so we set our trigger signal as the valid signal from the placeholder module (i.e. the module that feeds values into the 1st conv module). For the ``Trigger Condition`` we select Basic AND and then type a 1 in the box. Note that for some single bit signals you will have to right click on the box instead and select ``High`` or ``Low`` (or one of the rising edges). 

.. image:: /_static/Signal_Tap_Trigger_Signal.png
	:alt: Output Valid selected as our trigger signal

Next we need to add our reference clock. Click on the "..." beside ``Clock`` on the pane to the right of where all the signals are. In here search for ``*clock*`` in the hpipe top level module, select a single bit (important) of the ``sys_clock`` clock, add it and click ``OK``. See the screenshot below for reference.

.. image:: /_static/Signal_Tap_Adding_Clock.png
	:alt: Adding our sys_clock for signal_tap

Once you have added the clock you can change the ``Sample depth`` below that. The bigger the sample depth the longer your window will be for viewing your stored signals. This comes at a cost of more memory and lower clock frequency for when you have to run Signal Tap. Usually we use something 16K or bigger. The rest of the settings are usually left the same as the default (in particular continuous under ``Storage qualifier``), but feel free to play around with them if you wish. From here save signal tap; it will ask you if you want to add it to the project and you should click yes. Then you should recompile the entire project.

Running Signal Tap on the FPGA
----------------------------
Once you have finished compiling the project, transfer over both ``hpipe.sof`` (programming file) and the ``.stp`` (Signal Tap) file to whatever machine has the FPGA. From here open Quartus and then Signal Tap (using sudo if on one of the Savi machines). In Signal tap you should see the same signals as you had before, and the JTAG Chain Configuration should display your Hardware (USB Blaster I or USB Blaster II) and device (code for the board you are using). See an Example below:

.. image:: /_static/Signal_Tap_Ready.png
	:alt: Adding our sys_clock for signal_tap

If this information looks incorrect, you can try checking the JTAG connection in the terminal by running ``jtagconfig``. If nothing is displayed try power cycling the machine and double check you are sudo and have sourced the Quartus setup file after switching to sudo. 

If the information is correct, you can click the button beside where it says ``Instance Manager`` to start running Signal Tap. Then run the hpipe driver so the design can go through some images and the trigger can occur. After the trigger occurs (the write from the placeholder layer in our case) you should get a waveform window similar to the following:

.. image:: /_static/Signal_Tap_Waveform_Zoomed_Out.png
	:alt: Adding our sys_clock for signal_ta

From here you can zoom by left-clicking and zoom out by right-clicking. You can also change the display format of any signal (default is hex) by right-clicking on it, going to ``Bus Display Format``, and choosing the one you want. In the example image below we zoom in and change the placeholder output values (input to first convolution) to signed 2's complement. The top bar represents clock cycles for whatever clock you told Signal Tap to use as the reference. You will notice the values go negative; as mentioned before, we store some values before the trigger condition as well.

.. image:: /_static/Signal_Tap_Waveform_Zoomed_In.png
	:alt: Adding our sys_clock for signal_ta