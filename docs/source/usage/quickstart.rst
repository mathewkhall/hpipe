.. _quickstart:
Quick Start Guide
=============================================

This guide should enable a new user to gain some familiarity with HPIPE by taking an optimized neural
network model from TensorFlow and use the HPIPE generator to build hardware implementing that neural network.
Then take the generated hardware, run it through Quartus, generate a bitstream, program an FPGA, run
the ImageNet validation set of 50000 images through the hardware, then compare the accuracy from the
hardware with the TensorFlow model.

.. highlight:: sh
Installing Required Packages
-----------------------------------------
	Virtualenv
		First, I would recommend installing ``virtualenv`` and creating an environment specifically for HPIPE. For example,
		::
			virtualenv -p python3 ~/hpipe_virtualenv

		This will enable you to keep the versions of the packages required by HPIPE separate from the package versions you
		install for other python projects.  You can activate the environment with:
		::
			. ~/hpipe_virtualenv/bin/activate

		And deactivate it while it's active by running ``deactivate``

	Installing Dependencies
		I have started to track the dependencies of HPIPE in ``setup.py``.  You can easily install these dependencies by
		running:
		::
			python3 setup.py egg_info
			pip3 install -r *.egg-info/requires.txt

		Note: HPIPE supports Tensorflow 2.3
		
		This list may not always be perfect or up to date, so if you run into errors like
		::
			ModuleNotFoundError: No module named "<some_module_name>"

		You should be able to fix these by running
		::
			pip3 install <some_module_name>

Accessing Machines with Enough RAM to Build HPIPE, and Machines with DE10 Boards
-----------------------------------------
	``pchenry`` and ``wintermute`` both have enough RAM to build HPIPE in Quartus.  You can read the Computer Setup page
	on Vaugn's wiki to learn how to access these.  All quartus builds should be done on whichever of these two machines
	is either lightly loaded or has no other users.

	There are DE10-pro boards in SAVI machines 2 and 5.  Again, look at the Computer Setup page to find out how to get
	access to these.

Getting Required Files
-----------------------------------------
	For this quick start guide there are a few files/directories you will need to copy. These files available on ``pchenry`` and ``wintermute``:

	1. ``/scratch/shared/hpipe/resnet50_85p_sparse.pb``
		An optimized/pruned resnet50 model you can copy to your own machine to use as input to HPIPE


		Note: HPIPE uses "Frozen Graphs" (.pb files) as inputs.  You can find more about how to obtain/generate Frozen Graphs at https://docs.google.com/document/d/1hQN1om2P6MPH4s-ST-X9gl_RgGGSb6QMDrqB9iHmBpo/edit?usp=sharing  (You will find some reference models that are verified and working with HPIPE) 

	2. ``/scratch/shared/hpipe/quartus_project``
		A quartus project that has the PCIe controller and a few other things, and instantiates an HPIPE instance.
		You should copy this to your own directory in ``/scratch/shared/<your_username>`` on ``wintermute``

	3. ``/scratch/shared/hpipe/imagenet_test_binaries``
		A collection of binary files you should transfer to the SAVI machine you are using to use as input
		to the generated accelerator

	4. ``/scratch/shared/hpipe/PCIE_SW_KIT``
		You will need to transfer this to the SAVI machine you are using.  It contains some slightly modified code and the PCIe driver
		from Terasic for interfacing with the accelerator.

	5. ``/scratch/shared/hpipe/imagenet_test_binaries/classes.bin``
		You will need to copy this file to your own machine to check the accuracy once you have run the ImageNet validation set through
		the accelerator.

Generating an Accelerator
-----------------------------------------
	HPIPE requires a TensorFlow pb file, a precision annotations file (specifies the precision of weights and activations for each layer),
	an example input file, and a number of other parameters that specify things like input transformations, device resources, etc.  Rather than specify
	all of these on the command line, HPIPE takes a single argument (``--param_file_path <path/to/json/config/file.json>``) that specifies the path to a
	``JSON`` config file.  If one is not specified, it uses a default of ``params_file.json``.  If the specified params file does not exist, HPIPE creates one
	initialized with default values.  You can then open that file and modify the parameters.

	.. highlight:: javascript
	The following is an example ``JSON`` config file used for the ResNet-50 model in this tutorial.
	::
		{
		    "build_data_path": true,
		    "build_from": null,
		    "build_until": null,
		    "debug_individual_stage": null,
		    "dsp_target": 5000,
		    "generated_circuit_dir": "generated_files/circuits/quartus_circuit",
		    "input_adjustments": {
		        "bit_spec": {
		            "frac": 0,
		            "int": 7,
		            "sign": 1
		        },
		        "channel_biases": [
		            -123.68,
		            -116.78,
		            -103.94
		        ],
		        "post_scale": 1.0,
		        "pre_scale": 255.0
		    },
		    "input_bit_spec": {
		        "frac": 8,
		        "int": 0,
		        "sign": 0
		    },
		    "instantiate_quartus_ip": true,
		    "mem_target": null,
		    "op_precision_annotations_path": "op_precisions.annotations",
		    "output_names": [
		        "import/resnet_model/final_dense"
		    ],
		    "pb_path": "resnet50_85p_sparse.pb",
		    "quartus": true,
		    "sample_image": "individualImage.png",
		    "top_level_module_type": "quartus"
		}

	You can ignore most of this, but you may need to change the ``"pb_path"`` field to the correct path to point to the resnet50_85p_sparse.pb file you
	should have copied from wintermute (if you haven't done this, please read the Getting Required Files section above).

	A few interesting options in here, if you are interested:

		``dsp_target``
			This is currently the only parameter that controls the amount of device resources the tool should target.  The DE10-pro we are targeting
			has a Stratix 10 2800 that has about 5700 DSPs, so we have told the HPIPE generator to try to use 5000 of those.

		``input_adjustments``
			Typically neural networks will be trained from data that has had biases removed from it (since biases take a long time to learn).
			The transformations required to remove biases are not typically included in a TensorFlow graph, so we specify them in this config file.
			The input data will first be multiplied by ``pre_scale``, added to ``channel_biases``, then multiplied by ``post_scale``.

		``output_names``
			A list of output node names from the input TensorFlow graph.  These should all have ``import/`` added as a prefix to whatever the
			name was when you trained the network.

		``top_level_module_type``
			``quartus``
				This allows you to specify that you want a top level module designed for integration into a quartus project.  It supports the
				``build_until`` option for debugging.
			``tb``
				This is the original testbench top level module for use with VCS.  It supports ``build_from`` and ``debug_individual_stage``.
			``tbV2``
				This is the new testbench top level module for use with VCS and Verilator.  It supports only ``build_until``.

		``generated_circuit_dir``
			This is the output directory for the generated circuit.

		``precision_annotations_file``
			This specifies the path to the precision annotations file.  If the specified file does not exist, a default one will be created
			and the ``sample_image`` will be used to determine the precision for all weights and activations. Using the autocomputed values
			will likely hurt the accuracy at least a few percent, but it works surprisingly well.

	.. highlight:: csv
	The precision annotations file I have been using with the resnet-50 model for this tutorial is below.  You should copy it into a file named 
	``op_precisions.annotations``:
	::
		node_name,scale,sign_bits,exponent,int,f,a_sign_bits,a_exponent,a_int,a_f
		import/Placeholder,1.,1,0,4,11,1,0,8,0
		import/resnet_model/conv2d/Conv2D,1.,1,0,0,15,1,0,4,11
		import/resnet_model/max_pooling2d/MaxPool,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization/FusedBatchNorm,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_1/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_2/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_1/FusedBatchNorm,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_1,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_3/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_2/FusedBatchNorm,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_2,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_4/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/add,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_3/FusedBatchNorm_bias,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_3_split_by_bn,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_5/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_4/FusedBatchNorm,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_4,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_6/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_5/FusedBatchNorm,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_5,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_7/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/add_1,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_6/FusedBatchNorm_bias,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_6_split_by_bn,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_8/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_7/FusedBatchNorm,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_7,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_9/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_8/FusedBatchNorm,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_8,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_10/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/add_2,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_9/FusedBatchNorm_bias,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_9_split_by_bn,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_12/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_11/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_10/FusedBatchNorm,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_10,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_13/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_11/FusedBatchNorm,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_11,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_14/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/add_3,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_12/FusedBatchNorm_bias,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_12_split_by_bn,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_15/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_13/FusedBatchNorm,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_13,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_16/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_14/FusedBatchNorm,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_14,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_17/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/add_4,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_15/FusedBatchNorm_bias,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_15_split_by_bn,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_18/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_16/FusedBatchNorm,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_16,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_19/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_17/FusedBatchNorm,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_17,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_20/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/add_5,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_18/FusedBatchNorm_bias,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_18_split_by_bn,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_21/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_19/FusedBatchNorm,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_19,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_22/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_20/FusedBatchNorm,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_20,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_23/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/add_6,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_21/FusedBatchNorm_bias,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_21_split_by_bn,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_25/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_24/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_22/FusedBatchNorm,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_22,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_26/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_23/FusedBatchNorm,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_23,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_27/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/add_7,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_24/FusedBatchNorm_bias,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_24_split_by_bn,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_28/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_25/FusedBatchNorm,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_25,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_29/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_26/FusedBatchNorm,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_26,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_30/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/add_8,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_27/FusedBatchNorm_bias,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_27_split_by_bn,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_31/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_28/FusedBatchNorm,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_28,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_32/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_29/FusedBatchNorm,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_29,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_33/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/add_9,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_30/FusedBatchNorm_bias,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_30_split_by_bn,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_34/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_31/FusedBatchNorm,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_31,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_35/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_32/FusedBatchNorm,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_32,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_36/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/add_10,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_33/FusedBatchNorm_bias,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_33_split_by_bn,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_37/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_34/FusedBatchNorm,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_34,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_38/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_35/FusedBatchNorm,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_35,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_39/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/add_11,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_36/FusedBatchNorm_bias,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_36_split_by_bn,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_40/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_37/FusedBatchNorm,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_37,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_41/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_38/FusedBatchNorm,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_38,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_42/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/add_12,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_39/FusedBatchNorm_bias,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_39_split_by_bn,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_44/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_43/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_40/FusedBatchNorm,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_40,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_45/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_41/FusedBatchNorm,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_41,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_46/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/add_13,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_42/FusedBatchNorm_bias,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_42_split_by_bn,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_47/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_43/FusedBatchNorm,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_43,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_48/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_44/FusedBatchNorm,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_44,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_49/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/add_14,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_45/FusedBatchNorm_bias,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_45_split_by_bn,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_50/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_46/FusedBatchNorm,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_46,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_51/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_47/FusedBatchNorm,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_47,1.,1,0,4,11,1,0,4,11
		import/resnet_model/conv2d_52/Conv2D,1.,1,0,4,11,1,0,4,11
		import/resnet_model/add_15,1.,1,0,4,11,1,0,4,11
		import/resnet_model/batch_normalization_48/FusedBatchNorm_bias,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Relu_48_split_by_bn,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Mean,1.,1,0,4,11,1,0,4,11
		import/resnet_model/Reshape,1.,1,0,4,11,1,0,4,11
		import/resnet_model/dense/MatMul,1.,1,0,4,11,1,0,6,9
		import/resnet_model/dense/BiasAdd,1.,1,0,4,11,1,0,6,9
		import/resnet_model/final_dense,1.,1,0,4,11,1,0,6,9

	Now that you have prepared the inputs to HPIPE, run ``make quartus`` then ``make quartus_project_include_list`` and the HPIPE generator should generate a
	circuit for you in ``generated_files/circuits/quartus_circuit``.  ``tar`` this up and copy it to pchenry or wintermute
	and untar it in the same directory where you copied the quartus project (see Getting Required Files if you haven't done this).

	Now you should be able to run the quartus build.  This step should be done overnight.

Running the Accelerator
-----------------------------------------
	Running in hardware is detailed in :doc:`another section <hardware>`.
