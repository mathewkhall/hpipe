.. _testing:
Testing code before Pull Requests
========================================
.. role:: red
.. role:: green
This guide will explain how to test code changes to HPIPE.


Automatic Checks
----------------------------
Whenever a push is submitted in the HPIPE repo, Gitlab runs a continuous integration (CI) script (<repo root>/.gitlab-ci.yml) which executes some unit tests for individual 
layers from a collection of neural networks on a SAVI machine at the University of Toronto.  These unit tests have a large array of configurations of HPIPE 
convolution modules and some activation functions, but there are some gaps (for example, the Placeholder module does not have a unit test, and it is responsible 
for processing input data before it is sent into the main HPIPE pipeline).  The unit tests cover functionality and check that the cycle latency is within 5% of 
the expected number, but they do not check the physical design of the circuit.  It is therefore necessary to manually run some additional tests before checking 
in new code, but which checks are necessary can depend on the type of code changes.

    Steps
        * Push to a branch on Gitlab and let the CI script run the tests
        or

        * Run the commands in the file ``/Unit_tests/serial_multiple_tests.py``

    :green:`Success indicator`
        * Look for the checkmark in the status column of the pipeline tab
        .. image:: /_static/success.png
	        :alt: Image of succesful CI tests
        or

        * Read the output of the tests in the command line window


    :red:`What to do in case of failure`
        * Identify the failing test as specified by Gitlab or in the command line window
        * Simulate the Unit tests using vcs or verilator and find source of error

End-to-End simulation of CNNs
--------------------------------
If you are making significant changes, it is usually a good idea to run simulations of full CNNs when changes are made to any of the TF graph optimizer stages, 
the planner, or any of the HPIPE layers. If you need them, a set of config files and TF graphs are 
available `on google drive <https://drive.google.com/drive/folders/1b5is_O9a0NJpPBYj7MpyDrSbfUW5LVKu>`_.

    Steps
        * To run a simulation of a full CNN, follow the steps in :doc:`Running Simulations <simulating>` to simulate using Verilator
        * Find the accuracy of the simulated circuit by running the script at ``scripts/compare_output_classes.py``

    :green:`Success indicator`
        * Top 1% and Top 5% accuracy are very close to those of the original network


    :red:`What to do in case of failure`
        * Run the script at :ref:`generate distribution graphs and images <distribution-and-image-label>` and make sure activations and the distributions look almost identical to those generated by tensorflow. Note that 
          looking at images of activations alone does not reveal biasing issues since each image's bias is cancelled out for visual purposes. Moreover, looking at distributions alone does not reveal
          misalignement issues. Looking at images and distribution of the activation together will decrease the chances of any oversight.
        * After identifying some potential source of error, simulate using VCS to look at the detailed waveforms to confirm or deny your suspicions

Succesful Compilation of Quartus build
----------------------------------------
Compiling the code in Quartus to make sure it compiles correctly is a necessary step. Note that builds can take a long time (12+ hours), and require a significant amount of memory (256GB RAM or more).

    Steps
        * :ref:`Compile <quickstart>` the code on Quartus after setting ``instantiate_quartus_ip=true`` in the ``.json`` configuration file when running HPIPE compiler

    :green:`Success indicator`
        * If compilation is succesful, a message will appear in the message window and the timing analyzer will be open with the clock timings


    :red:`What to do in case of failure`
        * If synthesis fails, look for differences in IP names or in Quartus versions
        * If the fitter fails, consider changing the seed 
        * If fitter repeatedly fails, consider reducing HPIPE's target resource counts, or reducing the max length of a cascade chain (``break_chain_every_n_weights`` in ``LayerComponents.py``) 
          for DSPs and vector mode, or varying OCP by changing ``arch.max_nx_tensor_chain_length`` in ``ArchLoader.py`` for tensor mode
        * If the obtained Fmax is too low, consider changing the seed
        * If the obtained Fmax is repeatedly too low, look at the critical paths and pipeline them as much as possible, and consider changing resource counts.

Running in Hardware
--------------------------
After Quartus compiles succesfully, the networks can be run in Hardware to check for accuracy and performance. 

    Steps
        * Look at the page on :doc:`Running in Hardware <hardware>`

    :green:`Success indicator`
        * A measured accuracy that is close to the predicted accuracy


    :red:`What to do in case of failure`
        * Refer to the :doc:`Signal Tap guide <signaltap>` in order to view the waveform of signals running in hardware.
