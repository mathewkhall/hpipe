hpipe package
=============

Submodules
----------

.. toctree::
   :maxdepth: 4

   hpipe.ArchLoader
   hpipe.GraphBuilder
   hpipe.LayerComponents
   hpipe.Partition
   hpipe.Plan
   hpipe.build_accelerator
   hpipe.perf_est

Module contents
---------------

.. automodule:: hpipe
   :members:
   :undoc-members:
   :show-inheritance:
