mkdir intermediate_files_oct_20

cd intermediate_files_oct_20

mkdir results
mkdir MX
mkdir GX
mkdir MIX


cd results
mkdir MX
mkdir GX
mkdir MIX

cd MX
mkdir naive
mkdir u_shape
mkdir u_shape_move

cd ..
cd GX
mkdir naive
mkdir u_shape
mkdir u_shape_move

cd ..
cd MIX
mkdir naive
mkdir u_shape
mkdir u_shape_move