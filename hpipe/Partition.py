#Partitioning Algorithm
import math
import numpy as np
from hpipe.GraphBuilder import Graph
from digitallogic.utils import clog2, get_padding
from multiprocessing import Process, Value
import os
import tensorflow as tf
import time
import digitallogic as dl
import json
#Turning on multithreading (actually multiprocessing in this case) may either increase or decrease the speed of calculating the first cost estimate depending on the design
ENABLE_MULTITHREADING = 1
# Higher CPU core counts degrade performance due to process creation overhead. Setting this to 1 will still introduce some processes creation overhead; it's better to just use the flag above
CORE_LIMIT = 4


class Partition():
	def __init__(self, graph, partition_param_file_path="partition_params.json",called_from_planner=False, **kwargs):
		print("  Partitioning")
		self.graph = graph
		params_handle = open(partition_param_file_path)
		partition_params = json.load(params_handle)
		network_name = partition_params["filename"]
		
		#exit(1)
		# Read the partition_params file and figure out the hardware setup and the partitioning algorithm and other info
		# Initialize the partition_estimate object for every node
		self.initialize_partitioning(partition_params)

		# Put nodes into groups based on which nodes share same group (nodes on a skip path will share same group)
		self.assign_initial_groups()

		# Preprocessing includes setting finish times for perf estimation and also marking nodes on the skip path
		# such that only the branch with higher number of conv layers is used for perf estimation
		self.preprocess_conv_layers()

		#  This function merges all the convs that are in the same group and reduces the partitioning problem to 
		# groups of layers instead of individual layers, since those groups have to be placed together on the same chip
		num_groups,conv_group_ids = self.reduce_convs_to_groups()

		# Now that the Convs are in groups, we merge the other nodes to the closest conv group. 
		# closest --> the next conv group for all the nodes except the last nodes where there is no conv after,
		# they get assigned to the previous conv group
		self.merge_other_nodes(num_groups,conv_group_ids)


		if(partition_params["dump_numbers"] == 1): #Store the intermediate representations
			fh_info = open(partition_params["dump_numbers_file_path"],'w')
			#Dump all nodes info to files
			def dump_node_info(n):
				nonlocal fh_info
				#Name, Type ,DSP,M20K,group_id,total_time,on_skip_path,initial_chip_id
				fh_info.write(n.tf_op.name +',' + n.type + ',' + str(n.planner_estimate.multiplier_count) + ',' + str(n.m20k_estimate) + ',' + str(n.partition_estimate.group_id)  + ',' + str(n.planner_estimate.time_weight) + ','+ str(n.partition_estimate.on_skip_path) + ',' +str(n.partition_estimate.chip_id) + '\n')
				
				return
			self.graph.walk_graph(f_node=dump_node_info)
			fh_info.close()
			return
		elif (partition_params["dump_numbers"] == 0):
			fh_info = open(partition_params["intermediate_graph"])
			#print(fh_info)
			nodes = fh_info.readlines()
			#print(len(nodes))
			#print(nodes)
			if len(nodes) == 0:
				fh = open(partition_params["filename"]+"_partition.txt",'a')
				fh.write("==============================================================================================================\n")
				fh.write("COULD NOT FIND A SOLUTION FOR RUNNING WITH " + str(self.num_chips) + ' chips \n')
				fh.write('DSP distribution:  \n')
				fh.write('MEM distribution:  \n')
				fh.write('Assigned Nodes Distribution:  \n')
				fh.write('Non-Assigned Nodes after Distribution:  \n')
				fh = open(partition_params["filename"]+"_partition.txt")
				lines = fh.readlines()

				f2 = open(partition_params["filename"]+"_summary_results.txt",'a')
				table = ''
				key = 0
				p = 0
				c = 0
				self.latency = -1
				self.throughput = -1
				for i in range(len(lines)):
					if lines[i].split(':')[0] == 'Running with percentage':
						p = float(lines[i].split(':')[1])
						#print('p',p)
					if lines[i].split(':')[0] == 'Running with chips':
						c =  int(lines[i].split(':')[1])
						#print('c',c)

					if p == partition_params["dsp_percentage"] and c == partition_params["number_of_fpgas"]:	
						table+= ('F(0-0-0),')
						i += 5
						break

				f2.write(table)
				f2.close()
				fh.close()
				return
			index = 0
			total = 0
			g = 0
			def read_info(n):
				nonlocal nodes
				nonlocal index
				n.m20k_estimate = int(nodes[index].split(',')[3])
				n.planner_estimate.multiplier_count = int(nodes[index].split(',')[2])
				n.planner_estimate.time_weight = int(float(nodes[index].split(',')[5]))
				n.planner_estimate.cycles_per_row = int(n.planner_estimate.time_weight/n.get_output_shape()[1])
				n.partition_estimate.multiplier_count = n.planner_estimate.multiplier_count
				#print(n.tf_op.name +',' + n.type + ',' + str(n.planner_estimate.multiplier_count) + ',' + str(n.m20k_estimate) + ',' + str(n.partition_estimate.group_id)  + ',' + str(n.planner_estimate.time_weight) + ','+ str(n.partition_estimate.on_skip_path) + ',' +str(n.partition_estimate.chip_id) + '\n')
				index +=1
			self.graph.walk_graph(f_node=read_info)
			#exit(1)
		# Create the final arrays that contain the DSP and Memory distribution of the various groups 
		# (groups contain all nodes not only convs) and those arrays will be used to determine th epartitioning
		self.construct_dsp_mem_arrays()
		print(self.algorithm)
		if called_from_planner:
			return


		self.partition_graph(use_U_shape = self.algorithm in ['u_shape','u_shape_move'], only_backward = False)
		
		#===============================================Iterative Algorithm=================================================
		if self.algorithm == 'u_shape_move'	:
			while True: #There is no valid move available
				#Run the partition loop
				
				if not(self.failure):
					print("Found a solution")
					break
				
				#Do a move (start greedy do all possible moves)
				else:
					def move_generator():
						No_move = False
						found_move = False
						for i in range(len(self.chips)):
							if found_move:
								break
							if(i != len(self.chips)-1):
								if((self.chips[i+1] != self.chips[i]) and (self.chips[i+1] != -1)):
									if (self.utilized_dsps[self.chips[i+1]]+self.final_dsps[i] <= self.chip_dsp[self.chips[i+1]]) and (self.utilized_mem[self.chips[i+1]]+self.final_mems[i] <= self.chip_mem[self.chips[i+1]]):
										found_move = True
										self.utilized_dsps[self.chips[i+1]] += self.final_dsps[i]
										self.utilized_mem[self.chips[i+1]] += self.final_mems[i]
										self.utilized_dsps[self.chips[i]] -= self.final_dsps[i]
										self.utilized_mem[self.chips[i]] -= self.final_mems[i]
										self.chips[i] = self.chips[i+1]
							else: #Last node is reached without finding a move
								No_move = True

						return No_move
					
					no_move = move_generator()
					if(no_move):
						print("Couldn't find an extra move")
						break
					#Move the node to the new chip
					self.partition_graph(use_U_shape = self.algorithm == 'u_shape_move', only_backward = True)
		

		#After being done, assign all the nodes in the graph to the corresponding chips based on their group information
		def assign_node_chip(n):	
			n.partition_estimate.chip_id = self.chips[n.partition_estimate.group_id]
		
		self.graph.walk_graph(f_node = assign_node_chip)



		directory = 'generated_files/circuits/multi_chip/'
		if(partition_params["dump_numbers"] == -1): 
			final_path_logs = directory+'network'
		else:
			final_path_logs = partition_params["filename"]
		fh = open(final_path_logs+"_partition.txt",'a')
		fh_log = open(final_path_logs+"_partition_log.txt",'a')
		fh_log_summary = open(final_path_logs+"_partition_log_summary.txt",'a')

		fh.write("==============================================================================================================\n")
		

		flagg = 0
		legal = 1
		
		#Check the legality of the solution
		#if self.failure != 1 :
		error_all_assigned,error_group,error_dsp,error_mem,error_topology = self.lagality_check(use_U_shape = self.algorithm in ['u_shape','u_shape_move'])
		if ((error_all_assigned == -1) and  ((error_all_assigned == -2) or (error_group == 1) or error_dsp or error_mem or error_topology)):
			fh.write("COULD NOT FIND A SOLUTION FOR RUNNING WITH " + str(self.num_chips) + ' chips \n')
			flagg = 1
			fh.write("Non Legal Solution ! Based on the legality check \n")
			if(error_all_assigned == -1 and self.failure == 0):
				fh.write("layer not assigned \n")
			
			if(error_all_assigned == -2):
				fh.write("layer assigned to non-existent chip \n")
				legal = 0
			
			if(error_group == 1):
				fh.write("Layers from same group are assigned to different chips \n")
				legal = 0
			if (error_dsp):
				fh.write("Assignment passes DSP budget on one of the chips \n")
				legal = 0
			if (error_mem):
				fh.write("Assignment passes M20K budget on one of the chips \n")
				legal = 0
			if (error_topology):
				fh.write("Network topology violated  \n")
			self.failure = 1

		if (self.failure):
			if flagg == 0:
				fh.write("COULD NOT FIND A SOLUTION FOR RUNNING WITH " + str(self.num_chips) + ' chips \n')
		else:
			fh.write("FOUND A SOLUTION FOR " + str(self.num_chips) + ' chips \n')
		
		fh.write('DSP distribution: '+ str(self.utilized_dsps) + ' \n')
		fh.write('MEM distribution: '+ str(self.utilized_mem) + ' \n')
		node_distrib = [0 for i in range(self.num_chips)]
		
		def print_node_chip(n):
			nonlocal node_distrib
			if n.partition_estimate.chip_id == -1:
				return
			node_distrib[n.partition_estimate.chip_id] +=1

		self.graph.walk_graph(f_node=print_node_chip)

		self.node_distrib = node_distrib
		fh.write('Assigned Nodes Distribution: '+ str(self.node_distrib) + ' \n')
		fh.write('Non-Assigned Nodes after Distribution: '+ str(len(self.graph.nodes) - sum(self.node_distrib)) + ' \n')
		if (self.failure != 1):
			fh.write("For the found solution: ")

		
		self.estimate_performance(fh=fh,failure = self.failure)
		
		def print_node_info(n):
			nonlocal fh_log
			fh_log.write(n.tf_op.name + ' DSPs: ' + str(n.planner_estimate.multiplier_count) + ' M20K: ' + str(n.m20k_estimate) + ' Group ID: ' + str(n.partition_estimate.group_id) + ' Chip ID: ' + str(n.partition_estimate.chip_id) + '\n')

		self.graph.walk_graph(f_node=print_node_info)
		fh_log.write("==============================================================================================================\n")
		fh_log.close()
		fh_log_summary.write(str(self.chips))
		fh_log_summary.write(str(self.final_dsps))
		fh_log_summary.write(str(self.final_mems))
		fh_log_summary.write("==============================================================================================================\n")
		fh_log_summary.close()
		fh.close()


		# This one is for the exploration flow, it builds up a table of results for the different sweeps
		# Will make sure it's all functional
		if(partition_params["dump_numbers"] != -1): 
			fh = open(final_path_logs+"_partition.txt")
			lines = fh.readlines()

			f2 = open(final_path_logs+"_summary_results.txt",'a')
			table = ''
			key = 0
			p = 0
			c = 0

			for i in range(len(lines)):
				if lines[i].split(':')[0] == 'Running with percentage':
					p = float(lines[i].split(':')[1])
					#print('p',p)
				if lines[i].split(':')[0] == 'Running with chips':
					c =  int(lines[i].split(':')[1])
					#print('c',c)

				if p == partition_params["dsp_percentage"] and c == partition_params["number_of_fpgas"]:	
					if lines[i].split()[0] == 'COULD':
						table+= ('F(' + str(float("{:.5f}".format(self.latency))) + '-'+ str(self.throughput)  + '-' + str(sum(self.final_dsps))+ '),')
						i += 5
						continue
					if lines[i].split()[0] == 'FOUND':
						for j in range(10):
							if(lines[i+j].split()[0] == 'For'):
								table+= ('S(' + str(float("{:.5f}".format(float(lines[i+j].split()[10])))) + '-'+ lines[i+j].split()[14]  + '-' + str(sum(self.final_dsps))+ '),')
								i += j+1
								break
						continue

			f2.write(table)
			f2.close()
			fh.close()

		if(partition_params["dump_numbers"] == -1):
			fh_cut = open(directory+"/multi_chip_config.txt",'w')
			def dump_cut_points(n):
				nonlocal fh_cut
				#First Node
				if (len(n.inputs) == 0):
					fh_cut.write(n.tf_op.name + ',')

				#Last Node
				if (len(n.outputs) == 0):
					oc = n.get_output_shape()[3]
					fh_cut.write(n.tf_op.name+',' + str(oc) + '\n')

				if len(n.outputs) > 0:
					if n.partition_estimate.chip_id != n.outputs[0]._to.partition_estimate.chip_id:
						oc = n.get_output_shape()[3]
						fh_cut.write(n.tf_op.name+',' + str(oc) + '\n')
						fh_cut.write(n.outputs[0]._to.tf_op.name+',')
			
			if self.failure:
				fh_cut.write("Failed")
			else:
				self.graph.walk_graph(f_node=dump_cut_points)

	def initialize_partitioning(self,partition_params):
		self.num_chips = partition_params["number_of_fpgas"]
		self.utilized_dsps = 0
		self.utilized_mem = 0
		self.latency = 0
		self.throughput = 0
		self.model = 'line' #Can be 'line' or 'chunk' referring to the ability to transfer line by line or a whole image
		self.node_distrib = [0 for i in range(self.num_chips)]
		self.failure = 0
		self.highest_cycles = 0
		self.failure_backward = 0
		self.fmax = partition_params["fmax"] * 1000000
		self.network = network(partition_params)
		self.chip_mem = [int(h.m20k * h.target_m20k_percent) for h in self.network.hops]
		self.chip_dsp = [int(h.dsps * h.target_dsp_percent) for h in self.network.hops]
		self.algorithm = partition_params["algorithm"]
		#Initialize the partition estimate will all variables for all the nodes in the list
		for nn in self.graph.nodes:
			nn.partition_estimate = partition_estimate(nn)
			nn.partition_estimate.multiplier_count = nn.planner_estimate.multiplier_count

	def assign_initial_groups(self):
		group = 0
		def read_info(n):
			nonlocal group
			n.partition_estimate.group_id = group
			group+=1
			if(n.type == 'Placeholder'):
				return
			if self.highest_cycles < n.planner_estimate.time_weight:
				self.highest_cycles  =  n.planner_estimate.time_weight
			n.planner_estimate.cycles_per_row = int(n.planner_estimate.time_weight/n.get_output_shape()[1])
			
		self.graph.walk_graph(f_node=read_info)

	def preprocess_conv_layers(self):	
		for nn in self.graph.nodes:
			if nn.type not in ['Conv2D', 'DepthwiseConv2dNative']:
				continue
			nn.partition_estimate.finish_cycle = nn.planner_estimate.cycles_per_row

		group_id = 0
		def drop_skip_path_nodes(n):
			"""For a skip path, the latency estimate should only take in the maximum of the two paths 
				currently the one with more conv layers"""
			nonlocal group_id
			if not(len(n.outputs)>1): #pick the nodes that have a fork
				return
			group_id = n.outputs[0]._to.partition_estimate.group_id
			convs_in_path = np.zeros(len(n.outputs))
			nodes_in_path  = [[] for i in range(len(n.outputs))]
			#Check all the paths forking from that node
			for i in range(len(n.outputs)):
				follow_node = n.outputs[i]._to
				#follow_node.partition_estimate.group_id = group_id
				#Count all the conv nodes on a specific path
				if(follow_node.type in ['Conv2D', 'DepthwiseConv2dNative']):
					convs_in_path[i]+=1
					nodes_in_path[i].append(follow_node)

				while not(len(follow_node.inputs)>1): # Assume that the fork ends when a node with more than one input is encountered (like an ADD Node)
					follow_node = follow_node.outputs[0]._to
					follow_node.partition_estimate.group_id = group_id
					if(follow_node.type in ['Conv2D', 'DepthwiseConv2dNative']):
						convs_in_path[i]+=1
						nodes_in_path[i].append(follow_node)

			index_max = np.where(convs_in_path == np.max(convs_in_path))
			for i in range(len(n.outputs)):
				if(i == index_max[0]):
					continue
				for n in nodes_in_path[i]: #Mark nodes that are on the skip path
					n.partition_estimate.on_skip_path = True

		self.graph.walk_graph(f_node=drop_skip_path_nodes)	

	def reduce_convs_to_groups(self):
		# Reduce Problem to groups not nodes
		only_convs = []
		def get_convs_only(n):
			nonlocal only_convs
			if n.type not in ['Conv2D', 'DepthwiseConv2dNative']:
				return
			only_convs.append(n)
			#fh_log.write(n.tf_op.name + ' DSPs: ' + str(n.planner_estimate.multiplier_count) + ' M20K: ' + str(n.m20k_estimate) + ' Group ID: ' + str(n.partition_estimate.group_id) + ' Chip ID: ' + str(n.partition_estimate.chip_id) + '\n')
		self.graph.walk_graph(f_node=get_convs_only)
		#check convolutions everywhere to see how many groups you have
		num_groups = 1
		current_group = only_convs[0].partition_estimate.group_id
		for i in range(len(only_convs)):
			if i  == 0 :
				continue
			if(only_convs[i].partition_estimate.group_id != current_group):
				num_groups +=1
				current_group = only_convs[i].partition_estimate.group_id

		#Now I know the total number of groups and the nodes that come together
		conv_group_ids = []
		def get_conv_group_ids(n):
			nonlocal conv_group_ids
			if (n.type in ['Conv2D', 'DepthwiseConv2dNative']):
				if (len(conv_group_ids)>0):
					if n.partition_estimate.group_id != conv_group_ids[-1]:
						conv_group_ids.append(n.partition_estimate.group_id)
				else:
					conv_group_ids.append(n.partition_estimate.group_id)
		self.graph.walk_graph(f_node=get_conv_group_ids)
		return num_groups,conv_group_ids

	def merge_other_nodes(self,num_groups,conv_group_ids):
		cur_index = 0
		cur_group = 0
		def rearrange_nodes(n):
			nonlocal cur_index
			nonlocal conv_group_ids
			nonlocal cur_group
			#nodes at the end with no following conv layers
			if cur_group == num_groups-1:
				n.partition_estimate.group_id = cur_group
				return
			
			if n.partition_estimate.group_id <= conv_group_ids[cur_index]:
				n.partition_estimate.group_id = cur_group
				return

			
			if not(n.partition_estimate.group_id >= conv_group_ids[cur_index+1]):
				n.partition_estimate.group_id = cur_group
				return
			
			cur_group+=1
			cur_index+=1
			n.partition_estimate.group_id = cur_group

		self.graph.walk_graph(f_node=rearrange_nodes)

	def construct_dsp_mem_arrays(self):
		all_arranged_nodes =[]
		def get_all_modes(n):
			nonlocal all_arranged_nodes
			all_arranged_nodes.append(n)

		self.graph.walk_graph(f_node=get_all_modes)
		dsps_final = []
		mems_final = []
		chips = []
		current_group = -1
		current_chip = -1

		for i in range(len(all_arranged_nodes)):
			if(all_arranged_nodes[i].partition_estimate.group_id == current_group and all_arranged_nodes[i].partition_estimate.group_id != -1):
				dsps_final[-1] += all_arranged_nodes[i].planner_estimate.multiplier_count
				mems_final[-1] += all_arranged_nodes[i].m20k_estimate
			else:
				current_group = all_arranged_nodes[i].partition_estimate.group_id
				dsps_final.append(all_arranged_nodes[i].planner_estimate.multiplier_count)
				mems_final.append(all_arranged_nodes[i].m20k_estimate)
				chips.append(all_arranged_nodes[i].partition_estimate.chip_id)
				current_chip = all_arranged_nodes[i].partition_estimate.chip_id
		
		#This is the last chip and it will have the output buffer which is not accounted for in the nodes M20K estimates
		# An estimate is considered from real hardware projects (the output buffer doesn't change if it's the same classes)
		mems_final[-1] += 20 
		self.chips = chips
		self.final_dsps = dsps_final
		self.final_mems = mems_final

	def partition_graph(self,use_U_shape = False,only_backward = False):
		""" - The partitioning currently is assuming a point-to-point Network topology 
			- A greedy algorithm that packs layers to a chip until the resources (DSP/M20K) exceed the chip resources
			- The resource counting is missing the LUTRAM (assumed to fit in all cases, have to be fixed) """


		chips_dsps = self.chip_dsp 
		chips_m20k = self.chip_mem 
		index = 0

		if only_backward:
			direction = 'Backward'
		else:
			direction = 'Forward'
		#Forward_pass
		def partition_groups(direction = 'Forward'):
			self.failure = 0
			if direction == 'Forward':
				index = 0
				utilized_dsps = np.zeros(self.num_chips)
				utilized_mem = np.zeros(self.num_chips)
			else:
				index = 0
				for i in range(len(self.chips)):
					if self.chips[i] == -1:
						continue 
					else:
						index = self.chips[i]

				#exit(1)
				utilized_dsps = self.utilized_dsps
				utilized_mem = self.utilized_mem
			for i in range(len(self.chips)):
				if self.failure  == 1:
					break
				if(self.chips[i] != -1):
					continue
				while (index != self.num_chips and direction == 'Forward') or (index != -1 and direction == 'Backward'):
					if not((utilized_dsps[index] + self.final_dsps[i]) > chips_dsps[index] or (utilized_mem[index] + self.final_mems[i])>chips_m20k[index]):
						self.chips[i] = index
						utilized_dsps[index] += self.final_dsps[i]
						utilized_mem[index] += self.final_mems[i]
						break
					if direction == 'Forward':
						index +=1
					else:
						index -= 1
					if(index == self.num_chips and direction == 'Forward') or (index == -1 and direction == 'Backward'): #If the chip ID exceeds the total number of chips, raise an error
						self.failure= 1
						break
			self.utilized_dsps = utilized_dsps
			self.utilized_mem = utilized_mem
		
		if not only_backward:
			partition_groups(direction = 'Forward')

		#Now Do the backward pass
		if use_U_shape and self.num_chips >1:
			partition_groups(direction = 'Backward')

			
	def estimate_performance(self,frequency=400000000,fh=None,failure = 0):
		# The goal of this function is to estimate the throughput and latency of a given accelerator
		arranged_conv = []
		group_id = 0

		#Have an array with all the conv nodes that will affect the latency (in order)
		def add_conv(n): 
			nonlocal arranged_conv
			if len(n.outputs) > 0:
				if n.outputs[0]._to.partition_estimate.chip_id != n.partition_estimate.chip_id:
					n.partition_estimate.edge_node = 1
			
			if n.type not in ['Conv2D', 'DepthwiseConv2dNative']:
				return
			elif not n.partition_estimate.on_skip_path:
				arranged_conv.append(n)
		self.graph.walk_graph(f_node=add_conv)

		#Account for any reduction in th eoutput dimension like pooling layers or flatten layers
		for i,n in enumerate(arranged_conv[0:-1]):
			n_out_shape = n.get_output_shape()[1]
			next_n_in_shape = arranged_conv[i+1].inputs[0]._from.get_output_shape()[1]
			n.partition_estimate.outputs_for_next_layer =int(n_out_shape/ next_n_in_shape)

		""" A function that estimates the latency and throughput for the convolution layers
			The base idea is: given the time that each node takes to compute an entire row (all channels)
			and given the buffer space that each layer has, we can model how rows are transferred between the layers
			from the start till the last layer produces the last row of output """
		def cycle_cycle_simulator(arranged_conv):
			flag = 0
			cycle = 0
			times = []
			initial_time = 12000
			placeholder_time = 12000
			event_cycle = -1
			num_images = 10
			images_counter = 1
			ff = 0
			arranged_conv[0].partition_estimate.input_buffer_elements = [(1,images_counter) for i in range((arranged_conv[0].inputs[0]._from.get_output_shape()[1] +arranged_conv[0].partition_estimate.pad+arranged_conv[0].partition_estimate.remain_pad))]
			print(len(arranged_conv[0].partition_estimate.input_buffer_elements))
			#exit(1)
			cycle += initial_time #Start after the placeholder handling time (for now it is fixed but it should be a function of th einput size)
			print('Estimating .........')
			while(True):
				min_wait = 9999999999

				#Each time step the inner loop iterates over all the nodes to update the state of each node
				#Update --> mark the node as "started to work" or "finished producing a row"
				for i, no in enumerate(arranged_conv):
					#Running multiple images through, every time the input buffer is empty we can delay it "placeholder_time" and fill it again 
					if i == 0 and cycle >= event_cycle and images_counter < num_images and event_cycle != -1:
						images_counter +=1
						arranged_conv[0].partition_estimate.input_buffer_elements = [(1,images_counter) for i in range((arranged_conv[0].inputs[0]._from.get_output_shape()[1] +arranged_conv[0].partition_estimate.pad+arranged_conv[0].partition_estimate.remain_pad))]

						event_cycle = -1

					# When the node is marked as working and the time it takes to finish a row is completed we update the state
					if(cycle - no.partition_estimate.start_cycle >= no.planner_estimate.cycles_per_row and no.partition_estimate.start_cycle !=-1):
						no.partition_estimate.produced_outputs +=1  #Increase the number of produced rows for the node
						
						#Clear (stride) number of rows from the buffer
						for m in range(no.get_strides()[1]):
							for n in range(len(no.partition_estimate.input_buffer_elements)):
								if no.partition_estimate.input_buffer_elements[n][1] == no.partition_estimate.current_image:
									no.partition_estimate.input_buffer_elements.pop(n)
									break

						no.partition_estimate.total_subtracted += no.get_strides()[1]
						
						"""Transfer the produced line to the following layer after taking care of any reduction . For example if there is a maxpool 
						   with a size of 2, every two produced outputs from this layer, one row will be transferred to the following layer"""
						if i != len(arranged_conv)-1 and (no.partition_estimate.produced_outputs % no.partition_estimate.outputs_for_next_layer == 0) and no.partition_estimate.produced_outputs != 0:
							arranged_conv[i+1].partition_estimate.input_buffer_elements.append((1,no.partition_estimate.current_image))
							arranged_conv[i+1].partition_estimate.total_lines_received+=1

						# This condition happens when a node finishes producing all of its rows
						if(no.partition_estimate.produced_outputs % no.get_output_shape()[1] == 0 and no.partition_estimate.produced_outputs!=0):
							#print(no.tf_op.name + " finished at time " + str(cycle*1000/(450)))
							
							if (i != len(arranged_conv)-1):
								if arranged_conv[i+1].partition_estimate.remain_pad >  0:
									arranged_conv[i+1].partition_estimate.bottom_padding_stage = True
							no.partition_estimate.total_lines_received = 0
							no.partition_estimate.bottom_padding_stage = False

							no.partition_estimate.current_image +=1
							if i == 0 and images_counter < num_images: #For the first node, empty the buffer and mark the time "event_cycle" to wait some time "placeholder_time" and then fill the buffer again
								no.partition_estimate.input_buffer_elements = []
								event_cycle = cycle + placeholder_time
							else: #For all other nodes, just empty what's left in the buffer from the previous image
								#Empty the buffer of any lines that belong to an old image if you are done with this image
								temp = []
								for m,n in enumerate(no.partition_estimate.input_buffer_elements):
									if n[1] == no.partition_estimate.current_image:
										temp.append(n)
								no.partition_estimate.input_buffer_elements = temp
								no.partition_estimate.total_subtracted  = 0

								#Add padding to the buffer
								for m in range(no.partition_estimate.fixed_initial_buffer):
									no.partition_estimate.input_buffer_elements.append((1,no.partition_estimate.current_image))
							
							# Store the times when the last node finishes
							if((i == len(arranged_conv)-1) and no.partition_estimate.produced_outputs % no.get_output_shape()[1] == 0):
								times.append(cycle)
							if((i == len(arranged_conv)-1) and (no.partition_estimate.produced_outputs == no.get_output_shape()[1]*num_images)):
								return cycle,times
							
						no.partition_estimate.start_cycle = -1 # Marks the node as idle
							
					#Count how many elements you have in the buffer
					no.partition_estimate.current_elements = 0
					for m in range(len(no.partition_estimate.input_buffer_elements)):
						if no.partition_estimate.input_buffer_elements[m][1] == no.partition_estimate.current_image:
							no.partition_estimate.current_elements+=1
					# This condotion checks if a node should start and mark that node as working
					if (no.partition_estimate.current_elements >= no.get_kernel_shape()[0] and no.partition_estimate.start_cycle == -1) or  (no.partition_estimate.bottom_padding_stage and no.partition_estimate.start_cycle == -1):
					
						if i != len(arranged_conv)-1:
							#Check if the next node has space and it's not in the bottom padding stage
							# or if it is in the bottom padding stage and has space for padding lines and extra lines	
							c4 = (len(arranged_conv[i+1].partition_estimate.input_buffer_elements) < arranged_conv[i+1].partition_estimate.max_buffer_space and  arranged_conv[i+1].partition_estimate.bottom_padding_stage == False)
							c5 = (len(arranged_conv[i+1].partition_estimate.input_buffer_elements) < (arranged_conv[i+1].partition_estimate.max_buffer_space - arranged_conv[i+1].partition_estimate.remain_pad)  and  (arranged_conv[i+1].partition_estimate.bottom_padding_stage == True))
							if c4 or c5:
								no.partition_estimate.start_cycle = cycle
						else:
							no.partition_estimate.start_cycle = cycle
					
					# To speed up simulation we don't need to check all cycles, only those where events happen
					if (no.partition_estimate.start_cycle + no.planner_estimate.cycles_per_row) - cycle < min_wait and no.partition_estimate.start_cycle != -1 and (no.partition_estimate.start_cycle + no.planner_estimate.cycles_per_row) > cycle :
						min_wait = (no.partition_estimate.start_cycle + no.planner_estimate.cycles_per_row) - cycle
				
				#Advances the simulation by 1 cycle if the min_wait is 1 cycle or after reaching a node that produced an output otherwise advance with min_wait 
				if flag == 1:
					cycle += 1
					flag = 0
					continue
				if(min_wait > 1):
					if event_cycle != -1:
						if (cycle + min_wait-1 > event_cycle) and (cycle < event_cycle) and images_counter < num_images:
							cycle = event_cycle

							continue
					cycle += (min_wait-1)
					continue
				else:
					if flag == 0:
						cycle += 1
						flag = 1
						continue

			return cycle,[]
				
		



		
		a = time.time()
		x,times  = cycle_cycle_simulator(arranged_conv) 
		times = times 
		diff=[]
		for i in range(len(times)):
			times[i] = times[i] 
			if i == 0:
				continue
			diff.append(times[i]-times[i-1]) 
		y = 1000 * (times[0]/self.fmax) 
		average = np.mean(diff) 
		throughput = int(self.fmax/average) 
		
		#Add network latency effect
		extra_latency = 0
		min_thrpughput = 9999999999
		"""for n in self.graph.nodes:
			if n.partition_estimate.edge_node==0:
				continue

			if self.model == 'line': #If the transfer is line by line
				#get transfer size  W * bits
				transfer_bits = n.get_output_shape()[1]*n.get_bits(is_act= True) 	
			else: #If transferring the whole image
				#get transfer size  W * H * C *  bits
				transfer_bits = n.get_output_shape()[1]*n.get_output_shape()[1]*n.get_output_shape()[3]*n.get_bits(is_act= True)

			source = n.partition_estimate.chip_id
			destination = n.outputs[0]._to.partition_estimate.chip_id
			for l in self.network.links:
				if l.source == source and l.destination==destination: #Forward connectivity
					extra = transfer_bits /(l.bandwidth*1000000) #in ms
					local_through = int((l.bandwidth*1000000000) / transfer_bits)  #in images/s
				if l.source == destination and l.destination==source: #Backward connectivity (to be changed to check the consumed badwidth)
					extra = transfer_bits /(l.bandwidth*1000000) #in ms
					local_through = int((l.bandwidth*1000000000) / transfer_bits )

			extra_latency += extra
			if local_through < min_thrpughput:
				min_thrpughput = local_through"""
		if failure != 1:
			print("passsed")
			fh.write('latency in ms for: ' +str(self.fmax/1000000) +' MHz: ' + str(float(y)) + ' ms Throughput is ' + str(throughput) +  ' imgs/s Runtime in s: ' + str(float("{:.5f}".format(time.time()-a)))+'\n')
		print('latency in ms for: ' +str(self.fmax/1000000) +' MHz: ' + str(float(y)) + ' ms Throughput is ' + str(throughput) +  ' imgs/s Runtime in s: ' + str(float("{:.5f}".format(time.time()-a)))+'\n')

		self.latency = float(y)
		self.throughput = throughput
		return


	def lagality_check(self,use_U_shape = False):
		def check_all_assigned_and_max_chips(graph): #can add a step to add #of partitioned nodes
			 
			number_chips = self.num_chips
			for n in graph.nodes:
				if(n.partition_estimate.chip_id == -1):#Check that all layers are assigned to chips
					return(-1,n) #Error -1 refers to a layer not assigned
				elif(n.partition_estimate.chip_id > number_chips-1):#Check that no layers are assigned to a non-existent chip
					return(-2,n) #Error -2 refers to a layer is assigned to a chip that doesn't exist
			return (0,0)
		
		def check_same_group(graph):
			"""Verify that nodes in the same group are assigned to the same chip (for skip path)
			N0 == ======= N1 ======= N5 ====
				||				     ||
			     == N2 == N3 == N4 ==
			Nodes 1-4 are in the same group so they should be assigned to the same chip"""
			     
			group_nodes = []
			current_group = -1
			error = 0
			group_id = -1
			def check_node_group(n): #Not correct
				nonlocal group_nodes
				nonlocal current_group
				nonlocal error
				nonlocal group_id
				if(n.partition_estimate.group_id == -1 or error==1): #If the node is not in a group, pass
					current_group = -1
					return
				
				elif (n.partition_estimate.group_id !=-1) and  current_group == -1: #Node at the start of the group. Start saving the upcoming nodes
					current_group = n.partition_estimate.group_id
					group_nodes.append(n)
					return
				
				elif (n.partition_estimate.group_id !=-1) and (n.partition_estimate.group_id == current_group):#Nodes in the middle of the group
					group_nodes.append(n)
					return
				
				elif (n.partition_estimate.group_id !=-1) and (n.partition_estimate.group_id != current_group):#Node after the end of the group
					group_id = group_nodes[0].partition_estimate.group_id
					for node in group_nodes[1:]: #Verify that all the nodes within the group have the same chip ID
						if node.partition_estimate.group_id != group_id:
							error = 1
							return

					group_nodes = []
					group_nodes.append(n)
					current_group = n.partition_estimate.group_id
			graph.walk_graph(f_node=check_node_group)
			return(error)
		
		def check_network_topology(graph,use_U_shape = False): #Not correct yet
			""" Legality check to verify that the partition doesn't violate the network topology
				For example, if the topology is a point-to-point, layers should come in order on the chips"""
			error = 0
			ascending = 0
			descending = 0
			current_chip_id = 0
			required_bandwidth = 0

			def check_network_order(n): 
				#print("got here")
				nonlocal error
				if(len(n.outputs) == 0):
					return
				source_id = 0
				destination_id = 0
				if error:
					return
				out_node = n.outputs[0]._to
				if n.partition_estimate.chip_id == out_node.partition_estimate.chip_id:
					return

				source_id = n.partition_estimate.chip_id
				destination_id = out_node.partition_estimate.chip_id
				flag = False
				for l in self.network.links:
					if (l.source == source_id and l.destination == destination_id): 
						flag = True
						"""Compute the required bandwidth (Assume for now that you do a whole transfer not line by line)
							The network bandwidth should be at least higher than the lowest standalone throughput"""
						required_bandwidth = n.get_output_shape()[1]*n.get_output_shape()[1]*n.get_output_shape()[3]*n.get_bits(is_act= True)*self.fmax / (1000000000*self.highest_cycles)
						l.used_tx += required_bandwidth
						if(l.used_tx > l.bandwidth):
							print("Note exceeding available resobandwidthurce")
					elif (l.source ==destination_id  and l.destination == source_id):
						flag = True
						required_bandwidth = n.get_output_shape()[1]*n.get_output_shape()[1]*n.get_output_shape()[3]*n.get_bits(is_act= True)*self.fmax / (1000000000*self.highest_cycles)
						l.used_rx += required_bandwidth
						if(l.used_rx > l.bandwidth):
							print("Note exceeding available resobandwidthurce")
						#check that the required bandwidth + the already used bandwidth is not more than the available bandwidth
				if (flag == False): #Nodes are on different fpgas but there is actually no link between those
					print("Error! Nodes are on different fpgas but there is actually no link between those")
					print("source" + n.tf_op.name + ' destination: ' + out_node.tf_op.name)

						

			graph.walk_graph(f_node=check_network_order)

			return error
		def check_max_chip_budget():
			error_dsp = 0
			error_mem = 0
			for (used_dsp,dsp) in zip(self.utilized_dsps,self.chip_dsp):
				if used_dsp > dsp:
					error_mem = 1
					break
			for (used_mem,mem) in zip(self.utilized_mem,self.chip_mem):
				if used_mem > mem:
					error_mem = 1
					break
			return error_dsp,error_mem	

		error_all_assigned,_ = check_all_assigned_and_max_chips(self.graph)
		error_group= 0 #check_same_group(self.graph)
		error_dsp,error_mem = check_max_chip_budget()
		#error_topology = check_network_topology(self.graph,use_U_shape)
		#exit(1)
		return error_all_assigned,error_group,error_dsp,error_mem,0



class partition_estimate():
		def __init__(self, node):
			self.on_skip_path = False
			self.chip_id = -1
			self.group_id = -1
			self.input_buffer_elements = 0
			self.produced_outputs = 0
			self.max_buffer_space = 0
			self.done_working = 0
			self.start_cycle = -1
			self.pad = 0
			self.finish_cycle = 0
			self.outputs_for_next_layer=0  #If there is a Maxpool for example and this node's output is 112 the next node will have an input of 56 and this variable will be set to two
			self.multiplier_count = 0
			self.fixed_initial_buffer = 0
			self.last_produced_element = 0
			self.total_subtracted = 0
			self.remain_pad = 0
			self.current_image = 1
			self.edge_node = 0
			self.current_elements = 0
			self.use_pad = 0
			self.lines_without_bottom_padding=0
			self.total_lines_received = 0
			self.bottom_padding_stage = False
			self.determine_buffer_size(node)
			self.compute_row_cycle_latency(node)
			

		def determine_buffer_size(self,n):
			if n.type not in ['Conv2D', 'DepthwiseConv2dNative']:
				return
			input_channels= n.get_kernel_shape()[2]
			n_channel_splits = n.planner_estimate.n_channel_splits
			ic_groups = n_channel_splits
			kernel_height = n.get_kernel_shape()[0]
			vertical_stride =  n.get_strides()[1]

			f_max_channels_in_input_buffer_line = lambda: math.ceil(input_channels / n_channel_splits)
			f_max_ia_buffer_depth = lambda: f_max_channels_in_input_buffer_line()  * (max(kernel_height, vertical_stride) + vertical_stride)
			while f_max_ia_buffer_depth() > 64 and f_max_ia_buffer_depth() < 70:
				n_channel_splits += 2
			n.planner_estimate.n_channel_splits = n_channel_splits
			parallel_weights_per_dsp = min(2, n_channel_splits)
			max_channels_in_input_buffer_line = f_max_channels_in_input_buffer_line()
			max_ia_buffer_depth = f_max_ia_buffer_depth()
			min_ia_buffer_depth = max_ia_buffer_depth
			ia_buffer_has_lutrams = False
			if min_ia_buffer_depth <= 32:
				ia_buffer_has_lutrams = True
				min_ia_buffer_depth = math.floor(32 / max_channels_in_input_buffer_line) * max_channels_in_input_buffer_line
			elif min_ia_buffer_depth <= 64:
				ia_buffer_has_lutrams = True
				min_ia_buffer_depth = math.floor(64 / max_channels_in_input_buffer_line) * max_channels_in_input_buffer_line
			else:
				if min_ia_buffer_depth < 128:
					print("WARNING: IABuffer depth greater than 64, less than 128 ", min_ia_buffer_depth)
				target_buffer_depth = math.ceil(max_ia_buffer_depth / 512) * 512
				min_ia_buffer_depth = math.floor(target_buffer_depth / max_channels_in_input_buffer_line) * max_channels_in_input_buffer_line

			
			lines_in_ia_buffer = min_ia_buffer_depth // max_channels_in_input_buffer_line
			num_buffered_lines = max(kernel_height + vertical_stride, lines_in_ia_buffer)
			n_channels = [math.ceil(input_channels / float(ic_groups)) for _ in range(input_channels % ic_groups)]
			n_channels += [input_channels // ic_groups for _ in range(ic_groups - (input_channels % ic_groups))]
			total_buffer_depths = [n_channels[i] * num_buffered_lines for i in range(ic_groups)]
			buffer_specs = [dl.RAM.size_for_depth(bd) for bd in total_buffer_depths]
			buffer_depths = [[min(bd, (i * bs[0] % bd - 1) % bs[0] + 1) for i in range(math.ceil(bd / bs[0]))] for bd,bs in zip(total_buffer_depths, buffer_specs)]
			buffer_depths = [[np.sum(buffer_depth_list)] for buffer_depth_list in buffer_depths]
			self.max_buffer_space = (buffer_depths[0][0] // n_channels[0]) 
			ia_shape = n.inputs[0]._from.example_outputs[0].shape
			k_shape = n.get_kernel_shape()
			stride = n.get_strides()[1]
			padding, _ = get_padding(
			ia_shape,
			k_shape,
			stride,
			n.properties["padding"],
			n.explicit_padding)
			self.pad = padding[1][0]  #Padding is [[0,0],[top,bottom],[right,left],[0,0]]
			self.remain_pad = padding[1][1]
			if self.pad == 0:
				self.input_buffer_elements = []
			else:
				self.input_buffer_elements = [(1,1) for ss in range(self.pad)]

				
			self.lines_without_bottom_padding = n.get_output_shape()[1] - (math.floor(((n.inputs[0]._from.example_outputs[0].shape[1] - k_shape[0] + self.pad)/stride)+1))
			self.fixed_initial_buffer = len(self.input_buffer_elements)
			 

			

		def compute_row_cycle_latency(self,n):
			if n.type not in ['Conv2D', 'DepthwiseConv2dNative']:
				return
			n.planner_estimate.cycles_per_row = int(n.planner_estimate.time_weight/n.get_output_shape()[1])
				
class network():
	def __init__(self, partition_params):
		self.total_nodes = 0
		self.hops = []
		self.links = []
		self.read_network_file(partition_params)
		#for l in self.links:
			#print(str(l.id) + ' ' +str(l.source) + ' ' + str(l.destination) + ' ' + str(l.bandwidth))
		
		#for h in self.hops:
			#print(str(h.id) + ' ' +str(h.type) + ' ' + str(h.dsps) + ' ' + str(h.m20k))
	
	def read_network_file(self,partition_params):
		for h in partition_params["hops"]:
			new_hop = hop()
			new_hop.id = h["id"]
			new_hop.type = h["type"]
			new_hop.dsps = h["dsps"]
			new_hop.m20k = h["m20k"]
			new_hop.target_dsp_percent = h["target_dsp_percent"]
			new_hop.target_m20k_percent = h["target_m20k_percent"]
			self.hops.append(new_hop)

		for l in partition_params["links"]:
			new_link = link()
			new_link.id = l["id"]
			new_link.type = l["type"]
			new_link.source = l["source"]
			new_link.destination =l["destination"]
			new_link.bandwidth = l["bandwidth"]
			self.links.append(new_link)

class link():
	def __init__(self):
		self.id  = 0
		self.type = 'f-f'
		self.source = 0
		self.destination = 1  
		self.bandwidth  = 100
		self.used_tx = 0
		self.used_rx = 0

class hop():
	def __init__(self):
		self.id =0
		self.type = 'f'
		self.dsps =0 
		self.m20k = 0
		self.target_dsp_percent = 1
		self.target_m20k_percent = 1
