import warnings
import math
import os
import sys
import json
import argparse
import pprint
import numpy as np
import time
import digitallogic as dl
from pathlib import Path
from functools import reduce
from digitallogic.utils import clog2, get_padding, print_stage_header, v_comment_string, get_quantized, BitArray, cast_fixed, is_power_of_2
from hpipe.build_accelerator import get_plan
from hpipe.Plan import update_enable_bfp
from bashplotlib.histogram import plot_hist
from hpipe.GraphBuilder import Node
from hpipe.GraphBuilder import update_OVERWRITE_QUANT_BITS
import hpipe.ArchLoader as arch
from hpipe.GraphBuilder import Node, Edge
from random import randint, seed
import tensorflow
try:
	import tensorflow.compat.v1 as tf
except:
	import tensorflow as tf
modules_to_debug = []
from hpipe.Partition import Partition

enable_sim = False
real_ip_sim = False
WRITE_PATH_NARROW_CONSTANT = 8
RANDOM_SEED = 5724

# For tensor mode only, this will automatically get enabled if our bit width for convolutions is greater than 8-bit (e.g. 16 bits)
enable_bfp = False

class NNStageInput(dl.Module):
	def __init__(self, node, input_index, **kwargs):
		super(NNStageInput, self).__init__("nn_stage_input", kind="nn_stage_input", **kwargs)
		inst = self.inst
		with dl.ModuleScope(self):

			self.act_indices = {
				"batch"    : 0,
				"height"   : 1,
				"width"    : 2,
				"channels" : 3
			}

			self.input_index = input_index
			self.node = node
			self.ia_shape = node.inputs[input_index]._from.example_outputs[0].shape

			self.set_bit_widths(node)
			parallel_acts_per_ram = node.inputs[input_index]._from.get_parallel_acts_per_memory()
			input_width = self.get_input_act(dim="width")
			combined_count = math.ceil(float(input_width) / parallel_acts_per_ram)

			bits = self.bits
			signed = self.signed

			self.inputs = [inst(dl.Logic,
				bits,
				name="input_" + str(i),
				signed=signed) for i in range(input_width)]
			self.combined_inputs = []
			self.combined_writes = []
			self.writes = [inst(dl.Logic, 1, name="write_" + str(i)) for i in range(input_width)]
			for i in range(combined_count):
				lo = i * parallel_acts_per_ram
				hi = min(lo + parallel_acts_per_ram, combined_count*parallel_acts_per_ram)
				self.combined_inputs.append(inst(dl.Concat, *self.inputs[lo:hi], name="combined_input_" + str(i)))
				self.combined_writes.append(self.writes[lo])

			self.write = inst(dl.Logic, 1, name="write")
			self.space_to_write_line = inst(dl.Logic, 1, name="space_to_write_line")


	def set_bit_widths(self, node):
		a_precision_params = node.inputs[self.input_index]._from.precision_parameters
		bits = np.sum(np.array([int(a_precision_params[b]) for b in
			"a_sign_bits,a_exponent,a_int,a_f".split(",")]))
		signed = a_precision_params["a_sign_bits"] == 1

		self.signed          = signed
		self.exponent_bits   = a_precision_params["a_exponent"]
		self.int_bits        = a_precision_params["a_int"]
		self.fractional_bits = a_precision_params["a_f"]
		self.bits            = bits

	def get_input_act(self, dim=None):
		if dim is None:
			return self.node.inputs[self.input_index]._from.example_outputs[0]
		assert(dim in self.act_indices)
		return self.ia_shape[self.act_indices[dim]]

class NNStage(dl.Module):
	kind =  "nn_stage"
	def __init__(self, node, build_data_path=True, output_csv_path=None, depthwise_conv_parallelism=1,called_from_wrapper=False, **kwargs):

		super(NNStage, self).__init__(self.__class__.kind, kind=self.__class__.kind, **kwargs)
		inst = self.inst
		self.output_csv_path = output_csv_path
		self.buffered_layer = False		#This attribute is used only by the credit system. It is later overwritten to True by layers that have input buffers (e.g. BasicConv)
		with dl.ModuleScope(self):
			print("Instantiating " + self.__class__.kind + " for tf op " + node.tf_op.name)

			if "clock" in kwargs:
				self.clock = kwargs["clock"]
			self.node = node
			self.act_indices = {
				"batch"    : 0,
				"height"   : 1,
				"width"    : 2,
				"channels" : 3
			}
			self.input_modules = [inst(NNStageInput, node, i) for i in range(len(node.inputs))]
			if len(self.input_modules) == 1:
				self.inputs = self.input_modules[0].inputs
				self.write = self.input_modules[0].write
				self.combined_inputs = self.input_modules[0].combined_inputs
				self.writes = self.input_modules[0].writes
				#self.writes[0].copy()
				self.combined_writes = self.input_modules[0].combined_writes
				self.space_to_write_line = self.input_modules[0].space_to_write_line
			self.set_act_shapes(node)
			
			#For depthwise convs we need to adjust the output channels since each conv module will only compute a certain subset 
			if depthwise_conv_parallelism > 1:
				list_oa_shape = list(self.oa_shape)
				list_oa_shape[self.act_indices["channels"]] = depthwise_conv_parallelism
				self.oa_shape = tuple(list_oa_shape)

							
			self.set_bit_widths(node)
			output_width = self.get_output_act(dim="width")

			self.can_write_line = inst(dl.Logic, 1, name="can_write_line")
			self.output_valid = inst(dl.Logic, 1, name="control_output_valid")
			self.outputs_valid = [inst(dl.Logic, 1, name="output_valid_" + str(i)) for i in range(output_width)]

			###################################################################
			################### Credit Sysem Signals ##########################
			###################################################################
			#: The credit system allows for a latency insensitive pipeline that also doesn't rely on the backpressure signals reaching the previous computing layer in time
			#:
			#: The credit counter is a counter that counts up to the minimum buffer size of the next layer with it's own buffer:
			#:  	- Those layers have the attribute self.buffered_layer = True
			#: 		- At the time of writing, those layers are ["mean", "add", "max_pool", "conv_wrapper", "basic_conv", "tensor_conv"]
			#:      - The counter keeps track of the number of lines it sent. The counter starts at 0 and stops when credit_counter == min_buffer_size_ahead
			#: 
			#:
			#: The (non-signal) variables related to the credit system are the following:
			#:    	1. cred_dec_amount: Negative integer. The number of lines that the buffer frees in one shot in normal situations (traditionally -stride lines)
			#:      2. cred_dec_final_amount: Negative integer. The amount of lines that the buffer frees when an image is complete and it must start the next image
			#:		3. cred_max: Dictionary containing the names of the previous layers (2 for Add layers, 1 for other layers), 
			#:                   along with their associated buffer depths in the current layer
			#:      4. cred_padding: List of two positive integers. Contains the number of top and bottom padding lines for current layer
			#:
			#: The three main signals are the following:
			#: 		1. cred_inc: Signal issued by current layer, and used by current layer, to increase the credit counter when a line is issued to the next layer
			#: 		2. cred_dec: Signal issued by current layer and used by previous layer, to notify it that cred_dec_amount lines have been cleared
			#:      3. cred_dec_final: Signal issued by current layer and used by previous layer, to notify it that cred_dec_final_amount lines have been cleared
			#:		4. cred_padding_unpause (Optional, issued only in InputBufferController): Signal issued by current layer and used by previous layer,
			#:                                                                               to notify it that padding lines have been inserted in the IBC,
			#:																				 and that more lines can now be issued again

			self.cred_inc = inst(dl.Logic, 1, name="credit_inc")
			self.cred_dec = inst(dl.Logic, 1, name="credit_dec")
			self.cred_dec_final = inst(dl.Logic, 1, name="credit_dec_final")
			self.cred_dec_amount = 0
			self.cred_dec_final_amount = 0
			self.cred_max = {}
			self.cred_padding = [0,0]

			_, _, output_bits = self.get_bit_widths()
			_, _, output_signed = self.get_signs()
			output_bit_spec = self.get_bit_spec("output")
			self.outputs = [inst(dl.Logic,
				output_bits,
				signed=output_signed,
			#	arithmetic_bit_spec=output_bit_spec,
				name="output_" + str(i)) for i in range(output_width)]

			with dl.ModuleScope(inst(dl.Module, "done_controller", kind="done_controller")):
				if called_from_wrapper:
					output_channels = node.values[0].shape[-1]
				else:
					output_channels = self.get_output_act(dim="channels")

				self.output_channel_counter = inst(dl.Counter,
					reset_value=0,
					increment_value=1,
					end_value=output_channels - 1,
					name="output_channel_counter")
				self.output_channel_counter.set_increment_condition(self.output_valid.delay(1))
				self.output_lines_counter = inst(dl.Counter,
					reset_value=0,
					increment_value=1,
					end_value=self.get_output_act(dim="height")-1,
					name="output_lines_counter")
				self.output_lines_counter.set_increment_condition(self.output_channel_counter.done_and_incrementing.delay(1))
				states = ["IDLE", "PROCESSING_IMAGE", "DONE"]
				edges = [
					["IDLE", "PROCESSING_IMAGE", self.output_valid.delay(1)],
					["PROCESSING_IMAGE", "DONE", self.output_lines_counter.done_and_incrementing.delay(1)],
					["DONE", "IDLE"]]
				state_machine = inst(dl.StateMachine, states, edges, None)
			
			self.line_done = inst(dl.Logic, bits=1, name="line_done")
			self.line_done.set_driver(self.output_channel_counter.done_and_incrementing.delay(1))
			self.done = inst(dl.Logic, bits=1, name="done")
			self.done.set_driver(state_machine.is_done.delay(1))

			self.utilization_estimates = {}
			self.control_valid_to_output_valid_delay = 0
			self.control_to_input_delay = 0
			self.guarantees_output_valid_low_in_cycles = True
			self.reset = inst(dl.Logic, bits=1, name="reset")
			self.reset.set_driver(kwargs["reset"])
			self.instantiate(node, inst, build_data_path, **kwargs)
			print()

	
	def get_custom_verilog_str(self, t="  "):
		if self.output_csv_path is None:
			self.output_csv_path = 'generated_files/layer_images/verilog/' + self.name + '.csv'
		dump_input = False #self.node.type != "Placeholder"
		input_csv_path = self.output_csv_path[:-4] + "_input.csv"
		fh = self.name + "_fh"
		fh_i = self.name + "_input_fh"
		verilog = t + "/*verilator no_inline_module*/\n"
		verilog += "`ifdef WRITE_IMAGES\n"
		verilog += "  integer " + fh + ";\n"
		verilog += "  integer " + fh_i + ";\n"
		verilog += "  initial begin\n"
		verilog += '    ' + fh + ' = $fopen("' + self.output_csv_path + '", "w");\n'
		verilog += '    $fwrite(' + fh + ', "' + str(self.get_output_act(dim="channels")) + '\\n");\n'
		if dump_input:
			verilog += '    ' + fh_i + ' = $fopen("' + input_csv_path + '", "w");\n'
			verilog += '    $fwrite(' + fh_i + ', "' + str(self.get_input_act(dim="channels")) + '\\n");\n'
		verilog += """  end
  final begin
"""
		verilog += '    $fclose(' + fh + ');\n'
		verilog += '    $fclose(' + fh_i + ');\n'
		verilog += '  end\n'
		verilog += "`endif\n"

		verilog += f"  always @(posedge {self.clock.name}) begin\n"
		verilog += "`ifdef WRITE_IMAGES\n"
		verilog += "    if (" + self.outputs_valid[0].name + " && !" + self.reset.name + ") begin\n"
		verilog += '      $fwrite(' + fh + ', "' + ",".join(['%d'] * len(self.outputs)) + '\\n", ' + ", ".join([d.name for d in self.outputs]) + " );\n"
		verilog += "    end\n"
		if dump_input:
			verilog += "    if (" + self.writes[0].name + " && !" + self.reset.name + ") begin\n"
			verilog += '      $fwrite(' + fh_i + ', "' + ",".join(['%d'] * len(self.inputs)) + '\\n", ' + ", ".join([d.name for d in self.inputs]) + " );\n"
			verilog += "    end\n"
		verilog += "`endif\n"
		verilog += "`ifdef DISPLAY_FINISH_TIMES\n"
		verilog += "    if (" + self.done.name + ") begin\n"
		verilog += '      $display("' + self.name + ' finished at time %0t", $time);\n'
		verilog += "    end\n"
		#verilog += "    if (!" + self.can_write_line.name + ") begin\n"
		#verilog += '      $display("' + self.name + ' cannot write");\n'
		#verilog += "    end\n"
		verilog += "`endif\n"
		verilog += "  end\n"
		return verilog

	def get_instance_comment(self, t="  "):
		header = "/" * 80 + "\n"
		comment = header
		padding = max((80-len(self.node.tf_op.name)) // 2, 0)
		comment += padding * " " + self.node.tf_op.name + "\n"
		comment += header
		comment += "Input Nodes:\n"
		for i in self.node.inputs:
			n = i._from
			comment += "  (" + n.tf_op.name
			if n.module is not None:
				comment += ", " + n.module.name
			comment += ")\n"
		comment += "Output Nodes:\n"
		for i in self.node.outputs:
			n = i._to
			comment += "  (" + n.tf_op.name
			if n.module is not None:
				comment += ", " + n.module.name
			comment += ")\n"
		comment += "Control valid to output valid delay: " + str(self.get_control_valid_to_output_valid_delay()) + "\n"
		comment += "Control to input delay: " + str(self.get_control_to_input_delay()) + "\n"
		if len(self.input_modules) > 0:
			comment += "Input shape: " + str(self.get_input_act().shape) + "\n"
		comment += "Output shape: " + str(self.get_output_act().shape) + "\n"
		comment += "Output Act Bit Spec: " + "\n"
		comment += "  " + pprint.pformat(self.get_bit_spec("output"), indent=len(t)).replace("\n", "\n  ") + "\n"
		comment += "Parameter Bit Spec: " + "\n"
		comment += "  " + pprint.pformat(self.get_bit_spec("parameters"), indent=len(t)).replace("\n", "\n  ") + "\n"
		return v_comment_string(comment, t)

	def set_act_shapes(self, node):
		self.oa_shape = node.example_outputs[0].shape

	def get_input_act(self, dim=None, input_index=0):		
		if dim is None:
			return self.node.inputs[input_index]._from.example_outputs[0]
		assert(dim in self.act_indices)
		return self.input_modules[input_index].ia_shape[self.act_indices[dim]]

	def get_output_act(self, dim=None):
		if dim is None:
			return self.node.example_outputs[0]
		assert(dim in self.act_indices)	
		return self.oa_shape[self.act_indices[dim]]

	def set_bit_widths(self, node):
		oa_precision_params = node.precision_parameters
		output_bits = np.sum(np.array([int(oa_precision_params[b]) for b in
			"a_sign_bits,a_exponent,a_int,a_f".split(",")]))
		osigned = oa_precision_params["a_sign_bits"] == 1

		self.output_signed = osigned
		self.output_exponent_bits = oa_precision_params["a_exponent"]
		self.output_int_bits = oa_precision_params["a_int"]
		self.output_fractional_bits = oa_precision_params["a_f"]
		self.output_bits = output_bits

		if len(node.values) > 0:
			p_bits = np.sum(np.array([int(oa_precision_params[b]) for b in
				"sign_bits,exponent,int,f".split(",")]))
			psigned = oa_precision_params["sign_bits"] == 1
			self.parameters_signed = psigned
			self.parameters_exponent_bits = oa_precision_params["exponent"]
			self.parameters_int_bits = oa_precision_params["int"]
			self.parameters_fractional_bits = oa_precision_params["f"]
			self.parameter_bits = p_bits
		else:
			self.parameters_signed = -1
			self.parameters_exponent_bits = -1
			self.parameters_int_bits = -1
			self.parameters_fractional_bits = -1
			self.parameter_bits = -1


	def get_from_input_module(self, index, f):
		if len(self.input_modules) <= index:
			return None
		return f(self.input_modules[index])

	def get_signs(self, input_index=0):
		return (self.get_from_input_module(input_index, lambda x: x.signed), self.parameters_signed, self.output_signed)

	def get_bit_widths(self, input_index=0):
		return (
			self.get_from_input_module(input_index, lambda x: x.bits),
			self.parameter_bits,
			self.output_bits)

	def get_int_part_widths(self, input_index=0):
		return (
			self.get_from_input_module(input_index, lambda x: x.int_bits),
			self.parameters_int_bits,
			self.output_int_bits)

	def get_frac_part_widths(self, input_index=0):
		return (self.get_from_input_module(input_index, lambda x: x.fractional_bits), self.parameters_fractional_bits, self.output_fractional_bits)

	def get_bit_spec(self, spec_name, input_index=0):
		assert(spec_name in "input,output,parameters".split(","))
		widths = self.get_bit_widths(input_index)
		int_parts = self.get_int_part_widths(input_index)
		frac_parts = self.get_frac_part_widths(input_index)
		signs = self.get_signs(input_index)
		index = {
			"input"     :0,
			"parameters":1,
			"output"    :2}[spec_name]
		return {
			"width" : widths[index],
			"int"   : int_parts[index],
			"frac"  : frac_parts[index],
			"sign"  : signs[index]
		}

	def get_control_valid_to_output_valid_delay(self):
		return self.control_valid_to_output_valid_delay

	def _set_control_valid_to_output_valid_delay(self, ctod):
		self.control_valid_to_output_valid_delay = ctod

	def get_control_to_input_delay(self):
		return self.control_to_input_delay

	def _set_control_to_input_delay(self, ctid):
		self.control_to_input_delay = ctid

	def get_aligned_for_add(self, i1, i2, i1_bit_spec, i2_bit_spec):
		new_spec = {}
		for k,v in i1_bit_spec.items():
			new_spec[k] = v
		def get_extended(_input, bits_to_extend, signed, extend_integer=True):
			if extend_integer:
				string = "integer"
			else:
				string = "fractional"
			# Only use the sign bit for extending the integer, otherwise we should use 0 for the fractional component
			# According to Matthew these bits shouldn't matter to much for non-quantized networks as we are adding precision and have no way of knowing what the fraction should be
			# For pre-quantized networks, however, this introduces error since a lot of the weights are already quantized to a certain number of bits, hence any additional ones should be 0
			if signed and extend_integer:
				to_repeat = _input[_input._bit_width-1]
			else:
				to_repeat = self.inst(dl.Constant, bits=1, value=0, name=_input.name + "_" + string + "_extend")
			if extend_integer:
				to_concat = [_input] + [to_repeat] * bits_to_extend
			else:
				# extend fractional
				to_concat = [to_repeat] * bits_to_extend + [_input]

			return self.inst(dl.Concat, *to_concat, signed=signed, name=_input.name + "_" + string + "_extended")
		def get_fractional_extend_amount(greater_spec, lesser_spec):
			nonlocal new_spec
			new_spec["frac"] = greater_spec["frac"]
			return greater_spec["frac"] - lesser_spec["frac"]
		def get_int_extend_amount(greater_spec, lesser_spec):
			nonlocal new_spec
			new_spec["int"] = greater_spec["int"]
			new_spec["sign"] = greater_spec["sign"]
			add_or_subtract_sign_bit = 0
			if greater_spec["sign"] and not lesser_spec["sign"]:
				add_or_subtract_sign_bit = 1
			elif not greater_spec["sign"] and lesser_spec["sign"]:
				add_or_subtract_sign_bit = -1
			return greater_spec["int"] - lesser_spec["int"] + add_or_subtract_sign_bit
		if i1_bit_spec["frac"] < i2_bit_spec["frac"]:
			e = get_fractional_extend_amount(i2_bit_spec, i1_bit_spec)
			i1 = get_extended(i1, e, i1_bit_spec["sign"], extend_integer=False)
		elif i1_bit_spec["frac"] > i2_bit_spec["frac"]:
			e = get_fractional_extend_amount(i1_bit_spec, i2_bit_spec)
			i2 = get_extended(i2, e, i2_bit_spec["sign"], extend_integer=False)

		if i1_bit_spec["int"] < i2_bit_spec["int"]:
			e = get_int_extend_amount(i2_bit_spec, i1_bit_spec)
			if e > 0:
				i1 = get_extended(i1, e, i1_bit_spec["sign"], extend_integer=True)
		elif i1_bit_spec["int"] > i2_bit_spec["int"]:
			e = get_int_extend_amount(i1_bit_spec, i2_bit_spec)
			if e > 0:
				i2 = get_extended(i2, e, i2_bit_spec["sign"], extend_integer=True)

		# extend one additional bit to allow two max integer quantities to overflow without
		# overflowing into the sign bit
		i1 = get_extended(i1, 1, i1_bit_spec["sign"], extend_integer=True)
		i2 = get_extended(i2, 1, i2_bit_spec["sign"], extend_integer=True)
		new_spec["int"] += 1
		new_spec["width"] = new_spec["int"] + new_spec["frac"] + int(new_spec["sign"])
		return (i1, i2, new_spec)

	def get_individual_values_from_combined(self, combined, use_output_act_bits=False, input_index=0):
		node = self.node
		if use_output_act_bits:
			input_width = self.get_output_act(dim="width")
		else:
			input_width = self.get_input_act(dim="width", input_index=input_index)
		combined_count = len(combined)
		bits, _, output_bits = self.get_bit_widths()
		if use_output_act_bits:
			bits = output_bits
		parallel_acts_per_ram = combined[0]._bit_width // bits

		individuals = []
		for i,c in enumerate(combined):
			for j in range(parallel_acts_per_ram):
				#if (i * parallel_acts_per_ram + j) >= input_width:
				#	break
				with dl.ParamScope(name=c.name + "_" + str(j)):
					individuals.append(c[j*bits+bits-1:j*bits])
		individuals = individuals[len(individuals)-input_width:]
		return individuals

	def instantiate(self, node, inst, build_data_path=True, **kwargs):
		pass

	def connect_credit_system(self, inst, space_to_write_line_signals):
		n = self.node
		if self.module_name == "placeholder":
			if len(space_to_write_line_signals) > 1:
				self.can_write_line.set_driver(inst(dl.AND, *space_to_write_line_signals).delay(3))
			elif len(space_to_write_line_signals) == 1:
				self.can_write_line.set_driver(space_to_write_line_signals[0])
		elif self.buffered_layer:
			#Buffered layers can choose when to produce lines
			#They have as many credit counters as receiving layers they produce lines for
			#If the layer has more than one output, padding considerations become slightly more complicated
			if len(n.outputs) > 1 :
				################## Padding Signals ##########################
				upper_padding_done = [inst(dl.Flop, reset_driver=1, bits=1, name="upper_padding_done_"+str(i)) for i,o in enumerate(n.outputs)]
				lower_padding_done = [inst(dl.Flop, reset_driver=1, bits=1, name="lower_padding_done_"+str(i)) for i,o in enumerate(n.outputs)]
				credit_counter_upper_padding_inc = [inst(dl.Logic, bits=1, name="credit_counter_upper_padding_inc_"+str(i)) for i,o in enumerate(n.outputs)]
				credit_counter_lower_padding_inc = [inst(dl.Logic, bits=1, name="credit_counter_lower_padding_inc_"+str(i)) for i,o in enumerate(n.outputs)]
				credit_counter_change_values = [[1, o._to.module.cred_dec_amount, o._to.module.cred_dec_final_amount] for o in n.outputs]
				for i,o in enumerate(n.outputs):
					mod = o._to.module
					if (mod.cred_padding[0] != 0):
						credit_counter_change_values[i].append(mod.cred_padding[0])
					if (mod.cred_padding[1] != 0):
						credit_counter_change_values[i].append(mod.cred_padding[1])
				###############################################################################

				self.credit_counters = [inst(dl.MultiChangeCounter, reset_value=o._to.module.cred_padding[0], change_values=credit_counter_change_values[i], end_value=o._to.module.cred_max[self.name]) for i,o in enumerate(n.outputs)]	
				self.credit_counters_not_full = [inst(dl.AND, inst(dl.NOT, c.is_done.delay(1)), upper_padding_done[i], lower_padding_done[i]).delay(1) for i,c in enumerate(self.credit_counters)]
				self.all_have_credit = inst(dl.AND, *self.credit_counters_not_full)
				for i,o in enumerate(n.outputs):
					mod = o._to.module	
					self.credit_counters[i].change_signals[0].set_driver(self.cred_inc)
					self.credit_counters[i].change_signals[1].set_driver(mod.cred_dec.delay(3))
					self.credit_counters[i].change_signals[2].set_driver(mod.cred_dec_final.delay(3))
					print(self.module_name, " has a cred max of ", mod.cred_max, " and next layer is ", mod.module_name)
				self.can_write_line.set_driver(self.all_have_credit.delay(1))

				####################################################################################
				upper_padding_set = ["" for o in n.outputs]
				upper_padding_unset = ["" for o in n.outputs]
				lower_padding_set = ["" for o in n.outputs]
				lower_padding_unset = ["" for o in n.outputs]
				for i,o in enumerate(n.outputs):
					mod = o._to.module	
					if (mod.cred_padding[0] != 0):
						upper_padding_set[i] = inst(dl.Logic, bits=1, name="upper_padding_set_"+str(i))
						upper_padding_unset[i] = inst(dl.Logic, bits=1, name="upper_padding_unset_"+str(i))
						upper_padding_unset[i].set_driver(self.cred_dec_final.delay(1))
						upper_padding_set[i].set_driver(credit_counter_upper_padding_inc[i])
						upper_padding_done[i].set_driver(inst(dl.OR, inst(dl.AND, upper_padding_done[i], inst(dl.NOT, upper_padding_unset[i])), upper_padding_set[i]))
						if (hasattr(mod, "cred_padding_unpause")):
							credit_counter_upper_padding_inc[i].set_driver(inst(dl.AND, mod.cred_padding_unpause.delay(3), lower_padding_done[i], inst(dl.NOT, upper_padding_done[i]), inst(dl.LE, self.credit_counters[i].current_value, inst(dl.Constant, bits=clog2(mod.cred_max[self.name]), value=mod.cred_max[self.name]-mod.cred_padding[0]))))
						else:
							credit_counter_upper_padding_inc[i].set_driver(inst(dl.AND, lower_padding_done[i], inst(dl.NOT, upper_padding_done[i]), inst(dl.LE, self.credit_counters[i].current_value, inst(dl.Constant, bits=clog2(mod.cred_max[self.name]), value=mod.cred_max[self.name]-mod.cred_padding[0]))))
						self.credit_counters[i].change_signals[3].set_driver(credit_counter_upper_padding_inc[i])
					else:
						upper_padding_done[i].set_driver(inst(dl.Constant, bits=1, value=1))
						credit_counter_upper_padding_inc[i].set_driver(inst(dl.Constant, bits=1, value=0))

					if (mod.cred_padding[1] != 0):
						lower_padding_set[i] = inst(dl.Logic, bits=1, name="lower_padding_set_"+str(i))
						lower_padding_unset[i] = inst(dl.Logic, bits=1, name="lower_padding_unset_"+str(i))
						lower_padding_unset[i].set_driver(self.cred_dec_final.delay(1))
						lower_padding_set[i].set_driver(credit_counter_lower_padding_inc[i])	
						lower_padding_done[i].set_driver(inst(dl.OR, inst(dl.AND, lower_padding_done[i], inst(dl.NOT, lower_padding_unset[i])), lower_padding_set[i]))
						if (hasattr(mod, "cred_padding_unpause") and mod.cred_padding[0] == 0):
							credit_counter_lower_padding_inc[i].set_driver(inst(dl.AND, mod.cred_padding_unpause.delay(3), inst(dl.NOT, lower_padding_done[i]), inst(dl.LE, self.credit_counters[i].current_value, inst(dl.Constant, bits=clog2(mod.cred_max[self.name]), value=mod.cred_max[self.name]-mod.cred_padding[1]))))						
						else:
							credit_counter_lower_padding_inc[i].set_driver(inst(dl.AND, inst(dl.NOT, lower_padding_done[i]), inst(dl.LE, self.credit_counters[i].current_value, inst(dl.Constant, bits=clog2(mod.cred_max[self.name]), value=mod.cred_max[self.name]-mod.cred_padding[1]))))						
						if (mod.cred_padding[0] != 0):
							self.credit_counters[i].change_signals[4].set_driver(credit_counter_lower_padding_inc[i])
						else:
							self.credit_counters[i].change_signals[3].set_driver(credit_counter_lower_padding_inc[i])

					else:	
						lower_padding_done[i].set_driver(inst(dl.Constant, bits=1, value=1))
						credit_counter_lower_padding_inc[i].set_driver(inst(dl.Constant, bits=1, value=0))
				#########################################################################################		

			elif len(n.outputs) == 1 :
				mod = n.outputs[0]._to.module
				if not bool(mod.cred_max):
					self.can_write_line.set_driver(space_to_write_line_signals[0])	#In case the last layer is not a complex stage (for instance MV1 tensor_conv into bias_add, then cred_max is empty)
				else:							
					################## Padding Signals Part 1 ##########################
					upper_padding_done = inst(dl.Flop, reset_driver=1, bits=1, name="upper_padding_done")
					lower_padding_done = inst(dl.Flop, reset_driver=1, bits=1, name="lower_padding_done")
					credit_counter_upper_padding_inc = inst(dl.Logic, bits=1, name="credit_counter_upper_padding_inc")
					credit_counter_lower_padding_inc = inst(dl.Logic, bits=1, name="credit_counter_lower_padding_inc")
					credit_counter_change_values = [1, mod.cred_dec_amount, mod.cred_dec_final_amount]
					if (mod.cred_padding[0] != 0):
						credit_counter_change_values.append(mod.cred_padding[0])
					if (mod.cred_padding[1] != 0):
						credit_counter_change_values.append(mod.cred_padding[1])
					#####################		End 		######################################
					self.credit_counter = inst(dl.MultiChangeCounter,
						reset_value=mod.cred_padding[0],
						change_values=credit_counter_change_values,
						end_value=mod.cred_max[self.name],
						name="credit_counter")
					
					print(self.module_name, " has a cred max of ", mod.cred_max[self.name], " and next layer is ", mod.module_name)
					print("credit padding is:", mod.cred_padding)
					self.credit_counter.change_signals[0].set_driver(self.cred_inc.delay(1))
					self.credit_counter.change_signals[1].set_driver(mod.cred_dec.delay(3))
					self.credit_counter.change_signals[2].set_driver(mod.cred_dec_final.delay(3))	
					self.can_write_line.set_driver(inst(dl.AND, inst(dl.NOT, self.credit_counter.is_done), upper_padding_done, lower_padding_done))

					######################   Padding Signals Part 2    #########################################
					if (mod.cred_padding[0] != 0):
						upper_padding_set = inst(dl.Logic, bits=1, name="upper_padding_set")
						upper_padding_unset = inst(dl.Logic, bits=1, name="upper_padding_unset")
						upper_padding_unset.set_driver(self.cred_dec_final.delay(1))
						upper_padding_set.set_driver(credit_counter_upper_padding_inc)
						upper_padding_done.set_driver(inst(dl.OR, inst(dl.AND, upper_padding_done, inst(dl.NOT, upper_padding_unset)), upper_padding_set))
						if (hasattr(mod, "cred_padding_unpause")):
							credit_counter_upper_padding_inc.set_driver(inst(dl.AND, mod.cred_padding_unpause.delay(3), lower_padding_done, inst(dl.NOT, upper_padding_done), inst(dl.LE, self.credit_counter.current_value, inst(dl.Constant, bits=clog2(mod.cred_max[self.name]), value=mod.cred_max[self.name]-mod.cred_padding[0]))))
						else:
							credit_counter_upper_padding_inc.set_driver(inst(dl.AND, lower_padding_done, inst(dl.NOT, upper_padding_done), inst(dl.LE, self.credit_counter.current_value, inst(dl.Constant, bits=clog2(mod.cred_max[self.name]), value=mod.cred_max[self.name]-mod.cred_padding[0]))))
						self.credit_counter.change_signals[3].set_driver(credit_counter_upper_padding_inc)
					else:
						upper_padding_done.set_driver(inst(dl.Constant, bits=1, value=1))
						credit_counter_upper_padding_inc.set_driver(inst(dl.Constant, bits=1, value=0))

					if (mod.cred_padding[1] != 0):
						lower_padding_set = inst(dl.Logic, bits=1, name="lower_padding_set")
						lower_padding_unset = inst(dl.Logic, bits=1, name="lower_padding_unset")
						lower_padding_unset.set_driver(self.cred_dec_final.delay(1))
						lower_padding_set.set_driver(credit_counter_lower_padding_inc)	
						lower_padding_done.set_driver(inst(dl.OR, inst(dl.AND, lower_padding_done, inst(dl.NOT, lower_padding_unset)), lower_padding_set))
						if (hasattr(mod, "cred_padding_unpause") and mod.cred_padding[0] == 0):
							credit_counter_lower_padding_inc.set_driver(inst(dl.AND, mod.cred_padding_unpause.delay(3), inst(dl.NOT, lower_padding_done), inst(dl.LE, self.credit_counter.current_value, inst(dl.Constant, bits=clog2(mod.cred_max[self.name]), value=mod.cred_max[self.name]-mod.cred_padding[1]))))						
						else:
							credit_counter_lower_padding_inc.set_driver(inst(dl.AND, inst(dl.NOT, lower_padding_done), inst(dl.LE, self.credit_counter.current_value, inst(dl.Constant, bits=clog2(mod.cred_max[self.name]), value=mod.cred_max[self.name]-mod.cred_padding[1]))))						
						if (mod.cred_padding[0] != 0):
							self.credit_counter.change_signals[4].set_driver(credit_counter_lower_padding_inc)
						else:
							self.credit_counter.change_signals[3].set_driver(credit_counter_lower_padding_inc)

					else:	
						lower_padding_done.set_driver(inst(dl.Constant, bits=1, value=1))
						credit_counter_lower_padding_inc.set_driver(inst(dl.Constant, bits=1, value=0))
					####################			End				##################################################




		else:
			#Non-buffered layers merely transfer signals back and forth. 
			#If a layer has more than 2 outputs, it has to merge their decrements signals. A line is cleared only when all output layers have cleared a line or more.
			if len(n.outputs) > 1 :
				################## Padding Signals ##########################
				upper_padding_done = [inst(dl.Flop, reset_driver=1, bits=1, name="upper_padding_done_"+str(i)) for i,o in enumerate(n.outputs)]
				lower_padding_done = [inst(dl.Flop, reset_driver=1, bits=1, name="lower_padding_done_"+str(i)) for i,o in enumerate(n.outputs)]
				credit_counter_upper_padding_inc = [inst(dl.Logic, bits=1, name="credit_counter_upper_padding_inc_"+str(i)) for i,o in enumerate(n.outputs)]
				credit_counter_lower_padding_inc = [inst(dl.Logic, bits=1, name="credit_counter_lower_padding_inc_"+str(i)) for i,o in enumerate(n.outputs)]
				credit_counter_change_values = [[-1, -o._to.module.cred_dec_amount, -o._to.module.cred_dec_final_amount] for o in n.outputs]
				for i,o in enumerate(n.outputs):
					mod = o._to.module
					if (self.cred_padding[0] - mod.cred_padding[0] != 0):
						credit_counter_change_values[i].append(self.cred_padding[0] - mod.cred_padding[0])
					if (self.cred_padding[1] - mod.cred_padding[1] != 0):
						credit_counter_change_values[i].append(self.cred_padding[1] - mod.cred_padding[1])
				###############################################################################

				min_cred_max = min([o._to.module.cred_max[self.name] for o in n.outputs])
				cred_max_diff =[o._to.module.cred_max[self.name] - min_cred_max for o in n.outputs]
				self.credit_counters = [inst(dl.MultiChangeCounter, reset_value= (cred_max_diff[i]+ self.cred_padding[0] - o._to.module.cred_padding[0]), change_values=credit_counter_change_values[i], end_value=o._to.module.cred_max[self.name] + cred_max_diff[i]) for i,o in enumerate(n.outputs)]	
				self.credit_counters_not_empty = [inst(dl.NOT, inst(dl.EQ, c.current_value, 0).delay(1)) for c in self.credit_counters]
				self.all_have_credit = inst(dl.AND, *self.credit_counters_not_empty)
				cred_dec_temp = inst(dl.Flop, reset_driver=0, bits=1, name="cred_dec_temp")
				cred_dec_temp.set_driver(inst(dl.AND, self.all_have_credit, inst(dl.NOT, cred_dec_temp), inst(dl.NOT, cred_dec_temp).delay(1)))
				for i,o in enumerate(n.outputs):
					mod = o._to.module	
					self.credit_counters[i].change_signals[0].set_driver(cred_dec_temp)
					self.credit_counters[i].change_signals[1].set_driver(mod.cred_dec.delay(3))
					self.credit_counters[i].change_signals[2].set_driver(mod.cred_dec_final.delay(3))
				self.cred_dec.set_driver(cred_dec_temp)
				self.cred_dec_final.set_driver(inst(dl.Constant, bits=1, value=0))		
				self.can_write_line.set_driver(inst(dl.Constant, bits=1, value=0)) #This doesn't really matter	

				####################################################################################
				upper_padding_set = ["" for o in n.outputs]
				upper_padding_unset = ["" for o in n.outputs]
				lower_padding_set = ["" for o in n.outputs]
				lower_padding_unset = ["" for o in n.outputs]

				hold_unpause = []
				hold_unpause_set = []
				hold_unpause_unset = []
				all_unpause = inst(dl.Logic, bits=1, name="all_unpause")


				for i,o in enumerate(n.outputs):
					mod = o._to.module	

					if hasattr(mod, "cred_padding_unpause"):
						hold_unpause.append(inst(dl.Flop, reset_driver=0, bits=1, name="hold_cred_inc_dec"))
						hold_unpause_set.append(inst(dl.Logic, bits=1, name="hold_cred_inc_set"))
						hold_unpause_unset.append(inst(dl.Logic, bits=1, name="hold_cred_inc_unset"))
						hold_unpause_set[-1].set_driver(mod.cred_padding_unpause.delay(3))
						hold_unpause_unset[-1].set_driver(all_unpause.delay(1))
						hold_unpause[-1].set_driver(inst(dl.OR, inst(dl.AND, hold_unpause[-1], inst(dl.NOT, hold_unpause_unset[-1])), hold_unpause_set[-1]))

					if (self.cred_padding[0] - mod.cred_padding[0] != 0):
						upper_padding_set[i] = inst(dl.Logic, bits=1, name="upper_padding_set_"+str(i))
						upper_padding_unset[i] = inst(dl.Logic, bits=1, name="upper_padding_unset_"+str(i))
						upper_padding_unset[i].set_driver(mod.cred_dec_final.delay(3))
						upper_padding_set[i].set_driver(credit_counter_upper_padding_inc[i])
						upper_padding_done[i].set_driver(inst(dl.OR, inst(dl.AND, upper_padding_done[i], inst(dl.NOT, upper_padding_unset[i])), upper_padding_set[i]))
						credit_counter_upper_padding_inc[i].set_driver(inst(dl.AND, lower_padding_done[i], inst(dl.NOT, upper_padding_done[i]), inst(dl.LE, self.credit_counters[i].current_value, inst(dl.Constant, bits=clog2(mod.cred_max[self.name]), value=mod.cred_max[self.name]-(self.cred_padding[0]-mod.cred_padding[0])))))
						self.credit_counters[i].change_signals[3].set_driver(credit_counter_upper_padding_inc[i])
					else:
						upper_padding_done[i].set_driver(inst(dl.Constant, bits=1, value=1))
						credit_counter_upper_padding_inc[i].set_driver(inst(dl.Constant, bits=1, value=0))

					if (self.cred_padding[1] - mod.cred_padding[1] != 0):
						lower_padding_set[i] = inst(dl.Logic, bits=1, name="lower_padding_set_"+str(i))
						lower_padding_unset[i] = inst(dl.Logic, bits=1, name="lower_padding_unset_"+str(i))
						lower_padding_unset[i].set_driver(mod.cred_dec_final.delay(3))
						lower_padding_set[i].set_driver(credit_counter_lower_padding_inc[i])	
						lower_padding_done[i].set_driver(inst(dl.OR, inst(dl.AND, lower_padding_done[i], inst(dl.NOT, lower_padding_unset[i])), lower_padding_set[i]))
						credit_counter_lower_padding_inc[i].set_driver(inst(dl.AND, inst(dl.NOT, lower_padding_done[i]), inst(dl.LE, self.credit_counters[i].current_value, inst(dl.Constant, bits=clog2(mod.cred_max[self.name]), value=mod.cred_max[self.name]-(self.cred_padding[1]-mod.cred_padding[1])))))						
						if (self.cred_padding[0] - mod.cred_padding[0] != 0):
							self.credit_counters[i].change_signals[4].set_driver(credit_counter_lower_padding_inc[i])
						else:
							self.credit_counters[i].change_signals[3].set_driver(credit_counter_lower_padding_inc[i])

					else:	
						lower_padding_done[i].set_driver(inst(dl.Constant, bits=1, value=1))
						credit_counter_lower_padding_inc[i].set_driver(inst(dl.Constant, bits=1, value=0))
				
				if (hasattr(self, "cred_padding_unpause")):
					if len(hold_unpause) > 1:
						all_unpause.set_driver(inst(dl.AND, *hold_unpause))
					else:
						all_unpause.set_driver(hold_unpause[0])
					self.cred_padding_unpause.set_driver(all_unpause)
				#########################################################################################	


			elif len(n.outputs) == 1:
				mod = n.outputs[0]._to.module
				self.cred_dec.set_driver(mod.cred_dec.delay(3))
				self.cred_dec_final.set_driver(mod.cred_dec_final.delay(3))
				self.can_write_line.set_driver(inst(dl.Constant, bits=1, value=0))
				if (hasattr(mod, "cred_padding_unpause")):
					self.cred_padding_unpause.set_driver(mod.cred_padding_unpause)


	def get_test_module_string(self, t="  "):
		filehandles = [n.module.name + "_fh" for n in self.hpipe.nodes]
		verilog = "  integer " + filehandles[0]
		for fh in filehandles[1:]:
			verilog += ", " + fh
		verilog += ";\n"
		verilog += """
  initial begin
`ifndef VERILATOR
`ifndef DONTDUMP
  	$vcdplusfile("dump.vpd");
    $vcdpluson(0, hpipe_tb_0);
`endif
`endif
"""
		
		for fh,n in zip(filehandles, self.hpipe.nodes):
			verilog += '    ' + fh + ' = $fopen("generated_files/layer_images/verilog/' + fh[:-3] + '.csv", "w");\n'
			verilog += '    $fwrite(' + fh + ', "' + str(n.example_outputs[0].shape[-1]) + '\\n");\n'
		verilog += """  end
  reg [20:0] finish_counter;
  always @(posedge clock_0) begin
  	if (reset) begin
  		finish_counter <= '0;
  	end else begin
	  	finish_counter <= finish_counter + 1;
  		if (finish_counter == 200000) begin
"""
		for fh in filehandles:
			verilog += '  			$fclose(' + fh + ');\n'
		verilog += """
`ifndef VERILATOR
  			$finish;
`endif
  		end
  	end
  end
"""
		verilog += "  always @(posedge clock_0) begin\n"
		for fh,v,os in zip(filehandles, self.all_valids, self.all_outputs):
			verilog += "    if (" + v.name + ") begin\n"
			verilog += '      $fwrite(' + fh + ', "' + ",".join(['%d'] * len(os)) + '\\n", ' + ", ".join([d.name for d in os]) + " );\n"
			verilog += "    end\n"
		#for i,god in enumerate(self.golden_output_data):
		#	verilog += "      assert("+god.name+" == " + self.dsp_outputs[i].name + ");\n"
		verilog += "  end\n"
		return verilog


class Mean(NNStage):
	kind = "mean"
	def instantiate(self, node, inst, build_data_path=True, **kwargs):

		bits, _, output_bits = self.get_bit_widths()
		int_bits, _, _ = self.get_int_part_widths()
		frac_bits, _, output_frac_bits = self.get_frac_part_widths()
		signed, _, output_signed = self.get_signs()
		input_channels = self.get_input_act(dim="channels")
		input_height = self.get_input_act(dim="height")
		input_width = self.get_input_act(dim="width")
		self.buffered_layer = True

		read_delay = 2
		reduction_stages = max(clog2(input_width), read_delay)
		mul_stages = 4
		mux_and_add_delay = 1
		constant_frac_bits = 26

		memory_width = min(40, bits + reduction_stages + input_height)

		piped_inputs = [i.delay(reduction_stages) for i in self.inputs]
		reduction_tree = inst(dl.ReduceAdd, *piped_inputs)
		in_mul_value = get_quantized(np.array([1. / float(input_width)]), int(signed), int_bits + reduction_stages, frac_bits)[0]
		in_mul_constant = inst(dl.Constant, value=in_mul_value, signed=signed, bits=bits+reduction_stages, name="in_mul_constant")
		out_mul_value = get_quantized(np.array([1. / float(input_width*input_height)]), 1, 0, constant_frac_bits)[0]
		out_mul_constant = inst(dl.Constant, value=out_mul_value, signed=True, bits=27, name="out_mul_constant")

		#with dl.ParamScope(name="input_mul_out", signed=signed):
		#	input_mul_out = inst(dl.MUL, reduction_tree.output.delay(mul_stages), in_mul_constant.delay(mul_stages), name="input_multiplier")[bits + reduction_stages + frac_bits - 1: frac_bits]

		ram = inst(dl.RAM, bits=memory_width, depth=input_channels)
		ram.r_en.set_driver(self.write.delay(reduction_stages - read_delay))
		read_data = inst(dl.Flop, bits=memory_width, signed=signed, driver=ram.r_data)
		channel_counter = inst(dl.Counter, reset_value=0, end_value=input_channels-1, increment_value=1, name="channel_counter")
		channel_counter.set_increment_condition(ram.r_en)
		ram.r_addr.set_driver(channel_counter.current_value)
		input_line_counter = inst(dl.Counter, reset_value=0, end_value=input_height-1, increment_value=1, name="input_line_counter")
		input_line_counter.set_increment_condition(channel_counter.done_and_incrementing)
		to_add = inst(dl.MuxV2, input_line_counter.is_reset_value.delay(read_delay+mux_and_add_delay), read_data.delay(mux_and_add_delay), inst(dl.Constant, value=0, bits=memory_width).delay(mux_and_add_delay), signed=signed)
		with dl.ParamScope(name="fully_reduced"):
			fully_reduced = (to_add + reduction_tree.output.delay(mux_and_add_delay))[memory_width-1:0]

		input_frac_bits, _, desired_fractional_bits = self.get_frac_part_widths()
		_, _, output_int_bits = self.get_int_part_widths()
		fractional_bits_in_output = input_frac_bits
		lo = fractional_bits_in_output - desired_fractional_bits
		hi = fractional_bits_in_output + output_int_bits - 1

		ram.w_data.set_driver(fully_reduced)
		ram.w_addr.set_driver(channel_counter.current_value.delay(read_delay + mux_and_add_delay))
		ram.w_en.set_driver(ram.r_en.delay(read_delay + mux_and_add_delay))

		with dl.ParamScope(name="output_mul_out", signed=signed):
			#output_mul_out = inst(dl.MUL, fully_reduced.delay(mul_stages), out_mul_constant.delay(mul_stages), name="output_multiplier")[output_bits + frac_bits-output_frac_bits+27 - 1: frac_bits-output_frac_bits+27]
			dsp = inst(dl.MultiplyAccumulate,
				[s._bit_width for s in [fully_reduced, out_mul_constant]], fully_reduced._bit_width+constant_frac_bits, 2, name="output_multiplier")
			
			dsp.inputs[0].set_driver(fully_reduced)
			dsp.inputs[1].set_driver(out_mul_constant)
			output_mul_out = dsp.output[output_bits + frac_bits-output_frac_bits+constant_frac_bits - 1: frac_bits-output_frac_bits+constant_frac_bits]

		self.space_to_write_line.set_driver(inst(dl.NOT, inst(dl.AND, inst(dl.EQ, input_line_counter.current_value, max(0, input_height-2)), inst(dl.NOT, self.can_write_line))))
		
		#If we are using the Stratix 10 and out multiplier is greater than 8bits*8bits we need to use an IP which will take multiple cycles
		mul_stages = dsp.mul_stages

		self.outputs_valid[0].set_driver(inst(dl.AND, input_line_counter.is_done.delay(read_delay + mux_and_add_delay), ram.w_en).delay(mul_stages))
		self.output_valid.set_driver(self.outputs_valid[0])
		self.outputs[0].set_driver(output_mul_out)
		self._set_control_valid_to_output_valid_delay(0)
		self._set_control_to_input_delay(0)

		self.cred_inc.set_driver(inst(dl.AND, channel_counter.is_reset_value.delay(1), ram.r_en.delay(1), name="channel_counter_zero_and_inc"))
		self.cred_dec.set_driver(input_line_counter.done_and_incrementing)
		self.cred_dec_final.set_driver(input_line_counter.done_and_incrementing)
		self.cred_dec_amount = -input_height
		self.cred_dec_final_amount = 0
		if node.inputs[0]._from.module is not None:
			self.cred_max[node.inputs[0]._from.module.name] = input_height
		else:
			self.cred_max["None"] = input_height


class Flatten(NNStage):
	"""
		This class corresponds to the tensorflow Flatten layer that serializes the input in the height and width dimensions
		(e.g. from 7x7x512 to 1x1x25088 in VGG-16)

		HPIPE operates on the NHWC tensorflow data format (Number of images - height - width - channels), and Flatten iterates over the dimensions from right to left
		
		In the 7x7x512 example given above, assuming lines 3 and 4 are in the input buffer (LINES_TO_STORE=2),
		Flatten's input buffer contains the following data (format is h,w,c):

		-------------------------------------------------------------------------------------
		|	3,6,0	|	3,5,0	|	3,4,0	|	3,3,0	|	3,2,0	|	3,1,0	|	3,0,0	|
		|	3,6,1	|	3,5,1	|	3,4,1	|	3,3,1	|	3,2,1	|	3,1,1	|	3,0,1	|
												  .	
												  .	
												  .	
		|	3,6,511	|	3,5,511	|	3,4,511	|	3,3,511	|	3,2,511	|	3,1,511	|	3,0,511	|
		|	4,6,0	|	4,5,0	|	4,4,0	|	4,3,0	|	4,2,0	|	4,1,0	|	4,0,0	|
		|	4,6,1	|	4,5,1	|	4,4,1	|	4,3,1	|	4,2,1	|	4,1,1	|	4,0,1	|
												  .	
												  .	
												  .	
		|	4,6,511	|	4,5,511	|	4,4,511	|	4,3,511	|	4,2,511	|	4,1,511	|	4,0,511	|
		-------------------------------------------------------------------------------------

		The Flatten layer first iterates over the right-most column of the first row in the buffer (3,0,0 -> 3,0,511), followed by the column immediately to its left (3,1,0 -> 3,1,511),
		and repeats until the row is complete, and it can then be dropped. The second row can then be processed, with the deletion of the previous row making 
		room for a new row to come in.

		Outputs one value per cycle (output width=1).
	
	"""
	kind = "flatten"
	def instantiate(self, node, inst, build_data_path=True, **kwargs):

		bits, _, output_bits = self.get_bit_widths()
		int_bits, _, output_int_bits = self.get_int_part_widths()
		frac_bits, _, output_frac_bits = self.get_frac_part_widths()
		signed, _, output_signed = self.get_signs()
		input_channels = self.get_input_act(dim="channels")
		input_height = self.get_input_act(dim="height")
		input_width = self.get_input_act(dim="width")
		

		self.buffered_layer = True

		LINES_TO_STORE = 2
		cast_inputs = [cast_fixed(i, self.get_bit_spec("input"), self.get_bit_spec("output")) for i in self.inputs]
		concatinated_inputs = inst(dl.Concat, *cast_inputs, name="concatinated_inputs")
		ram_width = input_width * output_bits
		assert(ram_width == concatinated_inputs._bit_width)
		ram_depth = input_channels * LINES_TO_STORE

		input_delay = 1
		ram = inst(dl.RAM, bits=ram_width, depth=ram_depth)
		ram.w_en.set_driver(self.write.delay(input_delay))
		ram.w_data.set_driver(concatinated_inputs.delay(input_delay))
		input_channel_counter = inst(dl.Counter, reset_value=0, end_value=(input_channels)-1, increment_value=1, name="input_channel_counter")
		input_channel_counter.set_increment_condition(self.write)
		input_line_counter = inst(dl.Counter, reset_value=0, end_value=LINES_TO_STORE-1, increment_value=1, name="input_line_counter")
		input_line_counter.set_increment_condition(input_channel_counter.done_and_incrementing)
		w_addr_driver = inst(dl.Logic, bits=clog2(ram_depth), name="w_addr_driver")
		ram_depth_increments = [inst(dl.Constant, bits=clog2(ram_depth), value=(input_channels)*i, name="line_"+str(i)+"_base") for i in range(LINES_TO_STORE)]
		w_addr_driver.set_driver(inst(dl.ADD, inst(dl.MuxV2, input_line_counter.current_value, *ram_depth_increments), input_channel_counter.current_value).delay(input_delay))
		ram.w_addr.set_driver(w_addr_driver)

		width_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=input_width-1, name="width_counter")
		output_height_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=input_height-1, name="height_counter")
		output_channel_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=input_channels-1, name="output_channel_counter")

		width_counter.set_increment_condition(output_channel_counter.done_and_incrementing)
		output_height_counter.set_increment_condition(width_counter.done_and_incrementing)

		# This doesn't count the output lines, rather it counts the input lines but when reading for the output.
		output_line_counter = inst(dl.Counter, reset_value=0, end_value=LINES_TO_STORE-1, increment_value=1, name="output_line_counter") 
		output_line_counter.set_increment_condition(width_counter.done_and_incrementing)

		lines_used_counter = inst(dl.MultiChangeCounter,
				reset_value=0,
				change_values=[
					1,
					-1],
				end_value=LINES_TO_STORE,
				name="lines_used_counter")
		
		lines_used_counter.change_signals[0].set_driver(input_channel_counter.done_and_incrementing)
		lines_used_counter.change_signals[1].set_driver(width_counter.done_and_incrementing)
		
		self.space_to_write_line = inst(dl.NOT, lines_used_counter.is_done) #Only for unit tests
		self.has_line = inst(dl.NOT, lines_used_counter.is_reset_value, name="has_line")

		#If we are at the start of an image (which is equivalent to 1 output line because we're flattening it), then check we have a line and we can write line
		# else if we are at the start of input line but not the start of a new image, we know next layer has space since we already started writing, so only check if we have line
		# else write for sure because we already started processing an input line
		start_writing = inst(dl.MuxV2, inst(dl.AND, output_height_counter.is_reset_value, width_counter.is_reset_value, output_channel_counter.is_reset_value), 
					   inst(dl.MuxV2, inst(dl.AND, output_channel_counter.is_reset_value, width_counter.is_reset_value), inst(dl.Constant, bits=1, value=1, name="cst_1_write"), self.has_line), 
					   inst(dl.AND, self.has_line, self.can_write_line, inst(dl.NOT, self.output_valid.delay(15)))) #Just add an arbitrary pause between different output lines
		ram.r_en.set_driver(start_writing)
		output_channel_counter.set_increment_condition(start_writing)
		r_addr_driver = inst(dl.Logic, bits=clog2(ram_depth), name="r_addr_driver")
		r_addr_driver.set_driver(inst(dl.ADD, inst(dl.MuxV2, output_line_counter.current_value, *ram_depth_increments), output_channel_counter.current_value))
		ram.r_addr.set_driver(r_addr_driver)

		READ_DELAY = 1
		OUTPUT_DELAY = 2
		output_fractions = [ram.r_data[((i+1)*8) - 1:i*8] for i in range(input_width)]
		selected_output = inst(dl.MuxV2, width_counter.current_value.delay(READ_DELAY), *output_fractions, name="selected_output")
		self.outputs[0].set_driver(selected_output.delay(OUTPUT_DELAY))
		self.outputs_valid[0].set_driver(start_writing.delay(READ_DELAY + OUTPUT_DELAY))
		self.output_valid.set_driver(start_writing.delay(READ_DELAY + OUTPUT_DELAY))
		self._set_control_valid_to_output_valid_delay(0)
		self._set_control_to_input_delay(0)


		self.cred_inc.set_driver(inst(dl.AND, start_writing, inst(dl.NOT, start_writing.delay(1)), output_height_counter.is_reset_value))
		self.cred_dec.set_driver(width_counter.done_and_incrementing)
		self.cred_dec_final.set_driver(width_counter.done_and_incrementing)
		self.cred_dec_amount = -1
		self.cred_dec_final_amount = 0
		if node.inputs[0]._from.module is not None:
			self.cred_max[node.inputs[0]._from.module.name] = LINES_TO_STORE
		else:
			self.cred_max["None"] = LINES_TO_STORE

class Add(NNStage):
	kind =  "add"
	def build_controller(self, inst, input_channels):
		control_to_data_path_delay = 3
		self.done_line = inst(dl.Logic, 1, name="done_line")
		self._done_line = inst(dl.Logic, 1, name="_done_line")
		states = ["IDLE", "READING_LINE"]
		edges = [
			["IDLE", "READING_LINE", self.go],
			["READING_LINE", "IDLE", self._done_line]
		]
		self.state_machine = inst(dl.StateMachine, states, edges, None)
		in_channel_counter = inst(dl.Counter,
			reset_value=0,
			increment_value=1,
			end_value=input_channels-1,
			name="in_channel_counter")
		in_channel_counter.set_increment_condition(self.state_machine.is_reading_line)

		self._done_line.set_driver(in_channel_counter.is_done)
		self.done_line.set_driver(self._done_line.delay(control_to_data_path_delay))
		self.is_reading_line = self.state_machine.is_reading_line.delay(control_to_data_path_delay)

	def instantiate(self, node, inst, build_data_path=True, **kwargs):

		parallel_acts_per_ram = node.inputs[0]._from.get_parallel_acts_per_memory()

		input_channels = self.get_input_act(dim="channels")
		input_height = self.get_input_act(dim="height")
		input_width = self.get_input_act(dim="width")
		self.buffered_layer = True

		kernel_height = 1
		kernel_width = 1
		padding = [0,0]
		vertical_stride = 1
		horizontal_stride = 1

		bits, _, output_bits = self.get_bit_widths()
		signed, _, output_signed = self.get_signs()


		output_width = self.get_output_act(dim="width")
		output_height = self.get_output_act(dim="height")

		pre_modulo_addr_add_delay = 2
		address_computation_delay = pre_modulo_addr_add_delay
		memory_read_delay         = 2
		memory_model_delay        = 1
		added_read_delay          = memory_read_delay - memory_model_delay
		sr_write_delay            = 1
		single_addr_delay         = kernel_width
		single_pool_delay         = kernel_width * kernel_height

		self.go = inst(dl.Logic, 1, name="go")
		self.build_controller(inst, input_channels)

		self.ibcs = []
		self.ia_buffers = []
		self.read_data_groups = []
		have_full_lines = []
		per_ram_fanout_stages_list = []
		per_ram_write_stages_list = []
		is_reading_line_signals = []
		input_module_delayed_writes = []
		control_to_input_delays = []
		for i in range(len(node.inputs)):
			bits, _, output_bits = self.get_bit_widths(i)
			is_reading_line_input = inst(dl.Logic, bits=1, signed=False, name="is_reading_line_input_" + str(i))
			is_reading_line_signals.append(is_reading_line_input)
			input_module_delayed_write = inst(dl.Logic, bits=1, signed=False, name="input_module_delayed_write_" + str(i))
			input_module_delayed_writes.append(input_module_delayed_write)

			min_buffer_depth = max(node.planner_estimate.buffer_for_input[i], 2)

			input_channels = self.get_input_act(dim="channels", input_index=i)
			print("  Minimum buffer depth for input " + str(i) + ": " + str(min_buffer_depth))
			print("  Number of input channels for input " + str(i) + ": " + str(input_channels))

			#self, stride, kernel_height, channels, activation_height, padding, n_channel_splits, output_activation_height, min_buffer_depth=1, **kwargs):
			ibc = inst(InputBufferController,
				vertical_stride,
				kernel_height,
				input_channels,
				input_height,
				padding,
				1,
				output_height,
				min_buffer_depth=min_buffer_depth)
			self.input_modules[i].space_to_write_line.set_driver(ibc.space_to_write_line.delay(1))
			ibc.done_image.set_driver(self.done)
			ibc.write.set_driver(input_module_delayed_write)
			have_full_lines.append(ibc.has_full_kernel)
			ibc.finished_reading_line.set_driver(self.done_line)
			ibc.started_reading_line.set_driver(inst(dl.AND, self.go, self.state_machine.is_idle))
			self.ibcs.append(ibc)
			ia_buffer = inst(dl.FanoutRAM,
				bits * input_width,
				min_buffer_depth * input_channels)
			control_to_input_delays.append(ibc.write_counter_increment_delay)
			self.ia_buffers.append(ia_buffer)
			per_ram_fanout_stages_list.append(ia_buffer.read_delay)
			read_data_list = []
			w_data = inst(dl.Concat, *self.input_modules[i].inputs)
			ia_buffer.w_data.set_driver(w_data)
			select_base = 0			
			for j in self.input_modules[i].inputs:
				read_data_list.append(ia_buffer.r_data[select_base + j._bit_width - 1 : select_base])
				select_base += j._bit_width
			self.read_data_groups.append(read_data_list)

			addr_counter = inst(dl.Counter,
				reset_value=0,
				increment_value=1,
				end_value=min_buffer_depth*input_channels-1,
				name="addr_counter_" + str(i))
			addr_counter.set_increment_condition(is_reading_line_input)

			ia_buffer.w_en.set_driver(ibc.write_outs[0])
			ia_buffer.w_addr.set_driver(ibc.write_address)
			ia_buffer.r_en.set_driver(is_reading_line_input)
			ia_buffer.r_addr.set_driver(addr_counter.current_value)

		max_control_to_input_delay = max(control_to_input_delays)
		max_per_ram_fanout_delay = max(per_ram_fanout_stages_list)
		for s,d in zip(is_reading_line_signals, per_ram_fanout_stages_list):
			s.set_driver(self.is_reading_line, delay=max_per_ram_fanout_delay-d)
		for im, imdw, d in zip(self.input_modules, input_module_delayed_writes, control_to_input_delays):
			imdw.set_driver(im.write.delay(max_control_to_input_delay-d))

		input_frac_bits, _, desired_fractional_bits = self.get_frac_part_widths()
		_, _, output_int_bits = self.get_int_part_widths()
		fractional_bits_in_output = input_frac_bits
		lo = fractional_bits_in_output - desired_fractional_bits
		hi = fractional_bits_in_output + output_int_bits - 1
		fractional_padding = 0
		if output_signed:
			hi += 1
		if lo < 0:
			fractional_padding = -lo
			lo = 0

		add_trees = []
		pipe_stages = 3
		output_valid_fanout = self.is_reading_line.delay(max_per_ram_fanout_delay+pipe_stages, max_fan=4, final_fan=output_width)
		for i,inputs in enumerate(zip(*self.read_data_groups)):
			if build_data_path:
				ram_index = i // parallel_acts_per_ram

				to_reduce = [p.delay(pipe_stages) for p in inputs]
				for tr in to_reduce:
					tr.signed = signed
				add_trees.append(inst(dl.ReduceAdd, *to_reduce))
				with dl.ModuleScope(add_trees[-1]):
					with dl.ParamScope(name="output_precision"):
						max_input_int_bits = max([(self.get_bit_spec("input", i))['int'] for i in range(len(to_reduce))])
						input_bit_spec_list = [(self.get_bit_spec("input", i)) for i in range(len(to_reduce))]
						input_bit_spec = next((bit_spec_item for bit_spec_item in input_bit_spec_list if bit_spec_item['int'] == max_input_int_bits), None)
						#Adding the input with another value will increase the int field by 1 (technically clog2(len(to_reduce)) but I'm not sure we ever have more than 2)
						input_bit_spec['int'] += 1 
						input_bit_spec['width'] += 1
						# If target_act_bits and target_bias_and_act_func_bits are different (i.e. 8 and 16 respectively), then the output of the add tree might not 
						#     match the required output bit spec of the layer, and we need to cast it to the required bit spec  
						if (self.get_bit_spec("output")['width'] > input_bit_spec['width']):
							#At this point, input_bit_spec is the spec that the output of the adder tree will have. We must then cast it to the layer's output bit spec
							extended_output = cast_fixed(add_trees[-1].output, input_bit_spec, self.get_bit_spec("output"))
							assert(self.get_bit_spec("output")["frac"] - input_bit_spec["frac"] >= 0)
							delta = self.get_bit_spec("output")["frac"] - input_bit_spec["frac"]
						else:
							extended_output = add_trees[-1].output
							delta = 0
						output = extended_output[(hi+delta):(lo+delta)]
						if fractional_padding > 0:
							output = inst(dl.Concat, inst(dl.Constant, bits=fractional_padding, value=0), output)
						self.outputs[i].set_driver(output)
			self.outputs_valid[i].set_driver(output_valid_fanout)
		self.output_valid.set_driver(self.is_reading_line)
		self.go.set_driver(inst(dl.AND, *have_full_lines, self.can_write_line))
		ibc_is_idle = [inst(dl.Logic, bits=1, name="ibc_is_idle") for ibc in self.ibcs]
		for i,ibc in zip(ibc_is_idle, self.ibcs):
			i.set_driver(ibc.state_machine.c["is_idle"])
		self.debug_stall = inst(dl.OR, *[inst(dl.AND,
			idle,
			inst(dl.NOT, self.is_reading_line),
			inst(dl.NOT, dl.Verilog.current_params.kwargs["reset"]),
			inst(dl.NOT, i.space_to_write_line),
			inst(dl.NOT, h)) for idle,i,h in zip(ibc_is_idle, self.input_modules, have_full_lines)])
		self._set_control_to_input_delay(max_control_to_input_delay)
		self._set_control_valid_to_output_valid_delay(pipe_stages + max_per_ram_fanout_delay)

		self.cred_inc.set_driver(inst(dl.AND, self.state_machine.is_idle.delay(2), inst(dl.NOT, self.state_machine.is_idle.delay(1))))
		self.cred_dec.set_driver(self.ibcs[0].finished_reading_line.delay(2))
		self.cred_dec_final.set_driver(self.ibcs[0].done_image.delay(2))
		self.cred_dec_amount = -vertical_stride
		self.cred_dec_final_amount = self.ibcs[0].cred_dec_final_amount

		for i in range(len(node.inputs)):
			if node.inputs[i]._from.module is not None:
				self.cred_max[node.inputs[i]._from.module.name] = self.ibcs[i].cred_max
			else:
				self.cred_max["None"] = self.ibcs[i].cred_max


	def get_custom_verilog_str(self, t="  "):
		verilog = super(Add, self).get_custom_verilog_str(t)
		'''
		verilog += "  always @(posedge clock_0) begin\n"
		verilog += "    if (" + self.debug_stall.name + ") begin\n"
		verilog += '      $display("' + self.name + ' deadlocked because there is no space and it does not have enough lines to write one out %0t", $time);\n'
		verilog += "    end\n"
		#verilog += "    if (!" + self.can_write_line.name + ") begin\n"
		#verilog += '      $display("' + self.name + ' cannot write");\n'
		#verilog += "    end\n"
		verilog += "  end\n"'''
		return verilog

class InputChannelBufferWriteController(dl.Module):
	def __init__(self, stride, kernel_height, channels, min_buffer_depth=1, **kwargs):
		super(InputChannelBufferWriteController, self).__init__("input_channel_buffer_write_controller", kind="input_channel_buffer_write_controller", **kwargs)
		inst = self.inst
		with dl.ModuleScope(self):
			self.write = inst(dl.Logic, 1, name="write")
			depth = (stride + kernel_height) * channels
			depth = max(depth, min_buffer_depth * channels)
			self.addr_counter = inst(dl.Counter,
				reset_value=0,
				increment_value=1,
				end_value=depth-1)
			self.address = inst(dl.Logic,
				self.addr_counter.current_value._bit_width,
				self.addr_counter.current_value,
				name="address")
			self.addr_counter.set_increment_condition(self.write)
			self.requires_driver = [self.write]

class InputBufferController(dl.Module):
	"""
	InputBufferController (IBC)

	Background: The IBC controls writing to InputActivationBuffer modules (IABs).  The IAB
	            typically buffers kernel height (Kh) plus vertical stride (Sv) lines, but
	            the number of lines in the buffer can be overwritten by setting min_buffer_depth 
	            to something greater than stride + kernel_height.

	Components:
		input_channel_counter
	"""
	def __init__(self, stride, kernel_height, channels, activation_height, padding, n_channel_splits, output_activation_height, min_buffer_depth=1, **kwargs):
		super(InputBufferController, self).__init__("input_buffer_controller", kind="input_buffer_controller", **kwargs)
		inst = self.inst

		# store the input params so that we can dump them in the instance comment
		self.params = {
			"stride" : stride,
			"kernel_height" : kernel_height,
			"channels" : channels,
			"activation_height" : activation_height,
			"padding" : padding,
			"n_channel_splits" : n_channel_splits,
			"min_buffer_depth" : min_buffer_depth}


		with dl.ModuleScope(self):
			self.write = inst(dl.Logic, 1, name="write")
			self.finished_reading_line = inst(dl.Logic, 1, name="finished_reading_line")
			self.started_reading_line = inst(dl.Logic, 1, name="started_reading_line")
			self.space_to_write_line = inst(dl.Logic, 1, name="space_to_write_line")
			self.write_outs = [inst(dl.Logic, 1, name="write_out_" + str(i)) for i in range(n_channel_splits)]
			self.write_address = inst(dl.Logic, None, name="write_address")
			self.done_image = inst(dl.Logic, 1, name="done_image")
			self.increment_write_condition = inst(dl.Logic, 1, name="increment_write_condition")
			self.increment_write_condition.set_driver(inst(dl.Constant, value=1, bits=1, signed=False, name="default_increment_write_condition"))
			self.increment_lines_used = inst(dl.Logic, 1, name="increment_lines_used")
			
			n_channels = [math.ceil(channels / float(n_channel_splits)) for _ in range(channels % n_channel_splits)]
			n_channels += [channels // n_channel_splits for _ in range(n_channel_splits - (channels % n_channel_splits))]
			self.write_controllers = [inst(InputChannelBufferWriteController, stride, kernel_height, n, min_buffer_depth=min_buffer_depth) for n in n_channels]
			self.has_space_to_write = inst(dl.Logic, 1, name="has_space_to_write")
			self.input_channel_counter = inst(dl.Counter,
				reset_value=0,
				increment_value=1,
				end_value=channels-1,
				name="input_channel_counter")
			
			input_line_counter_end_val = int((activation_height + np.sum(padding)-1))
			
			self.input_line_counter = inst(dl.Counter,
				reset_value=0,
				increment_value=1,
				end_value=input_line_counter_end_val,
				name="input_line_counter")
			self.input_line_counter_next = inst(dl.Counter,
				reset_value=0,
				increment_value=1,
				end_value=input_line_counter_end_val,
				name="input_line_counter_next")

			total_lines_written = max(np.sum(padding),0) + activation_height
			lines_explicitly_read = stride * output_activation_height
			final_diff_value = lines_explicitly_read - total_lines_written
			print("Total lines written: ", total_lines_written, ", Lines explicitly read: ", lines_explicitly_read)
			done_line_decrement_value = -stride
			
			counter_end_value = max(kernel_height+stride, min_buffer_depth)
			
			self.lines_used_counter = inst(dl.MultiChangeCounter,
				reset_value=0,
				change_values=[
					1,
					done_line_decrement_value,
					final_diff_value],
				end_value=counter_end_value,
				name="lines_used_counter")
			self.lines_used_counter.change_signals[1].set_driver(self.finished_reading_line)
			self.lines_used_counter.change_signals[2].set_driver(self.done_image)
			self.lines_complete_counter = inst(dl.MultiChangeCounter,
				reset_value=0,
				change_values=[
					1,
					done_line_decrement_value,
					final_diff_value],
				end_value=counter_end_value,
				name="lines_complete_counter")
			with dl.ModuleScope(self.lines_complete_counter):
				started_reading_line_counter = inst(dl.Counter,
					reset_value=0,
					increment_value=1,
					end_value=output_activation_height-1,
					name="started_reading_line_counter")
				started_reading_line_counter.set_increment_condition(self.started_reading_line)
			self.lines_complete_counter.change_signals[1].set_driver(self.started_reading_line)
			self.lines_complete_counter.change_signals[2].set_driver(started_reading_line_counter.done_and_incrementing)
			
			
			#For debugging only, shouldn't be used for actual control logic as it may slow things down
			self.state_machine_idle = inst(dl.Logic, 1, name="state_machine_idle")
						
			with dl.ModuleScope(self.lines_used_counter):			
				self.has_space_to_write =  inst(dl.NOT, self.lines_used_counter.is_done, name="has_space_to_write")
			with dl.ModuleScope(self.lines_complete_counter):
				if (stride > kernel_height):
					self.has_full_kernel = self.lines_complete_counter.current_value >= inst(dl.Constant, stride, bits=self.lines_used_counter.current_value._bit_width, name="stride")
				else:
					self.has_full_kernel = self.lines_complete_counter.current_value >= inst(dl.Constant, kernel_height, bits=self.lines_used_counter.current_value._bit_width, name="kernel_height")
				self.has_full_kernel = self.has_full_kernel.delay(4)
			self.has_padding = padding[0] != 0 or padding[1] != 0
			if self.has_padding:
				with dl.ModuleScope(self.input_line_counter):				
					if padding[0] > 0:
						padding_lower_bound = inst(dl.Constant, padding[0], signed=False, bits=self.input_line_counter.current_value._bit_width, name="padding_lower_bound")
						in_lower_padding = self.input_line_counter.current_value < padding_lower_bound
						in_lower_padding_next = self.input_line_counter_next.current_value < padding_lower_bound
					else:
						in_lower_padding = inst(dl.Constant, bits=1, value=0, name="in_lower_padding")
						in_lower_padding_next = inst(dl.Constant, bits=1, value=0, name="in_lower_padding_next")
					if padding[1] > 0:
						padding_upper_start = inst(dl.Constant, padding[0]+activation_height, signed=False, bits=self.input_line_counter.current_value._bit_width, name="padding_upper_start")
						in_upper_padding = self.input_line_counter.current_value >= padding_upper_start
						in_upper_padding_next = self.input_line_counter_next.current_value >= padding_upper_start
					else:
						in_upper_padding = inst(dl.Constant, bits=1, value=0, name="in_upper_padding")
						in_upper_padding_next = inst(dl.Constant, bits=1, value=0, name="in_upper_padding_next")
						
									
					self.in_padding_no_delay = inst(dl.OR, in_lower_padding, in_upper_padding, name="in_padding")
					self.in_padding_next_no_delay = inst(dl.OR, in_lower_padding_next, in_upper_padding_next, name="in_padding_next")
					
					self.in_padding = inst(dl.Flop, bits=1, driver=self.in_padding_no_delay, name="in_padding_d1")
					self.in_padding_next = inst(dl.Flop, bits=1, driver=self.in_padding_next_no_delay, name="in_padding_next_d1")
					
				#Marius: need to add corner case where we are doing our padding write soon but the buffer is almost full. Otherwise the module will request another line which will at the same time as our padding write
				#This should only happen on layers with a small number of input channels (e.g. the first one which has 3) and is most likely to occur when output channel parallelism is turned on 
				#Right now this is only implemented for upper padding. I'm not sure if it would be an issue for lower padding
				if padding[1] > 0:
					line_counter_almost_done_val = inst(dl.Constant, (input_line_counter_end_val), signed=False, bits=self.input_line_counter.current_value._bit_width, name="line_counter_almost_done_val")
					line_counter_almost_done = self.input_line_counter.current_value >= line_counter_almost_done_val
					self.pad_next_and_buffer_almost_full= inst(dl.AND, line_counter_almost_done, inst(dl.EQ, self.lines_used_counter.current_value, counter_end_value-1, name="buffer_almost_full"), name="pad_next_and_buffer_almost_full")
				else:
					self.pad_next_and_buffer_almost_full= inst(dl.Constant, bits=1, value=0, name="pad_next_and_buffer_almost_full")
									
				#self.has_space_and_not_in_padding_next = inst(dl.AND, self.has_space_to_write, inst(dl.NOT, self.in_padding_next_no_delay), self.state_machine_idle, inst(dl.NOT, self.pad_next_and_buffer_almost_full, name="not_pad_next_and_buffer_almost_full"))
				self.has_space_and_not_in_padding_next = inst(dl.AND, self.has_space_to_write, inst(dl.NOT, self.in_padding_next_no_delay), inst(dl.NOT, self.pad_next_and_buffer_almost_full, name="not_pad_next_and_buffer_almost_full"))
			else:
				self.has_space_and_not_in_padding_next = self.has_space_to_write

			self.space_to_write_line.set_driver(self.has_space_and_not_in_padding_next)
			self.done_line = inst(dl.Logic, 1, name="done_line")
			self.input_line_counter.set_increment_condition(self.done_line)

			states = ["IDLE", "WRITING_LINE", "DONE_WRITING_LINE"]
			edges = [
				["IDLE", "WRITING_LINE", self.write],
				["WRITING_LINE", "DONE_WRITING_LINE", self.done_line],
				["DONE_WRITING_LINE", "IDLE"]]
			if self.has_padding:
				edges += [["IDLE", "WRITING_PADDING", inst(dl.AND, self.has_space_to_write, self.in_padding)],
					["WRITING_PADDING", "DONE_WRITING_PADDING", self.done_line],
					["DONE_WRITING_PADDING", "IDLE"]]
				states += ["WRITING_PADDING", "DONE_WRITING_PADDING"]

			self.state_machine = inst(dl.StateMachine, states, edges, None)
			self.state_machine_idle.set_driver(self.state_machine.c["is_idle"])
			if self.has_padding:
				self.is_writing_padding = self.state_machine.c["is_writing_padding"]
				self.is_done_writing_padding = self.state_machine.c["is_done_writing_padding"]
			else:
				self.is_writing_padding = inst(dl.Constant, bits=1, value=0, name="is_writing_padding")
				self.is_done_writing_padding = inst(dl.Constant, bits=1, value=0, name="is_done_writing_padding")

			self.illegal_write = inst(dl.AND, self.write, inst(dl.NOT, self.state_machine.c["is_writing_line"]), inst(dl.NOT, inst(dl.AND, self.state_machine.c["is_idle"], self.has_space_to_write)))
			self.should_write_out = inst(dl.OR, self.is_writing_padding, self.write, name="should_write_out")
			self.base_counter_increment_condition = inst(dl.AND, self.should_write_out, self.increment_write_condition, name="base_counter_increment_condition")
			ic_active_channel_sr_reset = inst(dl.OR, kwargs["reset"], self.input_channel_counter.done_and_incrementing, name="is_active_channel_sr_reset")
			with dl.ParamScope(reset=ic_active_channel_sr_reset):
				ic_active_channel_sr = inst(dl.ShiftRegister, bits=1, depth=n_channel_splits)
				with dl.ModuleScope(ic_active_channel_sr):
					ic_active_channel_sr.d.set_driver(ic_active_channel_sr.q)
					ic_active_channel_sr.sr[0].set_reset_driver(inst(dl.Constant, bits=1, value=1, name="one"))
					z = inst(dl.Constant, bits=1, value=0, name="zero")
					for sr in ic_active_channel_sr.sr[1:]:
						sr.set_reset_driver(z)
					ic_active_channel_sr.should_shift.set_driver(self.input_channel_counter.increment_condition)
			self.channel_matches_split = ic_active_channel_sr.sr


			self.cred_max = counter_end_value
			self.cred_dec = self.finished_reading_line.delay(2)
			self.cred_dec_amount = -stride
			self.cred_dec_final_amount = final_diff_value
				
			write_counter_increment_delay = min(max(0, len(self.write_outs)-2), 2)
			self.write_counter_increment_delay = write_counter_increment_delay
			for i,wo in enumerate(self.write_outs):
				wo.set_driver(inst(dl.AND, self.should_write_out, ic_active_channel_sr.sr[i]).delay(write_counter_increment_delay))
				# we can delay each of these by up to len(self.write_outs)-2 cycles since the
				# address will only need to be used again after we have written to all of the other
				# addresses
				self.write_controllers[i].write.set_driver(inst(dl.AND, wo, self.increment_write_condition.delay(write_counter_increment_delay)))


			self.write_addresses = [c.address for c in self.write_controllers]
			self.write_address.set_driver(inst(dl.OneHotMux, selects=ic_active_channel_sr.sr, values=[c.address for c in self.write_controllers]))
			self.write_address._bit_width = self.write_controllers[0].address._bit_width
			self.input_channel_counter.set_increment_condition(self.base_counter_increment_condition)
			
			self.increment_lines_used.set_driver(inst(dl.AND, self.state_machine.c["is_idle"], self.write))
			
			self.lines_used_counter.change_signals[0].set_driver(
				inst(dl.OR, self.is_done_writing_padding,self.increment_lines_used))
			self.lines_complete_counter.change_signals[0].set_driver(
				inst(dl.OR, self.is_done_writing_padding,
						self.state_machine.c["is_done_writing_line"], name="increment_lines_complete"))
			self.done_line.set_driver(self.input_channel_counter.done_and_incrementing)
			self.input_line_counter_next.set_increment_condition(inst(dl.OR, inst(dl.AND, self.state_machine.c["is_idle"].delay(2), self.write.delay(2)), self.is_done_writing_padding.delay(2)))

			self.requires_driver = [self.write, self.finished_reading_line, self.started_reading_line, self.done_image]

			self.dump_counter_values = self.input_line_counter.done_and_incrementing.copy()
			self.counter_values = {
				"input_channel_counter" : self.input_channel_counter.current_value.copy(),
				"input_line_counter" : self.input_line_counter.current_value.copy(),
				"input_line_counter_next" : self.input_line_counter_next.current_value.copy(),
				"lines_used_counter" : self.lines_used_counter.current_value.copy(),
				"lines_complete_counter" : self.lines_complete_counter.current_value.copy(),
				"started_reading_line_counter" : started_reading_line_counter.current_value.copy()
			}
			modules_to_debug.append(self)

			#add overflow assertion
			input_buff_overflow = inst(dl.Logic, bits=1, name="input_buff_overflow")
			input_buff_overflow.set_driver(inst(dl.AND, self.lines_complete_counter.is_done, self.lines_complete_counter.change_signals[0]).delay(1))
			m = self
			def full_condition(m, t="  ", fifo_condition=input_buff_overflow):
				condition = "(" + fifo_condition.name + " !== 1)"
				return condition
				
			inst(dl.Assertion,
				name="act_buff_overflow_check",
				condition_f=full_condition,
				condition_f_args=m,
				comment=input_buff_overflow.name + " is high! This means the InputBuffer is full and being written to! ",
				error_string=input_buff_overflow.name + " is high! This means the InputBuffer is full and being written to! ")

	def get_instance_comment(self, t="  "):
		comment = "\n".join([k + ": " + str(v) for k, v in self.params.items()]) + "\n"
		return v_comment_string(comment, t)

	def get_custom_verilog_str(self, t="  "):
		verilog = super(InputBufferController, self).get_custom_verilog_str(t)
		"""
		module_path = ".".join([o.name for o in reversed([self] + self.get_owner_list()[:-1])])
		verilog += "  always @(posedge clock_0) begin\n"
		#verilog += "`ifdef TESTING\n"
		verilog += "    if (" + self.illegal_write.name + ") begin\n"
		verilog += '      $display("' + module_path + ' got written to in an state where it is not allowed to be written to %0t", $time);\n'
		verilog += "    end\n"
		verilog += "    if (" + self.dump_counter_values.name + ") begin\n"
		verilog += '      $display("' + module_path + ' finished input.  Counter values are:");\n'
		for k,v in self.counter_values.items():
			verilog += '      $display("  ' + k + ': %d", ' + v.name + ");\n"
		verilog += "    end\n"
		#verilog += "    if (!" + self.can_write_line.name + ") begin\n"
		#verilog += '      $display("' + self.name + ' cannot write");\n'
		#verilog += "    end\n"
		#verilog += "`endif\n"
		verilog += "  end\n"""
		return verilog

#Serialize N inputs into M channels, where N is a multiple of M
#This can be used if you only have a limited number of LUTs
class GenericSerializer(dl.Module):
	
	# Instantiates a copy of only the logic to drive the control output write; called by __init__
	# Since that write is global and arrives as an input before the actual data writes, I have no idea how else to implement this without duplicating a bunch of the logic 
	# Adding a set delay would not work because the output write patterns are dependent on how fast the input data arrives
	# NOTE: This must be updated for every change to control logic in the __init__ function.
	def instantiate_control_logic(self, input_channels, output_channels):
		inst = self.inst
		one = inst(dl.Constant, value=1, bits=1, name="control_one")
		zero =  inst(dl.Constant, value=0, bits=1, name="control_zero")
		
		input_fifo = inst(dl.FIFO, bits=1, depth=32, name="control_generic_serializer_FIFO") 
		input_fifo.w_en.set_driver(self.control_write)
		input_fifo.w_data.set_driver(one) #Not technically needed, since output is not hooked up it should be synthesized out in Quartus
		
		parallel_sr_depth = int(input_channels/output_channels)
		counter = inst(dl.Counter,  reset_value=0, increment_value=1, end_value=(parallel_sr_depth-1), name="sr_counter")
		
		sr_init = inst(dl.Flop, bits=1, reset_driver=0, name="control_sr_init_control")
		last_val_stored = inst(dl.Flop, bits=1, name="control_last_val_stored_control")
		previous_write_delay = inst(dl.Logic, 1, name=("control_previous_write_delay_control"))
		
		input_fifos_not_empty = inst(dl.NOT, input_fifo.empty, name="control_input_fifo_not_empty")
		read_fifo = inst(dl.AND, input_fifos_not_empty, inst(dl.OR, counter.is_done, inst(dl.NOT, sr_init)), name="control_read_fifo")
		input_fifo.r_en.set_driver(read_fifo) 
		sr_init.set_driver(inst(dl.MuxV2, read_fifo, sr_init, one, name="control_sr_init_mux"))
		counter.increment_condition.set_driver(inst(dl.AND, sr_init, inst(dl.OR, inst(dl.NOT, counter.is_done), inst(dl.AND, counter.is_done, input_fifos_not_empty)), name="control_sr_counter_increment_cond"))
		
		output_writes = (inst(dl.AND, sr_init.delay(1), inst(dl.NOT, last_val_stored), name="control_sr_output_write_"))
		
		#only actual need 2 values, but it's easier to maintain the code if we do it in an array similar to the actual __init__ function
		output_actual_writes = parallel_sr_depth * [None]
		
		output_actual_writes[parallel_sr_depth-1] = inst(dl.AND, output_writes, inst(dl.OR, inst(dl.EQ, self._constants[0], counter.current_value, name="control_counter_eq_0"), inst(dl.AND, counter.is_done, previous_write_delay)), name=("control_actual_write_last"))
		output_actual_writes[parallel_sr_depth-2] = inst(dl.AND, output_writes, inst(dl.EQ, self._constants[parallel_sr_depth-1], counter.current_value, name="control_counter_eq_last"), inst(dl.NOT, previous_write_delay), name="control_actual_write_second_last" )

		set_last_val_stored = inst(dl.MuxV2, inst(dl.AND, counter.is_done, output_actual_writes[parallel_sr_depth-1] ), last_val_stored, one, name="set_last_val_stored") 
		last_val_stored.set_driver(inst(dl.MuxV2, inst(dl.OR, counter.is_reset_value, inst(dl.NOT, sr_init)), set_last_val_stored, zero))
		previous_write_delay.set_driver(output_actual_writes[parallel_sr_depth-2], delay=1)
		
		
		return output_actual_writes[parallel_sr_depth-1]
		
	
	def __init__(self, bits, input_channels, output_channels, **kwargs):
		super(GenericSerializer, self).__init__("GenericSerializer", kind="GenericSerializer", **kwargs)
		inst = self.inst
		
		if input_channels % output_channels != 0:
			raise Exception("Error! " + str(input_channels) + " is not a multiple of " + str(output_channels) + ". Cannot instantiate GenericSerializer")
		
		with dl.ModuleScope(self):
			#Instantiate a FIFO for every input
			input_width = input_channels
			
			#Make sure to hook up the following inputs
			self.writes = []
			self.control_write = inst(dl.Logic, 1, name="control_write")
			self.input_fifos = [] # input_fifos[i].w_data

			#Use the following for outputs as needed
			self.outputs = []
			self.output_writes = [] # One write signal for each of the output channels
			self.output_actual_writes = [] # One output write signal for each of the input channels, this should be used if you are planning on splitting the output signals back up later
			
			# Batch valid differs from output_write in that it only goes high once the current batch of data from the input channels has passed through the serializer
			self.batch_valid = inst(dl.Logic, 1, name="batch_valid")
			self.control_batch_valid = inst(dl.Logic, 1, name="control_batch_valid") # Global output write signal, will output before the synchronized write signal
			
			self._constants = []
			fifo_outputs = []
			
			for i in range(input_width):
				self.writes.append(inst(dl.Logic, 1, name="write"))
				#Create FIFO for each input
				#Setting FIFO depth to 32 as that is the max depth of a single MLAB FIFO
				input_fifo = inst(dl.FIFO, bits=bits, depth=32, name="generic_serializer_input_" + str(i) + "_FIFO") 
				self.input_fifos.append(input_fifo)
				input_fifo.w_en.set_driver(self.writes[i])

				fifo_output = inst(dl.Logic, bits, name=("fifo_output_" + str(i)))
				fifo_output.set_driver(input_fifo.r_data)
				fifo_outputs.append(fifo_output)

			#Group up inputs and instantiate a parallel input serial output (PISO) shift register
			parallel_sr_depth = int(input_channels/output_channels)
			parallel_sr_list = []
			
			#Instantiate global counter so we know when PISO finishes each batch of data
			#NOTE: this assumes all input data arrives at the same time. Otherwise you need seperate counters for each individual PISO
			counter = inst(dl.Counter,  reset_value=0, increment_value=1, end_value=(parallel_sr_depth-1), name="sr_counter")
			
			#Create constants for counter values
			for j in range(0,parallel_sr_depth):
				tmp_const = inst(dl.Constant, value=j, bits=counter.get_bit_width(), name=("constant_" + str(j)))
				self._constants.append(tmp_const)
			
			#Need a flop to store init state otherwise we would end up needing an extra cycle in the counter (innefficient performance-wise)
			sr_init = inst(dl.Flop, bits=1, reset_driver=0, name="sr_init")
			one = inst(dl.Constant, value=1, bits=1, name="one")
			zero =  inst(dl.Constant, value=0, bits=1, name="zero")
			
			#This flop is needed because the counter might need to wait in the last stage for a new FIFO value. If that is the case, we need to ensure the last write only happens once while it is waiting
			last_val_stored = inst(dl.Flop, bits=1, name="last_val_stored")
			previous_write_delay = inst(dl.Logic, 1, name=("previous_write_delay"))
			
			#All inputs arrive at the same time so we can just check the first FIFO
			input_fifos_not_empty = inst(dl.NOT, self.input_fifos[0].empty, name="input_fifo_not_empty")
			#Only load if input FIFOs are ready and if either the previous data has been shifted out (i.e. counter is done) or we have not initialized yet
			read_fifo = inst(dl.AND, input_fifos_not_empty, inst(dl.OR, counter.is_done, inst(dl.NOT, sr_init)), name="read_fifo")
			
			#Set init reg and counter increment condition based on first shift register since other ones should all be the same:
			sr_init.set_driver(inst(dl.MuxV2, read_fifo, sr_init, one, name="sr_init_mux"))
			#Start incrementing the counter after input FIFOs are ready . Continue incrementing until counter is reset
			counter.increment_condition.set_driver(inst(dl.AND, sr_init, inst(dl.OR, inst(dl.NOT, counter.is_done), inst(dl.AND, counter.is_done, input_fifos_not_empty)), name="sr_counter_increment_cond"))

			#Instantate parallel load shift registers and output wires
			for i in range(0,input_channels,parallel_sr_depth):
				cur_num = int(i/parallel_sr_depth) #For naming
			
				parallel_sr = inst(dl.ParallelLoadShiftRegister, depth=parallel_sr_depth, bits=bits, name=("parallel_sr_" + str(cur_num)))
				parallel_sr_list.append(parallel_sr)
				
				cur_output=inst(dl.Logic, bits, name=("shift_reg_output_" + str(cur_num)))
				cur_output.set_driver(parallel_sr.sr[-1])
				self.outputs.append(cur_output)
				
				# We must also check if the last write has occured already since the counter could be waiting in the last state for a new value		   
				self.output_writes.append(inst(dl.AND, sr_init.delay(1), inst(dl.NOT, last_val_stored), name=("sr_output_write_"+str(cur_num))))

				for j in range(0,parallel_sr_depth):
					#Assign load values and write value
					parallel_sr.input_data[j].set_driver(fifo_outputs[i+(parallel_sr_depth-1)-j]) #Output is backwards so load input in backwards to ensure correct order
					self.input_fifos[i+j].r_en.set_driver(read_fifo) 
					parallel_sr.loads[j].set_driver(read_fifo, delay=1)

					#Write happens 1 cycle after it's associated counter value. The last write might happen when counter=0 or when the counter is in the last stage if its waiting on a new FIFO value 
					if j == (parallel_sr_depth-1): 
						self.output_actual_writes.append(inst(dl.AND, self.output_writes[cur_num], inst(dl.OR, inst(dl.EQ, self._constants[0], counter.current_value, name=("counter_eq_"+str(i+j))), inst(dl.AND, counter.is_done, previous_write_delay)), name=("actual_write_" + str(i+j)) ))
					else:
						self.output_actual_writes.append(inst(dl.AND, self.output_writes[cur_num], inst(dl.EQ, self._constants[j+1], counter.current_value, name=("counter_eq_"+str(i+j))), inst(dl.NOT, previous_write_delay), name=("actual_write_" + str(i+j)) ))
						

			#Set batch as valid if final value has been written
			self.batch_valid.set_driver(self.output_actual_writes[parallel_sr_depth-1])
			self.control_batch_valid.set_driver(self.instantiate_control_logic(input_channels, output_channels))
			
			#Set to 1 if the last value in each shift register has been stored/outputed 
			set_last_val_stored = inst(dl.MuxV2, inst(dl.AND, counter.is_done, self.output_actual_writes[parallel_sr_depth-1] ), last_val_stored, one, name="set_last_val_stored") 
			last_val_stored.set_driver(inst(dl.MuxV2, inst(dl.OR, counter.is_reset_value, inst(dl.NOT, sr_init)), set_last_val_stored, zero))
			previous_write_delay.set_driver(self.output_actual_writes[parallel_sr_depth-2], delay=1)

			
class ReLU(NNStage):
	kind = "relu"
	def instantiate(self, node, inst, build_data_path=True, **kwargs):
		input_width = self.get_input_act(dim="width")
		bits, _, output_bits = self.get_bit_widths()
		signed, _, osigned = self.get_signs()
		input_act_bit_spec = self.get_bit_spec("input")
		output_act_bit_spec = self.get_bit_spec("output")
		input_bits = input_act_bit_spec["width"]
		relu_delay = 2
		zero = inst(dl.Constant, value=0, bits=input_bits, signed=osigned, name="zero")
		for i in range(input_width):
			#with dl.ParamScope(name="sign_bit_" + str(i)):
			less_than_0 = self.inputs[i][bits-1]
			non_clipped_output = self.inputs[i][input_bits-1:0]
			relu_mux = inst(dl.MuxV2,
				less_than_0,
				non_clipped_output,
				zero)
			self.outputs[i].set_driver(cast_fixed(relu_mux, input_act_bit_spec, output_act_bit_spec), delay=relu_delay)
			self.outputs_valid[i].set_driver(self.writes[i], delay=relu_delay)

		self.space_to_write_line.set_driver(self.can_write_line, delay=2)
		self.output_valid.set_driver(self.write, delay=relu_delay)
		self.requires_driver = self.writes + [self.write, self.can_write_line]
		self._set_control_valid_to_output_valid_delay(-1)
		self._set_control_to_input_delay(-1)
		
##################################################################################################################
class ReLU6(NNStage):
	kind = "relu6"
	def instantiate(self, node, inst, build_data_path=True, **kwargs):
		input_width = self.get_input_act(dim="width")
		input_act_bit_spec = self.get_bit_spec("input")
		output_act_bit_spec = self.get_bit_spec("output")
		bits, _, output_bits = self.get_bit_widths()
		input_bits = input_act_bit_spec["width"]
		
		signed, _, osigned = self.get_signs()
		relu_delay = 2
		six = inst(dl.Constant, value=6<<input_act_bit_spec['frac'], bits=bits, signed=signed, name="six")
		zero = inst(dl.Constant, value=0, bits=input_bits, signed=osigned, name="zero")
		
		if input_act_bit_spec['int'] < 3:
			print("WARNING: Input has less than 3 int bits so ReLU6 cannot be instantiated. Using a Regular RELU instead.")

		for i in range(input_width):
			#If we have less than 3 input bits we can't represent 6 so instantiate a regular ReLU mux
			if input_act_bit_spec['int'] < 3:
				less_than_0 = self.inputs[i][bits-1]
				non_clipped_output = self.inputs[i][input_bits-1:0]
				relu_mux = inst(dl.MuxV2,
					less_than_0,
					non_clipped_output,
					zero)
			else:
				less_than_0_greater_than_6 = inst(dl.Concat, self.inputs[i][bits-1],
						self.inputs[i] > six)
				non_clipped_output = self.inputs[i][input_bits-1:0]
				relu_mux = inst(dl.MuxV2,
					less_than_0_greater_than_6,
					non_clipped_output,
					zero,
					six,
					zero)
			self.outputs[i].set_driver(cast_fixed(relu_mux, input_act_bit_spec, output_act_bit_spec), delay=relu_delay)
			self.outputs_valid[i].set_driver(self.writes[i], delay=relu_delay)

		self.space_to_write_line.set_driver(self.can_write_line, delay=2)
		self.output_valid.set_driver(self.write, delay=relu_delay)
		self.requires_driver = self.writes + [self.write, self.can_write_line]
		self._set_control_valid_to_output_valid_delay(-1)
		self._set_control_to_input_delay(-1)

#General class for Swish, Sigmoid and Tanh since they are all implemented in the same way; only the LUT values are different 
class ActivationFunction(NNStage):
	kind = "ActivationFunction"
	reduction_factor = 1 #How much to reduce the number of LUTs by. For example if there are 32 inputs and you reduce by 2, there will only be 16 LUTs (each shared by 2 inputs)
	
	#Source: https://stackoverflow.com/questions/6800193/what-is-the-most-efficient-way-of-finding-all-the-factors-of-a-number-in-python
	def find_factors(self, n):
		step = 2 if n % 2 else 1
		return set(reduce(list.__add__, ([i, n//i] for i in range(1, int(math.sqrt(n))+1, step) if n % i == 0)))
	
	def instantiate(self, node, inst, build_data_path=True, reduction_factor_override=None, **kwargs):
		function_type = node.get_type().lower()
		
		# Allow override of reduction factor for unit tests
		if reduction_factor_override != None: 
			self.reduction_factor = reduction_factor_override
		else:
			#Look for the previous convolution node. This should give us a lower bound for the cycles per values (i.e. max possible throughput)
			# The throughput from a max pooling or bias adds node is always going to be much higher than the convolution node
			prev_node = node.get_input() 
			while prev_node != None and prev_node.get_type() not in ["Conv2D", "DepthwiseConv2dNative"]:
				prev_node = prev_node.get_input()

			# Use the planner estimate to determine a lower bound for cycles per value
			# It may be possible to serialize even more since there usualy is a delay until the next row comes in, however this would be more complicated to compute since the max input FIFO size is 32
			if prev_node != None:
				cycles_per_value = float(prev_node.planner_estimate.row_cycles)/np.shape(prev_node.example_outputs)[-1] # Divide by the number of channels
				#print("cycles_per_value", cycles_per_value, "row_cycles", prev_node.planner_estimate.row_cycles, "shape", np.shape(prev_node.example_outputs) )
				factor_set = self.find_factors(self.get_input_act(dim="width"))
				# Find the maximum possible reduction factor for the given throughput
				cur_max = 1
				for num in factor_set:
					if num > cur_max and float(num) < cycles_per_value: 
						cur_max = num
				self.reduction_factor = 1
				print("Width", self.get_input_act(dim="width"), "Reduction factor", self.reduction_factor)
		
		#Change the bit spec in assign_activation_precisions in GraphBuilder.py
		output_act_bit_spec = self.get_bit_spec("output")
		input_act_bit_spec =  self.get_bit_spec("input")
		
		LUT_range = 2 ** input_act_bit_spec["int"]
		LUT_size = 2 ** input_act_bit_spec["width"]
		LUT_size_half = int(LUT_size/2)
		
		pos_range = np.arange(0,LUT_range,(LUT_range*2)/LUT_size)
		neg_range = np.arange(-LUT_range,0,(LUT_range*2)/LUT_size)
		z = np.zeros(LUT_size)
		z[0:LUT_size_half] = pos_range
		z[LUT_size_half:LUT_size] = neg_range
		activation_lookup_indices = z
		
		activation_lookup = []
		#Generate correct LUT values based on activation function
		if function_type== "sigmoid":
			activation_lookup = list(get_quantized(1 / (1 +  np.exp(-1 * activation_lookup_indices)),
				sign_bits=int(output_act_bit_spec["sign"]),
				integer_bits=output_act_bit_spec["int"],
				fractional_bits=output_act_bit_spec["frac"]))
		elif function_type == "tanh":
			# Tanh(x) = (e^x-e^-x)/(e^x+e^-x) = (e^2x - 1)/(e^2x + 1)
			activation_lookup = list(get_quantized((np.exp(2 * activation_lookup_indices) - 1) / (np.exp(2 * activation_lookup_indices) + 1),
				sign_bits=int(output_act_bit_spec["sign"]),
				integer_bits=output_act_bit_spec["int"],
				fractional_bits=output_act_bit_spec["frac"]))
		else: #Swish
			activation_lookup = list(get_quantized( z / (1 +  np.exp(-1 * activation_lookup_indices)),
			sign_bits=int(output_act_bit_spec["sign"]),
			integer_bits=output_act_bit_spec["int"],
			fractional_bits=output_act_bit_spec["frac"]))
		
		#Warning message since input/output may have different bit specs then preceding nodes
		input_max = str((2 ** input_act_bit_spec["int"]) - 1)
		output_max = str((2 ** output_act_bit_spec["int"]) - 1)
		warnings.warn(function_type + " implementation casts all inputs to " + str(input_act_bit_spec["width"]) + " bits with a range of -" + input_max + " to " + input_max +" and casts the output to " + str(output_act_bit_spec["width"]) + " bits with a range of -" + output_max + " to " + output_max) 
		
		input_width = self.get_input_act(dim="width")
		input_act_bit_spec = self.get_bit_spec("input")
		bits, _, output_bits = self.get_bit_widths()
		signed, _, osigned = self.get_signs()
		
		activation_delay = 2  #mem delay 2
		memory_model_delay = 1
		added_read_delay = activation_delay - memory_model_delay	
		
		#If we don't serialize the input to a smaller number of channels, use the old method for initializing ROM
		if self.reduction_factor == 1:
			for i in range(input_width):
				rom = inst(dl.ROM, bits=output_act_bit_spec["width"], content_list=activation_lookup)
				rom.r_en.set_driver(self.writes[i]) 
				rom.r_addr.set_driver(self.inputs[i])
				self.outputs[i].set_driver(rom.r_data, delay=added_read_delay)
				self.outputs_valid[i].set_driver(rom.r_en, delay=activation_delay)

			self.space_to_write_line.set_driver(self.can_write_line, delay=1)
			self.output_valid.set_driver(self.write, delay=activation_delay)
			self._set_control_valid_to_output_valid_delay(-1)
			self._set_control_to_input_delay(-1)	

		else:
			#serialize input first
			number_LUTs = int(input_width/self.reduction_factor)
			serial = inst(GenericSerializer, input_act_bit_spec["width"], input_width, number_LUTs)
			serial.control_write.set_driver(self.write)

			#Instantiate ROMs. We can use the serializer to reduce the number of ROMs required
			roms = []
			for j in range(number_LUTs):
				rom = inst(dl.ROM, bits=output_act_bit_spec["width"], content_list=activation_lookup)
				rom.r_en.set_driver(serial.output_writes[j]) 
				rom.r_addr.set_driver(serial.outputs[j])
				roms.append(rom)

			#Hook up all the inputs to the serializer and split up the outputs again after
			for i in range(input_width):
				serial.input_fifos[i].w_data.set_driver(self.inputs[i])
				serial.writes[i].set_driver(self.writes[i])

				#Save all outputs to flops first since the next stage expects all values to be written at the same time
				output_flop = inst(dl.Flop, bits=output_act_bit_spec["width"], name=("activation_output_flop_" + str(i)))	
				actual_write_delay = inst(dl.Logic, bits=1, name=("actual_write_delay_" + str(i)))
				actual_write_delay.set_driver(serial.output_actual_writes[i], delay=memory_model_delay)

				rom_id = int(math.floor(i/float(self.reduction_factor)))
				output_flop.set_driver(inst(dl.MuxV2, actual_write_delay, output_flop, roms[rom_id].r_data, name=("output_flop_mux_"+str(i)))) 

				#Hook up output to current flop and the valid signal to the global batch valid so everything is written to memory at once
				self.outputs[i].set_driver(output_flop)
				self.outputs_valid[i].set_driver(serial.batch_valid, delay=(activation_delay))

			self.space_to_write_line.set_driver(self.can_write_line, delay=1)
			self.output_valid.set_driver(serial.control_batch_valid, delay=(activation_delay)) 
			self._set_control_valid_to_output_valid_delay(0)
			self._set_control_to_input_delay(-1)			

		self.requires_driver = self.writes + [self.write, self.can_write_line] 

class BiasAdd(NNStage):
	kind = "bias_add"
	def instantiate(self, node, inst, build_data_path=True, **kwargs):
		biases = node.values[0]
		input_width = self.get_input_act(dim="width")
		bias_bit_spec = self.get_bit_spec("parameters")
		stored_bits = clog2(max(abs(biases))*2+1)
		#Marius: technically the negative max value can be represented (e.g. for 8-bits it would be -128)
		if min(biases) == -2 ** (bias_bit_spec["int"]+bias_bit_spec["frac"]) and  abs(min(biases)) == max(abs(biases)):
			print("NOTE: the max absolute value stored was negative, and fits in the original bit spec (unlike the positive case)" )
			stored_bits = bias_bit_spec["width"]
		
		bias_bit_spec["int"] -= bias_bit_spec["width"] - stored_bits
		bias_bit_spec["width"] = stored_bits
		input_act_bit_spec = self.get_bit_spec("input")
		output_act_bit_spec = self.get_bit_spec("output")
		
		channels = self.get_input_act(dim="channels")
		physically_mapped = False
		if "physically_mapped_rams" in kwargs:
			physically_mapped = kwargs["physically_mapped_rams"]

		bias_add_cycles = 1
		cast_cycles = 2
		bias_rom = inst(dl.FanoutROM,
			content_list=biases,
			bits=stored_bits,
			physically_mapped=physically_mapped,
			name="bias_rom")

		address_counter = inst(dl.Counter,
			reset_value=0,
			increment_value=1,
			end_value=channels-1)
		address_counter.set_increment_condition(self.write)
		bias_rom.r_addr.set_driver(address_counter.current_value)
		bias_rom.r_en.set_driver(self.write)

		bias_fanout = [inst(dl.Flop, bits=stored_bits, name="bias_fanout_" + str(i)) for i in range(input_width)]
		bias_fanout_pipe = inst(dl.PipelinedFanout,
			bias_rom.r_data,
			*bias_fanout,
			max_fanout=8,
			name="bias_fanout_pipe")
		self._set_control_to_input_delay(bias_rom.read_delay + bias_fanout_pipe.get_added_delay() + 1)
		self._set_control_valid_to_output_valid_delay(self.get_control_to_input_delay() + bias_add_cycles + cast_cycles)

		self.output_valid.set_driver(self.write)
		for i, (b, a) in enumerate(zip(bias_fanout, self.inputs)):
			b, a, aligned_spec = self.get_aligned_for_add(b, a, bias_bit_spec, input_act_bit_spec)
			added = a + b
			output = cast_fixed(added.delay(bias_add_cycles), aligned_spec, output_act_bit_spec).delay(cast_cycles)
			self.outputs[i].set_driver(output)
			self.outputs_valid[i].set_driver(self.writes[i], delay=bias_add_cycles+cast_cycles)

		self.space_to_write_line.set_driver(self.can_write_line, delay=1)
		self.requires_driver = self.writes + [self.write, self.can_write_line]


		
class MaxPool(NNStage):
	kind = "max_pool"
	def get_instance_comment(self, t="  "):
		comment = "Kernel shape: " + str(self.node.get_kernel_shape()) + "\n"
		comment += "Strides: " + str(self.node.get_strides()) + "\n"
		comment += "Padding: " + self.node.properties["padding"] + ": " + str(self.padding) + "\n"
		return super(MaxPool, self).get_instance_comment(t=t) + v_comment_string(comment, t)
	def instantiate(self, node, inst, build_data_path=True, **kwargs):
		parallel_acts_per_ram = node.inputs[0]._from.get_parallel_acts_per_memory()

		k_shape = node.get_kernel_shape()
		kernel_height = k_shape[0]
		kernel_width = k_shape[1]

		stride = node.get_strides()
		vertical_stride = stride[0]
		horizontal_stride = stride[1]

		bits, _, output_bits = self.get_bit_widths()
		input_act_bit_spec = self.get_bit_spec("input")
		output_act_bit_spec = self.get_bit_spec("output")
		signed, _, _ = self.get_signs()
		ia_shape = self.get_input_act().shape
		if type(node.properties["padding"]) is bytes:
			node.properties["padding"] = node.properties["padding"].decode("ascii")
		padding, _ = get_padding(
			ia_shape,
			k_shape,
			vertical_stride,
			node.properties["padding"],
			None)
		self.padding = padding

		input_channels = self.get_input_act(dim="channels")
		input_height = self.get_input_act(dim="height")
		input_width = self.get_input_act(dim="width")
		self.buffered_layer = True

		output_width = self.get_output_act(dim="width")
		output_height = self.get_output_act(dim="height")

		cast_delay = 1
		pre_modulo_addr_add_delay = 2
		address_computation_delay = pre_modulo_addr_add_delay
		sr_write_delay            = 1
		single_addr_delay         = kernel_width
		single_pool_delay         = kernel_width * kernel_height * 2
		fanout_levels = math.ceil(math.log(input_width, 4))

		self.go = inst(dl.Logic, 1, name="go")
		self.can_go = inst(dl.Logic, 1, name="can_go")

		self.ibc = inst(InputBufferController, vertical_stride, kernel_height, input_channels, input_height, padding[1], 1, output_height)
		self.ibc.write.set_driver(self.write)
		self.ibc.done_image.set_driver(self.done)
		self.can_go.set_driver(self.ibc.has_full_kernel)
		self.go.set_driver(inst(dl.AND, self.can_go, self.can_write_line))
		self.ia_buffer = inst(IABuffer,
			bits,
			kernel_height,
			vertical_stride,
			input_channels,
			input_width,
			build_data_path=build_data_path,
			write_port_width=parallel_acts_per_ram, ic_groups=1)
		memory_read_delay = self.ia_buffer.control_delay + self.ia_buffer.read_delay
		self.ia_buffer.w_addrs[0].set_driver(self.ibc.write_address)
		self.ia_buffer.writes[0].set_driver(self.ibc.write_outs[0])
		ram_inputs = self.inputs
		if self.ibc.has_padding:
			padding_zeros = inst(dl.Constant, bits=input_act_bit_spec["width"], value=1<<(input_act_bit_spec["width"]-1), signed=True, name="padding_zeros")
			ram_inputs = [inst(dl.MuxV2, w, padding_zeros, i, name="input_or_padding_"+str(j))for j,(i,w) in enumerate(zip(self.inputs, self.writes))]
		for ri, ia_buffer_w_data in zip(ram_inputs, self.ia_buffer.inputs):
			ia_buffer_w_data.set_driver(ri)

		with dl.ModuleScope(inst(dl.Module, "max_pool_read_controller", kind="max_pool_read_controller")):
			self.pool_multicycle_counter = inst(dl.Counter,
				reset_value=0,
				increment_value=1,
				end_value=1,
				name="pool_multicycle_counter")
			self.channel_counter = inst(dl.Counter,
				reset_value=0,
				increment_value=1,
				end_value=input_channels-1,
				name="channel_counter")
			self.x_counter = inst(dl.Counter,
				reset_value=0,
				increment_value=1,
				end_value=kernel_width-1,
				name="x_counter")
			self.y_counter = inst(dl.Counter,
				reset_value=0,
				increment_value=input_channels,
				end_value=(kernel_height-1) * input_channels,
				name="y_counter")

			self.done_line = inst(dl.AND,
				self.channel_counter.is_done,
				self.y_counter.is_done,
				self.x_counter.is_done,
				self.pool_multicycle_counter.is_done)

			# the read controller will keep track of the current output index
			# so that it knows when we finish an input image.
			output_line_counter = inst(dl.Counter,
				reset_value=0,
				increment_value=1,
				end_value=output_height-1,
				name="output_line_counter")
			output_line_counter.set_increment_condition(self.done_line.delay(1))

			# The line address counter is the base address to which the pool kernel
			#  address is added.  Basically as the convolution window shifts down
			#  we increment this counter.  Our buffer has limited space though, so
			#  the counter decrements itself by the number of input lines we store
			#  whenever it exceeds the number of lines we store.
			# The second change value handles the edge case of the end of an input
			#  image where we need to add not the stride, but the size of the kernel.
			#  the stride will be added by the first change value, and the difference
			#  between the stride and the kernel size added by the second change 
			#  value.
			line_address_counter = inst(dl.ModuloMultiChangeCounter,
				reset_value=0,
				change_values=[
					input_channels * vertical_stride,
					input_channels * (kernel_height - vertical_stride)],
				modulo_value=input_channels * (vertical_stride + kernel_height),
				end_value=input_channels * (vertical_stride + kernel_height + 2))
			line_address_counter.change_signals[0].set_driver(self.done_line)
			line_address_counter.change_signals[1].set_driver(
				inst(dl.AND, self.done_line, output_line_counter.is_done.delay(1)))

			self.load_sr = inst(dl.AND,
				self.x_counter.is_reset_value,
				self.pool_multicycle_counter.is_reset_value).delay(address_computation_delay + memory_read_delay - fanout_levels)
			self.y_counter.set_increment_condition(
				inst(dl.AND, self.x_counter.is_done, self.pool_multicycle_counter.is_done))
			self.channel_counter.set_increment_condition(
				inst(dl.AND, self.y_counter.is_done, self.x_counter.is_done, self.pool_multicycle_counter.is_done))

			# This is where pre_modulo_addr_add_delay gets added
			# note that each of the inputs is delayed and then the output is delayed for a
			# total of 2 cycles added
			self.r_addr_pre_modulo = ((self.channel_counter.current_value.delay(1) +
				line_address_counter.current_value.delay(1) +
				self.y_counter.current_value.delay(1))).delay(1)
			self.r_addr_pre_modulo.name = "read_address_pre_modulo"

			modulo_value = inst(dl.Constant, input_channels * (vertical_stride + kernel_height), name="modulo_value")
			negative_modulo_value = inst(dl.Constant, bits=self.r_addr_pre_modulo._bit_width, value=-input_channels * (vertical_stride + kernel_height), name="negative_modulo_value")
			zeros = inst(dl.Constant, value=0, bits=negative_modulo_value._bit_width, name="read_addr_change_zeros")
			r_addr_ge_line_count = self.r_addr_pre_modulo >= modulo_value
			change = inst(dl.MuxV2, r_addr_ge_line_count, zeros, negative_modulo_value, name="read_addr_change")
			self.r_addr = (change + self.r_addr_pre_modulo)[self.ia_buffer.r_addrs[0]._bit_width-1:0]
			self.r_addr.name = "read_address"

			states = ["IDLE", "READING_LINE", "DONE_READING_LINE"]
			edges = [
				["IDLE", "READING_LINE", self.go],
				["READING_LINE", "DONE_READING_LINE", self.done_line],
				["DONE_READING_LINE", "IDLE"]]
			self.state_machine = inst(dl.StateMachine, states, edges, None)
			self.pool_multicycle_counter.set_increment_condition(self.state_machine.c["is_reading_line"])
			self.x_counter.set_increment_condition(inst(dl.AND, self.state_machine.c["is_reading_line"], self.pool_multicycle_counter.is_done))

		self.ia_buffer.r_addrs[0].set_driver(self.r_addr)
		self.ia_buffer.read.set_driver(inst(dl.AND, self.state_machine.is_reading_line, self.x_counter.is_reset_value).delay(address_computation_delay))
		self.ibc.finished_reading_line.set_driver(self.state_machine.c["is_done_reading_line"])
		self.ibc.started_reading_line.set_driver(inst(dl.AND, self.go, self.state_machine.c["is_idle"]))
		pooling_sr = inst(dl.ParallelLoadShiftRegister, bits, input_width + np.sum(padding[2]), signed=signed, name="pooling_sr")

		piped_shift_sr_fanouts = inst(dl.PipelinedFanout,
			inst(dl.AND, inst(dl.NOT, self.x_counter.is_reset_value), self.pool_multicycle_counter.is_reset_value).delay(address_computation_delay + memory_read_delay - fanout_levels),
			*[s for s in pooling_sr.shifts],
			levels=fanout_levels,
			name="shift_sr_fanout_pipe")
		piped_load_sr_fanouts = inst(dl.PipelinedFanout,
			self.load_sr,
			*[l for l in pooling_sr.loads],
			levels=fanout_levels,
			name="load_sr_fanout_pipe")

		self.new_pool = inst(dl.AND,
			self.state_machine.c["is_reading_line"],
			self.pool_multicycle_counter.is_reset_value,
			self.x_counter.is_reset_value,
			self.y_counter.is_reset_value).delay(address_computation_delay + memory_read_delay + sr_write_delay - fanout_levels)
		self.new_pool.name = "new_pool"
		zero = inst(dl.Constant, bits=1, value=0)
		with dl.ParamScope(reset_driver=zero):
			self.pool_valid = self.new_pool.delay(single_pool_delay + 1 + cast_delay)
		new_pool_fanout = [inst(dl.Logic, 1, name="new_pool_fanout") for _ in range(output_width)]
		pool_valid_fanout = [inst(dl.Logic, 1, name="pool_valid_fanout") for _ in range(output_width)]

		piped_new_pool_fanouts = inst(dl.PipelinedFanout,
			self.new_pool,
			*new_pool_fanout,
			levels=fanout_levels,
			name="new_pool_fanout_pipe")
		piped_pool_valid_fanouts = inst(dl.PipelinedFanout,
			self.pool_valid,
			*pool_valid_fanout,
			levels=fanout_levels,
			name="pool_valid_fanout_pipe")

		r_data = self.ia_buffer.outputs[0]

		for srdin, r in zip(pooling_sr.input_data[padding[2][0]:padding[2][0]+input_width], r_data):
			srdin.set_driver(r)
		with dl.ModuleScope(pooling_sr):
			for srdin in pooling_sr.input_data[:padding[2][0]] + pooling_sr.input_data[padding[2][0]+input_width:]:
				if signed:
					srdin.set_driver(inst(dl.Constant, value=1<<(srdin._bit_width-1), signed=True, bits=srdin._bit_width))
				else:
					srdin.set_driver(inst(dl.Constant, value=0, bits=srdin._bit_width))

		self.output_valid.set_driver(self.pool_valid)
		self.pooling_flops = []
		for i in range(output_width):
			input_index = kernel_width - 1 + i * stride[1]
			input_reg = inst(dl.Logic, pooling_sr.sr[input_index]._bit_width, pooling_sr.sr[input_index], signed=signed, name="max_input_" + str(i))
			load = pooling_sr.loads[input_index]
			new_load = new_pool_fanout[i]
			#Should have same bits as input otherwise it messes up the cast later on
			self.pooling_flops.append(inst(dl.Flop, bits=input_act_bit_spec["width"], signed=signed, name="pooling_flop"))
			can_load_gt = inst(dl.OR, pooling_sr.shifts[input_index], pooling_sr.loads[input_index]).delay(1)
			can_load_gt.name = "can_load_gt"
			select = inst(dl.OR, inst(dl.AND, can_load_gt, input_reg > self.pooling_flops[i]), new_load, name="select_max_" + str(i)).delay(1)

			self.pooling_flops[i].set_driver(inst(dl.MuxV2, select, self.pooling_flops[i], input_reg, signed=signed))
			# not sure why, but setting the driver is overriding the pooling flop signed property, so I am explicitly
			# resetting it here.
			self.pooling_flops[i].signed = True
			self.outputs[i].set_driver(cast_fixed(self.pooling_flops[i].delay(cast_delay), input_act_bit_spec, output_act_bit_spec))
			self.outputs_valid[i].set_driver(pool_valid_fanout[i])


		self._set_control_to_input_delay(self.ibc.write_counter_increment_delay + self.ia_buffer.control_to_input_delay)
		self._set_control_valid_to_output_valid_delay(fanout_levels)

		self.space_to_write_line.set_driver(self.ibc.space_to_write_line)
		self.requires_driver = self.inputs + [self.write, self.go]
		ibc_is_idle = inst(dl.Logic, bits=1, name="ibc_is_idle")
		ibc_is_idle.set_driver(self.ibc.state_machine.c["is_idle"])
		self.debug_stall = inst(dl.AND,
			ibc_is_idle,
			inst(dl.NOT, dl.Verilog.current_params.kwargs["reset"]),
			inst(dl.NOT, self.space_to_write_line),
			inst(dl.NOT, self.ibc.has_full_kernel),
			inst(dl.NOT, self.state_machine.c["is_reading_line"]))

		self.cred_inc.set_driver(inst(dl.AND, self.state_machine.is_idle.delay(2), inst(dl.NOT, self.state_machine.is_idle.delay(1))))
		self.cred_dec.set_driver(self.ibc.cred_dec)
		self.cred_dec_final.set_driver(self.ibc.done_image.delay(2))
		self.cred_dec_amount = self.ibc.cred_dec_amount
		self.cred_dec_final_amount = self.ibc.cred_dec_final_amount
		if node.inputs[0]._from.module is not None:
			self.cred_max[node.inputs[0]._from.module.name] = self.ibc.cred_max
		else:
			self.cred_max["None"] = self.ibc.cred_max
		self.cred_padding = padding[1] #Not 100% sure about this

		if (padding[1] != [0,0]):
			self.cred_padding_unpause = inst(dl.Logic, 1, name="credit_padding_unpause")
			self.cred_padding_unpause.set_driver(inst(dl.AND, self.ibc.in_padding_next.delay(2), inst(dl.NOT, self.ibc.in_padding_next.delay(1))))

	def get_custom_verilog_str(self, t="  "):
		verilog = super(MaxPool, self).get_custom_verilog_str(t)
		'''
		verilog += "  always @(posedge clock_0) begin\n"
		verilog += "    if (" + self.debug_stall.name + ") begin\n"
		verilog += '      $display("' + self.name + ' deadlocked because there is no space and it does not have enough lines to write one out %0t", $time);\n'
		verilog += "    end\n"
		#verilog += "    if (!" + self.can_write_line.name + ") begin\n"
		#verilog += '      $display("' + self.name + ' cannot write");\n'
		#verilog += "    end\n"
		verilog += "  end\n"'''
		return verilog

class Placeholder(NNStage):
	"""
	Components:
		we_shifters: This seems to be an absolutely ludicrous way to implement valid signals
		             should probably be re-written

	"""
	kind = "placeholder"
	def instantiate(self, node, inst, build_data_path=True, **kwargs):
		if "interface_width" in kwargs:
			interface_width = kwargs["interface_width"]
		else:
			interface_width = 256
		should_deserialize = node.get_parallel_acts_per_memory(interface_width) <= self.get_output_act(dim="width")
		self.buffered_layer = None

		input_clock = None
		input_reset = None
		if "input_clock" in kwargs:
			input_clock = kwargs["input_clock"]
		if "input_reset" in kwargs:
			input_reset = kwargs["input_reset"]
		if should_deserialize:
			self._inst_deserializer_new(node, inst, input_clock, input_reset,interface_width)
		else:
			self._inst_padded(node, inst,input_clock,input_reset,interface_width = interface_width)



	"""
	For Multi Chip development, for now, if the width of the data coming is less than the interface
	width, it will be padded with zeros and the placeholder has to select the valid number of
	outputs from that large line (they will be the lowest bits). This placeholder type doesn't have an
	input buffer, it just passes the outputs from the upstream module (FIFO) to the downstream layer  
	"""
	def _inst_padded(self, node, inst, input_clock=None, input_reset=None,interface_width=256):
		if input_clock == None:
			input_clock = dl.Verilog.current_params.kwargs["clock"]
		if input_reset == None:
			input_reset = dl.Verilog.current_params.kwargs["reset"]
		self.shifter_depth = 0
		output_clock = dl.Verilog.current_params.kwargs["clock"]
		output_reset = dl.Verilog.current_params.kwargs["reset"]
		input_bit_spec = dl.Verilog.current_params.kwargs["input_bit_spec"]
		input_bits = 16 #should be automated
		parallel_acts_per_ram = self.get_output_act(dim="width")
		self.parallel_acts_per_ram = parallel_acts_per_ram

		act_width = self.get_output_act(dim="width")
		act_height = self.get_output_act(dim="height")
		act_channels = self.get_output_act(dim="channels")
		_, _, act_bits = self.get_bit_widths()
		placeholder_bit_spec = self.get_bit_spec("output")
		input_width = interface_width
		scalars_per_transfer = act_width


		self.write = inst(dl.Logic, bits=1, name="write")
		self.writes = [self.write]
		self.write_addr = inst(dl.Logic, bits=20, name="write_addr")
		self.inputs = [inst(dl.Logic, bits=input_width, name="input_0")]
		input_scalars = [self.inputs[0][input_bits*(i+1)-1:input_bits*i] for i in range(scalars_per_transfer)]
		for i, o in enumerate(input_scalars):
			self.outputs[i].set_driver(o)

		self.output_valid.set_driver(self.write)
		for i in range(len(input_scalars)):
			self.outputs_valid[i].set_driver(self.writes[0])


		self.space_to_write_line = inst(dl.Logic, 1, name="space_to_write_line")
		self.space_to_write_line.set_driver(self.can_write_line, delay=0)

		self._set_control_to_input_delay(0)
		self._set_control_valid_to_output_valid_delay(0)
		
		self.guarantees_output_valid_low_in_cycles = False
		self.requires_driver = [self.write, self.can_write_line]



	def _inst_deserializer_new(self, node, inst, input_clock=None, input_reset=None,interface_width=256):
		if input_clock == None:
			input_clock = dl.Verilog.current_params.kwargs["clock"]
		if input_reset == None:
			input_reset = dl.Verilog.current_params.kwargs["reset"]
		output_clock = dl.Verilog.current_params.kwargs["clock"]
		output_reset = dl.Verilog.current_params.kwargs["reset"]
		input_bit_spec = dl.Verilog.current_params.kwargs["input_bit_spec"]
		input_bits = np.sum(list(input_bit_spec.values()))
		parallel_acts_per_ram = interface_width // input_bits
		self.parallel_acts_per_ram = parallel_acts_per_ram

		act_width = self.get_output_act(dim="width")
		act_height = self.get_output_act(dim="height")
		act_channels = self.get_output_act(dim="channels")
		_, _, act_bits = self.get_bit_widths()
		placeholder_bit_spec = self.get_bit_spec("output")
		input_width = parallel_acts_per_ram * input_bits

		scalars_per_transfer = interface_width // input_bits
		shifter_depth = math.ceil(act_width / float(parallel_acts_per_ram))
		self.shifter_depth = shifter_depth

		self.write = inst(dl.Logic, bits=1, name="write")
		self.writes = [self.write]
		self.inputs = [inst(dl.Logic, bits=input_width, name="input_0")]
		self.input_lines = shifter_depth * act_channels * act_height
		self.write_addr = inst(dl.Logic, bits=clog2(self.input_lines-1), name="write_addr")
		self.weight_transfer_in = inst(dl.Logic, bits=1, name="weight_transfer_in") # Data going in the placeholder is weight data
		self.increment_weight_in = inst(dl.Logic, bits=1, name="increment_weight_in") # One frame of weights is completed at the input of the placeholder
		self.weight_transfer_out = inst(dl.Logic, bits=1, name="weight_transfer_out") # Data going in the placeholder is weight data
		self.increment_weight_out = inst(dl.Logic, bits=1, name="increment_weight_out") # One frame of weights is completed at the output of the placeholder
		input_lines = self.input_lines
		with dl.ParamScope(arithmetic_bit_spec=input_bit_spec):
			input_scalars = [self.inputs[0][input_bits*(i+1)-1:input_bits*i] for i in range(scalars_per_transfer)]

		ram_width = scalars_per_transfer * placeholder_bit_spec["width"]
		max_fan = 4
		end_fan = math.ceil((ram_width * input_lines * 2) / 20000)
		ram_fanout_depth = 0#math.ceil(math.log(end_fan, max_fan))

		with dl.ParamScope(clock=input_clock, reset=input_reset):
			self._build_input_bias_selection(inst, self.write, self.write_addr,interface_width=interface_width)

			input_adjustments = dl.Verilog.current_params.kwargs["input_adjustments"]
			pre_scaled_inputs = [self._scale_input(inst, i, input_adjustments["pre_scale"]) for i in input_scalars]
			biased_inputs = [p.delay(2) + self.input_bias for p in pre_scaled_inputs]
			post_scaled_inputs = [self._scale_input(inst, b.delay(1), input_adjustments["post_scale"]) for b in biased_inputs]

			adjusted_inputs = [p.cast(placeholder_bit_spec).delay(2) for p in post_scaled_inputs]
			write_base_selection_counter = inst(dl.Counter,
				reset_value=0,
				increment_value=1,
				end_value=1,
				name="write_base_selection_counter")
			write_bases = [
				inst(dl.Constant, value=0, name="write_base_1"),
				inst(dl.Constant, value=self.input_lines, name="write_base_2")]

			current_write_base = inst(dl.MuxV2,
				write_base_selection_counter.current_value,
				*write_bases)
			ram_write_addr = current_write_base + self.write_addr.delay(1)

			ram_write = self.write.delay(5)
			ram_write_addr_counter = inst(dl.Counter,
				reset_value=0,
				increment_value=1,
				end_value=self.input_lines*2-1,
				name="ram_write_addr_counter")
			ram_write_addr_counter.set_increment_condition(ram_write)
			"""full_input_being_written = inst(dl.AND, ram_write.delay(2),
				inst(dl.OR,
					inst(dl.EQ, ram_write_addr_counter.current_value.delay(2), input_lines-1),
					inst(dl.EQ, ram_write_addr_counter.current_value.delay(2), input_lines*2-1)))
			input_start_writing = inst(dl.AND, ram_write.delay(2),
				inst(dl.OR,
					inst(dl.EQ, ram_write_addr_counter.current_value.delay(2), 0),
					inst(dl.EQ, ram_write_addr_counter.current_value.delay(2), input_lines)))"""

			full_input_being_written = inst(dl.AND, self.write.delay(2),
					inst(dl.EQ, self.write_addr.delay(2), input_lines-1))
			input_start_writing = inst(dl.AND, self.write.delay(2),
					inst(dl.EQ, self.write_addr.delay(2), 0))
			write_base_selection_counter.set_increment_condition(full_input_being_written)

			self.increment_weight_in = inst(dl.EQ, self.write_addr, inst(dl.Constant, bits=self.write_addr._bit_width, value=4703, name="last_line_in_image"))
			ram_input = inst(dl.MuxV2, self.weight_transfer_in, inst(dl.Concat, *adjusted_inputs, name="concatenated_inputs").delay(0), self.inputs[0].delay(5))#, max_fan=max_fan, final_fan=end_fan)
			ram_write_addr = ram_write_addr.delay(4)
			#ram_write = ram_write.delay(ram_fanout_depth)#, max_fan=max_fan, final_fan=end_fan)
			#ram_write_addr = ram_write_addr_counter.current_value.delay(0)#, max_fan=max_fan, final_fan=end_fan)


		new_input_cdc = inst(dl.SingleCycleControlDomainCrossing, input_clock, output_clock, clock_a_reset=input_reset, clock_b_reset=output_reset)
		new_input_cdc.control_in.set_driver(full_input_being_written)
		have_input = new_input_cdc.control_out

		start_input_cdc = inst(dl.SingleCycleControlDomainCrossing, input_clock, output_clock, clock_a_reset=input_reset, clock_b_reset=output_reset)
		start_input_cdc.control_in.set_driver(input_start_writing)
		new_input_writing = start_input_cdc.control_out

		read_enable = inst(dl.Logic, bits=1, name="read_enable")
		modified_read_enable = inst(dl.Logic, bits=1, name="modified_read_enable") #This goes high more than once on the same address to send it in the narrower write path
		read_address_counter = inst(dl.Counter,
			reset_value=0,
			increment_value=1,
			end_value=self.input_lines*2-1,
			name="read_address_counter")
		read_address_counter.set_increment_condition(read_enable)

		input_counter = inst(dl.UpDownCounter,
			reset_value=0,
			increment_value=1,
			decrement_value=-1,
			end_value=2,
			name="input_counter")
		input_counter.set_increment_condition(have_input.delay(3))
		input_counter.set_decrement_condition(inst(dl.AND,
			read_enable.delay(1),
			inst(dl.OR,
				inst(dl.EQ, read_address_counter.current_value, self.input_lines - shifter_depth).delay(1),
				inst(dl.EQ, read_address_counter.current_value, self.input_lines*2 - shifter_depth).delay(1))))

		partial_input_counter = inst(dl.UpDownCounter,
			reset_value=0,
			increment_value=1,
			decrement_value=-1,
			end_value=2,
			name="partial_input_counter")
		partial_input_counter.set_increment_condition(new_input_writing.delay(3))
		partial_input_counter.set_decrement_condition(inst(dl.AND,
			read_enable.delay(1),
			inst(dl.OR,
				inst(dl.EQ, read_address_counter.current_value, self.input_lines - 1).delay(1),
				inst(dl.EQ, read_address_counter.current_value, self.input_lines*2 - 1).delay(1))))

		transfers_per_channel_counter = inst(dl.Counter,
			reset_value=0,
			increment_value=1,
			end_value=shifter_depth-1,
			name="transfers_per_channel_counter")
		transfers_per_channel_counter.set_increment_condition(read_enable.delay(2))

		self.ram = inst(dl.FanoutRAM,
			depth=self.input_lines*2,
			bits=ram_input._bit_width,
			read_clock=output_clock,
			write_clock=input_clock,
			name="input_buffer")
		self.ram.w_en.set_driver(ram_write)
		self.ram.w_addr.set_driver(ram_write_addr)
		self.ram.r_addr.set_driver(read_address_counter.current_value)#, max_fan=max_fan, final_fan=end_fan, final_fan_of_1=True))
		self.ram.r_en.set_driver(modified_read_enable)#, max_fan=max_fan, final_fan=end_fan, final_fan_of_1=True))
		self.ram.w_data.set_driver(ram_input)

		# Quartus removes the last stage of our address fanout and renames it to a register packed into the M20Ks, so we set
		# these settings to manually duplicate that register... which shockingly works :)
		#get_sdc_filter_str = lambda ab: "*|" + "|".join([o.name + "_i" for o in reversed(self.ram.get_owner_list()[:-2])]) + "|*|address_reg_" + ab + "[*]"
		#dl.Verilog.current_circuit.add_quartus_setting_lambda(
		#	lambda: "set_instance_assignment -name DUPLICATE_REGISTER " + str(math.ceil(end_fan/max_fan)) + " -to " + '"' + get_sdc_filter_str("a") + '"')
		#dl.Verilog.current_circuit.add_quartus_setting_lambda(
		#	lambda: "set_instance_assignment -name DUPLICATE_REGISTER " + str(math.ceil(end_fan/max_fan)) + " -to " + '"' + get_sdc_filter_str("b") + '"')

		output_data_shifter = inst(dl.BasicShiftRegister,
			bits=self.ram.r_data._bit_width,
			depth=shifter_depth,
			name="output_data_shifter")
		output_data_shifter.d.set_driver(self.ram.r_data)
		individuals = self.get_individual_values_from_combined(output_data_shifter.sr, True)
		for i, o in enumerate(reversed(individuals)):
			shifter_index = i // parallel_acts_per_ram
			self.outputs[i].set_driver(o)
		output_will_be_valid = inst(dl.EQ, transfers_per_channel_counter.current_value, 1).delay(self.ram.read_delay-2)
		output_will_be_valid_or_weights = inst(dl.MuxV2, self.weight_transfer_out, output_will_be_valid, inst(dl.Constant, bits=1, value=0))
		self.output_valid.set_driver(output_will_be_valid_or_weights)
		inst(dl.PipelinedFanout,
			output_will_be_valid_or_weights,
			*self.outputs_valid,
			levels=shifter_depth-1,
			name="output_valid_fanout")


		self.have_enough_data_to_read_one_line = (input_counter.current_value > 0).delay(2)
		self.done_line = inst(dl.Logic, 1, name="done_line")
		self.within_line_counter = inst(dl.Counter,
			reset_value=0,
			increment_value=1,
			end_value=act_channels*shifter_depth-1,
			name="within_line_counter")
		self.within_line_counter.set_increment_condition(read_enable)
		self.done_line.set_driver(self.within_line_counter.done_and_incrementing)

		# When the can_write_line signal is asserted and the placeholder has enough data to write a full
		# set of input channels to the next layer, it can move to WRITING_LINE, where it stays until it
		# finishes one full set of input channels.  It then goes to WAITING_PERIOD, which is simply a state
		# where it waits before it is allowed to check self.can_write_line again.  This waiting period
		# is necessary for Placeholder because there are usually far fewer input channels in the first
		# layer, thus there is not enough time to allow the can_write_line signal to update
		self.waiting_period_counter = inst(dl.Counter,
			reset_value=0,
			increment_value=1,
			end_value=10,
			name="waiting_period_counter")
		states = ["IDLE", "WRITING_LINE", "WAITING_PERIOD"]
		edges = [
			["IDLE", "WRITING_LINE", inst(dl.AND, self.can_write_line, self.have_enough_data_to_read_one_line)],
			["WRITING_LINE", "WAITING_PERIOD", self.done_line],
			["WAITING_PERIOD", "IDLE", self.waiting_period_counter.is_done]]
		self.state_machine = inst(dl.StateMachine, states, edges, None)
		self.waiting_period_counter.set_increment_condition(self.state_machine.c["is_waiting_period"].delay(1))


		global real_ip_sim
		global WRITE_PATH_NARROW_CONSTANT #Basically we send out the width of the write path is 240/WRITE_PATH_NARROW_CONSTANT
		stall_counter_max_value = 64
		if real_ip_sim:
			WRITE_PATH_NARROW_CONSTANT = 2
			stall_counter_max_value = WRITE_PATH_NARROW_CONSTANT
		
		HBM_DATA_WIDTH=240 #NOTE: changing this variable alone will not change the data width
		assert((HBM_DATA_WIDTH % WRITE_PATH_NARROW_CONSTANT) == 0)
		self.selected_bits = inst(dl.Logic, bits=math.ceil(HBM_DATA_WIDTH/WRITE_PATH_NARROW_CONSTANT), name="weights_selected_bits")
		self.selected_bits_valid = inst(dl.Logic, bits=1, name="weights_selected_bits_valid")		
		weight_stall_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=math.ceil(stall_counter_max_value/WRITE_PATH_NARROW_CONSTANT)-1, name="weight_stall_counter")

		weight_mod_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=WRITE_PATH_NARROW_CONSTANT-1, name="weight_mod_3_counter")
		weight_stall_counter.set_increment_condition(self.state_machine.c["is_writing_line"])
		weight_mod_counter.set_increment_condition(weight_stall_counter.done_and_incrementing)
		divided_sections = []
		for i in range(WRITE_PATH_NARROW_CONSTANT):
			divided_sections.append(self.ram.r_data[((i+1)*math.ceil(HBM_DATA_WIDTH/WRITE_PATH_NARROW_CONSTANT))-1:(i*math.ceil(HBM_DATA_WIDTH/WRITE_PATH_NARROW_CONSTANT))])
		self.selected_bits.set_driver(inst(dl.MuxV2, weight_mod_counter.current_value.delay(self.ram.read_delay), *divided_sections))
		self.selected_bits_valid = inst(dl.AND, weight_stall_counter.is_done, self.state_machine.c["is_writing_line"])
		weight_read_enable = inst(dl.AND, weight_stall_counter.is_done, weight_mod_counter.is_done, self.state_machine.c["is_writing_line"])
		read_enable.set_driver(inst(dl.MuxV2, self.weight_transfer_out, self.state_machine.c["is_writing_line"], weight_read_enable))
		modified_read_enable.set_driver(inst(dl.MuxV2, self.weight_transfer_out, self.state_machine.c["is_writing_line"], inst(dl.AND, weight_stall_counter.is_done, self.state_machine.c["is_writing_line"])))
		self.increment_weight_out.set_driver(inst(dl.AND, weight_mod_counter.is_done, weight_stall_counter.is_done))

		self.space_to_write_line = inst(dl.Logic, 1, name="space_to_write_line")
		self.space_to_write_line.set_driver(inst(dl.NOT, inst(dl.EQ, partial_input_counter.current_value, 2)), delay=2)

		self._set_control_to_input_delay(0)
		self._set_control_valid_to_output_valid_delay(shifter_depth-1)
		self.guarantees_output_valid_low_in_cycles = False

		self.requires_driver = [self.write, self.can_write_line]#, self.write_address]


	def _scale_input(self, inst, _input, scale):
		input_adjustments = dl.Verilog.current_params.kwargs["input_adjustments"]
		scale_fractional_rescale = 2 ** input_adjustments["bit_spec"]["frac"]
		scale_signed = input_adjustments["bit_spec"]["sign"] == 1
		scale_bits = np.sum(list(input_adjustments["bit_spec"].values()))

		scale_constant = inst(dl.Constant,
			bits=scale_bits,
			value=int(scale * scale_fractional_rescale),
			signed=scale_signed,
			arithmetic_bit_spec=input_adjustments["bit_spec"],
			name="input_adjustment_scale")
		return inst(dl.MUL, _input, scale_constant)




	def _build_input_bias_selection(self, inst, new_input, input_address,interface_width=256):
		act_width = self.get_output_act(dim="width")
		act_height = self.get_output_act(dim="height")
		act_channels = self.get_output_act(dim="channels")

		input_bit_spec = dl.Verilog.current_params.kwargs["input_bit_spec"]
		input_bits = np.sum(list(input_bit_spec.values()))
		scalars_per_transfer = interface_width // input_bits
		transfers_per_channel = math.ceil(act_width / scalars_per_transfer)

		input_adjustments = dl.Verilog.current_params.kwargs["input_adjustments"]
		input_bias_ram_data = input_adjustments["channel_biases"]
		bias_fractional_rescale = 2 ** input_adjustments["bit_spec"]["frac"]
		bias_signed = input_adjustments["bit_spec"]["sign"] == 1
		bias_bits = np.sum(list(input_adjustments["bit_spec"].values()))

		is_p2 = is_power_of_2(len(input_bias_ram_data))
		if is_p2:
			hi = int(math.log2(float(len(input_bias_ram_data))))
			if hi == 0:
				current_bias_index = inst(dl.Constant, bits=1, value=0, name="current_bias_index")
			else:
				current_bias_index = input_address[hi-1:0].delay(2)
		else:
			content_list = np.arange(len(input_bias_ram_data))[...,np.newaxis]
			content_list = np.tile(content_list, [math.ceil(act_height * act_channels / len(input_bias_ram_data)), transfers_per_channel])
			content_list = np.reshape(content_list, [-1]).tolist()
			index_rom = inst(dl.ROM, content_list=content_list, bits=clog2(len(input_bias_ram_data)))

			index_rom.r_addr.set_driver(input_address)
			index_rom.r_en.set_driver(new_input)
			current_bias_index = index_rom.r_data.delay(1)

		with dl.ParamScope(arithmetic_bit_spec=input_adjustments["bit_spec"]):
			self.input_bias_mux = inst(dl.MuxV2,
				current_bias_index,
				*[inst(dl.Constant, value=int(v * bias_fractional_rescale), bits=bias_bits, signed=bias_signed, name="channel_bias_" + str(i))
					for i,v in enumerate(input_adjustments["channel_biases"])],
				name="input_bias_mux")

		with dl.ParamScope(name="input_bias", arithmetic_bit_spec=input_adjustments["bit_spec"]):
			self.input_bias = self.input_bias_mux.delay(0)





	def _inst_deserializer(self, node, inst, input_clock=None):
		if input_clock == None:
			input_clock = dl.Verilog.current_params.kwargs["clock"]
		parallel_acts_per_ram = node.get_parallel_acts_per_memory(256)
		act_width = self.get_output_act(dim="width")
		act_height = self.get_output_act(dim="height")
		act_channels = self.get_output_act(dim="channels")
		_, _, act_bits = self.get_bit_widths()
		buffer_width = parallel_acts_per_ram * act_bits

		shifter_depth = math.ceil(act_width / float(parallel_acts_per_ram))
		control_loop_cycles = (shifter_depth - 1) // 2
		read_delay = 2
		self.shifter_depth = shifter_depth
		self.parallel_acts_per_ram = parallel_acts_per_ram

		self.write = inst(dl.Logic, bits=1, name="write")
		self.writes = [self.write]
		self.inputs = [inst(dl.Logic, bits=buffer_width, name="input_0")]
		self.input_lines = shifter_depth * act_channels * act_height


		with dl.ParamScope(bits=1, signed=False):
			self.writing_line = inst(dl.Logic, name="writing_line")
			self.we_shifters  = [inst(dl.ShiftRegister, depth=(shifter_depth-i)+1, name="we_shifter") for i in range(shifter_depth)]
			self.next_we      = inst(dl.ShiftRegister, depth=shifter_depth, name="next_we")
			self.we_s         = [inst(dl.AND, self.writing_line, f, name="write_enable") for f in self.next_we.sr]
			self.next_we.d.set_driver(self.next_we.q)
			self.output_valid.set_driver(self.next_we.q)
			for shifter in self.we_shifters:
				with dl.ModuleScope(shifter):
					zero = inst(dl.Constant, 0, name="zero")
					for flop in shifter.sr:
						flop.set_reset_driver(zero)
			with dl.ModuleScope(self.next_we):
				one = inst(dl.Constant, 1, name="one")
				zero = inst(dl.Constant, 0, name="zero")
				for i,flop in enumerate(self.next_we.sr):
					if i == 0:
						flop.set_reset_driver(one)
					else:
						flop.set_reset_driver(zero)
		self.input_ram = inst(dl.FIFO,
			bits=buffer_width,
			depth=shifter_depth * act_channels * act_height * 2,
			almost_empty=shifter_depth*act_channels-1,
			almost_full=shifter_depth * act_channels * act_height-1,
			name="input_buffer")
		self.write_address = inst(dl.Logic, bits=self.input_ram.write_ptr.current_value._bit_width)
		self.write_address.set_driver(self.input_ram.write_ptr.current_value)
		self.input_ram.w_en.set_driver(self.write)
		self.input_ram.w_data.set_driver(self.inputs[0])

		with dl.ParamScope(bits=buffer_width):
			self.write_data = self.input_ram.w_data
			self.input_data_shifter = inst(dl.ShiftRegister, depth=shifter_depth, name="input_data_shifter")
		for we_shifter, we in zip(self.we_shifters, self.we_s):
			we_shifter.d.set_driver(we)

		individuals = self.get_individual_values_from_combined(self.input_data_shifter.sr, True)
		for i, o in enumerate(reversed(individuals)):
			shifter_index = i // parallel_acts_per_ram
			self.outputs[i].set_driver(o)
			self.outputs_valid[i].set_driver(self.we_shifters[shifter_index].q)

		self.have_enough_data_to_compute_one_line = inst(dl.NOT, self.input_ram.almost_empty).delay(2)
		self.done_line = inst(dl.Logic, 1, name="done_line")
		self.input_channel_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=act_channels-1, name="input_channel_counter")
		self.input_channel_counter.set_increment_condition(self.next_we.sr[shifter_depth-1-control_loop_cycles])
		self.done_line.set_driver(self.input_channel_counter.done_and_incrementing)

		states = ["IDLE", "WRITING_LINE"]
		edges = [
			["IDLE", "WRITING_LINE", inst(dl.AND, self.can_write_line, self.have_enough_data_to_compute_one_line)],
			["WRITING_LINE", "IDLE", self.done_line]]


		self.state_machine = inst(dl.StateMachine, states, edges, None)
		with dl.ParamScope(reset_driver=inst(dl.Constant, bits=1, value=0)):
			is_writing_line = self.state_machine.c["is_writing_line"].delay(control_loop_cycles)
		self.next_we.should_shift.set_driver(is_writing_line)
		self.space_to_write_line = inst(dl.Logic, 1, name="space_to_write_line")
		self.space_to_write_line.set_driver(inst(dl.NOT, self.input_ram.almost_full), delay=2)
		self.input_ram.r_en.set_driver(is_writing_line)
		self.input_data_shifter.d.set_driver(self.input_ram.r_data)
		self.writing_line.set_driver(is_writing_line)

		self._set_control_to_input_delay(read_delay)
		self._set_control_valid_to_output_valid_delay(read_delay)
		self.guarantees_output_valid_low_in_cycles = False

		self.requires_driver = [self.write, self.write_data, self.can_write_line]#, self.write_address]

	def _inst_serializer(self, node, inst):
		parallel_acts_per_ram = node.get_parallel_acts_per_memory(256)
		act_width = self.get_output_act(dim="width")
		parallel_acts_per_ram -= parallel_acts_per_ram % act_width
		act_height = self.get_output_act(dim="height")
		act_channels = self.get_output_act(dim="channels")
		_, _, act_bits = self.get_bit_widths()
		buffer_width = parallel_acts_per_ram * act_bits

		lines_per_group = parallel_acts_per_ram // act_width

		groups_per_oc_group = math.ceil(act_channels / float(lines_per_group))

		read_delay = 2
		self.shifter_depth = lines_per_group
		self.groups_per_oc_group = groups_per_oc_group
		self.parallel_acts_per_ram = parallel_acts_per_ram

		self.write = inst(dl.Logic, bits=1, name="write")
		self.writes = [self.write]
		self.inputs = [inst(dl.Logic, bits=buffer_width, name="input_0")]


		self.input_ram = inst(dl.FIFO,
			bits=buffer_width,
			depth=groups_per_oc_group * act_height * 2,
			almost_empty=groups_per_oc_group - 1,
			almost_full=groups_per_oc_group * act_height - 1,
			name="input_buffer")
		self.input_ram.w_en.set_driver(self.write)
		self.input_ram.w_data.set_driver(self.inputs[0])

		self.write_data = self.input_ram.w_data
		serialization_sr = inst(dl.ParallelLoadShiftRegister,
			depth=lines_per_group,
			bits=act_width*act_bits,
			name="serialization_sr")
		self.is_idle = inst(dl.Logic, bits=1, name="is_idle")
		self.is_writing_line = inst(dl.Logic, bits=1, name="is_writing_line")
		with dl.ParamScope(reset=self.is_idle):
			shift_counter = inst(dl.Counter,
				reset_value=0,
				increment_value=1,
				end_value=len(serialization_sr.input_data)-1,
				name="shift_counter")
			shift_counter.set_increment_condition(self.is_writing_line)
		oc_counter = inst(dl.Counter,
			reset_value=0,
			increment_value=1,
			end_value=act_channels-1,
			name="oc_counter")
		oc_counter.set_increment_condition(self.is_writing_line)

		self.should_read = inst(dl.AND, self.is_writing_line.delay(2), shift_counter.is_reset_value.delay(2))
		self.should_write_out = inst(dl.AND, self.is_writing_line, shift_counter.is_reset_value.delay(2))
		self.should_load_sr = self.should_read.delay(read_delay)
		line_width = act_width*act_bits
		for i,(s,l,d) in enumerate(zip(serialization_sr.shifts, serialization_sr.loads, serialization_sr.input_data)):
			d.set_driver(self.input_ram.r_data[(i+1)*line_width-1:i*line_width].delay(read_delay-1))
			l.set_driver(self.should_load_sr)


		self.have_enough_data_to_compute_one_line = inst(dl.NOT, self.input_ram.almost_empty).delay(2)
		self.done_line = inst(dl.Logic, 1, name="done_line")
		self.done_line.set_driver(oc_counter.is_done)

		states = ["IDLE", "WRITING_LINE"]
		edges = [
			["IDLE", "WRITING_LINE", inst(dl.AND, self.can_write_line, self.have_enough_data_to_compute_one_line)],
			["WRITING_LINE", "IDLE", self.done_line]]


		self.state_machine = inst(dl.StateMachine, states, edges, None)
		self.is_writing_line.set_driver(self.state_machine.c["is_writing_line"])
		self.is_idle.set_driver(self.state_machine.c["is_idle"])
		self.space_to_write_line = inst(dl.Logic, 1, name="space_to_write_line")
		self.space_to_write_line.set_driver(inst(dl.NOT, self.input_ram.almost_full), delay=2)
		self.input_ram.r_en.set_driver(self.should_read)

		self._set_control_to_input_delay(read_delay)
		self._set_control_valid_to_output_valid_delay(2+read_delay)
		self.guarantees_output_valid_low_in_cycles = False
		self.output_valid.set_driver(self.is_writing_line)
		for v in self.outputs_valid:
			v.set_driver(self.is_writing_line.delay(2+read_delay+1))
		for i,o in enumerate(self.outputs):
			o.set_driver(serialization_sr.sr[-1][(i+1)*act_bits-1:i*act_bits])

		self.requires_driver = [self.write, self.write_data, self.can_write_line]#, self.write_address]

	def get_instance_comment(self, t="  "):
		comment = "Shifter Depth: " + str(self.shifter_depth) + "\n"
		comment += "Parallel Acts Per RAM: " + str(self.parallel_acts_per_ram) + "\n"
		return super(Placeholder, self).get_instance_comment(t=t) + v_comment_string(comment, t=t)

	def get_custom_verilog_str(self, t="  "):
		verilog = super(Placeholder, self).get_custom_verilog_str(t)
		'''
		verilog += "  always @(posedge clock_0) begin\n"
		verilog += "    if (" + self.output_valid.name + "|| " + self.write.name + ") begin\n"
		verilog += '      $display("%d", ' + self.stored_lines_counter_current_value.name + ');\n'
		verilog += "    end\n"
		verilog += "  end\n"'''
		return verilog

class SequentialDistributedActivationBuffer(dl.Module):
	"""
	A type of input activation buffer that saves registers and routing
	resources by serializing the fanout of control and data signals
	to the individual buffers.

	Essentially this activation buffer takes control signals and
	fans them out like this:
	::
		R indicates a register
		M indicates a memory

		w_addr->-R--R--R--R--R--R
		         |  |  |  |  |  |
		         v  v  v  v  v  v
		         M  M  M  M  M  M
	
	Instead of like this:
	::
		                 |-R-->M
		                 |-R-->M
		            |-R--|-R-->M
		w_addr->-R--|
		            |-R--|-R-->M
		                 |-R-->M
		                 |-R-->M

	The sequential method is a much more efficient way to distribute inputs
	to the memories, but it can't be used in all situations.  The primary
	issue is that our memories receive inputs at different times.  In the
	context of our convolution module this means we can't use the sequential
	method if the memories have x muxes at the outputs.  It also means we
	need additional hardware (shift registers) at the outputs of the DSP
	chains to re-align the output data.

	In general this method is less flexible, so it should really only be
	used when we really need it.  In the case where our activation buffers
	are built with MLABs we see that we run out of registers and fast wiring
	resources around the activation buffers.  There are a lot of reasons why
	this happens.  Primarily:
		* We tend to use MLABs when we unroll a lot of input channels
		this spreads out the DSPs fed by memories that share the same
		control signals.  This means that we need more registers or
		more high speed wires.
		* The MLABs we use take up LABs that could previously have been
		used for registers
		* In some layers with a small number of weights (and in circuits
		that use almost all of the available memory blocks) the M20Ks
		get used as buffers for other layers, meaning that the region
		needs registers and wiring for completely unrelated control
		logic.

	"""
	def __init__(self, bits_per_act, kernel_height, stride, input_channels, activation_width, write_port_width, ic_groups=1, min_buffer_depth=1, activation_group_size=1, **kwargs):
		super(SequentialDistributedActivationBuffer, self).__init__("sequential_distributed_activation_buffer", kind="sequential_distributed_activation_buffer", **kwargs)
		inst = self.inst
		with dl.ModuleScope(self):
			num_buffered_lines = max(kernel_height + stride, min_buffer_depth)
			n_channels = [math.ceil(input_channels / float(ic_groups)) for _ in range(input_channels % ic_groups)]
			n_channels += [input_channels // ic_groups for _ in range(ic_groups - (input_channels % ic_groups))]
			total_buffer_depths = [n_channels[i] * num_buffered_lines for i in range(ic_groups)]
			buffer_specs = [dl.RAM.size_for_depth(bd) for bd in total_buffer_depths]
			buffer_depths = [[min(bd, (i * bs[0] % bd - 1) % bs[0] + 1) for i in range(math.ceil(bd / bs[0]))] for bd,bs in zip(total_buffer_depths, buffer_specs)]
			buffer_depths = [[np.sum(buffer_depth_list)] for buffer_depth_list in buffer_depths]
			buffer_widths = []
			has_greater_than_32 = max([bdl[0] for bdl in buffer_depths]) > 32
			if has_greater_than_32:
				output_delay = 2
			else:
				output_delay = 1

			for bd, bs in zip(total_buffer_depths, buffer_specs):
				if bs[0] <= 64 or bs[0] > 64 and bd <= 64:
					buffer_widths.append([bits_per_act * activation_group_size] * activation_width)
				else:
					print("bs", bs, bs[0])
					print(buffer_depths)
					print(min_buffer_depth)
					print(buffer_specs)
					raise Exception("SequentialDistributedActivationBuffer only supports buffers 64 words deep or less.  Exiting.")
					warnings.warn("SequentialDistributedActivationBuffer only supports buffers 64 words deep or less.  Exiting.")
					sys.exit(2)

			max_offset = min(8, activation_width)
			self.max_offset = max_offset
			parallel_sequential_fanouts = math.ceil(activation_width / max_offset)
			self.parallel_sequential_fanouts = parallel_sequential_fanouts

			self.inputs = [inst(dl.Logic, bits=bits_per_act, name="ia_buffer_write_data_" + str(j)) for j in range(activation_width)]
			self.writes = [inst(dl.Logic, bits=1, name="write_ic_group_" + str(i)) for i in range(ic_groups)]
			self.read = inst(dl.Logic, bits=1, name="read")
			self.outputs = [[inst(dl.Logic, bits=bits_per_act, name="ia_buffer_read_data_ic_group_" + str(i) + "_" + str(j)) for j in range(activation_width)] for i in range(ic_groups)]

			address_bits = [clog2(bd[0]) for bd in buffer_depths]
			self.w_addrs = [inst(dl.Logic, bits=ab, name="w_addr_ic_group_" + str(i)) for i, ab in enumerate(address_bits)]
			self.r_addrs = [inst(dl.Logic, bits=ab, name="r_addr_ic_group_" + str(i)) for i, ab in enumerate(address_bits)]
			self.outputs = [[inst(dl.Logic, bits=bits_per_act, name="ia_buffer_read_data_ic_group_" + str(i) + "_" + str(j)) for j in range(activation_width)] for i in range(ic_groups)]

			#:
			#: The output of input buffer controller is a list of write signals
			#: with at most 1 single write signal set to 1, so we will select 0
			#: whenever the write signal is 0 and select the address whenever
			#: the write signal is 1.  This way we can reduce all of the address
			#: signals to a single write address that can be shared across
			#: multiple activation buffers that will not be written at the same
			#: time.
			#:
			#: w_addr_A  w_addr_B ... w_addr_G
			#:     |         |            |
			#:   0 |       0 |          0 |
			#:   |_|       |_|          |_|
			#:  \___/-w_A \___/-w_B    \___/-w_G
			#:    \         |            /
			#:     \_______ | __________/
			#:             |||
			#:             \OR/
			#:              |
			#:           w_addr, d=bor_stages
			#:              |
			#:              V

			bor_stages = 1
			selected_w_addrs = [inst(dl.MuxV2, w, 0, wa).delay(bor_stages) for i, (w, wa) in enumerate(zip(self.writes, self.w_addrs))]
			to_or = selected_w_addrs
			while len(to_or) > 1:
				next_to_or = []
				bor_stages += 1
				for i in range(0, len(to_or), 6):
					if i == (len(to_or) - 1):
						next_to_or.append(to_or[-1].delay(1))
					else:
						next_to_or.append(inst(dl.BOR, *to_or[i:i+6]).delay(1))
				to_or = next_to_or
			w_addr = to_or[0]

			#: Letters are input channel groups
			#: Numbers are activation x indices
			#: 
			#: Input Activation Labeled by Input Channel Group
			#:        _____________
			#:       |B B B B B B B|
			#:      _____________ B|
			#:     |B B B B B B B|B|
			#:    _____________ B|B|
			#:   |A A A A A A A|B|B|
			#:  _____________ A|B|B|
			#: |A A A A A A A|A|B|B|----/
			#: |A A A A A A A|A|B|     /--- input channel group B
			#: |A A A A A A A|A|B|----/
			#: |A A A A A A A|A|
			#: |A A A A A A A|A|----/
			#: |A A A A A A A|     /--- input channel group A
			#: |A A A A A A A|----/
			#:
			#:           w_addr, d=bor_stages
			#:              |
			#:              V
			#:              |-----------------------------|
			#:              |                             |
			#:  D1          |__        D2 ... DN          |
			#:  |              |        |     |           |
			#:  | r_addr_A-v---|--v-----|-...-|-------v   |
			#:  |          |   |  |     |     |       |   |
			#:  |         |A1| | |A2|   |     |      |AN| | <-w_A, w_data, d=1
			#:  v----------||--v--||----v     v-------||--v
			#:  |         |B1| | |B2|   |     |      |BN| | <-w_B, w_data, d=1
			#:  |          |   |  |     |     |       |   |
			#:  | r_addr_B-^---|--^-----|-...-|-------^   |
			#:  | r_addr_C-v---|--v-----|-...-|-------v   |
			#:  |          |   |  |     |     |       |   |
			#:  |         |C1| | |C2|   |     |      |CN| | <-w_C, w_data, d=2
			#:  v----------||--v--||----v     v-------||--v
			#:  |         |D1| | |D2|   |     |      |DN| | <-w_D, w_data, d=2
			#:  |          |   |  |     |     |       |   |
			#:  | r_addr_D-^---|--^-----|-...-|-------^   |
			#:  | r_addr_E-v---|--v-----|-...-|-------v   |
			#:  |          |   |  |     |     |       |   |
			#:  |         |E1| | |E2|   |     |      |EN| | <-w_E, w_data, d=3
			#:  v----------||--v--||----v     v-------||--v
			#:  |         |F1| | |F2|   |     |      |FN| | <-w_F, w_data, d=3
			#:  |          |   |  |     |     |       |   |
			#:  | r_addr_F-^---|--^-----|-...-|-------^   |
			#:  | r_addr_G-v---|--v-----|-...-|-------v   |
			#:  |          |   |  |     |     |       |   |
			#:  |         |G1| | |G2|   |     |      |GN| | <-w_G, w_data, d=4
			#:  v----------||--v--||----v     v-------||--v
			#:             
			#: With this structure we can share routing between unrelated
			#: write data, write addresses, and read addresses.  We cannot share write
			#: signals, so the write signals for each input channel group must be 
			#: fanned out to match the delay for each of those input channel groups.

			r_addr_to_chain_din_delay = 1
			r_addr_shift_registers = []
			for ic_group, r_addr in enumerate(self.r_addrs):
				parallel_chains = []
				for chain in range(parallel_sequential_fanouts):
					parallel_chains.append(inst(
						dl.BasicShiftRegister, r_addr._bit_width, max_offset, name="r_addr_"+str(ic_group)+"_pipe_"+str(chain)))
					parallel_chains[-1].d.set_driver(r_addr.delay(r_addr_to_chain_din_delay))
				r_addr_shift_registers.append(parallel_chains)

			n_w_addr_chains = activation_width
			fanout_per_w_addr_chain_reg = 6
			w_addr_chain_length = math.ceil(ic_groups / fanout_per_w_addr_chain_reg)
			w_addr_chain_dins = [inst(dl.Logic, bits=w_addr._bit_width, name="w_addr_chain_din_" + str(i)) for i in range(n_w_addr_chains)]
			w_addr_to_w_addr_chain_din_fanout_pipe = inst(dl.PipelinedFanout,
				w_addr,
				*w_addr_chain_dins,
				max_fanout=4,
				name="w_addr_to_w_addr_chain_din_fanout_pipe")
			w_addr_chains = []
			w_data_chains = []
			for act, w_data in enumerate(self.inputs):
				w_data_chains.append(inst(
					dl.BasicShiftRegister, w_data._bit_width, w_addr_chain_length, name="w_data_chain_"+str(act)))
				w_data_chains[-1].d.set_driver(w_data)
			for chain, din in enumerate(w_addr_chain_dins):
				w_addr_chains.append(inst(
					dl.BasicShiftRegister, w_addr._bit_width, w_addr_chain_length, name="w_addr_chain_"+str(chain)))
				w_addr_chains[-1].d.set_driver(din)

			w_addr_to_w_addr_chain_din_fanout_levels = w_addr_to_w_addr_chain_din_fanout_pipe.get_levels()
			write_enable_fanout_levels = w_addr_to_w_addr_chain_din_fanout_levels
			self.control_delay = r_addr_to_chain_din_delay + 4 + output_delay
			self.control_to_input_delay = write_enable_fanout_levels + bor_stages
			self.read_delay = max_offset - 2
			we_s = [[inst(dl.Flop, bits=1, name="write_ic_group_" + str(ic_group) + "_act_" + str(activation)) for activation in range(activation_width)] for ic_group in range(ic_groups)]
			for i, (we_group, we) in enumerate(zip(we_s, self.writes)):
				inst(dl.PipelinedFanout,
					we.delay(i // fanout_per_w_addr_chain_reg + bor_stages),
					*we_group,
					levels=write_enable_fanout_levels,
					name="write_enable_fanout_pipe_" + str(i))

			#Needed if we end up having to use M20Ks
			r_en_const = inst(dl.Constant, bits=1, value=1, name="r_en_const")

			for ic_group in range(ic_groups):
				for act_number in range(activation_width):
					ram = inst(dl.RAM,
						bits=bits_per_act,
						register_output=True,
						depth=buffer_depths[ic_group][0],
						name="ram_icg_" + str(ic_group) + "_act_" + str(act_number))
					ram.w_en.set_driver(we_s[ic_group][act_number])
					ram.r_en.set_driver(r_en_const)
					ram.w_addr.set_driver(w_addr_chains[act_number].sr[ic_group//fanout_per_w_addr_chain_reg][ram.w_addr._bit_width-1:0])
					ram.r_addr.set_driver(r_addr_shift_registers[ic_group][act_number // max_offset].sr[act_number % max_offset].delay(1))
					ram.w_data.set_driver(w_data_chains[act_number].sr[ic_group//fanout_per_w_addr_chain_reg])
					self.outputs[ic_group][act_number].set_driver(ram.r_data.delay(output_delay-1))


class IABuffer(dl.Module):
	def __init__(self, bits_per_act, kernel_height, stride, input_channels, activation_width, write_port_width, ic_groups=1, min_buffer_depth=1, **kwargs):
		super(IABuffer, self).__init__("ia_buffer", kind="ia_buffer", **kwargs)
		inst = self.inst
		with dl.ModuleScope(self):
			num_buffered_lines = max(kernel_height + stride, min_buffer_depth)
			
			n_channels = [math.ceil(input_channels / float(ic_groups)) for _ in range(input_channels % ic_groups)]
			n_channels += [input_channels // ic_groups for _ in range(ic_groups - (input_channels % ic_groups))]
			total_buffer_depths = [n_channels[i] * num_buffered_lines for i in range(ic_groups)]
			buffer_specs = [dl.RAM.size_for_depth(bd) for bd in total_buffer_depths]
			buffer_depths = [[min(bd, (i * bs[0] % bd - 1) % bs[0] + 1) for i in range(math.ceil(bd / bs[0]))] for bd,bs in zip(total_buffer_depths, buffer_specs)]
			buffer_depths = [[np.sum(buffer_depth_list)] for buffer_depth_list in buffer_depths]
			buffer_widths = []
			for bs in buffer_specs:
				if bs[0] <= 64:
					buffer_widths.append([bits_per_act] * activation_width)
				else:
					total_width = activation_width * bits_per_act
					widths = [bs[1]] * math.floor(total_width / bs[1])
					remainder = total_width % bs[1]
					if remainder != 0:
						widths.append(remainder)
					assert(np.sum(widths) == total_width)
					buffer_widths.append(widths)

			max_fanout = 8
			added_read_delay = 1
			#if max([max(bds) for bds in buffer_depths]) <= 64:
				# for MLABs it hurts us to add output registers
			#	added_read_delay = 0

			self.inputs = [inst(dl.Logic, bits=bits_per_act, name="ia_buffer_write_data_" + str(j)) for j in range(activation_width)]
			self.writes = [inst(dl.Logic, bits=1, name="write_ic_group_" + str(i)) for i in range(ic_groups)]
			self.read = inst(dl.Logic, bits=1, name="read")
			self.outputs = [[inst(dl.Logic, bits=bits_per_act, name="ia_buffer_read_data_ic_group_" + str(i) + "_" + str(j)) for j in range(activation_width)] for i in range(ic_groups)]

			address_bits = [clog2(tbd) for tbd in total_buffer_depths]
			self.w_addrs = [inst(dl.Logic, bits=ab, name="w_addr_ic_group_" + str(i)) for i, ab in enumerate(address_bits)]
			self.r_addrs = [inst(dl.Logic, bits=ab, name="r_addr_ic_group_" + str(i)) for i, ab in enumerate(address_bits)]

			r_addr_fanouts = [[inst(dl.Logic, bits=r_addr._bit_width, name="r_addr_fanout_buffer_" + str(i)) for w in widths for d in depths]
				for i,(r_addr, widths, depths) in enumerate(zip(self.r_addrs, buffer_widths, buffer_depths))]

			physical_ram_addr_bits = [min(ab, clog2(bs[0])) for ab,bs in zip(address_bits, buffer_specs)]
			total_write_fanout = np.sum([len(bdl) for bdl in buffer_depths])
			total_ram_tile_count = np.sum([len(bdl) * len(bwl) for bdl, bwl in zip(buffer_depths, buffer_widths)])
			needed_din_fanout_delay = math.ceil(math.log(total_write_fanout, 10))
			needed_addr_fanout_delay = math.ceil(math.log(total_ram_tile_count, max_fanout))
			self.control_delay = max(needed_addr_fanout_delay, 4)
			self.data_in_delay = needed_din_fanout_delay
			self.control_to_input_delay = self.control_delay - self.data_in_delay
			memory_model_read_delay = 1
			ram_read_delay = memory_model_read_delay + added_read_delay
			self.read_delay = memory_model_read_delay + math.ceil(math.log(max([len(bdl) for bdl in buffer_depths]), max_fanout)) + ram_read_delay

			self.concatenated_inputs = inst(dl.Concat, *self.inputs, name="concatenated_inputs")
			#delayed_write_data = self.concatenated_inputs.delay(self.data_in_delay, max_fan=max_fanout, final_fan=total_write_fanout)
			delayed_write_data = [inst(dl.Logic, bits=self.concatenated_inputs._bit_width, name="delayed_write_data_" + str(i)) for i in range(total_write_fanout)]
			write_data_pipe = inst(dl.PipelinedFanout,
				self.concatenated_inputs,
				*delayed_write_data,
				levels=self.data_in_delay,
				name="write_data_pipe")

			r_addr_fanout_pipes = [inst(dl.PipelinedFanout, r_addr, *r_addr_l, levels=self.control_delay-2) for i, (r_addr, r_addr_l) in enumerate(zip(self.r_addrs, r_addr_fanouts))]


			self.ram_ic_group_module_list = [inst(dl.Module, "ic_group_ram", kind="ic_group_ram", name="ic_group_ram_" + str(i)) for i in range(ic_groups)]
			ic_group_read_data = []
			mux_stages = []
			cumulative_depth_counter = 0
			for input_channel_group, (
				ic_buffer_width_list, ic_buffer_depths_list, ic_group_ram, pb, w_addr, r_addrs, ic_group_write) in enumerate(
				zip(buffer_widths, buffer_depths, self.ram_ic_group_module_list, physical_ram_addr_bits, self.w_addrs, r_addr_fanouts, self.writes)):
				with dl.ModuleScope(ic_group_ram):
					ic_group_ram_tile_count = len(ic_buffer_width_list) * len(ic_buffer_depths_list)
					read_muxes = []
					wr_addr = w_addr.delay(self.control_delay-2, max_fan=max_fanout, final_fan=ic_group_ram_tile_count, final_fan_of_1=False)

					if len(ic_buffer_depths_list) > 1:
						write_chip_select_bits = w_addr[w_addr._bit_width-1:pb]
						decoded_writes = [inst(dl.AND, inst(dl.EQ, write_chip_select_bits, j), ic_group_write).delay(
							self.control_delay, max_fan=max_fanout, final_fan=ic_group_ram_tile_count) for j,r in enumerate(ic_buffer_depths_list)]

						read_chip_select_bits = r_addr[r_addr._bit_width-1:pb]
						decoded_reads = [inst(dl.AND, inst(dl.EQ, read_chip_select_bits, j), self.read).delay(
							self.control_delay, max_fan=max_fanout, final_fan=ic_group_ram_tile_count) for j,r in enumerate(ic_buffer_depths_list)]
					else:
						#decoded_writes = [ic_group_write.delay(self.control_delay, max_fan=max_fanout, final_fan=len(ic_buffer_width_list))]
						#if self.control_delay > 1:
						#	fanout_type = dl.Flop
						#else:
						fanout_type = dl.Logic
						with dl.ParamScope(bits=1):
							decoded_reads = [inst(fanout_type, name="ic_group_" + str(input_channel_group) + "_read_fanout_" + str(i)) for i in range(len(ic_buffer_width_list))]
							decoded_writes = [inst(fanout_type, name="ic_group_" + str(input_channel_group) + "_write_fanout_" + str(i)) for i in range(len(ic_buffer_width_list))]
						write_fanout_pipe = inst(dl.PipelinedFanout,
							ic_group_write,
							*decoded_writes,
							levels=self.control_delay-1,
							name="ic_group_" + str(input_channel_group) + "_write_fanout_pipe")
						read_fanout_pipe = inst(dl.PipelinedFanout,
							self.read,
							*decoded_reads,
							levels=self.control_delay-1,
							name="ic_group_" + str(input_channel_group) + "_read_fanout_pipe")

					select_signals = []
					buffer_width_start = 0
					r_addr_index = 0
					for x, (buffer_width, read, write) in enumerate(zip(ic_buffer_width_list, decoded_reads, decoded_writes)):
						depth_ram_list = []
						for y, buffer_depth in enumerate(ic_buffer_depths_list):
							ram = inst(dl.RAM,
								bits=buffer_width,
								depth=buffer_depth,
								register_output=True,
								name="ram_x" + str(x) + "_y" + str(y))
							ram.w_en.set_driver(write.delay(1))
							ram.r_en.set_driver(read.delay(1))
							ram.w_data.set_driver(delayed_write_data[y+cumulative_depth_counter][buffer_width_start+buffer_width-1:buffer_width_start])
							ram.w_addr.set_driver(wr_addr[ram.w_addr._bit_width-1:0].delay(2))
							ram.r_addr.set_driver(r_addrs[r_addr_index][ram.r_addr._bit_width-1:0].delay(2))
							r_addr_index += 1
							depth_ram_list.append(ram)
						read_mux = inst(dl.PipelinedMux,
							bits=depth_ram_list[-1].r_data._bit_width,
							max_select_bits_per_stage=clog2(max_fanout),
							total_mux_size=len(depth_ram_list),
							name="read_mux_" + str(x))
						read_muxes.append(read_mux)
						if len(select_signals) == 0:
							ra_base_select = pb
							for stage,(select,mux_count) in enumerate(zip(read_mux.select_signals, read_mux.muxes_per_stage)):
								next_ra_base_select = ra_base_select + select._bit_width
								select_signals.append(r_addr[next_ra_base_select-1:ra_base_select].delay(self.control_delay + ram_read_delay + stage, max_fan=max_fanout, final_fan=len(ic_buffer_width_list)))
								ra_base_select = next_ra_base_select

						for select,m_select in zip(select_signals, read_mux.select_signals):
							m_select.set_driver(select)
						for _input, ram in zip(read_mux.inputs, depth_ram_list):
							_input.set_driver(ram.r_data, delay=added_read_delay-1)
						buffer_width_start += buffer_width
					cumulative_depth_counter += len(ic_buffer_depths_list)
					concatenated_read_data = inst(dl.Concat, *[mux.output for mux in read_muxes], name="concatenated_read_data")
					mux_stages.append(read_muxes[0].stages)
					ic_group_read_data.append(concatenated_read_data)

			self.params_to_print = {
				"control_delay": self.control_delay,
				"data_in_delay": self.data_in_delay,
				"control_to_input_delay" : self.control_to_input_delay,
				"read_delay" : self.read_delay,
				"n_channels" : n_channels,
				"buffer_depths" : buffer_depths,
				"total_buffer_depths" : total_buffer_depths,
				"num_buffered_lines" : num_buffered_lines}

			delayed_read_data = [c.delay(self.read_delay - (ram_read_delay + stages)) for c,stages in zip(ic_group_read_data, mux_stages)]
			for i in range(ic_groups):
				for j in range(activation_width):
					self.outputs[i][j].set_driver(delayed_read_data[i][(j+1)*bits_per_act-1:j*bits_per_act])

			"""for rl in self.ram_list:
				for r in rl:
					r.has_fanout_control = True
					tile_estimate = r.tile_estimate
					fanout = math.ceil(tile_estimate/max_fanout)
					if fanout > 1:
						get_sdc_filter_str = lambda reg_name, m=r: "*|" + "|".join([o.name + "_i" for o in reversed(m.get_owner_list()[:-2])]) + "|" + m.name + "_i|*|" + reg_name + "[*]"
						dl.Verilog.current_circuit.add_quartus_setting_lambda(
							lambda te=fanout, fs=get_sdc_filter_str: "set_instance_assignment -name DUPLICATE_REGISTER " + str(te) + " -to " + '"' + fs("address_reg_a") + '"')
						dl.Verilog.current_circuit.add_quartus_setting_lambda(
							lambda te=fanout, fs=get_sdc_filter_str: "set_instance_assignment -name DUPLICATE_REGISTER " + str(te) + " -to " + '"' + fs("address_reg_b") + '"')
						dl.Verilog.current_circuit.add_quartus_setting_lambda(
							lambda te=fanout, fs=get_sdc_filter_str: "set_instance_assignment -name DUPLICATE_REGISTER " + str(te) + " -to " + '"' + fs("rdaddr_reg") + '"')"""


	def get_instance_comment(self, t="  "):
		return v_comment_string("\n".join([k + ": " + str(v) for k,v in self.params_to_print.items()]), t=t)

class BasicConvDSPs(dl.Module):
	def __init__(self,
		count,
		bits,
		signed,
		parallel_weights_per_dsp=2,
		parallel_weights=2,
		build_data_path=True,
		delay_for_chain_break=4,
		staggered_weight_info=None,
		round_const=0, **kwargs):

		# standard module initialization steps
		super(BasicConvDSPs, self).__init__("basic_conv_dsps", kind="basic_conv_dsps", **kwargs)
		assert(staggered_weight_info is not None)
		kwargs = self.kwargs
		inst = self.inst

		# build inputs to be driven by instantiating module
		self.inputs = [inst(dl.Logic, bits=bits[i%len(bits)], signed=signed[i%len(signed)], name="input_" + str(i)) for i in range(count * parallel_weights * 2)]
		self.accumulate_signals = [inst(dl.Logic, bits=1, signed=False, name="accumulate") for _ in range(count)]

		# A fully registered S10 DSP block has 3 input pipeline stages plus one output register
		input_pipe_depth = 3
		output_stages = 1

		#For the NX tensor block in fixed point vector mode there is 1 input register, 1 register after the multiplier and 1 (output) register after the accumulator
		if arch.hpipe_device_arch.arch_name == "S10_NX":
			input_pipe_depth = 2
		
		dsp_mac_delay = input_pipe_depth + output_stages
		self.added_delay = dsp_mac_delay
		self.extra_delay = 0
		self.count = 0

		#For grouping together mults on S10 NX
		independent_mults = False
		group_size = 0
		group_bits = []
		group_signed = []
		group_inputs = []


		# Having a bunch of long DSP chains causes quartus to fail placement, so we break
		# long chains into multiple smaller ones.  The result output of one DSP chain will then
		# be fed into the input pipeline of the next DSP block, so we need at least 3 pipe stages
		assert(delay_for_chain_break >= input_pipe_depth)
		additional_delay_between_chain_breaks = delay_for_chain_break - input_pipe_depth
		with dl.ModuleScope(self):
			if not build_data_path:
				self.outputs = [inst(dl.Logic, bits=36) for i in range(count)]
				self.dsp_scopes = [self for i in range(count)]
			else:
				self.outputs = []
				self.dsp_scopes = []
				for i in range(count):
					if sys.stdout.isatty():
						print("   Chains " + str(i+1) + "/" + str(count), end="\r")
					dsps_for_output = []
					if not independent_mults or i%3 == 0: self.dsp_scopes.append(inst(dl.Module, "dsp_output_group", kind="dsp_output_group", name="dsp_output_group_" + str(i)))
					with dl.ModuleScope(self.dsp_scopes[-1]):
						remaining_weights = parallel_weights
						multipliers_built = 0

						for chain_index, chain_length in enumerate(staggered_weight_info["multipliers_per_chain"]):
							inputs = self.inputs[parallel_weights*2*i + multipliers_built*2: parallel_weights*2*i + multipliers_built*2+chain_length*2]
							

							#On the Stratix 10 NX we want to group together independent multiplications into 1 tensor block in scalar mode (3 8bit*8bit mults)
							#This is only possible when there is not carry-in or carry-out (i.e. in depthwise conv layers)
							if arch.hpipe_device_arch.arch_name == "S10_NX" and chain_length == 1 and len(inputs) == 2:
							#if False:
								independent_mults = True
								group_size += 1
								group_bits += bits[multipliers_built*2:multipliers_built*2+chain_length*2]
								group_signed += [_input.signed for _input in inputs]
								group_inputs += inputs
								multipliers_built += 1

								if group_size == 3 or i == (count-1):
									chain_data = self._build_dsp_chain(
										1,
										group_bits,
										group_signed,
										False,
										parallel_weights_per_dsp,
										group_inputs,
										independent_mults)

									group_size = 0
									group_bits = []
									group_signed = []
									group_inputs = []


							else:
								chain_data = self._build_dsp_chain(
									chain_length,
									bits[multipliers_built*2:multipliers_built*2+chain_length*2],
									[_input.signed for _input in inputs],
									chain_index > 0,
									parallel_weights_per_dsp,
									inputs)
								multipliers_built += chain_length
								#On the Stratix 10 NX we cannot accumulate and cascade at the same time so we will need an extra accumulate register at then end of the chain. Thus, add an extra cycle 
								if len(chain_data["dsps"]) > 1 and arch.hpipe_device_arch.arch_name == "S10_NX":
									self.extra_delay = 1

								if chain_index > 0:
									chain_data["carry_in"].set_driver(chain_output, delay=additional_delay_between_chain_breaks)
								chain_output = chain_data["output"]


						if independent_mults == False:
							chain_data["dsps"][-1].inputs[-1].set_driver(self.accumulate_signals[i])
							chain_data["dsps"][-1].load_const = round_const
							self.outputs.append(chain_data["output"])
						elif (independent_mults and group_size == 0):
							chain_data["dsps"][-1].inputs[-1].set_driver(self.accumulate_signals[i])
							chain_data["dsps"][-1].load_const = round_const
							self.outputs += chain_data["outputs"]


	def _build_dsp_chain(
		self,
		num_multipliers,
		bits,
		signed,
		build_carry_in,
		parallel_weights_per_dsp,
		inputs,
		independent_mults=False):
		inst = self.inst
		# instantiate DSP blocks until we have built enough multipliers for the chain
		remaining_multipliers = num_multipliers
		multipliers_built = 0

		# Build a small data structure with data necessary for the caller to connect
		#  the chain to other logic
		chain_data = {
			"carry_in" : None,
			"output" : None,
			"outputs" : [],
			"dsps" : None
		}

		is_chain_start = True
		dsps = []
		while remaining_multipliers > 0:
			start_chain_carry_in = is_chain_start and build_carry_in
			# Number of weight inputs for the DSP we are about to instantiate
			#  if the number of multipliers is not evenly divisible by the number
			#  of parallel multiplications per DSP then the first DSP will use fewer
			#  inputs.
			num_weight_inputs = remaining_multipliers % parallel_weights_per_dsp
			if num_weight_inputs == 0:
				num_weight_inputs = parallel_weights_per_dsp
			# If there is a carry in to the chain, the DSP will have only one multiplication
			# and one carry in input.
			if arch.hpipe_device_arch.arch_name != "S10_NX" and start_chain_carry_in:
				num_weight_inputs = 1

			if independent_mults:
				num_weight_inputs = len(inputs) // 2
				assert(not start_chain_carry_in)

			# Each weight input will have a corresponding activation input, so the total
			# inputs is num_weight_inputs * 2 (chain in and out do not count here)
			inputs_for_this_dsp = num_weight_inputs * 2
			bits_for_this_dsp = bits[multipliers_built*2:multipliers_built*2 + inputs_for_this_dsp]
			input_signals = inputs[multipliers_built*2:multipliers_built*2 + inputs_for_this_dsp]
			dsp = inst(dl.MultiplyAccumulate,
				input_widths=bits_for_this_dsp,
				signed=signed,
				accumulate_width=64,
				parallel_reduction_count=inputs_for_this_dsp,
				external_carry_in=start_chain_carry_in,
				independent_mults=independent_mults)
			for i,input_signal in enumerate(input_signals):
				dsp.inputs[i].set_driver(input_signal)
			dsps.append(dsp)
			self.count += 1
			if start_chain_carry_in:
				chain_data["carry_in"] = dsp.external_carry_in
			if not is_chain_start:
				dsp.carry_in.set_driver(dsps[-2].carry_out)
			is_chain_start = False
			remaining_multipliers -= num_weight_inputs
			multipliers_built += num_weight_inputs

		chain_data["output"] = dsps[-1].output
		if independent_mults:
			chain_data["outputs"] = dsps[-1].independent_outputs

		chain_data["dsps"] = dsps

		return chain_data


				

class OCController(dl.Module):
	"""
	OCController

	Controls accumulation in the multipliers, run-length accumulation, and line address
	for the input activation buffer read address calculations

	Background: Each output channel can have a different number of weights since the
	weights can be sparse.  Activations corresponding to these weights must be multiplied
	and accumulated, and the accumulation must reset whenever we begin to process a new
	set of weights and activations for a new output channel.  To compute the cycles when
	we need to reset the accumulator we need a ROM that stores how many cycles we accumulate
	for each output channel, and a counter that counts down the number of cycles for each
	output channel.  The ROM is called ``weights_per_oc``, and the counter is called
	``in_oc_counter``.  All of the other components are just control logic for these two components
	that provide an address for the ROM, compute when to reset the counter, and track
	whether we are currently processing output channels or waiting for data.

	Args:
		weights_per_oc (list): a list containing the maximum number of weights for an output channel
			processing group (so if the processing is split along the input channel
			dimension 3 times, and the partitions have [3,7,5] nonzero weights for
			output channel #1, then ``weights_per_oc`` for output channel #1 should be 7,
			and the weights for the other input channels should be padded to 7 weights
			with 0 weights)
		weights_per_oc_bits (int): this should really just be ``clog2(max(weights_per_oc)+1)``
		done_delay (int): If weights are staggered for systolic accumulation, then ``done_delay`` should
			be the maximum amount of staggering.

	Attributes:
		weights_per_oc (:class:`digitallogic.digitallogic.ROM`)
			ROM containing the number of weights for each output channel

			Depth
				# of Output Channels in layer
			Values
				Number of weights for the output channel with an index matching the address
			Address
				index of output channel

		weights_per_oc_latency_hider (:class:`digitallogic.digitallogic.ROMLatencyHider`):
			Controller for ``weights_per_oc`` that allows for single cycle reads with a ROM that 
			takes two cycles to read

		current_output_channel_counter (:class:`digitallogic.digitallogic.Counter`):
			Stores the index of the output channel
			being processed.  When it finishes and
			in_oc_counter is done it causes the
			state machine to go from ``PROCESSING_OCS``
			to ``DONE``

			Increment Condition
				``in_oc_counter`` is done and in state ``PROCESSING_OCS``

			Done Condition
				counter equals ``output_channels - 1``

		weights_per_oc_addr (:class:`digitallogic.digitallogic.Counter`):
			Like ``current_output_channel_counter``,
			``weights_per_oc_addr`` stores the current
			output channel, but it does so for the
			``weights_per_oc_latency_hider``.  The
			latency hider reads from the ROM before
			values are actually needed, so this counter
			will be slightly ahead of 
			``current_output_channel_counter`` and is
			used as the address for the ROM
			(whereas the ``current_output_channel_counter``)
			is used for control.

			Increment Condition
				the latency hider reads from the ROM

			Done Condition
				counter equals the number of
				output channels.
				NOTE: this intentionally counts to 1 greater
				than the highest address in the ROM.  This is
				because the condition to get a new value from
				the ROM (``get_next_oc_counter_value``) fires a total
				of # output channels + 1 times.  By having this
				counter count to # output channels it will be reset
				to 0 when we transition to the DONE state.
				We might load an X into in_oc_counter as a result,
				but this is fine since we don't use in_oc_counter
				once we transition out of ``PROCESSING_OCS``.  When
				we enter ``START`` again the X value will be overwritten
				with the correct value at index 0 of the ROM.

		in_oc_counter (:class:`digitallogic.digitallogic.DownCounter`):
			Counter that counts down from the number of
			weights in an output channel to zero.  It
			loads the number of weights in an output
			channel from the ``weights_per_oc`` ROM

			Decrement Condition
				state is ``PROCESSING_OCS``

			Reset Condition
				``get_next_oc_counter_value``

			Reset Value
				the number of weights in an output channel, from ``weights_per_oc`` ROM


	"""
	def __init__(self, weights_per_oc, weights_per_oc_bits, done_delay=0, **kwargs):
		super(OCController, self).__init__(
			"oc_controller", kind="oc_controller", **kwargs)
		inst = self.inst
		with dl.ModuleScope(self):
			internal_to_external_control_delay = 2
			self.go = inst(dl.Logic, 1, name="go")
			max_weights_per_oc = np.max(np.array(weights_per_oc) - 1)

			#: We will use a counter that counts down the total
			#: number of weights to track processing of one line.
			#: This reduces loading on the the in_oc_counter
			total_weights = np.sum(np.array(weights_per_oc))
			weights_per_oc_bits = max(weights_per_oc_bits,1)
			self.processing_ocs_counter = inst(dl.Counter,
				reset_value=0,
				increment_value=1,
				end_value=total_weights-1,
				name="processing_ocs_counter")

			#: Instantiate a ROM that has the number of weights in 
			#: each output channel.
			self.weights_per_oc = inst(dl.ROM, 
				content_list=list(np.array(weights_per_oc) - 1),
				bits=weights_per_oc_bits,
				name="weights_per_oc_buffer")

			#: For layers with many output channels this ROM will be in a block RAM that
			#: requires two cycles to read.  The FIFO will allow us to read from a LUTRAM
			#: with fast single cycle control latency
			self.weights_per_oc_fifo = inst(
				dl.FIFO,
				bits=self.weights_per_oc.r_data._bit_width,
				depth=16,
				almost_full=8,
				use_lutram=True,
				dont_allow_x_write=False,
				name="weights_per_oc_latency_hiding_fifo")


			#: We will maintain two counters that track the current output channel
			#: 'current_output_channel_counter' is one that tracks the output channel
			#: so that we know the progress of a layer
			#: 'weights_per_oc_addr' is one that tracks the read location in the ROM
			self.current_output_channel_counter = inst(dl.Counter,
				reset_value=0,
				increment_value=1,
				end_value=len(weights_per_oc)-1,
				name="current_output_channel_counter")
			self.weights_per_oc_addr = inst(dl.Counter,
				reset_value=0,
				increment_value=1,
				end_value=len(weights_per_oc),
				name="weights_per_oc_addr")

			read_weights_per_oc_rom = inst(dl.AND,
				inst(dl.NOT, self.weights_per_oc_fifo.almost_full),
				inst(dl.NOT, kwargs["reset"]),
				name="read_weights_per_oc_rom").delay(1)
			self.weights_per_oc_fifo.w_data.set_driver(self.weights_per_oc.r_data.delay(1))
			self.weights_per_oc_fifo.w_en.set_driver(read_weights_per_oc_rom.delay(2))
			self.weights_per_oc.r_en.set_driver(read_weights_per_oc_rom)
			#: We increment the ROM read address whenever the latency hider reads 
			#:  from the memory
			self.weights_per_oc_addr.set_increment_condition(
				read_weights_per_oc_rom)

			#: We set it to be the driver for the ROM Read address
			self.weights_per_oc.r_addr.set_driver(
				self.weights_per_oc_addr.current_value[self.weights_per_oc.r_addr._bit_width-1:0])

			#: In OC counter tracks how many weights we have processed in an OC
			#: it gets reset to the value we read from the ROM and counts down to
			#: zero
			self.in_oc_counter = inst(dl.DownCounter,
				max_weights_per_oc, name="in_oc_counter")
			self.in_oc_counter.reset_value.set_driver(
				self.weights_per_oc_fifo.r_data)

			#: row here means that we are done a set of output channels
			#self.done_row = inst(dl.AND,
			#	self.current_output_channel_counter.is_done,
			#	self.in_oc_counter.is_done, name="done_row")
			self.done_row = self.processing_ocs_counter.is_done.copy()
			self.done_row.name = "done_row"

			#: there is a bug in the state machine module
			#: that means we can't drive edge conditions with
			#: signals with a name like is_<state_name>
			can_finish = inst(dl.Logic, bits=1, name="can_finish")

			states = ["IDLE", "START", "PROCESSING_OCS", "DONE"]
			edges = [
				["IDLE", "START", self.go],
				["START", "PROCESSING_OCS"],
				["PROCESSING_OCS", "DONE", self.done_row],
				["DONE", "IDLE", can_finish]]
			
			self.state_machine = inst(dl.StateMachine, states, edges, None, name="state_machine")
			self.processing_ocs_counter.set_increment_condition(self.state_machine.is_processing_ocs)
			self.should_read_weights = inst(dl.Logic, bits=1, name="should_read_weights")
			
			#Marius: if we are using a ConvWrapper (for output channel parallelism) we may need to stall the next row depending on sparsity in other convolution modules 
			#These signals should be passed up to the ConvWrapper
			self.fsm_idle = self.state_machine.is_idle
			self.fsm_has_started = self.state_machine.is_start

			#:  Done delay allows for weight staggering (which allows for systolic multiplier chaining).
			#:  Basically weight staggering causes computation of all output channels to take an additional
			#:  number of cycles equal to the maximum amount of staggering.  A done delay of greater than 0
			#:  will cause a counter to be created that keeps us in the DONE state an extra done_delay cycles.
			#:
			#:  If you want to see why this is necessary, the following should explain the weight staggering:
			#:  Let's say we have 3 multipliers each connected in a chain like: ([R] is a register)
			#:   a1\
			#:      x -->[R]
			#:   b1/      |
			#:   a2\\      V
			#:      x --> + -> [R]
			#:   b2/            |
			#:   a3\\            V
			#:      x --------> + -> [R]-|->
			#:   b3/            ^--------|
			#:
			#:  The idea here is that we are using 3 multipliers to accelerate the computation of
			#:  a single output channel.  To make this fast the output of each multiplier is registered
			#:  prior to being added to the output of the next multiplier.  This means that we need
			#:  to delay the inputs to each multiplier to get them to align properly, like this:
			#:  ___________________________
			#:  Time:     1 2 3 4 5 6 7 8 9
			#:  ---------------------------
			#:  a1 input: 1 1 1 2 2 2 2
			#:  a2 input:   1 1 1 2 2 2 2
			#:  a3 input:     1 1 1 2 2 2 2
			#:  ---------------------------
			#:  Here the numbers in the table indicate which output channel the input corresponds to,
			#:  so a1 gets inputs for OC#1 from time 1-3, and gets inputs for OC#2 from time 4-7, while
			#:  a2 gets inputs for OC#1 from time 2-4. One cycle after a3 gets its final OC#1 input (cycle 6), the
			#:  output register for the third multiplier will contain the accumulation of all the multiplications
			#:  for OC#1.
			#:  For this example, OCController would finish and go back to idle after time 7 if done_delay was 0,
			#:  but we don't want that, so we delay going back to IDLE until all of the multipliers have finished
			#:  with done_delay_counter.  In this example, done_delay_counter should delay going back to IDLE by 2
			#:  cycles.

			if done_delay > 0:
				done_delay_counter = inst(dl.Counter,
					reset_value=0,
					increment_value=1,
					end_value=done_delay,
					name="done_delay_counter")
				with dl.ModuleScope(done_delay_counter):
					done_delay_counter_almost_done_value = inst(dl.Constant, bits=done_delay_counter.current_value._bit_width, value=done_delay-1, name="almost_done_value")
					done_delay_counter_almost_done = inst(dl.EQ, done_delay_counter.current_value, done_delay_counter_almost_done_value, name="almost_done")
					can_finish.set_driver(inst(dl.AND, self.state_machine.is_done, done_delay_counter_almost_done), delay=1)
					done_delay_counter.set_increment_condition(self.state_machine.is_done)
				self.should_read_weights.set_driver(
					inst(dl.OR,
						self.state_machine.is_processing_ocs,
						inst(dl.AND,
							inst(dl.NOT, can_finish),
							self.state_machine.is_done)), delay=internal_to_external_control_delay)
			else:
				can_finish.set_driver(self.state_machine.is_done)
				self.should_read_weights.set_driver(self.state_machine.is_processing_ocs, delay=internal_to_external_control_delay)


			self.current_output_channel_counter.set_increment_condition(
				inst(dl.AND, 
					self.state_machine.is_processing_ocs,
					self.in_oc_counter.is_done))
			self._done_oc = inst(dl.AND, self.state_machine.is_processing_ocs, self.in_oc_counter.is_done, name="_done_oc")
			self.done_oc = self._done_oc.delay(internal_to_external_control_delay)
			self.get_next_oc_counter_value = inst(dl.OR,
					self.state_machine.is_start,
					self._done_oc,
					name="get_next_oc_counter_value")
			self.in_oc_counter.reset_signal.set_driver(
				self.get_next_oc_counter_value)
			self.in_oc_counter_reset = self.in_oc_counter.reset_signal.delay(internal_to_external_control_delay)
			self.done = inst(dl.Logic, 1)
			self.done.set_driver(can_finish, delay=internal_to_external_control_delay)
			self.weights_per_oc_fifo.r_en.set_driver(self.get_next_oc_counter_value)
			self.in_oc_counter.enable.set_driver(self.state_machine.is_processing_ocs)

			self.requires_driver = [self.go]

class WeightBufferController(dl.Module):
	def __init__(self, buffer, depth, **kwargs):
		super(WeightBufferController, self).__init__("weight_buffer_controller", kind="weight_buffer_controller", **kwargs)
		inst = self.inst
		with dl.ModuleScope(self):
			self.load_weights = inst(dl.Logic, 1, name="load_weights")
			self.weight_buffer_addr_counter = inst(dl.Counter,
				reset_value=0,
				increment_value=1,
				end_value=depth-1,
				name="weight_buffer_addr_counter")

			self.weight_buffer_addr_counter.set_increment_condition(self.load_weights)

			self.weight_buffer_addr = inst(dl.Logic, bits=self.weight_buffer_addr_counter.current_value._bit_width)
			self.weight_buffer_addr.set_driver(self.weight_buffer_addr_counter.current_value)

			self.requires_driver = [self.load_weights]

class RLProcessor(dl.Module):
	def __init__(self, rl_width, addr_width, **kwargs):
		super(RLProcessor, self).__init__("rl_processor", kind="rl_processor", **kwargs)
		inst = self.inst
		with dl.ModuleScope(self):
			self.rl = inst(dl.Logic, rl_width, name="rl")
			self.rl_valid = inst(dl.Logic, 1, name="rl_valid")
			self.clear = inst(dl.Logic, 1, name="clear")
			self.address = inst(dl.Flop, bits=addr_width, name="address")

			cumulative_mux = inst(dl.MuxV2,
				self.clear,
				self.address,
				inst(dl.Constant, bits=addr_width, value=0, name="zeros"),
				name="cumulative_mux")
			rl_mux = inst(dl.MuxV2,
				self.rl_valid,
				inst(dl.Constant, bits=rl_width, value=0),
				self.rl)

			with dl.ParamScope(name="next_address_wide"):
				next_address_wide = cumulative_mux + rl_mux
			with dl.ParamScope(name="next_address"):
				next_address = next_address_wide[addr_width-1:0]

			self.address.set_driver(next_address)

			self.requires_driver = [self.rl, self.rl_valid, self.clear]

#For printing a more detail assertion message in FIFO overflow assertion
ConvWrapper_counter = 0

class ConvWrapper(NNStage):
	"""
	ConvWrapper

	Wraps multiple BasicConv module instantiations to enable unrolling along output channels

	"""
	kind =  "conv_wrapper"
	def instantiate(self, node, inst, build_data_path=True, use_tensor_block_override=False, enable_bfp_override=False, **kwargs):
		self.buffered_layer = True
		fifo_to_mux_delay = 3
		global ConvWrapper_counter		
		n_output_channel_groups = node.planner_estimate.n_output_channel_groups
		print(f"  n_output_channel_groups: {n_output_channel_groups}")
		group_ocs = [[] for _ in range(n_output_channel_groups)]
		group_cycles = [0 for _ in range(n_output_channel_groups)]
		cycles_per_oc = []
		oc_order = []
		buffer_depths = [0 for _ in range(n_output_channel_groups)]
		max_buffer_depths = [32 for _ in range(n_output_channel_groups)]
		is_depthwise = (node.type == "DepthwiseConv2dNative")
		oc_order_fifo_depth = 16

		#Need more output buffer space if we have more output channel groups
		if n_output_channel_groups >= 8: max_buffer_depths = [64 for _ in range(n_output_channel_groups)]
		elif n_output_channel_groups > 6: max_buffer_depths = [32 for _ in range(n_output_channel_groups)]

		#Switch conv module to tensor mode if specified. Depthwise convs don't make use of tensor mode due to no ICP
		conv_module = BasicConv
		if use_tensor_block_override or (arch.hpipe_device_arch.tensor_block_mode == "tensor" and (not is_depthwise or (is_depthwise and self.get_bit_spec("input")["width"] > 8))):
			conv_module = TensorConv
			#need to update max buffer depths since TensorConv will produce 1 value every cycle (followed by a break of oc cycles)
			#e.g. for OCP=2 and OC=64, each tensor conv will produce 32 values one after another followed by 32 cycles of break (assuming IC=32 and ICP=20)
			output_channel_dimension = node.values[0].shape[-1]
			if is_depthwise: output_channel_dimension = node.values[0].shape[-2]

			input_channel_dimension = node.values[0].shape[-2]
			input_channel_parallelism  = node.planner_estimate.n_channel_splits
			kernel_shape = node.get_kernel_shape()
			cycles_per_value = math.ceil(input_channel_dimension/input_channel_parallelism)*kernel_shape[0]*kernel_shape[1]

			# Need to make sure ICP isn't maxed out, otherwise each module will produce 1 value a cycle without any breaks and we would need an infinite FIFO size
			# To fully take advantage of this parallelism HPIPE would need to be updated to be able to accept 2+ values a cycle, which is a big endeavour 
			if cycles_per_value < n_output_channel_groups:
				
				print("CRITICAL WARNING: Could not map current layer to tensor mode as it would theoretically require an infinite FIFO size for the ConvWrapper (Output channel parallelism)!")
				print("cycles_per_value =", cycles_per_value, "input_channel_dimension =", input_channel_dimension, "input_channel_parallelism =", input_channel_parallelism, "kernel_shape =", kernel_shape)

				#Decrease the ICP if possible. Otherwise just exit
				highest_icp_supported = math.ceil(((kernel_shape[0]*kernel_shape[1]*input_channel_dimension)/n_output_channel_groups)/dl.NX_ICP_PER_TENSOR_BLOCK)*dl.NX_ICP_PER_TENSOR_BLOCK
				if math.ceil(input_channel_dimension/highest_icp_supported)*kernel_shape[0]*kernel_shape[1] < n_output_channel_groups: exit(1)

				print("Reducing ICP of this layer to maximum supported value of", highest_icp_supported, "(performance will suffer)")
				node.planner_estimate.n_channel_splits = highest_icp_supported
				import time
				time.sleep(5)

				
			#Tensor mode will do a burst of all output channels at once so we need to be able to store all of them in the FIFO
			max_fifo_size = int(output_channel_dimension) #Technically we should be able to do output_channel_dimension/n_output_channel_groups but it overflows for some reason...
			max_buffer_depths = [max_fifo_size for _ in range(n_output_channel_groups)]

			# For smaller IC values, we may end up with a case where one conv module has 11 and one has 10, in which case the one with 11 will take almost double the time (with preloading)
			# In that case we need larger output buffers
			if math.ceil(input_channel_dimension/n_output_channel_groups) <= 11:
				max_buffer_depths = [max_fifo_size*2 for _ in range(n_output_channel_groups)]
			print("max_fifo_size", max_fifo_size, "input_channel_dimension", input_channel_dimension, "output_channel_dimension", output_channel_dimension)

				
		#Choose the correct dimension for the Depthwise conv
		output_channel_dimension = node.values[0].shape[-1]
		if is_depthwise:
			output_channel_dimension = node.values[0].shape[-2]

		weights_tmp = np.copy(node.values[0])
		# Since tensor mode doesn't support sparsity, temporarily set all 0 weights to 1 so the loop below balances stuff evenly 
		if conv_module == TensorConv:
			weights_tmp[weights_tmp == 0] = 1

		last_min_index = 0
		group_ocs_cycles = [[] for _ in range(n_output_channel_groups)]
		for oc in range(output_channel_dimension):
			if is_depthwise:
				cycles_for_oc = np.nonzero(weights_tmp[:,:,oc,:])[0].size
			else:
				cycles_for_oc = np.nonzero(weights_tmp[:,:,:,oc])[0].size						
			min_index = np.argmin(group_cycles)
			
			#Uncomment below and comment out above to force the split to be even (will impact performance but useful for debugging)
			#min_index = last_min_index
			#last_min_index = (not last_min_index)

			group_cycles[min_index] += cycles_for_oc
			cycles_per_oc.append(group_cycles[min_index])
			group_ocs[min_index].append(oc)
			group_ocs_cycles[min_index].append(cycles_for_oc)
			buffer_depths[min_index] += 1
			oc_order.append(min_index)
			if min_index != np.argmin(group_cycles):
				max_buffer_depths[min_index] = max(2 * buffer_depths[min_index], max_buffer_depths[min_index])
				buffer_depths[min_index] = 0		

		# For the unit tests
		example_input = node.input_nodes()[0].example_outputs[0]
		self.example_input = example_input
		self.weights = self.node.values[0]
		if node.explicit_padding is not None:
			self.example_input = np.pad(example_input, node.explicit_padding, "constant", constant_values=0)
		
		#: Instantiate a ROM that has the order in which the BasicConv instances
		#: will produce output channels
		self.oc_order_rom = inst(dl.ROM, 
			content_list=oc_order,
			bits=clog2(n_output_channel_groups),
			name="oc_order_rom")

		# For a depthwise conv we don't want to send the entire activation depth to each conv module
		# Essentially we only want to send the ones each conv module needs for its filters
		# Need to use a FIFO similar to what we did with the OC 
		if is_depthwise:
			#Need a second ROM for depthwise convolutions since we need to know which conv to send the current value to
			self.oc_order_rom_2 = inst(dl.ROM, 
				content_list=oc_order,
				bits=clog2(n_output_channel_groups),
				name="oc_order_rom_2")
			
			self.oc_order_rom_addr_2 = inst(dl.Counter,
			reset_value=0,
			increment_value=1,
			end_value=len(oc_order) - 1,
			name="oc_order_rom_addr_2")
			
			oc_order_fifo_depthwise = inst(dl.FIFO,
				bits=self.oc_order_rom_2.r_data._bit_width,
				depth=oc_order_fifo_depth,
				use_lutram=True,
				almost_full=oc_order_fifo_depth//2,
				name="oc_order_fifo_depthwise")

			oc_order_fifo_depthwise_control = inst(dl.FIFO,
				bits=self.oc_order_rom_2.r_data._bit_width,
				depth=oc_order_fifo_depth,
				use_lutram=True,
				almost_full=oc_order_fifo_depth//2,
				name="oc_order_fifo_depthwise_control")	
			
			oc_order_rom_2_read = inst(dl.AND, inst(dl.NOT, kwargs["reset"]), inst(dl.NOT, inst(dl.AND, oc_order_fifo_depthwise.almost_full, oc_order_fifo_depthwise_control.almost_full), name="oc_order_fifos_depthwise_alomst_full")).delay(1)
			self.oc_order_rom_2.r_en.set_driver(oc_order_rom_2_read)
			
			self.oc_order_rom_2.r_addr.set_driver(self.oc_order_rom_addr_2.current_value)
			self.oc_order_rom_addr_2.set_increment_condition(oc_order_rom_2_read)
							
			oc_order_fifo_depthwise.w_en.set_driver(oc_order_rom_2_read.delay(2))
			oc_order_fifo_depthwise.w_data.set_driver(self.oc_order_rom_2.r_data.delay(1))
			oc_order_fifo_depthwise.r_en.set_driver(self.writes[0])
			oc_order_fifo_depthwise_control.w_en.set_driver(oc_order_rom_2_read.delay(2))
			oc_order_fifo_depthwise_control.w_data.set_driver(self.oc_order_rom_2.r_data.delay(1))
			oc_order_fifo_depthwise_control.r_en.set_driver(self.write)

		if hasattr(self.node, "offload_to_hbm"):
			offload_to_hbm = self.node.offload_to_hbm
		else:
			offload_to_hbm = False

		# HBM Signals
		self.hbm_interface = []	#Won't be used unless hbm is enabled
		self.hbm_binary_weights = []
		self.hbm_line_count = []

		bc_insts = []
		output_fifo_lists = []
		buffered_output_counters = []
		buffered_output_counters_gt_0 = []
		read_fifo_list = []
		conv_fsm_idle_signals = []
		
		all_convs_idle = inst(dl.Logic, bits=1, name="all_convs_idle")
		
		for i, (bd, gocs) in enumerate(zip(max_buffer_depths, group_ocs)):
			dummy_node = Node(node.tf_op, node.example_outputs, node.precision_parameters)
			dummy_node.type = node.type
			dummy_node.planner_estimate = node.planner_estimate
			dummy_node.inputs = node.inputs
			dummy_node.outputs = node.outputs
			dummy_node.properties = node.properties
			dummy_node.explicit_padding = node.explicit_padding
			dummy_node.offload_to_hbm = offload_to_hbm
			if is_depthwise:			
				values = node.values[0][:,:,gocs,:]
			else:
				values = node.values[0][:,:,:,gocs]	
				
				
			dummy_node.values = [values]

			if is_depthwise:
				bc_inst = self.inst(conv_module, dummy_node, depthwise_conv_parallelism=len(gocs), enable_bfp_override=enable_bfp_override)
			else:
				bc_inst = self.inst(conv_module, dummy_node, called_from_wrapper=True, enable_bfp_override=enable_bfp_override)
				
			if i == 0:
				self.utilization_estimates = bc_inst.utilization_estimates
			else:
				for k in bc_inst.utilization_estimates.keys():
					self.utilization_estimates[k] += bc_inst.utilization_estimates[k]
			bc_insts.append(bc_inst)
			for sink, source in zip(bc_inst.inputs, self.inputs):
				sink.set_driver(source, delay=1)
				
			#Writes to conv module are depndent on which output channel was sent in for depthwise convolutions
			if is_depthwise:
				actual_write_en = inst(dl.AND, inst(dl.EQ, i, oc_order_fifo_depthwise_control.r_data), self.write)
				bc_inst.write.set_driver(actual_write_en, delay=1)
			else:
				bc_inst.write.set_driver(self.write, delay=1)
				
			for sink, source in zip(bc_inst.writes, self.writes):
				if is_depthwise:
					actual_write_en = inst(dl.AND, inst(dl.EQ, i, oc_order_fifo_depthwise.r_data), source)
					sink.set_driver(actual_write_en, delay=1)
				else:
					sink.set_driver(source, delay=1)
				
				
			#Pass the hbm controllers to the ConvWrapper
			if offload_to_hbm and conv_module == TensorConv:
				for hbmc in bc_inst.hbm_interface:
					self.hbm_interface.append(hbmc)
				for bw in bc_inst.hbm_binary_weights:
					self.hbm_binary_weights.append(bw)
				for lc in bc_inst.hbm_line_count:
					self.hbm_line_count.append(lc)
			
			output_fifos = []
			for j,(output, valid) in enumerate(zip(bc_inst.outputs, bc_inst.outputs_valid)):
				output_fifo = inst(dl.FIFO, bits=output._bit_width, depth=bd, name="oc_group_" + str(i) + "_output_fifo_" + str(j)) 
				output_fifos.append(output_fifo)
				output_fifo.w_data.set_driver(output.delay(2))
				output_fifo.w_en.set_driver(valid.delay(2))
				
				out_fifo_full = inst(dl.Logic, bits=1, name="oc_group_" + str(i) + "_output_fifo_full_" + str(j))
				out_fifo_full.set_driver(inst(dl.AND, output_fifo.full, valid.delay(2)))
				
				#Checks if FIFOs have overflowed or not, should not affect Quartus build (should be optimized out)
				m = self
				def full_condition(m, t="  ", fifo_condition=out_fifo_full):
					condition = "(" + fifo_condition.name + " !== 1)"
					return condition
					
				inst(dl.Assertion,
					name="output_valid_delay_check",
					condition_f=full_condition,
					condition_f_args=m,
					comment=out_fifo_full.name + " is full! " + str(ConvWrapper_counter),
					error_string=out_fifo_full.name + " is full! " + str(ConvWrapper_counter))

			output_fifo_lists.append(output_fifos)

			
			# Marius: Some groups may take less cycles than other due to spartsity
			# If this is the case then they need to wait for the group with the longest cycle latency to complete 
			# Otherwise the output FIFOs may eventually overflow as we could have an extra value that needs to be stored after each row
			# Tensor mode convolutions don't support sparsity since they operate in vectors of size 10, so we don't need this
			# ====================
			out_fifo_has_space = inst(dl.Logic, bits=1, name="out_fifo_has_space")
			out_fifo_has_space.set_driver(inst(dl.LT, output_fifos[0].lines_used_counter.current_value,  inst(dl.Constant, bits=bc_inst.outputs[0]._bit_width, value=math.ceil(bd/2), name="half_buffer_depth"), name="out_fifo_lines_used_lt_half"))
			if conv_module != TensorConv:
				# We may need a delay on all the idle signals for timing
				conv_fsm_idle_signals.append(bc_inst.oc_controller.fsm_idle.delay(1))
			
				# We want to store all_convs_idle until the module actually starts
				# For depthwise convolutions some modules might start earlier. Reset driver can be 0 or 1, it shouldn't matter
				conv_ready_to_start = inst(dl.Flop, bits=1, reset_driver=1, name="control_sr_init_control")
				tmp_reg_input = inst(dl.MuxV2, conv_ready_to_start, all_convs_idle, inst(dl.NOT, bc_inst.oc_controller.fsm_has_started), name="all_convs_idle_mux_oc_group_" + str(i))
				conv_ready_to_start.set_driver(tmp_reg_input)
				
				bc_inst.can_write_line.set_driver(inst(dl.AND, self.can_write_line.delay(1), conv_ready_to_start, out_fifo_has_space.delay(2)))
			else:
				bc_inst.can_write_line.set_driver(inst(dl.AND, self.can_write_line.delay(1), out_fifo_has_space.delay(2)))
			# ==================				
				

			read_fifo_list.append(inst(dl.Logic, bits=1, name="read_fifo_oc_group_" + str(i)))
			buffered_output_counter = inst(dl.UpDownCounter,
				reset_value=0,
				increment_value=1,
				decrement_value=-1,
				end_value=bd,
				name="buffered_output_counter_oc_group_" + str(i))
			buffered_output_counters.append(buffered_output_counter)
			buffered_output_counters_gt_0.append(inst(dl.NOT, inst(dl.EQ, buffered_output_counter.current_value, 0)))
			buffered_output_counter.set_increment_condition(bc_inst.output_valid)
			buffered_output_counter.set_decrement_condition(read_fifo_list[-1])

		if conv_module != TensorConv:
			all_convs_idle.set_driver(inst(dl.AND, *conv_fsm_idle_signals))
				
		#: The ROM could be deep and may require multiple cycles to read
		#: to hide this latency we will use a FIFO
		oc_order_fifo = inst(dl.FIFO,
			bits=max(self.oc_order_rom.r_data._bit_width,1),
			depth=oc_order_fifo_depth,
			use_lutram=True,
			almost_full=oc_order_fifo_depth//2)
		oc_order_rom_read = inst(dl.AND, inst(dl.NOT, kwargs["reset"]), inst(dl.NOT, oc_order_fifo.almost_full)).delay(1)
		self.oc_order_rom.r_en.set_driver(oc_order_rom_read)
		oc_order_fifo.w_en.set_driver(oc_order_rom_read.delay(2))
		oc_order_fifo.w_data.set_driver(self.oc_order_rom.r_data.delay(1))

		#: For layers with many output channels this ROM will be in a block RAM that
		#: requires two cycles to read.  The latency hider buffers two reads in registers
		#: so that we can get the next one in one cycle
		self.oc_order_rom_addr = inst(dl.Counter,
			reset_value=0,
			increment_value=1,
			end_value=len(oc_order) - 1,
			name="oc_order_rom_addr")
		self.oc_order_rom.r_addr.set_driver(self.oc_order_rom_addr.current_value)
		self.oc_order_rom_addr.set_increment_condition(
			oc_order_rom_read)
		oc_order_fifo.r_en.set_driver(inst(dl.OR, *read_fifo_list))
		next_oc_group = oc_order_fifo.r_data
		for i, (read, has_enough_values) in enumerate(zip(read_fifo_list, buffered_output_counters_gt_0)):
			read.set_driver(inst(dl.AND, has_enough_values, inst(dl.EQ, next_oc_group, i)))

		any_read = inst(dl.OR, *read_fifo_list)

		max_control_to_output_delay = max([bc.get_control_valid_to_output_valid_delay() for bc in bc_insts]) + 3

		delayed_fifo_read_lists = []
		for read, ofifol in zip(read_fifo_list, output_fifo_lists):
			delayed_fifo_read_data = []
			fifo_count = len(ofifol)
			r_en = read.delay(max_control_to_output_delay, max_fan=4, final_fan=fifo_count)
			for fifo in ofifol:
				fifo.r_en.set_driver(r_en)
				delayed_fifo_read_data.append(fifo.r_data.delay(fifo_to_mux_delay-1)) #Need to subtract 1 here since FIFO output comes 1 cycles after read enable (control signals below stay the same) 
			delayed_fifo_read_lists.append(delayed_fifo_read_data)

		delayed_mux_select = next_oc_group.delay(max_control_to_output_delay + fifo_to_mux_delay, max_fan=4, final_fan=len(self.outputs))
		delayed_any_read = any_read.delay(max_control_to_output_delay + fifo_to_mux_delay, max_fan=4, final_fan=len(self.outputs))
		for i,(o,ov) in enumerate(zip(self.outputs, self.outputs_valid)):
			mux_inputs = [l[i] for l in delayed_fifo_read_lists]
			o.set_driver(inst(dl.MuxV2, delayed_mux_select, *mux_inputs))
			ov.set_driver(delayed_any_read)

		self._set_control_to_input_delay(bc_insts[0].get_control_to_input_delay())
		self._set_control_valid_to_output_valid_delay(max_control_to_output_delay + fifo_to_mux_delay)
		self.output_valid.set_driver(any_read)
		#Potential issue here if unbalanced so we have to AND all the signals
		space_to_write_line_list = [cur_inst.space_to_write_line for cur_inst in bc_insts]
		all_space_to_write_line = inst(dl.AND, *space_to_write_line_list)
		self.space_to_write_line.set_driver(all_space_to_write_line)
		ConvWrapper_counter += 1

		#We must connect all the counters of the individual convs into one big counter. 
		#We send cred_dec when all drivers have a non-zero value, and we decrement them by one
		#Convs with bigger buffers have their counters start at a non-zero reset value 
		min_cred_max = min([list(bc.cred_max.values())[0] for bc in bc_insts])
		cred_max_diff =[list(bc.cred_max.values())[0] - min_cred_max for bc in bc_insts]
		credit_counters = [inst(dl.MultiChangeCounter, reset_value=cred_max_diff[i], change_values=[-1, -bc.cred_dec_amount, -bc.cred_dec_final_amount], end_value=list(bc.cred_max.values())[0] + cred_max_diff[i]) for i,bc in enumerate(bc_insts)]	
		credit_counters_not_empty = [inst(dl.NOT, inst(dl.EQ, c.current_value, 0).delay(1)) for c in credit_counters]
		all_have_credit = inst(dl.AND, *credit_counters_not_empty)
		for i,bc in enumerate(bc_insts):	
			credit_counters[i].change_signals[0].set_driver(inst(dl.AND, all_have_credit, inst(dl.NOT, credit_counters[i].change_signals[0].delay(1))))
			credit_counters[i].change_signals[1].set_driver(bc.cred_dec.delay(3))
			credit_counters[i].change_signals[2].set_driver(bc.cred_dec_final.delay(3))		
		self.cred_dec.set_driver(inst(dl.AND, all_have_credit, inst(dl.NOT, self.cred_dec.delay(1))))

		#We use the cred_inc and cred_dec_final of the slowest conv
		max_i = -1
		shape_prod_max = -1
		#We try to find the slowest conv
		for i, bc in enumerate(bc_insts):
			shape_prod = np.prod(bc.weights.shape)
			if (shape_prod >= shape_prod_max):
				shape_prod_max = shape_prod
				max_i = i
		self.cred_inc.set_driver(bc_insts[max_i].cred_inc.delay(1))
		self.cred_dec_final.set_driver(bc_insts[max_i].cred_dec_final.delay(1))

		self.cred_dec_amount = -1
		self.cred_dec_final_amount = 0
		if node.inputs[0]._from.module is not None:
			self.cred_max[node.inputs[0]._from.module.name] = min([list(bc.cred_max.values())[0] for bc in bc_insts])
		else:
			self.cred_max["None"] = min([list(bc.cred_max.values())[0] for bc in bc_insts])
		self.cred_padding = bc_insts[0].cred_padding

		#If the convs have the cred_padding_unpause attribute, then we hold the individual unpause signals, and release the main cred_padding_unpause for the layer
		# when all of them have been received. 
		if hasattr(bc_insts[0], "cred_padding_unpause"):
			hold_unpause = [inst(dl.Flop, reset_driver=0, bits=1, name="hold_cred_inc_dec") for bc in bc_insts]
			hold_unpause_set = [inst(dl.Logic, bits=1, name="hold_cred_inc_set") for bc in bc_insts]
			hold_unpause_unset = [inst(dl.Logic, bits=1, name="hold_cred_inc_unset") for bc in bc_insts]
			all_unpause = inst(dl.AND, *hold_unpause)
			for i,bc in enumerate(bc_insts):
				hold_unpause_set[i].set_driver(bc.cred_padding_unpause.delay(3))
				hold_unpause_unset[i].set_driver(all_unpause.delay(1))
				hold_unpause[i].set_driver(inst(dl.OR, inst(dl.AND, hold_unpause[i], inst(dl.NOT, hold_unpause_unset[i])), hold_unpause_set[i]))
			self.cred_padding_unpause = inst(dl.Logic, 1, name="credit_padding_unpause")
			self.cred_padding_unpause.set_driver(all_unpause)

		

class BasicConv(NNStage):
	"""
	BasicConv

	Implements convolutions of variable kernel size and stride.  Does not support dilated
	convolutions yet, and support for depthwise convolutions is not validated.

	Attributes:
		ibc (:class:`InputBufferController`):
			controls write addresses for the input activation buffers

		iab (:class:`IABuffer`):
			buffers input activation lines

		dsps (:class:`BasicConvDSPs`):
			A set of DSP blocks potentially chained together that implement multiplications
			and accumulations
		
		oc_controller (:class:`OCController`):
			The output channel controller.  This controller and buffer tell us
			what cycles we should accumulate both runlengths and weight-activation
			products.

		activation_address_computers (list of privately defined modules):
			This is a set of controllers that compute read addresses
			for each of the activation buffers.  There is one
			address computer for each input channel split.

		x_muxes (list of :class:`digitallogic.digitallogic.MuxV2`):
			One mux for each activation input along the x dimension.  These allow
			us to shift the input activations to the correct DSP to for kernels with
			a width greater than 1.

			The following diagram is an example showing these x muxes in a layer with
			a kernel_width of 3, an input width of 7, and ``padding="SAME"``.
			The input activation buffer is 7 activations wide (a-g), and 
			``(kernel_height + stride) * input_channels`` deep.  We load all activations
			from the same address, and apply the same weight, but in order to accumulate
			in place we need to have each multiplier select the activation along the
			x dimension that matches the weight.
			::

			  xsel1 --|
			          |
			     0  -|\\
			     a1 -| |------\\
			     b1 -|/        x---- oa
			          | w1 ---/
			     a1 -|\\
			     b1 -| |------\\
			     c1 -|/        x---- ob
			          | w1 ---/
			     b1 -|\\
			     c1 -| |------\\
			     d1 -|/        x---- oc
			          | w1 ---/
			     c1 -|\\
			     d1 -| |------\\
			     e1 -|/        x---- od
			          | w1 ---/
			     d1 -|\\
			     e1 -| |------\\
			     f1 -|/        x---- oe
			          | w1 ---/
			     e1 -|\\
			     f1 -| |------\\
			     g1 -|/        x---- of
			          | w1 ---/
			     f1 -|\\
			     g1 -| |------\\
			     0  -|/        x----og
			            w1 ---/

			Continuing this example, if we look at a 3x3 slice of the weights:
			::
				w1 w2 w3
				w4 w5 w6
				w7 w8 w9

			The corresponding xsel values are:
			::
				0  1  2
				0  1  2
				0  1  2

			and the corresponding input activations for oa are:
			::
				0 a0 b0
				0 a1 b1
				0 a2 b2

			NOTE: technically the above input activation indices should
			all be multiplied by the number of input channels since
			the input activations are stored with the channel dimension
			contiguous and the height dimension as the outer dimension

	"""
	kind = "basic_conv"
	def get_instance_comment(self, t="  "):
		comment = "Kernel shape: " + str(self.node.get_kernel_shape()) + "\n"
		comment += "Strides: " + str(self.node.get_strides()) + "\n"
		comment += "Number of input channel divisions: " + str(self.node.planner_estimate.n_channel_splits) + "\n"
		comment += "Number of parallel acts in a RAM: " + str(self.node.inputs[0]._from.get_parallel_acts_per_memory()) + "\n"
		comment += "Padding: " + self.node.properties["padding"] + ": " + str(self.padding) + "\n"
		return super(BasicConv, self).get_instance_comment(t=t) + v_comment_string(comment, t)

	def instantiate(self, node, inst, build_data_path=True, clock_2x=None, enable_bfp_override=False, **kwargs):

		assert(enable_bfp_override == False) #No block floating point support in regular convolution module, only TensorConv

		physically_mapped = False
		if "physically_mapped_rams" in kwargs:
			physically_mapped = kwargs["physically_mapped_rams"]

		estimate = self.node.planner_estimate

		parallel_acts_per_ram = node.inputs[0]._from.get_parallel_acts_per_memory()
		input_signed, weights_signed, output_signed = self.get_signs()
		signed = [weights_signed, input_signed]

		input_bits, weight_bits, output_bits = self.get_bit_widths()
		planned_bits = [weight_bits, input_bits]
		
		is_depthwise = node.type == "DepthwiseConv2dNative"
		input_act_bit_spec = self.get_bit_spec("input")
		output_act_bit_spec = self.get_bit_spec("output")

		n_channel_splits = estimate.n_channel_splits
		self.n_channel_splits = n_channel_splits
		parallel_weights_per_dsp = min(2, n_channel_splits)

		break_chain_every_n_weights = 57 # Adds a delay of delay_for_chain_break every N weights (for breaking long DSP chains to allow placement)
		delay_for_chain_break = 9        # Delay added whenever a DSP chain is broken.  The default is the number of input pipeline stages for a DSP plus the output DSP delay
		stagger_every_n_weights = 2 
		if arch.hpipe_device_arch.arch_name == "S10_NX":
			stagger_every_n_weights = 6 #Vector mode allows for 6 multiplications instead of 2
		reduce_with_carry_out = stagger_every_n_weights > 0

		input_channels = self.get_input_act(dim="channels")	
		input_height = self.get_input_act(dim="height")
		input_width = self.get_input_act(dim="width")
		output_width = self.get_output_act(dim="width")
		output_height = self.get_output_act(dim="height")
		k_shape = node.get_kernel_shape()
		kernel_height = k_shape[0]
		kernel_width = k_shape[1]
		vertical_stride = node.get_strides()[1]
		horizontal_stride = node.get_strides()[2]
		self.buffered_layer = True

		input_frac_bits, weight_frac_bits, desired_fractional_bits = self.get_frac_part_widths()
		_, _, output_int_bits = self.get_int_part_widths()
		fractional_bits_in_output = weight_frac_bits + input_frac_bits	
		
		if is_depthwise:
			input_channels = k_shape[2]

		from_spec = {
			"frac" : fractional_bits_in_output,
			"sign" : 1,
			"int" : 64 - 1 - fractional_bits_in_output
		}
		to_spec = {
			"frac" : desired_fractional_bits,
			"int" : output_int_bits
		}
		if output_signed:
			to_spec["sign"] = 1
		else:
			to_spec["sign"] = 0

		round_const = -1
		if fractional_bits_in_output > desired_fractional_bits:
			round_const = (fractional_bits_in_output - desired_fractional_bits - 1)

		# I've added this for compatibility because the code originally used a 
		# single stride but over time I do hope to migrate it over to use 
		# independent strides (so new code should use the correct stride
		#  above, but old code still limits us to a single stride)
		stride = vertical_stride
	
		f_max_channels_in_input_buffer_line = lambda: math.ceil(input_channels / n_channel_splits)
		f_max_ia_buffer_depth = lambda: f_max_channels_in_input_buffer_line()  * (max(kernel_height, vertical_stride) + vertical_stride)

		while f_max_ia_buffer_depth() > 64 and f_max_ia_buffer_depth() < 70 and not is_depthwise:
			n_channel_splits += 2
		self.n_channel_splits = n_channel_splits
		parallel_weights_per_dsp = min(stagger_every_n_weights, n_channel_splits) 
		max_channels_in_input_buffer_line = f_max_channels_in_input_buffer_line()
		max_ia_buffer_depth = f_max_ia_buffer_depth()
		min_ia_buffer_depth = max_ia_buffer_depth
		ia_buffer_has_lutrams = False
		if min_ia_buffer_depth <= 32:
			ia_buffer_has_lutrams = True
			min_ia_buffer_depth = math.floor(32 / max_channels_in_input_buffer_line) * max_channels_in_input_buffer_line
		elif min_ia_buffer_depth <= 64:
			ia_buffer_has_lutrams = True
			min_ia_buffer_depth = math.floor(64 / max_channels_in_input_buffer_line) * max_channels_in_input_buffer_line
		else:
			if min_ia_buffer_depth < 128:
				print("WARNING: IABuffer depth greater than 64, less than 128 ", min_ia_buffer_depth)
			target_buffer_depth = math.ceil(max_ia_buffer_depth / 512) * 512
			min_ia_buffer_depth = math.floor(target_buffer_depth / max_channels_in_input_buffer_line) * max_channels_in_input_buffer_line

		lines_in_ia_buffer = min_ia_buffer_depth // max_channels_in_input_buffer_line 
		self.lines_in_ia_buffer = lines_in_ia_buffer

		sorted_x_accumulation = estimate.sorted_x_accumulation(node)
		#sorted_x_accumulation = False
		if sorted_x_accumulation:
			post_dsp_x_mux_delay = 1
			dsp_to_x_mux_delay = math.ceil(math.sqrt(n_channel_splits) / 3)
			has_stride_counter = min(stride, kernel_width) > 1
			in_dsp_round_const = -1
			if round_const == -1:
				round_const = 0
			else:
				round_const = 1 << round_const
		else:
			post_dsp_x_mux_delay = 0
			has_stride_counter = False
			dsp_to_x_mux_delay = 0
			in_dsp_round_const = round_const

		# Delay scheme
		# OC Controller controls which Weight Buffer Address to read from
		# + 2 Cycles for read
		# + 1 Cycle for RLProcessor
		# + 1 Cycle to add RL address to line address
		# + 1 Cycle to MOD added RL Address by the memory depth
		# + 2 Cycles for read from Activation Buffer
		# + 2 Cycles to mux the right x
		# + 4 cycles to multiply and accumulate + external_reduction delay (3 cycles for S10_NX, computed in BasicConvDSPs)

		memory_model_delay        = 1
		memory_read_delay         = 2
		if min_ia_buffer_depth < 64:
			memory_read_delay = 1
		added_read_delay          = memory_read_delay - memory_model_delay

		weight_buffer_to_rl_processor_delay = 1
		rl_processor_delay        = 1
		line_addr_add_delay       = 1
		address_mod_delay         = 1
		address_computation_delay = weight_buffer_to_rl_processor_delay + rl_processor_delay + line_addr_add_delay + address_mod_delay
		overflow_detection_delay  = 2

		mux_delay                 = 2
		if kernel_width <= 4:
			mux_delay = 1
		if kernel_width <= 2:
			mux_delay = 0
		mux_to_dsp_delay          = 1 #math.ceil(math.sqrt(n_channel_splits * output_width) / 4.)
		if ia_buffer_has_lutrams and kernel_width == 1:
			mux_to_dsp_delay = 0

		if sorted_x_accumulation:
			mux_to_dsp_delay = 0
			mux_delay = 0
		# The in_oc_counter counts down from the number of weights in the output channel minus 1 to zero
		#  We use the reset signal from this to determine when to accumulate.  We don't want to accumulate
		#  for the first element because we need to start the accumulation from zero, but the reset signal
		#  happens one cycle earlier than this so we need to add a delay to align them.
		in_oc_reset_to_counter_start_delay = 1

		done_to_controller_delay = 4
		reset_fanout_delay = 0

		ia_shape = node.inputs[0]._from.example_outputs[0].shape
		if type(node.properties["padding"]) is bytes:
			node.properties["padding"] = node.properties["padding"].decode("ascii")
		padding, _ = get_padding(
			ia_shape,
			k_shape,
			stride,
			node.properties["padding"],
			node.explicit_padding)
		self.padding = padding
		dsp_chain_count = output_width
		if sorted_x_accumulation and horizontal_stride > 1:
			total_input_width = np.sum(padding[2]) + ia_shape[2]
			used_input_width = output_width * horizontal_stride
			if kernel_width > horizontal_stride:
				used_input_width += kernel_width - horizontal_stride
			dsp_chain_count = math.ceil(used_input_width / horizontal_stride)


		self.ibc = inst(InputBufferController,
			vertical_stride,
			kernel_height,
			input_channels,
			input_height,
			padding[1],
			n_channel_splits,
			output_height, min_buffer_depth=lines_in_ia_buffer)
		self.space_to_write_line.set_driver(self.ibc.space_to_write_line)
		self.ibc.write.set_driver(self.write)
		self.ibc.done_image.set_driver(self.done.delay(done_to_controller_delay))
		
		use_ia_buffer = max_ia_buffer_depth > 64 or ((kernel_width > 1 or horizontal_stride > 1) and not sorted_x_accumulation) or has_stride_counter
		#use_ia_buffer = True

		if use_ia_buffer:
			self.ia_buffer = inst(IABuffer,
				planned_bits[1],
				kernel_height,
				stride,
				k_shape[2],
				input_width,
				write_port_width=parallel_acts_per_ram, ic_groups=n_channel_splits, min_buffer_depth=lines_in_ia_buffer)
		else:
			assert(not has_stride_counter)
			self.ia_buffer = inst(SequentialDistributedActivationBuffer,
				planned_bits[1],
				kernel_height,
				stride,
				k_shape[2],
				input_width,
				write_port_width=parallel_acts_per_ram, ic_groups=n_channel_splits, min_buffer_depth=lines_in_ia_buffer)

		# drive write control signals for IABuffer (write addrs, write enables, and write data)
		# Addrs:
		for ibc_w_addr, ia_buffer_w_addr in zip(self.ibc.write_addresses, self.ia_buffer.w_addrs):
			ia_buffer_w_addr.set_driver(ibc_w_addr)
		# Enables:
		for wo, ia_buffer_write in zip(self.ibc.write_outs, self.ia_buffer.writes):
			ia_buffer_write.set_driver(wo)
		# Data:
		ram_inputs = self.inputs
		if self.ibc.has_padding:
			padding_zeros = inst(dl.Constant, bits=planned_bits[1], value=0, name="padding_zeros")
			ram_inputs = [inst(dl.MuxV2, w, padding_zeros, i, name="input_or_padding_"+str(j))for j,(i,w) in enumerate(zip(self.inputs, self.writes))]

			"""
			padding_zeros = inst(dl.Constant, bits=planned_bits[1], value=0, name="padding_zeros")
			ram_inputs = []
			for i in range(math.ceil(len(self.inputs) / 2)):
				with dl.ParamScope(clock=clock_2x):
					ab_input_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=1)
				pad_mux_sel = inst(dl.Concat, self.writes, ab_input_counter, name="pad_mux_sel")
				ram_inputs.append(inst(dl.MuxV2,
					pad_mux_sel, padding_zeros, *self.inputs[i*2:i*2+2], name=f"input_or_padding_{i}"))"""

		for ri, ia_buffer_w_data in zip(ram_inputs, self.ia_buffer.inputs):
			ia_buffer_w_data.set_driver(ri)

		staggered_chain_break_delay = delay_for_chain_break
		if arch.hpipe_device_arch.arch_name == "S10_NX":
			staggered_chain_break_delay += 1 #NX has extra registers on cascade chain so we need an extra cycle

		staggered_weight_info = BasicConv.get_staggered_weight_info(
			parallel_weights=n_channel_splits,
			stagger_every_n_weights=stagger_every_n_weights,
			break_chain_every_n_weights=break_chain_every_n_weights,
			delay_for_chain_break=staggered_chain_break_delay) 
		max_stagger = max(staggered_weight_info["staggers"])

		(weights_per_oc,
			weights_per_oc_bits,
			serial_weights,
			serial_weights_bits,
			numpy_combined_weights) = self._get_serialized_weights_and_rls(
				parallel_weights=n_channel_splits, stagger_every_n_weights=stagger_every_n_weights,
				is_depthwise=is_depthwise, staggered_weight_info=staggered_weight_info,
				group_x_dim=sorted_x_accumulation,
				delay_for_chain_break=staggered_chain_break_delay)
		example_input = node.input_nodes()[0].example_outputs[0]
		self.numpy_combined_weights = numpy_combined_weights
		self.weights = self.node.values[0]
		self.weights_per_oc = weights_per_oc
		self.example_input = example_input
		if node.explicit_padding is not None:
			self.example_input = np.pad(example_input, node.explicit_padding, "constant", constant_values=0)
		if padding[2][0] >= 0:
			self.padded_example_input = np.pad(example_input, padding, "constant", constant_values=0)
		else:
			self.padded_example_input = example_input[0,:padding[1][0],:padding[2][0],:]

		#print("  Serial weights bits " + str(serial_weights_bits))
		bits = []
		for i in range(n_channel_splits):
			bits.append(serial_weights_bits[i * 3])
			bits.append(planned_bits[1])

		if "partitioning" in kwargs:
			partitioning = kwargs["partitioning"]
		else:
			partitioning = False	
		if partitioning == False:
			dsps = inst(BasicConvDSPs,
				dsp_chain_count, bits, [input_signed, weights_signed],
				parallel_weights=n_channel_splits,
				parallel_weights_per_dsp=parallel_weights_per_dsp,
				reduce_with_carry_out=reduce_with_carry_out,
				build_data_path=build_data_path,
				staggered_weight_info=staggered_weight_info,
				delay_for_chain_break=delay_for_chain_break,
				round_const=in_dsp_round_const,
				name="dsps")
			mac_delay = dsps.added_delay + dsps.extra_delay
			self.dsps = dsps
			self.utilization_estimates["DSPs"] = self.dsps.count


		weight_bit_divisions = np.pad(np.cumsum(serial_weights_bits), pad_width=[1, 0], mode="constant", constant_values=0)
		self.weight_buffer = inst(dl.FanoutROM,
			content_list=serial_weights,
			bits=np.sum(serial_weights_bits),
			physically_mapped=physically_mapped,
			name="weight_buffer")


		#print("  Weight bit divisions: " + str(weight_bit_divisions))
		self.utilization_estimates["Weight Buffer Lines"] = len(serial_weights)
		self.utilization_estimates["Weight Buffer Width"] = np.sum(serial_weights_bits)
		
		self.weight_buffer_read_enable = inst(dl.Logic, 1, name="weight_buffer_read_enable")
		self.weight_buffer.r_en.set_driver(self.weight_buffer_read_enable)
		self.weight_buffer_addr_counter = inst(dl.Counter,
			reset_value=0,
			increment_value=1,
			end_value=len(serial_weights)-1,
			name="weight_buffer_read_addr")
		self.weight_buffer_addr_counter.set_increment_condition(self.weight_buffer_read_enable)
		self.weight_buffer.r_addr.set_driver(self.weight_buffer_addr_counter.current_value)

		self.oc_controller = inst(OCController,
			weights_per_oc,
			weights_per_oc_bits,
			done_delay=max_stagger,
			name="output_channel_controller")
		normal_go = inst(dl.AND, self.can_write_line, self.ibc.has_full_kernel.delay(2))
		self.oc_controller.go.set_driver(normal_go)
		self.ibc.finished_reading_line.set_driver(self.oc_controller.done.delay(self.weight_buffer.read_delay+weight_buffer_to_rl_processor_delay + rl_processor_delay + line_addr_add_delay + address_mod_delay))
		self.ibc.started_reading_line.set_driver(self.oc_controller.state_machine.is_start.delay(2))
	
		self.weight_buffer_read_enable.set_driver(self.oc_controller.should_read_weights)
		# If the layer is called with partitioning, it means the caller only needs Memory estimation info and can terminate here
		if partitioning:
			return

		# Stamp out multiple state machines for the weight buffer since the FIFO could be very wide
		"""oc_controllers = [self.oc_controller]
		if isinstance(self.weight_buffer.rom, dl.FIFORingROM):
			for i in range(0, len(self.weight_buffer.rom.fifo.fifos) // 6, 6):
				ibc = inst(InputBufferController, vertical_stride, kernel_height, input_channels, input_height, padding[1], n_channel_splits, output_height, min_buffer_depth=lines_in_ia_buffer)
				ibc.write.set_driver(self.write)
				ibc.done_image.set_driver(self.done.delay(done_to_controller_delay))

				oc_controller = inst(OCController,
					weights_per_oc,
					weights_per_oc_bits,
					done_delay=max_stagger,
					name="output_channel_controller")
				oc_controllers.append(oc_controller)
				oc_controller.go.set_driver(inst(dl.AND, self.can_write_line, ibc.has_full_kernel, inst(dl.NOT, self.weight_buffer.rom.fifo.fifos[i].empty)))
				ibc.finished_reading_line.set_driver(oc_controller.done)
				ibc.started_reading_line.set_driver(oc_controller.state_machine.c["is_start"])
				for fifo in self.weight_buffer.rom.fifo.fifos[i:i+6]:
					fifo.r_en.set_driver(oc_controller.should_read_weights)"""


		self.rls = []
		self.read_addresses = []
		with dl.ModuleScope(inst(dl.Module, "activation_address_computers",
				kind="activation_address_computers", name="activation_address_computers")):
			for i in range(n_channel_splits):
				stagger_amount = staggered_weight_info["staggers"][i]

				# Each address computer computes addresses for a group of input channels,
				# so group channels is the number of channels in this group
				group_channels = self.node.values[0][0,0,i::n_channel_splits,0].shape[0]

				# to minimize the load on individual drivers we will take our done line
				# signal from one of the oc controller copies
				#oc_controller_index = int((i / n_channel_splits) * (len(oc_controllers)-1) + 0.5)
				occ = self.oc_controller#oc_controllers[oc_controller_index]
				done_line = occ.done.delay(self.weight_buffer.read_delay+stagger_amount+weight_buffer_to_rl_processor_delay)

				# each address computer will keep track of the current output index
				# so that it knows when we finish an input image.
				output_line_counter = inst(dl.Counter,
					reset_value=0,
					increment_value=1,
					end_value=output_height-1,
					name="output_line_counter")
				output_line_counter.set_increment_condition(done_line)

				# The line address counter is the base address to which the decoded RL
				#  values are added.  Basically as the convolution window shifts down
				#  we increment this counter.  Our buffer has limited space though, so
				#  the counter decrements itself by the number of input lines we store
				#  whenever it exceeds the number of lines we store.
				# The second change value handles the edge case of the end of an input
				#  image where we need to add not the stride, but the size of the kernel.
				#  the stride will be added by the first change value, and the difference
				#  between the stride and the kernel size added by the second change 
				#  value.
				# The third change value is for Depthwise convolutions.  Since the input
				#  channel is no longer computed by runlengths in depthwise convolutions,
				#  we have added the input channel component to the line_address_counter
				line_addr_counter_reset_value = 0
				total_lines_written = max(np.sum(padding[1]),0) + input_height
				target_second_start_value = (group_channels * total_lines_written + line_addr_counter_reset_value)
				value_after_output_lines_read = (group_channels * stride * output_height)
				start_end_difference = target_second_start_value - value_after_output_lines_read
				# The modulo only works if we go over the modulo value. If our change value
				# is negative and we are at address 0, then we go negative and end up with
				# something totally wrong
				if start_end_difference < 0:
					start_end_difference += group_channels * lines_in_ia_buffer
				line_address_change_values = [
						group_channels * stride,
						start_end_difference]
				if is_depthwise:
					line_address_change_values.append(1)
					line_address_change_values[0] -= group_channels
				line_address_counter = inst(dl.ModuloMultiChangeCounter,
					reset_value=line_addr_counter_reset_value,
					change_values=line_address_change_values,
					modulo_value=group_channels * lines_in_ia_buffer,
					end_value=group_channels * (lines_in_ia_buffer + 2),
					tolerable_delay=1, # this only changes when we finish a line, so we can
					                   # tolerate at least 2 cycles to perform the modulo operation.
					                   # This optimization is for timing.
					name="line_address")
				line_address_counter.change_signals[0].set_driver(done_line)
				line_address_counter.change_signals[1].set_driver(inst(dl.AND, done_line, output_line_counter.is_done.delay(1)))
				if is_depthwise:
					line_address_counter.change_signals[2].set_driver(occ.done_oc.delay(stagger_amount),
						delay=self.weight_buffer.read_delay + weight_buffer_to_rl_processor_delay+rl_processor_delay)

				with dl.ParamScope(name="runlength"):
					
					upper_index = weight_bit_divisions[i*3+2] - 1
					lower_index = weight_bit_divisions[i*3+1]

					if upper_index < lower_index: 
						raise Exception("Error! The runlengths for this input channel group", i, "have a width of 0. This is a bug and the hardware generated if we continued would be incorrect. Please see https://gitlab.com/mathewkhall/hpipe/-/issues/4 for more information")
					
					self.rls.append(self.weight_buffer.r_data[upper_index:lower_index].delay(weight_buffer_to_rl_processor_delay))
							
				rl = self.rls[-1]
				if is_depthwise:
					rl = inst(dl.MUL, rl, inst(dl.Constant, value=group_channels), extend_to_match_bits=False)
				rl_bits = rl._bit_width

				# this is where rl_processor_delay comes from
				rl_processor = inst(RLProcessor,
					rl_bits,
					self.ia_buffer.r_addrs[i]._bit_width,
					name="rl_processor_c_" + str(i))

				# technically the chip supports reads with a single cycle delay, so that's what the memory models,
				# but that only works for a low FMAX, so we need to add an additional cycle
				#
				rl_processor.rl.set_driver(rl)
				# delay the read signal by the memory read delay to create a valid signal
				rl_processor.rl_valid.set_driver(self.weight_buffer_read_enable, delay=self.weight_buffer.read_delay + weight_buffer_to_rl_processor_delay)
				rl_processor.clear.set_driver(occ.in_oc_counter_reset.delay(stagger_amount),
					delay=self.weight_buffer.read_delay + weight_buffer_to_rl_processor_delay + rl_processor_delay)

				# This is where line_addr_add_delay comes from
				line_address_plus_rl_address = inst(dl.Flop,
					driver=line_address_counter.current_value + rl_processor.address,
					name="line_address_plus_rl_address")

				to_add = inst(dl.MuxV2,
					line_address_plus_rl_address >= inst(dl.Constant,
						bits=line_address_plus_rl_address._bit_width,
						value=group_channels * (lines_in_ia_buffer), name="last_address"),
					inst(dl.Constant,
						bits=line_address_plus_rl_address._bit_width,
						value=0, name="dont_subtract_buffer_depth_zeros"),
					inst(dl.Constant,
						bits=line_address_plus_rl_address._bit_width,
						value=-group_channels * (lines_in_ia_buffer),
						name="negative_buffer_depth"))

				# This is where address_mod_delay comes from
				address = inst(dl.Flop,
					driver=(line_address_plus_rl_address + to_add)[self.ia_buffer.r_addrs[i]._bit_width-1:0],
					name="read_address_" + str(i))
				self.ia_buffer.r_addrs[i].set_driver(address)
				self.read_addresses.append(address)
		self.ia_buffer.read.set_driver(self.weight_buffer_read_enable.delay(self.weight_buffer.read_delay+address_computation_delay))

		act_indices = None
		if serial_weights_bits[2] != 0:
			with dl.ParamScope(name="act_index"):
				act_indices = [self.weight_buffer.r_data[weight_bit_divisions[i*3+3]-1:weight_bit_divisions[i*3+2]] for i in range(n_channel_splits)]
			act_index_fanouts = [[inst(dl.Logic, act_indices[i]._bit_width, name="act_index_" + str(i) + "_fanout") for _ in range(dsp_chain_count)] for i in range(n_channel_splits)]
		else:
			mux_delay = 0
		if sorted_x_accumulation:
			mux_delay = 0
		with dl.ParamScope(name="weight"):
			weights = [self.weight_buffer.r_data[weight_bit_divisions[i*3+1]-1:weight_bit_divisions[i*3]] for i in range(n_channel_splits)]
		weight_fanouts_in = [[inst(dl.Logic, bits=weights[i]._bit_width, name="weight_" + str(i) + "_fanout") for _ in range(dsp_chain_count)] for i in range(n_channel_splits)]
		weight_spread_fanout_delay = min(2, math.floor(((n_channel_splits**2) * dsp_chain_count)**(0.25)/3))
		weight_fanout_pipe_levels = max(math.ceil(math.log(dsp_chain_count, 1 + 32 / n_channel_splits)), 2)

		# Adding this limit to 9 to work around an issue that causes a mismatch in the latency between the weights
		# and activations when they reach a DSP block
		# 9 Seems to work well and it has been tested on ResNet-50 sparse
		# For more details see issue "Mismatch in delay between weights and activations arriving at DSP"
		weight_fanout_pipe_levels = min(9,weight_fanout_pipe_levels)
		if not use_ia_buffer:
			weight_spread_fanout_delay = 0
			weight_fanout_pipe_levels = 1
		weight_fanouts = [[wf.delay(weight_spread_fanout_delay) for wf in wfil] for wfil in weight_fanouts_in]

		accumulate_fanouts_in = [inst(dl.Logic, bits=1, synthesis_attributes="synthesis dont_merge", name="accumulate") for _ in range(dsp_chain_count)]
		accumulate_fanouts = [af.delay(weight_spread_fanout_delay) for af in accumulate_fanouts_in]

		weight_shift_delay = address_computation_delay + self.ia_buffer.control_delay + self.ia_buffer.read_delay + mux_delay + mux_to_dsp_delay - weight_fanout_pipe_levels
		if use_ia_buffer:
			piped_accumulate_fanouts = inst(dl.PipelinedFanout,
				inst(dl.NOT, self.oc_controller.in_oc_counter_reset, name="accumulate").delay(
					in_oc_reset_to_counter_start_delay + 
					self.weight_buffer.read_delay + 
					address_computation_delay + 
					self.ia_buffer.control_delay +
					self.ia_buffer.read_delay +
					mux_delay +
					mux_to_dsp_delay +
					max_stagger - weight_fanout_pipe_levels - weight_spread_fanout_delay),
				*accumulate_fanouts_in,
				levels=weight_fanout_pipe_levels,
				name="accumulate_fanout_pipe")

			piped_weight_fanouts = [inst(dl.PipelinedFanout,
				w.delay(weight_shift_delay - weight_spread_fanout_delay, pack_to_memory=True),
				*weight_fanouts_in[i],
				levels=weight_fanout_pipe_levels,
				name="weight_" + str(i) + "_fanout_pipe") for i,w in enumerate(weights)]
		else:
			print(
				in_oc_reset_to_counter_start_delay,
				self.weight_buffer.read_delay,
				address_computation_delay,
				self.ia_buffer.control_delay,
				mux_delay,
				mux_to_dsp_delay,
				max_stagger,
				weight_fanout_pipe_levels,
				weight_spread_fanout_delay)
			# NOTE: Need to insure delay accounts for stride group size for pre muxes
			weight_fanout_chains = []
			accumulate_fanout_chains = []
			delayed_a = inst(dl.NOT, self.oc_controller.in_oc_counter_reset, name="accumulate").delay(
				in_oc_reset_to_counter_start_delay + 
				self.weight_buffer.read_delay + 
				address_computation_delay + 
				self.ia_buffer.control_delay +
				mux_delay +
				mux_to_dsp_delay -
				weight_fanout_pipe_levels +
				max_stagger - 1)
			for chain_number in range(math.ceil(dsp_chain_count / self.ia_buffer.max_offset)):
				accumulate_fanout_chains.append(inst(dl.BasicShiftRegister,
						1, self.ia_buffer.max_offset,
						name="accumulate_chain_" + str(chain_number)))
				accumulate_fanout_chains[-1].d.set_driver(delayed_a)

			for wn, w in enumerate(weights):
				delayed_w = w.delay(weight_shift_delay - self.ia_buffer.read_delay - 2, pack_to_memory=True)
				parallel_chains = []
				for chain_number in range(self.ia_buffer.parallel_sequential_fanouts):
					parallel_chains.append(inst(dl.BasicShiftRegister,
						w._bit_width, self.ia_buffer.max_offset,
						name="weight_" + str(wn) + "_chain_" + str(chain_number)))
					parallel_chains[-1].d.set_driver(delayed_w)
				weight_fanout_chains.append(parallel_chains)

			for i,afi in enumerate(accumulate_fanouts_in):
				afi.set_driver(accumulate_fanout_chains[i//self.ia_buffer.max_offset].sr[i%self.ia_buffer.max_offset])

			for ic_group, (wfl, wcl) in enumerate(zip(weight_fanouts_in, weight_fanout_chains)):
				for activation_number, wf in enumerate(wfl):
					wf.set_driver(wcl[activation_number//self.ia_buffer.max_offset].sr[activation_number%self.ia_buffer.max_offset].delay(1))

			
		control_to_output_valid_delay = (
			self.weight_buffer.read_delay +
			address_computation_delay +
			self.ia_buffer.control_delay +
			self.ia_buffer.read_delay +
			mux_delay +
			mux_to_dsp_delay +
			mac_delay +
			dsp_to_x_mux_delay +
			post_dsp_x_mux_delay +
			overflow_detection_delay)
		output_valid_fanout_levels = math.ceil(math.log(dsp_chain_count, 4))
		piped_output_valid_fanouts = inst(dl.PipelinedFanout,
			self.output_valid.delay(
				control_to_output_valid_delay - 
				output_valid_fanout_levels),
			*self.outputs_valid,
			levels=output_valid_fanout_levels,
			name="output_valid_fanout_pipe")

		if act_indices is not None:
			total_delay = address_computation_delay + self.ia_buffer.control_delay + self.ia_buffer.read_delay
			fanout_component = min(total_delay, math.ceil(math.log(n_channel_splits * dsp_chain_count, 4)))
			piped_act_index_fanouts = [inst(dl.PipelinedFanout,
				ai.delay(total_delay - fanout_component),
				*act_index_fanouts[i],
				levels=fanout_component,
				name="act_index_" + str(i) + "_fanout_pipe") for i,ai in enumerate(act_indices)]

		self._set_control_to_input_delay(self.ibc.write_counter_increment_delay + self.ia_buffer.control_to_input_delay)
		self._set_control_valid_to_output_valid_delay(control_to_output_valid_delay)

		r_data = []
		for rdl in self.ia_buffer.outputs:
			r_data_l = []
			for rd in rdl:
				r_data_l.append(rd.delay(mux_delay))
			r_data.append(r_data_l)
		if sorted_x_accumulation:
			pre_dsp_mux_size = min(stride, kernel_width)
			pre_dsp_mux_name = "stride_muxes"
		else:
			pre_dsp_mux_size = kernel_width
			pre_dsp_mux_name = "x_muxes"
		pre_dsp_muxes = inst(dl.Module, pre_dsp_mux_name, kind=pre_dsp_mux_name, name=pre_dsp_mux_name)
		if not sorted_x_accumulation or has_stride_counter:
			with dl.ModuleScope(pre_dsp_muxes):
				padding_data = inst(dl.Constant, bits=r_data[0][0]._bit_width, value=0, name="x_padding_zeros")
			for i in range(n_channel_splits):
				if padding[2][0] >= 0:
					r_data[i] = [padding_data] * padding[2][0] + r_data[i] + [padding_data] * padding[2][1]

		self.dsp_valid = inst(dl.Logic, bits=1, name="dsp_valid")
		self.not_accumulate = inst(dl.Logic, bits=1, name="not_accumulate")
		self.dsp_valid_no_stagger = inst(dl.Logic, bits=1, name="dsp_valid_no_stagger")
		self.not_accumulate_no_stagger = inst(dl.Logic, bits=1, name="not_accumulate_no_stagger")
		if sorted_x_accumulation:
			kernel_x_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=kernel_width-1, name="kernel_x_counter")
			kernel_x_counter.set_increment_condition(inst(dl.AND, self.dsp_valid, self.not_accumulate))
			post_dsp_x_count = kernel_x_counter
			total_delay = (
				self.weight_buffer.read_delay +
				address_computation_delay +
				self.ia_buffer.control_delay +
				self.ia_buffer.read_delay)
			if has_stride_counter:
				stride_kernel_x_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=kernel_width-1, name="stride_kernel_x_counter")
				stride_kernel_x_counter.set_increment_condition(inst(dl.AND, self.dsp_valid_no_stagger, self.not_accumulate_no_stagger))
				stride_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=min(stride, kernel_width)-1, name="stride_counter")
				stride_counter.set_increment_condition(inst(dl.AND, self.dsp_valid_no_stagger, self.not_accumulate_no_stagger))
				stride_counter.reset_signal.set_driver(inst(dl.OR, kwargs["reset"], stride_counter.done_and_incrementing, stride_kernel_x_counter.done_and_incrementing))
				stride_counter_fanout = [[inst(dl.Logic, bits=stride_counter.current_value._bit_width, name=f"stride_counter_fanout_{i}_{j}") for j in range(dsp_chain_count)] for i in range(n_channel_splits)]

				fanout_component = min(total_delay, math.ceil(math.log(dsp_chain_count, 4)))
				previous_stagger = 0
				staggered_stride_counter = stride_counter.current_value
				for i in range(n_channel_splits):
					stagger_amount = staggered_weight_info["staggers"][i]
					to_stagger = stagger_amount - previous_stagger
					previous_stagger = stagger_amount
					staggered_stride_counter = staggered_stride_counter.delay(to_stagger)
					inst(dl.PipelinedFanout,
						staggered_stride_counter.delay(total_delay - fanout_component),
						*stride_counter_fanout[i],
						levels=fanout_component,
						name="stride_counter_fanout_pipe")
				kernel_from_stride_column_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=math.ceil(kernel_width / stride)-1)
				kernel_from_stride_column_counter.set_increment_condition(stride_counter.done_and_incrementing.delay(max_stagger))
				kernel_from_stride_column_counter.reset_signal.set_driver(inst(dl.OR, kwargs["reset"], kernel_from_stride_column_counter.done_and_incrementing.delay(max_stagger), stride_kernel_x_counter.done_and_incrementing.delay(max_stagger)))
				post_dsp_x_count = kernel_from_stride_column_counter

			fanout_count = dsp_chain_count
			post_dsp_x_count_fanout = [inst(dl.Flop,
				bits=post_dsp_x_count.current_value._bit_width,
				name="post_dsp_x_count_fanout_" + str(i)) for i in range(fanout_count)]
			total_delay += mux_delay + mux_to_dsp_delay + mac_delay + dsp_to_x_mux_delay - 1
			fanout_component = min(total_delay, math.ceil(math.log(fanout_count, 4)))
			inst(dl.PipelinedFanout,
				post_dsp_x_count.current_value.delay(total_delay - fanout_component),
				*post_dsp_x_count_fanout,
				levels=fanout_component,
				name="post_dsp_x_counter_fanout_pipe")


		with dl.ModuleScope(pre_dsp_muxes):
			for i in range(dsp_chain_count):
				dsp_base_index = i*n_channel_splits*2
				muxes = []
				for j in range(n_channel_splits):
					if has_stride_counter:
						mux = inst(dl.MuxV2, stride_counter_fanout[j][i], *r_data[j][i*stride:i*stride+pre_dsp_mux_size]).delay(mux_to_dsp_delay)
						muxes.append(mux)
					elif act_indices is not None:
						mux = inst(dl.MuxV2, act_index_fanouts[j][i].delay(mux_delay), *r_data[j][i*stride:i*stride+pre_dsp_mux_size]).delay(mux_to_dsp_delay)
						muxes.append(mux)
					else:
						mux = r_data[j][i*stride].delay(mux_to_dsp_delay)
						muxes.append(mux)
				for j in range(n_channel_splits):
					dsps.inputs[dsp_base_index+j*2+1].set_driver(muxes[j])
					dsps.inputs[dsp_base_index+j*2].set_driver(weight_fanouts[j][i])
				dsps.accumulate_signals[i].set_driver(accumulate_fanouts[i])

		self.not_accumulate.set_driver(self.oc_controller.in_oc_counter_reset.delay(max_stagger))
		self.dsp_valid.set_driver(self.weight_buffer_read_enable.delay(max_stagger))
		self.not_accumulate_no_stagger.set_driver(self.oc_controller.in_oc_counter_reset)
		self.dsp_valid_no_stagger.set_driver(self.weight_buffer_read_enable)
		if sorted_x_accumulation:
			x_counter_increment = inst(dl.AND, self.dsp_valid, self.not_accumulate)
			x_counter = inst(dl.Counter, reset_value=0, end_value=kernel_width-1, increment_value=1, name="x_counter")
			x_counter.set_increment_condition(x_counter_increment)
			external_accumulate_clear_fanout = [inst(dl.Flop, bits=1, name="external_accumulate_clear_fanout_" + str(i)) for i in range(output_width)]
			external_accumulate_add_fanout = [inst(dl.Flop, bits=1, name="external_accumulate_add_fanout_" + str(i)) for i in range(output_width)]

			total_delay = (
				self.weight_buffer.read_delay +
				address_computation_delay +
				self.ia_buffer.control_delay +
				self.ia_buffer.read_delay +
				mux_delay +
				mux_to_dsp_delay +
				mac_delay +
				dsp_to_x_mux_delay -
				1)
			fanout_count = output_width
			fanout_component = min(total_delay, math.ceil(math.log(fanout_count, 4)))
			external_accumulate_add_fanout_pipe = inst(dl.PipelinedFanout,
				x_counter_increment.delay(total_delay - fanout_component),
				*external_accumulate_add_fanout,
				levels=fanout_component)
			external_accumulate_clear_fanout_pipe = inst(dl.PipelinedFanout,
				x_counter.done_and_incrementing.delay(total_delay - fanout_component + 1),
				*external_accumulate_clear_fanout,
				levels=fanout_component)
			self.output_valid.set_driver(inst(dl.AND, x_counter_increment, x_counter.is_done))
		else:
			self.output_valid.set_driver(inst(dl.AND, self.dsp_valid, self.not_accumulate))
		self.requires_driver = [self.can_write_line]


		if use_ia_buffer:
			realign_delays = [0 for _ in range(dsp_chain_count)]
		else:
			realign_delays = [self.ia_buffer.max_offset - 1 - i % self.ia_buffer.max_offset for i in range(dsp_chain_count)]
		outputs = self.dsps.outputs
		output_cast_modules = self.dsps.dsp_scopes
		if sorted_x_accumulation:
			external_accumulator_bits = 36
			delayed_accumulated_data = [o[external_accumulator_bits-1:0].delay(d, pack_to_memory=True).delay(dsp_to_x_mux_delay) for o,d in zip(self.dsps.outputs, realign_delays)]
			realign_delays = [0 for _ in range(len(self.outputs))]
			if not has_stride_counter:
				padding_data = inst(dl.Constant, bits=delayed_accumulated_data[0]._bit_width, value=0, name="x_padding_zeros")
				delayed_accumulated_data = [padding_data] * padding[2][0] + delayed_accumulated_data + [padding_data] * padding[2][1]
			chains_per_output = math.ceil(kernel_width / stride)
			outputs = []
			output_cast_modules = self
			for i in range(output_width):
				accumulation_register = inst(dl.Flop, bits=external_accumulator_bits, signed=True, name="external_accumulation_register_" + str(i))
				round_constant = inst(dl.Constant, bits=external_accumulator_bits, value=round_const, signed=True, name="round_const")
				accumulation_register.set_reset_driver(round_constant)
				accumulation_register_or_round_const = inst(dl.MuxV2, external_accumulate_clear_fanout[i], accumulation_register, round_constant, signed=True, name="accumulation_register_or_round_const")
				kernel_select_mux = inst(dl.MuxV2, post_dsp_x_count_fanout[i], *delayed_accumulated_data[i:i+chains_per_output], signed=True, name="kernel_select_mux")
				accumulate_mux = inst(dl.MuxV2, external_accumulate_add_fanout[i], inst(dl.Constant, bits=external_accumulator_bits, signed=True, value=0), kernel_select_mux, name="accumulate_mux")
				accumulation_register.set_driver((accumulation_register_or_round_const + accumulate_mux)[external_accumulator_bits-1:0])
				outputs.append(accumulation_register)
			from_spec = {
				"frac" : fractional_bits_in_output,
				"sign" : 1,
				"int" : external_accumulator_bits - 1 - fractional_bits_in_output
			}

		with dl.ParamScope(name="conv_mac_output"):
			for i,(o, realign_delay) in enumerate(zip(outputs, realign_delays)):
				if type(output_cast_modules) is list:
					#If this is the case then it means we want to use the tensor block in the mode that does 3 individual mults
					#In that case each cast module will instantate one MAC for 3 outputs
					if arch.hpipe_device_arch.arch_name == "S10_NX" and len(output_cast_modules) < len(outputs):
						output_cast_module = output_cast_modules[math.floor(i/3.0)]
					else:
						output_cast_module = output_cast_modules[i]
				else:
					output_cast_module = output_cast_modules
				with dl.ModuleScope(output_cast_module):
					output = o
					self.outputs[i].set_driver(cast_fixed(o.delay(overflow_detection_delay), from_spec, to_spec).delay(realign_delay, pack_to_memory=True))
		ibc_is_idle = inst(dl.Logic, bits=1, name="ibc_is_idle")
		ibc_is_idle.set_driver(self.ibc.state_machine.c["is_idle"])
		"""self.debug_stall = inst(dl.AND,
			ibc_is_idle,
			inst(dl.NOT, dl.Verilog.current_params.kwargs["reset"]),
			inst(dl.NOT, self.space_to_write_line),
			inst(dl.NOT, self.ibc.has_full_kernel),
			inst(dl.NOT, self.oc_controller.state_machine.is_processing_ocs))"""

		self.cred_inc.set_driver(self.oc_controller.done_row)
		self.cred_dec.set_driver(self.ibc.cred_dec)
		self.cred_dec_final.set_driver(self.ibc.done_image.delay(1))
		self.cred_dec_amount = self.ibc.cred_dec_amount
		self.cred_dec_final_amount = self.ibc.cred_dec_final_amount
		if node.inputs[0]._from.module is not None:
			self.cred_max[node.inputs[0]._from.module.name] = self.ibc.cred_max
		else:
			self.cred_max["None"] = self.ibc.cred_max
		self.cred_padding = padding[1] #Not too sure about this
		
		if (padding[1] != [0,0]):
			self.cred_padding_unpause = inst(dl.Logic, 1, name="credit_padding_unpause")
			self.cred_padding_unpause.set_driver(inst(dl.AND, self.ibc.in_padding_next.delay(2), inst(dl.NOT, self.ibc.in_padding_next.delay(1))))
		

	@classmethod
	def get_staggered_weight_info(cls,
		parallel_weights=2,
		stagger_every_n_weights=0,
		break_chain_every_n_weights=57,
		delay_for_chain_break=4):
		
		#NX does not currently support breaking tensor block chains (we would need 2 additional tensor blocks just to pass in the new values)
		#Ideally we won't even need support for this as Tensor Blocks can pack more multiplications and thus we'll need smaller chains for each layer
		stagger_value = 1
		if arch.hpipe_device_arch.arch_name == "S10_NX":
			break_chain_every_n_weights=150 #Support a max chain length of 25 tensors
			#NX tensor block has a extra register in the cascade chains
			stagger_value=2

		staggers = []
		multiplications_per_dsp = []
		chain_lengths = []
		if stagger_every_n_weights > 0:
			if break_chain_every_n_weights == 0 or parallel_weights <= (break_chain_every_n_weights + stagger_every_n_weights - 1):
				chains = 1
				total_systolic_delay = (parallel_weights - 1) // stagger_every_n_weights
				chain_lengths = [total_systolic_delay + 1]
				multipliers_per_chain = [parallel_weights]
				multiplications_per_dsp = [parallel_weights]
				
			else:
				chain_breaks = parallel_weights // break_chain_every_n_weights
				# We can only fit one multiplication in the first DSP in a chain if we split it,
				# but if we don't split it, we can fit parallel_weights.  In the case that
				# parallel_weights % break_chain_every_n_weights is less than parallel_weights, we can actually pack the last
				# broken chain into the second last chain (since there's an additional parallel_weights multipliers if we don't break it)
				first_chain_length = (parallel_weights % break_chain_every_n_weights)
				combine_first_two_chains = first_chain_length < stagger_every_n_weights
				if combine_first_two_chains:
					chain_breaks -= 1
					first_chain_length += break_chain_every_n_weights

				last_chain_delay = (first_chain_length - 1) // stagger_every_n_weights

				break_delay = delay_for_chain_break * chain_breaks
				systolic_delay_per_chain = (break_chain_every_n_weights - 1) // stagger_every_n_weights
				nonbreak_delay = chain_breaks * systolic_delay_per_chain + last_chain_delay
				total_systolic_delay = nonbreak_delay + break_delay
				chains = chain_breaks + 1
				chain_lengths = [last_chain_delay + 1] + [systolic_delay_per_chain + 1] * chain_breaks
				multipliers_per_chain = [first_chain_length] + [break_chain_every_n_weights] * chain_breaks


			# Compute the total systolic delay of the multiplier chain (excludes DSP input register delays)
			stagger_amount = 0
			accumulated_index = 0
			for chain_index, chain_length in enumerate(chain_lengths):
				multipliers_remaining_in_chain = multipliers_per_chain[chain_index]
				for i in range(chain_length):
					multipliers_for_this_dsp = multipliers_remaining_in_chain % stagger_every_n_weights
					if multipliers_for_this_dsp == 0:
						multipliers_for_this_dsp = stagger_every_n_weights
					if arch.hpipe_device_arch.arch_name != "S10_NX" and chain_index != 0 and i == 0:
						multipliers_for_this_dsp = 1
					multiplications_per_dsp.append(multipliers_for_this_dsp)
					staggers += [stagger_amount] * multipliers_for_this_dsp
					accumulated_index += multipliers_for_this_dsp
					multipliers_remaining_in_chain -= multipliers_for_this_dsp
					stagger_amount += stagger_value
				stagger_amount += delay_for_chain_break
		else:
			chain_lengths = [parallel_weights]
			staggers = [0] * parallel_weights
			multipliers_per_chain = [parallel_weights]

		#print("staggers", staggers, "multipliers_per_chain", multipliers_per_chain, "multipliers_per_dsp", multiplications_per_dsp, "chain_lengths", chain_lengths)
		return {
			"staggers" : staggers,
			"multipliers_per_chain": multipliers_per_chain,
			"multipliers_per_dsp": multiplications_per_dsp,
			"chain_lengths" : chain_lengths
		}


	@classmethod
	def get_serialized_weights_and_rls(cls,
		weights,
		parallel_weights=2,             # Number of multiplications per DSP
		stagger_every_n_weights=0,      # Adds a delay of 1 cycle every N weights for systolic chaining of DSPs
		delay_for_chain_break=4,        # Delay added whenever a DSP chain is broken.  The default is the number of input pipeline stages for a DSP plus the output DSP delay
		staggered_weight_info=None,
		group_x_dim=False,
		is_depthwise=False):            # Depthwise is a little different since each output channel has only one input channel
		assert(staggered_weight_info is not None)
		weight_order = {"H" : 0, "W" : 1, "I" : 2, "O" : 3}
		
		if is_depthwise:
			weights = np.reshape(weights, [weights.shape[0], weights.shape[1], 1, -1])
			assert(weights.shape[-1] != 1)
			
		def get_oc_weight_list(w):
			"""
			 Gets a tuple of:
			  1. List of lists of weight values for each output channel
			  2. The corresponding number of input channels and y positions that should be skipped 
			      to decode the position of the weight in the input group
			  3. The corresponding x positins for each weight
			 Args:
				w: a 4 dimensional tensor.  This tensor should be a group of weights that should
				   be computed serially.  The number of parallel weights can be increased by
				   dividing the input channels, so this should be a group of weights corresponding
				   to a group of input channels
			"""
			nonlocal group_x_dim
			ws = []
			rls = []
			x_sels = []
			if group_x_dim:
				x_range = w.shape[-3]
			else:
				x_range = 1
			for oc in range(w.shape[-1]):
				for x in range(x_range):
					if group_x_dim:
						_w = np.transpose(w[:,x:x+1,:,oc], [0,2,1])
					else:
						_w = np.transpose(w[:,:,:,oc], [0,2,1])
					nz = np.nonzero(_w)
					# need at least one weight in each output channel
					if nz[0].size == 0:
						nz = (*[np.array([0]) for _ in _w.shape],)
					x_sels.append(nz[-1])
					nz_w = _w[nz]
					ws.append(nz_w)
					indices = w.shape[-2] * nz[0] + nz[1]
					_rls = indices - np.pad(indices[:-1], [1,0], mode="constant", constant_values=0)
					rls.append(_rls)
					assert(_rls.size == nz_w.size)
			return (ws, rls, x_sels)

		# The following lists are lists of lists of serial weights, runlengths, and x weight indices.
		#  The outer list contains lists for each input channel group
		#  The list within that contains lists for each output channel
		ws = []
		rls = []
		x_act_indices = []
		lists = [ws, rls, x_act_indices]
		bits = []
		for j in range(parallel_weights):
			w = weights[:,:,j::parallel_weights,:]
			w_bits = math.floor(math.log2(max(np.max(np.abs(w)),1))) + 2
			#Have to check if fewer bits can be used for a negative number
			if np.max(np.abs(w)) == abs(np.min(w)) and np.max(np.abs(w)) != np.max(w) and np.min(w) < 0:
				w_bits_neg = math.floor(math.log2(np.max(np.abs(w))-1)) + 2
				if w_bits_neg < w_bits:
					w_bits = w_bits_neg
			w_s, rl_s, x_sels = get_oc_weight_list(w)
			rl_bits = clog2(1+np.max(np.array([i for l in rl_s for i in l])))
			#Can't allow runlength bits to be 0
			if rl_bits == 0:
				rl_bits = 1
			x_sels_bits = clog2(1+np.max(np.array([i for l in x_sels for i in l])))
			bits.extend([w_bits, rl_bits, x_sels_bits])
			ws.append(w_s)
			rls.append(rl_s)
			x_act_indices.append(x_sels)


		# pad each of the input channel groups to be the same length
		for i in range(len(ws[0])):
			counts = [w_group_list[i].size for w_group_list in ws]
			max_count = max(counts)
			for j in range(len(ws)):
				current_group_length = ws[j][i].size
				if current_group_length < max_count:
					zp_count = max_count - current_group_length
					args = {"pad_width" : [0, zp_count], "mode" : "constant", "constant_values" : 0}
					for l in lists:
						l[j][i] = np.pad(l[j][i], **args)
			for j in range(1,len(ws)):
				assert(ws[j][i].size == ws[j-1][i].size)

		# Now we need to get the groups of acts, rls, and indices in the order that we want them
		#  to be concatenated into the memory
		# We want a repetition of weights[0], rls[0], indices[0], weights[1], rls[1], indices[1], ...
		#  so we will iterate over the group index in the outer loop and then iterate over the list
		#  type in the inner loop, appending to an array
		output_channels = len(ws[0])
		input_channel_groups = len(ws)
		num_element_types = len(lists)
		combined = [[] for _ in range(num_element_types * input_channel_groups)]
		for i in range(output_channels):
			concatenate_elements = []
			for j in range(input_channel_groups):
				# now go through every type of element (weight values, rl values, x indices)
				for k,l in enumerate(lists):
					combined[k + j * num_element_types].extend(list(l[j][i]))


		# Now we will optionally stagger every n lists
		max_stagger = 0
		if stagger_every_n_weights > 0:
			combined_index = 0
			max_stagger = max(staggered_weight_info["staggers"])
			for i,stagger in enumerate(staggered_weight_info["staggers"]):
				for j in range(num_element_types):
					combined[combined_index] = [0] * stagger + combined[combined_index] + [0] * (max_stagger - stagger)
					combined_index += 1

		numpy_combined = combined
		bit_arrays = [BitArray(bits=b, value_vector=ns) for b, ns in zip(bits, combined)]
		combined = bit_arrays[0]
		for ba in bit_arrays[1:]:
			combined += ba

		weights_per_oc = [len(l) for l in ws[0]]
		weights_per_oc_bits = max(1, clog2(np.max(weights_per_oc)))
		return (weights_per_oc, weights_per_oc_bits, combined, bits, numpy_combined)

	def _get_serialized_weights_and_rls(self,
		parallel_weights=2,             # Number of multiplications per DSP
		stagger_every_n_weights=0,      # Adds a delay of 1 cycle every N weights for systolic chaining of DSPs
		delay_for_chain_break=4,        # Delay added whenever a DSP chain is broken.  The default is the number of input pipeline stages for a DSP plus the output DSP delay
		staggered_weight_info=None,
		group_x_dim=False,
		is_depthwise=False):            # Depthwise is a little different since each output channel has only one input channel
		weights = self.node.values[0]
		return BasicConv.get_serialized_weights_and_rls(weights, parallel_weights, stagger_every_n_weights, delay_for_chain_break, staggered_weight_info, group_x_dim, is_depthwise)

	def get_custom_verilog_str(self, t="  "):
		verilog = super(BasicConv, self).get_custom_verilog_str(t)
		'''
		verilog += "  always @(posedge clock_0) begin\n"
		verilog += "    if (" + self.debug_stall.name + ") begin\n"
		verilog += '      $display("' + self.name + ' deadlocked because there is no space and it does not have enough lines to write one out %0t", $time);\n'
		verilog += "    end\n"
		#verilog += "    if (!" + self.can_write_line.name + ") begin\n"
		#verilog += '      $display("' + self.name + ' cannot write");\n'
		#verilog += "    end\n"
		verilog += "  end\n"'''
		return verilog

# Handles conversions from fixed point to floating point
# Note that this does not follow the IEEE spec except for fp32. It assumes the exponent is always 8-bits, the sign 1-bit and the mantissa is the remaining bits
# In addition. NaN is not handled, however 0 is. The exponent is assumed to have an offset of 127 like in IEEE, and the mantissa an implicit 1
# Input and output are registered, so it will take 2 cycles to get the result
class FixedToFloatingPointConversion(dl.Module):

	def __init__(self, input_width, fxp_bit_spec, fp_num_bits=16, output_seperate_mantissa_and_exp=True, **kwargs):
		super(FixedToFloatingPointConversion, self).__init__("fxp_to_fp_converter", kind="fxp_to_fp_converter", **kwargs)
		inst = self.inst
		with dl.ModuleScope(self):

				self.delay = 2
				FP_IEEE_EXP_OFFSET = 127
				FP32_EXP_BITS = 8
				FP_MANTISSA_BITS = fp_num_bits-FP32_EXP_BITS-1
				self.inputs = [inst(dl.Logic, bits=fxp_bit_spec["width"], name=("fixed_point_input_"+str(i))) for i in range(input_width)]
				self.signs_and_mantissas = [inst(dl.Flop, bits=(fp_num_bits-FP32_EXP_BITS), name=("floating_point_output_mantissa_"+str(i))) for i in range(input_width)]
				self.exponents = [inst(dl.Flop, bits=FP32_EXP_BITS, name=("floating_point_output_exponent_"+str(i))) for i in range(input_width)]
				conv_const_1 = inst(dl.Constant, bits=(fxp_bit_spec["width"]-1), value=1, name="conv_const_1")

				last_bit = (fxp_bit_spec["width"]-1)

				#Step 1: convert from 2s complimant to sign and magnitude and store in register
				signs = []
				maginitudes = []
				zero_magnitude = inst(dl.Constant, bits=(fxp_bit_spec["width"]-1), value=0, name="zero_magnitude")
				for i in range(input_width):
					signs.append(inst(dl.Flop, bits=1, name=("sign_"+str(i))))
					signs[-1].set_driver(self.inputs[i][last_bit])

					maginitudes.append(inst(dl.Flop, bits=(fxp_bit_spec["width"]-1), name=("magnitude_"+str(i))))
					inverted_number = inst(dl.ADD, inst(dl.INV, self.inputs[i][(last_bit-1):0], reduction_output_bits=last_bit), conv_const_1)[(last_bit-1):0]
					maginitudes[-1].set_driver(inst(dl.MuxV2, self.inputs[i][last_bit], self.inputs[i][(last_bit-1):0], inverted_number))

			
				# Create all possible exponents for the given number (will be based on number of bits)
				current_exponent = fxp_bit_spec["int"]
				exp_constants = []
				for j in range(fxp_bit_spec["width"]):
					const_string = str(abs(current_exponent))
					if current_exponent < 0:
						const_string = "neg_" + const_string
					exp_constants.append(inst(dl.Constant, bits=FP32_EXP_BITS, value=(FP_IEEE_EXP_OFFSET+current_exponent), name=("const_exp_" + const_string)))
					current_exponent -= 1

				#Special case for zero exponent
				exp_constants.append(inst(dl.Constant, bits=FP32_EXP_BITS, value=0, name="const_exp_0"))



				#Create any constants we need to concatinate in the mantissa
				const_zero_mantissa = inst(dl.Constant, bits=FP_MANTISSA_BITS, value=0, name="const_mantissa_0")
				zero_padded_constants = []
				for j in range(FP_MANTISSA_BITS):
					zero_padded_constants.append(inst(dl.Constant, bits=j, value=0, name=("const_mantissa_0_"+str(j)+"_bits")))


				for i in range(input_width):
					#Corner case for when the input is the max negative number
					#In this case the exponent needs to go one higher than the maximum positive exponent
					max_neg_num = inst(dl.AND, signs[i], inst(dl.EQ, maginitudes[i], zero_magnitude))

					# Step 2a: determine exponent based on position of leading 1
					# This is done using a casez statement that detects the leading 1 (it uses wildcards for some values)
					cur_leading_one_mux = inst(dl.LeadingOneMux, inst(dl.Concat, maginitudes[i], max_neg_num), *exp_constants, bits=(fxp_bit_spec["width"]))
					self.exponents[i].set_driver(cur_leading_one_mux)

					# Step 2b: extract mantissa based on position of leading 1
					#Create list of possible mantissas
					possible_mantissas = []
					for j in range(fxp_bit_spec["width"]-1, 0, -1):
						ending_bit = j-1-1 #implicit one means we don't include one of the bits
						starting_bit = j - FP_MANTISSA_BITS - 1
						cur_possible_mantissa = inst(dl.Logic, bits=FP_MANTISSA_BITS, name=("mantissa_sel_" + str(i) + "_bit_" + str(ending_bit+1)))
						
						#Select the correct mantissa bits and pad in zeros if needed
						if starting_bit < 0:
							if ending_bit < 0: 
								cur_possible_mantissa.set_driver(const_zero_mantissa)
							else:
								num_const_bits_needed = FP_MANTISSA_BITS - ending_bit - 1 
								cur_possible_mantissa.set_driver(inst(dl.Concat, zero_padded_constants[num_const_bits_needed], maginitudes[i][ending_bit:0]))
						else:
							cur_possible_mantissa.set_driver(maginitudes[i][ending_bit:starting_bit]) 
									
						possible_mantissas.append(cur_possible_mantissa)

					#Special case for zero
					possible_mantissas.append(const_zero_mantissa)

					#Next, create a leading one mux so that we can select the correct mantissa bits
					cur_leading_one_mantissa_mux = inst(dl.LeadingOneMux, maginitudes[i], *possible_mantissas, bits=(fxp_bit_spec["width"]-1))
					self.signs_and_mantissas[i].set_driver(inst(dl.Concat, cur_leading_one_mantissa_mux, signs[i]))


				#Can access the full outputs as well (we don't in the TensorConv module)
				if not output_seperate_mantissa_and_exp:
					self.full_outputs = [inst(dl.Logic, bits=fp_num_bits, name=("floating_point_output_"+str(i))) for i in range(input_width)]
					for i in range(input_width):
						self.full_outputs[i].set_driver(inst(dl.Concat, self.signs_and_mantissas[i][FP_MANTISSA_BITS-1:0], self.exponents[i], self.signs_and_mantissas[i][FP_MANTISSA_BITS]))

# Converts floating point to block floating point for the NX tensor block
# Note that is uses an NX specific format where after determining the shared exponent and shifting as needed we do the following to the mantissa:
# 1. Re-introduce the implicit 1
# 2. Integrate sign using 2s-complement
# 3. Crop down to 8 bits (i.e. remove LSB since we have an extra bit from implicit one)
# We calso need to add an extra bias of -6 to the shared exponent
class FloatingPointToBfloat16(dl.Module):
	def __init__(self, fp_num_bits=16, input_is_bus=True, **kwargs):
		super(FloatingPointToBfloat16, self).__init__("fp_to_bfloat_converter", kind="fp_to_bfloat_converter", **kwargs)
		inst = self.inst
		with dl.ModuleScope(self):

			MANTISSA_BITS = fp_num_bits - dl.NX_SHARED_EXP_SIZE - 1

			# There is a false circular logic warning in this module
			self.disable_verilator_UNOPTFLAT = True

			# Declare inputs; use the first if input_is_bus is true, otherwise use the second
			self.input_mantissa_bus = inst(dl.Logic, bits=dl.NX_ICP_PER_TENSOR_BLOCK*dl.NX_INPUT_DATA_BITS, name="input_mantissa_bus")
			self.input_exp_bus = inst(dl.Logic, bits=dl.NX_ICP_PER_TENSOR_BLOCK*dl.NX_SHARED_EXP_SIZE, name="input_exp_bus")

			self.input_mantissas = [inst(dl.Logic, bits=dl.NX_INPUT_DATA_BITS, name=("input_mantissa_"+str(i))) for i in range(dl.NX_ICP_PER_TENSOR_BLOCK)]
			self.input_exps = [inst(dl.Logic, bits=dl.NX_SHARED_EXP_SIZE, name=("input_exp_"+str(i))) for i in range(dl.NX_ICP_PER_TENSOR_BLOCK)]

			#declare outputs for later
			self.output_shared_exp = inst(dl.Flop, bits=dl.NX_SHARED_EXP_SIZE, name="output_shared_exp")
			if input_is_bus:
				self.output_mantissas = [inst(dl.Logic, bits=dl.NX_INPUT_DATA_BITS, name=("output_mantissa_"+str(i))) for i in range(dl.NX_ICP_PER_TENSOR_BLOCK)]
			else:
				self.output_mantissas = [inst(dl.Flop, bits=dl.NX_INPUT_DATA_BITS, name=("output_mantissa_"+str(i))) for i in range(dl.NX_ICP_PER_TENSOR_BLOCK)]

			if input_is_bus:
				for i in range(dl.NX_ICP_PER_TENSOR_BLOCK):
					self.input_mantissas[i].set_driver(self.input_mantissa_bus[(i*dl.NX_INPUT_DATA_BITS + dl.NX_INPUT_DATA_BITS - 1):(i*dl.NX_INPUT_DATA_BITS)])
					self.input_exps[i].set_driver(self.input_exp_bus[(i*dl.NX_SHARED_EXP_SIZE + dl.NX_SHARED_EXP_SIZE - 1):(i*dl.NX_SHARED_EXP_SIZE)])

			# First find the shared exponent
			# This is done by creating a comparison tree
			num_wires_needed = math.ceil(dl.NX_ICP_PER_TENSOR_BLOCK/2.0)
			previous_level_wires = []
			current_level_wires = []
			for tree_level in range(clog2(dl.NX_ICP_PER_TENSOR_BLOCK)):

				# This loops generates the greater than muxing we need for comparing values
				# It uses previous_level_wires and current_level_wires to save the values we need to look-up in the next iteration
				for i in range(num_wires_needed):
					if tree_level == 0:
						if i*2+1 >= len(self.input_exps):
							# If there is an odd number of wires, just pass forward the value
							greater_than_mux = self.input_exps[i*2]
						else: 
							greater_than_comparison = inst(dl.GT, self.input_exps[i*2], self.input_exps[i*2+1])
							greater_than_mux = inst(dl.MuxV2, greater_than_comparison, self.input_exps[i*2+1], self.input_exps[i*2], name=("exp_greater_than_tree_level_"+str(tree_level)+"_mux"))
					else:
						if i*2+1 >= len(previous_level_wires):
							greater_than_mux = previous_level_wires[i*2]
						else:
							greater_than_comparison = inst(dl.GT, previous_level_wires[i*2], previous_level_wires[i*2+1])
							greater_than_mux = inst(dl.MuxV2, greater_than_comparison, previous_level_wires[i*2+1], previous_level_wires[i*2], name=("exp_greater_than_tree_level_"+str(tree_level)+"_mux"))


					current_level_wires.append(greater_than_mux)

				previous_level_wires = current_level_wires.copy()
				current_level_wires.clear()

				num_wires_needed = math.ceil(num_wires_needed/2.0)

			# This is set to the last saved wire from that loop iteration
			greatest_exponent = inst(dl.Flop, bits=dl.NX_SHARED_EXP_SIZE, name="greatest_exponent")
			greatest_exponent.set_driver(previous_level_wires[0])

			#Create registers for mantissas and original exponents for next stage
			input_mantissa_registers = [inst(dl.Flop, bits=dl.NX_INPUT_DATA_BITS, name=("input_mantissa_reg_"+str(i))) for i in range(dl.NX_ICP_PER_TENSOR_BLOCK)]
			input_exp_registers = [inst(dl.Flop, bits=dl.NX_SHARED_EXP_SIZE, name=("input_exp_reg_"+str(i))) for i in range(dl.NX_ICP_PER_TENSOR_BLOCK)]

			implicit_one = inst(dl.Constant, bits=1, value=1, name="implicit_1")
			const_1 = inst(dl.Constant, bits=(MANTISSA_BITS-1), value=1, name="const_inv_1")

			# Now that we know the greatest exponent, we need to shift all the mantissas			
			for i in range(dl.NX_ICP_PER_TENSOR_BLOCK):
				input_mantissa_registers[i].set_driver(self.input_mantissas[i])
				input_exp_registers[i].set_driver(self.input_exps[i])

				# Add in implicit 1 first, otherwise shifting makes no sense
				shift_amount = inst(dl.SUB, greatest_exponent, input_exp_registers[i], name=("amount_to_shift_mantissa_"+str(i)))
				mantissa_with_implicit_one = inst(dl.Concat, input_mantissa_registers[i][MANTISSA_BITS-1:0], implicit_one) # Need to make sure we don't include the sign here
				shifted_mantissa = inst(dl.RightShift, mantissa_with_implicit_one, shift_amount, name=("shifted_mantissa_"+str(i)))

				# Now convert to 2s complement if needed using sign
				inverted_mantissa = inst(dl.ADD, inst(dl.INV, shifted_mantissa[shifted_mantissa._bit_width-2:0], reduction_output_bits=(MANTISSA_BITS+1)), const_1)[(MANTISSA_BITS):0]
				inverted_mantissa_select = inst(dl.MuxV2, input_mantissa_registers[i][MANTISSA_BITS], shifted_mantissa, inverted_mantissa, name=("inverted_mantissa_select_"+str(i)))
				#Remove lowest bit from final mantissa and add in sign
				final_mantissa = inst(dl.Concat, inverted_mantissa_select[(MANTISSA_BITS):1], input_mantissa_registers[i][MANTISSA_BITS])
				self.output_mantissas[i].set_driver(final_mantissa)

			# Add in the extra S10_NX exponent bias
			# Need to check for underflow in this case, especially if we have an exponent of 0 (which will happen way more often than 1-5 as they are really small and won't fit in fixed-point)
			six_exp = inst(dl.Constant, bits=dl.NX_SHARED_EXP_SIZE, value=6, name="const_6_exp")
			zero_exp = inst(dl.Constant, bits=dl.NX_SHARED_EXP_SIZE, value=0, name="const_0_exp")
			biased_exp = inst(dl.SUB, greatest_exponent, six_exp)[dl.NX_SHARED_EXP_SIZE-1:0]
			sel_output_exp = inst(dl.LT, greatest_exponent, six_exp, "underflow_detected")
			self.output_shared_exp.set_driver(inst(dl.MuxV2, sel_output_exp, biased_exp, zero_exp))

			if input_is_bus:
				self.output_mantissa_bus = inst(dl.Flop, bits=dl.NX_ICP_PER_TENSOR_BLOCK*dl.NX_INPUT_DATA_BITS, name="output_mantissa_bus")
				self.output_mantissa_bus.set_driver(inst(dl.Concat, *self.output_mantissas))

# Converts floating point to fixed point, takes 2 cycles
# Assumes there is a 1 bit sign, 8 bit exponent, and the remaining bits are the mantissa
class FpToFxpConverter(dl.Module):
	def __init__(self, output_width, fxp_bit_spec, fp_num_bits=24, **kwargs):
		super(FpToFxpConverter, self).__init__("fp_to_fxp_conversion", kind="fp_to_fxp_conversion", **kwargs)
		inst = self.inst
		with dl.ModuleScope(self):

			IEEE_EXPONENT_OFFSET = 127

			# There is a false circular logic warning in this module
			self.disable_verilator_UNOPTFLAT = True

			self.inputs = [inst(dl.Flop, bits=fp_num_bits, name=("floating_point_input_"+str(i))) for i in range(output_width)]
			self.outputs = [inst(dl.Flop, bits=fxp_bit_spec["width"], name=("fixed_point_output_"+str(i))) for i in range(output_width)]

			frac_rounding_bit = [inst(dl.Flop, bits=1, name=("frac_rounding_bit_"+str(i))) for i in range(output_width)]
			saturate = [inst(dl.Flop, bits=1, name=("saturate_"+str(i))) for i in range(output_width)]
			self.rounded_outputs = [inst(dl.Flop, bits=fxp_bit_spec["width"], name=("fixed_point_output_rounded_"+str(i))) for i in range(output_width)]	

			#Determine correct offset for shifting
			EXP_OFFSET = IEEE_EXPONENT_OFFSET + (fxp_bit_spec["int"] - 1)
			exp_offset_const = inst(dl.Constant, bits=dl.NX_SHARED_EXP_SIZE, value=EXP_OFFSET, name="exp_offset_const")

			fp_mantissa_bits = fp_num_bits-8-1
			overflow_const = inst(dl.Constant, bits=(fp_mantissa_bits+1), value=(2**(fp_mantissa_bits+1) - 1), name="overflow_const")

			implicit_one = inst(dl.Constant, bits=1, value=1, name="implicit_one")
			const_0_1bit = inst(dl.Constant, bits=1, value=0, name="const_0_1bit")
			const_1 = inst(dl.Constant, bits=(fp_mantissa_bits+1), value=1, name="const_1")
			const_out_1 = inst(dl.Constant, bits=fxp_bit_spec["width"], value=1, name="const_out_1")
			const_0 = inst(dl.Constant, bits=(fp_mantissa_bits+1), value=0, name="const_0")

			for i in range(output_width):
				# Calculate amount to shift based on exponent
				cur_exp = self.inputs[i][fp_num_bits-2:fp_num_bits-9]
							
				amount_to_right_shift = inst(dl.SUB, exp_offset_const, cur_exp, name=("amount_to_shift_"+str(i)))

				#Add back in implicit 1
				mantissa = inst(dl.Concat, self.inputs[i][fp_num_bits-10:0], implicit_one)

				#Shift according to exponent. If the new exponent is negative, we need to saturate the fixed-point value as this means the exponent is very large
				shifted_mantissa = inst(dl.MuxV2, amount_to_right_shift[dl.NX_SHARED_EXP_SIZE], inst(dl.RightShift, mantissa, amount_to_right_shift, name=("shifted_mantissa"+str(i))), overflow_const, name=("shifted_mantissa_sel"+str(i)))
				saturate[i].set_driver(amount_to_right_shift[dl.NX_SHARED_EXP_SIZE])

				#Convert to 2s complement
				sign_bit = inst(dl.MuxV2, inst(dl.EQ, shifted_mantissa, const_0), self.inputs[i][fp_num_bits-1], const_0_1bit) #Corner case for where our mantissa is 0 (then our included sign bit should also be 0)
				inverted_number = inst(dl.ADD, inst(dl.INV, shifted_mantissa, reduction_output_bits=(fp_mantissa_bits+1)), const_1)[fp_mantissa_bits:0]
				twos_complement = inst(dl.Concat, inst(dl.MuxV2, sign_bit, shifted_mantissa, inverted_number), sign_bit, name="twos_complement")

				self.outputs[i].set_driver(twos_complement[twos_complement._bit_width-1:twos_complement._bit_width-fxp_bit_spec["width"]])
				if twos_complement._bit_width-fxp_bit_spec["width"]-1 >= 0:
					frac_rounding_bit[i].set_driver(twos_complement[twos_complement._bit_width-fxp_bit_spec["width"]-1])

					rounded_output = inst(dl.MuxV2, self.outputs[i][fxp_bit_spec["width"]-1], 
						inst(dl.MuxV2, frac_rounding_bit[i], self.outputs[i], inst(dl.ADD, self.outputs[i], const_out_1), name=("rounded_output_positive_"+str(i))),
						inst(dl.MuxV2, frac_rounding_bit[i], inst(dl.SUB, self.outputs[i], const_out_1), self.outputs[i], name=("rounded_output_negative_"+str(i))), name=("rounded_output_sel_sign_"+str(i)))
					self.rounded_outputs[i].set_driver(inst(dl.MuxV2, saturate[i], rounded_output[fxp_bit_spec["width"]-1:0], self.outputs[i]))

				else:
					self.rounded_outputs[i].set_driver(self.outputs[i])


			self.requires_driver = self.inputs


class TensorActivationBuffer(dl.Module):
	'''
	Stores activations from previous nerual network layer and reorganizes them for the tensor block to use (need to be in vectors of size 10)
	Note that we only currently support cascade preloading

	The memory is optimized by packing in fixe 8-bit values per line. These values are activations across the input width
	When loading the values, they will be sent to different tensor block chains (3 per chain)
	Extra muxing is needed if the filter is larger than 1x1, as we need to choose from multiple columns along the input width
	The preloading logic consists of an FSM and a bunch of counters
	These counters handle things like padding and the number of input rows we need to store in the memory, which depends on the filter size

	Note that for cascade mode, M20Ks may be instantiated as each memory will need to hold a lot of input channels. For side-input mode, MLABs would be instantiated as we need more of them for high ICPs
	'''

	MEM_READ_WIDTH = 40 #MLAB read width is 20 bits, so we use 2 MLABs for 40 bits so that our 8-bit data size divides evenly (we will have 5 values per memory instead of 2.5). M20K supports 40-bits for 1 memory
	VALS_PER_MEMORY = int(MEM_READ_WIDTH/dl.NX_INPUT_DATA_BITS)
	MIN_LINES_PER_CHANNEL = 2

	def __init__(self, input_channels, input_channel_parallelism, kernel_width, kernel_height, padding, stride, input_width, input_height, output_width, output_height, CONTROL_TO_DATA_DELAY, is_depthwise=False, cascade_loading=True, **kwargs):
		super(TensorActivationBuffer, self).__init__("tensor_act_buffer", kind="tensor_act_buffer", **kwargs)
		inst = self.inst
		with dl.ModuleScope(self):
			#min_lines_per_kernel = kernel_height*self.MIN_LINES_PER_CHANNEL
			min_lines_per_kernel = max(kernel_height, stride) + stride
			assert(self.MEM_READ_WIDTH % dl.NX_INPUT_DATA_BITS == 0) #The code written doesn't support having MEM_READ_WIDTH not divisible by the data bits

			global enable_bfp
			#extra RAM output reg enable
			EXTRA_OUTPUT_REG = dl.ENABLE_RAM_OUTPUT_REG_NX

			num_tensors_in_chain = math.ceil(input_channel_parallelism/float(dl.NX_ICP_PER_TENSOR_BLOCK))
			num_chains = math.ceil(output_width/float(dl.NX_OUT_WIDTH_PARALLELISM_PER_TENSOR_BLOCK))

			#calculate multipliers used for final estimate
			dl.modify_final_mult_count(output_width * min(input_channel_parallelism,input_channels))

			self.ack_next_ping_pong = inst(dl.Logic, bits=1, name="ack_next_ping_pong")
			#The difference between these two is that the first one is for when we finish loading one instance of the tensor block
			#The second one is for when we're done reading the entire line (i.e. all channels) which could require us pre-loading multiple times
			self.read_counter_done = inst(dl.Logic, bits=1, name="read_counter_done")
			self.read_counter_completely_done = inst(dl.Logic, bits=1, name="read_counter_completely_done")

			self.write_en = inst(dl.Logic, bits=1, name="tensor_act_buff_write_en")
			if enable_bfp:
				self.write_en_no_delay = inst(dl.Logic, bits=1, name="tensor_act_buff_write_en_no_delay")

			self.output_lines_counter = inst(dl.Counter,reset_value=0, increment_value=1, end_value=(output_height-1), name="output_lines_counter")

			self.space_to_write_line = inst(dl.Logic, bits=1, name="space_to_write_line")
			self.cred_dec = inst(dl.Logic, bits=1, name="cred_dec")
			self.cred_dec_final = inst(dl.Logic, bits=1, name="cred_dec_final")
			self.cred_dec_amount = -stride

			#Calculate extra padding needed for stride (e.g. with a stride of 2 and a filter height of 1, we may need an extra row at the output of padding)
			left_padding = padding[0]
			upper_and_lower_padding = padding[1]	
			extra_padding_needed = upper_and_lower_padding[1]
			final_rows_used = kernel_height - extra_padding_needed if kernel_height > stride else stride # opposite of above, needed to subtract appropriate number of lines from lines used below
			in_upper_padding = inst(dl.Logic, bits=1, name="in_upper_padding")
			#This keeps track of how much room we have in the input buffer. Note that a line in this case is not a activation RAM line, it is a full input line (i.e. input_width*input_channels)
			#lines_used_counter = inst(dl.MultiChangeCounter, reset_value=0, change_values=[1,-stride,-final_rows_used], increment_value=1,decrement_value=-stride, end_value=min_lines_per_kernel, name="lines_used_counter")

			#Regular decrement condition
			if (upper_and_lower_padding[0] - stride) >= 0:
				# Handle upper padding if needed
				if (upper_and_lower_padding[0] % stride == 0):
					lines_used_counter = inst(dl.MultiChangeCounter, reset_value=0, change_values=[1,-stride,-final_rows_used], increment_value=1,decrement_value=-stride, end_value=min_lines_per_kernel, name="lines_used_counter")
					lines_used_counter.change_signals[1].set_driver(inst(dl.AND, self.read_counter_completely_done, inst(dl.NOT, self.output_lines_counter.is_done), inst(dl.NOT, in_upper_padding)))
				else:
					#In this case, the first time we start decrementing, we don't remove the standard number of lines
					#i.e. If we have 3 lines of padding with a stride of 2, then the first 2 times we decrement 0 lines, then we decrement 1 line, then we dec 2 every time
					lines_used_counter = inst(dl.MultiChangeCounter, reset_value=0, change_values=[1,-1,-final_rows_used], increment_value=1,decrement_value=-1, end_value=min_lines_per_kernel, name="lines_used_counter")
					self.cred_dec_amount = -1
					
					mild_decrement = stride - (upper_and_lower_padding[0] % stride)
					mild_dec_signal = inst(dl.AND, self.read_counter_completely_done, inst(dl.NOT, self.output_lines_counter.is_done), inst(dl.EQ, self.output_lines_counter.current_value, inst(dl.Constant, bits=self.output_lines_counter.current_value._bit_width, value=math.ceil(upper_and_lower_padding[0]/stride))))
					normal_dec_signal = inst(dl.AND, self.read_counter_completely_done, inst(dl.NOT, self.output_lines_counter.is_done), inst(dl.NOT, in_upper_padding))
					lines_used_counter_decrement_proxy = inst(dl.MultiChangeCounter, reset_value=0, change_values=[-1,stride,mild_decrement], end_value=2*(stride+mild_decrement), name="lines_used_counter_decrement_proxy")
					lines_used_counter_decrement_proxy.change_signals[1].set_driver(normal_dec_signal)
					lines_used_counter_decrement_proxy.change_signals[2].set_driver(mild_dec_signal)
					lines_used_counter_decrement_proxy.change_signals[0].set_driver(inst(dl.NOT, lines_used_counter_decrement_proxy.is_reset_value))
					lines_used_counter.change_signals[1].set_driver(inst(dl.NOT, lines_used_counter_decrement_proxy.is_reset_value).delay(1))

			else:
				lines_used_counter = inst(dl.MultiChangeCounter, reset_value=0, change_values=[1,-stride,-final_rows_used], increment_value=1,decrement_value=-stride, end_value=min_lines_per_kernel, name="lines_used_counter")
				lines_used_counter.change_signals[1].set_driver(inst(dl.AND, self.read_counter_completely_done, inst(dl.NOT, self.output_lines_counter.is_done)))
			
			self.output_lines_counter.set_increment_condition(self.read_counter_completely_done)

			#decrement condition at the end of an image. We need to "capture" the output_lines_counter.done signal to make sure we decrement the correct amount and don't double count
			lines_used_counter.change_signals[2].set_driver(inst(dl.AND, self.read_counter_completely_done, self.output_lines_counter.is_done))

			self.cred_dec.set_driver(lines_used_counter.change_signals[1])
			self.cred_dec_final.set_driver(lines_used_counter.change_signals[2])
			self.cred_dec_final_amount = -final_rows_used
			self.cred_max = min_lines_per_kernel

			self.no_freeze = inst(dl.Logic, bits=1, name="no_freeze") #This is sometimes zero when HBM is freezing the pipeline to catch up. It's 1 when no slowdown is happening


			if cascade_loading:
				total_loading_cycles = 3+3*num_tensors_in_chain

				#For cascade loading the input channel parallelism doesn't matter, we will always load 10 values per chain
				input_channels_per_mem = math.ceil(input_channels/dl.NX_ICP_PER_TENSOR_BLOCK)
				cur_memory_depth = input_channels_per_mem * min_lines_per_kernel

				#Create signals for ROMs
				if enable_bfp:
					WRITE_DELAY = 1 #Too many registers if we do this
				else:
					WRITE_DELAY = 1 #Delay for write signals

				
				READ_ADDR_DELAY = 1 #delay for read addr signals 
				READ_ADDR_TO_READ_DELAY = None #Should be set once a ram is instantiated
				READ_VAL_DELAY = 1 #delay for read values 
				self.READ_VAL_DELAY = READ_VAL_DELAY
				if enable_bfp:
					self.fp_exponent_inputs = [inst(dl.Logic, bits=dl.NX_SHARED_EXP_SIZE, name=("tensor_act_buff_fp_exponent_input_"+str(i))) for i in range(input_width)]
					assert(dl.NX_SHARED_EXP_SIZE == dl.NX_INPUT_DATA_BITS)
				self.inputs = [inst(dl.Logic, bits=dl.NX_INPUT_DATA_BITS, name=("tensor_act_buff_input_"+str(i))) for i in range(input_width)]

				constant_0 = inst(dl.Constant, bits=dl.NX_INPUT_DATA_BITS, value=0, name=("constant_0"))


				#Create a counter to keep track of which input channel we're on; it's based on the default NX ICP (10). If timing is bad this may have to be duplicated 
				channel_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=(dl.NX_ICP_PER_TENSOR_BLOCK-1), name="tensor_block_icp_counter")
				channel_counter.set_increment_condition(self.write_en)

				#This counts all the channels in the layer; its used for space to write line
				full_channel_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=(input_channels-1), name="input_channel_counter")
				full_channel_counter.set_increment_condition(self.write_en)
				#Create a seperate signal for this to digitallogic duplicating it a bunch of times
				full_channel_counter_is_done = inst(dl.Logic, bits=1, name="input_channel_counter_done")
				full_channel_counter_is_done.set_driver(full_channel_counter.is_done)

				if enable_bfp and input_channels <= 3:
					# Corner case for bfp where we have an extra delay on the input data. Sometimes the space_to_write_line arrives too late to stop the next line, causing an overflow in the buffer
					lines_used_counter_almost_full_and_write = inst(dl.AND, inst(dl.EQ, lines_used_counter.current_value, inst(dl.Constant, bits=clog2(min_lines_per_kernel+1), value=(min_lines_per_kernel-1))), 
						inst(dl.OR, self.write_en_no_delay, inst(dl.GT, full_channel_counter.current_value, inst(dl.Constant, bits=clog2(input_channels), value=0))), name="lines_used_counter_almost_full_and_write")
				else:
					lines_used_counter_almost_full_and_write = inst(dl.AND, inst(dl.EQ, lines_used_counter.current_value, inst(dl.Constant, bits=clog2(min_lines_per_kernel+1), value=(min_lines_per_kernel-1))), 
						inst(dl.OR, self.write_en, inst(dl.GT, full_channel_counter.current_value, inst(dl.Constant, bits=clog2(input_channels), value=0))), name="lines_used_counter_almost_full_and_write")

				self.space_to_write_line.set_driver(inst(dl.NOT, inst(dl.OR, lines_used_counter_almost_full_and_write, lines_used_counter.is_done)).delay(1))				

				increment_lines_used = inst(dl.AND, self.write_en, inst(dl.EQ, full_channel_counter.current_value, inst(dl.Constant, bits=clog2(input_channels), value=max(input_channels-1, 0)), name="full_channel_counter_almost_done"), name="increment_lines_used")

				#add overflow assertion
				act_buff_overflow = inst(dl.Logic, bits=1, name="act_buff_overflow")
				act_buff_overflow.set_driver(inst(dl.AND, lines_used_counter.is_done, increment_lines_used).delay(1))
				m = self
				def full_condition(m, t="  ", fifo_condition=act_buff_overflow):
					condition = "(" + fifo_condition.name + " !== 1)"
					return condition
					
				inst(dl.Assertion,
					name="act_buff_overflow_check",
					condition_f=full_condition,
					condition_f_args=m,
					comment=act_buff_overflow.name + " is high! This means the TensorActivationBuffer is full and being written to! ",
					error_string=act_buff_overflow.name + " is high! This means the TensorActivationBuffer is full and being written to! ")

				#Note: for writes we may have to stop early if IC is not divisible by 10
				ic_remainder = input_channels % 10 
				if ic_remainder != 0:
					channel_counter.update_reset_condition(inst(dl.AND, full_channel_counter_is_done, self.write_en))

				channel_count_consts = [inst(dl.Constant, bits=clog2(dl.NX_ICP_PER_TENSOR_BLOCK), value=i, name=("channel_count_const_" + str(i))) for i in range(dl.NX_ICP_PER_TENSOR_BLOCK)]

				
				lines_used_counter.change_signals[0].set_driver(increment_lines_used) #Regular increment condition	

				#for debug
				total_lines_used = inst(dl.Counter, reset_value=0, increment_value=1, end_value=(output_height-1), name="total_lines_used")
				total_lines_used.set_increment_condition(increment_lines_used)


				#Read FSM. In cascade mode we need to load in 10 values at a time for the length of the chain
				self.line_avaliable = inst(dl.Logic, bits=1, name="line_avaliable")
				self.weight_available = inst(dl.Logic, bits=1, name="weight_available") #Weight available is passed by TensorConv. It's a constant 1 if we're using On-Chip Memory
				self.everything_available = inst(dl.Logic, bits=1, name="everything_available")
				self.everything_available.set_driver(inst(dl.AND, self.line_avaliable, self.weight_available))

				states = ["IDLE", "LOAD_ACT", "READY"]
				edges = [["IDLE", "LOAD_ACT", inst(dl.AND, self.everything_available, self.no_freeze)], 
						["LOAD_ACT", "READY", self.read_counter_done],
						["READY", "LOAD_ACT", inst(dl.AND, self.ack_next_ping_pong, self.everything_available, self.no_freeze)], #Wait for an ack from the weight broadcast fsm and then either continue loading if a line is avaliable opr go to idle if not
						["READY", "IDLE", inst(dl.AND, self.ack_next_ping_pong, inst(dl.OR, inst(dl.NOT, self.no_freeze), inst(dl.NOT, self.everything_available)))]] 
				control = {"IDLE" : {"load_act" : 0, "next_ping_pong_ready" : 0},
						"LOAD_ACT" : {"load_act" : 1, "next_ping_pong_ready" : 0},
						"READY" : {"load_act" : 0, "next_ping_pong_ready" : 1} }				
				self.load_act_fsm = inst(dl.StateMachine, states, edges, control, name="load_act_fsm")

				#The load signal for the ping pong registers needs to happen 1 cycle before the data comes in since there is a pipeline register in the tensor block for it
				#Rather than just adding an extra pipeline register for all the data inputs which would drastically increase area, we just modify it in the FSM
				activate_load_signal = inst(dl.OR, inst(dl.AND, self.load_act_fsm.c["is_idle"], self.everything_available, self.no_freeze, name="activate_load_signal_cond_1"),
					  inst(dl.AND, self.load_act_fsm.c["is_ready"], self.everything_available, self.no_freeze, self.ack_next_ping_pong, name="activate_load_signal_cond_2"),
					  inst(dl.AND, self.load_act_fsm.c["is_load_act"], self.no_freeze, inst(dl.NOT, self.read_counter_done), name="activate_load_signal_cond_3"), name="activate_load_signal")

				#Counter to select which of the 3 activations we are loading from every line of the memory (we store multiple values in 1 line)
				#The read address should only increment once this reaches its end value
				mod_3_counter = inst(dl.Counter, reset_value=0, increment_value=1+max(0,stride-kernel_width), end_value=(1+max(0,stride-kernel_width))*(dl.NX_OUT_WIDTH_PARALLELISM_PER_TENSOR_BLOCK-1), name="mod_3_counter")
				#Counts how many cycles we have loaded in values into the tensor chain. Note that we will need an extra 3 cycles at the end in which case the read_addr_counter below should not increment
				load_cycle_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=(total_loading_cycles-1), name="load_cycle_counter")
				#should only increment when we want to actually change the read address of the memories (i.e. after reading 3 activations from the same line) 
				read_addr_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=(cur_memory_depth-1), load_value=(stride != kernel_height), name="read_addr_counter")
				#Should increment after every time we load all the ping pong registers. This will tell us when we are done with the current line of activations
				number_channel_loads_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=math.ceil(input_channels/input_channel_parallelism), name="number_channel_loads_counter")
				
				DONE_DELAY = max(0,READ_ADDR_DELAY-WRITE_DELAY) #Need to potentially delay signals if the write delay is shorter than the delay to complete a read

				#Should increment based on filter width
				filter_width_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=(kernel_width-1), name="filter_width_counter")
				filter_width_counter.set_increment_condition(number_channel_loads_counter.is_done)
				#Should increment based on filter height
				filter_height_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=(kernel_height-1), load_value=True, name="filter_height_counter")
				increment_filter_height_counter = inst(dl.AND, filter_width_counter.is_done, number_channel_loads_counter.is_done, name="increment_filter_height_counter")
				filter_height_counter.set_increment_condition(increment_filter_height_counter)


				#For upper and lower padding, as the filter moves down the input activations, the number of padding line changes, along with other variables
				#We identify all possible constants and select the correct constant through a mux whose select signal is an upper/lower padding counter (that counts to max number of padding lines)
				#A simple example, would be in a 11x11 filter with 5 lines of padding on each side, and stride 2. 
				#Number of padding lines = mux select between 5, 3 and 1.
				#Other padding-dependent variables might have similar muxing structures
				# Check if we're in the upper padding
				if upper_and_lower_padding[0] > 0:
					output_lines_that_require_upper_padding = math.ceil(upper_and_lower_padding[0]/stride)
					in_upper_padding.set_driver(inst(dl.LT, self.output_lines_counter.current_value, inst(dl.Constant, bits=clog2(output_height), value=output_lines_that_require_upper_padding)))
					self.in_upper_padding = in_upper_padding
					upper_padding_amounts = [upper_and_lower_padding[0] - i*stride for i in range(output_lines_that_require_upper_padding)]
					upper_padding_amounts_constants = [inst(dl.Constant, bits=max(1, clog2(kernel_height)), value=upper_padding_amounts[i], name="filt_height_after_upper_padding_const"+str(i)) for i in range(output_lines_that_require_upper_padding)]
					upper_padding_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=math.ceil(output_lines_that_require_upper_padding-1), name="upper_padding_counter")
					upper_padding_counter.set_increment_condition(inst(dl.AND, self.read_counter_completely_done, in_upper_padding))
					upper_padding_counter_current_value = upper_padding_counter.current_value
				else:
					in_upper_padding.set_driver(inst(dl.Constant, bits=1, value=0, name="in_upper_padding_const_0"))
					upper_padding_amounts_constants = [inst(dl.Constant, bits=max(1,clog2(kernel_height)), value=0)]
					upper_padding_counter_current_value = inst(dl.Constant, bits=1, value=0)
					self.in_upper_padding = inst(dl.Constant, bits=1, value=0, name="in_upper_padding")

				#Check if we're in the lower padding
				if upper_and_lower_padding[1] > 0:
					output_lines_that_require_lower_padding = math.ceil(upper_and_lower_padding[1]/stride)
					in_lower_padding = inst(dl.GT, self.output_lines_counter.current_value, inst(dl.Constant, bits=clog2(output_height), value=(output_height-1-output_lines_that_require_lower_padding)), name="in_lower_padding")
					self.in_lower_padding = in_lower_padding
					lower_padding_amounts = [upper_and_lower_padding[1] - i*stride for i in range(output_lines_that_require_lower_padding)]
					lower_padding_amounts.reverse()
					lower_padding_amounts_constants = [inst(dl.Constant, bits=max(1, clog2(kernel_height)), value=lower_padding_amounts[i], name="filt_height_after_lower_padding_const"+str(i)) for i in range(output_lines_that_require_lower_padding)]
					lower_padding_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=math.ceil(output_lines_that_require_lower_padding-1), name="lower_padding_counter")
					lower_padding_counter.set_increment_condition(inst(dl.AND, self.read_counter_completely_done, in_lower_padding))
					lower_padding_counter_current_value = lower_padding_counter.current_value
				else:
					in_lower_padding = inst(dl.Constant, bits=1, value=0, name="in_lower_padding_const_0")
					lower_padding_amounts_constants = [inst(dl.Constant, bits=max(1,clog2(kernel_height)), value=0)]
					lower_padding_counter_current_value = inst(dl.Constant, bits=1, value=0)
					self.in_lower_padding = inst(dl.Constant, bits=1, value=0, name="in_lower_padding")


				if upper_and_lower_padding[0] > 0 or upper_and_lower_padding[1] > 0:
					#If we have padding, we don't need as many lines in the activation buffer to begin the current convolution 
					#Basically we need to subtract how ever many lines of padding we need from the kernel height, while also making sure we don't start with 0 lines (note that comparison further down is > not >=, so > 0 is valid)
					filt_height_after_upper_padding_const = inst(dl.MuxV2, upper_padding_counter_current_value, *upper_padding_amounts_constants, name="filt_height_after_upper_padding_const")
					
					lines_needed_during_upper_padding = inst(dl.MuxV2, inst(dl.NOT, inst(dl.LT, filter_height_counter.current_value, filt_height_after_upper_padding_const)), filter_height_counter.current_value, 
						inst(dl.SUB, filter_height_counter.current_value, filt_height_after_upper_padding_const), name="lines_needed_during_upper_padding")

					filt_height_after_lower_padding_const = inst(dl.MuxV2, lower_padding_counter_current_value, *lower_padding_amounts_constants, name="filt_height_after_lower_padding_const")
					lines_needed_during_lower_padding = inst(dl.MuxV2, inst(dl.NOT, inst(dl.LT, filter_height_counter.current_value, filt_height_after_lower_padding_const)), filter_height_counter.current_value, 
						inst(dl.SUB, filter_height_counter.current_value, filt_height_after_lower_padding_const), name="lines_needed_during_lower_padding")

					lines_needed_to_start = inst(dl.MuxV2, in_upper_padding, inst(dl.MuxV2, in_lower_padding, filter_height_counter.current_value, lines_needed_during_lower_padding, name="in_lower_padding_mux"), lines_needed_during_upper_padding, name="lines_needed_to_start")
					if stride > kernel_height:
						self.line_avaliable.set_driver(inst(dl.GT, lines_used_counter.current_value, inst(dl.ADD, lines_needed_to_start, inst(dl.Constant, bits=clog2(stride), value=stride-kernel_height))))
					else:
						self.line_avaliable.set_driver(inst(dl.GT, lines_used_counter.current_value, lines_needed_to_start))

				else:
					if stride > kernel_height:
						self.line_avaliable.set_driver(inst(dl.GT, lines_used_counter.current_value, inst(dl.ADD, filter_height_counter.current_value, inst(dl.Constant, bits=clog2(stride), value=stride-kernel_height))))
					else:
						self.line_avaliable.set_driver(inst(dl.GT, lines_used_counter.current_value, filter_height_counter.current_value))

				#For last row in a image we may have to end early. To do this we just need to ensure it resets to something other than 0
				filter_height_counter_enable_load_value_lower_padding = inst(dl.AND, filter_height_counter.done_and_incrementing.delay(DONE_DELAY+1), in_lower_padding, name="filter_height_counter_enable_load_value_lower_padding")
				extra_lower_padding_needed = inst(dl.MuxV2, lower_padding_counter_current_value, *lower_padding_amounts_constants, name="extra_lower_padding_needed")

				# Check if we have upper padding as well, in that case we need to load a value as well
				if upper_and_lower_padding[0] > 0:
					filter_height_counter_enable_load_value_upper_padding = inst(dl.AND, filter_height_counter.is_reset_value, in_upper_padding, name="filter_height_counter_enable_load_value_upper_padding")
					filter_height_counter.enable_load_value.set_driver(inst(dl.OR, filter_height_counter_enable_load_value_lower_padding, filter_height_counter_enable_load_value_upper_padding, name="filter_height_counter_enable_load_value"))
					extra_upper_padding_needed = inst(dl.MuxV2, upper_padding_counter.current_value, *upper_padding_amounts_constants, name="extra_upper_padding_needed")
					filter_height_counter.load_value.set_driver(inst(dl.MuxV2, filter_height_counter_enable_load_value_upper_padding, extra_lower_padding_needed, extra_upper_padding_needed, name="extra_padding_needed_const"))
				else:
					filter_height_counter.enable_load_value.set_driver(filter_height_counter_enable_load_value_lower_padding) 
					filter_height_counter.load_value.set_driver(extra_lower_padding_needed)

				#If the stride is less than kernel height we need to re-use some input lines
				if (kernel_width > 1 or kernel_height > 1) and stride < kernel_height:
					#assert(stride <= kernel_height) #if the stride is greater we can't handle it right now
					#don't want to rollback on final line of image

					rollback_addr = inst(dl.Flop, reset_driver=0, bits=clog2(cur_memory_depth), name="rollback_addr")

					#Filter height check based on stride (we need to check when we're about to increment to capture just 1 cycle)
					if upper_and_lower_padding[0] > 0:
						filter_height_check_constant = inst(dl.MuxV2, inst(dl.AND, in_upper_padding, inst(dl.GT, filt_height_after_upper_padding_const, inst(dl.Constant, bits=max(1, clog2(kernel_height)), value=(stride)))), inst(dl.Constant, bits=max(1, clog2(kernel_height)), value=(stride)), filt_height_after_upper_padding_const)
						filter_height_check = inst(dl.AND, inst(dl.EQ, filter_height_counter.counter_plus_change.delay(1), filter_height_check_constant), 
							inst(dl.OR, increment_filter_height_counter.delay(1), filter_height_counter_enable_load_value_upper_padding.delay(2)), name="filter_height_check")  
					else:
						filter_height_check = inst(dl.AND, inst(dl.EQ, filter_height_counter.counter_plus_change.delay(1), inst(dl.Constant, bits=max(1, clog2(kernel_height)), value=(stride))), increment_filter_height_counter.delay(1), name="filter_height_check")  

					rollback_addr.set_driver(inst(dl.MuxV2, filter_height_check, rollback_addr, read_addr_counter.current_value))

					#need to add a rollback for depthwise convs as well since we do the entire depth before moving on to the next filter along the width
					if is_depthwise:
						read_addr_counter_load_val_signal = inst(dl.AND, number_channel_loads_counter.is_done, filter_width_counter.is_done, filter_height_counter.is_done, inst(dl.NOT, self.output_lines_counter.is_done, name="read_addr_counter_load_val_signal")) 
						en_depthwise_rollback= inst(dl.AND, number_channel_loads_counter.is_done, inst(dl.NOT, increment_filter_height_counter), name="en_depthwise_rollback")

						rollback_addr_depthwise = inst(dl.Flop, reset_driver=0, bits=clog2(cur_memory_depth), name="rollback_addr_depthwise")
						rollback_addr_depthwise.set_driver(inst(dl.MuxV2, number_channel_loads_counter.is_done.delay(1), rollback_addr_depthwise, read_addr_counter.current_value))
						
						read_addr_counter.load_value.set_driver(inst(dl.MuxV2, read_addr_counter_load_val_signal, rollback_addr_depthwise, rollback_addr))

						read_addr_counter.enable_load_value.set_driver(inst(dl.OR, en_depthwise_rollback, read_addr_counter_load_val_signal)) 
					else:
						read_addr_counter_load_val_signal = inst(dl.AND, number_channel_loads_counter.is_done, inst(dl.NOT, inst(dl.AND, inst(dl.OR, inst(dl.NOT, filter_height_counter.is_done), self.output_lines_counter.is_done), filter_width_counter.is_done)), name="read_addr_counter_load_val_signal") 
						channel_rollback_addr = inst(dl.Flop, reset_driver=0, bits=clog2(cur_memory_depth), name="channel_rollback_addr")
						channel_rollback_update = inst(dl.AND, number_channel_loads_counter.is_reset_value, self.load_act_fsm.c["load_act"], inst(dl.NOT, self.load_act_fsm.c["load_act"].delay(1)))
						channel_rollback_addr.set_driver(inst(dl.MuxV2, channel_rollback_update, channel_rollback_addr, read_addr_counter.current_value))
						rollback_addr_final = inst(dl.MuxV2, inst(dl.AND, filter_width_counter.is_done, filter_height_counter.is_done), channel_rollback_addr, rollback_addr)
						read_addr_counter.enable_load_value.set_driver(read_addr_counter_load_val_signal) 
						read_addr_counter.load_value.set_driver(rollback_addr_final)
				elif stride > kernel_height:
					assert(not is_depthwise) #I'm not sure how to support this but it shouldn't be too complex. It's not a case we encounter currently
					#If the stride is greater than kernel height, we need to skip some lines
					skip_amount = ((stride - kernel_height)*input_channels_per_mem) 
					read_addr_counter_load_val_signal = inst(dl.AND, filter_height_counter.is_done, filter_width_counter.is_done , number_channel_loads_counter.is_done ,name="read_addr_counter_load_val_signal")
					read_addr_counter.enable_load_value.set_driver(read_addr_counter_load_val_signal)
					normal_skip_addr = inst(dl.ADD, read_addr_counter.current_value.delay(1), inst(dl.Constant, bits=read_addr_counter.current_value._bit_width, value=skip_amount, name="skip_constant"), name="normal_skip_addr")
					loop_around_skip_addr = inst(dl.SUB, read_addr_counter.current_value.delay(1), inst(dl.Constant, bits=read_addr_counter.current_value._bit_width, value= cur_memory_depth - skip_amount, name="loop_around_constant"), name="loop_around_skip_addr")
					loop_around = inst(dl.GT, read_addr_counter.current_value.delay(1), inst(dl.Constant, bits=read_addr_counter.current_value._bit_width, value = cur_memory_depth - skip_amount - 1, name="loop_around_condition"))
					skip_ahead_addr = inst(dl.MuxV2, loop_around, normal_skip_addr[read_addr_counter.current_value._bit_width-1:0], loop_around_skip_addr[read_addr_counter.current_value._bit_width-1:0])
					read_addr_counter.load_value.set_driver(skip_ahead_addr)


				load_cycle_counter.set_increment_condition(inst(dl.AND, self.no_freeze, self.load_act_fsm.c["load_act"]))
				mod_3_counter.set_increment_condition(inst(dl.AND, self.no_freeze, self.load_act_fsm.c["load_act"]))
				before_last_3_cycles = inst(dl.LT, load_cycle_counter.current_value, inst(dl.Constant, bits=clog2(total_loading_cycles), value=(total_loading_cycles-3)), name="before_last_3_cycles")
				

				#Corner case: if ICP not a factor of ICs, then the read address counter should not increase after a certain point on the last iteration of the number_channel_loads_counter
				icp_remainder = (dl.NX_ICP_PER_TENSOR_BLOCK*math.ceil(input_channels/dl.NX_ICP_PER_TENSOR_BLOCK)) % input_channel_parallelism
				if icp_remainder != 0:
					#Calculate how many activations we actually have to load in last interation
					#For example, with the NX's vector size of 10, IC=40, and ICP=30: the first iteration will pre-load 30 activations and the second will only pre-load 10
					#The values still need to be cascaded down the chain, however (OC of most layers is more than large enough to hide the latency regardless)
					tensors_loaded_for_last_load = int(icp_remainder/dl.NX_ICP_PER_TENSOR_BLOCK)
					number_channel_loads_counter_almost_done = inst(dl.EQ, number_channel_loads_counter.current_value, 
						inst(dl.Constant, bits=clog2(math.ceil(input_channels/input_channel_parallelism)+1), value=math.ceil(input_channels/input_channel_parallelism)-1), name="number_channel_loads_counter_almost_done")
					after_last_x_cycles = inst(dl.GE, load_cycle_counter.current_value, 
						inst(dl.Constant, bits=clog2(total_loading_cycles), value=(tensors_loaded_for_last_load*dl.NX_OUT_WIDTH_PARALLELISM_PER_TENSOR_BLOCK)), name="after_last_x_cycles")
					
					pause_read_addr_counter = inst(dl.AND, number_channel_loads_counter_almost_done, after_last_x_cycles, name="pause_read_addr_counter")
					increment_read_addr_counter = inst(dl.AND, self.load_act_fsm.c["load_act"], self.no_freeze, before_last_3_cycles, inst(dl.NOT, pause_read_addr_counter), mod_3_counter.is_done, name="increment_read_addr_counter")
					assert(not is_depthwise) #Depthwise convolutions have no ICP so there should not be any sort of "remainder"
					read_addr_counter.set_increment_condition(increment_read_addr_counter)
				else:
					#This just avoids incrementing the read address for the last 3 cycles, as those values would end up in the top tensor which isn't used for calculations
					increment_read_addr_counter = inst(dl.AND, self.load_act_fsm.c["load_act"], self.no_freeze, before_last_3_cycles, mod_3_counter.is_done, name="increment_read_addr_counter")
					#Depthwise convolutions should increment the address before finishing the current filter width and then it should reset it back later
					if is_depthwise: increment_read_addr_counter = inst(dl.AND, self.load_act_fsm.c["load_act"], self.no_freeze, before_last_3_cycles, mod_3_counter.is_done, name="increment_read_addr_counter_depthwise")
					
					read_addr_counter.set_increment_condition(increment_read_addr_counter)

				self.read_counter_done.set_driver(inst(dl.AND, self.load_act_fsm.c["load_act"],load_cycle_counter.is_done, self.no_freeze))
				number_channel_loads_counter.set_increment_condition(inst(dl.OR, self.read_counter_done, number_channel_loads_counter.is_done)) #This counter needs to reset 1 cycle after it reaches the max value
				
				self.read_counter_completely_done.set_driver(inst(dl.AND, number_channel_loads_counter.is_done.delay(DONE_DELAY), 
					filter_width_counter.is_done.delay(DONE_DELAY), filter_height_counter.is_done.delay(DONE_DELAY))) 


				write_addr_counters = [None for i in range(dl.NX_ICP_PER_TENSOR_BLOCK)]


				# We get an entire input width's worth for activations at a time (for 1 channel) so we must create enough memories for that
				# We will use the variable below to account for the fact that a memory can support 40-bit reads/writes, so multiple iterations can map to the same memory
				mem_width_remaining = 0
				new_mem_count = -1 #number of new memories (groups of 10)

				#Instantiate connections for all inputs
				ram_write_data = [inst(dl.Logic, bits=self.MEM_READ_WIDTH, name=("mem_" + str(i) + "_write_data")) for i in range(math.ceil(input_width*dl.NX_INPUT_DATA_BITS/self.MEM_READ_WIDTH))]
				if enable_bfp: fp_exp_ram_write_data = [inst(dl.Logic, bits=self.MEM_READ_WIDTH, name=("fp_exp_mem_" + str(i) + "_write_data")) for i in range(math.ceil(input_width*dl.NX_INPUT_DATA_BITS/self.MEM_READ_WIDTH))]
				const_0_bus = inst(dl.Constant, bits=self.MEM_READ_WIDTH, value=0, name=("const_0_bus"))

				channel_modules = []

				#Instantiate output data. In tensor mode we will have one 10-value output (8-bits each) for each tensor chain
				self.output_data = [inst(dl.Logic, bits=(dl.NX_ICP_PER_TENSOR_BLOCK*dl.NX_INPUT_DATA_BITS), name=("output_data_bus_" + str(i))) for i in range(num_chains)]
				if enable_bfp: self.output_exp_data = [inst(dl.Logic, bits=(dl.NX_ICP_PER_TENSOR_BLOCK*dl.NX_SHARED_EXP_SIZE), name=("output_exp_data_bus_" + str(i))) for i in range(num_chains)]
				self.output_write = inst(dl.Logic, bits=2, name=("output_data_write"))
				output_data_non_concat = [[] for i in range(num_chains)] #non-concatinated version of the above for when the loop is filling it in 
				output_exp_non_concat = [[] for i in range(num_chains)]
				output_data_indexes = [0 for i in range(dl.NX_ICP_PER_TENSOR_BLOCK)]

				#Stores the current activations we are reading (len from 0 to NX_OUT_WIDTH_PARALLELISM_PER_TENSOR_BLOCK)
				cur_act_vals = [[] for i in range(dl.NX_ICP_PER_TENSOR_BLOCK)]
				cur_act_exp_vals = [[] for i in range(dl.NX_ICP_PER_TENSOR_BLOCK)]

				# Handle left side padding by appending 0s as the first set of values you can select
				# Padding on the right side is handled automatically and padding at the bottom is handled using counters
				if left_padding[0] > 0:
					for i in range(dl.NX_ICP_PER_TENSOR_BLOCK):
						for j in range(left_padding[0]):
							cur_act_vals[i].append(constant_0)
							cur_act_exp_vals[i].append(constant_0)

				for cur_in_act in range(input_width):
					new_memory_needed = False
					if mem_width_remaining == 0:
						new_mem_count += 1
						mem_width_remaining = self.MEM_READ_WIDTH					
						concatinated_inputs_list = []
						concatinated_exp_inputs_list = []
						new_memory_needed = True
						

					#Create the 10 memories if needed, otherwise just hook up the signals
					for cur_in_channel in range(dl.NX_ICP_PER_TENSOR_BLOCK):
						#Split these memories up into multiple modules (1 per channel) as otherwise we end up having 5000+ lines in one file and a ton of signals (debugging is annoying)
						if len(channel_modules) < (cur_in_channel+1):
							channel_modules.append(inst(dl.Module, ("tensor_act_buff_channel_"+str(cur_in_channel)), kind=("tensor_act_buff_channel_"+str(cur_in_channel))))

						with dl.ModuleScope(channel_modules[cur_in_channel]):
							if new_memory_needed:
								#Instantiate the RAM only if the current layer's input channels support it (e.g. for a layer with 3 input channels, 7 of the RAMs should not be instantiated)
								if cur_in_channel < input_channels: 
									cur_ram = inst(dl.RAM, bits=self.MEM_READ_WIDTH, depth=cur_memory_depth, register_output=bool(EXTRA_OUTPUT_REG), name=("mem_" + str(new_mem_count) + "_channel_" + str(cur_in_channel)))
									cur_ram.r_en.set_driver(inst(dl.Constant, bits=1, value=1, name="const_read_en_1")) #just set read enable to a constant since we don't care about what happens with invalid values
									#Create a seperate RAM for the exponents if we're using Bfloat
									if enable_bfp:
										cur_fp_exp_ram = inst(dl.RAM, bits=self.MEM_READ_WIDTH, depth=cur_memory_depth, register_output=bool(EXTRA_OUTPUT_REG), name=("fp_exp_mem_" + str(new_mem_count) + "_channel_" + str(cur_in_channel)))
										cur_fp_exp_ram.r_en.set_driver(inst(dl.Constant, bits=1, value=1, name="fp_exp_const_read_en_1")) #just set read enable to a constant since we don't care about what happens with invalid values

								#We have 10 seperate input channels that need to be written in parallel to the cascade chain, and out input values come in 1 at a time. Therefore we must check which mem we are writing to next based on the counter
								eq_tmp = inst(dl.EQ, channel_counter.current_value, channel_count_consts[cur_in_channel], name=("channel_counter_" +str(new_mem_count)+ "_eq_cur_channel_" + str(cur_in_channel)))
								#Clean up extra channels here by turning on writes (later down we set the values to 0)
								if ic_remainder != 0 and cur_in_channel >= ic_remainder:
									cur_ram_write_en = inst(dl.AND, self.write_en.delay(WRITE_DELAY), inst(dl.OR, eq_tmp.delay(WRITE_DELAY), full_channel_counter_is_done.delay(WRITE_DELAY)), name=("mem_" + str(new_mem_count) + "_channel_" + str(cur_in_channel) + "_wr_en"))
								else:	
									cur_ram_write_en = inst(dl.AND, self.write_en.delay(WRITE_DELAY), eq_tmp.delay(WRITE_DELAY), name=("mem_" + str(new_mem_count) + "_channel_" + str(cur_in_channel) + "_wr_en"))
								
								if cur_in_channel < input_channels:
									cur_ram.w_en.set_driver(cur_ram_write_en.delay(CONTROL_TO_DATA_DELAY))
									if enable_bfp: cur_fp_exp_ram.w_en.set_driver(cur_ram_write_en.delay(CONTROL_TO_DATA_DELAY))

								if READ_ADDR_TO_READ_DELAY is None:
									READ_ADDR_TO_READ_DELAY = READ_ADDR_DELAY + cur_ram.model_delay #delay until we read a value

								# Create address counters if needed for each of the 10 memories we will be hooking up in parallel
								if write_addr_counters[cur_in_channel] == None:
									write_addr_counters[cur_in_channel] = inst(dl.Counter, reset_value=0, increment_value=1, end_value=(cur_memory_depth-1), name=("write_addr_counter_channel_"+str(cur_in_channel)))
									write_addr_counters[cur_in_channel].set_increment_condition(cur_ram_write_en)

								# Hook up address for the RAM
								if cur_in_channel < input_channels: 
									w_addr_driver = write_addr_counters[cur_in_channel].current_value.delay(WRITE_DELAY+CONTROL_TO_DATA_DELAY)
									cur_ram.w_addr.set_driver(w_addr_driver)
									if enable_bfp: cur_fp_exp_ram.w_addr.set_driver(w_addr_driver)

								#hook up write value
								if cur_in_channel < input_channels: 
									if ic_remainder != 0 and cur_in_channel >= ic_remainder:
										cur_ram.w_data.set_driver(inst(dl.MuxV2, full_channel_counter_is_done.delay(WRITE_DELAY+CONTROL_TO_DATA_DELAY), ram_write_data[new_mem_count].delay(WRITE_DELAY), const_0_bus, name=("mem_" + str(new_mem_count) + "_write_data_sel")))
										if enable_bfp: cur_fp_exp_ram.w_data.set_driver(inst(dl.MuxV2, full_channel_counter_is_done.delay(WRITE_DELAY+CONTROL_TO_DATA_DELAY), fp_exp_ram_write_data[new_mem_count].delay(WRITE_DELAY), const_0_bus, name=("fp_exp_mem_" + str(new_mem_count) + "_write_data_sel")))
									else:
										cur_ram.w_data.set_driver(ram_write_data[new_mem_count].delay(WRITE_DELAY))
										if enable_bfp: cur_fp_exp_ram.w_data.set_driver(fp_exp_ram_write_data[new_mem_count].delay(WRITE_DELAY))

									r_addr_driver = read_addr_counter.current_value.delay(READ_ADDR_DELAY+CONTROL_TO_DATA_DELAY)
									cur_ram.r_addr.set_driver(r_addr_driver)
									if enable_bfp: cur_fp_exp_ram.r_addr.set_driver(r_addr_driver)


								#Handle read data
								for i in range(0,self.MEM_READ_WIDTH, dl.NX_INPUT_DATA_BITS):								
									#Add in the new value

									cur_val_sel = inst(dl.Logic, bits=dl.NX_INPUT_DATA_BITS, name=("mem_" + str(new_mem_count) + "_channel_" + str(cur_in_channel) + "_sel_" + str(i)))
									if enable_bfp: cur_exp_val_sel = inst(dl.Logic, bits=dl.NX_SHARED_EXP_SIZE, name=("exp_mem_" + str(new_mem_count) + "_channel_" + str(cur_in_channel) + "_sel_" + str(i)))

									if cur_in_channel < input_channels: 
										cur_val_sel.set_driver(cur_ram.r_data[i+dl.NX_INPUT_DATA_BITS-1:i])
										if enable_bfp: cur_exp_val_sel.set_driver(cur_fp_exp_ram.r_data[i+dl.NX_INPUT_DATA_BITS-1:i])
									else:
										cur_val_sel.set_driver(inst(dl.Constant, bits=dl.NX_INPUT_DATA_BITS, value=0, name="no_ram"))
										if enable_bfp: cur_exp_val_sel.set_driver(inst(dl.Constant, bits=dl.NX_INPUT_DATA_BITS, value=0, name="no_ram"))

									cur_act_vals[cur_in_channel].append(cur_val_sel)
									if enable_bfp: cur_act_exp_vals[cur_in_channel].append(cur_exp_val_sel)

									#For larger filters we need to route more values to each tensor chain. Some values may be shared if we have overlap
									overlap = (kernel_width-stride)
									total_overlap = overlap*(dl.NX_OUT_WIDTH_PARALLELISM_PER_TENSOR_BLOCK-1)
									#If overlap is negative, we add the useless activation values to the required output values to make the pattern regular
									required_output_values = (dl.NX_OUT_WIDTH_PARALLELISM_PER_TENSOR_BLOCK*kernel_width) - total_overlap - min(0,overlap)


									#Add zeros for remaining values
									if dl.NX_INPUT_DATA_BITS*(input_width-cur_in_act) <= self.MEM_READ_WIDTH and i == (self.MEM_READ_WIDTH-dl.NX_INPUT_DATA_BITS): 
										while len(cur_act_vals[cur_in_channel]) != required_output_values:
											cur_act_vals[cur_in_channel].append(constant_0)
											if enable_bfp: cur_act_exp_vals[cur_in_channel].append(constant_0)

									# If we have gathered enough values for one tensor block we can instantiate the muxes to select the appropriate values
									# Note that it's highly reccomended that both the memory and output of this mux are registered for better timing
									# We only hook up values to the top of a chain so timing shouldn't be too bad
									if len(cur_act_vals[cur_in_channel]) == required_output_values:
										#Corner case for where we have extra values in a memory such that they wouldn't hook up to a tensor chain (possible since we have 5 read vals per mem, input width might not be divisible)
										if output_data_indexes[cur_in_channel] >= num_chains:
											cur_act_vals[cur_in_channel].clear()
											if enable_bfp: cur_act_exp_vals[cur_in_channel].clear()
											break

										if kernel_width == 1:
											delayed_mod_3_counter = mod_3_counter.current_value.delay(READ_ADDR_TO_READ_DELAY+CONTROL_TO_DATA_DELAY)

											output_data_non_concat[output_data_indexes[cur_in_channel]].append(inst(dl.MuxV2, delayed_mod_3_counter, 
												*cur_act_vals[cur_in_channel], name=("output_data_" + str(output_data_indexes[cur_in_channel]) + "_channel_" + str(cur_in_channel))))
											cur_act_vals[cur_in_channel] = []
											
											if enable_bfp:
												output_exp_non_concat[output_data_indexes[cur_in_channel]].append(inst(dl.MuxV2, delayed_mod_3_counter, 
												*cur_act_exp_vals[cur_in_channel], name=("output_exp_data_" + str(output_data_indexes[cur_in_channel]) + "_channel_" + str(cur_in_channel))))
												cur_act_exp_vals[cur_in_channel] = []
										else:
											#First we need to create the muxes to select the current column for the filter value
											first_layer_muxes = []
											first_layer_exp_muxes = []
											window_start = 0
											for i in range(dl.NX_OUT_WIDTH_PARALLELISM_PER_TENSOR_BLOCK):
												delayed_filter_width_counter = filter_width_counter.current_value.delay(READ_ADDR_TO_READ_DELAY+CONTROL_TO_DATA_DELAY)
												cur_mux = inst(dl.MuxV2, delayed_filter_width_counter, 
													*cur_act_vals[cur_in_channel][window_start:window_start+kernel_width], name=("filter_mux_" + str(i) + "_output_index_" + str(output_data_indexes[cur_in_channel]) + "_channel_" + str(cur_in_channel)))
												first_layer_muxes.append(cur_mux)

												if enable_bfp:
													cur_exp_mux = inst(dl.MuxV2, delayed_filter_width_counter, 
													*cur_act_exp_vals[cur_in_channel][window_start:window_start+kernel_width], name=("filter_mux_" + str(i) + "_output_exp_index_" + str(output_data_indexes[cur_in_channel]) + "_channel_" + str(cur_in_channel)))
													first_layer_exp_muxes.append(cur_exp_mux)

												window_start += kernel_width - overlap

											delayed_mod_3_counter = mod_3_counter.current_value.delay(READ_ADDR_TO_READ_DELAY+CONTROL_TO_DATA_DELAY)
											#Now we can create the muxes to select the correct output column (each tensor does 3 output columns)
											output_data_non_concat[output_data_indexes[cur_in_channel]].append(inst(dl.MuxV2, delayed_mod_3_counter, 
												*first_layer_muxes, name=("output_data_" + str(output_data_indexes[cur_in_channel]) + "_channel_" + str(cur_in_channel))))
											cur_act_vals[cur_in_channel] = cur_act_vals[cur_in_channel][-overlap:]
											if enable_bfp:
												output_exp_non_concat[output_data_indexes[cur_in_channel]].append(inst(dl.MuxV2, delayed_mod_3_counter, 
												*first_layer_exp_muxes, name=("output_exp_data_" + str(output_data_indexes[cur_in_channel]) + "_channel_" + str(cur_in_channel))))
												cur_act_exp_vals[cur_in_channel] = cur_act_exp_vals[cur_in_channel][-overlap:]

										assert(len(output_data_non_concat[output_data_indexes[cur_in_channel]]) == cur_in_channel+1)
										output_data_indexes[cur_in_channel] += 1

					#add the current input activation to the list of inputs
					concatinated_inputs_list.append(self.inputs[cur_in_act])
					if enable_bfp: concatinated_exp_inputs_list.append(self.fp_exponent_inputs[cur_in_act])


					#If we need to create a new memory on the next iteration, concatinate the inputs and hook them up to the current memory (through ram write data)
					if mem_width_remaining == dl.NX_INPUT_DATA_BITS or cur_in_act==(input_width-1):
						# Pad in zeros for any remaining inputs
						for i in range(0,mem_width_remaining-dl.NX_INPUT_DATA_BITS,dl.NX_INPUT_DATA_BITS):
							concatinated_inputs_list.append(constant_0)
							if enable_bfp: concatinated_exp_inputs_list.append(constant_0)

						concatinated_inputs = inst(dl.Concat, *concatinated_inputs_list, name=("concatinated_inputs_"+str(new_mem_count)))
						ram_write_data[new_mem_count].set_driver(concatinated_inputs)
						if enable_bfp: 
							concatinated_exp_inputs = inst(dl.Concat, *concatinated_exp_inputs_list, name=("concatinated_exp_inputs_"+str(new_mem_count)))
							fp_exp_ram_write_data[new_mem_count].set_driver(concatinated_exp_inputs)

					mem_width_remaining -= dl.NX_INPUT_DATA_BITS


				#Combine output activations by concatinating
				for i in range(len(output_data_non_concat)):
					cur_out_data = output_data_non_concat[i]

					assert(len(cur_out_data) == dl.NX_ICP_PER_TENSOR_BLOCK) #This list must be the same length as the number of channels. The write should already handle extra channels by setting them to 0 if not a multiple of 10
					concatinated_outputs = inst(dl.Concat, *cur_out_data, name=("concatinated_outputs_"+str(i)))
					self.output_data[i].set_driver(concatinated_outputs)

					if enable_bfp:
						cur_exp_out_data = output_exp_non_concat[i]
						concatinated_exp_outputs = inst(dl.Concat, *cur_exp_out_data, name=("concatinated_exp_outputs_"+str(i)))
						self.output_exp_data[i].set_driver(concatinated_exp_outputs)

				#Set control signals for each chain
				ping_pong_sel_reg = inst(dl.Flop, bits=2, reset_driver=1, name="ping_pong_reg_sel")
				ping_pong_sel_reg.set_driver(inst(dl.MuxV2, inst(dl.AND, self.load_act_fsm.c["is_load_act"], self.read_counter_done), ping_pong_sel_reg, inst(dl.INV, ping_pong_sel_reg, reduction_output_bits=2), name="next_ping_pong_reg_sel"))
				sel_ping_pong_write = inst(dl.MuxV2, activate_load_signal, inst(dl.Constant, bits=2, value=0, name="const_two_bit_zero"), ping_pong_sel_reg, name="sel_ping_pong_write")
				self.output_write.set_driver(sel_ping_pong_write.delay(READ_ADDR_TO_READ_DELAY+CONTROL_TO_DATA_DELAY))


			else:
				print("ERROR: side-channel loading not yet supported!")
				exit(1)

		return



class TensorOutputBuffer(dl.Module):
	# This module creates an output buffer for when ICP != IC and/or the filter size is greater than 1x1xdepth
	# This module assumes that the outputs are in the correct order already (in cascade mode they are reveresed by default)
	# It should be instantiated in a loop by TensorConv
	# If using block floating-point, you can select between a soft-logic adder or hard-logic adder for the 3 output adders
	# The soft-logic adder uses 314 ALMs per adder and can add up to a huge chunk of area for each layer depending on the output size. Thus, it should only be used on small networks
	# The hard-logic adder uses 1 tensor block to perform 2 additions, which will minimize the number of tensor blocks used for other features

	def __init__(self, input_channel_parallelism, input_channels, output_channels, input_height, kernel_width, kernel_height, padding, stride, weight_mem_read_delay, CONTROL_TO_DATA_DELAY, use_tensor_for_fp_add=True, hbm_on = False, **kwargs):
		super(TensorOutputBuffer, self).__init__("tensor_output_buffers", kind="tensor_output_buffers", **kwargs)
		inst = self.inst
		with dl.ModuleScope(self):
			TENSOR_INPUT_DELAY = 1
			ACCUMULATE_OUTPUT_DELAY = 1 #Must be 1 or 0
			COUNTER_DELAY = 1 #Constant, don't modify this
			OUTPUT_DELAY = 1 #Constant, its the delay of the NN counter
			LAST_BATCH_DELAY = 1
			#These two are to help digitallogic create the fanout tree for the memories
			WRITE_DATA_DELAY = 1 #Must be 1 or 0
			INPUT_ADDR_EXTRA_DELAY = 1

			global enable_bfp

			#extra RAM output reg enable
			EXTRA_OUTPUT_REG = dl.ENABLE_RAM_OUTPUT_REG_NX

			assert(ACCUMULATE_OUTPUT_DELAY <= 1 and WRITE_DATA_DELAY <=1) #design won't work with delays outside this range

			if enable_bfp:
				TENSOR_BLOCK_DELAY = dl.NX_INPUT_TO_OUTPUT_DELAY_FXP #For some reason using the actual BFB delay (4 instead of 3) causes an incorrect result
				OUTPUT_PRECISION = dl.NX_TENSOR_BFLOAT_OUTPUT_PRECISION
				ADDER_MODULE_DELAY = 3 #FP24 adder has 2 pipeline registers and we also need to pipeline inputs
				if use_tensor_for_fp_add: ADDER_MODULE_DELAY = 3 #FP24 adder  in ternsor block has 3 pipeline registers
			else:
				TENSOR_BLOCK_DELAY = dl.NX_INPUT_TO_OUTPUT_DELAY_FXP
				OUTPUT_PRECISION = dl.NX_TENSOR_OUTPUT_PRECISION
				ADDER_MODULE_DELAY = 0
				
			#Create input and output signals
			self.inputs = [inst(dl.Logic, bits=OUTPUT_PRECISION, name=("output_buffer_input_"+str(i))) for i in range(dl.NX_OUT_WIDTH_PARALLELISM_PER_TENSOR_BLOCK)]
			self.outputs = [inst(dl.Logic, bits=OUTPUT_PRECISION, name=("output_buffer_output_"+str(i))) for i in range(dl.NX_OUT_WIDTH_PARALLELISM_PER_TENSOR_BLOCK)]
			self.ouputs_valid = inst(dl.Logic, bits=1, name="output_buffer_outputs_valid")

			self.last_batch = inst(dl.Logic, bits=1, name="last_batch")
			self.padding_batch = inst(dl.Logic, bits=1, name="first_batch")
			self.tab_in_upper_padding = inst(dl.Logic, bits=1, name="tab_in_upper_padding")
			self.early_last_fifo_has_weight = inst(dl.Logic, bits=1, name="tob_early_last_fifo_has_weight")
			self.pure_broadcast_weights = inst(dl.Logic, bits=1, name="tob_pure_broadcast_weights")
			delayed_last_batch = self.last_batch.delay(LAST_BATCH_DELAY)
			delayed_padding_batch  = self.padding_batch.delay(LAST_BATCH_DELAY)

			#broadcast signal from weight FSM. Should be hooked up to the last FSM in the chain
			self.broadcast_weights = inst(dl.Logic, bits=1, name="broadcast_weights")

			if input_channel_parallelism < input_channels or kernel_height > 1 or kernel_width > 1:

				#Combine output widths in 1 tensor block so we can better utilize memory (e.x. in this case we use 75-bits and we save 1+ M20K or 2+ MLABs than if we were to do seperately)
				tensor_output_buffer_ram = inst(dl.RAM, bits=(OUTPUT_PRECISION*dl.NX_OUT_WIDTH_PARALLELISM_PER_TENSOR_BLOCK), depth=output_channels, register_output=bool(EXTRA_OUTPUT_REG), name=("tensor_output_buffer_ram"))
				tensor_output_buffer_ram.r_en.set_driver(inst(dl.Constant, bits=1, value=1, name="const_read_en_1")) #just set read enable to a constant since we don't care about what happens with invalid values

				accumulator_mux_select = inst(dl.Logic, bits=1, name="accumulator_mux_select")
				constant_0 = inst(dl.Constant, bits=OUTPUT_PRECISION, value=0, name="constant_zero")

				accumulator_results = []

				#If we are using the tensor block to perform a fp24 (bfloat24) add, we need to group together 2 additions per block
				previous_inputs = []
				previous_res = None

				for i in range(dl.NX_OUT_WIDTH_PARALLELISM_PER_TENSOR_BLOCK):
					#Extract different outputs from the memory
					cur_accumulated_value = tensor_output_buffer_ram.r_data[((i+1)*OUTPUT_PRECISION-1):i*OUTPUT_PRECISION]

					if enable_bfp:
						assert(CONTROL_TO_DATA_DELAY >= (ADDER_MODULE_DELAY)) #need to subtract from this delay in bfloat mode
						#Select between previously accumulated value or 0 if we are starting a new batch of data
						accumulator_mux = inst(dl.MuxV2, accumulator_mux_select.delay(CONTROL_TO_DATA_DELAY-(ADDER_MODULE_DELAY)), constant_0, cur_accumulated_value, name=("accumulate_mux_"+str(i)))

						#Check if we are using the hard bfloat24 adder or the soft-logic adder
						if use_tensor_for_fp_add:
							#Store wires until we have 4 total inputs for 2 additions (or we are on last addition)
							if len(previous_inputs) > 0 or i == (dl.NX_OUT_WIDTH_PARALLELISM_PER_TENSOR_BLOCK-1):
								#Instantiate the hard-logic bfloat24 adder that uses 1 tensor block to perform 2 additions
								bfloat24_tensor_adder = inst(dl.TensorAdd, enable_reset=False, name=("hard_logic_adder_"+str(int(i/2))))
								added_res_temp = inst(dl.Logic, bits=OUTPUT_PRECISION, name=("accumulator_result"+str(i)))

								if len(previous_inputs) > 0:
									bfloat24_tensor_adder.inputs[0].set_driver(previous_inputs[0])
									bfloat24_tensor_adder.inputs[1].set_driver(previous_inputs[1])	
									bfloat24_tensor_adder.inputs[2].set_driver(accumulator_mux)
									bfloat24_tensor_adder.inputs[3].set_driver(self.inputs[i])
									previous_res.set_driver(bfloat24_tensor_adder.results[0])
									added_res_temp.set_driver(bfloat24_tensor_adder.results[1])
								else:
									bfloat24_tensor_adder.inputs[0].set_driver(accumulator_mux)
									bfloat24_tensor_adder.inputs[1].set_driver(self.inputs[i])	
									added_res_temp.set_driver(bfloat24_tensor_adder.results[0])	

								previous_inputs.clear()		
								previous_res = None		
														
							else:							
								previous_inputs.append(accumulator_mux)  
								previous_inputs.append(self.inputs[i])  
								added_res_temp = inst(dl.Logic, bits=OUTPUT_PRECISION, name=("accumulator_result"+str(i)))
								previous_res = added_res_temp

						else:
							# Instantiate the soft-logic adder. 
							# Note that this uses 314 ALMs per adder and can add up to a huge chunk of area for each layer. Thus, it should only be used on small networks
							added_res_temp = inst(dl.FP24_ADD, accumulator_mux.delay(1), self.inputs[i].delay(TENSOR_INPUT_DELAY), reset=None, name=("accumulator_result"+str(i)))
					else:
						#Select between previously accumulated value or 0 if we are starting a new batch of data
						accumulator_mux = inst(dl.MuxV2, accumulator_mux_select.delay(CONTROL_TO_DATA_DELAY), constant_0, cur_accumulated_value, name=("accumulate_mux_"+str(i)))
						added_res_temp = inst(dl.ADD, accumulator_mux, self.inputs[i].delay(TENSOR_INPUT_DELAY), name=("accumulator_result"+str(i)))

					accumulator_results.append(added_res_temp[OUTPUT_PRECISION-1:0])
					self.outputs[i].set_driver(accumulator_results[i][OUTPUT_PRECISION-1:0].delay(ACCUMULATE_OUTPUT_DELAY)) #to-do: implement proper rounding and see if it makes a difference

				#Accumulate concatinated results and send them back into the memory
				concatinated_accumulator_results = inst(dl.Concat, *accumulator_results, name="concatinated_accumulator_results")
				tensor_output_buffer_ram.w_data.set_driver(concatinated_accumulator_results.delay(WRITE_DATA_DELAY+ACCUMULATE_OUTPUT_DELAY))


				# Counter for write address. Needs to be delayed such that the weights can propagate through the tensor block
				# Need to have a version that comes in slightly quicker 
				total_delay = weight_mem_read_delay+TENSOR_BLOCK_DELAY+TENSOR_INPUT_DELAY+ACCUMULATE_OUTPUT_DELAY
				reduced_delay = total_delay - tensor_output_buffer_ram.model_delay - COUNTER_DELAY - INPUT_ADDR_EXTRA_DELAY 
				assert(reduced_delay >= 0) #Delay cannot be negative here
				#Subtract 1 register here and add it after the read/write counters for better fanout optimization in digital logic
				reduced_delay_broadcast_weights = self.broadcast_weights.delay(reduced_delay)
				delayed_broadcast_weights = reduced_delay_broadcast_weights.delay(tensor_output_buffer_ram.model_delay+COUNTER_DELAY+WRITE_DATA_DELAY+ADDER_MODULE_DELAY)
				input_addr_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=(output_channels-1), name="input_addr_counter")
				#Input address counter increment condition is delayed to later because it will need the restoration counter
				tensor_output_buffer_ram.w_en.set_driver(delayed_broadcast_weights.delay(INPUT_ADDR_EXTRA_DELAY+CONTROL_TO_DATA_DELAY))
				tensor_output_buffer_ram.w_addr.set_driver(input_addr_counter.current_value.delay(INPUT_ADDR_EXTRA_DELAY+CONTROL_TO_DATA_DELAY))

				#Calculate extra padding needed for stride (e.g. with a stride of 2 and a filter height of 1, we may need an extra row at the output of padding)
				top_row_padding = padding[0]
				bottom_row_padding = padding[1]

				#Counter for keeping track of which batch of weights we are currently processing
				number_of_batches = math.ceil(input_channels/float(input_channel_parallelism))*kernel_width*kernel_height
				num_batches_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=(number_of_batches-1), load_value=True, name="num_batches_counter")

				#For upper and lower padding, as the filter moves down the input activations, the number of padding line changes, along with other variables
				#We identify all possible constants and select the correct constant through a mux whose select signal is an upper/lower padding counter (that counts to max number of padding lines)
				#A simple example, would be in a 11x11 filter with 5 lines of padding on each side, and stride 2. 
				#Number of padding lines = mux select between 5, 3 and 1.
				#Other padding-dependent variables might have similar muxing structures
				# If we have padding in the top row we need to skip over a couple batches
				if top_row_padding != 0:
					output_lines_that_require_upper_padding = math.ceil(top_row_padding/stride)
					upper_padding_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=math.ceil(output_lines_that_require_upper_padding-1), name="upper_padding_counter")
					upper_padding_amounts = [top_row_padding - i*stride for i in range(output_lines_that_require_upper_padding)]
					first_batches_constants = [inst(dl.Constant, bits=clog2(number_of_batches) , value=(upper_padding_amounts[i]*kernel_width*math.ceil(input_channels/float(input_channel_parallelism)))) for i in range(output_lines_that_require_upper_padding)]
					if len(first_batches_constants) > 1:
						upper_batch_constants = inst(dl.MuxV2, upper_padding_counter.current_value, *first_batches_constants, name="upper_batch_constant_selected")
					else:
						upper_batch_constants = first_batches_constants[0]
					num_batches_eq_first_batch_const = inst(dl.EQ, num_batches_counter.current_value, upper_batch_constants, name="num_batches_eq_first_batch_const")
					load_padding_batch = inst(dl.AND, num_batches_counter.is_reset_value, delayed_padding_batch, name="load_padding_batch")
					in_upper_padding = inst(dl.AND, num_batches_eq_first_batch_const, delayed_padding_batch, name="in_upper_padding")
					upper_padding_counter_increment_condition = inst(dl.AND, self.tab_in_upper_padding, num_batches_counter.done_and_incrementing)
					upper_padding_counter.set_increment_condition(upper_padding_counter_increment_condition)
					


				num_batches_counter.set_increment_condition(input_addr_counter.done_and_incrementing.delay(INPUT_ADDR_EXTRA_DELAY))

				#For the last batch we need to potentially load fewer rows due to implicit padding
				last_line_in_image_increment = inst(dl.AND, delayed_last_batch, num_batches_counter.done_and_incrementing.delay(LAST_BATCH_DELAY+COUNTER_DELAY+OUTPUT_DELAY+CONTROL_TO_DATA_DELAY), name="last_line_in_image_increment")	
				if bottom_row_padding != 0:	
					output_lines_that_require_lower_padding = math.ceil(bottom_row_padding/stride)
					lower_padding_amounts = [bottom_row_padding - i*stride for i in range(output_lines_that_require_lower_padding)]
					lower_padding_amounts.reverse()	
					load_val_constants = [math.ceil(input_channels/float(input_channel_parallelism))*kernel_width*lower_padding_amounts[i] for i in range(output_lines_that_require_lower_padding)]
				else:
					load_val_constants = [0]
				num_batches_counter_load_const = [inst(dl.Constant, bits=clog2(number_of_batches), value=load_val_constants[i], name="num_batches_counter_load_const"+str(i)) for i in range(len(load_val_constants))]
				if len(num_batches_counter_load_const) == 1:
					lower_batch_constants = num_batches_counter_load_const[0]
				else:
					lower_padding_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=math.ceil(output_lines_that_require_lower_padding-1), name="lower_padding_counter")
					lower_padding_iteration_delayed = inst(dl.Flop, reset_driver=0, bits=1, name="lower_padding_inc_delayed")
					lower_padding_iteration_delayed.set_driver(inst(dl.MuxV2, num_batches_counter.done_and_incrementing, lower_padding_iteration_delayed, delayed_last_batch))
					lower_padding_counter_increment_condition = inst(dl.AND, num_batches_counter.done_and_incrementing, lower_padding_iteration_delayed)
					lower_padding_counter.set_increment_condition(lower_padding_counter_increment_condition)
					lower_batch_constants = inst(dl.MuxV2, lower_padding_counter.current_value, *num_batches_counter_load_const, name="lower_batch_constant_selected")
				num_batches_eq_last_batch_const = inst(dl.EQ, num_batches_counter.current_value, lower_batch_constants)
				num_batches_counter_last_line_and_init_val = inst(dl.AND, delayed_last_batch, num_batches_eq_last_batch_const, name="num_batches_counter_last_line_and_init_val")

				if top_row_padding != 0:
					num_batches_counter.enable_load_value.set_driver(inst(dl.OR, last_line_in_image_increment, load_padding_batch))
					num_batches_counter.load_value.set_driver(inst(dl.MuxV2, last_line_in_image_increment, upper_batch_constants, lower_batch_constants))
				else:
					num_batches_counter.enable_load_value.set_driver(last_line_in_image_increment)
					num_batches_counter.load_value.set_driver(lower_batch_constants)
				
				
				#For a delay of 0 we can just check if the batch is past 0. For a delay of 1 we must check if the batch is about to go past 1 since the mux will be before the accumulate register
				if ACCUMULATE_OUTPUT_DELAY == 0: 
					if top_row_padding != 0: 
						accumulator_mux_select.set_driver(inst(dl.AND, inst(dl.NOT, num_batches_counter.is_reset_value), inst(dl.NOT, in_upper_padding), inst(dl.NOT, num_batches_counter_last_line_and_init_val), name="accumulator_mux_select"))
					else: 
						accumulator_mux_select.set_driver(inst(dl.AND, inst(dl.NOT, num_batches_counter.is_reset_value), inst(dl.NOT, num_batches_counter_last_line_and_init_val), name="accumulator_mux_select"))
				else: 
					if top_row_padding != 0:
						accumulator_mux_select.set_driver(inst(dl.AND, inst(dl.OR, inst(dl.NOT, num_batches_counter.is_reset_value), inst(dl.AND, num_batches_counter.is_reset_value, input_addr_counter.done_and_incrementing.delay(1))),
							inst(dl.NOT, in_upper_padding), inst(dl.NOT, num_batches_counter_last_line_and_init_val), name="accumulator_mux_select"))
					else:			
						accumulator_mux_select.set_driver(inst(dl.AND, inst(dl.OR, inst(dl.NOT, num_batches_counter.is_reset_value), inst(dl.AND, num_batches_counter.is_reset_value, input_addr_counter.done_and_incrementing.delay(1))),
							inst(dl.NOT, num_batches_counter_last_line_and_init_val), name="accumulator_mux_select"))

				#Set output valid signal
				self.ouputs_valid.set_driver(inst(dl.AND, reduced_delay_broadcast_weights.delay(tensor_output_buffer_ram.model_delay+COUNTER_DELAY+INPUT_ADDR_EXTRA_DELAY+ADDER_MODULE_DELAY), num_batches_counter.is_done).delay(CONTROL_TO_DATA_DELAY))

				#Counter to keep track of the read address 
				read_addr_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=(output_channels-1), name="read_addr_counter")

				# The reason we need to slightly modify the read address counter increment condition for a relatively obscure reason
				# In fact, it's not obvious that you need it even when running unit tests.
				# There is a scenario where in end-to-end tests, after completing a broadcast and going to idle, early_last_fifo is high for 5 cycles
				# Usually you have data available so you spend 1 cycle in idle and then 1 in ack_ping_pong and then 3 cycles in broadcast 
				# However, if data becomes available within the first three cycles after you exit broadcast_weights, meaning you stay in idle a bit more than 1 cycle,
				# you will still go into broadcast before early_last_fifo goes low (potentially), 
				# and this means that the output_buffer output channel counter will start running ahead of you, because
				# the output channel counter here should be at 3 after early_last_fifo goes low, but there's a small chance it's at 0, 1 or 2 if the activations
				# arrive in this tiny window.
				#To avoid this, we simply make sure there are 3 'valid' cycles of broadcast weights, namely 3 cycles when broadcast weights is high and last_fifo_has_weight 
				# before we start incrementing the read address counter
				if hbm_on:
					restoration_counter = inst(dl.Counter, reset_value = 0, increment_value = 1, end_value=3, name="restoration_counter")
					restoration_counter.set_increment_condition(inst(dl.MuxV2, restoration_counter.is_done, 
								inst(dl.AND, inst(dl.NOT, restoration_counter.is_done), self.pure_broadcast_weights, self.early_last_fifo_has_weight.delay(6)),
								inst(dl.NOT, self.pure_broadcast_weights).delay(1)))
					#Now the restoration counter runs 4 cycles ahead of the tensor_chain_memories r_addr and can count to 2 before read_addr_counter needs to actually increment

					restoration_counter_complete = inst(dl.OR, restoration_counter.is_done, 
											inst(dl.AND, inst(dl.EQ, restoration_counter.current_value, inst(dl.Constant, bits=restoration_counter.current_value._bit_width, value=2)), restoration_counter.increment_condition))
					
					read_addr_counter.set_increment_condition(inst(dl.AND, restoration_counter_complete, reduced_delay_broadcast_weights))
					input_addr_counter.set_increment_condition(inst(dl.AND, 
														restoration_counter_complete.delay(tensor_output_buffer_ram.model_delay+COUNTER_DELAY+WRITE_DATA_DELAY+ADDER_MODULE_DELAY), 
														delayed_broadcast_weights))
				else:
					read_addr_counter.set_increment_condition(reduced_delay_broadcast_weights)
					input_addr_counter.set_increment_condition(delayed_broadcast_weights)

				tensor_output_buffer_ram.r_addr.set_driver(read_addr_counter.current_value.delay(INPUT_ADDR_EXTRA_DELAY+CONTROL_TO_DATA_DELAY))

			else:
				#If the input channel parallelism equals the input channels and the filter is 1x1, we don't need any temporary storage
				for i in range(dl.NX_OUT_WIDTH_PARALLELISM_PER_TENSOR_BLOCK):
					self.outputs[i].set_driver(self.inputs[i].delay(TENSOR_INPUT_DELAY))

				BFLOAT_DELAY = 0
				if enable_bfp:
					BFLOAT_DELAY = 1

				self.ouputs_valid.set_driver(self.broadcast_weights.delay(weight_mem_read_delay+TENSOR_BLOCK_DELAY+TENSOR_INPUT_DELAY+CONTROL_TO_DATA_DELAY+BFLOAT_DELAY))




# Creates the tensor block chains for the TensorConv module 
# We need enough chains to cover an entire output row (each tensor can compute 3 output values if we pre-load activations)
# If we do not want to share the weight buffer will all tensor chains, multiple copies of this module should be instantiated, each with a smaller "output_width" to add up to the whole
# Loading mode can be cascade or side-input/side-channel
class TensorConvChains(dl.Module):
	def __init__(self, input_channels, input_channel_parallelism, input_height, output_width, output_channels, kernel_width, kernel_height, padding, stride, weight_mem_read_delay, CONTROL_TO_DATA_DELAY, weight_ROM_duplication_factor, cascade_loading=True, hbm_on = False, **kwargs):
		super(TensorConvChains, self).__init__("tensor_conv_chains", kind="tensor_conv_chains", **kwargs)
		inst = self.inst
		with dl.ModuleScope(self):
			self.num_tensors_in_chain = math.ceil(input_channel_parallelism/float(dl.NX_ICP_PER_TENSOR_BLOCK))
			self.num_chains = math.ceil(output_width/float(dl.NX_OUT_WIDTH_PARALLELISM_PER_TENSOR_BLOCK))

			if cascade_loading:
				print("Creating TensorConvChains with cascade preloading where num_tensors_in_chain =", self.num_tensors_in_chain, "(+1 block for cascade loading)")
			else:
				print("Creating TensorConvChains with side-input preloading where num_tensors_in_chain =", self.num_tensors_in_chain)

			assert(self.num_tensors_in_chain <= 26) #tensor chains longer than 26 will not route in Quartus

			self.num_roms_to_hook_up = math.ceil(self.num_chains/weight_ROM_duplication_factor)

			TENSOR_INPUT_DATA_DELAY = 1
			global enable_bfp

			#Need to duplicate these depnding on duplication factor
			self.weight_broadcast_wires = [[inst(dl.Logic, bits=(dl.NX_ICP_PER_TENSOR_BLOCK*dl.NX_INPUT_DATA_BITS), name=("weight_broadcast_wire_"+str(j) + "_copy_" +str(i))) for j in range(self.num_tensors_in_chain)] for i in range(self.num_roms_to_hook_up)]
			if enable_bfp: self.shared_exp_broadcast_wire = [[inst(dl.Logic, bits=dl.NX_SHARED_EXP_SIZE, name=("shared_exp_broadcast_wire"+str(j) + "_copy_" +str(i))) for j in range(self.num_tensors_in_chain)] for i in range(self.num_roms_to_hook_up)]
			self.weight_broadcast_signal = [inst(dl.Logic, bits=1, name=("weight_broadcast_signal_copy_"+str(i))) for i in range(self.num_roms_to_hook_up)] #Only last fsm in chain should be hooked up to this
			self.pure_broadcast_weights = [inst(dl.Logic, bits=1, name=("pure_weight_broadcast_signal_copy_"+str(i))) for i in range(self.num_roms_to_hook_up)]
			self.select_ping_pong = [[inst(dl.Logic, bits=1, name=("sel_ping_pong_"+str(j)+"_copy_"+str(i))) for j in range(self.num_tensors_in_chain)] for i in range(self.num_roms_to_hook_up)]

			#Lists of lists of tensors (dl.TensorBlock). First index is chain index, second index is tensor block index for the chain
			self.tensor_block_list = []
			#In case we are using cascade mode, we should store the top of the chain seperately 
			self.tensor_chain_top_list = []
			#List of outputs. This isn't correct right now as we need an accumulate memory at the output
			self.results = []
			self.results_valid = []

			#eventually we are going to want to set this to go high a couple of cycles early depending on the read delay of the weight ROM
			self.next_ping_pong_ready = inst(dl.Constant, bits=1, value=1, name="next_ping_pong_ready")
			temp_const_ping = inst(dl.Constant, bits=2, value=0, name=("temp_const_ping"))

			self.chain_modules = []

			self.last_row = inst(dl.Logic, bits=1, name="last_row")
			self.padding_row = inst(dl.Logic, bits=1, name="first_row")
			self.tab_in_upper_padding = inst(dl.Logic, bits=1, name="tab_in_upper_padding")
			self.early_last_fifo_has_weight = inst(dl.Logic, bits=1, name="tcc_early_last_fifo_has_weight")

			for i in range(self.num_chains):
				self.chain_modules.append(inst(dl.Module, ("tensor_chain_"+str(i)), kind=("tensor_chain_"+str(i))))
				rom_index = math.floor(i/float(weight_ROM_duplication_factor))

				with dl.ModuleScope(self.chain_modules[i]):
					self.tensor_block_list.append([])

					#Un-accumulated results
					temp_results = []
					
					#Need a tensor block at the top of each chain for cascade_loading mode
					if cascade_loading:
						cascade_tensor = inst(dl.TensorBlock, cascade_loading=True, first_tensor=True, external_carry_in=False, enable_reset=False, enable_bfloat=enable_bfp, name=("tensor_chain_" +str(i) + "_block_top"))
						self.tensor_chain_top_list.append(cascade_tensor)
						#temp signals, set them once activation buffers are implemented
						temp_const_input_bus = inst(dl.Constant, bits=80, value=0, name="temp_const_input_bus")
						cascade_tensor.input_bus.set_driver(temp_const_input_bus)
						temp_const_load_buff = inst(dl.Constant, bits=1, value=0, name="temp_const_load_buff")
						cascade_tensor.load_buf_sel.set_driver(temp_const_load_buff)
						cascade_tensor.load_ping_pong_reg.set_driver(temp_const_ping)


					#Instantiate all tensor blocks in chain depending on ICP (each increase of 10 creates a new tensor block in the chain)
					for j in range(self.num_tensors_in_chain):
						cur_tensor = inst(dl.TensorBlock, cascade_loading=cascade_loading, first_tensor=False, external_carry_in=False, enable_reset=False, enable_bfloat=enable_bfp, name=("tensor_chain_" +str(i) + "_block_"+str(j)))

						tensor_input_bus = inst(dl.Logic, bits=80, name=("tensor_input_bus_" + str(j)))
						tensor_input_bus.set_driver(self.weight_broadcast_wires[rom_index][j])
						cur_tensor.input_bus.set_driver(tensor_input_bus.delay(TENSOR_INPUT_DATA_DELAY))

						if enable_bfp:
							tensor_input_shared_exp = inst(dl.Logic, bits=dl.NX_SHARED_EXP_SIZE, name=("tensor_input_shared_exp" + str(j)))
							tensor_input_shared_exp.set_driver(self.shared_exp_broadcast_wire[rom_index][j])
							cur_tensor.shared_exp.set_driver(tensor_input_shared_exp.delay(TENSOR_INPUT_DATA_DELAY))

						self.tensor_block_list[i].append(cur_tensor)

						#Hook up cascade inputs 
						if j > 0:
							self.tensor_block_list[i][j].cascade_data_in.set_driver(self.tensor_block_list[i][j-1].cascade_data_out)

						#Hook up weight cascades if needed
						if cascade_loading:
							if j == 0:
								self.tensor_block_list[i][j].cascade_weight_in.set_driver(self.tensor_chain_top_list[i].cascade_weight_out)
							else:
								self.tensor_block_list[i][j].cascade_weight_in.set_driver(self.tensor_block_list[i][j-1].cascade_weight_out)

						#Hook up results. This isn't correct right now as we need an accumulate memory at the output
						if j == (self.num_tensors_in_chain-1):
							temp_results = self.tensor_block_list[i][j].results

						cur_tensor.load_buf_sel.set_driver(self.select_ping_pong[rom_index][j].delay(CONTROL_TO_DATA_DELAY+TENSOR_INPUT_DATA_DELAY))
						cur_tensor.load_ping_pong_reg.set_driver(temp_const_ping) #this is already delayed in the tensor activation buffer

					#Instantiate output memory for accumulates
					tensor_out_buff = inst(TensorOutputBuffer, input_channel_parallelism=input_channel_parallelism, input_channels=input_channels, 
						output_channels=output_channels, input_height=input_height, kernel_width=kernel_width, kernel_height=kernel_height, padding=padding, stride=stride, weight_mem_read_delay=weight_mem_read_delay, CONTROL_TO_DATA_DELAY=(CONTROL_TO_DATA_DELAY+TENSOR_INPUT_DATA_DELAY), hbm_on = hbm_on)

					tensor_out_buff.last_batch.set_driver(self.last_row)
					tensor_out_buff.padding_batch.set_driver(self.padding_row)
					tensor_out_buff.tab_in_upper_padding.set_driver(self.tab_in_upper_padding)
					for i in range(dl.NX_OUT_WIDTH_PARALLELISM_PER_TENSOR_BLOCK): 
							#Cascade outputs end up being reversed due to the way we are loading in values
						if cascade_loading:
							tensor_out_buff.inputs[i].set_driver(temp_results[-(i+1)])
						else:
							tensor_out_buff.inputs[i].set_driver(temp_results[i])

					tensor_out_buff.broadcast_weights.set_driver(self.weight_broadcast_signal[rom_index])
					tensor_out_buff.early_last_fifo_has_weight.set_driver(self.early_last_fifo_has_weight)
					tensor_out_buff.pure_broadcast_weights.set_driver(self.pure_broadcast_weights[rom_index])
					self.results += tensor_out_buff.outputs
					self.results_valid.append(tensor_out_buff.ouputs_valid)



			self.requires_driver = [val for sublist in self.weight_broadcast_wires for val in sublist]
			return

	#Hooks up the activations after the module has been instantiated
	def connect_activations(self, activation_values, activation_write, iab_read_delay, conversion_delay=0, shared_exp_values=None, cascade_loading=True, **kwargs):
		# This means there is a conversion module before the tensor chains and thus the data is delayed there and we shouldn't delay it again here
		if conversion_delay > 0:
			iab_read_data_delay = 0
		else:
			iab_read_data_delay = iab_read_delay

		with dl.ModuleScope(self):
			#Hook up activation buffers to tensor chains depending on preloading method
			if cascade_loading:
				assert(len(self.tensor_chain_top_list) == len(activation_values))
				assert(len(self.tensor_chain_top_list) == len(self.tensor_block_list))
				for i in range(len(self.tensor_chain_top_list)):
					with dl.ModuleScope(self.chain_modules[i]):
						self.tensor_chain_top_list[i].input_bus.set_driver(activation_values[i].delay(iab_read_data_delay))
						if shared_exp_values != None: self.tensor_chain_top_list[i].shared_exp.set_driver(shared_exp_values[i].delay(iab_read_data_delay))
						self.tensor_chain_top_list[i].load_ping_pong_reg.set_driver(activation_write.delay(iab_read_delay+conversion_delay))
						#Hook up all the other blocks in the chain
						for j in range(len(self.tensor_block_list[i])):
							self.tensor_block_list[i][j].load_ping_pong_reg.set_driver(activation_write.delay(iab_read_delay+conversion_delay))
			else:
				print("ERROR: side channel mode not yet implemented!")
				exit(1)


		return


class TensorConv(NNStage):

	'''
	The tensor convolution module implements regular or depthwise convolutions using the tensor blocks on the Stratix 10 NX.
	In particular, it uses tensor mode to ensure that we can use all 30 multipliers for maximum throughput.
	There are a couple restrictions compared to standard DSP blocks. First, it operates in vectors of ten 8-bit elements, where 3
	of these vectors must be preloaded into a set of ping-pong registers using data-input mode, side-input mode, or cascade mode.
	Once these vectors have been preloaded, the computation can begin, where 1 vector is broadcast and multplied against all 3 
	stored vectors. The ping-pong registers can be used to hide the loading latency; one set of registers is preloaded while the 
	other is used for computations. This is only possible with side-input or cascade preloading, as data-input mode uses the 
	entire input. The multiplication results are added for each vector and can then be accumulated with the previous result or
	the cascade input for a tensor block higher up in the chain (not both in the same cycle). The output is a 25-bit fixed-point
	number. Tensor blocks also support block-floating point, where a 8-bit exponent is shared with 10 vector values. In that case,
	the output is a 24-bit floating-point number. The 3 different preloading methods operate as follows:

	data-input mode: values loaded 10 at a time over 3 cycles using the main data input. Cannot hide loading latency with computations

	side-input mode: values loaded in 2 at a time over 15-cycles using the top 16-bits of the data-input. The bottom 80-bits can 
	be used for computations, thus overlaping the loading latency.

	cascade mode: values loaded 10 at a time over 3 cycles times the number of tensor blocks in the cascade chain. The vectors travel
	down a dedicated wire for preloading, where an extra block at the top of the chain configured to data-input mode is required to
	pass in the vectors from the general programmable routing. All blocks except the top one can hide the loading latency with 
	computations.

	The TensorConv module consists of  a tensor activation buffer, weight ROMs, tensor block chains, and tensor chain output memories.
	The tensor activation buffer takes in the results from the previous layer and stores them in memory, reformatting them into 
	10 element vectors. The weight ROMs pack in the network weights that are broadcast into the tensor blocks. The tensor block chains 
	module instantiates output_width/3 chains with P_i/10 +1 tensor blocks in each chain, where P_i is the input channel parallelism.
	The tensor chain output memories are required if $ P_i $ is not equal to the number of input channels, as each tensor block will 
	output a partial computation that needs to be stored somewhere until the next set of input channels are pre-loaded. 

	Note that the weight ROMs are not a seperate class and are instead a function in this class. All other mentioned modules are
	seperate class files in LayerCompoents above this one.

	'''

	#Class-specific constants
	OUTPUT_ACC_MEM_PRECISION = 20 #Must limit to 20 otherwise m20k only a depth of 512 lines instead of 1024 ad we'll need to double the memory
	TENSOR_BLOCK_STAGGER_DELAY = 2 #Number of cycles to stagger the weights to the next tensor block in the chain
	
	kind = "tensor_conv"
	def get_instance_comment(self, t="  "):
		comment = "Kernel shape: " + str(self.node.get_kernel_shape()) + "\n"
		comment += "Strides: " + str(self.node.get_strides()) + "\n"
		comment += "Number of input channel divisions: " + str(self.node.planner_estimate.n_channel_splits) + "\n"
		comment += "Padding: " + self.node.properties["padding"] + ": " + str(self.padding) + "\n"
		return super(TensorConv, self).get_instance_comment(t=t) + v_comment_string(comment, t)

	def instantiate(self, node, inst, build_data_path=True, enable_bfp_override=False, **kwargs):

		physically_mapped = False
		if "physically_mapped_rams" in kwargs:
			physically_mapped = kwargs["physically_mapped_rams"]

		estimate = self.node.planner_estimate

		parallel_acts_per_ram = node.inputs[0]._from.get_parallel_acts_per_memory()
		input_signed, weights_signed, output_signed = self.get_signs()
		signed = [weights_signed, input_signed]

		input_bits, weight_bits, output_bits = self.get_bit_widths()
		planned_bits = [weight_bits, input_bits]
		
		is_depthwise = node.type == "DepthwiseConv2dNative"
		self.is_depthwise = is_depthwise
		self.node.values_modified = False #We will need to modify the weights exactly once for depthwise convs
		
		input_act_bit_spec = self.get_bit_spec("input")

		global enable_bfp
		if input_act_bit_spec["width"] > 8 or enable_bfp_override:
			enable_bfp = True

		#Prints the type of tensor convolution we are instantiating
		conv_print_string = "regular convolution "
		if is_depthwise:
			conv_print_string = "depthwise convolution "

		if enable_bfp:
			conv_print_string += "in block floating-point mode"

		print("Implementing a " + conv_print_string)

		if is_depthwise and not enable_bfp:
			print("WARNING: Unless using block fp, depthwise convs should map to the regular convolution engine not the tensor conv engine as utilization will be poor.")
			import time
			time.sleep(2) 

		output_act_bit_spec = self.get_bit_spec("output")

		input_channel_parallelism = estimate.n_channel_splits
		input_channel_parallelism = int(math.ceil(input_channel_parallelism/10.0)*10)
		self.input_channel_parallelism = input_channel_parallelism

		#Not sure if we'll be able to support this, or if we'll even need to support this
		#Since our ICP is 10 or more, the tensor block chains should be a lot shorter
		#If we do need to break a chain, it will be pretty complicated; we'll need 2 tensor blocks at the top of the new chain just to pass in the 3 accumulation values
		break_chain_every_n_weights = 57 # Adds a delay of delay_for_chain_break every N weights (for breaking long DSP chains to allow placement)
		delay_for_chain_break = 9    
		OUTPUT_DELAY = 1 
		FSM_TO_WEIGHT_ROM_DELAY = 1

		#Switch between cascade or side-channel loading. We may want to add this as a parameter in the json depeding on the benefits of each mode
		cascade_loading = arch.tensor_block_cascade_loading

		input_channels = self.get_input_act(dim="channels")	
		input_height = self.get_input_act(dim="height")
		input_width = self.get_input_act(dim="width")
		output_width = self.get_output_act(dim="width")
		output_height = self.get_output_act(dim="height")
		self.buffered_layer = True

		if is_depthwise:
			#For depthwise convs input channels equal output channels
			output_channels = self.node.values[0].shape[-2]
			input_channels = self.node.values[0].shape[-2]
		else:
			output_channels = self.node.values[0].shape[-1]
		k_shape = node.get_kernel_shape()
		kernel_height = k_shape[0]
		kernel_width = k_shape[1]
		vertical_stride = node.get_strides()[1]
		horizontal_stride = node.get_strides()[2]

		#To make debugging easier
		print("Kernel Shape =", k_shape, "Input Width =", input_width, "Output Width =", output_width)
		print("ICP =", input_channel_parallelism)
		print("Input Bit spec", input_act_bit_spec, "\nWeight bit spec", self.get_bit_spec("parameters"), "\nOutput bit spec", output_act_bit_spec)	

		# Should have some delay between control path and data path for extra pipelining and improved timing
		# This also digitallogic to create fanout trees for the RAMS/ROMs, as that's where these extra control pipeline registers will go
		# Should be at least 2 for proper fanout trees, 3 if we're using output registers in the RAMs/ROMs
		self.CONTROL_TO_DATA_DELAY = 2 + dl.ENABLE_RAM_OUTPUT_REG_NX

		# For the unit tests
		example_input = node.input_nodes()[0].example_outputs[0]
		self.example_input = example_input
		self.weights = self.node.values[0]
		if node.explicit_padding is not None:
			self.example_input = np.pad(example_input, node.explicit_padding, "constant", constant_values=0)

		input_frac_bits, weight_frac_bits, desired_fractional_bits = self.get_frac_part_widths()
		_, _, output_int_bits = self.get_int_part_widths()
		fractional_bits_in_output = weight_frac_bits + input_frac_bits

		if hasattr(self.node, "offload_to_hbm"):
			offload_to_hbm = self.node.offload_to_hbm
			if offload_to_hbm:
				assert(not enable_bfp) # We have not verified functionality of bfp and HBM together
		else:
			offload_to_hbm = False
		#Define output spec (to_spec) and the spec we are converting from (from_spec) based on the tensor block output 
		SIGN_BITS = 1
		from_spec = {
			"frac" : fractional_bits_in_output,
			"sign" : 1,
			"int" : dl.NX_TENSOR_OUTPUT_PRECISION - SIGN_BITS - fractional_bits_in_output
		}
		to_spec = {
			"frac" : desired_fractional_bits,
			"int" : output_int_bits
		}


		if output_signed:
			to_spec["sign"] = 1
		else:
			to_spec["sign"] = 0

		to_spec['width'] = to_spec["sign"] + to_spec["frac"] + to_spec["int"]

		round_const = -1
		if fractional_bits_in_output > desired_fractional_bits:
			round_const = (fractional_bits_in_output - desired_fractional_bits - 1)

		stride = vertical_stride

		ia_shape = node.inputs[0]._from.example_outputs[0].shape
		if type(node.properties["padding"]) is bytes:
			node.properties["padding"] = node.properties["padding"].decode("ascii")
		padding, _ = get_padding(
			ia_shape,
			k_shape,
			stride,
			node.properties["padding"],
			node.explicit_padding)
		self.padding = padding

		const_0 = inst(dl.Constant, bits=1, value=0, name="const_0")
		const_1 = inst(dl.Constant, bits=1, value=1, name="const_1")
		
		fp_to_bfloat_conversion_delay = 0
		if enable_bfp:
			fp_to_bfloat_conversion_delay = 2

		self.tensor_activation_buffers = inst(TensorActivationBuffer, input_channels=input_channels, input_channel_parallelism=input_channel_parallelism, kernel_width=kernel_width, kernel_height=kernel_height, padding=self.padding[1:3],
			stride=stride, input_width=input_width, input_height=input_height, output_width=output_width, output_height=output_height, cascade_loading=cascade_loading, CONTROL_TO_DATA_DELAY=self.CONTROL_TO_DATA_DELAY, is_depthwise=is_depthwise)

		tensor_conv_chains_ic = input_channels
		# For depthwise convolutions we want IC=ICP so that there is no accumulate memory instantiated in the output buffers
		if is_depthwise:
			tensor_conv_chains_ic = 1

		#Create the tensor block chains
		self.tensor_conv_chains = inst(TensorConvChains, input_channels=tensor_conv_chains_ic, input_channel_parallelism=input_channel_parallelism, input_height=input_height, output_width=output_width, output_channels=output_channels, kernel_width=kernel_width,
			kernel_height=kernel_height, padding=self.padding[2], stride=stride, weight_mem_read_delay=self.tensor_activation_buffers.READ_VAL_DELAY, cascade_loading=cascade_loading, hbm_on = offload_to_hbm, CONTROL_TO_DATA_DELAY=self.CONTROL_TO_DATA_DELAY, weight_ROM_duplication_factor=dl.weight_ROM_duplication_factor)
		self.tensor_conv_chains.last_row.set_driver(self.tensor_activation_buffers.in_lower_padding)
		#self.tensor_conv_chains.padding_row.set_driver(inst(dl.AND, self.tensor_activation_buffers.in_upper_padding.delay(1), self.output_channel_counter.is_reset_value.delay(2)))
		self.tensor_conv_chains.padding_row.set_driver(self.tensor_activation_buffers.in_upper_padding.delay(1))

		self.tensor_conv_chains.tab_in_upper_padding.set_driver(self.tensor_activation_buffers.in_upper_padding.delay(2))

		self.space_to_write_line.set_driver(self.tensor_activation_buffers.space_to_write_line) 

		#Hook up inputs to tensor activation buffer
		if enable_bfp:
			self.fxp_to_fp16_conversion = inst(FixedToFloatingPointConversion, input_width=input_width, fxp_bit_spec=input_act_bit_spec, fp_num_bits=16, output_seperate_mantissa_and_exp=True)

		
		for i in range(len(self.inputs)): 
			if enable_bfp:
				self.fxp_to_fp16_conversion.inputs[i].set_driver(self.inputs[i])
				self.tensor_activation_buffers.inputs[i].set_driver(self.fxp_to_fp16_conversion.signs_and_mantissas[i])
				self.tensor_activation_buffers.fp_exponent_inputs[i].set_driver(self.fxp_to_fp16_conversion.exponents[i])
			else:
				self.tensor_activation_buffers.inputs[i].set_driver(self.inputs[i])


		if enable_bfp:
			#Conversion from fxp to fp16 takes 2 cycles
			self.tensor_activation_buffers.write_en.set_driver(self.write.delay(2))
			self.tensor_activation_buffers.write_en_no_delay.set_driver(self.write)
		else:
			self.tensor_activation_buffers.write_en.set_driver(self.write)

		#If we're using block floating point we need to convert the activation values first (they are in fp16)
		fp_to_bfloat_conversion_delay = 0
		shared_exp_values = None
		if enable_bfp:
			shared_exp_values = []
			activation_values = []			
			#Will need a loop here
			for i in range(len(self.tensor_activation_buffers.output_data)):
				fp16_to_block_fp_conversion = inst(FloatingPointToBfloat16, fp_num_bits=16, input_is_bus=True)
				# Note that inputs are delayed here (since the conversion function itself doesn't register them) and they are no longer delayed before the tensor chain (conversion function ooutput is registered)
				fp16_to_block_fp_conversion.input_mantissa_bus.set_driver(self.tensor_activation_buffers.output_data[i].delay(self.tensor_activation_buffers.READ_VAL_DELAY))
				fp16_to_block_fp_conversion.input_exp_bus.set_driver(self.tensor_activation_buffers.output_exp_data[i].delay(self.tensor_activation_buffers.READ_VAL_DELAY))
				activation_values.append(fp16_to_block_fp_conversion.output_mantissa_bus)
				shared_exp_values.append(fp16_to_block_fp_conversion.output_shared_exp)
				fp_to_bfloat_conversion_delay = 2

		else:
			activation_values = self.tensor_activation_buffers.output_data

		#Hook up activation buffer signals to tensor chains
		self.tensor_conv_chains.connect_activations(activation_values=activation_values, activation_write=self.tensor_activation_buffers.output_write, iab_read_delay=(self.tensor_activation_buffers.READ_VAL_DELAY), 
			cascade_loading=cascade_loading, conversion_delay=fp_to_bfloat_conversion_delay, shared_exp_values=shared_exp_values, **kwargs)

		#Will have to instantiate multiple memories depending on ICP and how many tensor chains we want to share values with
		#We'll split it into multiple modules to make it less confusing	
		chain_depth = int(input_channel_parallelism/dl.NX_ICP_PER_TENSOR_BLOCK)
		module_list = []
		hbm_distribution_fifos = [[] for i in range(self.tensor_conv_chains.num_roms_to_hook_up)] # Won't be used unless HBM is turned on
		last_fifo_has_weight = inst(dl.Logic, bits=1, name="last_fifo_has_weight")
		early_last_fifo_has_weight = inst(dl.Logic, bits=1, name="early_last_fifo_has_weight")
		self.tensor_conv_chains.early_last_fifo_has_weight.set_driver(early_last_fifo_has_weight)


		#Define the signals that won't be used unless HBM is enabled
		self.hbm_interface = []
		self.hbm_binary_weights = []
		self.hbm_line_count = []
		
		for cur_rom_for_chains in range(math.ceil(self.tensor_conv_chains.num_roms_to_hook_up/dl.weight_FSM_duplication_factor)):		
			#Calculate whether or not we need to create a new set of ROMs and FSMs and if so, create a new module to put these in
			if len(module_list) < (cur_rom_for_chains+1):
				module_list.append(inst(dl.Module, ("tensor_chain_memories_instance_"+str(cur_rom_for_chains)), kind=("tensor_chain_memories_instance_"+str(cur_rom_for_chains))))

			weight_memory_fsms = []
			weight_memory_fsms_broadcast = []
			weight_memory_fsms_early_broadcast = []
			weight_memory_fsms_pure_broadcast = []
			tensor_conv_enable = []

			with dl.ModuleScope(module_list[cur_rom_for_chains]):
				for cur_tensor_num in range(0, chain_depth):
					with dl.ModuleScope(inst(dl.Module, ("tensor_chain_memories_block_"+str(cur_tensor_num)), kind=("tensor_chain_memories_block_"+str(cur_tensor_num)))):
						VALS_PER_LINE = 5

						r_en_roms = [None for i in range(dl.weight_FSM_duplication_factor)]
						address_roms = [None for i in range(dl.weight_FSM_duplication_factor)]
						data_val_roms = [None for i in range(dl.weight_FSM_duplication_factor)]
						shared_exp_read_val = [None for i in range(dl.weight_FSM_duplication_factor)] # Won't be used unless shared exponent is enabled

						#We may want to share the FSM between multiple ROMs to reduce area
						for cur_rom_num in range(dl.weight_FSM_duplication_factor):
							tensor_chain_index = cur_rom_for_chains*dl.weight_FSM_duplication_factor + cur_rom_num
							if tensor_chain_index >= len(self.tensor_conv_chains.weight_broadcast_wires): break
							if (tensor_chain_index != 0 and offload_to_hbm): break

							r_en_roms[cur_rom_num], address_roms[cur_rom_num], data_val_roms[cur_rom_num], rom_depth, shared_exp_read_val[cur_rom_num] = self.create_weight_memory(self.CONTROL_TO_DATA_DELAY, self.hbm_binary_weights, self.hbm_line_count, cur_tensor_num+1,chain_depth, VALS_PER_LINE=VALS_PER_LINE, offload_to_hbm=offload_to_hbm)
						

						num_batches = 1
						if self.is_depthwise:
							# For depthwise convolutions we want to do the calculation in batches of size 10 to match the memory layout
							# This is because we don't accumulate across the entire depth, instead we accumulate one channel at a time
							oc_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=(dl.NX_ICP_PER_TENSOR_BLOCK-1), name="tensor_chain_memory_oc_counter")
							# Need an extra counter for the last set of weights which may not be a multiple of 10
							last_batch_counter_value = input_channels%dl.NX_ICP_PER_TENSOR_BLOCK
							if last_batch_counter_value == 0: last_batch_counter_value = dl.NX_ICP_PER_TENSOR_BLOCK
							oc_counter_last_batch = inst(dl.Counter, reset_value=0, increment_value=1, end_value=(last_batch_counter_value-1), name="oc_counter_last_batch")
							num_batches = math.ceil(input_channels/dl.NX_ICP_PER_TENSOR_BLOCK)
							batch_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=(num_batches-1), name="batch_counter")
						else:
							oc_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=(output_channels-1), name="tensor_chain_memory_oc_counter")
						

						assert(rom_depth % (output_channels*kernel_height*kernel_width) == 0) #Make sure that the smaller counter above splits the bigger one evenly 

						# FSM for the memory
						# It will wait for the ping pong buffer to be ready before broadcasting weights. It will use one of the broadcast cycles to send an ACK back to the ping-pong fsm so it can start loading the next one
						# For tensor blocks further down the chain it will also implement a delay before sending in the weights (values from tensor block above need some time to go down the chain)
						addr_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=(rom_depth-1), load_value=(not offload_to_hbm), name="tensor_chain_memory_addr_counter")
						
						broadcast_finish_condition = oc_counter.done_and_incrementing
						if self.is_depthwise:
							broadcast_finish_condition = inst(dl.OR, oc_counter.is_done, oc_counter_last_batch.done_and_incrementing)

						next_ping_pong_ready = self.tensor_activation_buffers.load_act_fsm.c["next_ping_pong_ready"].delay(1)
						if enable_bfp:
							#Need to AND this signal with its less delayed version to prevent a corner case where we only broadcast 1 output channel and then return back to IDLE
							#In that case the signal would not get a chance to go low again and would immediately start broadcasting the next set before preloading is finished
							next_ping_pong_ready = inst(dl.AND, self.tensor_activation_buffers.load_act_fsm.c["next_ping_pong_ready"].delay(fp_to_bfloat_conversion_delay), self.tensor_activation_buffers.load_act_fsm.c["next_ping_pong_ready"].delay(1))

						
						allow_state_transitions = last_fifo_has_weight
						states = ["IDLE", "ACK_PING_PONG", "BROADCAST_WEIGHTS"]
						if offload_to_hbm:
							edges = [["IDLE", "ACK_PING_PONG", inst(dl.AND, next_ping_pong_ready, allow_state_transitions, self.can_write_line.delay(max(1,fp_to_bfloat_conversion_delay)))], #delay here for timing
									["ACK_PING_PONG", "BROADCAST_WEIGHTS", allow_state_transitions],
									["BROADCAST_WEIGHTS", "IDLE", broadcast_finish_condition]] 
						else:
							edges = [["IDLE", "ACK_PING_PONG", inst(dl.AND, next_ping_pong_ready, self.can_write_line.delay(max(1,fp_to_bfloat_conversion_delay)))], #delay here for timing
									["ACK_PING_PONG", "BROADCAST_WEIGHTS"],
									["BROADCAST_WEIGHTS", "IDLE", broadcast_finish_condition]] 							
						control = {"IDLE" : {"ack_next_ping_pong" : 0},
								"ACK_PING_PONG" : {"ack_next_ping_pong" : 1},
								"BROADCAST_WEIGHTS" : {"ack_next_ping_pong" : 0} }

						#If our chain depth is greater than 1 we need to stagger the weights 
						#The logic for this is a bit confusing as we want to overlap as much of the computation as possible, even if the bottom tensor in the chain is still processing the previous results
						wait_counter_created = False
						if chain_depth > 1 and cur_tensor_num >= 1 and self.TENSOR_BLOCK_STAGGER_DELAY != 0:

							# Need to look at previous tensor in chain to see if it's in the broadcast state (or ack for a delay of just 1). If so, we should start our stagger delay 
							# This prevents a bunch of corner cases that occur if we make each fsm completey independent (depending when next_ping_pong_ready arrives, so FSMs may end up being further behind)
							if offload_to_hbm:
								if self.TENSOR_BLOCK_STAGGER_DELAY == 1:
									edges[0] = ["IDLE", "ACK_PING_PONG", inst(dl.AND, allow_state_transitions, weight_memory_fsms[-1].c["is_ack_ping_pong"], name=("previous_fsm_"+str(cur_tensor_num-1) + "_is_ack"))]
								else:
									edges[0] = ["IDLE", "ACK_PING_PONG", inst(dl.AND, allow_state_transitions, weight_memory_fsms[-1].c["is_broadcast_weights"], name=("previous_fsm_"+str(cur_tensor_num-1) + "_is_broadcast_weights"))]
							else:
								if self.TENSOR_BLOCK_STAGGER_DELAY == 1:
									edges[0] = ["IDLE", "ACK_PING_PONG", inst(dl.AND,weight_memory_fsms[-1].c["is_ack_ping_pong"], name=("previous_fsm_"+str(cur_tensor_num-1) + "_is_ack"))]
								else:
									edges[0] = ["IDLE", "ACK_PING_PONG", inst(dl.AND, weight_memory_fsms[-1].c["is_broadcast_weights"], name=("previous_fsm_"+str(cur_tensor_num-1) + "_is_broadcast_weights"))]
															
							# For delays greater than or equal to 3 cycles we will need an extra wait state
							if self.TENSOR_BLOCK_STAGGER_DELAY >= 3:
								states.append("WAIT")
								edges[1] = ["ACK_PING_PONG", "WAIT"]

								#Only need a counter for larger delays; the wait state and moving the broadcast signal (further down) will give us 3 cycles already (loading the weights also takes 1 cycle)
								if self.TENSOR_BLOCK_STAGGER_DELAY > 3:
									wait_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=(self.TENSOR_BLOCK_STAGGER_DELAY-3), name="stagger_wait_counter")
									edges.append(["WAIT", "BROADCAST_WEIGHTS", wait_counter.is_done])
									wait_counter_created = True
								elif self.TENSOR_BLOCK_STAGGER_DELAY == 3:
									edges.append(["WAIT", "BROADCAST_WEIGHTS"])

								control["WAIT"] = {"ack_next_ping_pong" : 0}

							# Need to remove the broadcast signal during ACK
							# Benefit is that it give us a 1 cycle delay meaning for staggers of 2 we don't need an extra state (and for staggers of 3 we don't need a counter)
							control["ACK_PING_PONG"] = {"ack_next_ping_pong" : 1}

						weight_memory_fsms.append(inst(dl.StateMachine, states, edges, control, name=("weight_memory_fsm_" + str(cur_tensor_num))))
						#If we don't use HBM, the weights are always available, so we can broadcast at any time
						#If we do use HBM, we need to make sure the FIFOs have weights in them
						if not offload_to_hbm:
							weight_memory_fsms_broadcast.append(weight_memory_fsms[-1].c["is_broadcast_weights"])
						else:
							weight_memory_fsms_broadcast.append(inst(dl.AND, weight_memory_fsms[-1].c["is_broadcast_weights"], last_fifo_has_weight))
							weight_memory_fsms_early_broadcast.append(inst(dl.AND, weight_memory_fsms[-1].c["is_broadcast_weights"], early_last_fifo_has_weight.delay(2)))
							weight_memory_fsms_pure_broadcast.append(weight_memory_fsms[-1].c["is_broadcast_weights"])													
							
							tensor_conv_enable.append(last_fifo_has_weight)
						#Ping pong must start at 1 so first value it gets set to is 0
						cur_ping_pong = inst(dl.Flop, reset_driver=1, bits=1, name="cur_ping_pong")
						if offload_to_hbm:
							cur_ping_pong.set_driver(inst(dl.MuxV2, inst(dl.AND, weight_memory_fsms[cur_tensor_num].c["ack_next_ping_pong"], allow_state_transitions), cur_ping_pong, inst(dl.NOT, cur_ping_pong), name="next_ping_pong"))

							#The last fsm in the chain needs to send the ack to signal that the next ping pong buffer can be loaded. Otherwise tensors lower down the chain might not be done yet b/c of the delay
							if cur_tensor_num == (chain_depth-1):
								self.tensor_activation_buffers.ack_next_ping_pong.set_driver(inst(dl.AND, weight_memory_fsms[cur_tensor_num].c["ack_next_ping_pong"], last_fifo_has_weight))
						else:
							cur_ping_pong.set_driver(inst(dl.MuxV2, weight_memory_fsms[cur_tensor_num].c["ack_next_ping_pong"], cur_ping_pong, inst(dl.NOT, cur_ping_pong), name="next_ping_pong"))

							#The last fsm in the chain needs to send the ack to signal that the next ping pong buffer can be loaded. Otherwise tensors lower down the chain might not be done yet b/c of the delay
							if cur_tensor_num == (chain_depth-1):
								self.tensor_activation_buffers.ack_next_ping_pong.set_driver(weight_memory_fsms[cur_tensor_num].c["ack_next_ping_pong"])
						

						if self.is_depthwise:
							oc_counter.set_increment_condition(inst(dl.AND, weight_memory_fsms_broadcast[cur_tensor_num], inst(dl.NOT, batch_counter.is_done), name="increment_oc_counter"))
							oc_counter_last_batch.set_increment_condition(inst(dl.AND, weight_memory_fsms_broadcast[cur_tensor_num], batch_counter.is_done, name="increment_oc_counter_last_batch"))
							batch_counter.set_increment_condition(inst(dl.OR, oc_counter.is_done, oc_counter_last_batch.done_and_incrementing))
						else:
							oc_counter.set_increment_condition(weight_memory_fsms_broadcast[cur_tensor_num])

						if self.is_depthwise:
							ic_per_rom = dl.NX_ICP_PER_TENSOR_BLOCK
						else:
							ic_per_rom = math.ceil(input_channels/input_channel_parallelism)*dl.NX_ICP_PER_TENSOR_BLOCK

						if not offload_to_hbm:
							#For upper and lower padding, as the filter moves down the input activations, the number of padding line changes, along with other variables
							#We identify all possible constants and select the correct constant through a mux whose select signal is an upper/lower padding counter (that counts to max number of padding lines)
							#A simple example, would be in a 11x11 filter with 5 lines of padding on each side, and stride 2. 
							#Number of padding lines = mux select between 5, 3 and 1.
							#Other padding-dependent variables might have similar muxing structures
							# If we have upper padding, we need to start the filters at a different value (i.e. the second row for a 3x3 filter with 1 row of padding)
							if self.padding[2][0] != 0:
								output_lines_that_require_upper_padding = math.ceil(self.padding[2][0]/stride)
								upper_padding_amounts = [self.padding[2][0] - i*stride for i in range(output_lines_that_require_upper_padding)]
								upper_padding_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=math.ceil(output_lines_that_require_upper_padding-1), name="upper_padding_counter")
								upper_padding_counter.set_increment_condition(inst(dl.AND, self.tensor_activation_buffers.read_counter_completely_done, self.tensor_activation_buffers.in_upper_padding))

								if is_depthwise: addresses_for_padding = [upper_padding_amounts[i] * output_channels * kernel_width for i in range(output_lines_that_require_upper_padding)]
								else: addresses_for_padding = [upper_padding_amounts[i] * output_channels * kernel_width * math.ceil(ic_per_rom/(VALS_PER_LINE*2)) for i in range(output_lines_that_require_upper_padding)]

								assert(addresses_for_padding[0] < rom_depth) #The first element will always be the biggest, no need to assert for the other ones
								addr_counter_load_constants = [inst(dl.Constant, bits=clog2(rom_depth), value=(addresses_for_padding[i]), name="addr_counter_load_const"+str(i)) for i in range(output_lines_that_require_upper_padding)]
								addr_counter_load_value = inst(dl.MuxV2, upper_padding_counter.current_value, *addr_counter_load_constants, name="addr_counter_load_value")
								
								#In a normal scenario, we allow for the addr_counter to take the padding address after the main load if:
									# (Addr counter is 0 obviously) AND
									# (     We are at the start of an image: Line counter = 0, channel counter = 0 OR we are at the end of an image (line counter = end value, channel counter > 0 which means we started counting) )
								#When OC<20 (or OC is low in general), addr_counter updates rapidly every iteration, and manages to catch the loading signal again after line 1, which makes it load the padding addr for line 2
								#In order to avoid loading the padding address for line 2, we change the equation to the following:
									# (Addr counter == 0 AND Output_line_counter == end_value ) OR
									# (Addr counter == 0 (delay by 4) and channel counter == 0 and line counter == 0) AND fsm_not_in_broadcast state
									# 
									# Delay the address by 4 to slow it down, but now that we did delay the address, we need to make sure we don't overlap with the broadcast state	
								if output_channels < 20 and rom_depth < 150 :
									#If the rom depth is high enough this shouldn't have time to happen. 150 is just a reasonable estimate
									# Delay the address by 4 as sometimes the output line and channel counter hasn't updated yet (especially for smaller numbers of OCs). But now that we did delay the address, we need to make sure we don't overlap with the broadcast state
									output_line_and_channel_done_or_reset = self.tensor_activation_buffers.in_upper_padding.delay(1)
									addr_counter.enable_load_value.set_driver(inst(dl.OR, inst(dl.AND, output_line_and_channel_done_or_reset, inst(dl.NOT, weight_memory_fsms_broadcast[cur_tensor_num]), addr_counter.is_reset_value.delay(4)), inst(dl.AND, self.output_lines_counter.is_done.delay(1), addr_counter.is_reset_value) ,name='addr_counter_enable_load_value'))
								else:
									output_line_and_channel_done_or_reset = inst(dl.OR, self.tensor_activation_buffers.in_upper_padding.delay(1), 
										inst(dl.AND, self.output_lines_counter.is_done.delay(1), inst(dl.NOT, self.output_channel_counter.is_reset_value.delay(1))), name="output_line_and_channel_done_or_reset")			

									addr_counter.enable_load_value.set_driver(inst(dl.AND, addr_counter.is_reset_value, output_line_and_channel_done_or_reset, name="addr_counter_enable_load_value"))


								addr_counter.load_value.set_driver(addr_counter_load_value)
							else:
								const_0_1bit = inst(dl.Constant, bits=1, value=0, name="const_0_1bit")
								const_0_addr = inst(dl.Constant, bits=clog2(rom_depth), value=0, name="const_0_addr")
								addr_counter.enable_load_value.set_driver(const_0_1bit)
								addr_counter.load_value.set_driver(const_0_addr)


							addr_counter.set_increment_condition(weight_memory_fsms_broadcast[cur_tensor_num])

							#Need to make sure we reset after an image is finished
							if self.padding[2][1] != 0:
								output_lines_that_require_lower_padding = math.ceil(self.padding[2][1]/stride)
								in_lower_padding = inst(dl.GT, self.output_lines_counter.current_value, inst(dl.Constant, bits=clog2(output_height), value=(output_height-1-output_lines_that_require_lower_padding)), name="in_lower_padding")
								lower_padding_amounts = [self.padding[2][1] - i*stride for i in range(output_lines_that_require_lower_padding)]
								last_line_rows = [kernel_height - lower_padding_amounts[i] for i in range(output_lines_that_require_lower_padding)]
							else:
								last_line_rows = [kernel_height]
								in_lower_padding = inst(dl.Constant, bits = 1, value = 0, name="in_lower_padding")
							
							image_finished = inst(dl.Flop, reset_driver=0, bits=1, name="image_finished")

							padding_row_finished_const_vals = [int((kernel_width*last_line_rows[i]*output_channels*math.ceil(ic_per_rom/(VALS_PER_LINE*2)))) for i in range(len(last_line_rows))]
							padding_row_finished_eq_conditions = [inst(dl.EQ, addr_counter.current_value, inst(dl.Constant, bits=clog2(rom_depth), value=padding_row_finished_const_vals[i], name="reset_cond_addr_const"+str(i))) for i in range(len(last_line_rows))]
							if len(padding_row_finished_eq_conditions) > 1:
								#We should probably be muxing them instead of ORing them, but turthfully I don't think there's a layer yet where there are two lines of lower padding
								padding_row_eq_combined = inst(dl.OR, *padding_row_finished_eq_conditions, name="reset_cond_addr_const_combined")
							else:
								padding_row_eq_combined = padding_row_finished_eq_conditions[0]
							image_finished_reset = inst(dl.AND, image_finished, padding_row_eq_combined, name="image_finished_reset")
							addr_counter.update_reset_condition(image_finished_reset)
							image_finished.set_driver(inst(dl.MuxV2, inst(dl.AND, self.tensor_activation_buffers.read_counter_completely_done, in_lower_padding, name="last_lines_addr_select"), 
								inst(dl.MuxV2, image_finished_reset, image_finished, inst(dl.Constant, bits=1, value=0, name="const_zero")), 
								inst(dl.Constant, bits=1, value=1, name="const_one"), name="image_finished_driver"))

							if(cur_rom_for_chains == 0 and cur_tensor_num == 0):
								const_one_weight_available = inst(dl.Logic, bits=1, name="weight_available")
								const_one_weight_available.set_driver(inst(dl.Constant, bits=1, value=1, name="const_one_weight_available"))
								self.tensor_activation_buffers.weight_available.set_driver(const_one_weight_available) #Setting a constant directly doesn't make it propagate through the different files
								const_no_freeze = inst(dl.Logic, bits=1, name="no_freeze_const")
								const_no_freeze.set_driver(inst(dl.Constant, bits=1, value=1, name="no_freeze_constant"))
								self.tensor_activation_buffers.no_freeze.set_driver(const_no_freeze)
						elif cur_rom_for_chains == 0:
							tensor_conv_will_freeze = self.create_hbm_read_logic(inst, kwargs["clock"], rom_depth, output_height, stride, kernel_height, kernel_width, output_channels, ic_per_rom, chain_depth, cur_tensor_num, hbm_distribution_fifos, oc_counter, early_last_fifo_has_weight, last_fifo_has_weight, VALS_PER_LINE, FSM_TO_WEIGHT_ROM_DELAY)
								


						if wait_counter_created: wait_counter.set_increment_condition(weight_memory_fsms[cur_tensor_num].c["is_wait"])

						for cur_rom_num in range(dl.weight_FSM_duplication_factor):
							tensor_chain_index = cur_rom_for_chains*dl.weight_FSM_duplication_factor + cur_rom_num
							if tensor_chain_index >= len(self.tensor_conv_chains.weight_broadcast_wires): break

							if not offload_to_hbm:
								address_roms[cur_rom_num].set_driver(addr_counter.current_value.delay(FSM_TO_WEIGHT_ROM_DELAY))
								r_en_roms[cur_rom_num].set_driver(weight_memory_fsms_broadcast[cur_tensor_num].delay(FSM_TO_WEIGHT_ROM_DELAY))

								self.tensor_conv_chains.weight_broadcast_wires[tensor_chain_index][cur_tensor_num].set_driver(data_val_roms[cur_rom_num])
								if enable_bfp: self.tensor_conv_chains.shared_exp_broadcast_wire[tensor_chain_index][cur_tensor_num].set_driver(shared_exp_read_val[cur_rom_num])
								self.tensor_conv_chains.select_ping_pong[tensor_chain_index][cur_tensor_num].set_driver(cur_ping_pong.delay(FSM_TO_WEIGHT_ROM_DELAY))
							else:
								hbm_distribution_fifos[tensor_chain_index][cur_tensor_num].r_en.set_driver(weight_memory_fsms_broadcast[cur_tensor_num].delay(FSM_TO_WEIGHT_ROM_DELAY + self.CONTROL_TO_DATA_DELAY))
								self.tensor_conv_chains.weight_broadcast_wires[tensor_chain_index][cur_tensor_num].set_driver(hbm_distribution_fifos[tensor_chain_index][cur_tensor_num].r_data)
								self.tensor_conv_chains.select_ping_pong[tensor_chain_index][cur_tensor_num].set_driver(cur_ping_pong.delay(FSM_TO_WEIGHT_ROM_DELAY))								


				for cur_rom_num in range(dl.weight_FSM_duplication_factor):
					tensor_chain_index = cur_rom_for_chains*dl.weight_FSM_duplication_factor + cur_rom_num
					if tensor_chain_index >= len(self.tensor_conv_chains.weight_broadcast_signal): break
					if not offload_to_hbm:
						self.tensor_conv_chains.weight_broadcast_signal[tensor_chain_index].set_driver(weight_memory_fsms_broadcast[-1].delay(FSM_TO_WEIGHT_ROM_DELAY))
					else:
						self.tensor_conv_chains.weight_broadcast_signal[tensor_chain_index].set_driver(weight_memory_fsms_early_broadcast[-1].delay(FSM_TO_WEIGHT_ROM_DELAY))
						self.tensor_conv_chains.pure_broadcast_weights[tensor_chain_index].set_driver(weight_memory_fsms_pure_broadcast[-1].delay(FSM_TO_WEIGHT_ROM_DELAY))

				#We connect the tensor chains to enable signals if HBM is enabled (we turn off enable if we run out of weights)
				if offload_to_hbm:
					#How many tensor chains share the same FSM
					fsm_to_chain_ratio = dl.weight_ROM_duplication_factor * dl.weight_FSM_duplication_factor

					for local_tensor_idx in range(fsm_to_chain_ratio):
						#We compute the global index of the chain targeted by the current FSM
						global_tensor_idx = local_tensor_idx + fsm_to_chain_ratio*cur_rom_for_chains
						if global_tensor_idx < len(self.tensor_conv_chains.tensor_block_list):
							#We connect each tensor block in the chain to the right enable signal
							for tensor_idx_in_chain in range(len(self.tensor_conv_chains.tensor_block_list[0])):
								self.tensor_conv_chains.tensor_block_list[global_tensor_idx][tensor_idx_in_chain].enable_clock.set_driver((tensor_conv_enable[tensor_idx_in_chain]).delay(FSM_TO_WEIGHT_ROM_DELAY + self.CONTROL_TO_DATA_DELAY + 2))
							#We connect the top block (used to cascade activations) to the same enable signal as the first block
							self.tensor_conv_chains.tensor_chain_top_list[global_tensor_idx].enable_clock.set_driver((tensor_conv_enable[0]).delay(FSM_TO_WEIGHT_ROM_DELAY + self.CONTROL_TO_DATA_DELAY + 2))

					if (cur_rom_for_chains == 0):
						self.tensor_activation_buffers.no_freeze.set_driver(tensor_conv_will_freeze.delay(4))
						

		
		EXTRA_OUTPUT_DELAY = 0
		if enable_bfp:
			EXTRA_OUTPUT_DELAY = 3
			cast_output_module = inst(FpToFxpConverter, output_width=len(self.outputs), fxp_bit_spec=to_spec, fp_num_bits=24)

		#Hook up output and output valid signals for data
		for i,(o,ov) in enumerate(zip(self.outputs, self.outputs_valid)):
			ov.set_driver(self.tensor_conv_chains.results_valid[math.floor(i/dl.NX_OUT_WIDTH_PARALLELISM_PER_TENSOR_BLOCK)].delay(OUTPUT_DELAY+EXTRA_OUTPUT_DELAY))
	
			if enable_bfp:
				cast_output_module.inputs[i].set_driver(self.tensor_conv_chains.results[i])
				cast_output = cast_output_module.rounded_outputs[i]
			else:
				cast_output = cast_fixed(self.tensor_conv_chains.results[i], from_spec, to_spec, scale = self.node.precision_parameters["scale"])
			o.set_driver(cast_output.delay(OUTPUT_DELAY))

		#Control output valid signals and delays
		self.output_valid.set_driver(self.tensor_conv_chains.results_valid[0])
		self._set_control_to_input_delay(self.CONTROL_TO_DATA_DELAY) #Delay inbetween when the control signal arrives and when the input arrives. Must be at least 1
		self._set_control_valid_to_output_valid_delay(1+EXTRA_OUTPUT_DELAY) #Delay inbetween when the control output signal goes valid and the data output signal goes valid
		self.cred_inc.set_driver(inst(dl.AND, self.output_valid.delay(1), self.output_channel_counter.is_reset_value))
		#		self.cred_inc.set_driver(self.tensor_activation_buffers.read_counter_completely_done.delay(1))
		self.cred_dec.set_driver(self.tensor_activation_buffers.cred_dec.delay(2))
		# self.cred_dec_final.set_driver(self.tensor_activation_buffers.cred_dec_final.delay(2))
		self.cred_dec_amount = self.tensor_activation_buffers.cred_dec_amount
		self.cred_dec_final_amount = self.tensor_activation_buffers.cred_dec_final_amount
		if node.inputs[0]._from.module is not None:
			self.cred_max[node.inputs[0]._from.module.name] = self.tensor_activation_buffers.cred_max
		else:
			self.cred_max["None"] = self.tensor_activation_buffers.cred_max

		hold_final_dec = inst(dl.Flop, reset_driver=0, bits=1, name="hold_final_dec")
		hold_final_dec_set = inst(dl.Logic, bits=1, name="hold_final_dec_set")
		hold_final_dec_unset = inst(dl.Logic, bits=1, name="hold_final_dec_unset")

		hold_final_dec_set.set_driver(self.tensor_activation_buffers.cred_dec_final.delay(2))
		hold_final_dec_unset.set_driver(inst(dl.AND, hold_final_dec, inst(dl.AND, self.output_valid, inst(dl.NOT, self.output_valid.delay(1))).delay(2)))
		hold_final_dec.set_driver(inst(dl.OR, inst(dl.AND, hold_final_dec, inst(dl.NOT, hold_final_dec_unset)), hold_final_dec_set))
		self.cred_dec_final.set_driver(hold_final_dec_unset.delay(1))

	#Converts a set of values from fixed point to a custom fp16 format (1-bit sign, 8-bit exponent, 7-bit mantissa)
	def np_convert_fxp_to_fp16(self, weight_values):
		MANTISSA_BITS = 7
		IEEE_EXPONENT_OFFSET = 127
		weight_bit_spec = self.get_bit_spec("parameters")

		# Extract the signs
		signs = weight_values.copy()
		signs[signs > 0] = 1
		signs[signs < 0] = -1
		
		mantissas = np.absolute(weight_values)

		# Calculate exponent assuming no fractional bits
		exponents = np.floor(np.log2(mantissas))

		#Calculate shifting amounts for the mantissa based on the number of bits
		#Need to shift mantissas in 2 stages since apparently the right shift function doesn't support negative numbers
		mantissa_right_shift_amount = exponents - MANTISSA_BITS
		mantissa_right_shift_amount[mantissa_right_shift_amount == -np.inf] = 0

		mantissa_left_shift_amount = mantissa_right_shift_amount.copy()
		mantissa_left_shift_amount[mantissa_left_shift_amount > 0] = 0
		mantissa_left_shift_amount = np.abs(mantissa_left_shift_amount)

		mantissa_right_shift_amount[mantissa_right_shift_amount < 0] = 0

		# Subtract implicit ones from mantissa then shift with values above
		implicit_ones = np.power(2, exponents).astype(int)
		mantissas = mantissas - implicit_ones
		
		mantissas = mantissas.astype(int) >> mantissa_right_shift_amount.astype(int)
		mantissas = mantissas << mantissa_left_shift_amount.astype(int)
		
		#Now adjust the offset for the fractional bits and the IEEE exponent offset
		exponents += IEEE_EXPONENT_OFFSET - weight_bit_spec['frac']
		# Take care of all the numbers that are zero
		exponents[exponents == -np.inf] = 0
		exponents = exponents.astype(int)


		#debug (convert back to floating point and compare)
		#converted_values = ((signs * mantissas).astype(float) + (2**7)*signs) * np.power(2.0, exponents - 127) * (2.0**-7) 
		#print("converted_values", converted_values)
		#print("fxp_vals", fxp_vals)


		return mantissas, exponents, signs

	# Converts list of mantissas, exponents and signs to the tensor block Bfloat format
	# For this we re-introduce the implcit 1, convert to 2s complement, and crop down to 8-bits
	# The shared exponent must have an extra offset of -6
	def convert_fp16_to_bfloat_rom(self, mantissas, exponents, signs):
		shared_exp = max(exponents)
		shared_exp_shift_amount = shared_exp - np.array(exponents)
		shared_exp -= 6 # Tensor block offset from spec
		if shared_exp < 0: shared_exp = 0
	
		mantissas = np.array(mantissas) #convert to numpy so we don't have to use loops
		#First add back in implicit 1
		mantissas = mantissas + (2**7)
		# Convert to 2s complement based on sign
		mantissas = mantissas * signs
		# Shift to align the shared exponents
		# Also Remove the bottom bit so numbers are in an 8-bit range
		mantissas = mantissas >> (1 + shared_exp_shift_amount)

		mantissas = mantissas.tolist()

		return mantissas, shared_exp

	# Helper function that converts a list to a int64 binary number for wider ROM reads
	def list_to_int64_bin(self, input_list):
		int64_val = 0
		input_list.reverse()
		for val in input_list:
			int64_val = int64_val << dl.NX_INPUT_DATA_BITS
			int64_val = int64_val | (val & 0xff)

		return int64_val


	#:  Creates a weight memory to feed into the tensor block. Since tensor mode doesn't support sparsity, this is a lot simpler than the regular conv module
	#:  Unlike vector mode or regular DSPs, an identical 10 element vector of weights is broadcast to each tensor block chain
	#:  Because of this, we can pack weights into M20Ks much more efficiently and actually save space despite not supporting sparsity
	#:  The Downside of this is that the memories have a high fanout (have to parallelise over entire output width)
	#:  Thus to improve timing we may have to duplicate them when we have many chains (we are helped by the fact that there are less chains than vector mode since each tensor block computes 3 output values across width)
	#:  The format of the each M20K is something like the following for a 1x1x32x64 kernel with 8-bit weights (e.g. w0-1 means the first weight and the second channel) 
	#:  	=========================================
	#:  	| w0-4  | w0-3  | w0-2  | w0-1  | w0-0  |
	#:  	=========================================
	#:  	| w1-4  | w1-3  | w1-2  | w1-1  | w1-0  |
	#:                       ...
	#:      =========================================
	#:	    | w63-4 | w63-3 | w63-2 | w63-1 | w63-0 |
	#:  	=========================================
	#:  	| w0-14 | w0-13 | w0-12 | w0-11 | w0-10 |
	#:  	=========================================
	#:  	| w1-14 | w1-13 | w1-12 | w1-11 | w1-10 |
	#:                        ...
	#:  	=========================================
	#:  	|   0   |   0   |   0   | w0-31 | w0-30 | 
	#:                        ... 
	#:  We favour wide reads and small depths (40-bit width or 5 values, depth of 512), but still require at least 2 M20Ks to get all 10 values in the vector
	#:  The second M20K in the example above would start with weight w0-9 and continue
	#:	The cur_block_in_chain speicifes which tensor block in the chain we're on, starting from the top
	def create_weight_memory(self, CONTROL_TO_DATA_DELAY, hbm_binary_weights, hbm_line_count, cur_block_in_chain=1, total_blocks_in_chain=1, duplicate_num=0, VALS_PER_LINE=5, offload_to_hbm=False):

		assert(cur_block_in_chain <= total_blocks_in_chain) #number of blocks in chain and current block should be valid

		inst = self.inst

		EXTRA_OUTPUT_REG = dl.ENABLE_RAM_OUTPUT_REG_NX
		assert(CONTROL_TO_DATA_DELAY-EXTRA_OUTPUT_REG > 0)

		filter_height = self.node.values[0].shape[0]
		filter_width = self.node.values[0].shape[1]
		assert(filter_width == filter_height) #For now, we don't support non-square filters
		filter_depth = self.node.values[0].shape[2]
		total_filters = self.node.values[0].shape[3]
		if self.is_depthwise:
			total_filters = filter_depth

		# check how many channels we should cover in each memory
		channels_per_mem = math.ceil((math.ceil(filter_depth/dl.NX_ICP_PER_TENSOR_BLOCK)*dl.NX_ICP_PER_TENSOR_BLOCK) / (total_blocks_in_chain*dl.NX_ICP_PER_TENSOR_BLOCK))

		#For ICP greater than NX_ICP_PER_TENSOR_BLOCK (10), the top block in the chain starts on a higher input channel
		starting_filter = (total_blocks_in_chain-cur_block_in_chain)*dl.NX_ICP_PER_TENSOR_BLOCK
		# Similar to above, we need to skip certain filters for higher ICPs, as they will be in seperate memories
		filters_to_cover = [filt for filt in range(starting_filter, (math.ceil(filter_depth/dl.NX_ICP_PER_TENSOR_BLOCK)*dl.NX_ICP_PER_TENSOR_BLOCK), total_blocks_in_chain*dl.NX_ICP_PER_TENSOR_BLOCK)]

		#Extract weights for each line of the memory, see the module comment for info on format
		first_rom_contents = []
		second_rom_contents = []
		if enable_bfp:
			first_rom_contents_bfloat = []
			second_rom_contents_bfloat = []
	

		#For depthwise convolutions we need to re-organize the weights such that the ICP=1 and we just multiply a different IC every time
		if self.is_depthwise and not self.node.values_modified:

			# Here, we just create rows of 10 such that the weight is in the correct position
			# In the loops further down we make sure that the only the first 10 rows are used first, followed by the second 10 later on

			self.node.values[0] = np.pad(self.node.values[0], ((0,0),(0,0), (0, 0), (0,dl.NX_ICP_PER_TENSOR_BLOCK-1)), 'constant')
			
			# Move each value to correct spot. Not sure how to do this in 1 line as we need to move stuff based on a modulo index
			for x in range(self.node.values[0].shape[0]):
				for y in range(self.node.values[0].shape[1]):
					for i in range(self.node.values[0].shape[-2]):
						self.node.values[0][x,y,i,i%dl.NX_ICP_PER_TENSOR_BLOCK] = self.node.values[0][x,y,i,0]

						if i%dl.NX_ICP_PER_TENSOR_BLOCK != 0:
							self.node.values[0][x,y,i,0] = 0

			self.node.values_modified = True

		weight_values = self.node.values[0]
		if enable_bfp:
			# Create 3 copies
			weight_values = self.node.values[0].copy()

			mantissas, exponents, signs = self.np_convert_fxp_to_fp16(weight_values)

			weight_values = mantissas

		non_zero_indexes = []
		non_zero_values = []

		#Have to store along entire filter width/height last as it means we need to switch out activations
		for k in range(filter_width*filter_width):
			fill_first_rom = True
			cur_total_filters = total_filters

			#Loop through filter depth and store along that next. Note that we will have to skip certain filters for higher IPCs
			#Since each M20k can only store VALS_PER_LINE (5) values, we have to increment by that instead of NX_ICP_PER_TENSOR_BLOCK (10)
			for i in range(starting_filter,filter_depth,VALS_PER_LINE):
				#Probably not the most efficient way to handle higher ICP, but simpler than changing the whole loop
				if (math.floor(i/dl.NX_ICP_PER_TENSOR_BLOCK)*dl.NX_ICP_PER_TENSOR_BLOCK) not in filters_to_cover: continue


				num_filters = cur_total_filters
				if self.is_depthwise:
					num_filters = min(cur_total_filters, dl.NX_ICP_PER_TENSOR_BLOCK)
					if not fill_first_rom: cur_total_filters -= num_filters

				#Now we have to go through the number of filters as we will broadcast those in order before moving on to the next depth (before moving on to the next height/width)
				for j in range(num_filters):
					cur_vals = weight_values[math.floor(k/float(filter_width)),k%filter_width,i:(i+VALS_PER_LINE),j].flatten().tolist()
					if enable_bfp:
						cur_exponents = exponents[math.floor(k/float(filter_width)),k%filter_width,i:(i+VALS_PER_LINE),j].flatten().tolist()
						cur_signs = signs[math.floor(k/float(filter_width)),k%filter_width,i:(i+VALS_PER_LINE),j].flatten().tolist()

					if len(cur_vals) < VALS_PER_LINE: 
						cur_vals += ([0] * (VALS_PER_LINE-len(cur_vals))) #Pad in zeros for any extra values
						if enable_bfp:
							cur_exponents += ([0] * (VALS_PER_LINE-len(cur_exponents)))
							cur_signs += ([0] * (VALS_PER_LINE-len(cur_signs)))

					# For Bfloat we need to wait until we know the contents of both ROMs in the loop
					if enable_bfp:
						if fill_first_rom: first_rom_contents_bfloat.append([cur_vals, cur_exponents, cur_signs])
						else: second_rom_contents_bfloat.append([cur_vals, cur_exponents, cur_signs])
					else:
						#For depthwise convolutions, we can compress the memory since only one value in each row is non zero
						#Thus, we extract the position and value of the non-zero number
						if self.is_depthwise:
							
							#Create lists of non-zero indexes and corresponding values
							#After we have gone through all output channels the first time (and are filling the second ROM) we can concatinate the sublists together
							if not fill_first_rom:
								constant = VALS_PER_LINE
								#Index ffset is for if we are find the values for the second memory (overall index should be from 0 to 9 but each individual memory is 0 to 4)
								non_zero_indexes[j] += [i+VALS_PER_LINE for i, e in enumerate(cur_vals) if e != 0]
								non_zero_values[j] += [e for i, e in enumerate(cur_vals) if e != 0]
							else:
								non_zero_indexes.append([i for i, e in enumerate(cur_vals) if e != 0])
								non_zero_values.append([e for i, e in enumerate(cur_vals) if e != 0])
							assert(len(non_zero_indexes[-1]) <= 1)

							#Need to make sure we only fill in one of the memories to get correct index
							if not fill_first_rom:

								if len(non_zero_indexes[j]) == 0: non_zero_indexes[j].append(0)
								if len(non_zero_values[j]) == 0: non_zero_values[j].append(0)
								cur_vals = [non_zero_values[j][0], non_zero_indexes[j][0]]
								int64_val = self.list_to_int64_bin(cur_vals)

								first_rom_contents.append(int64_val)
								if (j+1) == num_filters:
									non_zero_indexes = []
									non_zero_values = []

						else:
							#Concatenate all values together into one line. Note that if each line somehow stores more than an 64-bits, this will no longer work (right now it is 40-bits)
							int64_val = self.list_to_int64_bin(cur_vals)

							if fill_first_rom: first_rom_contents.append(int64_val)
							else: second_rom_contents.append(int64_val)

				# Re-align the two ROMs
				# If for example we have 32 filters (if depthwise) or 32 input channels (normal), we need to append 2 extra 0 filters to the second ROM as otherwise the first ROM will have 2 extra filters 
				# 	(kernel needs to be bigger than 1x1 for this to show)
				# For example: 
				# In a 2x2x14x3 filter:
				# If nothing is adjusted, the lower and upper memories will look like (format is w output_channel,input_channel,x,ycoordinates)
				# | w0,4,(0,0)	| w0,3,(0,0)	| ... | w0,0,(0,0)	|		AND 		| w0,9,(0,0) | w0,8,(0,0) | ... | w0,5,(0,0) |
				# | w1,4,(0,0)	| w1,3,(0,0)	| ... | w1,0,(0,0)	|					| w1,9,(0,0) | w1,8,(0,0) | ... | w1,5,(0,0) |
				# | w2,4,(0,0)	| w2,3,(0,0)	| ... | w2,0,(0,0)	|					| w2,9,(0,0) | w2,8,(0,0) | ... | w2,5,(0,0) |				
				# | 	0		| w0,13,(0,0)	| ... | w0,10,(0,0)	|			 		| w0,9,(1,0) | w0,8,(1,0) | ... | w0,5,(1,0) |
				# | 	0		| w1,13,(0,0)	| ... | w1,10,(0,0)	|			 		| w1,9,(1,0) | w1,8,(1,0) | ... | w1,5,(1,0) |
				# | 	0		| w2,13,(0,0)	| ... | w2,10,(0,0)	|			 		| w2,9,(1,0) | w2,8,(1,0) | ... | w2,5,(1,0) |
				# | w0,4,(1,0)	| w0,3,(1,0)	| ... | w0,0,(1,0)	|			 		| w0,9,(0,1) | w0,8,(0,1) | ... | w2,5,(0,1) |
				#					.																		.
				#					.																		.
				#					.																		.
				#
				# It's immediately clear that the values get misaligned if not adjusted for. After adjustements, the values should look something like
				# | w0,4,(0,0)	| w0,3,(0,0)	| ... | w0,0,(0,0)	|		AND 		| w0,9,(0,0) | w0,8,(0,0) | ... | w0,5,(0,0) |
				# | w1,4,(0,0)	| w1,3,(0,0)	| ... | w1,0,(0,0)	|					| w1,9,(0,0) | w1,8,(0,0) | ... | w1,5,(0,0) |
				# | w2,4,(0,0)	| w2,3,(0,0)	| ... | w2,0,(0,0)	|					| w2,9,(0,0) | w2,8,(0,0) | ... | w2,5,(0,0) |				
				# | 	0		| w0,13,(0,0)	| ... | w0,10,(0,0)	|			 		| 		0	 |		0	  | ... |		0	 |
				# | 	0		| w1,13,(0,0)	| ... | w1,10,(0,0)	|			 		| 		0	 |		0	  | ... |		0	 |
				# | 	0		| w2,13,(0,0)	| ... | w2,10,(0,0)	|			 		| 		0	 |		0	  | ... |		0	 |
				# | w0,4,(1,0)	| w0,3,(1,0)	| ... | w0,0,(1,0)	|			 		| w0,9,(1,0) | w0,8,(1,0) | ... | w0,5,(1,0) |
				#					.																		.
				#					.																		.
				#					.																		.

				if (i+VALS_PER_LINE) >= filter_depth and fill_first_rom:
					for d in range(num_filters):
						zero_list = [0]*VALS_PER_LINE
						if self.is_depthwise and  enable_bfp: 
							second_rom_contents_bfloat.append([zero_list, zero_list, zero_list])
						elif not enable_bfp and not self.is_depthwise:
							second_rom_contents.append(self.list_to_int64_bin(zero_list))



				fill_first_rom = (not fill_first_rom)


			if len(filters_to_cover) < channels_per_mem and k != range(filter_width*filter_width)[-1] and not self.is_depthwise:
				for c in range(channels_per_mem - len(filters_to_cover)):
					for d in range(num_filters):
						zero_list = [0]*VALS_PER_LINE 
						if enable_bfp:
							first_rom_contents_bfloat.append([zero_list, zero_list, zero_list])
							second_rom_contents_bfloat.append([zero_list, zero_list, zero_list])
						else:
							first_rom_contents.append(self.list_to_int64_bin(zero_list))
							second_rom_contents.append(self.list_to_int64_bin(zero_list))

			#clean up any remaining values for depthwise convolutions
			if self.is_depthwise and len(non_zero_indexes) > 0:
				for j in range(len(non_zero_indexes)):

					if len(non_zero_indexes[j]) == 0: non_zero_indexes[j].append(0)
					if len(non_zero_values[j]) == 0: non_zero_values[j].append(0)
					cur_vals = [non_zero_values[j][0], non_zero_indexes[j][0]]
					int64_val = self.list_to_int64_bin(cur_vals)

					first_rom_contents.append(int64_val)
					if (j+1) == num_filters:
						non_zero_indexes = []
						non_zero_values = []


		shared_exponent_rom_contents = []
		# Now that we know the contents of both roms for bfloat, we can convert 
		if enable_bfp:
			for i in range(len(first_rom_contents_bfloat)):

				# If the second ROM is empty past this point, pad in zeros
				if i >= len(second_rom_contents_bfloat):
					cur_vals = first_rom_contents_bfloat[i][0] + ([0] * len(first_rom_contents_bfloat[i][0]))
					cur_exponents = first_rom_contents_bfloat[i][1] + ([0] * len(first_rom_contents_bfloat[i][1]))
					cur_signs = first_rom_contents_bfloat[i][2] + ([0] * len(first_rom_contents_bfloat[i][2]))
				else:

					cur_vals = first_rom_contents_bfloat[i][0] + second_rom_contents_bfloat[i][0]
					cur_exponents = first_rom_contents_bfloat[i][1] + second_rom_contents_bfloat[i][1]
					cur_signs = first_rom_contents_bfloat[i][2] + second_rom_contents_bfloat[i][2]

				#Extract shared exponent and values for ROM
				cur_vals, shared_exponent = self.convert_fp16_to_bfloat_rom(cur_vals, cur_exponents, cur_signs)
				
				if self.is_depthwise:
					#For depthwise convolutions, just determine the index and value
					non_zero_index = [i for i, e in enumerate(cur_vals) if e != 0]
					non_zero_value = [e for i, e in enumerate(cur_vals) if e != 0]
					assert(len(non_zero_value) <= 1)
					if len(non_zero_value) == 0:
						non_zero_value = [0]
						non_zero_index = [0]

					cur_vals = [non_zero_value[0], non_zero_index[0]]
					int64_val = self.list_to_int64_bin(cur_vals)
					first_rom_contents.append(int64_val)

				else:
					#split current values into 2 lists since we have 2 ROMs
					half_of_list = int(len(cur_vals)/2)
					first_rom_contents.append(self.list_to_int64_bin(cur_vals[:half_of_list]))
					second_rom_contents.append(self.list_to_int64_bin(cur_vals[half_of_list:]))

				#Add shared exponent to list
				shared_exponent_rom_contents.append(shared_exponent)

		#If the second ROM is empty, then do not instantiate it(e.g. like in the first layer since IC=3)
		#To-do: We may also want to implement an optimization where the second ROM is smaller if the rest of the values are 0s (e.g. a layer where IC=32 and we use more than 1 M20K)
		second_rom_empty = False
		if len(second_rom_contents) == 0:
			second_rom_empty = True
		else:
			assert(not self.is_depthwise)
			#Take care of any leftover lines in second memory
			while len(second_rom_contents) < len(first_rom_contents):
				second_rom_contents.append(0)

		#If using an uneven ICP (e.g. IC=40 and ICP=30), then we will need to adjust the "depth" for some of the ROMs and just output zero for the remaining addresses
		if len(filters_to_cover) < channels_per_mem:
			depth_without_padding = len(first_rom_contents)/(1 + ((filter_height*filter_width - 1)/(filter_height * filter_width))*((channels_per_mem - len(filters_to_cover))/(len(filters_to_cover))))
			rom_depth = int(depth_without_padding / len(filters_to_cover)) * channels_per_mem
			enable_force_to_zero = True
		else:
			rom_depth = len(first_rom_contents)
			enable_force_to_zero = False
		
		#Create the ROMs and signals
		duplicate_str = ""
		if duplicate_num > 0: duplicate_str = "_duplicate_" + str(duplicate_num)

		rom_width = (VALS_PER_LINE*dl.NX_INPUT_DATA_BITS)
		#Rom width is just one value plus extra bits to store the index for the output muxing
		if self.is_depthwise:
			output_position_bits = clog2(dl.NX_ICP_PER_TENSOR_BLOCK)
			rom_width = dl.NX_INPUT_DATA_BITS + output_position_bits

		if (offload_to_hbm != True):
			first_ROM = inst(dl.ROM, content_list=first_rom_contents, bits=rom_width, name=("weight_ROM_lower_bits"+duplicate_str), enable_force_to_zero=enable_force_to_zero, register_output=bool(EXTRA_OUTPUT_REG))
			if not second_rom_empty: second_ROM = inst(dl.ROM, content_list=second_rom_contents, bits=(VALS_PER_LINE*8), name=("weight_ROM_upper_bits"+duplicate_str), enable_force_to_zero=enable_force_to_zero, register_output=bool(EXTRA_OUTPUT_REG))

			
			if self.is_depthwise:
				#For depthwise convolutions, we always load only one value and need to place it in the correct position
				loaded_value = inst(dl.Logic, bits=dl.NX_INPUT_DATA_BITS, name="loaded_value")
				output_position = inst(dl.Logic, bits=output_position_bits, name="output_position")
				loaded_value.set_driver(first_ROM.r_data[dl.NX_INPUT_DATA_BITS-1:0])
				output_position.set_driver(first_ROM.r_data[rom_width-1:dl.NX_INPUT_DATA_BITS])
				constant_0 = inst(dl.Constant, bits=dl.NX_INPUT_DATA_BITS, value=0, name=("constant_0"))

				#Create muxes for all values in output bus to check if position matches. If it does, then output the value, otherwise 0
				data_val = inst(dl.Logic, bits=(dl.NX_INPUT_DATA_BITS*dl.NX_ICP_PER_TENSOR_BLOCK), name=("depthwise_conv_weight_rom_r_data_"+duplicate_str))
				data_values = [inst(dl.Logic, bits=dl.NX_INPUT_DATA_BITS, name=("data_val_"+str(i))) for i in range(dl.NX_ICP_PER_TENSOR_BLOCK)]
				for i in range(dl.NX_ICP_PER_TENSOR_BLOCK):
					cur_constant = inst(dl.Constant, bits=output_position_bits, value=i, name=("depthwise_constant_"+str(i)))
					cur_mux_condition = inst(dl.EQ, cur_constant, output_position, name=("depthwise_mux_condition_"+str(i)))
					data_values[i].set_driver(inst(dl.MuxV2, cur_mux_condition, constant_0, loaded_value))

				data_val.set_driver(inst(dl.Concat, *data_values, name="concatinated_output_values"))

			else:
				#Concacinate the two ROM read values (or the zeros if we don't need the second ROM)
				if not second_rom_empty:
					data_val = inst(dl.Concat, first_ROM.r_data, second_ROM.r_data, name=("weight_rom_r_data"+duplicate_str))
				else:
					zeros = inst(dl.Constant, bits=(VALS_PER_LINE*dl.NX_INPUT_DATA_BITS), value=0, name="rom_read_val_const_zeros")
					data_val = inst(dl.Concat, first_ROM.r_data, zeros, name=("weight_rom_r_data"+duplicate_str))


			address_roms = inst(dl.Logic, bits=clog2(rom_depth), name=("weight_roms_addr"+duplicate_str))
			cropped_addr = address_roms[(clog2(len(first_rom_contents))-1):0] #In case we have made our address bigger than our ROM (i.e. ICP does not divide IC equally so we need some addresses set to 0)

			first_ROM.r_addr.set_driver(cropped_addr.delay(CONTROL_TO_DATA_DELAY-EXTRA_OUTPUT_REG))
			if not second_rom_empty: second_ROM.r_addr.set_driver(cropped_addr.delay(CONTROL_TO_DATA_DELAY-EXTRA_OUTPUT_REG))

			r_en_roms = inst(dl.Logic, bits=1, name=("read_weight_roms"+duplicate_str))

			if enable_force_to_zero:
				length_of_rom_contents = inst(dl.Constant, bits=clog2(rom_depth), value=len(first_rom_contents), name="length_of_rom_contents_const")
				disable_rom = inst(dl.Constant, bits=1, value=0, name="disable_rom_const")
				r_en_modified = inst(dl.MuxV2, inst(dl.LT, address_roms, length_of_rom_contents, name=("address_in_bounds"+duplicate_str)), disable_rom, r_en_roms, name=("read_weight_roms_check_bounds_"+duplicate_str))
			else:
				r_en_modified = r_en_roms

			first_ROM.r_en.set_driver(r_en_modified.delay(CONTROL_TO_DATA_DELAY-EXTRA_OUTPUT_REG))

			if not second_rom_empty: 
				second_ROM.r_en.set_driver(r_en_modified.delay(CONTROL_TO_DATA_DELAY-EXTRA_OUTPUT_REG))

			# Create shared exponent ROM
			if enable_bfp:
				shared_exponent_ROM = inst(dl.ROM, content_list=shared_exponent_rom_contents, bits=dl.NX_SHARED_EXP_SIZE, name=("shared_exponent_ROM"+duplicate_str), enable_force_to_zero=enable_force_to_zero, register_output=bool(EXTRA_OUTPUT_REG))
				shared_exp_read_val = shared_exponent_ROM.r_data
				shared_exponent_ROM.r_addr.set_driver(cropped_addr.delay(CONTROL_TO_DATA_DELAY-EXTRA_OUTPUT_REG))
				shared_exponent_ROM.r_en.set_driver(r_en_modified.delay(CONTROL_TO_DATA_DELAY-EXTRA_OUTPUT_REG))

			else:
				shared_exp_read_val = None
		else:
			#If HBM offload is on
			#Add the weights to the list 
			COMPLETE_WIDTH = 256
			zero_vector = [0 for e in range(math.ceil(rom_depth/3))]
			zero_vector_full_depth = [0 for e in range(rom_depth)]
			complete_bitarray = BitArray(bits=COMPLETE_WIDTH, value_vector=zero_vector)
			if second_rom_empty:
				second_rom_contents = zero_vector_full_depth
			second_rom_bitarray = BitArray(bits=(8*VALS_PER_LINE), value_vector=second_rom_contents)
			first_rom_bitarray = BitArray(bits=(8*VALS_PER_LINE), value_vector=first_rom_contents)
			#Get BitArrays of Zeros to complete the 256 bits
			mod3_index = 0
			#We can fit 3 80-bit tensors in 256 bits (we use 240 of the 256 for data)
			for i in range(len(first_rom_bitarray)):
				BYTE_WIDTH =int(COMPLETE_WIDTH / 8)
				complete_bitarray[i//3].bit_array[(mod3_index):(VALS_PER_LINE + mod3_index)] = first_rom_bitarray[i].bit_array
				complete_bitarray[i//3].bit_array[(VALS_PER_LINE + mod3_index):((2*VALS_PER_LINE)+mod3_index)] = second_rom_bitarray[i].bit_array
				mod3_index += 2*VALS_PER_LINE
				if (mod3_index == 6*VALS_PER_LINE):
					mod3_index = 0
			hbm_binary_weights.append(complete_bitarray)

			#Add the line count to hbm_line_count
			hbm_line_count.append(len(complete_bitarray))


			#Create the necessary signals to return (although they won't be used)
			r_en_roms = inst(dl.Logic, bits=1, name=("read_weight_roms"))
			address_roms = inst(dl.Logic, bits=1, name=("address_roms"))
			data_val = inst(dl.Logic, bits=1, name=("data_val"))
			shared_exp_read_val = inst(dl.Logic, bits=1, name=("shared_exp_read_val"))


		#Counts the number of empty vectors. This isn't used right now but may be implemented in the future for sparsity
		num_empty_vectors = 0
		num_total_vectors = 0
		for i in range(len(first_rom_contents)):
			if (second_rom_empty or second_rom_contents[i] == first_rom_contents[i]) and first_rom_contents[i] == 0:
				num_empty_vectors += 1
			num_total_vectors += 1

		return r_en_roms, address_roms, data_val, rom_depth, shared_exp_read_val

	def create_hbm_read_logic(self, inst, clock, rom_depth, output_height, stride, kernel_height, kernel_width, output_channels, ic_per_rom, chain_depth, cur_tensor_num, hbm_distribution_fifos, oc_counter, early_last_fifo_has_weight, last_fifo_has_weight, VALS_PER_LINE, FSM_TO_WEIGHT_ROM_DELAY):
		#We only instantiate the HBM interface once. The signals are then shared with everyone
		#We create an hbm_interface
		self.hbm_interface.append(inst(dl.SharedHBMController, ADDR_WIDTH=clog2(math.ceil((rom_depth)/3)-1), DATA_WIDTH=240))
		hbm_clk = inst(dl.Clock, force_simple_assignment=True, name = "hbm_clk")
		hbm_clk.set_driver(self.hbm_interface[-1].hbm_interface_clk)
		hbm_reset = self.hbm_interface[-1].hbm_interface_reset
		rd_fifo_number_of_lines_read = [[],[]] #First list is for upper padding, second list is for lower padding

		#We create a credit system to avoid sending in too many requests in a shared-controller situation
		#We need 3 sync chains for 3 of the change signals that will cross clock domains
		sync_chain_change_signal_1 = inst(dl.SynchronizationChain, clock, hbm_clk, stages=4)	
		sync_chain_change_signal_2 = inst(dl.SynchronizationChain, clock, hbm_clk, stages=4)
		sync_chain_change_signal_3 = inst(dl.SynchronizationChain, clock, hbm_clk, stages=4)
		#We also create two lists that will be filled later						
		#Logic that should be in the HBM clock domain
		times_where_padding_misses_one_value = []
		times_where_padding_misses_two_values = []


		with dl.ParamScope(clock=hbm_clk, reset=hbm_reset):

			#The credit counter has 4 different inputs:
			# First input: +3 when you enqueue an address (240 bits will be read, and each computation is 80 bits)
			# Second input: -1 when you dequeue for computation
			# Third input: -1 when you don't read one value for padding reasons (use 160 out of 240 bits)
			# Fourth input: -2 when you don't rea two values for padding reasons (use 80 out of 240 bits)
			weight_credit_counter = inst(dl.MultiChangeCounter, reset_value=0, change_values=[3,-1,-1,-2], end_value=512, name="weight_distribution_fifos_credit_counter") #End value is the same as the depth of the hbm distribution FIFOs below
			weight_credit_counter_has_space = inst(dl.LT, weight_credit_counter.current_value, inst(dl.Constant, bits=weight_credit_counter.current_value._bit_width, value=254), name="weight_credit_counter_has_space")
			weight_credit_counter.change_signals[1].set_driver(sync_chain_change_signal_1.control_out)
			weight_credit_counter.change_signals[2].set_driver(sync_chain_change_signal_2.control_out)
			weight_credit_counter.change_signals[3].set_driver(sync_chain_change_signal_3.control_out)



			#Reading from HBM
			hbm_output_line_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=output_height-1, name="hbm_output_line_counter")
			hbm_addr_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=math.ceil((rom_depth)/3)-1, load_value=True, name="hbm_addr_counter")

			address_inserted = inst(dl.Logic, bits=1, name="address_inserted")
			
			#We take care of the lower and upper padding for the HBM logic
			if self.padding[2][1] != 0:
				output_lines_that_require_lower_padding = math.ceil(self.padding[2][1]/stride)
				hbm_in_lower_padding = inst(dl.GT, hbm_output_line_counter.current_value, inst(dl.Constant, bits=clog2(output_height), value=(output_height-1-output_lines_that_require_lower_padding)), name="hbm_in_lower_padding")
				lower_padding_amounts = [self.padding[2][1] - i*stride for i in range(output_lines_that_require_lower_padding)]
				last_line_rows = [kernel_height - lower_padding_amounts[i] for i in range(output_lines_that_require_lower_padding)]								

				padding_row_finished_const_vals = [int((kernel_width*last_line_rows[i]*output_channels*math.ceil(ic_per_rom/(VALS_PER_LINE*2))))-1 for i in range(len(last_line_rows))]
				for pad_constant_number in padding_row_finished_const_vals:
					rd_fifo_number_of_lines_read[1].append(pad_constant_number)
				padding_row_finished_const_vals = [x//3 for x in padding_row_finished_const_vals] #Now that we're writing 3 addresses per line to HBM, this number is shrunk
				padding_row_finished_eq_conditions = [inst(dl.EQ, hbm_addr_counter.current_value, inst(dl.Constant, bits=hbm_addr_counter.current_value._bit_width, value=padding_row_finished_const_vals[i], name="reset_cond_addr_const"+str(i))) for i in range(len(last_line_rows))]
				if len(padding_row_finished_eq_conditions) > 1:
					#We should probably be muxing them instead of ORing them, but turthfully I don't think there's a layer yet where there are two lines of lower padding
					assert(padding_row_finished_eq_conditions == 1) #For future users, so you don't lose time debugging this
					padding_row_eq_combined = inst(dl.OR, *padding_row_finished_eq_conditions, name="reset_cond_addr_const_combined")
				else:
					padding_row_eq_combined = padding_row_finished_eq_conditions[0]
				hbm_addr_counter_is_done = inst(dl.MuxV2, hbm_in_lower_padding, hbm_addr_counter.is_done, padding_row_eq_combined)
				hbm_addr_counter.update_reset_condition(inst(dl.OR, hbm_reset, inst(dl.AND, hbm_addr_counter_is_done, address_inserted)))
			else:
				hbm_addr_counter_is_done = hbm_addr_counter.is_done


			if self.padding[2][0] != 0:
				output_lines_that_require_upper_padding = math.ceil(self.padding[2][0]/stride)
				upper_padding_amounts = [self.padding[2][0] - i*stride for i in range(output_lines_that_require_upper_padding)]
				upper_padding_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=math.ceil(output_lines_that_require_upper_padding-1), name="upper_padding_counter")
				
				hbm_in_upper_padding = inst(dl.LT, hbm_output_line_counter.current_value, inst(dl.Constant, bits=clog2(output_height), value=output_lines_that_require_upper_padding))
				upper_padding_counter.set_increment_condition(inst(dl.AND, hbm_in_upper_padding, hbm_output_line_counter.increment_condition))

				hbm_addresses_for_padding = [upper_padding_amounts[i] * output_channels * kernel_width * math.ceil(ic_per_rom/(VALS_PER_LINE*2)) for i in range(output_lines_that_require_upper_padding)]
				for padding_address in hbm_addresses_for_padding:
					rd_fifo_number_of_lines_read[0].append(padding_address)
				hbm_addresses_for_padding = [x//3 for x in hbm_addresses_for_padding]
				assert(hbm_addresses_for_padding[0] < rom_depth) #The first element will always be the biggest, no need to assert for the other ones
				hbm_addr_counter_load_constants = [inst(dl.Constant, bits=hbm_addr_counter.current_value._bit_width, value=(hbm_addresses_for_padding[i]), name="addr_counter_load_const"+str(i)) for i in range(output_lines_that_require_upper_padding)]
				hbm_addr_counter_load_value = inst(dl.MuxV2, upper_padding_counter.current_value, *hbm_addr_counter_load_constants, name="addr_counter_load_value")
				
				#If we are in upper_padding and hbm_addr_counter is reset value, the address is not sent to HBM. The control logic waits for the padding value to be loaded
				hbm_addr_counter.enable_load_value.set_driver(inst(dl.AND, hbm_addr_counter.is_reset_value, hbm_in_upper_padding, name="addr_counter_enable_load_value"))
				hbm_addr_counter.load_value.set_driver(hbm_addr_counter_load_value)
			else:
				const_0_1bit = inst(dl.Constant, bits=1, value=0, name="const_0_1bit")
				const_0_addr = inst(dl.Constant, bits=hbm_addr_counter.current_value._bit_width, value=0, name="const_0_addr")
				hbm_addr_counter.enable_load_value.set_driver(const_0_1bit)
				hbm_addr_counter.load_value.set_driver(const_0_addr)	
				hbm_in_upper_padding = const_0_1bit

			#An address is inserted every time the axi interface is ready, except when we need to load in a value in upper padding (in that case we wait 1 cycle)	
			# Next three lines slow down weight read speeds to 50% during simulations if you need it (you need to remove address inserted below)
			#stalling_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=1, name="stalling_counter")
			#stalling_counter.set_increment_condition(inst(dl.AND, self.hbm_interface[-1].arready, self.hbm_interface[-1].hbm_writing_is_done.delay(2), inst(dl.NOT, inst(dl.AND, hbm_in_upper_padding, hbm_addr_counter.is_reset_value))))
			#address_inserted.set_driver(stalling_counter.done_and_incrementing)	
			address_inserted.set_driver(inst(dl.AND, self.hbm_interface[-1].has_space_for_address, self.hbm_interface[-1].hbm_writing_is_done.delay(2), weight_credit_counter_has_space, inst(dl.NOT, inst(dl.AND, hbm_in_upper_padding, hbm_addr_counter.is_reset_value))))				
			self.hbm_interface[-1].address_valid.set_driver(address_inserted)
			self.hbm_interface[-1].read_address.set_driver(hbm_addr_counter.current_value)
			weight_credit_counter.change_signals[0].set_driver(address_inserted)

			hbm_addr_counter.set_increment_condition(address_inserted)
			hbm_output_line_counter.set_increment_condition(inst(dl.AND, hbm_addr_counter_is_done, address_inserted))


		#We use shift-registers to shift the data from the HBM_read FIFO to all the other FIFOs
		num_fsms_to_hook_up = math.ceil(self.tensor_conv_chains.num_roms_to_hook_up/dl.weight_FSM_duplication_factor)
		weight_data_shifter = inst(dl.BasicShiftRegister,
			bits=(80+1),
			depth=num_fsms_to_hook_up,
			name="weight_data_shifter")

		
		#The data is now available at the output of self.hbm_interface[-1].read_data, in order
		#We add some extra early leeway to be able to have some time before stopping the pipeline. This is useful because the TensorActivationBuffer
		# takes 7 cycles to send data to the tensorblocks, and consequently runs 7 cycles ahead
		EARLY_ALERT_LEEWAY = 5
		AVOIDING_STOPPING_AT_THE_END = 5
		for fifo_index in range(len(hbm_distribution_fifos)):
			hbm_fifo = inst(dl.FIFO,
				bits=80,
				depth=512,
				almost_empty=FSM_TO_WEIGHT_ROM_DELAY + FSM_TO_WEIGHT_ROM_DELAY + self.CONTROL_TO_DATA_DELAY + EARLY_ALERT_LEEWAY + EARLY_ALERT_LEEWAY + EARLY_ALERT_LEEWAY + AVOIDING_STOPPING_AT_THE_END)		#The almost full signal is also pipelined on the way back to HBM, around the same number of times as SR stages, and the 1 is because the dcfifo is not in showahead mode
			#Three EARLY_ALERT_LEEWAY were necessary during debugging to cover the roundtrip delay of some signals. At the time of writing this I don't remember the exact edge cases, but any less would cause issues.
			hbm_fifo.w_en.set_driver(weight_data_shifter.sr[fifo_index//dl.weight_FSM_duplication_factor][80])
			hbm_fifo.w_data.set_driver(weight_data_shifter.sr[fifo_index//dl.weight_FSM_duplication_factor][79:0])
			hbm_distribution_fifos[fifo_index].append(hbm_fifo)

		sync_chain_change_signal_1.control_in.set_driver(hbm_distribution_fifos[fifo_index][-1].r_en)
		
		# At this point, every read from self.hbm_interface[-1].read_data is 240 bits wide, but we want to choose which data to read
		# rd_fifo_number_of_lines_read holds the number of lines read for every padding line. 
		deserialization_line_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=output_height-1, name="deserialization_line_counter")
		deserialization_addr_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=math.ceil(rom_depth-1), load_value=True, name="deserialization_addr_counter")
		deserialization_mod_3_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=2, load_value=True, name="deserialization_mod3_counter")
		address_dequeued = inst(dl.Logic, bits=1, name="deserialization_address_dequeued")
		
		#We take care of padding just like above, we just have the values precomputed this time around
		#If we have lower padding, deserialization_addr_counter and mod_3_counter should reset even if the the 240 bits are not fully taken care of (they might be padding)
		if (len(rd_fifo_number_of_lines_read[1]) > 0):
			deserialization_in_lower_padding = inst(dl.GE, deserialization_line_counter.current_value, inst(dl.Constant, bits=deserialization_addr_counter.current_value._bit_width, value=output_height-len(rd_fifo_number_of_lines_read[1])), name="deserialization_in_lower_padding")
			#If we have lower padding
			assert(len(rd_fifo_number_of_lines_read[1]) == 1) #For future users, so you don't lose time debugging this
			#Add mux if there's more than 1 value, but right now it's never the case so I'm simplifying it a little bit
			deserialization_padding_finished_eq_condition = inst(dl.EQ, deserialization_addr_counter.current_value, inst(dl.Constant, bits=clog2(rom_depth), value=rd_fifo_number_of_lines_read[1][0], name="deserialization_reset_cond_addr_const"))									
			deserialization_addr_counter_is_done = inst(dl.MuxV2, deserialization_in_lower_padding, deserialization_addr_counter.is_done, deserialization_padding_finished_eq_condition)
			deserialization_addr_counter.update_reset_condition(inst(dl.OR, hbm_reset, inst(dl.AND, deserialization_addr_counter_is_done, address_dequeued)))
			deserialization_mod_3_counter.update_reset_condition(inst(dl.OR, hbm_reset, inst(dl.AND, deserialization_addr_counter_is_done, address_dequeued)))
			number_of_reads_skipped = 3 - ((rd_fifo_number_of_lines_read[1][0]+1) % 3) #The addr is because the counter is indexed at 0, but we want to see if the number indexed at 1 is divisible by 3
			if number_of_reads_skipped == 1:
				times_where_padding_misses_one_value.append(inst(dl.AND, deserialization_in_lower_padding, deserialization_padding_finished_eq_condition, address_dequeued))
			elif number_of_reads_skipped == 2:
				times_where_padding_misses_two_values.append(inst(dl.AND, deserialization_in_lower_padding, deserialization_padding_finished_eq_condition, address_dequeued))
			number_of_reads_skipped_outside_padding = 3 - (rom_depth % 3)
			if number_of_reads_skipped_outside_padding == 1:
				times_where_padding_misses_one_value.append(inst(dl.AND, inst(dl.NOT, deserialization_in_lower_padding), deserialization_addr_counter.is_done, address_dequeued))
			elif number_of_reads_skipped_outside_padding == 2:
				times_where_padding_misses_two_values.append(inst(dl.AND, inst(dl.NOT, deserialization_in_lower_padding), deserialization_addr_counter.is_done, address_dequeued))													
		else:
			deserialization_addr_counter_is_done = deserialization_addr_counter.is_done
			deserialization_mod_3_counter.update_reset_condition(inst(dl.OR, hbm_reset, inst(dl.AND, deserialization_addr_counter_is_done, address_dequeued)))
			number_of_reads_skipped_outside_padding = 3 - (rom_depth % 3)
			if number_of_reads_skipped_outside_padding == 1:
				times_where_padding_misses_one_value.append(inst(dl.AND, deserialization_addr_counter.is_done, address_dequeued))
			elif number_of_reads_skipped_outside_padding == 2:
				times_where_padding_misses_two_values.append(inst(dl.AND, deserialization_addr_counter.is_done, address_dequeued))													

		#If we have upper padding, we want deserialization_addr_counter to initialize to a certain value, and deserialization_mod_3_counter as well
		if (len(rd_fifo_number_of_lines_read[0]) > 0):
			deserialization_upper_padding_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=math.ceil(len(rd_fifo_number_of_lines_read[0])-1), name="deserialization_upper_padding_counter")
			
			deserialization_in_upper_padding = inst(dl.LT, deserialization_line_counter.current_value, inst(dl.Constant, bits=clog2(output_height), value=len(rd_fifo_number_of_lines_read[0])))
			deserialization_upper_padding_counter.set_increment_condition(inst(dl.AND, deserialization_in_upper_padding, deserialization_line_counter.increment_condition))
			
			deserialization_addr_counter_load_constants = [inst(dl.Constant, bits=clog2(rom_depth), value=(rd_fifo_number_of_lines_read[0][i]), name="deserialization_addr_counter_load_const"+str(i)) for i in range(len(rd_fifo_number_of_lines_read[0]))]
			deserialization_addr_counter_load_value = inst(dl.MuxV2, deserialization_upper_padding_counter.current_value, *deserialization_addr_counter_load_constants, name="deserialization_addr_counter_load_value")
			deserialization_mod_3_counter_load_constants = [inst(dl.Constant, bits=2, value=(rd_fifo_number_of_lines_read[0][i] % 3), name="deserialization_mod_counter_load_const"+str(i)) for i in range((len(rd_fifo_number_of_lines_read[0])))]
			deserialization_mod_3_counter_load_value = inst(dl.MuxV2, deserialization_upper_padding_counter.current_value, *deserialization_mod_3_counter_load_constants, name="deserialization_mod_3_counter_load_value")
			#If we are in upper_padding and deserialization_addr_counter is reset value, we shouldn't read the address. The control logic waits for the padding value to be loaded
			deserialization_addr_counter.enable_load_value.set_driver(inst(dl.AND, deserialization_addr_counter.is_reset_value, deserialization_in_upper_padding, name="deserialization_addr_counter_enable_load_value"))
			deserialization_addr_counter.load_value.set_driver(deserialization_addr_counter_load_value)
			deserialization_mod_3_counter.enable_load_value.set_driver(inst(dl.AND, deserialization_addr_counter.is_reset_value, deserialization_in_upper_padding, name="deserialization_mod_3_counter_enable_load_value"))
			deserialization_mod_3_counter.load_value.set_driver(deserialization_mod_3_counter_load_value)
			for i,n_of_lines in enumerate(rd_fifo_number_of_lines_read[0]):
				number_of_reads_skipped = (n_of_lines % 3)
				if number_of_reads_skipped == 1:
					times_where_padding_misses_one_value.append(inst(dl.AND, deserialization_addr_counter.is_reset_value, deserialization_in_upper_padding, 
																	inst(dl.EQ, deserialization_upper_padding_counter.current_value, inst(dl.Constant, bits=deserialization_upper_padding_counter.current_value._bit_width, value=i))))
				elif number_of_reads_skipped == 2:
					times_where_padding_misses_two_values.append(inst(dl.AND, deserialization_addr_counter.is_reset_value, deserialization_in_upper_padding, 
																	inst(dl.EQ, deserialization_upper_padding_counter.current_value, inst(dl.Constant, bits=deserialization_upper_padding_counter.current_value._bit_width, value=i))))							
		else:
			ds_const_0_1bit = inst(dl.Constant, bits=1, value=0, name="ds_const_0_1bit")
			ds_const_0_addr = inst(dl.Constant, bits=clog2(rom_depth), value=0, name="ds_const_0_addr")
			ds_const_0_mod3 = inst(dl.Constant, bits=2, value=0, name="ds_const_0_mod3")
			deserialization_addr_counter.enable_load_value.set_driver(ds_const_0_1bit)
			deserialization_addr_counter.load_value.set_driver(ds_const_0_addr)	
			deserialization_in_upper_padding = ds_const_0_1bit
			deserialization_mod_3_counter.enable_load_value.set_driver(ds_const_0_1bit)
			deserialization_mod_3_counter.load_value.set_driver(ds_const_0_mod3)

		#An address is dequeued every time rd_fifo_hbm is not empty and the last fifo has space, except when we need to load in a value in upper padding (in that case we wait 1 cycle)	
		address_dequeued.set_driver(inst(dl.AND, self.hbm_interface[-1].read_data_available, inst(dl.NOT, inst(dl.AND, deserialization_in_upper_padding, deserialization_addr_counter.is_reset_value))))				
		deserialization_addr_counter.set_increment_condition(address_dequeued)
		deserialization_mod_3_counter.set_increment_condition(address_dequeued)
		deserialization_line_counter.set_increment_condition(inst(dl.AND, deserialization_addr_counter_is_done, address_dequeued))

		#Complete the signals required for the credit counter
		credit_counter_change_signals_zero = inst(dl.Constant, bits=1, value=0, name="credit_const_zero")
		if (len(times_where_padding_misses_one_value)==0):
			sync_chain_change_signal_2.control_in.set_driver(credit_counter_change_signals_zero)
		else:
			sync_chain_change_signal_2.control_in.set_driver(inst(dl.OR, *times_where_padding_misses_one_value))
		
		if (len(times_where_padding_misses_two_values)==0):
			sync_chain_change_signal_3.control_in.set_driver(credit_counter_change_signals_zero)
		else:
			sync_chain_change_signal_3.control_in.set_driver(inst(dl.OR, *times_where_padding_misses_two_values))
		

		chosen_80_bits = inst(dl.Logic, bits=80, name="chosen_80_bits")
		#We end up with as many FIFOs as needed, with the useful signals for each of them being (hbm_fifo.r_en, hbm_fifo.r_data, hbm_fifo.almost_empty)
		chosen_80_bits.set_driver(inst(dl.MuxV2, deserialization_mod_3_counter.current_value.delay(1), self.hbm_interface[-1].read_data[79:0], self.hbm_interface[-1].read_data[159:80], self.hbm_interface[-1].read_data[239:160]))
		weight_data_shifter.d.set_driver(inst(dl.Concat, chosen_80_bits, address_dequeued.delay(1)).delay(1))
		self.hbm_interface[-1].read_data_request.set_driver(inst(dl.AND, address_dequeued, inst(dl.OR, deserialization_mod_3_counter.is_done, deserialization_addr_counter_is_done)))

		#At the end, we connect the weight available signal
		#We make sure none of the furthest FIFOs are empty (if Pi<=10, or we're using the minimal amount of tensorblocks, this means 1 FIFO)
		#If Pi=20, then this means 2 FIFOs, each carrying different parts of the weights
		if (cur_tensor_num == chain_depth-1):
			almost_empty_signals = []
			for i in hbm_distribution_fifos[-1]:
				almost_empty_signals.append(i.almost_empty.delay(FSM_TO_WEIGHT_ROM_DELAY))
			all_have_weight_available = inst(dl.NOT, inst(dl.OR, *almost_empty_signals))
			self.tensor_activation_buffers.weight_available.set_driver(all_have_weight_available)
			early_oc_counter_not_close = inst(dl.GE, oc_counter.current_value, inst(dl.Constant, bits=oc_counter.current_value._bit_width, value=output_channels-(EARLY_ALERT_LEEWAY+AVOIDING_STOPPING_AT_THE_END-1)))
			early_last_fifo_has_weight.set_driver(inst(dl.OR, all_have_weight_available, early_oc_counter_not_close))
			last_fifo_has_weight.set_driver(early_last_fifo_has_weight.delay(EARLY_ALERT_LEEWAY))
			oc_counter_at_last_fifo_has_weight_early_raise = inst(dl.Flop, reset_driver=1, bits=oc_counter.current_value._bit_width, name="oc_counter_at_last_fifo_has_weight_early_raise")
			oc_counter_at_last_fifo_has_weight_early_raise.set_driver(inst(dl.MuxV2, early_last_fifo_has_weight, 
													oc_counter_at_last_fifo_has_weight_early_raise, oc_counter.current_value))
			tensor_conv_will_freeze = early_last_fifo_has_weight
			return tensor_conv_will_freeze




class HPipe(dl.Module):
	def __init__(self, plan, debug_individual_stage=None, build_until=None, build_from=None, **kwargs):
		super(HPipe, self).__init__("hpipe", kind="hpipe", **kwargs)
		inst = self.inst
		with dl.ModuleScope(self):
			self.alternate_reset = inst(dl.Logic, bits=1, name="alternate_reset")
			self.alternate_reset.set_driver(inst(dl.Constant, value=0, bits=1, name="alternate_reset_default_driver"))
			combined_reset = inst(dl.OR, self.alternate_reset, kwargs["reset"])
			def build_reset(combined_reset, count_value=100, name="reset"):
				nonlocal inst
				reset_counter = inst(dl.Counter, reset=combined_reset, reset_value=0, end_value=count_value, increment_value=1, name=name + "_counter")
				reset_counter.set_increment_condition(inst(dl.NOT, reset_counter.is_done))
				reset = inst(dl.NOT, reset_counter.is_done)
				# I don't use delay here because the DUPLICATE_HIERARCHY_DEPTH
				# assignment requires our pipe to be in the module from which
				# it fans out.
				reset_pipe = [inst(dl.Flop, name="reset_pipe_" + str(i))for i in range(12)]
				prior_stage = [reset] + reset_pipe
				for rp,prior in zip(reset_pipe, prior_stage):
					rp.set_driver(prior)
				reset_pipe[-1].name=name
				reset = reset_pipe[-1]
				# The DUPLICATE_HIERARCHY_DEPTH assignment doesn't support wildcards even though it
				# explicitly says it does.  As a result, this needs the full path :(
				dl.Verilog.current_circuit.add_quartus_setting_lambda(
					lambda: "set_instance_assignment -name DUPLICATE_HIERARCHY_DEPTH 10 -to " + '"u0|generic_component_0' + reset.get_sdc_filter_string()[1:] + '"')
				return reset
			self.input_reset = None
			self.reset = inst(dl.Logic, bits=1, name="system_reset")
			if "input_clock" in kwargs:
				input_clock = kwargs["input_clock"]
				with dl.ParamScope(clock=input_clock):
					reset_control_counter = inst(dl.Counter, reset=combined_reset, reset_value=0, end_value=20, increment_value=1, name="reset_control_counter")
					reset_control_counter.set_increment_condition(inst(dl.NOT, reset_control_counter.is_done))
					# Since the input clock is slower than the system clock, 
					delayed_combined_reset = inst(dl.NOT, reset_control_counter.is_done).delay(3)
					second_delayed_combined_reset = inst(dl.NOT, reset_control_counter.is_done).delay(1)
					input_reset = build_reset(delayed_combined_reset, name="input_reset")
					self.input_reset = input_reset
				combined_reset_cdc = inst(dl.SynchronizationChain, input_clock, kwargs["clock"])
				combined_reset_cdc.control_in.set_driver(second_delayed_combined_reset)
				combined_reset = combined_reset_cdc.control_out
			self.reset.set_driver(build_reset(combined_reset, name="system_reset"))
			with dl.ParamScope(reset=self.reset, input_reset=self.input_reset):
				nodes = []
				self.nodes = nodes
				done = False
				last_node = None
				add_count = 0
				found_start = True
				if build_from is not None:
					found_start = False
				def process_node(n):
					nonlocal last_node
					nonlocal done
					nonlocal nodes
					nonlocal add_count
					nonlocal found_start
					nonlocal debug_individual_stage
					nonlocal build_until
					nonlocal build_from
					if done:
						return
					if build_from is not None and build_from in n.tf_op.name:
						if not found_start:
							nodes.append(n.inputs[0]._from)
						found_start = True
					if found_start:
						nodes.append(n)
						last_node = n
					if build_until is not None and build_until in n.tf_op.name:
						add_count += 1
						done = True
						#if add_count == 3:
					if debug_individual_stage is not None and debug_individual_stage in n.tf_op.name:
						nodes = [n]
						self.nodes = nodes
						done = True
					#if n.type == "Conv2D":
					#	done = True
						#add_count += 1
						#if add_count == 10:
						#	done = True"""
				plan.graph.walk_graph(f_node=process_node)
				if debug_individual_stage is not None:
					fake_placeholder = nodes[0].inputs[0]._from
					fake_placeholder.type = "Placeholder"
					fake_placeholder.inputs = []
					dl.Verilog.current_params.kwargs["input_bit_spec"] = {
						"int" : fake_placeholder.precision_parameters["a_int"],
						"frac" : fake_placeholder.precision_parameters["a_f"],
						"sign" : 1
					}
					nodes.append(fake_placeholder)

				for n in nodes:
					if n.type == "Placeholder":
						first_placeholder = n
						break
				for n in nodes:
					if n.type == "Conv2D":
						first_conv = n
						break
				for n in nodes:
					if n.type == "MaxPool":
						first_max_pool = n
						break
				for n in nodes:
					if n.type == "BiasAdd":
						first_bias_add = n
						break
				for n in nodes:
					if n.type == "Relu":
						first_relu = n
						break

				instances = []
				module_map = {
					"Placeholder" : Placeholder,
					"Conv2D" : BasicConv,
					"DepthwiseConv2dNative" : BasicConv,
					"MaxPool": MaxPool,
					"BiasAdd": BiasAdd,
					"Relu"   : ReLU,
					"Relu6"  : ReLU6,
					"Swish"  : ActivationFunction,
					"Add"    : Add,
					"AddV2"    : Add,
					"Sigmoid"  : ActivationFunction,
					"Tanh"	 : ActivationFunction,
					"Mean"   : Mean,
					"Flatten": Flatten,
				}

				#For tensor blocks in tensor mode we only switch Conv2D modules
				#Depthwise convs don't have any ICP and thus there is no point of using tensor block in tensor mode (we can just use scalar mode to simplify the logic)
				if arch.hpipe_device_arch.tensor_block_mode == "tensor":
					module_map["Conv2D"] = TensorConv
					global enable_bfp
					if enable_bfp:
						module_map["DepthwiseConv2dNative"] = TensorConv

				edges_to_connect = []
				#with dl.ParamScope(extend_to_match_bits=True):
				override_args = {}
				if build_from is not None:
					override_args["input_bit_spec"] = {
						"sign" : first_placeholder.precision_parameters["a_sign_bits"],
						"int" : first_placeholder.precision_parameters["a_int"],
						"frac" : first_placeholder.precision_parameters["a_f"]
					}
				if "partitioning" in kwargs:
					partitioning = kwargs["partitioning"]
				else:
					partitioning = False
				for n in nodes:
					if n.type == "Conv2D" or n.type == "DepthwiseConv2dNative":
						if n.planner_estimate.n_output_channel_groups > 1:
							instances.append(inst(ConvWrapper, n))
						else:
							instances.append(inst(module_map[n.type], n))
							
					else:
						instances.append(inst(module_map[n.type], n))
					n.module = instances[-1]
					edges_to_connect.extend(list(enumerate(n.inputs)))
					if partitioning:
						n.module.get_memory_estimate(n)
				self.placeholder = first_placeholder.module
				#For Partitioning We don't ned to connect modules so we jump ahead to the partitioning code
				if partitioning:
					return 

				for i,e in edges_to_connect:
					_to = e._to
					_from = e._from
					i_driven = _to.module.input_modules[i]
					o_driven = _to.module
					driver = _from.module
					if driver is None:
						continue
					ctoo = driver.get_control_valid_to_output_valid_delay()
					ctoi = o_driven.get_control_to_input_delay()
					o_to_i_delay = ctoi - ctoo
					c_to_c_delay = ctoo - ctoi
					if ctoi == -1:
						o_driven._set_control_valid_to_output_valid_delay(driver.get_control_valid_to_output_valid_delay())
						o_driven._set_control_to_input_delay(driver.get_control_valid_to_output_valid_delay())
						o_to_i_delay = 0
						c_to_c_delay = 0
					#if build_from is not None:
					#	ctoo = 0
					#	driver._set_control_to_input_delay(0)
					#	driver._set_control_valid_to_output_valid_delay(0)
					#else:
					assert(ctoo != -1)


					max_fan = None
					final_fan = None
					standard_pipe_delay = 2
					added_pipe_fanout_delay = 0
					if isinstance(o_driven, Add):
						input_index = _to.inputs.index(e)
						min_buffer_depth = max(_to.planner_estimate.buffer_for_input[input_index], 2)
						standard_pipe_delay = min(min_buffer_depth, 5)

					c_to_c_delay = max(standard_pipe_delay, c_to_c_delay + standard_pipe_delay)
					o_to_i_delay = max(standard_pipe_delay, o_to_i_delay + standard_pipe_delay)
					with dl.ModuleScope(o_driven):
						i_driven.write.set_driver(driver.output_valid, delay=c_to_c_delay)
						for _input,o in zip(i_driven.inputs, driver.outputs):
							_input.set_driver(o.delay(o_to_i_delay))
						for _input,o in zip(i_driven.writes, driver.outputs_valid):
							_input.set_driver(o.delay(o_to_i_delay))

						"""
						outputs_and_outputs_valid = driver.outputs[0].get_delay(driver.outputs + driver.outputs_valid + [driver.output_valid], hyperpipe=True, delay=5)
						output_valid = outputs_and_outputs_valid[-1]
						outputs_and_outputs_valid = outputs_and_outputs_valid[:-1]
						i_driven.write.set_driver(output_valid.delay(c_to_c_delay, auto_shift_register_recognition=False))
						for i,o in zip(i_driven.inputs + i_driven.writes, outputs_and_outputs_valid):
							i.set_driver(o.delay(o_to_i_delay))"""


				#Non-buffered layer propagate the credit system signals of buffered layers around them
				#Here, we propagate the attributes such as cred_dec_amount (not the actual signals)
				for r in range(len(nodes)):
					for n in nodes[::-1]:
						m = n.module
						with dl.ModuleScope(m):
							#Placeholder has buffered_layer == None, and buffered layers have it set as True
							if m.buffered_layer == False:
								if len(n.outputs) == 1:
									#We pass the information forward if there's only one output (and later we'll pass forward the actual signals)
									mod = n.outputs[0]._to.module
									m.cred_dec_amount = mod.cred_dec_amount
									m.cred_dec_final_amount = mod.cred_dec_final_amount
									m.cred_max[n.inputs[0]._from.module.name] = mod.cred_max[m.name]
									m.cred_padding = mod.cred_padding
									if hasattr(mod, "cred_padding_unpause") and not hasattr(m, "cred_padding_unpause"):
										m.cred_padding_unpause = inst(dl.Logic, 1, name="credit_padding_unpause")
								if len(n.outputs) > 1 :
									#If there are more than 2 outputs, then there will later be a simplifying counter that sends credits of size 1. For that reason, we set cred_dec_amount to 1
									m.cred_dec_amount = -1
									m.cred_dec_final_amount = 0
									m.cred_padding = [0,0]
									m.cred_max[n.inputs[0]._from.module.name] = 1000000
									temp_cred_padding_0 = 0
									temp_cred_padding_1 = 0
									any_output_has_unpause = False
									for o in n.outputs:
										mod = o._to.module
										m.cred_max[n.inputs[0]._from.module.name] = min(m.cred_max[n.inputs[0]._from.module.name], mod.cred_max[m.name])
										temp_cred_padding_0 = max(temp_cred_padding_0, mod.cred_padding[0])
										temp_cred_padding_1 = max(temp_cred_padding_1, mod.cred_padding[1])
										if hasattr(mod, "cred_padding_unpause"):
											any_output_has_unpause = True
									m.cred_padding = [temp_cred_padding_0, temp_cred_padding_1]
									if (any_output_has_unpause and not hasattr(m, "cred_padding_unpause")):
										m.cred_padding_unpause = inst(dl.Logic, 1, name="credit_padding_unpause")


				for n in nodes:
					print("Currently at node ", n.module.module_name, " where the credit values are ", n.module.cred_dec_amount, " ", n.module.cred_dec_final_amount, " ", n.module.cred_max, " ", n.module.cred_padding)			

				for n in nodes:
					m = n.module
					def valid_condition(m, t="  ", n=2):
						control_valid_to_output_valid_delay = m.get_control_valid_to_output_valid_delay()
						condition = "(" + m.output_valid.name + " === 1 |-> ##" + str(control_valid_to_output_valid_delay) + " " + m.outputs_valid[0].name + " === 1)"
						if m.guarantees_output_valid_low_in_cycles:
							condition += " and\n"
							condition += t * n + "(" + m.output_valid.name + " === 0 |-> ##" + str(control_valid_to_output_valid_delay) + " " + m.outputs_valid[0].name + " === 0)"
						return condition
					def write_condition(m, t="  ", n=2):
						assert(isinstance(m, NNStageInput))
						control_to_input_delay = m.owner.get_control_to_input_delay()
						condition = "(" + m.write.name + " === 1 |-> ##" + str(control_to_input_delay) + " " + m.writes[0].name + " === 1)"
						return condition


					"""for im in m.input_modules:
						with dl.ModuleScope(im):
							clocked_logic = inst(dl.Flop, bits=1, name="just_need_to_get_a_clock_in_here")
							clocked_logic.set_driver(inst(dl.Constant, bits=1, value=0))
							inst(dl.Assertion,
								name="output_valid_delay_check",
								condition_f=write_condition,
								condition_f_args=im,
								comment=n.tf_op.name + " specifies the delay from write to writes[0] should be " + str(m.get_control_to_input_delay()) + " cycles",
								error_string=n.tf_op.name + " writes[0] should have had a delay of " + str(m.get_control_to_input_delay()) + " from write")"""

					with dl.ModuleScope(m):
						"""inst(dl.Assertion,
							name="output_valid_delay_check",
							condition_f=valid_condition,
							condition_f_args=m,
							comment=n.tf_op.name + " specifies the delay from output_valid to outputs_valid[0] should be " + str(m.get_control_valid_to_output_valid_delay()) + " cycles",
							error_string=n.tf_op.name + " outputs_valid[0] should have had a delay of " + str(m.get_control_valid_to_output_valid_delay()) + " from output_valid")"""
						space_to_write_line_signals = []
						for o in n.outputs:
							mod = o._to.module
							if mod is None:
								continue
							input_mod = mod.input_modules[o._to.inputs.index(o)]
							space_to_write_line_signals.append(input_mod.space_to_write_line.delay(3))

						m.connect_credit_system(inst, space_to_write_line_signals)

				self.connect_all_hbm_connections(inst, nodes, input_clock, input_reset, clock_kwargs = kwargs["clock"] if "clock" in kwargs else None)

				self.output_modules = []
				for n in nodes:
					m = n.module
					if len(m.can_write_line.drivers) == 0:
						m.can_write_line.set_driver(inst(dl.Constant, bits=1, value=1))
						self.output_modules.append(m)

				#self.bc = inst(BasicConv, first_conv)
				#self.iwc.finished_reading_line.set_driver(self.bc.oc_controller.done)
				#self.bc.oc_controller.go.set_driver(inst(dl.AND, self.iwc.has_full_kernel, self.max_pool.can_write_line.delay(5)))

				# we will make a shift register that shifts the input and control data to each memory
				"""shifter_depth = math.ceil(len(self.bc.ia_buffer.ram_list)/2.)
				with dl.ModuleScope(self.bc.ia_buffer):
					self.input_data_shifter = inst(dl.ShiftRegister, bits=self.bc.ia_buffer.ram_list[0].w_data._bit_width, depth=shifter_depth, name="input_data_shifter")
					self.w_addr_shifter = inst(dl.ShiftRegister, bits=self.iwc.write_addr._bit_width, depth=shifter_depth+1, name="w_addr_shifter")
					self.w_addr_shifter.d.set_driver(self.iwc.write_addr)
					self.ic_lsb_shifter = inst(dl.ShiftRegister, bits=1, depth=shifter_depth+1, name="ic_lsb_shifter")
					self.ic_lsb_shifter.d.set_driver(self.iwc.input_channel_counter.current_value[0:0])
					self.we_shifters = [inst(dl.ShiftRegister, bits=1, depth=i+2, name="we_shifter") for i in range(len(self.iwc.we_s))]
					for we_shifter, we in zip(self.we_shifters, self.iwc.we_s):
						we_shifter.d.set_driver(we)

				for i, r in enumerate(self.bc.ia_buffer.ram_list):
					with dl.ModuleScope(r):
						target_ic = i % 2
						shifter_index = i // 2
						r.w_en.set_driver(
							inst(dl.AND,
								inst(dl.EQ,
									inst(dl.Constant, target_ic, bits=1),
									self.ic_lsb_shifter.sr[shifter_index+1]),
								self.we_shifters[shifter_index].q))
						r.w_addr.set_driver(self.w_addr_shifter.sr[shifter_index+1])
						r.w_data.set_driver(self.input_data_shifter.sr[shifter_index])"""
	def get_custom_verilog_str(self, t="  "):
		return t + "/*verilator no_inline_module*/\n"

	def connect_all_hbm_connections(self, inst, nodes, input_clock, input_reset, clock_kwargs = None):
		#Check if any layer requires HBM
		hbm_used = False
		for i,n in enumerate(nodes):
			if n.offload_to_hbm == True:
					hbm_used = True	

		# If we're instanting a real HBM IP
		if hbm_used == True and not enable_sim:	
			TOTAL_PSEUDO_CHANNELS = 32
			with dl.ModuleScope(inst(dl.Module, "hbm_connections", kind=("hbm_connections"))):
				#Add HBM
				global WRITE_PATH_NARROW_CONSTANT
				self.hbm = inst(dl.HBM, WRITE_PATH_NARROW_CONSTANT=WRITE_PATH_NARROW_CONSTANT)
				self.hbm.hbm_only_reset.set_driver(self.reset)

				#We need to match layers to their pseudo-channels
				hbm_binary_weights_total = []
				self.hbm_line_count_total = [0]
				for i,n in enumerate(nodes):
					if n.offload_to_hbm == True:
						assert(len(n.module.hbm_interface) > 0)
						for j in n.module.hbm_binary_weights:
							hbm_binary_weights_total.append(j)
						for lc in n.module.hbm_line_count:
							self.hbm_line_count_total.append(self.hbm_line_count_total[-1] + lc)


				# We create a list of interfaces for each controller (not physically bound to any physical location), and assign 3 interfaces per controller
				# Each degree of input or output parallelism requires 1 interface, so one controller can for instance take care of one layer with pi=1,po=1 and one layer where pi=1, po=2
				hbm_channels_controllers = [[] for i in range(TOTAL_PSEUDO_CHANNELS)]
				hbm_controller_counter = 0
				for i,n in enumerate(nodes):
					if n.offload_to_hbm == True:
						assert(len(n.module.hbm_interface) > 0)
						for j in n.module.hbm_interface:
							j.assign_start_end_addresses(self.hbm_line_count_total[hbm_controller_counter], self.hbm_line_count_total[hbm_controller_counter+1])
							hbm_channels_controllers[hbm_controller_counter//3].append(j)
							hbm_controller_counter += 1	

				#We then instantiate the controllers (without assigning them to a physical channel), and assign to them the list selected above (3 interfaces per controller)
				hbm_controllers = []
				for hbmc_list in hbm_channels_controllers:
					if len(hbmc_list) > 0:
						hbmc = inst(dl.HBMController, ADDR_WIDTH=23, DATA_WIDTH=hbmc_list[0].DATA_WIDTH + 9) #We set araddr_width to max for simplicity. 9 bits of data are used for ID.
						hbmc.assign_consumers_to_controller(hbmc_list, enable_sim = enable_sim)			 #Padding is taken care of inside assign_consumers_to_controller
						hbm_controllers.append(hbmc)


				#Finally, we assign the pseudo-channel controllers to their final physical locations
				#We proceed clockwise: 0 to 15 means left to right on top hbm, then 31 to 16 means right to left on bottom HBM
				controller_count = 0
				channel_number_int = 0
				for i,hbmc in enumerate(hbm_controllers):
					self.hbm.assign_controller_to_channel(hbmc = hbmc, channel_number = channel_number_int, hbmc_number = i)	
					self.hbm.create_write_path(self.hbm_line_count_total[-1], channel_number_int)
					if (channel_number_int < 15):
						channel_number_int += 1
					elif (channel_number_int == 15):
						channel_number_int = 31
					else:
						channel_number_int -= 1 
					controller_count += 1

				self.hbm.hbm_weights = hbm_binary_weights_total
				HBM_DATA_WIDTH=240 #NOTE: Changing this variable alone will not change the HBM width
				weights_data = inst(dl.Logic, bits=math.ceil(HBM_DATA_WIDTH/WRITE_PATH_NARROW_CONSTANT), name="weights_data") #The width of the write path is dependent on the global variable WRITE_PATH_NARROW_CONSTANT
				weights_valid = inst(dl.Logic, bits=1, name="weights_valid")
				
				weights_data.set_driver(self.placeholder.selected_bits)
				weights_valid.set_driver(self.placeholder.selected_bits_valid.delay(self.placeholder.ram.read_delay))

				max_weight_value = math.ceil(self.hbm_line_count_total[-1]/4704)*4704
				weight_write_counter_out = inst(dl.Counter, reset_value=0, increment_value=1, end_value=(max_weight_value), name="weight_write_counter_out")
				weight_write_counter_out_not_done = inst(dl.NOT, weight_write_counter_out.is_done, name="weight_write_counter_out_not_done")
				self.placeholder.weight_transfer_out.set_driver(weight_write_counter_out_not_done)
				weight_write_counter_out.set_increment_condition(inst(dl.AND, self.placeholder.increment_weight_out, weight_write_counter_out_not_done))
				
				with dl.ParamScope(clock=input_clock, reset=input_reset):
					max_weight_image_value = math.ceil(self.hbm_line_count_total[-1]/4704)
					weight_write_counter_in = inst(dl.Counter, reset_value=0, increment_value=1, end_value=(max_weight_image_value), name="weight_write_counter_in")
					weight_write_counter_in_not_done = inst(dl.NOT, weight_write_counter_in.is_done, name="weight_write_counter_in_not_done")
					self.placeholder.weight_transfer_in.set_driver(weight_write_counter_in_not_done.delay(5))
					weight_write_counter_in.set_increment_condition(inst(dl.AND, self.placeholder.increment_weight_in, weight_write_counter_in_not_done))


				#The write path for the top HBM stack
				write_path_data_shifter = inst(dl.BasicShiftRegister,
					bits=(math.ceil(HBM_DATA_WIDTH/WRITE_PATH_NARROW_CONSTANT)),
					depth=24,
					name="write_path_data_shifter")	

				write_path_valid_shifter = inst(dl.BasicShiftRegister,
					bits=(1),
					depth=24,
					name="write_path_valid_shifter")	

				write_path_data_shifter.d.set_driver(weights_data)
				write_path_valid_shifter.d.set_driver(weights_valid)

				for i in range(TOTAL_PSEUDO_CHANNELS//2):
					# We connect the different pseudo-channels of the top HBM stack to the shift register on the write path
					self.hbm.wire_write_path(i, write_path_data_shifter.sr[8+i], write_path_valid_shifter.sr[8+i])

				#The write path for the bottom HBM stack
				write_path_data_shifter_bot = inst(dl.BasicShiftRegister,
					bits=(math.ceil(HBM_DATA_WIDTH/WRITE_PATH_NARROW_CONSTANT)),
					depth=20,
					name="write_path_data_shifter_bot")	

				write_path_valid_shifter_bot = inst(dl.BasicShiftRegister,
					bits=(1),
					depth=20,
					name="write_path_valid_shifter_bot")	

				write_path_data_shifter_bot.d.set_driver(weights_data)
				write_path_valid_shifter_bot.d.set_driver(weights_valid)

				for i in range(TOTAL_PSEUDO_CHANNELS//2):
					# We connect the different pseudo-channels of the bottom HBM stack to the shift register on the write path
					self.hbm.wire_write_path((16+i), write_path_data_shifter_bot.sr[4+i], write_path_valid_shifter_bot.sr[4+i])
		elif hbm_used == True and enable_sim:
			#We don't instantiate an HBM module, instead we use the FakeHBMChannel which simulates a single Pseudo-Channel
			zero_weight_transfer = inst(dl.Constant, bits=1, value=0, name="zero_weight_transfer")
			self.placeholder.weight_transfer_out.set_driver(zero_weight_transfer)
			self.placeholder.weight_transfer_in.set_driver(zero_weight_transfer)

			#Collect all the weights we need to store on fake hbm channels
			hbm_channels = []
			seed(RANDOM_SEED)

			hbm_content_buckets = []
			hbmc_list_buckets = []
			counter_to_3 = 0
			bucket_index = 0
			start_address = 0
			end_address = 0
			for i,n in enumerate(nodes):
				if n.offload_to_hbm == True:					
					for j,hbmc_interface in enumerate(n.module.hbm_interface):
						if counter_to_3 == 3:
							bucket_index += 1
							counter_to_3 = 0
							start_address = 0
							end_address = 0
						
						for line_index in range(len(n.module.hbm_binary_weights[j].bit_array)):
							n.module.hbm_binary_weights[j].bit_array[line_index][30] = np.uint8(counter_to_3)

						if (counter_to_3 == 0):
							hbm_content_buckets.append(n.module.hbm_binary_weights[j])
						else:
							hbm_content_buckets[bucket_index].bit_array  = np.append(hbm_content_buckets[bucket_index].bit_array, n.module.hbm_binary_weights[j].bit_array, axis=0)
						
						end_address = len(hbm_content_buckets[bucket_index])
						hbmc_interface.assign_start_end_addresses(start_address, end_address)
						start_address = len(hbm_content_buckets[bucket_index])

						# Put interfaces in buckets of 3, to later be assigned to controllers
						if counter_to_3 == 0:
							hbmc_list_buckets.append([hbmc_interface])
						else:
							hbmc_list_buckets[bucket_index].append(hbmc_interface)
						

						counter_to_3 += 1

			for i,hbmc_list in enumerate(hbmc_list_buckets):
				# Connect each bucket of 3 to a controller, then connect the controller to a fakeHBMChannel with a given seed
				hbmc = inst(dl.HBMController, ADDR_WIDTH=23, DATA_WIDTH=hbmc_list[0].DATA_WIDTH + 9)
				hbmc.assign_consumers_to_controller(hbmc_list, enable_sim = enable_sim)
				hbm_channel = inst(dl.FakeHBMchannel, hbm_content = hbm_content_buckets[i], given_seed=randint(1000,10000))

				#Resolve the non-axi signals of the hbm controller
				fake_hbm_clk_propagation = inst(dl.Logic, bits=1, name="fake_hbm_clk_propagation_"+str(j))
				if clock_kwargs is not None:
					system_clk = clock_kwargs
					fake_hbm_clk_propagation.set_driver(system_clk)
				hbmc.hbm_interface_clk.set_driver(fake_hbm_clk_propagation)
				hbmc.hbm_interface_reset.set_driver(self.reset)
				hbmc.hbm_writing_is_done.set_driver(inst(dl.Constant, bits=1, value=1))

				#Connect the axi signals of the hbm controller
				# read address
				hbm_channel.axi_araddr.set_driver(hbmc.axi_araddr)
				hbm_channel.axi_arvalid.set_driver(hbmc.axi_arvalid)
				hbmc.axi_arready.set_driver(hbm_channel.axi_arready)

				# read data
				hbmc.axi_rdata.set_driver(hbm_channel.axi_rdata)
				hbmc.axi_rvalid.set_driver(hbm_channel.axi_rvalid)
				hbm_channel.axi_rready.set_driver(hbmc.axi_rready)

				hbm_channels.append(hbm_channel)	
		else:
			zero_weight_transfer = inst(dl.Constant, bits=1, value=0, name="zero_weight_transfer")
			self.placeholder.weight_transfer_out.set_driver(zero_weight_transfer)
			self.placeholder.weight_transfer_in.set_driver(zero_weight_transfer)


	def write_stats_to_dir(self, directory):
	 #Marius:  Create output directory if it does not exist 
		if not os.path.exists(directory):
			Path(directory).mkdir(parents=True, exist_ok=True)
		
		path = directory + "/utilization_estimates.txt"
		print("Writing Utilization Estimates to " + path)
		with open(path, "w") as fh:
			for n in self.nodes:
				if len(list(n.module.utilization_estimates.keys())) > 0:
					fh.write(n.tf_op.name + ": " + pprint.pformat(n.module.utilization_estimates) + "\n")


class HPIPETLM(dl.Module):
	def __init__(self, module_name, kind, **kwargs):
		super(HPIPETLM, self).__init__(module_name, kind=kind, **kwargs)

	def dump_tensorflow_activations(self):
		print_stage_header("Writing TensorFlow Outputs For Nodes in Graph.\nIf multiple example images were specified, defaulting to the first one.")
		os.makedirs("generated_files/layer_images/tensorflow", exist_ok=True)
		for n in self.hpipe.nodes:
			output_image = n.example_outputs[0]
			#print("NODE OF INTEREST SHAPE: " + str(output_image.shape))
			output_shape = output_image.shape
			output_image = np.reshape(output_image[0], output_image.shape[1:])
			output_image = np.transpose(output_image, [0,2,1])
			output_image = np.reshape(output_image, [np.prod(output_image.shape[:2]),output_image.shape[-1]])
			f_name = "generated_files/layer_images/tensorflow/" + n.module.name + ".csv"
			print("  Generating " + f_name)
			with open(f_name, "w") as f:
				f.write(str(output_shape[-1]) + "\n")
				for i in range(output_image.shape[0]):
					f.write(",".join([str(d) for d in list(output_image[i])]) + "\n")


class QuartusTop(HPIPETLM):
	def __init__(self, plan, build_from=None, build_until=None, debug_individual_stage=None, **kwargs):
		super(QuartusTop, self).__init__("quartus_top", kind="quartus_top", **kwargs)
		inst = self.inst
		with dl.ModuleScope(self):
			clock = inst(dl.Clock, name="sys_clock")
			clock2x = inst(dl.Clock, name="sys_clock_2x")
			reset = inst(dl.Logic, name="reset", bits=1, signed=False)
			self.clock = clock
			self.reset = reset
			with dl.ParamScope(clock=clock, reset=reset, write_port_width=4, clock2x=clock2x):
				hpipe = inst(HPipe, plan, build_from=build_from, build_until=build_until, debug_individual_stage=debug_individual_stage)
				self.hpipe = hpipe
				write = inst(dl.Constant, value=1, bits=1)
				input_data = inst(dl.Constant, value=1, bits=hpipe.placeholder.inputs[0]._bit_width)
				with dl.ModuleScope(hpipe):
					hpipe.placeholder.inputs[0].set_driver(input_data.delay(10))
					hpipe.placeholder.write.set_driver(write.delay(10))
					if len(hpipe.output_modules[0].outputs) == 1:
						output_data = hpipe.output_modules[0].outputs[0].delay(10)
					else:
						output_sr = inst(dl.ParallelLoadShiftRegister, bits=hpipe.output_modules[0].outputs[0]._bit_width, depth=len(hpipe.output_modules[0].outputs))
						for l,v in zip(output_sr.loads, hpipe.output_modules[0].outputs_valid):
							l.set_driver(v)
						for i,d in zip(output_sr.input_data, hpipe.output_modules[0].outputs):
							i.set_driver(d)
						output_data = output_sr.sr[-1].delay(10)
					output_valid = hpipe.output_modules[0].outputs_valid[0].delay(10)
					space_to_write_line = hpipe.placeholder.space_to_write_line.delay(10)
				top_output_data = output_data.delay(1)
				top_output_valid = output_valid.delay(1)
				space_to_write_line_top = space_to_write_line.delay(1)

class SystemBuilderComponent(HPIPETLM):
	def __init__(self, plan, build_from=None, build_until=None, debug_individual_stage=None,clk =None, io_clk=None, pll_ref_clk=None, pll_ref_clk_bot=None, rst=None, physical_hbm = False, **kwargs):
		super(SystemBuilderComponent, self).__init__("system_builder_component", kind="system_builder_component", **kwargs)
		inst = self.inst
		with dl.ModuleScope(self):
			
			clock2x = inst(dl.Clock, name="sys_clock_2x")
			
			
			if clk == None:
				clock = inst(dl.Clock, name="sys_clock")
				self.clock = clock
			else:
				self.clock = clk

			if io_clk == None:
				io_clock = inst(dl.Clock, name="io_clock")
				self.io_clock = io_clock
			else:
				self.io_clock = io_clk

			if pll_ref_clk == None:
				pll_ref_clk = inst(dl.Clock, half_period = 6, name="pll_ref_clk")
				self.pll_ref_clk = pll_ref_clk
			else:
				self.pll_ref_clk = pll_ref_clk

			if pll_ref_clk_bot == None:
				pll_ref_clk_bot = inst(dl.Clock, half_period = 6, name="pll_ref_clk_bot")
				self.pll_ref_clk_bot = pll_ref_clk_bot
			else:
				self.pll_ref_clk_bot = pll_ref_clk_bot
			if rst == None:
				reset = inst(dl.Logic, name="reset", bits=1, signed=False)
				self.reset = reset
			else:
				self.reset = rst

			#Extra Signals for HBM
			if physical_hbm:
				self.reset_release = inst(dl.Logic, name="reset_release", bits=1, signed=False)
				self.ext_core_clk_locked = inst(dl.Logic, name="ext_logic_clk_locked", bits=1, signed=False)
				#Mem_m2u_bridge
				self.bridge_shiftwr = inst(dl.Logic, signed=False, bits=1, name="mem_m2u_bridge_shiftwr")
				self.bridge_updatewr = inst(dl.Logic, signed=False, bits=1, name="mem_m2u_bridge_updatewr")
				self.bridge_temp = inst(dl.Logic, signed=False, bits=3, name="mem_m2u_bridge_temp")
				self.bridge_wso = inst(dl.Logic, signed=False, bits=8, name="mem_m2u_bridge_wso")
				self.bridge_cattrip = inst(dl.Logic, signed=False, bits=1, name="mem_m2u_bridge_cattrip")
				self.bridge_wrck = inst(dl.Logic, signed=False, bits=1, name="mem_m2u_bridge_wrck")
				self.bridge_wrst_n = inst(dl.Logic, signed=False, bits=1, name="mem_m2u_bridge_wrst_n")
				self.bridge_reset_n = inst(dl.Logic, signed=False, bits=1, name="mem_m2u_bridge_reset_n")
				self.bridge_capturewr = inst(dl.Logic, signed=False, bits=1, name="mem_m2u_bridge_capturewr")
				self.bridge_selectwir = inst(dl.Logic, bits=1, name="mem_m2u_bridge_selectwir")	
				self.bridge_wsi = inst(dl.Logic, bits=1, name="mem_m2u_bridge_wsi")
				self.bridge_shiftwr_bot = inst(dl.Logic, signed=False, bits=1, name="mem_m2u_bridge_shiftwr_bot")
				self.bridge_updatewr_bot = inst(dl.Logic, signed=False, bits=1, name="mem_m2u_bridge_updatewr_bot")
				self.bridge_temp_bot = inst(dl.Logic, signed=False, bits=3, name="mem_m2u_bridge_temp_bot")
				self.bridge_wso_bot = inst(dl.Logic, signed=False, bits=8, name="mem_m2u_bridge_wso_bot")
				self.bridge_cattrip_bot = inst(dl.Logic, signed=False, bits=1, name="mem_m2u_bridge_cattrip_bot")
				self.bridge_wrck_bot = inst(dl.Logic, signed=False, bits=1, name="mem_m2u_bridge_wrck_bot")
				self.bridge_wrst_n_bot = inst(dl.Logic, signed=False, bits=1, name="mem_m2u_bridge_wrst_n_bot")
				self.bridge_reset_n_bot = inst(dl.Logic, signed=False, bits=1, name="mem_m2u_bridge_reset_n_bot")
				self.bridge_capturewr_bot = inst(dl.Logic, signed=False, bits=1, name="mem_m2u_bridge_capturewr_bot")
				self.bridge_selectwir_bot = inst(dl.Logic, bits=1, name="mem_m2u_bridge_selectwir_bot")	
				self.bridge_wsi_bot = inst(dl.Logic, bits=1, name="mem_m2u_bridge_wsi_bot")

			
			with dl.ParamScope(clock=self.clock, clock2x=clock2x, pll_ref_clk=pll_ref_clk, pll_ref_clk_bot=pll_ref_clk_bot, reset=self.reset, write_port_width=4, extend_to_match_bits=True): #, rom_path_modifier="quartus_circuit/"
				self.instantiate(inst, plan, build_from, build_until, debug_individual_stage, physical_hbm=physical_hbm, **kwargs)

	def compute_transfer_info(self, h, w, c, b, transfer_bits):
		width_bits = w * b
		is_deserialized = True
		if width_bits >= transfer_bits:
			is_deserialized = False

		lines_per_width = math.ceil(width_bits/float(transfer_bits))
		acts_per_line = transfer_bits//width_bits
		if is_deserialized:
			total_lines = math.ceil(c*h/float(acts_per_line))
		else:
			total_lines = c*h*lines_per_width

		return (total_lines * (transfer_bits // 8), total_lines)

	def instantiate(self, inst, plan, build_from, build_until, debug_individual_stage, directly_propagate_outputs_to_top=False, physical_hbm = False, **kwargs):
		hpipe = inst(HPipe, plan, build_from=build_from, build_until=build_until, debug_individual_stage=debug_individual_stage, input_clock=self.io_clock, **kwargs)
		self.hpipe = hpipe
		if "partitioning" in kwargs:
			partitioning = kwargs["partitioning"]
		else:
			partitioning = False

		#If partitioning is True, no need to continue the flow
		if partitioning :
			return  
		
		# We have two interfaces currently for sending data to/from HPIPE
		# 1) PCIe and  it's using 256-bits
		# 2) Ethernet, and it's using 512-bits

		if "interface_width" in kwargs:
			interface_width = kwargs["interface_width"]
		else:
			interface_width = 256

		# The instance_location parameter is for Multi Chip Development. it definesthe location of the HPIPE instance
		# Location could be "input", "intermediate" or "output"
		if "instance_location" in kwargs:
			instance_location = kwargs["instance_location"]
		else:
			instance_location = "input"


		# For Multi FPGA HPIPE, chip_count will be > 1
		if "chip_count" in kwargs:
			chip_count = kwargs["chip_count"]
		else:
			chip_count = 1

		bits_per_output = hpipe.output_modules[0].outputs[0]._bit_width
		outputs_per_line = interface_width // bits_per_output
		ram_bit_width = outputs_per_line * bits_per_output

		input_act_width = hpipe.placeholder.get_output_act(dim="width")
		input_act_height = hpipe.placeholder.get_output_act(dim="height")
		input_act_channels = hpipe.placeholder.get_output_act(dim="channels")
		input_bit_spec = dl.Verilog.current_params.kwargs["input_bit_spec"]
		input_act_bits = np.sum(list(input_bit_spec.values()))

		output_act_width = hpipe.output_modules[0].get_output_act(dim="width")
		output_act_height = hpipe.output_modules[0].get_output_act(dim="height")
		output_act_channels = hpipe.output_modules[0].get_output_act(dim="channels")
		output_act_bits = hpipe.output_modules[0].get_bit_spec("output")["width"]

		output_bytes, output_lines = self.compute_transfer_info(output_act_height, output_act_width, output_act_channels, output_act_bits, interface_width)
		input_bytes, input_lines = self.compute_transfer_info(input_act_height, input_act_width, input_act_channels, input_act_bits, interface_width)

		width_lines = math.ceil(hpipe.output_modules[0].get_output_act(dim="width") / float(outputs_per_line))

		shifter_depth = width_lines
		if width_lines == 1: # The output_width * bits <= interface_width
			shifter_depth = interface_width // (hpipe.output_modules[0].get_output_act(dim="width")*bits_per_output)
		
		print(str(shifter_depth)  + " " +str(hpipe.output_modules[0].get_output_act(dim="width")*bits_per_output))
		output_buffer_depth = output_lines
		parallel_out_count = min(width_lines, output_act_width)
		shifter_bit_width = min(ram_bit_width, bits_per_output*output_act_width)

		self.interface = {
			"addr" : inst(dl.Logic, bits=20, name="addr"),
			"data" : inst(dl.Logic, bits=interface_width, name="data"),
			"valid" : inst(dl.Logic, bits=1, name="valid"),
			"chipselect" : inst(dl.Logic, bits=1, name="chipselect"),
			"waitrequest" : inst(dl.Logic, bits=1, name="waitrequest"),

			"addr2" : inst(dl.Logic, bits=max(1,clog2(output_buffer_depth)), name="addr2"),
			"data2" : inst(dl.Logic, bits=interface_width, name="data2"),
			"valid2" : inst(dl.Logic, bits=1, name="valid2"),
			"chipselect2" : inst(dl.Logic, bits=1, name="chipselect2"),
			"waitrequest2" : inst(dl.Logic, bits=1, name="waitrequest2"),

			"addr3" : inst(dl.Logic, bits=clog2(10), name="addr3"),
			"data3" : inst(dl.Logic, bits=32, name="data3"),
			"valid3" : inst(dl.Logic, bits=1, name="valid3"),
			"chipselect3" : inst(dl.Logic, bits=1, name="chipselect3")
		}

		#Connect the HBM signals here
		if physical_hbm == True:
			hpipe.hbm.ext_logic_clk_locked.set_driver(self.ext_core_clk_locked)
			hpipe.hbm.rst_n.set_driver(self.reset_release)

			hpipe.hbm.bridge_temp.set_driver(self.bridge_temp)
			hpipe.hbm.bridge_wso.set_driver(self.bridge_wso)
			hpipe.hbm.bridge_cattrip.set_driver(self.bridge_cattrip)
			self.bridge_shiftwr.set_driver(hpipe.hbm.bridge_shiftwr)
			self.bridge_updatewr.set_driver(hpipe.hbm.bridge_updatewr)
			self.bridge_wrck.set_driver(hpipe.hbm.bridge_wrck)
			self.bridge_wrst_n.set_driver(hpipe.hbm.bridge_wrst_n)
			self.bridge_reset_n.set_driver(hpipe.hbm.bridge_reset_n)
			self.bridge_capturewr.set_driver(hpipe.hbm.bridge_capturewr)
			self.bridge_selectwir.set_driver(hpipe.hbm.bridge_selectwir)
			self.bridge_wsi.set_driver(hpipe.hbm.bridge_wsi)

			hpipe.hbm.bridge_temp_bot.set_driver(self.bridge_temp_bot)
			hpipe.hbm.bridge_wso_bot.set_driver(self.bridge_wso_bot)
			hpipe.hbm.bridge_cattrip_bot.set_driver(self.bridge_cattrip_bot)
			self.bridge_shiftwr_bot.set_driver(hpipe.hbm.bridge_shiftwr_bot)
			self.bridge_updatewr_bot.set_driver(hpipe.hbm.bridge_updatewr_bot)
			self.bridge_wrck_bot.set_driver(hpipe.hbm.bridge_wrck_bot)
			self.bridge_wrst_n_bot.set_driver(hpipe.hbm.bridge_wrst_n_bot)
			self.bridge_reset_n_bot.set_driver(hpipe.hbm.bridge_reset_n_bot)
			self.bridge_capturewr_bot.set_driver(hpipe.hbm.bridge_capturewr_bot)
			self.bridge_selectwir_bot.set_driver(hpipe.hbm.bridge_selectwir_bot)
			self.bridge_wsi_bot.set_driver(hpipe.hbm.bridge_wsi_bot)

		with dl.ModuleScope(hpipe):
			with dl.ParamScope(reset=hpipe.reset):
				hpipe.alternate_reset.set_driver(inst(dl.AND, self.interface["chipselect3"], self.interface["valid3"], inst(dl.EQ, self.interface["addr3"], 8)))
				# For Multiple chip set up, any chip other than the first chip will have the placeholder just as a passing 
				# module to the next layer and thus it will be in the sys_clk domain (It will not have any buffers)
				if  instance_location != "input":
					placeholder_clock = self.clock
				else:
					placeholder_clock = self.io_clock
				with dl.ParamScope(clock=placeholder_clock):
					hpipe.placeholder.inputs[0].set_driver(self.interface["data"][hpipe.placeholder.inputs[0]._bit_width-1:0].delay(10))
					hpipe.placeholder.write.set_driver(inst(dl.AND, self.interface["valid"], self.interface["chipselect"]).delay(10))
					hpipe.placeholder.write_addr.set_driver(self.interface["addr"], delay=10)
					address_copy_to_keep_in_port_list = inst(dl.Logic, bits=20)
					address_copy_to_keep_in_port_list.set_driver(self.interface["addr"], delay=10)

				with dl.ParamScope(reset_driver=inst(dl.Constant, value=0, bits=1), clock=self.io_clock, reset=hpipe.input_reset):
					status_register_components = {
						"has_space_to_write" : inst(dl.Flop, bits=1, name="has_space_to_write"),
						"output_data_ready" : inst(dl.Flop, bits=1, name="output_data_ready")
					}
				status_register_order = [
					"has_space_to_write",
					"output_data_ready"
				]
				status_register = inst(dl.Concat, *[status_register_components[k] for k in status_register_order], name="status_register")
				self.status_register = status_register
				if status_register._bit_width < 32:
					status_register = inst(dl.Concat,
						status_register,
						inst(dl.Constant, value=0, bits=32-status_register._bit_width),
						name="padded_status_register")
				with dl.ParamScope(clock=self.io_clock, reset=hpipe.input_reset):
					status_reg_delay = 5
					global enable_sim
					if arch.hpipe_device_arch.arch_name == "S10_NX" and not enable_sim:
						status_reg_delay = 1 #Shorter delay for S10_NX since we need to handle it manually in top level module, not sure why we need such a long delay for the GX to begin with...

					status_register_select = inst(dl.MuxV2,
						self.interface["addr3"].delay(status_reg_delay),
						inst(dl.Constant, value=output_act_height, name="output_act_height"),
						inst(dl.Constant, value=output_act_width, name="output_act_width"),
						inst(dl.Constant, value=output_act_channels, name="output_act_channels"),
						inst(dl.Constant, value=output_act_bits, name="output_act_bits"),
						inst(dl.Constant, value=input_act_height, name="input_act_height"),
						inst(dl.Constant, value=input_act_width, name="input_act_width"),
						inst(dl.Constant, value=input_act_channels, name="input_act_channels"),
						inst(dl.Constant, value=input_act_bits, name="input_act_bits"),
						inst(dl.Constant, bits=32, value=0, name="reset_register"),
						status_register,
						extend_to_match_bits=True,
						name="status_register_select")
					
					status_register_pcie_read = status_register_select.delay(status_reg_delay).copy()
					status_register_pcie_read.name = "status_register_pcie_read"		
					self.interface["data3"].set_driver(status_register_pcie_read)

				# I delay this here just so that we have a register instantiated in the HPipe module to add the timing constraint.
				# If you try to use the get_registers tcl command on a wire driven by a register, it doesn't find the register :(
				# Since the hpipe.placeholder.space_to_write_line signal is actually a wire driven by a register I instantiate
				# a new register in HPIPE just so that we have a clean signal to reference with our timing constraints.
				self.placeholder_space_pre_domain_cross = hpipe.placeholder.space_to_write_line.delay(1)
				self.placeholder_space_pre_domain_cross.synthesis_attributes = "synthesis keep"
				with dl.ParamScope(clock=self.io_clock):
					with dl.ParamScope(intended_clock_crossing=True):
						self.placeholder_space_clock_cross_flop = self.placeholder_space_pre_domain_cross.delay(1)
					self.placeholder_space_clock_cross_flop.synthesis_attributes = "synthesis keep"
					status_register_components["has_space_to_write"].set_driver(self.placeholder_space_clock_cross_flop.delay(3))
					# This TLM does not actually get generated.  It instantiates signals both inside and outside the HPIPE module
					# which makes the necessary input and output signals appear in the HPIPE port list.
					# As a result, its get_sdc_constraints_string function never gets called, so have manually added it
					# to a global list of lambdas to call to get more sdc constraints
					dl.Verilog.current_circuit.add_sdc_lambda(lambda: self.get_sdc_constraints_string())
					
				# For multi FPGA, Propagate to top means we are not instantiating an output buffer
				# and that the outputs will be taken to the top module, so we return from this point
				if directly_propagate_outputs_to_top and chip_count > 1:
					return

				# For SSD MobileNet-V1, we actually propagate multiple layer signals to the top module

				elif directly_propagate_outputs_to_top:
					for output in hpipe.output_modules:
						backpressure = inst(dl.Logic, bits=1, name=output.name + "_backpressure")
						about_to_be_valid = inst(dl.Logic, bits=1, name=output.name + "_about_to_be_valid")
						output.can_write_line.set_driver(backpressure)
						about_to_be_valid.set_driver(output.output_valid)
						dl.Verilog.current_circuit.propagate_to_top[backpressure] = "input"
						dl.Verilog.current_circuit.propagate_to_top[about_to_be_valid] = "output"
						all_outputs = inst(dl.Concat, *output.outputs, name=output.name + "_data_outputs")
						all_data_valid_signals = inst(dl.Concat, *output.outputs_valid, name=output.name + "_data_valid_signals")
						dl.Verilog.current_circuit.propagate_to_top[all_outputs] = "output"
						dl.Verilog.current_circuit.propagate_to_top[all_data_valid_signals] = "output"
					return


				output_fifo = inst(dl.FIFO,
					bits=len(hpipe.output_modules[0].outputs) * hpipe.output_modules[0].outputs[0]._bit_width,
					depth=2*hpipe.output_modules[0].get_output_act(dim="channels"),
					almost_empty=0,
					almost_full=hpipe.output_modules[0].get_output_act(dim="channels"))
				output_fifo.w_en.set_driver(hpipe.output_modules[0].outputs_valid[0])
				output_fifo.w_data.set_driver(inst(dl.Concat, *hpipe.output_modules[0].outputs))

				shift_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=shifter_depth-1, name="shift_counter")
				output_channel_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=output_act_height * output_act_channels-1, name="output_channel_counter")
				shift_to_idle_control = shift_counter.is_done
				if width_lines == 1:
					shift_to_idle_control = inst(dl.Constant, bits=1, value=1, name="constant_1")

				wait_for_state_update_counter = inst(dl.Counter,
					reset_value=0,
					increment_value=1,
					end_value=3,
					name="wait_for_state_update_counter")

				waiting_for_readback_to_finish = inst(dl.Logic, bits=1, name="waiting_for_readback_to_finish")
				states = ["IDLE", "READ_FIFO", "SHIFT", "DRAIN_LAST_ELEMENTS", "WAIT_FOR_STATE_UPDATE"]
				edges = [
					["IDLE", "READ_FIFO", inst(dl.AND,
						inst(dl.NOT, output_fifo.empty),
						inst(dl.NOT, waiting_for_readback_to_finish))],
					["READ_FIFO", "SHIFT"],
					["SHIFT", "IDLE", shift_to_idle_control],
					["SHIFT", "DRAIN_LAST_ELEMENTS", inst(dl.AND, output_channel_counter.is_reset_value, inst(dl.NOT, shift_counter.is_done))],
					["DRAIN_LAST_ELEMENTS", "WAIT_FOR_STATE_UPDATE", shift_counter.is_done],
					["WAIT_FOR_STATE_UPDATE", "IDLE", wait_for_state_update_counter.is_done]]

				with dl.ParamScope(clock=self.io_clock):
					last_read_occured = inst(dl.AND, 
						self.interface["chipselect2"],
						self.interface["valid2"],
						inst(dl.EQ, self.interface["addr2"], output_buffer_depth-1),
						name="last_read_occured").delay(4)
				last_read_occured_cross_clock = inst(dl.SingleCycleControlDomainCrossing, self.io_clock, self.clock, clock_a_reset=hpipe.input_reset, clock_b_reset=hpipe.reset)
				last_read_occured_cross_clock.control_in.set_driver(last_read_occured)
				last_read_occured_system_clock = last_read_occured_cross_clock.control_out
				
				state_machine = inst(dl.StateMachine, states, edges, None, name="output_fifo_state_machine")
				wait_for_state_update_counter.set_increment_condition(state_machine.c["is_wait_for_state_update"])
				done_writing_out = inst(dl.AND,
					output_channel_counter.is_reset_value,
					shift_counter.is_done,
					inst(dl.OR, state_machine.c["is_shift"],
						state_machine.c["is_drain_last_elements"]),
					name="done_writing_out")
				done_writing_out_cross_clock = inst(dl.SingleCycleControlDomainCrossing, self.clock, self.io_clock, clock_a_reset=hpipe.reset, clock_b_reset=hpipe.input_reset)
				done_writing_out_cross_clock.control_in.set_driver(done_writing_out)
				done_writing_out_io_clock = done_writing_out_cross_clock.control_out

				with dl.ParamScope(clock=self.io_clock, reset=hpipe.input_reset):
					done_state_machine = inst(dl.StateMachine,
						["OUTPUT_NOT_READY", "OUTPUT_READY"],
						[
							["OUTPUT_NOT_READY", "OUTPUT_READY", done_writing_out_io_clock],
							["OUTPUT_READY", "OUTPUT_NOT_READY", inst(dl.AND, 
																		self.interface["chipselect2"],
																		self.interface["valid2"]).delay(4)]], None, name="done_state_machine")
				status_register_components["output_data_ready"].set_driver(done_state_machine.c["is_output_ready"])



				states = ["WAITING_FOR_OUTPUT_TO_BE_READY", "WAITING_FOR_READBACK_TO_FINISH"]
				edges = [["WAITING_FOR_OUTPUT_TO_BE_READY", "WAITING_FOR_READBACK_TO_FINISH", done_writing_out.delay(1)],
					["WAITING_FOR_READBACK_TO_FINISH", "WAITING_FOR_OUTPUT_TO_BE_READY", last_read_occured_system_clock]]
				system_output_backpressure_state_machine = inst(dl.StateMachine, states, edges, None, name="system_output_backpressure_state_machine")
				waiting_for_readback_to_finish.set_driver(system_output_backpressure_state_machine.c["is_waiting_for_readback_to_finish"])


				should_shift = inst(dl.OR, state_machine.c["is_shift"], state_machine.c["is_drain_last_elements"], name="should_shift")
				shift_counter.set_increment_condition(should_shift)
				output_fifo.r_en.set_driver(state_machine.c["is_read_fifo"], delay=1)
				output_channel_counter.set_increment_condition(state_machine.c["is_read_fifo"])
				output_sr = inst(dl.ParallelLoadShiftRegister, bits=shifter_bit_width, depth=shifter_depth)
				for i in range(width_lines):
					output_sr.loads[i].set_driver(state_machine.c["is_read_fifo"], delay=3)
					hi = min((i+1)*shifter_bit_width-1, bits_per_output*output_act_width-1)
					input_data = output_fifo.r_data[hi:i*shifter_bit_width]
					if hi < ((i+1)*shifter_bit_width-1):
						input_data = inst(dl.Concat, input_data, inst(dl.Constant, value=0, bits=((i+1)*shifter_bit_width-1)-(bits_per_output*output_act_width-1)))
					output_sr.input_data[width_lines-i-1].set_driver(input_data, delay=1)
				for i in range(width_lines, shifter_depth):
					output_sr.loads[i].set_driver(inst(dl.Constant, value=0, bits=1))
					output_sr.input_data[width_lines - i - 1].wave_needs_driver = True
				for i in range(shifter_depth):
					output_sr.shifts[i].set_driver(should_shift, delay=3)


				write_to_output_ram = should_shift
				if width_lines == 1:
					write_to_output_ram = inst(dl.AND, should_shift, shift_counter.is_done)

				output_ram = inst(dl.RAM,
					write_clock=self.clock,
					read_clock=self.io_clock,
					bits=ram_bit_width,
					depth=output_buffer_depth,
					name="output_ram")
				self.output_ram = output_ram
				counter = inst(dl.Counter,
					reset_value=0,
					increment_value=1,
					end_value=output_buffer_depth-1,
					name="output_ram_write_addr")
				counter.set_increment_condition(write_to_output_ram.delay(1))
				output_ram.w_addr.set_driver(counter.current_value.delay(4))
				output_ram.w_en.set_driver(write_to_output_ram.delay(5))
				multi_chip_intermediate_stage_test = False #Turn on to dump the outputs of an intermediate chip to the output buffer
				if multi_chip_intermediate_stage_test:
					test_zeros = inst(dl.Constant,value = 0,bits = 64) #'64' to be changed to the #of zeros for paading
					#'448' to be changed to the #of zeros for paading
					concat_sig = inst(dl.Concat,inst(dl.Concat, *list(reversed(output_sr.sr)))[448-1:0].delay(2),test_zeros)
					output_ram.w_data.set_driver(concat_sig)
				else:
					output_ram.w_data.set_driver(inst(dl.Concat, *list(reversed(output_sr.sr)))[ram_bit_width-1:0].delay(2))
				with dl.ParamScope(clock=self.io_clock):
					output_ram.r_addr.set_driver(self.interface["addr2"].delay(4))
					output_ram.r_en.set_driver(inst(dl.AND, self.interface["chipselect2"], self.interface["valid2"]).delay(4))
					output_data = output_ram.r_data.delay(4).copy()
					if output_data._bit_width < self.interface["data2"]._bit_width:
						output_data = inst(dl.Concat, output_data, inst(dl.Constant, value=0, bits=self.interface["data2"]._bit_width-output_data._bit_width))
					output_data.name="pcie_output_data"
					self.interface["data2"].set_driver(output_data)

				hpipe.output_modules[0].can_write_line.set_driver(inst(dl.NOT, output_fifo.almost_full))
				self.interface["waitrequest"].set_driver(inst(dl.Constant, bits=1, value=0))
				self.interface["waitrequest2"].set_driver(inst(dl.Constant, bits=1, value=0))

	def get_sdc_constraints_string(self):
		pre_cross_filter_string = self.placeholder_space_pre_domain_cross.get_sdc_filter_string() + "[*]"
		post_cross_filter_string = self.placeholder_space_clock_cross_flop.get_sdc_filter_string() + "[*]"
		string = ""

		# The assumption we are making here is that space to write will change faster than the cpu
		# can start to write input data and then check again if it is able to write more data
		# The other assumption we are making is that the signal will deassert for many cycles, then
		# re-assert for many cycles, not toggling at anywhere near the rate of either clock
		# the flops are
		string += "set_max_delay -from [get_registers {" + pre_cross_filter_string + "}]"
		string += " -to [get_registers {" + post_cross_filter_string + "}] 10.000\n"
		# We hold the driving signal for multiple cycles, so there shouldn't be any hold
		# constraint
		string += "set_min_delay -from [get_registers {" + pre_cross_filter_string + "}]"
		string += " -to [get_registers {" + post_cross_filter_string + "}] -2.000\n"
		return string


class NPU_Wrapper(HPIPETLM):
	def __init__(self, plan, build_from=None, build_until=None, debug_individual_stage=None, wait_for_output=False, **kwargs):
		super(NPU_Wrapper, self).__init__("npu_wrapper", kind="npu_wrapper",name="npu_wrapper", **kwargs)
		inst = self.inst
		with dl.ModuleScope(self):
			clock = inst(dl.Clock, name="sys_clk")
			io_clock = inst(dl.Clock, name="io_clk")
			reset = inst(dl.Logic, name="reset", bits=1, signed=False)
			self.clock = clock
			self.io_clock = io_clock
			self.reset = reset


		inst = self.inst
		if "interface_width" in kwargs:
			interface_width = kwargs["interface_width"]
		else:
			interface_width = 256

		with dl.ModuleScope(self):
			#Instantiate HPIPE (within SystemBuilderComponent)
			sbc = inst(SystemBuilderComponent, plan, build_from, build_until, debug_individual_stage, clk = self.clock, io_clk = self.io_clock,rst = self.reset, **kwargs)
			self.hpipe = sbc.hpipe
			interface = sbc.interface
			hpipe_instance_type = "first"
			if "instance_location" in kwargs:
				instance_location = kwargs["instance_location"]
			else:
				instance_location = "input"
			#Input
			if instance_location == "input":
				#Drive the input from the PCIE
				self.interface = {
					"system_builder_component_0_addr": inst(dl.Logic, bits=20, name="system_builder_component_0_addr"),
					"system_builder_component_0_data": inst(dl.Logic, bits=interface_width, name="system_builder_component_0_data"),
					"system_builder_component_0_valid": inst(dl.Logic, bits=1, name="system_builder_component_0_valid"),
					"system_builder_component_0_chipselect": inst(dl.Logic, bits=1, name="system_builder_component_0_chipselect"),

					"system_builder_component_0_addr3": inst(dl.Logic, bits=12, name="system_builder_component_0_addr3"),
					"system_builder_component_0_valid3": inst(dl.Logic, bits=1, name="system_builder_component_0_valid3"),
					"system_builder_component_0_chipselect3": inst(dl.Logic, bits=1, name="system_builder_component_0_chipselect3"),
					"status_register_pcie_read": inst(dl.Logic, bits=interface_width, name="status_register_pcie_read"),
					"out_data" : 		inst(dl.Logic, bits=interface_width, name="out_data"),
					"out_valid" : 		inst(dl.Logic, bits=1, name="out_valid"),
					"out_ready" : 		inst(dl.Logic, bits=1, name="out_ready"),
					"out_almost_full" : inst(dl.Logic, bits=1, name="out_almost_full"),	
					"sys_clk":			inst(dl.Logic, bits=1, name="sys_clk"),
					"io_clk":			inst(dl.Logic, bits=1, name="io_clk"),
					"rst" :				inst(dl.Logic, bits=1, name="rst")
				}
				interface["addr"].set_driver(self.interface["system_builder_component_0_addr"])
				interface["data"].set_driver(self.interface["system_builder_component_0_data"])
				interface["valid"].set_driver(self.interface["system_builder_component_0_valid"])
				interface["chipselect"].set_driver(self.interface["system_builder_component_0_chipselect"])

				#Drive the Status register from the PCIE
				interface["addr3"].set_driver(self.interface["system_builder_component_0_addr3"])
				interface["valid3"].set_driver(self.interface["system_builder_component_0_valid3"])
				interface["chipselect3"].set_driver(self.interface["system_builder_component_0_chipselect3"])
				status_reg_512 = inst(dl.Concat,interface["data3"],inst(dl.Constant,value=0,bits=480))
				self.interface["status_register_pcie_read"].set_driver(status_reg_512)
				
				#Connect the output Data to the Ethernet
				all_outputs = inst(dl.Concat, *sbc.hpipe.output_modules[0].outputs, name=sbc.hpipe.output_modules[0].name + "_data_outputs")
				if (all_outputs._bit_width > interface_width):
					print("Error! Can't transfer outputs with widths more than the interface width.")
					exit(1)
				if all_outputs._bit_width < interface_width:
					self.interface["out_data"].set_driver(inst(dl.Concat,all_outputs,inst(dl.Constant,value=0,bits=interface_width - all_outputs._bit_width)))
				else:
					self.interface["out_data"].set_driver(all_outputs)

				self.interface["out_valid"].set_driver(sbc.hpipe.output_modules[0].outputs_valid[0])

				#Connect the backpressure
				sbc.hpipe.output_modules[0].can_write_line.set_driver(self.interface["out_almost_full"])

				#Connect the reset
				self.reset.set_driver(self.interface["rst"])


				#Mark the signals as ports
				#Clock and reset
				self.interface["sys_clk"].is_input = True
				self.interface["io_clk"].is_input = True
				self.interface["rst"].is_input = True

				#PCIE interface
				self.interface["system_builder_component_0_addr"].is_input  = True
				self.interface["system_builder_component_0_data"].is_input  = True
				self.interface["system_builder_component_0_valid"].is_input  = True
				self.interface["system_builder_component_0_chipselect"].is_input  = True

				self.interface["system_builder_component_0_addr3"].is_input  = True
				self.interface["system_builder_component_0_valid3"].is_input  = True
				self.interface["system_builder_component_0_chipselect3"].is_input  = True
				self.interface["status_register_pcie_read"].is_output  = True

				#Output data (for Ethernet)
				self.interface["out_data"].is_output  = True
				self.interface["out_valid"].is_output  = True
				self.interface["out_almost_full"].is_input = True
				self.interface["out_ready"].is_input = True
				
			elif instance_location == "intermediate":
				self.interface = {
					"in_data" : 		inst(dl.Logic, bits=interface_width, name="in_data"),
					"in_valid" : 		inst(dl.Logic, bits=1, name="in_valid"),
					"in_ready" : 		inst(dl.Logic, bits=1, name="in_ready"),
					"out_data" : 		inst(dl.Logic, bits=interface_width, name="out_data"),
					"out_valid" : 		inst(dl.Logic, bits=1, name="out_valid"),
					"out_ready" : 		inst(dl.Logic, bits=1, name="out_ready"),
					"out_almost_full" : inst(dl.Logic, bits=1, name="out_almost_full"),	
					"sys_clk":			inst(dl.Logic, bits=1, name="sys_clk"),
					"io_clk":			inst(dl.Logic, bits=1, name="io_clk"),
					"rst" :				inst(dl.Logic, bits=1, name="rst")
				}

				#Connect the input Data from Ethernet
				interface["addr"].set_driver(inst(dl.Constant,value=0,bits=20))
				interface["data"].set_driver(self.interface["in_data"])
				valid_chipselect_and_has_space = inst(dl.AND,self.interface["in_valid"],sbc.hpipe.placeholder.space_to_write_line)
				interface["valid"].set_driver(valid_chipselect_and_has_space)
				interface["chipselect"].set_driver(valid_chipselect_and_has_space)
				self.interface["in_ready"].set_driver(sbc.hpipe.placeholder.space_to_write_line)

				#Connect the output Data to the Ethernet
				all_outputs = inst(dl.Concat, *sbc.hpipe.output_modules[0].outputs, name=sbc.hpipe.output_modules[0].name + "_data_outputs")
				if (all_outputs._bit_width > interface_width):
					print("Error! Can't transfer outputs with widths more than the interface width.")
					exit(1)
				if all_outputs._bit_width < interface_width:
					self.interface["out_data"].set_driver(inst(dl.Concat,all_outputs,inst(dl.Constant,value=0,bits=interface_width - all_outputs._bit_width)))
				else:
					self.interface["out_data"].set_driver(all_outputs)

				self.interface["out_valid"].set_driver(sbc.hpipe.output_modules[0].outputs_valid[0])

				#Connect the backpressure
				sbc.hpipe.output_modules[0].can_write_line.set_driver( self.interface["out_almost_full"])

				#Connect the reset
				self.reset.set_driver(self.interface["rst"])


				#Mark the signals as ports
				#Clock and reset
				self.interface["sys_clk"].is_input = True
				self.interface["io_clk"].is_input = True
				self.interface["rst"].is_input = True

				#Input data (for Ethernet)
				self.interface["in_data"].is_input  = True
				self.interface["in_valid"].is_input  = True
				self.interface["in_ready"].is_output = True

				#Output data (for Ethernet)
				self.interface["out_data"].is_output  = True
				self.interface["out_valid"].is_output  = True
				self.interface["out_almost_full"].is_input = True
				self.interface["out_ready"].is_input = True

			else:
				#Drive the output to the PCIE
				self.interface = {
					"in_data" : 		inst(dl.Logic, bits=interface_width, name="in_data"),
					"in_valid" : 		inst(dl.Logic, bits=1, name="in_valid"),
					"in_ready" : 		inst(dl.Logic, bits=1, name="in_ready"),
					"system_builder_component_0_addr2": inst(dl.Logic, bits=20, name="system_builder_component_0_addr2"),
					"pcie_output_data": inst(dl.Logic, bits=interface_width, name="pcie_output_data"),
					"system_builder_component_0_valid2": inst(dl.Logic, bits=1, name="system_builder_component_0_valid2"),
					"system_builder_component_0_chipselect2": inst(dl.Logic, bits=1, name="system_builder_component_0_chipselect2"),

					"system_builder_component_0_addr3": inst(dl.Logic, bits=12, name="system_builder_component_0_addr3"),
					"system_builder_component_0_valid3": inst(dl.Logic, bits=1, name="system_builder_component_0_valid3"),
					"system_builder_component_0_chipselect3": inst(dl.Logic, bits=1, name="system_builder_component_0_chipselect3"),
					"status_register_pcie_read": inst(dl.Logic, bits=interface_width, name="status_register_pcie_read"),
					"sys_clk":			inst(dl.Logic, bits=1, name="sys_clk"),
					"io_clk":			inst(dl.Logic, bits=1, name="io_clk"),
					"rst" :				inst(dl.Logic, bits=1, name="rst")
				}
				interface["addr2"].set_driver(self.interface["system_builder_component_0_addr2"])
				self.interface["pcie_output_data"].set_driver(interface["data2"])
				interface["valid2"].set_driver(self.interface["system_builder_component_0_valid2"])
				interface["chipselect2"].set_driver(self.interface["system_builder_component_0_chipselect2"])

				#Drive the Status register from the PCIE
				interface["addr3"].set_driver(self.interface["system_builder_component_0_addr3"])
				interface["valid3"].set_driver(self.interface["system_builder_component_0_valid3"])
				interface["chipselect3"].set_driver(self.interface["system_builder_component_0_chipselect3"])
				status_reg_512 = inst(dl.Concat,interface["data3"],inst(dl.Constant,value=0,bits=480))
				self.interface["status_register_pcie_read"].set_driver(status_reg_512)
				
				#Connect the input Data from Ethernet
				interface["addr"].set_driver(inst(dl.Constant,value=0,bits=20))
				interface["data"].set_driver(self.interface["in_data"])
				valid_chipselect_and_has_space = inst(dl.AND,self.interface["in_valid"],sbc.hpipe.placeholder.space_to_write_line)
				interface["valid"].set_driver(valid_chipselect_and_has_space)
				interface["chipselect"].set_driver(valid_chipselect_and_has_space)
				self.interface["in_ready"].set_driver(sbc.hpipe.placeholder.space_to_write_line)


				#Connect the reset
				self.reset.set_driver(self.interface["rst"])

				#Mark the signals as ports
				#Clock and reset
				self.interface["sys_clk"].is_input = True
				self.interface["io_clk"].is_input = True
				self.interface["rst"].is_input = True

				#PCIE interface
				self.interface["system_builder_component_0_addr2"].is_input  = True
				self.interface["pcie_output_data"].is_output  = True
				self.interface["system_builder_component_0_valid2"].is_input  = True
				self.interface["system_builder_component_0_chipselect2"].is_input  = True

				self.interface["system_builder_component_0_addr3"].is_input  = True
				self.interface["system_builder_component_0_valid3"].is_input  = True
				self.interface["system_builder_component_0_chipselect3"].is_input  = True
				self.interface["status_register_pcie_read"].is_output  = True

				#Input data (for Ethernet)
				self.interface["in_data"].is_input  = True
				self.interface["in_valid"].is_input  = True
				self.interface["in_ready"].is_output = True



class HPIPETB2(HPIPETLM):
	def __init__(self, plan, build_from=None, build_until=None, debug_individual_stage=None, wait_for_output=False, **kwargs):
		super(HPIPETB2, self).__init__("hpipe_tb", kind="hpipe_tb", **kwargs)
		inst = self.inst
		if "interface_width" in kwargs:
			interface_width = kwargs["interface_width"]
		else:
			interface_width = 256
		self.plan = plan
		with dl.ModuleScope(self):
			clock = inst(dl.Clock)
			self.pll_ref_clk = inst(dl.Clock, name="pll_ref_clk", half_period=6)
			self.pll_ref_clk_bot = inst(dl.Clock, name="pll_ref_clk_bot", half_period=6)
			reset = inst(dl.ForcedSignal, name="reset", bits=1, signed=False)
			locked = inst(dl.ForcedSignal, name="ext_logic_clk_locked", bits=1, signed=False)
			self.clock = clock
			self.reset = reset
			reset.add_value(1, 0)
			reset.add_value(0, 100)
			locked.add_value(0,0)
			locked.add_value(1,300)
			with dl.ParamScope(clock=clock, reset=reset):
				sbc = inst(SystemBuilderComponent, plan, build_from, build_until, debug_individual_stage, **kwargs)
				sbc.reset.set_driver(reset)
				sbc.clock.set_driver(self.clock)
				sbc.io_clock.set_driver(self.clock)
				if (hasattr(sbc.hpipe, "hbm")):
					if len(sbc.hpipe.hbm.hbm_weights) > 0:
						self.hbm_used = True
					else:
						self.hbm_used = False
				else:
					self.hbm_used = False

				if self.hbm_used == True and not enable_sim:
					sbc.pll_ref_clk.set_driver(self.pll_ref_clk)
					sbc.pll_ref_clk_bot.set_driver(self.pll_ref_clk_bot)
					sbc.hpipe.hbm.ext_logic_clk_locked.set_driver(locked)
					sbc.hpipe.hbm.rst_n.set_driver(inst(dl.NOT, reset))
					self.hbm_binary_weights_total = sbc.hpipe.hbm.hbm_weights
					self.hbm_line_count_total = sbc.hpipe.hbm_line_count_total[-1]
			# the reset we defined above is an external reset that only resets a reset controller within HPIPE.
			# After that reset is deasserted, an internally generated reset is asserted for some number of cycles
			reset = sbc.hpipe.reset
			self.hpipe = sbc.hpipe
			interface = sbc.interface

			# At the moment we only support one placeholder.  The plan is to support multiple
			# and use a memory mapped interface where address 0 is the first placeholder,
			# address 1 is the second, etc.  The placeholders use a FIFO interface, so they don't
			# actually accept an address.

			with dl.ParamScope(clock=clock, reset=reset):
				images_to_run = 2
				serial_inputs = self.get_serialized_inputs(build_from is not None,interface_width=interface_width)

				writing_input = inst(dl.Logic, bits=1, name="writing_input")
				for n, si in serial_inputs.items():
					input_rom = inst(dl.ROM, name="test_input_image", bits=si.bits, content_list=si)
					input_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=len(si)-1)
					input_rom.r_addr.set_driver(input_counter.current_value)
					input_rom.r_en.set_driver(writing_input)
					input_counter.set_increment_condition(writing_input)

				if self.hbm_used == True and not enable_sim:
					#If we use the real HBM IP, we need to create ROMs that hold the weights before writing them to HBM, and send the weights as extra images at the beginning
					extra_image_count, hbm_serial_weights = self.get_hbm_serialized_weights(build_from is not None, interface_width=interface_width)
					weight_roms = [inst(dl.ROM, name="weight_rom_" +str(i), bits=interface_width, content_list=ba) for i,ba in enumerate(hbm_serial_weights)]
					weight_input_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=len(hbm_serial_weights[0])-1)
					for i in range(len(weight_roms)):
						weight_roms[i].r_addr.set_driver(weight_input_counter.current_value)
						weight_roms[i].r_en.set_driver(writing_input)
					weight_input_counter.set_increment_condition(writing_input)
					images_to_run += extra_image_count
				else:
					extra_image_count = 0

				if input_counter.current_value._bit_width < interface["addr"]._bit_width:
					padded_counter = inst(dl.Concat, input_counter.current_value, inst(dl.Constant, value=0, bits=interface["addr"]._bit_width-input_counter.current_value._bit_width))

				written_image_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=images_to_run, name="written_image_counter")
				written_image_counter.set_increment_condition(input_counter.is_done)

				if self.hbm_used == True and not enable_sim:
					original_images_to_run = images_to_run - extra_image_count
					input_r_data_duplicated = [input_rom.r_data for i in range(original_images_to_run)]
					weight_rom_data = [wr.r_data for wr in weight_roms]
					weight_and_input_steering = inst(dl.MuxV2, written_image_counter.current_value.delay(1), *weight_rom_data, *input_r_data_duplicated)
				else:
					weight_and_input_steering = input_rom.r_data

				interface["addr"].set_driver(padded_counter)
				start_writing = inst(dl.Logic, bits=1, name="start_writing")
				output_ready = interface["data3"][1]
				ready_to_accept_input = interface["data3"][0]
				states = ["IDLE", "WRITING_INPUT"]
				edges = [
					["IDLE", "WRITING_INPUT", inst(dl.AND, start_writing, inst(dl.NOT, written_image_counter.is_done))],
					["WRITING_INPUT", "IDLE", input_counter.is_done]]
				state_machine = inst(dl.StateMachine, states, edges, None)
				output_counter = inst(dl.Counter,
					reset_value=0,
					increment_value=1,
					end_value=sbc.output_ram.depth-1,
					name="output_counter")

				readback_states = ["IDLE", "READING_BACK", "DONE_READING_BACK"]
				readback_edges = [
					["IDLE", "READING_BACK", output_ready],
					["READING_BACK", "DONE_READING_BACK", output_counter.is_done],
					["DONE_READING_BACK", "IDLE"]]
				readback_state_machine = inst(dl.StateMachine, readback_states, readback_edges, None, name="readback_state_machine")
				output_counter.set_increment_condition(readback_state_machine.is_reading_back)
				writing_input.set_driver(state_machine.c["is_writing_input"])
				in_pipe_counter = inst(dl.MultiChangeCounter,
					reset_value=0,
					change_values=[1,-1],
					end_value=15,
					name="in_pipe_counter")
				in_pipe_counter.change_signals[0].set_driver(input_counter.is_done)
				in_pipe_counter.change_signals[1].set_driver(readback_state_machine.c["is_done_reading_back"])

				if wait_for_output:
					start_writing.set_driver(inst(dl.AND, in_pipe_counter.is_reset_value, ready_to_accept_input, state_machine.c["is_idle"]))
				else:
					start_writing.set_driver(inst(dl.AND, ready_to_accept_input, state_machine.c["is_idle"]))


				interface["addr3"].set_driver(inst(dl.Constant, value=9, bits=interface["addr3"]._bit_width))
				interface["addr"].set_driver(inst(dl.Concat, input_counter.current_value, inst(dl.Constant, bits=10, value=0)).delay(1))
				interface["valid"].set_driver(writing_input.delay(1))
				interface["chipselect"].set_driver(writing_input.delay(1))
				interface["data"].set_driver(weight_and_input_steering)
				interface["valid2"].set_driver(readback_state_machine.is_reading_back)
				interface["chipselect2"].set_driver(readback_state_machine.is_reading_back)
				interface["addr2"].set_driver(output_counter.current_value)
				done_image_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=(images_to_run-extra_image_count-1), name="done_image_counter")
				done_image_counter.set_increment_condition(readback_state_machine.is_done_reading_back)

				interface["data2"].copy()
				self.done_simulation = done_image_counter.done_and_incrementing.copy()

	def get_serialized_inputs(self, build_from_not_none,interface_width=256):
		example_inputs = []
		inputs_per_transfer = []
		used_bits_per_transfer = []
		bits_per_input = []
		placeholder_names = []

		for n in self.plan.graph.nodes:
			if n.type == "Placeholder":
				placeholder_names.append(n.tf_op.name)
				example_inputs.append(n.example_outputs[0])
				bits = n.get_bits(is_act=True)
				bits_per_input.append(bits)
				inputs_per_transfer.append(interface_width // bits)
				used_bits_per_transfer.append(inputs_per_transfer[-1] * bits)
		if build_from_not_none:
			p = [n for n in self.plan.graph.nodes if n.tf_op.name == placeholder_names[0]][0]
			input_bit_spec = p.precision_parameters
			example_inputs = [get_quantized(self.plan.graph.example_input, *[input_bit_spec[k] for k in ["a_sign_bits", "a_int", "a_f"]])]
		else:
			input_bit_spec = dl.Verilog.current_params.kwargs["input_bit_spec"]
			bits = np.sum(list(input_bit_spec.values()))
			inputs_per_transfer = [interface_width // bits]
			used_bits_per_transfer = [inputs_per_transfer[0] * bits]
			bits_per_input = [bits]
			example_inputs = [get_quantized(self.plan.graph.example_input, *[input_bit_spec[k] for k in ["sign", "int", "frac"]])]


		byte_arrays = {}
		for i,p,u,b,n in zip(example_inputs, inputs_per_transfer, used_bits_per_transfer, bits_per_input, placeholder_names):
			# Convert example image to byte array and serialize it to fit over the interface width
			BHCW = np.transpose(i, [0,1,3,2])
			H = BHCW.shape[1]
			C = BHCW.shape[2]
			W = BHCW.shape[3]

			# This pads the width dimension so that the width can be evenly divisible by the number of elements per transfer
			# the additional zeros get discarded by the Placeholder deserializer
			#
			BHCWP = np.pad(BHCW, [[0,0],[0,0],[0,0],[0, W % p]], mode="constant", constant_values=0)
			# This merges the BHCW dimensions so that we have a fully serialized image that is of shape 
			# [PADDED_ELEMENT_COUNT / inputs_per_transfer, inputs_per_transfer]
			LI = np.reshape(BHCWP, [BHCWP.size // p, p])

			# Now we create a BitArray for each vector in LI and combine them to get the full BitArray
			test_in = [BitArray(bits=b, value_vector=LI[:,l]) for l in range(LI.shape[-1])]
			tmp = test_in[-1]
			for t in reversed(test_in[:-1]):
				tmp += t
			test_in = tmp#BitArray(bits=tmp.bits, bit_array=tmp)
			if test_in.bits != interface_width:
				test_in += BitArray(bits=interface_width - test_in.bits, length=test_in.bit_array.shape[0])

			byte_arrays[n] = test_in
		return byte_arrays

	def get_hbm_serialized_weights(self, build_from_not_none, interface_width=256):
		# This function concatenates all the weights that must be sent to HBM and divides them into input images of size 224x224x3
		assert(interface_width == 256) #We're only going to support 256 for now

		example_inputs = []
		inputs_per_transfer = []
		placeholder_names = []

		#We calculate the depth of the serialized inputs
		for n in self.plan.graph.nodes:
			if n.type == "Placeholder":
				placeholder_names.append(n.tf_op.name)
				example_inputs.append(n.example_outputs[0])
				bits = n.get_bits(is_act=True)
				inputs_per_transfer.append(interface_width // bits)
		if build_from_not_none:
			p = [n for n in self.plan.graph.nodes if n.tf_op.name == placeholder_names[0]][0]
			input_bit_spec = p.precision_parameters
			example_inputs = [get_quantized(self.plan.graph.example_input, *[input_bit_spec[k] for k in ["a_sign_bits", "a_int", "a_f"]])]
		else:
			input_bit_spec = dl.Verilog.current_params.kwargs["input_bit_spec"]
			bits = np.sum(list(input_bit_spec.values()))
			inputs_per_transfer = [interface_width // bits]
			example_inputs = [get_quantized(self.plan.graph.example_input, *[input_bit_spec[k] for k in ["sign", "int", "frac"]])]
		BHCW = np.transpose(example_inputs[0], [0,1,3,2])
		W = BHCW.shape[3]
		# This pads the width dimension so that the width can be evenly divisible by the number of elements per transfer
		# the additional zeros get discarded by the Placeholder deserializer
		BHCWP = np.pad(BHCW, [[0,0],[0,0],[0,0],[0, W % inputs_per_transfer[0]]], mode="constant", constant_values=0)
		# This merges the BHCW dimensions so that we have a fully serialized image that is of shape 
		# [PADDED_ELEMENT_COUNT / inputs_per_transfer, inputs_per_transfer]
		LI = np.reshape(BHCWP, [BHCWP.size // inputs_per_transfer[0], inputs_per_transfer[0]])
		depth_serialized = LI.shape[0]


		required_depth = 0
		for oba in self.hbm_binary_weights_total:
			required_depth += len(oba)
		total_extra_images = math.ceil(required_depth/depth_serialized)

		weight_rom_data = []
		for j in range(total_extra_images):
			zero_vector = [0 for i in range(depth_serialized)]
			empty_bitarray = BitArray(bits=interface_width, value_vector=zero_vector)
			weight_rom_data.append(empty_bitarray)

		#Finally we fill the bitarrays with the correct data
		depth_index = 0
		rom_index = 0
		for oba in self.hbm_binary_weights_total:
			for line in oba.bit_array:
				weight_rom_data[rom_index].bit_array[depth_index] = line
				if (depth_index == depth_serialized-1):
					depth_index = 0
					rom_index += 1
				else:
					depth_index += 1
		return [total_extra_images, weight_rom_data]


	def get_custom_verilog_str(self, t="  "):
		verilog = """
  initial begin
`ifndef VERILATOR
`ifdef DUMP
  	$vcdplusfile("dump.vpd");
"""
		'''for m in modules_to_debug:
			owner_list = m.get_owner_list()
			verilog += "    $vcdpluson(0, " + ".".join([o.name + "_i" for o in reversed(owner_list[:-2])] + [m.name + "_i"]) + ");\n"'''
		verilog += """
    $vcdpluson(0, hpipe_tb_0);
    //$vcdplusmemon(0, hpipe_tb_0);
    //$vcdpluson(0, hpipe_0_i.basic_conv_24_i);
`endif
`endif
"""
		
		verilog += f"""  end
  reg [20:0] finish_counter;
  always @(posedge clock_0) begin
  	if (reset) begin
  		finish_counter <= '0;
  	end else begin
	  	finish_counter <= finish_counter + 1;
  		if (finish_counter == 10000000 || {self.done_simulation.name}) begin
"""
		verilog += """
`ifndef VERILATOR
  			$finish;
`endif
  		end
  	end
  end
"""
		return verilog

	def get_custom_verilog_port_list(self, t="  "):
		verilog = "`ifdef VERILATOR\n"
		verilog += t + self.clock.name + ",\n"
		verilog += t + self.reset.name + "\n"
		verilog += "`endif\n"
		return verilog


class HPipeTB(HPIPETLM):
	def __init__(self, plan, build_from=None, build_until=None, debug_individual_stage=None, **kwargs):
		super(HPipeTB, self).__init__("hpipe_tb", kind="hpipe_tb", **kwargs)
		inst = self.inst
		if "interface_width" in kwargs: 
			interface_width = kwargs["interface_width"]
		else:
			interface_width = 256
		interface_elements = interface_width // 9
		interface_bits = interface_elements * 9
		with dl.ModuleScope(self):
			clock = inst(dl.Clock)
			reset = inst(dl.ForcedSignal, name="reset", bits=1, signed=False)
			self.clock = clock
			self.reset = reset
			reset.add_value(1, 0)
			reset.add_value(0, 500)
			example_inputs = []
			example_input_precisions = []
			for n in plan.graph.nodes:
				print(n.tf_op.name + " " + n.type)
			for n in plan.graph.nodes:
				if not debug_individual_stage and not build_from and n.type == "Placeholder":
					example_inputs.append(n.example_outputs[0])
					example_input_precisions.append(n.precision_parameters)
				elif (debug_individual_stage is not None and debug_individual_stage in n.tf_op.name) or (build_from is not None and build_from in n.tf_op.name):
					for i in n.inputs:
						example_inputs.append(i._from.example_outputs[0])
					example_input_precisions.append(n.precision_parameters)
					break

			conv_count = 0
			for n in plan.graph.nodes:
				if n.type == "Conv2D":
					conv_count += 1
					if conv_count == 3:
						first_conv = n
						break
			for n in plan.graph.nodes:
				if n.type == "BiasAdd":
					first_bias = n
					break
			for n in plan.graph.nodes:
				if n.type == "MaxPool":
					first_max_pool = n
					break
			for n in plan.graph.nodes:
				if n.type == "Relu":
					first_relu = n
					break
			for n in plan.graph.nodes:
				if n.type in ["Add","AddV2"]:
					first_add = n
					break

			"""
			output_image = [np.sum(np.multiply(input_image[0,i*2:i*2+7,j*2:j*2+7,:], weights[:,:,:,c])) for i in range(112) for j in range(112) for c in range(weights.shape[-1])]
			output_image = np.array(output_image)
			output_image = np.reshape(output_image, [112,112,weights.shape[-1]])
			output_image = np.transpose(output_image, [0,2,1])
			output_image = np.reshape(output_image, [112*weights.shape[-1], 112])
			output_image //= 2**4
			output_image = [np.max(output_image[i*2:i*2+3,j*2:j*2+3,c]) for i in range(56) for j in range(56) for c in range(weights.shape[-1])]
			output_image = np.array(output_image)
			output_image = np.reshape(output_image, [56,56,weights.shape[-1]])
			output_image += biases[np.newaxis, np.newaxis, :] * 100
			output_image *= (output_image > 0).astype(int)
			"""
			node_of_interest = first_conv
			if len(node_of_interest.values) > 0:
				weights = node_of_interest.values[0]
				np.save("first_layer_weights.npy", weights)
				input_image = node_of_interest.inputs[0]._from.example_outputs[0]
				np.save("input_image.npy", input_image)

			#im = Image.fromarray((example_inputs[0][0]+128).astype(np.uint8), 'RGB')
			#im.show()
			if debug_individual_stage is None and build_from is None:
				BHCW = np.transpose(example_inputs[0], [0,1,3,2])
				BHCWP = np.pad(BHCW, [[0,0],[0,0],[0,0],[0, 224 % interface_elements]], mode="constant", constant_values=0)
				LI = np.reshape(BHCWP, [224 * 3 * 224 // interface_elements, interface_elements])
				test_in = [BitArray(bits=9, value_vector=LI[:,l]) for l in range(LI.shape[-1])]
				tmp = test_in[-1]
				for t in reversed(test_in[:-1]):
					tmp += t
				test_in = tmp#BitArray(bits=tmp.bits, bit_array=tmp)
				tmp = BitArray(bits=tmp.bits, bit_array=tmp.bit_array)
				if tmp.bits != 256:
					tmp += BitArray(bits=256 - tmp.bits, length=tmp.bit_array.shape[0])
					with open("input.bin", "wb") as fh:
						fh.write(tmp.get_byte_array())
			else:
				test_inputs = [np.reshape(np.transpose(i, [0,1,3,2]), [np.prod(np.array(list(i.shape))[[0,1,3]]), i.shape[2]]) for i in example_inputs]

			with dl.ParamScope(clock=clock, reset=reset, write_port_width=4, extend_to_match_bits=True):
				hpipe = inst(HPipe, plan, debug_individual_stage, build_until, build_from=build_from)
				self.hpipe = hpipe
			with dl.ParamScope(clock=clock, reset=hpipe.reset, write_port_width=4):
				for n in plan.graph.nodes:
					if "conv2d_25" in n.tf_op.name:
						continue
						np.save("first_layer_weights.npy", n.values[0])
						exit(0)

				if debug_individual_stage is None and build_from is None:
					input_rom = inst(dl.ROM, name="test_input_image", bits=(256 // 9) * 9, content_list=test_in)
					input_counter = inst(dl.Counter, reset_value=0, increment_value=1, end_value=len(test_in)-1)
					input_rom.r_addr.set_driver(input_counter.current_value)
					# inst(dl.NOT, input_counter.is_done)
					read_input = inst(dl.AND, inst(dl.NOT, reset), hpipe.placeholder.space_to_write_line)
					input_rom.r_en.set_driver(read_input)
					input_counter.set_increment_condition(read_input)
					hpipe.placeholder.write.set_driver(read_input.delay(1))
					hpipe.placeholder.inputs[0].set_driver(input_rom.r_data)
					input_rom.r_en.set_driver(hpipe.placeholder.space_to_write_line)
					write_counter = inst(dl.Counter,
						reset_value=0,
						increment_value=1,
						end_value=hpipe.placeholder.input_lines,
						name="write_counter")
					write_counter.set_increment_condition(hpipe.placeholder.write)
				else:
					input_precision,_,_ = self.hpipe.nodes[0].module.get_bit_widths()
					width = len(test_inputs[0][0])
					
					for i,t in enumerate(test_inputs):
						t = [BitArray(bits=input_precision, value_vector=t[:,l]) for l in range(t.shape[-1])]
						tmp = t[-1]
						for _t in reversed(t[:-1]):
							tmp += _t
						test_inputs[i] = tmp
					input_roms = [inst(dl.ROM, bits=input_precision*width, content_list=t) for t in test_inputs]
					input_counters = [inst(dl.Counter, reset_value=0, increment_value=1, end_value=len(t)-1) for t in test_inputs]
					channel_counters = [inst(dl.Counter, reset_value=0, increment_value=1, end_value=example_inputs[i].shape[-1]-1) for i,t in enumerate(test_inputs)]
					m = hpipe.nodes[0].module
					for i,(r,c) in enumerate(zip(input_roms, input_counters)):
						states = ["IDLE", "WRITING_LINE", "DONE"]
						edges = [["IDLE", "WRITING_LINE", m.input_modules[i].space_to_write_line],
							["WRITING_LINE", "IDLE", channel_counters[i].is_done],
							["WRITING_LINE", "DONE", inst(dl.AND, channel_counters[i].is_done, c.is_done)],
							#["DONE", "IDLE"]
							]
						state_machine = inst(dl.StateMachine, states, edges, None, name="input_state_machine_" + str(i))
						with dl.ModuleScope(state_machine):
							f = inst(dl.Flop, bits=1, name="phantom_flop")
							f.set_reset_driver(inst(dl.Constant, bits=1, value=0))
						read_input = state_machine.c["is_writing_line"]
						channel_counters[i].set_increment_condition(state_machine.c["is_writing_line"])
						r.r_addr.set_driver(c.current_value)
						r.r_en.set_driver(read_input)
						c.set_increment_condition(read_input)
						m.input_modules[i].write.set_driver(read_input.delay(1))
						delayed_write = read_input.delay(1 + m.get_control_to_input_delay())
						delayed_read_data = r.r_data.delay(m.get_control_to_input_delay())
						for j,inp in enumerate(m.input_modules[i].inputs):
							inp.set_driver(delayed_read_data[(j+1)*input_precision-1:j*input_precision])
							m.input_modules[i].writes[j].set_driver(delayed_write)



				"""with dl.ModuleScope(hpipe.bc.ia_buffer, True):
					input_mux = inst(dl.Mux, input_rom.r_data._bit_width,
						hpipe.iwc.state_machine.c["is_writing_padding"].delay(1),
						input_rom.r_data,
						inst(dl.Constant, bits=input_rom.r_data._bit_width, value=0))"""


				"""
				self.all_outputs = []
				self.all_valids = []
				self.done_signals = []
				for n in self.hpipe.nodes:
					output_signals = []
					self.all_valids.append(inst(dl.Logic, bits=1))
					self.all_valids[-1].set_driver(n.module.outputs_valid[0])
					self.done_signals.append(inst(dl.Logic, bits=1))
					self.done_signals[-1].set_driver(n.module.done)
					for o in n.module.outputs:
						output_signals.append(inst(dl.Logic, bits=o._bit_width, signed=o.signed))
						output_signals[-1].set_driver(o)
					self.all_outputs.append(output_signals)"""

			print_stage_header("Writing TensorFlow Outputs For Nodes in Graph")
			for n in self.hpipe.nodes:
				output_image = n.example_outputs[0]
				#print("NODE OF INTEREST SHAPE: " + str(output_image.shape))
				output_shape = output_image.shape
				output_image = np.reshape(output_image, output_image.shape[1:])
				output_image = np.transpose(output_image, [0,2,1])
				output_image = np.reshape(output_image, [np.prod(output_image.shape[:2]),output_image.shape[-1]])
				f_name = "generated_files/layer_images/tensorflow/" + n.module.name + ".csv"
				if not os.path.isfile(f_name) or True:
					print("  Generating " + f_name)
					with open(f_name, "w") as f:
						f.write(str(output_shape[-1]) + "\n")
						for i in range(output_image.shape[0]):
							f.write(",".join([str(d) for d in list(output_image[i])]) + "\n")
				else:
					print("  Not touching " + f_name + " since it already exists")


				"""with dl.ModuleScope(inst(dl.Module, "test_output_mux", kind="test_output_mux", name="test_output_mux")):
					self.output_roms = [inst(dl.ROM, name="golden_output_image", bits=36, content_list=list(output_image[i])) for i in range(output_image.shape[0])]
					for otr in self.output_roms:
						otr.r_en.set_driver(inst(dl.Constant, bits=1, value=1, signed=False))
						otr.r_addr.set_driver(output_data_counter.current_value)
				self.golden_output_data = [inst(dl.Logic, bits=36, signed=True, name="golden_output_data") for _ in self.output_roms]
				for god, otr in zip(self.golden_output_data, self.output_roms):
					god.set_driver(otr.r_data)"""



	def get_custom_verilog_str(self, t="  "):
		verilog = """
  initial begin
`ifndef VERILATOR
`ifdef DUMP
  	$vcdplusfile("max_pool_debug.vpd");
    $vcdpluson(0, hpipe_tb_0);
    //$vcdpluson(0, hpipe_0_i.basic_conv_24_i);
`endif
`endif
"""
		
		verilog += """  end
  reg [20:0] finish_counter;
  always @(posedge clock_0) begin
  	if (reset) begin
  		finish_counter <= '0;
  	end else begin
	  	finish_counter <= finish_counter + 1;
  		if (finish_counter == 2000000) begin
"""
		verilog += """
`ifndef VERILATOR
  			$finish;
`endif
  		end
  	end
  end
"""
		return verilog

	def get_custom_verilog_port_list(self, t="  "):
		verilog = "`ifdef VERILATOR\n"
		verilog += t + self.clock.name + ",\n"
		verilog += t + self.reset.name + "\n"
		verilog += "`endif\n"
		return verilog

def get_argparser():
	parser = argparse.ArgumentParser(description="Generate verilog that implements a neural network exported from TensorFlow")
	parser.add_argument("--param_file_path", type=str, default="params_file.json")
	parser.add_argument("--quartus", const=True, default=False, action="store_const")
	parser.add_argument("--dont_dump_verilog", const=True, default=False, action="store_const")
	parser.add_argument("--dump_tensorflow_activations", const=True, default=False, action="store_const")
	parser.add_argument("--print_graph_nodes", const=True, default=False, action="store_const")
	parser.add_argument("--make_fake_weights", const=True, default=False, action="store_const")
	parser.add_argument("--print_resource_estimates", const=True, default=False, action="store_const")
	parser.add_argument("--fake_weights_sparsity", default=0.0, type=float)
	parser.add_argument("--generated_circuit_path", default="", type=str)
	parser.add_argument("--dsp_target", default=0, type=int)
	parser.add_argument("--arch", default="architectures/s10.json")
	parser.add_argument("--device", default="devices/s10_2800.json")
	parser.add_argument("--instance_location", default="input")
	parser.add_argument("--partitioning", default=False)
	parser.add_argument("--partition_ready", default=0, type=int)
	parser.add_argument("--chip_count", default=1, type=int)
	parser.add_argument("--setup_mode", default="auto", type=str)
	return parser

def process_params_file(args):
	params_file = args.param_file_path
	generated_circuit_dir = "generated_circuit"
	top_level_module_type = "tb"
	
	if args.quartus:
		generated_circuit_dir = "quartus_circuit"
		top_level_module_type = "quartus"

	params = {
		"pb_path" : "/home/mat/drive1/different_sparsity_resnet_models/304811/frozen_graph.opt.swapped_max_pool_and_batch_norm.pb",
		"sample_image" : "individualImage.png",
		"output_names" : ["import/resnet_model/final_dense"],
		"quartus" : args.quartus,
		"debug_individual_stage" : None,
		"build_from" : None,
		"build_until" : None,
		"target_act_bits" : 16,
		"target_parameter_bits" : 16,
		"oc_unroll_threshold" : 32,
		"input_adjustments" : {
			"pre_scale" : 1.0,
			"channel_biases" : [0.0, 0.0, 0.0],
			"post_scale" : 1.0,
			"bit_spec" : {
				"sign" : 0,
				"int" : 1,
				"frac" : 0
			},
		},
		"input_bit_spec" : {
			"sign" : 0,
			"int" : 0,
			"frac" : 8
		},
		"example_input_only_scale" : 1.0,
		"apply_input_adjustments_to_example_input" : True,
		"dsp_target" : 5000,
		"mem_target" : None,
		"instantiate_quartus_ip" : False,
		"physically_mapped_rams" : True,
		"tensor_block_mode" : "default",
		"hbm_layer_selection" : None,
		"hbm_manual_layers": [],
		"op_precision_annotations_path" : "default_precisions.annotations",
		"generated_circuit_dir" : generated_circuit_dir,
		"build_data_path" : True,
		"top_level_module_type" : top_level_module_type,
		"import_mobilenet_ssd" : False,
		"propagate_outputs_to_top": False,
		"interface_width" : 256,
		"partitioning": False,
		"instance_location" : "input",
		"chip_count" : 1,
		"setup_mode" : "auto"
	}
	default_input_adjustments = params["input_adjustments"]
	
	if os.path.isfile(params_file):
		required_keys = list(params.keys())
		with open(params_file, "r") as fh:
			new_params = json.load(fh)
			for k in required_keys:
				if k not in new_params:
					print("Param file " + params_file + " doesn't have required key '" + k + "'.  Adding it with default value.")
					new_params[k] = params[k]
			params = new_params
	with open(params_file, "w") as fh:
		json.dump(params, fh, sort_keys=True, indent=4)
		
	# In case we want to use seperate bit specs for Bias and activation functions we can use target_bias_and_act_func_bits 
	# (e.g. Stratix 10 NX has 8-bit fixed-point limit on tensor blocks but we can have larger precision for the bias add after)
	# If this isn't specified match it up with the activation precision
	if "target_bias_and_act_func_bits" not in params:
		params["target_bias_and_act_func_bits"] = params["target_act_bits"]
		
	#Make sure generated circuit directory is correct if using VCS	
	if params["top_level_module_type"] == "tbV2":
		params["generated_circuit_dir"] = "generated_files/circuits/generated_circuit"

	#if params["build_from"] is not None:
	#	params["input_adjustments"] = default_input_adjustments
	return params

def main(argv):
	warnings.simplefilter('once', UserWarning)
	parser = get_argparser()
	args = parser.parse_args(argv[1:])
	arch.hpipe_device_arch = arch.Architecture(args.arch, args.device)

	dump_tensorflow_activations = args.dump_tensorflow_activations
	params = process_params_file(args)
	pb_path = params["pb_path"]
	image_path = params["sample_image"]
	output_names = params["output_names"]
	generate_quartus_project = params["quartus"]
	debug_individual_stage = params["debug_individual_stage"]
	build_from = params["build_from"]
	build_until = params["build_until"]
	input_adjustments = params["input_adjustments"]
	input_bit_spec = params["input_bit_spec"]
	dsp_target = params["dsp_target"]
	target_act_bits = params["target_act_bits"]
	target_parameter_bits = params["target_parameter_bits"]
	tensor_block_mode = params["tensor_block_mode"]
	hbm_layer_selection = params["hbm_layer_selection"]
	hbm_manual_layers = params["hbm_manual_layers"]
	target_bias_and_act_func_bits = params["target_bias_and_act_func_bits"]
	instantiate_quartus_ip = params["instantiate_quartus_ip"]
	generated_circuit_dir = params["generated_circuit_dir"]
	op_precisions_path = params["op_precision_annotations_path"]
	build_data_path = params["build_data_path"]
	oc_unroll_threshold = params["oc_unroll_threshold"]
	physically_mapped_rams = params["physically_mapped_rams"]
	import_mobilenet_ssd = params["import_mobilenet_ssd"]
	apply_input_adjustments_to_example_input = params["apply_input_adjustments_to_example_input"]
	example_input_only_scale = params["example_input_only_scale"]
	propagate_outputs_to_top = params["propagate_outputs_to_top"]
	mem_target = params["mem_target"]
	#Added for multi chip generation
	interface_width = params["interface_width"]
	partitioning = params["partitioning"]
	instance_location = params["instance_location"]
	chip_count = params["chip_count"]
	setup_mode = params["setup_mode"]
	
	top_level_module_types = {
		"quartus" : SystemBuilderComponent,
		"tb" : HPipeTB,
		"tbV2" : HPIPETB2,
		"npu_wrapper" : NPU_Wrapper
	}


	arch.hpipe_device_arch.set_network(pb_path)
	arch.hpipe_device_arch.set_tensor_block_type(tensor_block_mode)

	if arch.hpipe_device_arch.arch_name == "S10_NX" and (target_act_bits > 8 or target_parameter_bits > 8):
		if arch.hpipe_device_arch.tensor_block_mode != "tensor":
			print("ERROR: we do not support more than 8-bits in S10-NX vector mode for activations or parameters in convolution modules!")
			exit(1)
		else:
			print("WARNING: more than 8-bits specified for S10-NX tensor mode for activations or parameters in convolution modules.")
			print("HPIPE will default to using Bfloat16, which may result in some accuracy loss as mantissas are limited to 7 bits with shared exponents")
			global enable_bfp
			enable_bfp = True
			update_enable_bfp(enable_bfp) #update bfp support in Planner
			update_OVERWRITE_QUANT_BITS(max(target_act_bits, target_parameter_bits)) #Overwrite quantize nodes to use 16-bits
			import time
			time.sleep(3)

	hbm_settings = None
	if (hbm_layer_selection != ''):
		if (hbm_layer_selection != "auto") and (hbm_layer_selection != "manual"):
			print("ERROR: hbm_layer_selection should be set to auto, manual or the empty string (off)")
			exit(1)	
		else:
			hbm_settings = (hbm_layer_selection, hbm_manual_layers)

	global enable_sim
	global real_ip_sim
	enable_sim = (not instantiate_quartus_ip)
	real_ip_sim = instantiate_quartus_ip and (params["top_level_module_type"] == "tbV2")
	physical_hbm =  instantiate_quartus_ip and (params["top_level_module_type"] == "quartus") and (hbm_layer_selection != '')
	simulate_hbm = (params["top_level_module_type"] == "tbV2") and (hbm_layer_selection != '')

	if args.generated_circuit_path != "":
		generated_circuit_dir = args.generated_circuit_path
	if args.dsp_target != 0:
		dsp_target = args.dsp_target
	if args.partitioning != None:
		partitioning = args.partitioning
	if args.partition_ready != None:
		partition_ready = args.partition_ready
	top_level_module_type = params["top_level_module_type"]
	if top_level_module_type not in top_level_module_types:
		print("Parameter top_level_module_type must be on of: " + ", ".join(top_level_module_types.keys()) + ". You specified " + top_level_module_type)
	top_level_module_type = top_level_module_types[top_level_module_type]


	print_stage_header("Optimizing Graph and Forming Plan")

	from hpipe.build_accelerator import get_plan
	plan = get_plan(
		pb_path,
		image_path,
		output_names,
		oc_unroll_threshold=oc_unroll_threshold,
		op_precision_annotations_path=op_precisions_path,
		dsp_target=dsp_target,
		mem_target=mem_target,
		input_adjustments=input_adjustments,
		build_from=build_from,
		target_act_bits=target_act_bits,
		target_parameter_bits=target_parameter_bits,
		target_bias_and_act_func_bits=target_bias_and_act_func_bits,
		print_graph_nodes=args.print_graph_nodes,
		make_fake_weights=args.make_fake_weights,
		fake_weights_sparsity=args.fake_weights_sparsity,
		print_resource_estimates=args.print_resource_estimates,
		import_mobilenet_ssd=import_mobilenet_ssd,
		example_input_only_scale=example_input_only_scale,
		apply_input_adjustments_to_example_input=apply_input_adjustments_to_example_input,
		partition_ready = partition_ready,
		hbm_settings = hbm_settings)

	#If called to partition, no need to generate the Verilog files, just return here
	if partitioning and partition_ready==1:
		part = Partition(plan.graph)
		return 

	if args.print_resource_estimates:
		return

	cs_list = []
	for n in plan.graph.nodes:
		pe = n.planner_estimate
		if pe.multiplier_count > 0:
			cs_list.append(pe.n_channel_splits)

	print_stage_header("Channel Splits Histogram")
	plot_hist(cs_list, xlab=True, showSummary=True)

	for n in plan.graph.nodes:
		if build_from == n.tf_op.name:
			example_input = n.inputs[0]._from.example_outputs[0]
			original_edge = n.inputs[0]
			precision_parameters = n.inputs[0]._from.precision_parameters
			tf_op = tf.placeholder(dtype=tf.float32, shape=example_input.shape).op
			node = Node(tf_op, [example_input], precision_parameters)
			node.outputs = []
			
			#if len(n.inputs[0]._from.outputs) > 1:
			output_nodes = n.inputs[0]._from.outputs
			#else:
			#	output_nodes = n.inputs[1]._from.outputs
			#print("ll" + " " + str(len(output_nodes)))
			#print(n.inputs[1]._from.tf_op.name)
			for cut_edge in output_nodes:
				cut_node = cut_edge._to
				edge = Edge(node, cut_node)
				node.outputs.append(edge)
				#IF the node has more than one input, iterate over all of them to 
				#check which edge to replace with the new edge
				#print(len(cut_node.inputs))
				if(len(cut_node.inputs) > 1):
					for i in range(len(cut_node.inputs)):
						if(cut_node.inputs[i]._from.tf_op.name == original_edge._from.tf_op.name):
							cut_node.inputs[i] = edge
				else:
					cut_node.inputs[0] = edge
				plan.graph.edges.append(edge)
			plan.graph.heads = [node]
			plan.graph.nodes.append(node)
			break
	os.makedirs(generated_circuit_dir, exist_ok=True)
	with open(generated_circuit_dir + "/conv_cycle_estimates.txt", "w") as fh:
		for n in plan.graph.nodes:
			if n.type not in ["Conv2D", "DepthwiseConv2dNative"]:
				continue
			fh.write(n.tf_op.name + ", " + str(n.planner_estimate.n_channel_splits) + ", " + str(n.planner_estimate.time_weight) + "\n")

	print_stage_header("Building Circuit")
	circuit = dl.Circuit()
	tlm_args = {
		"circuit" : circuit,
		"build_until" : build_until,
		"debug_individual_stage" : debug_individual_stage,
		"build_data_path" : build_data_path,
		"directly_propagate_outputs_to_top" : propagate_outputs_to_top,
		"interface_width" : interface_width,
		"instance_location" : instance_location,
		"partitioning" : partitioning,
		"chip_count": chip_count,
		"physical_hbm": physical_hbm}

	with dl.ModuleScope(circuit):
		with dl.ParamScope(circuit=circuit, implement_with_megafunction=instantiate_quartus_ip,
			input_bit_spec=input_bit_spec, input_adjustments=input_adjustments,
			physically_mapped_rams=physically_mapped_rams, simulate_hbm=simulate_hbm):
			netlist = top_level_module_type(plan, **tlm_args)
			if top_level_module_type == SystemBuilderComponent:
				netlist.should_create_verilog_definition = False
	print("M20K Estimate: ", circuit.m20k_estimates())
	mif_additional_path = ""
	
	#If called to partition, no need to generate the Verilog files, just return here
	if partitioning:
		print(partitioning)
		part = Partition(plan.graph)
		return 
	#else:
	#	return
	#if generate_quartus_project:
	#	mif_additional_path = "/generated_circuit"
	numpy_dump_dir = generated_circuit_dir + "/numpy_weights_and_inputs/"
	os.makedirs(numpy_dump_dir, exist_ok=True)
	unit_test_configs = []

	unit_test_arch = arch.hpipe_device_arch.arch_name
	tensor_block_mode = arch.hpipe_device_arch.tensor_block_mode

	for n in plan.graph.nodes:
		if (n.type != "Conv2D" and n.type != "DepthwiseConv2dNative") or n.module is None:
			continue
		m = n.module
		
		#Not needed for ConvWrapper
		if isinstance(m, BasicConv):
			np.save(numpy_dump_dir + m.name + "_weights_per_oc.npy", m.weights_per_oc)
			np.save(numpy_dump_dir + m.name + "_weights.npy", m.numpy_combined_weights)
			
		np.save(numpy_dump_dir + m.name + "_input.npy", m.example_input)
		np.save(numpy_dump_dir + m.name + "_input_weights.npy", m.weights)
		
		#cwd = os.getcwd() + "/"
		#File paths should be relative to Unit Test folder
		network_folder = arch.hpipe_device_arch.network.replace("_", "-")
		if arch.hpipe_device_arch.arch_name == "S10_NX": network_folder += "-nx"
		if tensor_block_mode == "tensor": network_folder += "-tensor-mode"
		cwd = "test_configs/" + network_folder + "/"
		explicit_padding_lines = 0
		if tensor_block_mode == "tensor" and n.explicit_padding is not None:
			if (n.explicit_padding[1][0] == n.explicit_padding[1][1] == n.explicit_padding[2][0] == n.explicit_padding[2][1]):
				explicit_padding_lines = int(n.explicit_padding[1][0])
			else:
				print("WARNING: The layer ", n.module.name, " has explicit padding that cannot be properly replicated using Tensor Mode")
		
		HBM_SIMULATED_EFFICIENCY = 100
		if (n.offload_to_hbm == True):
			HBM_SIMULATED_EFFICIENCY = 90 # 90% in unit tests
			n.planner_estimate.time_weight = math.ceil(n.planner_estimate.time_weight * (100/HBM_SIMULATED_EFFICIENCY))
		unit_test_configs.append({
			"aux" : {
				"name" : m.name,
				"time_estimate" : str(n.planner_estimate.time_weight)
			},
			"command" : [
			"--sy" , str(n.get_strides()[1]),
			"--sx" , str(n.get_strides()[2]),
			#"-i" , cwd + numpy_dump_dir + m.name + "_input.npy",
			"-i" , cwd + m.name + "_input.npy",
			#"--weights" , cwd + numpy_dump_dir + m.name + "_input_weights.npy",
			"--weights" , cwd + m.name + "_input_weights.npy",
			"--iwbf" , str(n.precision_parameters["f"]),
			"--iibf" , str(n.input_nodes()[0].precision_parameters["a_f"]),
			"--wbf" , str(n.precision_parameters["f"]),
			"--wbi" , str(n.precision_parameters["int"]),
			"--ibf" , str(n.input_nodes()[0].precision_parameters["a_f"]),
			"--ibi" , str(n.input_nodes()[0].precision_parameters["a_int"]),
			"--obf" , str(n.precision_parameters["a_f"]),
			"--obi" , str(n.precision_parameters["a_int"]),
			"--scale" , str(n.precision_parameters["scale"]),
			"--type", n.type,	
			"--padding", n.properties["padding"],
			"--explicit_padding", str(explicit_padding_lines),
			"--arch",  unit_test_arch,
			"--tensor_block_mode", tensor_block_mode,
			"--enable_bfp_support", str(enable_bfp),
			"--offload_to_hbm", str(n.offload_to_hbm),
			"--hbm_efficiency", str(HBM_SIMULATED_EFFICIENCY),
			"--ic_parallelism", str(n.planner_estimate.n_channel_splits),
			"--oc_parallelism", str(n.planner_estimate.n_output_channel_groups)
			]})
	with open(numpy_dump_dir + "unit_test_configs.json", "w") as fh:
		json.dump(unit_test_configs, fh)

	if dump_tensorflow_activations:
		netlist.dump_tensorflow_activations()
	if args.dont_dump_verilog:
		sys.exit(0)
	netlist.hpipe.write_stats_to_dir(generated_circuit_dir)
	circuit.write_verilog_to_dir(generated_circuit_dir)#, mif_additional_path=mif_additional_path)
	os.makedirs("generated_files/layer_images/verilog", exist_ok=True)

	#print_stage_header("Finished Verilog Generation")
	"""
	print_stage_header("Generating Filelists and Info For Modular Verilator Flow")
	with open(generated_circuit_dir + "/verilog_paths.txt", "r") as fh:
		paths = [l for l in fh]
	for n in netlist.hpipe.nodes:
		m = n.module
		paths_for_module = []
		for p in paths:
			if ("/" + m.name + "/") in p or ("/" + m.name + ".v") in p:
				paths_for_module.append(p)
		paths_for_module_str = "".join(paths_for_module)
		paths_path = generated_circuit_dir + "/CIRCUIT/" + netlist.name + "/" + netlist.hpipe.name + "/" + m.name + "/verilog_paths.txt"
		file_matches = False
		if os.path.isfile(paths_path):
			file_matches = True
			with open(paths_path, "r") as pp:
				ppe_str = [l for l in "".join(pp)]
				if ppe_str != paths_for_module_str:
					file_matches = False
		if not file_matches:
			with open(paths_path, "w") as pp:
				pp.write(paths_for_module_str)"""


if __name__ == "__main__":
	main(sys.argv)


#	node_precision_file = open("op_precisions.annotations", "w")
#	node_precision_file.write("node_name,scale,sign_bits,exponent,int,f,a_sign_bits,a_exponent,a_int,a_f\n")
#	def print_node_precision_file(node):
#		node_precision_file.write(",".join([node.tf_op.name, "1.", "1","0","4","11","1","0","4","11"]) + "\n")
#	plan.graph.walk_graph(f_node=print_node_precision_file)
#	node_precision_file.close()

