#Partitioning Algorithm
import math
import numpy as np
from hpipe.GraphBuilder import Graph
from digitallogic.utils import clog2
from multiprocessing import Process, Value
import os
import tensorflow as tf
import time
import digitallogic as dl
import json
#Turning on multithreading (actually multiprocessing in this case) may either increase or decrease the speed of calculating the first cost estimate depending on the design
ENABLE_MULTITHREADING = 1
# Higher CPU core counts degrade performance due to process creation overhead. Setting this to 1 will still introduce some processes creation overhead; it's better to just use the flag above
CORE_LIMIT = 4
FMAX = 400000000 #400 MHz


class perf_est():
	def __init__(self, graph, **kwargs):
		print("  perf_est")
		self.graph = graph
		
		# Read the partition_params file and figure out the hardware setup and the partitioning algorithm and other info
		# Initialize the partition_estimate object for every node
		self.initialize_partitioning(fmax = FMAX)

		# Put nodes into groups based on which nodes share same group (nodes on a skip path will share same group)
		self.assign_initial_groups()

		# Preprocessing includes setting finish times for perf estimation and also marking nodes on the skip path
		# such that only the branch with higher number of conv layers is used for perf estimation
		self.preprocess_conv_layers()

			
		self.estimate_performance(fmax = FMAX)
		


	def initialize_partitioning(self,fmax):
		self.latency = 0
		self.throughput = 0
		self.highest_cycles = 0
		self.fmax = fmax
		#Initialize the partition estimate will all variables for all the nodes in the list
		for nn in self.graph.nodes:
			nn.partition_estimate = partition_estimate(nn)
			nn.partition_estimate.multiplier_count = nn.planner_estimate.multiplier_count

	def assign_initial_groups(self):
		group = 0
		def read_info(n):
			nonlocal group
			n.partition_estimate.group_id = group
			group+=1
			if(n.type == 'Placeholder'):
				return
			if self.highest_cycles < n.planner_estimate.time_weight:
				self.highest_cycles  =  n.planner_estimate.time_weight
			n.planner_estimate.cycles_per_row = int(n.planner_estimate.time_weight/n.get_output_shape()[1])
			
		self.graph.walk_graph(f_node=read_info)

	def preprocess_conv_layers(self):	
		for nn in self.graph.nodes:
			if nn.type not in ['Conv2D', 'DepthwiseConv2dNative']:
				continue
			nn.partition_estimate.finish_cycle = nn.planner_estimate.cycles_per_row

		group_id = 0
		def drop_skip_path_nodes(n):
			"""For a skip path, the latency estimate should only take in the maximum of the two paths 
				currently the one with more conv layers"""
			nonlocal group_id
			if not(len(n.outputs)>1): #pick the nodes that have a fork
				return
			group_id = n.outputs[0]._to.partition_estimate.group_id
			convs_in_path = np.zeros(len(n.outputs))
			nodes_in_path  = [[] for i in range(len(n.outputs))]
			#Check all the paths forking from that node
			for i in range(len(n.outputs)):
				follow_node = n.outputs[i]._to
				follow_node.partition_estimate.group_id = group_id
				#Count all the conv nodes on a specific path
				if(follow_node.type in ['Conv2D', 'DepthwiseConv2dNative']):
					convs_in_path[i]+=1
					nodes_in_path[i].append(follow_node)

				while not(len(follow_node.inputs)>1): # Assume that the fork ends when a node with more than one input is encountered (like an ADD Node)
					follow_node = follow_node.outputs[0]._to
					follow_node.partition_estimate.group_id = group_id
					if(follow_node.type in ['Conv2D', 'DepthwiseConv2dNative']):
						convs_in_path[i]+=1
						nodes_in_path[i].append(follow_node)

			index_max = np.where(convs_in_path == np.max(convs_in_path))
			for i in range(len(n.outputs)):
				if(i == index_max[0].any()):

					continue
				for n in nodes_in_path[i]: #Mark nodes that are on the skip path
					n.partition_estimate.on_skip_path = True

		self.graph.walk_graph(f_node=drop_skip_path_nodes)	


			
	def estimate_performance(self,fmax):
		# The goal of this function is to estimate the throughput and latency of a given accelerator
		arranged_conv = []
		group_id = 0

		#Have an array with all the conv nodes that will affect the latency (in order)
		def add_conv(n): 
			nonlocal arranged_conv
			if len(n.outputs) > 0:
				if n.outputs[0]._to.partition_estimate.chip_id != n.partition_estimate.chip_id:
					n.partition_estimate.edge_node = 1
			
			if n.type not in ['Conv2D', 'DepthwiseConv2dNative']:
				return
			elif not n.partition_estimate.on_skip_path:
				arranged_conv.append(n)
		self.graph.walk_graph(f_node=add_conv)

		#Account for any reduction in th eoutput dimension like pooling layers or flatten layers
		for i,n in enumerate(arranged_conv[0:-1]):
			n_out_shape = n.get_output_shape()[1]
			next_n_in_shape = arranged_conv[i+1].inputs[0]._from.get_output_shape()[1]
			n.partition_estimate.outputs_for_next_layer =int(n_out_shape/ next_n_in_shape)

		""" A function that estimates the latency and throughput for the convolution layers
			The base idea is: given the time that each node takes to compute an entire row (all channels)
			and given the buffer space that each layer has, we can model how rows are transferred between the layers
			from the start till the last layer produces the last row of output """
		def cycle_cycle_simulator(arranged_conv):
			flag = 0
			cycle = 0
			times = []
			initial_time = 12000
			placeholder_time = 12000
			event_cycle = -1
			num_images = 8
			images_counter = 1
			ff = 0
			arranged_conv[0].partition_estimate.input_buffer_elements = [(1,images_counter) for i in range((arranged_conv[0].inputs[0]._from.get_output_shape()[1] +arranged_conv[0].partition_estimate.pad))]
			cycle += initial_time #Start after the placeholder handling time (for now it is fixed but it should be a function of th einput size)
			print('Estimating .........')
			while(True):
				min_wait = 9999999999

				#Each time step the inner loop iterates over all the nodes to update the state of each node
				#Update --> mark the node as "started to work" or "finished producing a row"
				for i, no in enumerate(arranged_conv):
					#Running multiple images through, every time the input buffer is empty we can delay it "placeholder_time" and fill it again 
					if i == 0 and cycle >= event_cycle and images_counter < num_images and event_cycle != -1:
						images_counter +=1
						arranged_conv[0].partition_estimate.input_buffer_elements = [(1,images_counter) for i in range((arranged_conv[0].inputs[0]._from.get_output_shape()[1] +arranged_conv[0].partition_estimate.pad))]
						event_cycle = -1

					# When the node is marked as working and the time it takes to finish a row is completed we update the state
					if(cycle - no.partition_estimate.start_cycle >= no.planner_estimate.cycles_per_row and no.partition_estimate.start_cycle !=-1):
						no.partition_estimate.produced_outputs +=1  #Increase the number of produced rows for the node
						
						"""This part is added to account for the fact that the last padding line is virtual so we need to alert the layers that
							the buffer is actually full with this virtual last line"""
						if no.partition_estimate.remain_pad != 0 and ((no.partition_estimate.produced_outputs+1) %no.get_output_shape()[1]==0):
							no.partition_estimate.use_pad = 1
						else:
							no.partition_estimate.use_pad = 0
						
						#Clear (stride) number of rows from the buffer
						for m in range(no.get_strides()[1]):
							for n in range(len(no.partition_estimate.input_buffer_elements)):
								if no.partition_estimate.input_buffer_elements[n][1] == no.partition_estimate.current_image:
									no.partition_estimate.input_buffer_elements.pop(n)
									break

						no.partition_estimate.total_subtracted += no.get_strides()[1]
						
						"""Transfer the produced line to the following layer after taking care of any reduction . For example if there is a maxpool 
						   with a size of 2, every two produced outputs from this layer, one row will be transferred to the following layer"""
						if i != len(arranged_conv)-1 and (no.partition_estimate.produced_outputs % no.partition_estimate.outputs_for_next_layer == 0) and no.partition_estimate.produced_outputs != 0:
							arranged_conv[i+1].partition_estimate.input_buffer_elements.append((1,no.partition_estimate.current_image))
						

						# This condition happens when a node finishes producing all of its rows
						if(no.partition_estimate.produced_outputs % no.get_output_shape()[1] == 0 and no.partition_estimate.produced_outputs!=0):
							no.partition_estimate.current_image +=1
							if i == 0 and images_counter < num_images: #For the first node, empty the buffer and mark the time "event_cycle" to wait some time "placeholder_time" and then fill the buffer again
								no.partition_estimate.input_buffer_elements = []
								event_cycle = cycle + placeholder_time
							else: #For all other nodes, just empty what's left in the buffer from the previous image
								if no.partition_estimate.pad == 0:
									remaining = (no.inputs[0]._from.get_output_shape()[1] + no.partition_estimate.pad) - no.partition_estimate.total_subtracted
								else:
									remaining = (no.inputs[0]._from.get_output_shape()[1] + no.partition_estimate.fixed_initial_buffer) - no.partition_estimate.total_subtracted
								
								#Empty the buffer of any lines that belong to an old image if you are done with this image
								for m,n in enumerate(no.partition_estimate.input_buffer_elements):
									if n[1] != no.partition_estimate.current_image:
										no.partition_estimate.input_buffer_elements.pop(m)

								no.partition_estimate.total_subtracted  = 0

								#Add padding to the buffer
								for m in range(no.partition_estimate.fixed_initial_buffer):
									no.partition_estimate.input_buffer_elements.append((1,no.partition_estimate.current_image))
								if no.partition_estimate.pad == 0:
									no.partition_estimate.remain_pad = 0
								else:
									no.partition_estimate.remain_pad =no.partition_estimate.pad - 1
							
							# Store the times when the last node finishes
							if((i == len(arranged_conv)-1) and no.partition_estimate.produced_outputs % no.get_output_shape()[1] == 0):
								times.append(cycle)
							if((i == len(arranged_conv)-1) and (no.partition_estimate.produced_outputs == no.get_output_shape()[1]*num_images)):
								return cycle,times
							
						
						
						no.partition_estimate.start_cycle = -1 # Marks the node as idle
							
					#Count how many elements you have in the buffer
					no.partition_estimate.current_elements = 0
					for m in range(len(no.partition_estimate.input_buffer_elements)):
						if no.partition_estimate.input_buffer_elements[m][1] == no.partition_estimate.current_image:
							no.partition_estimate.current_elements+=1

					# This condotion checks if a node should start and mark that node as working
					if (no.partition_estimate.current_elements >= no.get_kernel_shape()[0] and no.partition_estimate.start_cycle == -1) or (no.partition_estimate.current_elements+no.partition_estimate.remain_pad >= no.get_kernel_shape()[0] and no.partition_estimate.start_cycle == -1
						and no.partition_estimate.remain_pad > 0 and ((no.partition_estimate.produced_outputs+1) %no.get_output_shape()[1]==0)) :
						if i != len(arranged_conv)-1:
							if (len(arranged_conv[i+1].partition_estimate.input_buffer_elements) < arranged_conv[i+1].partition_estimate.max_buffer_space and arranged_conv[i+1].partition_estimate.remain_pad == 0) or (len(arranged_conv[i+1].partition_estimate.input_buffer_elements) < arranged_conv[i+1].partition_estimate.max_buffer_space-1 and arranged_conv[i+1].partition_estimate.remain_pad != 0 and arranged_conv[i+1].partition_estimate.use_pad == 1 ) or (len(arranged_conv[i+1].partition_estimate.input_buffer_elements) < arranged_conv[i+1].partition_estimate.max_buffer_space and arranged_conv[i+1].partition_estimate.remain_pad != 0 and arranged_conv[i+1].partition_estimate.use_pad == 0):
								no.partition_estimate.start_cycle = cycle
						else:
							no.partition_estimate.start_cycle = cycle
					
					# To speed up simulation we don't need to check all cycles, only those where events happen
					if (no.partition_estimate.start_cycle + no.planner_estimate.cycles_per_row) - cycle < min_wait and no.partition_estimate.start_cycle != -1 and (no.partition_estimate.start_cycle + no.planner_estimate.cycles_per_row) > cycle :
						min_wait = (no.partition_estimate.start_cycle + no.planner_estimate.cycles_per_row) - cycle
				
				#Advances the simulation by 1 cycle if the min_wait is 1 cycle or after reaching a node that produced an output otherwise advance with min_wait 
				if flag == 1:
					cycle += 1
					flag = 0
					continue
				if(min_wait > 1):
					if event_cycle != -1:
						if (cycle + min_wait-1 > event_cycle) and (cycle < event_cycle) and images_counter < num_images:
							cycle = event_cycle

							continue
					cycle += (min_wait-1)
					continue
				else:
					if flag == 0:
						cycle += 1
						flag = 1
						continue

			return cycle,[]
		



		
		a = time.time()
		x,times  = cycle_cycle_simulator(arranged_conv) 
		times = times 
		diff=[]
		for i in range(len(times)):
			times[i] = times[i] 
			if i == 0:
				continue
			diff.append(times[i]-times[i-1]) 
		y = 1000 * (times[0]/self.fmax) 
		average = np.mean(diff) 
		throughput = int(self.fmax/average) 
		
		#Add network latency effect
		extra_latency = 0
		min_thrpughput = 9999999999
		#fh.write('latency in ms for: ' +str(self.fmax/1000000) +' MHz: ' + str(float(y)) + ' ms Throughput is ' + str(throughput) +  ' imgs/s Runtime in s: ' + str(float("{:.5f}".format(time.time()-a)))+'\n')
		print('latency in ms for: ' +str(self.fmax/1000000) +' MHz: ' + str(float(y)) + ' ms Throughput is ' + str(throughput) +  ' imgs/s Runtime in s: ' + str(float("{:.5f}".format(time.time()-a)))+'\n')

		self.latency = float(y)
		self.throughput = throughput
		return





class partition_estimate():
		def __init__(self, node):
			self.on_skip_path = False
			self.chip_id = -1
			self.group_id = -1
			self.input_buffer_elements = 0
			self.produced_outputs = 0
			self.max_buffer_space = 0
			self.done_working = 0
			self.start_cycle = -1
			self.pad = 0
			self.finish_cycle = 0
			self.outputs_for_next_layer=0  #If there is a Maxpool for example and this node's output is 112 the next node will have an input of 56 and this variable will be set to two
			self.multiplier_count = 0
			self.fixed_initial_buffer = 0
			self.last_produced_element = 0
			self.total_subtracted = 0
			self.remain_pad = 0
			self.current_image = 1
			self.edge_node = 0
			self.current_elements = 0
			self.use_pad = 0
			self.determine_buffer_size(node)
			self.compute_row_cycle_latency(node)
			

		def determine_buffer_size(self,n):
			if n.type not in ['Conv2D', 'DepthwiseConv2dNative']:
				return
			input_channels= n.get_kernel_shape()[2]
			n_channel_splits = n.planner_estimate.n_channel_splits
			ic_groups = n_channel_splits
			kernel_height = n.get_kernel_shape()[0]
			vertical_stride =  n.get_strides()[1]

			f_max_channels_in_input_buffer_line = lambda: math.ceil(input_channels / n_channel_splits)
			f_max_ia_buffer_depth = lambda: f_max_channels_in_input_buffer_line()  * (max(kernel_height, vertical_stride) + vertical_stride)
			while f_max_ia_buffer_depth() > 64 and f_max_ia_buffer_depth() < 70:
				n_channel_splits += 2
			n.planner_estimate.n_channel_splits = n_channel_splits
			parallel_weights_per_dsp = min(2, n_channel_splits)
			max_channels_in_input_buffer_line = f_max_channels_in_input_buffer_line()
			max_ia_buffer_depth = f_max_ia_buffer_depth()
			min_ia_buffer_depth = max_ia_buffer_depth
			ia_buffer_has_lutrams = False
			if min_ia_buffer_depth <= 32:
				ia_buffer_has_lutrams = True
				min_ia_buffer_depth = math.floor(32 / max_channels_in_input_buffer_line) * max_channels_in_input_buffer_line
			elif min_ia_buffer_depth <= 64:
				ia_buffer_has_lutrams = True
				min_ia_buffer_depth = math.floor(64 / max_channels_in_input_buffer_line) * max_channels_in_input_buffer_line
			else:
				if min_ia_buffer_depth < 128:
					print("WARNING: IABuffer depth greater than 64, less than 128 ", min_ia_buffer_depth)
				target_buffer_depth = math.ceil(max_ia_buffer_depth / 512) * 512
				min_ia_buffer_depth = math.floor(target_buffer_depth / max_channels_in_input_buffer_line) * max_channels_in_input_buffer_line

			
			lines_in_ia_buffer = min_ia_buffer_depth // max_channels_in_input_buffer_line
			num_buffered_lines = max(kernel_height + vertical_stride, lines_in_ia_buffer)
			n_channels = [math.ceil(input_channels / float(ic_groups)) for _ in range(input_channels % ic_groups)]
			n_channels += [input_channels // ic_groups for _ in range(ic_groups - (input_channels % ic_groups))]
			total_buffer_depths = [n_channels[i] * num_buffered_lines for i in range(ic_groups)]
			buffer_specs = [dl.RAM.size_for_depth(bd) for bd in total_buffer_depths]
			buffer_depths = [[min(bd, (i * bs[0] % bd - 1) % bs[0] + 1) for i in range(math.ceil(bd / bs[0]))] for bd,bs in zip(total_buffer_depths, buffer_specs)]
			buffer_depths = [[np.sum(buffer_depth_list)] for buffer_depth_list in buffer_depths]
			self.max_buffer_space = (buffer_depths[0][0] // n_channels[0]) 
			self.pad  = int(np.ceil((vertical_stride*(n.get_output_shape()[1]-1) - n.inputs[0]._from.example_outputs[0].shape[1] + kernel_height)))
			if self.pad == 0:
				self.input_buffer_elements = []
			else:
				self.input_buffer_elements = [(1,1)]
				self.remain_pad =self.pad - 1

			self.fixed_initial_buffer = len(self.input_buffer_elements)
			 

			

		def compute_row_cycle_latency(self,n):
			if n.type not in ['Conv2D', 'DepthwiseConv2dNative']:
				return
			n.planner_estimate.cycles_per_row = int(n.planner_estimate.time_weight/n.get_output_shape()[1])
				
