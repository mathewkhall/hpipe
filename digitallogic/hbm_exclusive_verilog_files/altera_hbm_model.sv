// (C) 2001-2020 Intel Corporation. All rights reserved.
// Your use of Intel Corporation's design tools, logic functions and other 
// software and tools, and its AMPP partner logic functions, and any output 
// files from any of the foregoing (including device programming or simulation 
// files), and any associated documentation or information are expressly subject 
// to the terms and conditions of the Intel Program License Subscription 
// Agreement, Intel FPGA IP License Agreement, or other applicable 
// license agreement, including, without limitation, that your use is for the 
// sole purpose of programming logic devices manufactured by Intel and sold by 
// Intel or its authorized distributors.  Please refer to the applicable 
// agreement for further details.



`timescale 1 ps / 1 ps

`define ALTERA_HBM_MODEL_DECL_CH_PORTS(CH) \
   input ck_t_``CH, \
   input ck_c_``CH, \
   input [1 - 1:0] cke_``CH, \
   input [PORT_MEM_C_WIDTH - 1:0] c_``CH, \
   input [PORT_MEM_R_WIDTH - 1:0] r_``CH, \
   inout  [PORT_MEM_DQ_WIDTH - 1:0]  dq_``CH, \
   inout  [PORT_MEM_DM_WIDTH - 1:0] dm_``CH, \
   inout  [PORT_MEM_DBI_WIDTH - 1:0] dbi_``CH, \
   inout  [PORT_MEM_PAR_WIDTH - 1:0] par_``CH, \
   input [PORT_MEM_WDQS_T_WIDTH - 1:0]    wdqs_t_``CH, \
   input [PORT_MEM_WDQS_C_WIDTH - 1:0]    wdqs_c_``CH, \
   output  [PORT_MEM_RDQS_T_WIDTH - 1:0]    rdqs_t_``CH, \
   output  [PORT_MEM_RDQS_C_WIDTH - 1:0]    rdqs_c_``CH, \
   inout  [8 - 1:0] rd_``CH, \
   input rr_``CH, \
   input rc_``CH, \
   output  [PORT_MEM_DERR_WIDTH - 1:0] derr_``CH, \
   output  aerr_``CH,

`define ALTERA_HBM_MODEL_CH_INST(CH) \
    altera_hbm_mem # (\
        .PORT_MEM_R_WIDTH (PORT_MEM_R_WIDTH),\
        .PORT_MEM_C_WIDTH (PORT_MEM_C_WIDTH),\
        .PORT_MEM_DQ_WIDTH (PORT_MEM_DQ_WIDTH),\
        .PORT_MEM_DM_WIDTH (PORT_MEM_DM_WIDTH),\
        .PORT_MEM_DBI_WIDTH (PORT_MEM_DBI_WIDTH),\
        .PORT_MEM_PAR_WIDTH (PORT_MEM_PAR_WIDTH),\
        .PORT_MEM_DERR_WIDTH (PORT_MEM_DERR_WIDTH),\
        .PORT_MEM_RDQS_T_WIDTH (PORT_MEM_RDQS_T_WIDTH),\
        .PORT_MEM_RDQS_C_WIDTH (PORT_MEM_RDQS_C_WIDTH),\
        .PORT_MEM_WDQS_T_WIDTH (PORT_MEM_WDQS_T_WIDTH),\
        .PORT_MEM_WDQS_C_WIDTH (PORT_MEM_WDQS_C_WIDTH),\
        .PORT_MEM_RD_WIDTH (PORT_MEM_RD_WIDTH),\
        .MEM_NUM_PSEUDO_CHANNEL (MEM_NUM_PSEUDO_CHANNEL),\
        .MEM_CHANNEL_ID (CH),\
        .MEM_EFFECTIVE_BA_WIDTH (MEM_EFFECTIVE_BA_WIDTH),\
        .CTRL_RA_WIDTH (CTRL_RA_WIDTH),\
        .CTRL_CA_WIDTH (CTRL_CA_WIDTH),\
        .CTRL_HBM_RL (MEM_CH``CH``_HBM_RL),\
        .CTRL_HBM_WL (MEM_CH``CH``_HBM_WL),\
        .CTRL_HBM_TWR (MEM_CH``CH``_HBM_TWR),\
        .CTRL_HBM_TRCDRD (MEM_CH``CH``_HBM_TRCDRD),\
        .CTRL_HBM_TRCDWR (MEM_CH``CH``_HBM_TRCDWR),\
        .CTRL_HBM_TRTP (MEM_CH``CH``_HBM_TRTPL_BL4),\
        .CTRL_HBM_TRP (MEM_CH``CH``_HBM_TRP),\
        .CTRL_CA_PAR_EN (CTRL_CA_PAR_EN),\
        .CTRL_WR_PAR_EN (CTRL_WR_PAR_EN[CH]),\
        .CTRL_RD_PAR_EN (CTRL_RD_PAR_EN[CH]),\
        .CTRL_WR_DBI_EN (CTRL_WR_DBI_EN[CH]),\
        .CTRL_RD_DBI_EN (CTRL_RD_DBI_EN[CH]),\
        .CTRL_WR_DM_EN  (CTRL_WR_DM_EN[CH]),\
        .CTRL_MECC_EN (MEM_CH``CH``_MECC_EN),\
        .CTRL_WR_PAR_DIAG (CTRL_WR_PAR_DIAG),\
        .CTRL_RD_PAR_DIAG (CTRL_RD_PAR_DIAG),\
        .CTRL_SBE_DIAG (CTRL_SBE_DIAG),\
        .CTRL_PAR_LAT (MEM_CH``CH``_PAR_LAT),\
        .MEM_DQS_TO_CLK_CAPTURE_DELAY (MEM_DQS_TO_CLK_CAPTURE_DELAY),\
        .MEM_CLK_TO_DQS_CAPTURE_DELAY (MEM_CLK_TO_DQS_CAPTURE_DELAY),\
        .TIMING_TDQSCK (TIMING_TDQSCK),\
        .MEM_VERBOSE (MEM_VERBOSE)\
    ) altera_hbm_mem_``CH (\
       .reset_n(reset_n),\
       .ck_t(ck_t_``CH),\
       .ck_c(ck_c_``CH),\
       .cke(cke_``CH),\
       .c(c_``CH),\
       .r(r_``CH),\
       .dq(dq_``CH),\
       .dm(dm_``CH),\
       .dbi(dbi_``CH),\
       .par(par_``CH),\
       .wdqs_t(wdqs_t_``CH),\
       .wdqs_c(wdqs_c_``CH),\
       .rdqs_t(rdqs_t_``CH),\
       .rdqs_c(rdqs_c_``CH),\
       .rd(rd_``CH),\
       .rr(rr_``CH),\
       .rc(rc_``CH),\
       .derr(derr_``CH),\
       .aerr(aerr_``CH)\
    );

module altera_hbm_model
    # (parameter
        PORT_MEM_R_WIDTH = 6,
        PORT_MEM_C_WIDTH = 8,
        PORT_MEM_DQ_WIDTH = 128,
        PORT_MEM_DM_WIDTH = 16,
        PORT_MEM_DBI_WIDTH = 16,
        PORT_MEM_PAR_WIDTH = 16,
        PORT_MEM_DERR_WIDTH = 16,
        PORT_MEM_RDQS_T_WIDTH = 16,
        PORT_MEM_RDQS_C_WIDTH = 16,
        PORT_MEM_WDQS_T_WIDTH = 16,
        PORT_MEM_WDQS_C_WIDTH = 16,
        PORT_MEM_RD_WIDTH = 8,
        PORT_M2U_BRIDGE_TEMP_WIDTH = 3,
        PORT_M2U_BRIDGE_WSO_WIDTH = 8,
        MEM_NUM_PSEUDO_CHANNEL = 2,

        MEM_EFFECTIVE_BA_WIDTH = 0,
        CTRL_RA_WIDTH = 0,
        CTRL_CA_WIDTH = 0,

        MEM_CH0_HBM_RL = 0,
        MEM_CH1_HBM_RL = 0,
        MEM_CH2_HBM_RL = 0,
        MEM_CH3_HBM_RL = 0,
        MEM_CH4_HBM_RL = 0,
        MEM_CH5_HBM_RL = 0,
        MEM_CH6_HBM_RL = 0,
        MEM_CH7_HBM_RL = 0,
        MEM_CH0_HBM_WL = 0,
        MEM_CH1_HBM_WL = 0,
        MEM_CH2_HBM_WL = 0,
        MEM_CH3_HBM_WL = 0,
        MEM_CH4_HBM_WL = 0,
        MEM_CH5_HBM_WL = 0,
        MEM_CH6_HBM_WL = 0,
        MEM_CH7_HBM_WL = 0,
        MEM_CH0_HBM_TWR = 0,
        MEM_CH1_HBM_TWR = 0,
        MEM_CH2_HBM_TWR = 0,
        MEM_CH3_HBM_TWR = 0,
        MEM_CH4_HBM_TWR = 0,
        MEM_CH5_HBM_TWR = 0,
        MEM_CH6_HBM_TWR = 0,
        MEM_CH7_HBM_TWR = 0,
        MEM_CH0_PAR_LAT = 0,
        MEM_CH1_PAR_LAT = 0,
        MEM_CH2_PAR_LAT = 0,
        MEM_CH3_PAR_LAT = 0,
        MEM_CH4_PAR_LAT = 0,
        MEM_CH5_PAR_LAT = 0,
        MEM_CH6_PAR_LAT = 0,
        MEM_CH7_PAR_LAT = 0,
        MEM_CH0_HBM_TRCDRD = 16,
        MEM_CH1_HBM_TRCDRD = 16,
        MEM_CH2_HBM_TRCDRD = 16,
        MEM_CH3_HBM_TRCDRD = 16,
        MEM_CH4_HBM_TRCDRD = 16,
        MEM_CH5_HBM_TRCDRD = 16,
        MEM_CH6_HBM_TRCDRD = 16,
        MEM_CH7_HBM_TRCDRD = 16,
        MEM_CH0_HBM_TRCDWR = 14,
        MEM_CH1_HBM_TRCDWR = 14,
        MEM_CH2_HBM_TRCDWR = 14,
        MEM_CH3_HBM_TRCDWR = 14,
        MEM_CH4_HBM_TRCDWR = 14,
        MEM_CH5_HBM_TRCDWR = 14,
        MEM_CH6_HBM_TRCDWR = 14,
        MEM_CH7_HBM_TRCDWR = 14,
        MEM_CH0_HBM_TRTPL_BL4 = 1,
        MEM_CH1_HBM_TRTPL_BL4 = 1,
        MEM_CH2_HBM_TRTPL_BL4 = 1,
        MEM_CH3_HBM_TRTPL_BL4 = 1,
        MEM_CH4_HBM_TRTPL_BL4 = 1,
        MEM_CH5_HBM_TRTPL_BL4 = 1,
        MEM_CH6_HBM_TRTPL_BL4 = 1,
        MEM_CH7_HBM_TRTPL_BL4 = 1,
        MEM_CH0_HBM_TRP = 1,
        MEM_CH1_HBM_TRP = 1,
        MEM_CH2_HBM_TRP = 1,
        MEM_CH3_HBM_TRP = 1,
        MEM_CH4_HBM_TRP = 1,
        MEM_CH5_HBM_TRP = 1,
        MEM_CH6_HBM_TRP = 1,
        MEM_CH7_HBM_TRP = 1,
        MEM_CH0_MECC_EN = 0,
        MEM_CH1_MECC_EN = 0,
        MEM_CH2_MECC_EN = 0,
        MEM_CH3_MECC_EN = 0,
        MEM_CH4_MECC_EN = 0,
        MEM_CH5_MECC_EN = 0,
        MEM_CH6_MECC_EN = 0,
        MEM_CH7_MECC_EN = 0,

        CTRL_CA_PAR_EN = 0,
        CTRL_WR_PAR_EN = 0,
        CTRL_RD_PAR_EN = 0,
        CTRL_WR_DBI_EN = 0,
        CTRL_RD_DBI_EN = 0,
        CTRL_WR_DM_EN  = 8'b11111111,
        CTRL_WR_PAR_DIAG = 0,
        CTRL_RD_PAR_DIAG = 0,
        CTRL_SBE_DIAG = 0,
        MEM_DQS_TO_CLK_CAPTURE_DELAY = 100,
        MEM_CLK_TO_DQS_CAPTURE_DELAY = 100000,
        TIMING_TDQSCK = 1000,
        MEM_VERBOSE = 1,
        MEM_HBM_FLIPPED = 0
    )
    (

   `ALTERA_HBM_MODEL_DECL_CH_PORTS(0)
   `ALTERA_HBM_MODEL_DECL_CH_PORTS(1)
   `ALTERA_HBM_MODEL_DECL_CH_PORTS(2)
   `ALTERA_HBM_MODEL_DECL_CH_PORTS(3)
   `ALTERA_HBM_MODEL_DECL_CH_PORTS(4)
   `ALTERA_HBM_MODEL_DECL_CH_PORTS(5)
   `ALTERA_HBM_MODEL_DECL_CH_PORTS(6)
   `ALTERA_HBM_MODEL_DECL_CH_PORTS(7)

   input  reset_n,
   output cattrip,
   output [PORT_M2U_BRIDGE_TEMP_WIDTH - 1:0] temp,
   output [PORT_M2U_BRIDGE_WSO_WIDTH - 1 : 0] wso,

   input wrst_n,
   input wrck,
   input shiftwr,
   input capturewr,
   input updatewr,
   input selectwir,
   input wsi

);

   assign cattrip = 1'b0;
   assign temp    = 3'b011;
   assign wso     = '0;

   `ALTERA_HBM_MODEL_CH_INST(0)
   `ALTERA_HBM_MODEL_CH_INST(1)
   `ALTERA_HBM_MODEL_CH_INST(2)
   `ALTERA_HBM_MODEL_CH_INST(3)
   `ALTERA_HBM_MODEL_CH_INST(4)
   `ALTERA_HBM_MODEL_CH_INST(5)
   `ALTERA_HBM_MODEL_CH_INST(6)
   `ALTERA_HBM_MODEL_CH_INST(7)
endmodule
