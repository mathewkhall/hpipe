# How to obtain these files
The files in this directory can be obtained by creating an example design in Quartus using the hbm ip provided in this project. Opening the IP in Quartus gives the option to generate an example design. Any change in the IP will require regeneration of these files. 
