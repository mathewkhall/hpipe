from copy import copy
from digitallogic.utils import clog2, v_hex_str, get_bit_mask, v_comment_string, print_stage_header, hex_str, BitArray
from digitallogic.digitallogic import *
import sys
import os
import warnings
import math
import inspect
import numpy as np
import hashlib
from string import Template
import hpipe.ArchLoader as arch


class HBMController(Module):
	"""
		The HBM controller is the interface between different SharedHBMControllers and HBM
		Using the method assign_consumers_to_controller, it connects the DCFIFO it instantiates to the consumer-specific burst-matching FIFOs of each SharedHBMController
	"""
	def __init__(self, clock, reset, kind="hbmcontroller", ADDR_WIDTH=23, DATA_WIDTH=256, **kwargs):
		inst = self.inst
		super(HBMController, self).__init__("hbmcontroller", kind=kind, **kwargs)

		self.address_pairs = []

		with ModuleScope(self):
			inst(PhantomFlop, clock=clock, reset=reset)
			clock.add_sink(self)
			self.clock = clock
			self.reset = reset

			self.hbm_interface_clk = inst(Logic, bits=1, signed=False, name = "hbm_interface_clk")
			self.hbm_interface_reset = inst(Logic, bits=1, signed=False, name="hbm_interface_reset")
			self.hbm_writing_is_done = inst(Logic, bits=1, name="hbm_writing_is_done")
			MAX_DATA_WIDTH = 256
			MAX_ADDR_WIDTH = 23 #The total address is 28 bits but the last 5 must be zero as per the documentation

			with ParamScope(bits=1):
				self.arready = inst(Logic, name="arready")
				self.arvalid = inst(Logic, name="arvalid")
				self.rready = inst(Logic, name="rready")
				self.rvalid = inst(Logic, name="rvalid")

				self.axi_arready = inst(Logic, name="axi_arready")
				self.axi_arvalid = inst(Logic, name="axi_arvalid")
				self.axi_rready = inst(Logic, name="axi_rready")
				self.axi_rvalid = inst(Logic, name="axi_rvalid")

				self.axi_rready.set_driver(self.rready)
				self.rvalid.set_driver(self.axi_rvalid)
				self.arready.set_driver(self.axi_arready)
				self.axi_arvalid.set_driver(self.arvalid)

			self.araddr = inst(Logic, bits=ADDR_WIDTH, name="araddr")

			self.rdata = inst(Logic, bits=DATA_WIDTH, name="rdata")	

			self.axi_araddr = inst(Logic, bits=MAX_ADDR_WIDTH + 5, name="axi_araddr")

			self.axi_rdata = inst(Logic, bits=MAX_DATA_WIDTH, name="axi_rdata")


			if (ADDR_WIDTH == MAX_ADDR_WIDTH):
				self.axi_araddr.set_driver(inst(Concat, inst(Constant, bits=5 , value=0, name="araddr_fivezero_padding"), self.araddr))
			else:
				if (ADDR_WIDTH > MAX_ADDR_WIDTH):
					print("Address width is larger than 23, which is currently not supported. The 23 are concatenated with 5 zeros as per the HBM documentation.")
					exit(-1)
				else:
					self.axi_araddr.set_driver(inst(Concat, inst(Constant, bits=5, value=0), self.araddr, inst(Constant, bits=MAX_ADDR_WIDTH-ADDR_WIDTH, value=0, name="araddr_zero_padding")))

			if (DATA_WIDTH == MAX_DATA_WIDTH):
				self.rdata.set_driver(self.axi_rdata)
			else:
				if (DATA_WIDTH > MAX_DATA_WIDTH):
					print("Data width can only go up to 256 currently.")
					exit(-1)
				else:
					self.rdata.set_driver(self.axi_rdata[DATA_WIDTH-1:0])


		self.requires_driver = [self.hbm_interface_clk, self.hbm_interface_reset, self.rready, self.axi_rvalid, self.axi_arready, self.arvalid, self.axi_rdata, self.araddr, self.hbm_writing_is_done]

	def assign_consumers_to_controller(self, hbmc_list, enable_sim):
		#We need to connect all the consumers through the different FIFOs available in the SharedHBMController modules


		inst = self.inst
		with ModuleScope(self):

			#Insert a shared DCFIFO
			hbm_clk = inst(Clock, force_simple_assignment=True, name = "hbm_clk")
			hbm_clk.set_driver(self.hbm_interface_clk)
			if enable_sim:
				rd_hbm_fifo = inst(FIFO, bits=(240+9), depth=64)
				rd_hbm_fifo_data = rd_hbm_fifo.r_data
			else:
				rd_hbm_fifo = inst(DCFIFO, rdclk=self.clock, wrclk=hbm_clk, bits=(240+9), depth=64, showahead = "ON")
				rd_hbm_fifo_data = rd_hbm_fifo.r_data.delay(1)


			with ParamScope(clock=hbm_clk, reset=self.hbm_interface_reset):
				BURST_SIZE=8 #If you change BURST_SIZE, you must also change the depth of the burst-matching FIFOs in SharedHBMController to ensure depth >= BURST_SIZE
				#The burst counter allows at most BURST_SIZE transfers from one consumer before giving control to another consumer
				burst_counter = inst(Counter, reset_value=0, increment_value=1, end_value=BURST_SIZE-1, name="burst_counter")
				burst_counter.set_increment_condition(self.arready)

				#The consumer counter helps as a select signal for the muxes
				consumer_selection_counter = inst(Counter, reset_value=0, increment_value=1, end_value=len(hbmc_list)-1, name="consumer_selection_counter")
				consumer_selection_counter.set_increment_condition(burst_counter.done_and_incrementing)				
				#Let's make arrays of signals for laters
				enable_consumer_addr_transactions = [inst(EQ, consumer_selection_counter.current_value, inst(Constant, bits=consumer_selection_counter.current_value._bit_width, value=i)) for i in range(len(hbmc_list))]
				enable_consumer_data_transactions = [inst(EQ, rd_hbm_fifo_data[(consumer_selection_counter.current_value._bit_width + 239):240], inst(Constant, bits=consumer_selection_counter.current_value._bit_width, value=i)) for i in range(len(hbmc_list))]
				araddr_array = []
				arvalid_array = []

			has_space_for_data_array = [[] for _ in range(len(hbmc_list)+1)]
			all_have_space_for_data = [inst(Logic, bits=1, name="all_have_space_for_data_"+str(i)) for i in range(len(hbmc_list))] #We'll make multiple copies of this so timing improves
			

			max_address = 0
			for hbmc in hbmc_list:
				max_address += (hbmc.end_address - hbmc.start_address)

			all_addresses_counted = 0
			for consumer_number,hbmc in enumerate(hbmc_list):
				self.address_pairs.append((hbmc.start_address, hbmc.end_address))

				hbmc.hbm_interface_clk.set_driver(self.hbm_interface_clk)
				hbmc.hbm_interface_reset.set_driver(self.hbm_interface_reset)
				hbmc.hbm_writing_is_done.set_driver(self.hbm_writing_is_done)

				with ParamScope(clock=hbm_clk, reset=self.hbm_interface_reset):
					hbmc.has_space_for_address.set_driver(inst(AND, self.axi_arready, enable_consumer_addr_transactions[consumer_number]))
					araddr_array.append(inst(ADD, hbmc.read_address, inst(Constant, bits=clog2(max_address), value=all_addresses_counted, name="base_address_"+str(consumer_number))))
					arvalid_array.append(hbmc.address_valid)
				for i in range(len(hbmc_list)+1):
					has_space_for_data_array[i].append(hbmc.has_space_for_data.delay(2)) #Has space for data is an almost full signal


				hbmc.shared_data.set_driver(rd_hbm_fifo_data[239:0])
				hbmc.shared_data_valid.set_driver(inst(AND, inst(AND, all_have_space_for_data[consumer_number], inst(NOT, rd_hbm_fifo.empty)).delay(1), enable_consumer_data_transactions[consumer_number]))


				all_addresses_counted += (hbmc.end_address - hbmc.start_address)

			#Connect all_have_space_for_data
			for i in range(len(hbmc_list)):
				if len(has_space_for_data_array[0]) == 1:
					all_have_space_for_data[i].set_driver(has_space_for_data_array[0][0])
				else:
					all_have_space_for_data[i].set_driver(inst(AND, *(has_space_for_data_array[i])))

			#Connect signals for the rd_hbm_fifo
			self.rready.set_driver(inst(NOT, rd_hbm_fifo.full))
			rd_hbm_fifo.w_en.set_driver(inst(AND, self.rvalid, inst(NOT, rd_hbm_fifo.full)))
			rd_hbm_fifo.w_data.set_driver(self.rdata)
			rd_hbm_fifo.r_en.set_driver(inst(AND, *(has_space_for_data_array[-1]), inst(NOT, rd_hbm_fifo.empty)))
			
			with ParamScope(clock=hbm_clk, reset=self.hbm_interface_reset):
				#Connect the right araddr and arvalid signals
				selected_araddr = inst(MuxV2, consumer_selection_counter.current_value, *araddr_array)
				self.araddr.set_driver(inst(Concat, selected_araddr, inst(Constant, bits=23-selected_araddr._bit_width, value=0, name="araddr_zero_padding"))) #23 is the araddr bit width
				self.arvalid.set_driver(inst(MuxV2, consumer_selection_counter.current_value, *arvalid_array))
		return

class SharedHBMController(Module):
	"""
		The SharedHBMController is the interface a consumer (e.g. HPIPE's TensorConv) sees when accessing HBM.
		A SharedHBMController is connected along with other SharedHBMController to a single HBMController, which is in turn connected to an HBM Pseudo-Channel
	"""
	def __init__(self, clock, reset, kind="sharedhbmcontroller", ADDR_WIDTH=23, DATA_WIDTH=256, **kwargs):
		inst = self.inst
		super(SharedHBMController, self).__init__("sharedhbmcontroller", kind=kind, **kwargs)

		self.start_address = -1
		self.end_address = -1

		with ModuleScope(self):
			inst(PhantomFlop, clock=clock, reset=reset)
			clock.add_sink(self)
			self.clock = clock
			self.reset = reset

			self.ADDR_WIDTH = ADDR_WIDTH
			self.DATA_WIDTH = DATA_WIDTH

			self.hbm_interface_clk = inst(Logic, bits=1, signed=False, name = "hbm_interface_clk")
			self.hbm_interface_reset = inst(Logic, bits=1, signed=False, name="hbm_interface_reset")
			self.hbm_writing_is_done = inst(Logic, bits=1, name="hbm_writing_is_done")

			with ParamScope(bits=1):
				self.read_data_available = inst(Logic, name="read_data_valid")
				self.shared_data_valid = inst(Logic, name="shared_data_valid")
				self.has_space_for_data = inst(Logic, name="has_space_for_data")
				self.read_data_request = inst(Logic, name="read_data_request")

			with ParamScope(bits=DATA_WIDTH):
				self.read_data = inst(Logic, name="read_data")
				self.shared_data = inst(Logic, name="shared_data")

			with ParamScope(clock=self.hbm_interface_clk, reset=self.hbm_interface_reset):
				with ParamScope(bits=1):
					self.address_valid = inst(Logic, name="address_valid")
					self.has_space_for_address = inst(Logic, name="has_space_for_address")
				with ParamScope(bits=ADDR_WIDTH):
					self.read_address = inst(Logic, name="read_address")

			#Change the depth local_hbm_rd_data_fifo if you change BURST_SIZE in HBMController above. 16 for BURST_SIZE=8, 32 for BURST_SIZE=16, 48 for BURST_SIZE=32 (48 not 64 because 64 will instantiate M20Ks which are more in demand than ALMs)
			local_hbm_rd_data_fifo = inst(FIFO, bits=DATA_WIDTH, depth=16, almost_full=4, name="local_hbm_rd_data_fifo") #Almost full is because in simulation we use FIFOs in non-showahead mode so we need a guardband of 1
			local_hbm_rd_data_fifo.w_en.set_driver(inst(AND, self.shared_data_valid, inst(NOT, local_hbm_rd_data_fifo.full)))
			local_hbm_rd_data_fifo.w_data.set_driver(self.shared_data)
			self.has_space_for_data.set_driver(inst(NOT, local_hbm_rd_data_fifo.almost_full))
			self.read_data.set_driver(local_hbm_rd_data_fifo.r_data)
			self.read_data_available.set_driver(inst(NOT, local_hbm_rd_data_fifo.empty))
			local_hbm_rd_data_fifo.r_en.set_driver(self.read_data_request)


		self.requires_driver = [self.hbm_interface_clk, self.hbm_interface_reset, self.address_valid, self.read_address, self.shared_data, self.shared_data_valid, self.read_data_request, self.hbm_writing_is_done, self.has_space_for_address]


	def assign_start_end_addresses(self, start_address, end_address):
		self.start_address = start_address
		self.end_address = end_address
		return

class FakeHBMchannel(Module):
	"""
		A FakeHBMchannel simulates the HBM Pseudo-Channel and connects to an HBMController via an AXI interface.
		This module must be used exclusively in simulation. It is functionally correct but timing approximate. 
		It is instantiated with the correct HBM content (avoids losing time writing to HBM in simulations) and relies on a ROM.
	"""
	def __init__(self, clock, reset, hbm_content, given_seed=5724, kind="FakeHBMchannel", HBM_EFFICIENCY = 90, **kwargs):
		inst = self.inst
		super(FakeHBMchannel, self).__init__("FakeHBMchannel", kind=kind, **kwargs)

		with ModuleScope(self):
			inst(PhantomFlop, clock=clock, reset=reset)
			clock.add_sink(self)
			self.clock = clock
			self.reset = reset

			MAX_DATA_WIDTH = 256
			MAX_ADDR_WIDTH = 23 #The total address is 28 bits but the last 5 must be zero as per the documentation

			with ParamScope(bits=1):
				self.axi_arready = inst(Logic, name="axi_arready")
				self.axi_arvalid = inst(Logic, name="axi_arvalid")
				self.axi_rready = inst(Logic, name="axi_rready")
				self.axi_rvalid = inst(Logic, name="axi_rvalid")

			self.axi_araddr = inst(Logic, bits=MAX_ADDR_WIDTH + 5, name="axi_araddr")
			
			self.axi_rdata = inst(Logic, bits=MAX_DATA_WIDTH, name="axi_rdata")

			self.pseudo_random_shift = inst(Logic, bits=1, name="pseudo_random_shift") #This should be 1 around x % of the time where x is the read efficiency
			lfsr = inst(LFSR, bits=16, seed=given_seed)
			closest_value_to_efficiency = math.ceil((HBM_EFFICIENCY/100)*((2**(16))-1))
			self.pseudo_random_shift.set_driver(inst(LT, lfsr.random_value, inst(Constant, bits=16, value=closest_value_to_efficiency, name="efficiency_comparison")))

			araddr_fifo = inst(FIFO, bits=28, depth=64, name="araddr_fifo")
			araddr_fifo.w_en.set_driver(inst(AND, inst(NOT, araddr_fifo.full), self.axi_arvalid))
			araddr_fifo.w_data.set_driver(self.axi_araddr)
			self.axi_arready = inst(NOT, araddr_fifo.full, name="fifo_not_full")

			araddr_sr = inst(ShiftRegister, bits=(28+1), depth=30, name="araddr_shifter")
			rdata_fifo = inst(FIFO, bits=256, depth=16, almost_full=2, name="rdata_fifo")
			araddr_sr.should_shift.set_driver(inst(AND, self.pseudo_random_shift, inst(NOT, rdata_fifo.almost_full)).delay(1))
			address_is_valid = inst(AND, self.pseudo_random_shift, inst(NOT, araddr_fifo.empty), inst(NOT, rdata_fifo.almost_full))
			araddr_fifo.r_en.set_driver(address_is_valid)
			araddr_sr.d.set_driver(inst(Concat, address_is_valid.delay(1), araddr_fifo.r_data))

			hbm_rom = inst(ROM, bits=256, content_list=hbm_content)
			len_of_content = clog2(len(hbm_content))
			hbm_rom.r_addr.set_driver(araddr_sr.q[(6+(len_of_content-1)):6])
			bootup_counter = inst(Counter, reset_value=0, increment_value=1, end_value=30, name="bootup_counter")
			bootup_counter.set_increment_condition(inst(AND, araddr_sr.should_shift, inst(NOT, bootup_counter.is_done)))
			rom_en = inst(AND, araddr_sr.should_shift, araddr_sr.q[0], bootup_counter.is_done)
			hbm_rom.r_en.set_driver(rom_en)

			rdata_fifo.w_en.set_driver(rom_en.delay(1))
			rdata_fifo.w_data.set_driver(hbm_rom.r_data)
			rdata_fifo.r_en.set_driver(inst(AND, inst(NOT, rdata_fifo.empty), self.axi_rready))
			hbm_output = inst(Flop, bits=256, name="hbm_output")
			hbm_output_valid = inst(Flop, bits=1, name="hbm_output_valid_flop")
			hbm_output.set_driver(inst(MuxV2, self.axi_rready.delay(1), hbm_output, rdata_fifo.r_data))
			hbm_output_valid.set_driver(inst(MuxV2, inst(OR, self.axi_rready, inst(NOT, hbm_output_valid)), hbm_output_valid, inst(AND, inst(NOT, self.axi_rready), inst(AND, inst(NOT, rdata_fifo.empty), self.axi_rready).delay(1))))
			self.axi_rdata.set_driver(inst(MuxV2, hbm_output_valid, rdata_fifo.r_data, hbm_output))
			self.axi_rvalid.set_driver(inst(MuxV2, hbm_output_valid, inst(AND, self.axi_rready, inst(AND, inst(NOT, rdata_fifo.empty), self.axi_rready).delay(1)), self.axi_rready))

		self.requires_driver = [self.axi_rready, self.axi_arvalid, self.axi_araddr]



class HBM(Module):
	"""
		High-Bandwidth Memory. Please refer to the FPL 2024 Paper H2PIPE: High-Throughput CNN Inference on FPGAs with High-Bandwidth Memory for more information about the 
		challenges encountered in implementing HBM.
		The High Bandwidth Memory class contains top and bottom HBM stacks that each have 16 Pseudo-Channels (8 channels)
		Each pseudo-channel can be connected to an HBMController class via the method assign_controller_to_channel
	"""
	def __init__(self, pll_ref_clk, pll_ref_clk_bot, clock, kind="hbm", simulate_hbm = False, WRITE_PATH_NARROW_CONSTANT = 8, **kwargs):
		inst = self.inst
		super(HBM, self).__init__("hbm", kind=kind, **kwargs)
		with ModuleScope(self):
			self.pll_ref_clk = pll_ref_clk
			self.pll_ref_clk_bot = pll_ref_clk_bot
			self.ext_logic_clk = clock
			self.ext_logic_clk_locked = inst(Logic, bits=1, name="ext_logic_clk_locked")
			self.rst_n = inst(Logic, bits=1, signed=False, name="rst_n")
			self.hbm_only_reset = inst(Logic, bits=1, signed=False, name="hbm_only_reset")

			self.pll_ref_clk.add_sink(self)
			self.pll_ref_clk_bot.add_sink(self)
			self.ext_logic_clk.add_sink(self)

			self.simulate_hbm = simulate_hbm

			self.hbm_connections = []
			TOTAL_PSEUDO_CHANNELS = 32 

			channel_names = ["aw", "w", "ar", "r", "b"]

			for _ in range(TOTAL_PSEUDO_CHANNELS):
				axi_channels = {}
				for i in range(len(channel_names)):
					axi_signals = {
						"ready": [],
						"valid": [],
						"data": []
					}
					axi_channels[channel_names[i]] = axi_signals

				self.hbm_connections.append(axi_channels)

			
			self.hbm_pc_assignments = [[] for i in range(TOTAL_PSEUDO_CHANNELS)]
			self.address_assignments = [[] for i in range(TOTAL_PSEUDO_CHANNELS)]
			self.hbm_weights = []


			# Generate each HBM channel's basic AXI signals
			for channel_number in range(TOTAL_PSEUDO_CHANNELS):
				for k in self.hbm_connections[channel_number]:
					for h in self.hbm_connections[channel_number][k]:
						data_width = 256
						if k == "aw" or k == "ar": 
							data_width = 28
						if k == "b":
							data_width = 2
						if h != "data": 
							data_width = 1
						
						if (k == "aw" or k == "ar") and (h == "data"):
							end_of_attribute_string = k + "addr"
						else:
							end_of_attribute_string = k + h
						axi_placeholder = inst(Logic, bits=data_width, name="axi_"+str(channel_number//2)+ "_" + str(channel_number % 2)+ "_" + end_of_attribute_string)
						self.hbm_connections[channel_number][k][h].append(axi_placeholder)





			# Generate the clocks and resets for each channel (or each 2 pseudo-channels)\

			#There are two sets of clocks. The Logic type is propagated automatically by HPIPE
			#We drive clock type with the Logic type, setting force_simple_assignment to true
			self.hbm_clocks = [inst(Logic, bits=1, name="wmc_clk_"+str(i)) for i in range(TOTAL_PSEUDO_CHANNELS//2)]
			self.hbm_clocks_clk = [inst(Clock, name="wmc_clk_clk_"+str(i)) for i in range(TOTAL_PSEUDO_CHANNELS//2)]
			for i in range(len(self.hbm_clocks_clk)):
				self.hbm_clocks_clk[i].set_driver(self.hbm_clocks[i])

			#Resets
			self.hbm_resets = [inst(Logic, bits=1, name="wmc_rst_n_"+str(i)) for i in range(TOTAL_PSEUDO_CHANNELS//2)]

			#Write path information
			self.WRITE_PATH_NARROW_CONSTANT = WRITE_PATH_NARROW_CONSTANT
			assert(240 % self.WRITE_PATH_NARROW_CONSTANT == 0)
			self.write_path_data = [inst(Logic, bits=math.ceil(240/self.WRITE_PATH_NARROW_CONSTANT), name="write_path_data_"+str(i)) for i in range(TOTAL_PSEUDO_CHANNELS)]
			self.write_path_valid = [inst(Logic, bits=1, name="write_path_data_"+str(i)) for i in range(TOTAL_PSEUDO_CHANNELS)]
			self.writing_is_done = [inst(Logic, bits=1, name="hbm_writing_is_done_"+str(i)) for i in range(TOTAL_PSEUDO_CHANNELS)]

			# Generate the cal_success signal to indicate a succesful calibration
			self.cal_success = inst(Logic, signed=False, bits=1, name="hbm_cal_success")
			self.cal_success_bot = inst(Logic, signed=False, bits=1, name="hbm_cal_success_bot")

			# Finally, we define the mem_m2u_bridge conduit
			#Top
			self.bridge_shiftwr = inst(Logic, signed=False, bits=1, name="mem_m2u_bridge_shiftwr")
			self.bridge_updatewr = inst(Logic, signed=False, bits=1, name="mem_m2u_bridge_updatewr")
			self.bridge_temp = inst(Logic, signed=False, bits=3, name="mem_m2u_bridge_temp")
			self.bridge_wso = inst(Logic, signed=False, bits=8, name="mem_m2u_bridge_wso")
			self.bridge_cattrip = inst(Logic, signed=False, bits=1, name="mem_m2u_bridge_cattrip")
			self.bridge_wrck = inst(Logic, signed=False, bits=1, name="mem_m2u_bridge_wrck")
			self.bridge_wrst_n = inst(Logic, signed=False, bits=1, name="mem_m2u_bridge_wrst_n")
			self.bridge_reset_n = inst(Logic, signed=False, bits=1, name="mem_m2u_bridge_reset_n")
			self.bridge_capturewr = inst(Logic, signed=False, bits=1, name="mem_m2u_bridge_capturewr")
			self.bridge_selectwir = inst(Logic, bits=1, name="mem_m2u_bridge_selectwir")	
			self.bridge_wsi = inst(Logic, bits=1, name="mem_m2u_bridge_wsi")	

			#Bot
			self.bridge_shiftwr_bot = inst(Logic, signed=False, bits=1, name="mem_m2u_bridge_shiftwr_bot")
			self.bridge_updatewr_bot = inst(Logic, signed=False, bits=1, name="mem_m2u_bridge_updatewr_bot")
			self.bridge_temp_bot = inst(Logic, signed=False, bits=3, name="mem_m2u_bridge_temp_bot")
			self.bridge_wso_bot = inst(Logic, signed=False, bits=8, name="mem_m2u_bridge_wso_bot")
			self.bridge_cattrip_bot = inst(Logic, signed=False, bits=1, name="mem_m2u_bridge_cattrip_bot")
			self.bridge_wrck_bot = inst(Logic, signed=False, bits=1, name="mem_m2u_bridge_wrck_bot")
			self.bridge_wrst_n_bot = inst(Logic, signed=False, bits=1, name="mem_m2u_bridge_wrst_n_bot")
			self.bridge_reset_n_bot = inst(Logic, signed=False, bits=1, name="mem_m2u_bridge_reset_n_bot")
			self.bridge_capturewr_bot = inst(Logic, signed=False, bits=1, name="mem_m2u_bridge_capturewr_bot")
			self.bridge_selectwir_bot = inst(Logic, bits=1, name="mem_m2u_bridge_selectwir_bot")	
			self.bridge_wsi_bot = inst(Logic, bits=1, name="mem_m2u_bridge_wsi_bot")			
			

		self.requires_driver = [self.pll_ref_clk, self.pll_ref_clk_bot, self.ext_logic_clk, self.ext_logic_clk_locked, self.rst_n, self.hbm_only_reset]


	def assign_controller_to_channel(self, hbmc, channel_number, hbmc_number):
		inst = self.inst
		assert isinstance(hbmc, HBMController)
		with ModuleScope(self):
			for k in self.hbm_connections[channel_number]:
				for h in self.hbm_connections[channel_number][k]:
					data_width = 256
					if k == "aw" or k == "w" or k == "b":
						#We previously wired this here but we no longer do that now that the writes are driven by the write path
						continue 
					if k == "ar": 
						data_width = 28
					if h != "data": 
						data_width = 1

					if (k == "ar") and (h == "data"):
						attribute_string = "axi_" + k + "addr"
						end_of_attribute_string = k + "addr"
					else:
						attribute_string = "axi_" + k + h
						end_of_attribute_string = k + h

					if (len(self.hbm_pc_assignments[channel_number]) != 0):
						# It's not our first time inserting in this pseudo-channel, we need to add a new signal
						axi_placeholder = inst(Logic, bits=data_width, name="axi_"+str(channel_number//2)+ "_" + str(channel_number % 2) + "_" + end_of_attribute_string +"_" + str(len(self.hbm_pc_assignments[channel_number])))
						self.hbm_connections[channel_number][k][h].append(axi_placeholder)

					# We connect the signals driven by hbmc
					if hasattr(hbmc, attribute_string):
						axi_attribute = getattr(hbmc, attribute_string)
						set_driver_function = getattr(axi_attribute, "set_driver")


						if (k != "r" and k!= "b") and (h != "ready"):
							self.hbm_connections[channel_number][k][h][-1].set_driver(getattr(hbmc, attribute_string))

						elif (k == "r") and (h == "ready"):
							self.hbm_connections[channel_number][k][h][-1].set_driver(getattr(hbmc, attribute_string))

						elif (k != "r" and k!= "b") and (h == "ready"):
							set_driver_function(self.hbm_connections[channel_number][k][h][-1])

						elif (k == "r") and ("h" != "ready"):
							set_driver_function(self.hbm_connections[channel_number][k][h][-1])




			# Append the node number to finalize assignment to Pseudo-Channel
			self.hbm_pc_assignments[channel_number].append(hbmc_number)	

			#Add the addresses assigned to the Controller to the Phsyical Pseudo-Channel
			self.address_assignments[channel_number] = hbmc.address_pairs

			#Assign correct clock and reset to HBM Controller
			hbmc.hbm_interface_clk.set_driver(self.hbm_clocks[channel_number//2])
			hbmc.hbm_interface_reset.set_driver(inst(NOT, self.hbm_resets[channel_number//2]))

			#Assign the writing_is_done signal
			hbmc.hbm_writing_is_done.set_driver(self.writing_is_done[channel_number])	

	def create_write_path(self, max_addr, channel_number):
		# This function creates the infrastructure surrounding the write DCFIFO for a selected channel. 
		inst = self.inst
		with ModuleScope(self):
			used_address = self.address_assignments[channel_number]

			#The address counter counts the total number of address ever sent via the write path
			wr_addr_counter = inst(Counter,
					reset_value=0,
					increment_value=1,
					end_value=max_addr,
					name="wr_addr_counter")	

			#The write path counter helps with deserializing the data before writing it to HBM
			wr_path_counter = inst(Counter, reset_value=0, increment_value=1, end_value=self.WRITE_PATH_NARROW_CONSTANT-1, name="wr_path_counter")
			wr_path_reconstructed = [inst(Flop, bits=math.ceil(240/self.WRITE_PATH_NARROW_CONSTANT), name="wr_path_part"+str(i)) for i in range(self.WRITE_PATH_NARROW_CONSTANT)]
			wr_path_right_time = [inst(EQ, wr_path_counter.current_value, inst(Constant, bits=wr_path_counter.current_value._bit_width, value=i, name="wr_path_const"+str(i))) for i in range(self.WRITE_PATH_NARROW_CONSTANT)]
			for i in range(self.WRITE_PATH_NARROW_CONSTANT):
				wr_path_reconstructed[i].set_driver(inst(MuxV2, wr_path_right_time[i], wr_path_reconstructed[i], self.write_path_data[channel_number]))
			wr_path_counter.set_increment_condition(inst(AND, self.write_path_valid[channel_number], inst(NOT, wr_addr_counter.is_done)))
			wr_addr_counter.set_increment_condition(inst(AND, self.write_path_valid[channel_number], wr_path_counter.is_done, inst(NOT, wr_addr_counter.is_done)))

			#We need to check if the addresses are within bounds before writing
			# All channels get the data, but only the data within some addresses are meant for this channel
			within_address_space_list = []
			total_address_diff = 0
			end_addresses_where_id_changes = []
			for i,(start_addr, end_addr) in enumerate(used_address):	
				total_address_diff += (end_addr - start_addr)		
				end_addresses_where_id_changes.append(total_address_diff-1)		
				wr_addr_counter_ge_lower_bound = inst(GE, wr_addr_counter.current_value, inst(Constant, bits=wr_addr_counter._bit_width, value=start_addr, name="lower_bound"+str(i)))	
				wr_addr_counter_lt_upper_bound = inst(LT, wr_addr_counter.current_value, inst(Constant, bits=wr_addr_counter._bit_width, value=end_addr, name="upper_bound"+str(i)))
				within_address_space_partial = inst(AND, wr_addr_counter_ge_lower_bound, wr_addr_counter_lt_upper_bound, name="within_address_space_part_"+str(i))
				within_address_space_list.append(within_address_space_partial.delay(1))	
			within_address_space_delayed = inst(OR, *within_address_space_list, name="within_address_space_delayed")
			#We create a fifo to write to HBM
			wr_hbm_fifo = inst(DCFIFO, rdclk=self.hbm_clocks_clk[channel_number//2], wrclk=self.ext_logic_clk, bits=240, depth=32, almost_full = 5, showahead = "ON")

			#Logic to write to the fifo (that will in turn write to HBM)
			delayed_w_en = inst(AND, self.write_path_valid[channel_number].delay(1), (wr_path_counter.is_done).delay(1), within_address_space_delayed)											
			wr_hbm_fifo.w_en.set_driver(inst(AND, delayed_w_en, inst(NOT, wr_hbm_fifo.full)))
			wr_hbm_fifo.w_data.set_driver(inst(Concat, *wr_path_reconstructed))

			#We need some reset signal
			hbm_reset_inverted = inst(Logic, bits=1, signed=False, name="hbm_reset_inverted")
			hbm_reset_inverted.set_driver(inst(NOT, self.hbm_resets[channel_number//2]))
			hpipe_reset = inst(NOT, self.rst_n, name="hpipe_reset_hbm")
			with ParamScope(clock=self.hbm_clocks_clk[channel_number//2], reset=hbm_reset_inverted):
				#Filling up the HBM FIFO
				hbm_wr_addr_counter = inst(Counter, reset_value=0, increment_value=1, end_value=total_address_diff, name="hbm_wr_addr_counter")
				write_valid = inst(AND, inst(NOT, hbm_wr_addr_counter.is_done), self.hbm_connections[channel_number]["aw"]["ready"][-1], self.hbm_connections[channel_number]["w"]["ready"][-1], inst(NOT, wr_hbm_fifo.empty))
				hbm_wr_addr_counter.set_increment_condition(write_valid)
				
			
				#We add a counter that counts the IDs, the ID is appended to the 240-bit data (uses the upper 16 bits of the 256 bits)
				# Traditionally, three convolution engines are fed by a single pseudo-channel, so 3 IDs are in use. More or less conv. engines are also supported.
				id_counter = inst(Counter, reset_value=0, increment_value=1, end_value=len(used_address)-1, name="id_counter")
				id_counter_constants = [inst(Constant, bits=hbm_wr_addr_counter.current_value._bit_width, value=id_change) for id_change in end_addresses_where_id_changes]
				if len(id_counter_constants) == 1:
					id_counter_mux_constant = id_counter_constants[0]
				else:
					id_counter_mux_constant = inst(MuxV2, id_counter.current_value, *id_counter_constants)
				id_counter.set_increment_condition(inst(AND, write_valid, inst(EQ, hbm_wr_addr_counter.current_value, id_counter_mux_constant)))

				# We extend the signals for awaddr and wdata
				MAX_ADDR_WIDTH = 23
				MAX_DATA_WIDTH = 256
				if (hbm_wr_addr_counter.current_value._bit_width == MAX_ADDR_WIDTH):
					hbm_wr_addr_counter_extended = inst(Concat, inst(Constant, bits=5 , value=0, name="awaddr_fivezero_padding"), hbm_wr_addr_counter.current_value)
				else:
					if (hbm_wr_addr_counter.current_value._bit_width > MAX_ADDR_WIDTH):
						print("Address width is larger than 23, which is currently not supported. The 23 are concatenated with 5 zeros as per the HBM documentation.")
						exit(-1)
					else:
						hbm_wr_addr_counter_extended = inst(Concat, inst(Constant, bits=5, value=0), hbm_wr_addr_counter.current_value, inst(Constant, bits=MAX_ADDR_WIDTH-hbm_wr_addr_counter.current_value._bit_width, value=0, name="awaddr_zero_padding"))

				if (wr_hbm_fifo.r_data._bit_width == MAX_DATA_WIDTH):
					extended_wdata = wr_hbm_fifo.r_data
				else:
					if (wr_hbm_fifo.r_data._bit_width > MAX_DATA_WIDTH):
						print("Data width can only go up to 256 currently.")
						exit(-1)
					else:
						extended_wdata = inst(Concat, wr_hbm_fifo.r_data, id_counter.current_value, inst(Constant, bits=MAX_DATA_WIDTH-wr_hbm_fifo.r_data._bit_width - id_counter.current_value._bit_width, value=0, name="wdata_zero_padding"))

				self.hbm_connections[channel_number]["aw"]["data"][-1].set_driver(hbm_wr_addr_counter_extended)
				self.hbm_connections[channel_number]["aw"]["valid"][-1].set_driver(write_valid)
				self.hbm_connections[channel_number]["w"]["data"][-1].set_driver(extended_wdata)
				self.hbm_connections[channel_number]["w"]["valid"][-1].set_driver(write_valid)
				bready_const = inst(Constant, bits=1, value=1, name="bready_const")
				self.hbm_connections[channel_number]["b"]["ready"][-1].set_driver(bready_const)
				wr_hbm_fifo.r_en.set_driver(write_valid)


				# sync_chain_writing_is_done = inst(SynchronizationChain, self.hbm_clocks_clk[channel_number//2], self.ext_logic_clk, stages=3)
				# sync_chain_writing_is_done.control_in.set_driver(hbm_wr_addr_counter.is_done)
				# with ParamScope(clock=self.ext_logic_clk, reset=hpipe_reset):
				# 	self.writing_is_done[channel_number].set_driver(sync_chain_writing_is_done.control_out)
				self.writing_is_done[channel_number].set_driver(hbm_wr_addr_counter.is_done)

				#writing_is_done_domain_crossing = inst(SingleCycleControlDomainCrossing, self.hbm_clocks_clk[channel_number//2], self.ext_logic_clk, clock_a_reset=hbm_reset_inverted, clock_b_reset=hpipe_reset)
				#writing_is_done_domain_crossing.control_in.set_driver(hbm_wr_addr_counter.is_done.delay(1))
				#self.writing_is_done[channel_number].set_driver(hbm_wr_addr_counter.is_done.delay(2))


	def wire_write_path(self, channel_number, data, valid):
		# Connect the write path of a channel to the arguments of this function (usually the output of a shift-register)
		with ModuleScope(self):
			self.write_path_data[channel_number].set_driver(data)
			self.write_path_valid[channel_number].set_driver(valid)

	def format_weights_for_hardware(self):
		# Format weights to the size of input images and in binary format (we are re-using the image input buffer for weights).
		depth_serialized = 4704 #This is 224x224x3x8 / 256 (256 is interface width, 224x224x3 is input image dimension, and weights are 8-bit)
		required_depth = 0
		for oba in self.hbm_weights:
			required_depth += len(oba)
		total_extra_images = math.ceil(required_depth/depth_serialized)

		bitarrays_to_be_binarized = []
		for j in range(total_extra_images):
			zero_vector = [0 for i in range(depth_serialized)]
			empty_bitarray = BitArray(bits=256, value_vector=zero_vector)
			bitarrays_to_be_binarized.append(empty_bitarray)

		#Finally we fill the bitarrays with the correct data
		depth_index = 0
		rom_index = 0
		for oba in self.hbm_weights:
			for line in oba.bit_array:
				bitarrays_to_be_binarized[rom_index].bit_array[depth_index] = line
				if (depth_index == depth_serialized-1):
					depth_index = 0
					rom_index += 1
				else:
					depth_index += 1
		
		binary_images = []
		for ba in bitarrays_to_be_binarized:
			ba_binary = ba.get_byte_array()
			binary_images.append(ba_binary)
		return binary_images


	def get_verilog(self, t="  "):

		verilog = ""
		verilog += self.get_module_header(t)
		if self.__class__.__doc__:
			verilog += v_comment_string(self.__class__.__doc__, t=t)

		verilog += self.get_parameter_declaration_string() + "\n"

		inputs = self.get_inputs()
		outputs = self.get_outputs()

		if self.disable_verilator_UNOPTFLAT == True:
			verilog += "\n//verilator lint_off UNOPTFLAT\n"

		internal = self.get_internal_signals() + self.clocks
		driven_by_output = [s for s in internal if s.driven_by_output]

		signals = inputs + outputs + internal
		for s in signals:
			verilog += t + s.get_declaration_string() + "\n"

		verilog += "\n"

		verilog += self.get_custom_verilog_str(t=t)

		verilog += self.get_combinational_assignments(t=t)

		reset_flops = self.get_reset_flops()
		nr_flops = self.get_no_reset_flops()

		for clock_name, reset_flop_dict in reset_flops.items():
			verilog += t + "always @(posedge " + clock_name + ") begin\n"
			for reset_name, flop_list in reset_flop_dict.items():
				n = 2
				verilog += n * t + "if (" + reset_name + ") begin\n"
				n = 3
				for f in flop_list:
					verilog += n * t + f.name + " <= " + f.get_reset_driver().name + ";\n"
				n = 2
				verilog += n * t + "end else begin\n"
				n = 3
				for f in flop_list:
					if f and f.get_driver():
						verilog += n * t + f.name + " <= " + f.get_driver().name + ";\n"
				n = 2
				verilog += n * t + "end\n"
			verilog += t + "end\n\n"

		for clock_name, flop_list in nr_flops.items():
			verilog += t + "always @(posedge " + clock_name + ") begin\n"
			n = 2
			for f in flop_list:
				verilog += n * t + f.name + " <= " + f.get_driver().name + ";\n"
			verilog += t + "end\n\n"

		for m in self.modules:
			verilog += m.get_instance_verilog(t=t) + "\n\n"

		if self.disable_verilator_UNOPTFLAT == True:
			verilog += "\n//verilator lint_on UNOPTFLAT\n"

		verilog += self._megafunction_string(t)

		verilog += "endmodule // " + self.name

		return verilog

	def _megafunction_string(self, t="  "):
		CHANNELS = 8 #Channels in one HBM
		instance_name = self.name + "_i"
		verilog = ""

		if self.simulate_hbm:
			# When HBM is simulated, we instantiate ed_sim_mem which are Quartus IPs that simualate HBM

			for i in range(2 * CHANNELS):
				verilog += f"""
			wire         ck_t_{i};                      //               mem_0.ck_t
			wire         ck_c_{i};                     //                    .ck_c
			wire         cke_{i};                       //                    .cke
			wire [7:0]   c_{i};                         //                    .c
			wire [5:0]   r_{i};                         //                    .r
			wire [127:0] dq_{i};                        //                    .dq
			wire [15:0]  dm_{i};                        //                    .dm
			wire [15:0]  dbi_{i};                       //                    .dbi
			wire [3:0]   par_{i};                       //                    .par
			wire [3:0]   derr_{i};                      //                    .derr
			wire [3:0]   rdqs_t_{i};                    //                    .rdqs_t
			wire [3:0]   rdqs_c_{i};                    //                    .rdqs_c
			wire [3:0]   wdqs_t_{i};                    //                    .wdqs_t
			wire [3:0]   wdqs_c_{i};                    //                    .wdqs_c
			wire [7:0]   rd_{i};                        //                    .rd
			wire         rr_{i};                        //                    .rr
			wire         rc_{i};                        //                    .rc
			wire         aerr_{i};                      //                    .aerr
			"""

			verilog += f"""
			ed_sim_mem mem_0 (
			"""
			for i in range(CHANNELS):
				verilog += f"""
				.ck_t_{i}(ck_t_{i}),
				.ck_c_{i}(ck_c_{i}),
				.cke_{i}(cke_{i}),
				.c_{i}(c_{i}),
				.r_{i}(r_{i}),
				.dq_{i}(dq_{i}),
				.dm_{i}(dm_{i}),
				.dbi_{i}(dbi_{i}),
				.par_{i}(par_{i}),
				.derr_{i}(derr_{i}),
				.rdqs_t_{i}(rdqs_t_{i}),
				.rdqs_c_{i}(rdqs_c_{i}),
				.wdqs_t_{i}(wdqs_t_{i}),
				.wdqs_c_{i}(wdqs_c_{i}),
				.rd_{i}(rd_{i}),
				.rr_{i}(rr_{i}),
				.rc_{i}(rc_{i}),
				.aerr_{i}(aerr_{i}),
				"""
			verilog += f"""
				.cattrip({self.bridge_cattrip.name}),
				.temp({self.bridge_temp.name}),
				.wso({self.bridge_wso.name}),
				.reset_n({self.bridge_reset_n.name}),
				.wrst_n({self.bridge_wrst_n.name}),
				.wrck({self.bridge_wrck.name}),
				.shiftwr({self.bridge_shiftwr.name}),
				.capturewr({self.bridge_capturewr.name}),
				.updatewr({self.bridge_updatewr.name}),
				.selectwir({self.bridge_selectwir.name}),
				.wsi({self.bridge_wsi.name})
			);
				"""
			verilog += f"""
			ed_sim_mem mem_1 (
			"""
			for i in range(CHANNELS):
				verilog += f"""
				.ck_t_{i}(ck_t_{(8+i)}),
				.ck_c_{i}(ck_c_{(8+i)}),
				.cke_{i}(cke_{(8+i)}),
				.c_{i}(c_{(8+i)}),
				.r_{i}(r_{(8+i)}),
				.dq_{i}(dq_{(8+i)}),
				.dm_{i}(dm_{(8+i)}),
				.dbi_{i}(dbi_{(8+i)}),
				.par_{i}(par_{(8+i)}),
				.derr_{i}(derr_{(8+i)}),
				.rdqs_t_{i}(rdqs_t_{(8+i)}),
				.rdqs_c_{i}(rdqs_c_{(8+i)}),
				.wdqs_t_{i}(wdqs_t_{(8+i)}),
				.wdqs_c_{i}(wdqs_c_{(8+i)}),
				.rd_{i}(rd_{(8+i)}),
				.rr_{i}(rr_{(8+i)}),
				.rc_{i}(rc_{(8+i)}),
				.aerr_{i}(aerr_{(8+i)}),
				"""
			verilog += f"""
				.cattrip({self.bridge_cattrip_bot.name}),
				.temp({self.bridge_temp_bot.name}),
				.wso({self.bridge_wso_bot.name}),
				.reset_n({self.bridge_reset_n_bot.name}),
				.wrst_n({self.bridge_wrst_n_bot.name}),
				.wrck({self.bridge_wrck_bot.name}),
				.shiftwr({self.bridge_shiftwr_bot.name}),
				.capturewr({self.bridge_capturewr_bot.name}),
				.updatewr({self.bridge_updatewr_bot.name}),
				.selectwir({self.bridge_selectwir_bot.name}),
				.wsi({self.bridge_wsi_bot.name})
			);
				"""

		verilog += f"""
			hbm_top {instance_name}
			(
				.pll_ref_clk({self.pll_ref_clk.name}),
				.ext_core_clk({self.ext_logic_clk.name}),
				.ext_core_clk_locked({self.ext_logic_clk_locked.name}),
				.wmcrst_n_in({self.rst_n.name}),
			"""
		if self.simulate_hbm:
			verilog += f"""
				.hbm_only_reset_in(1'b0),
			"""
		else:
			verilog += f"""
				.hbm_only_reset_in(1'b0),
			"""
		verilog += f"""
				.local_cal_success({self.cal_success.name}),
				.local_cal_fail(),
				.cal_lat(),
			"""
		
		if self.simulate_hbm:
			# Connect the UIB signals to the simulation IPs
			for i in range(CHANNELS):
				verilog += f"""	
				.ck_t_{i}(ck_t_{i}),
				.ck_c_{i}(ck_c_{i}),
				.cke_{i}(cke_{i}),
				.c_{i}(c_{i}),
				.r_{i}(r_{i}),
				.dq_{i}(dq_{i}),
				.dm_{i}(dm_{i}),
				.dbi_{i}(dbi_{i}),
				.par_{i}(par_{i}),
				.derr_{i}(derr_{i}),
				.rdqs_t_{i}(rdqs_t_{i}),
				.rdqs_c_{i}(rdqs_c_{i}),
				.wdqs_t_{i}(wdqs_t_{i}),
				.wdqs_c_{i}(wdqs_c_{i}),
				.rd_{i}(rd_{i}),
				.rr_{i}(rr_{i}),
				.rc_{i}(rc_{i}),
				.aerr_{i}(aerr_{i}),
				"""
		else:
			for i in range(CHANNELS):
				verilog += f"""		
				.ck_t_{i}(),
				.ck_c_{i}(),
				.cke_{i}(),
				.c_{i}(),
				.r_{i}(),
				.dq_{i}(),
				.dm_{i}(),
				.dbi_{i}(),
				.par_{i}(),
				.derr_{i}(),
				.rdqs_t_{i}(),
				.rdqs_c_{i}(),
				.wdqs_t_{i}(),
				.wdqs_c_{i}(),
				.rd_{i}(),
				.rr_{i}(),
				.rc_{i}(),
				.aerr_{i}(),
			"""
		for i in range(CHANNELS):
			verilog += f"""
				.wmc_clk_{i}_clk({self.hbm_clocks[i].name}),
				.phy_clk_{i}_clk(),
				.wmcrst_n_{i}_reset_n({self.hbm_resets[i].name}),
			"""

		for i in range(CHANNELS):
			for j in range(2):
				# i is channel index and j is pseudo-channel index (0 or 1)
				hbm_idx=2*i + j
				if len(self.hbm_pc_assignments[hbm_idx]) == 0:
					verilog += f"""
				.axi_{i}_{j}_awid(9'd0),
				.axi_{i}_{j}_awaddr(28'd0),
				.axi_{i}_{j}_awlen(8'd0),
				.axi_{i}_{j}_awsize(3'b101),
				.axi_{i}_{j}_awburst(2'd1),
				.axi_{i}_{j}_awprot(3'd0),
				.axi_{i}_{j}_awqos(4'd0),
				.axi_{i}_{j}_awuser(1'b0),
				.axi_{i}_{j}_awvalid(1'b0),
				.axi_{i}_{j}_awready({self.hbm_connections[hbm_idx]["aw"]["ready"][-1].name}),
				//
				.axi_{i}_{j}_wdata(256'd0),
				.axi_{i}_{j}_wstrb(32'hffff_ffff),
				.axi_{i}_{j}_wlast(1'b0),
				.axi_{i}_{j}_wvalid(1'b0),
				.axi_{i}_{j}_wready({self.hbm_connections[hbm_idx]["w"]["ready"][-1].name}),
				//
				.axi_{i}_{j}_bid(),
				.axi_{i}_{j}_bresp({self.hbm_connections[hbm_idx]["b"]["data"][-1].name}),
				.axi_{i}_{j}_bvalid({self.hbm_connections[hbm_idx]["b"]["valid"][-1].name}),
				.axi_{i}_{j}_bready(1'b1),
				//
				.axi_{i}_{j}_arid(9'd0),
				.axi_{i}_{j}_araddr(28'd0),
				.axi_{i}_{j}_arlen(8'd0),
				.axi_{i}_{j}_arsize(3'b101),
				.axi_{i}_{j}_arburst(2'd1),
				.axi_{i}_{j}_arprot(3'd0),
				.axi_{i}_{j}_arqos(4'd0),
				.axi_{i}_{j}_aruser(1'b0),
				.axi_{i}_{j}_arvalid(1'b0),
				.axi_{i}_{j}_arready({self.hbm_connections[hbm_idx]["ar"]["ready"][-1].name}),
				//	
				.axi_{i}_{j}_rid(),
				.axi_{i}_{j}_rdata({self.hbm_connections[hbm_idx]["r"]["data"][-1].name}),
				.axi_{i}_{j}_rresp(),
				.axi_{i}_{j}_rlast(),
				.axi_{i}_{j}_rvalid({self.hbm_connections[hbm_idx]["r"]["valid"][-1].name}),
				.axi_{i}_{j}_rready(1'b1),	 
				"""	
		
				else:
					verilog += f"""
				.axi_{i}_{j}_awid(9'd0),
				.axi_{i}_{j}_awaddr({self.hbm_connections[hbm_idx]["aw"]["data"][-1].name}),
				.axi_{i}_{j}_awlen(8'd0),
				.axi_{i}_{j}_awsize(3'b101),
				.axi_{i}_{j}_awburst(2'd1),
				.axi_{i}_{j}_awprot(3'd0),
				.axi_{i}_{j}_awqos(4'd0),
				.axi_{i}_{j}_awuser(1'b0),
				.axi_{i}_{j}_awvalid({self.hbm_connections[hbm_idx]["aw"]["valid"][-1].name}),
				.axi_{i}_{j}_awready({self.hbm_connections[hbm_idx]["aw"]["ready"][-1].name}),
				//
				.axi_{i}_{j}_wdata({self.hbm_connections[hbm_idx]["w"]["data"][-1].name}),
				.axi_{i}_{j}_wstrb(32'hffff_ffff),
				.axi_{i}_{j}_wlast({self.hbm_connections[hbm_idx]["w"]["valid"][-1].name}),
				.axi_{i}_{j}_wvalid({self.hbm_connections[hbm_idx]["w"]["valid"][-1].name}),
				.axi_{i}_{j}_wready({self.hbm_connections[hbm_idx]["w"]["ready"][-1].name}),
				//
				.axi_{i}_{j}_bid(),
				.axi_{i}_{j}_bresp({self.hbm_connections[hbm_idx]["b"]["data"][-1].name}),
				.axi_{i}_{j}_bvalid({self.hbm_connections[hbm_idx]["b"]["valid"][-1].name}),
				.axi_{i}_{j}_bready({self.hbm_connections[hbm_idx]["b"]["ready"][-1].name}),
				//
				.axi_{i}_{j}_arid(9'd0),
				.axi_{i}_{j}_araddr({self.hbm_connections[hbm_idx]["ar"]["data"][-1].name}),
				.axi_{i}_{j}_arlen(8'd0),
				.axi_{i}_{j}_arsize(3'b101),
				.axi_{i}_{j}_arburst(2'd1),
				.axi_{i}_{j}_arprot(3'd0),
				.axi_{i}_{j}_arqos(4'd0),
				.axi_{i}_{j}_aruser(1'b0),
				.axi_{i}_{j}_arvalid({self.hbm_connections[hbm_idx]["ar"]["valid"][-1].name}),
				.axi_{i}_{j}_arready({self.hbm_connections[hbm_idx]["ar"]["ready"][-1].name}),
				//	
				.axi_{i}_{j}_rid(),
				.axi_{i}_{j}_rdata({self.hbm_connections[hbm_idx]["r"]["data"][-1].name}),
				.axi_{i}_{j}_rresp(),
				.axi_{i}_{j}_rlast(),
				.axi_{i}_{j}_rvalid({self.hbm_connections[hbm_idx]["r"]["valid"][-1].name}),
				.axi_{i}_{j}_rready({self.hbm_connections[hbm_idx]["r"]["ready"][-1].name}),	 
				"""	
		
		for i in range(CHANNELS):
			verilog += f"""		
				.apb_{i}_ur_paddr(16'd0),
				.apb_{i}_ur_psel(1'b0),
				.apb_{i}_ur_penable(1'b0),
				.apb_{i}_ur_pwrite(1'b0),
				.apb_{i}_ur_pwdata(16'd0),
				.apb_{i}_ur_pstrb(2'd0),
				.apb_{i}_ur_prready(),
				.apb_{i}_ur_prdata(),
			"""
		
		# These signals are propagated to the top level
		verilog += f"""  
				.cattrip({self.bridge_cattrip.name}),
				.temp({self.bridge_temp.name}),
				.wso({self.bridge_wso.name}),
				.reset_n({self.bridge_reset_n.name}),
				.wrst_n({self.bridge_wrst_n.name}),
				.wrck({self.bridge_wrck.name}),
				.shiftwr({self.bridge_shiftwr.name}),
				.capturewr({self.bridge_capturewr.name}),
				.updatewr({self.bridge_updatewr.name}),
				.selectwir({self.bridge_selectwir.name}),
				.wsi({self.bridge_wsi.name})			
			);
				"""
		
		#Insert the bottom HBM
		instance_name_bot = instance_name + "_bot"
		module_name_hbm_bot = "hbm_bot"
		if self.simulate_hbm:
			module_name_hbm_bot = "hbm_top"
		verilog += f"""
			{module_name_hbm_bot} {instance_name_bot}
			(
				.pll_ref_clk({self.pll_ref_clk_bot.name}),
				.ext_core_clk({self.ext_logic_clk.name}),
				.ext_core_clk_locked({self.ext_logic_clk_locked.name}),
				.wmcrst_n_in({self.rst_n.name}),
			"""
		if self.simulate_hbm:
			verilog += f"""
				.hbm_only_reset_in(1'b0),
			"""
		else:
			verilog += f"""
				.hbm_only_reset_in(1'b0),
			"""
		verilog += f"""
				.local_cal_success({self.cal_success_bot.name}),
				.local_cal_fail(),
				.cal_lat(),
			"""
		if self.simulate_hbm:
			for i in range(CHANNELS):
				idx_converted = 8 + i
				verilog += f"""	
				.ck_t_{i}(ck_t_{idx_converted}),
				.ck_c_{i}(ck_c_{idx_converted}),
				.cke_{i}(cke_{idx_converted}),
				.c_{i}(c_{idx_converted}),
				.r_{i}(r_{idx_converted}),
				.dq_{i}(dq_{idx_converted}),
				.dm_{i}(dm_{idx_converted}),
				.dbi_{i}(dbi_{idx_converted}),
				.par_{i}(par_{idx_converted}),
				.derr_{i}(derr_{idx_converted}),
				.rdqs_t_{i}(rdqs_t_{idx_converted}),
				.rdqs_c_{i}(rdqs_c_{idx_converted}),
				.wdqs_t_{i}(wdqs_t_{idx_converted}),
				.wdqs_c_{i}(wdqs_c_{idx_converted}),
				.rd_{i}(rd_{idx_converted}),
				.rr_{i}(rr_{idx_converted}),
				.rc_{i}(rc_{idx_converted}),
				.aerr_{i}(aerr_{idx_converted}),
				"""
		else:
			for i in range(CHANNELS):
				verilog += f"""		
				.ck_t_{i}(),
				.ck_c_{i}(),
				.cke_{i}(),
				.c_{i}(),
				.r_{i}(),
				.dq_{i}(),
				.dm_{i}(),
				.dbi_{i}(),
				.par_{i}(),
				.derr_{i}(),
				.rdqs_t_{i}(),
				.rdqs_c_{i}(),
				.wdqs_t_{i}(),
				.wdqs_c_{i}(),
				.rd_{i}(),
				.rr_{i}(),
				.rc_{i}(),
				.aerr_{i}(),
			"""
		for i in range(CHANNELS):
			idx_converted = 8 + i
			verilog += f"""
				.wmc_clk_{i}_clk({self.hbm_clocks[idx_converted].name}),
				.phy_clk_{i}_clk(),
				.wmcrst_n_{i}_reset_n({self.hbm_resets[idx_converted].name}),
			"""

		for i in range(CHANNELS):
			for j in range(2):
				idx_converted = 8+i
				hbm_idx=2*idx_converted + j
				if len(self.hbm_pc_assignments[hbm_idx]) == 0:
					verilog += f"""
				.axi_{i}_{j}_awid(9'd0),
				.axi_{i}_{j}_awaddr(28'd0),
				.axi_{i}_{j}_awlen(8'd0),
				.axi_{i}_{j}_awsize(3'b101),
				.axi_{i}_{j}_awburst(2'd1),
				.axi_{i}_{j}_awprot(3'd0),
				.axi_{i}_{j}_awqos(4'd0),
				.axi_{i}_{j}_awuser(1'b0),
				.axi_{i}_{j}_awvalid(1'b0),
				.axi_{i}_{j}_awready({self.hbm_connections[hbm_idx]["aw"]["ready"][-1].name}),
				//
				.axi_{i}_{j}_wdata(256'd0),
				.axi_{i}_{j}_wstrb(32'hffff_ffff),
				.axi_{i}_{j}_wlast(1'b0),
				.axi_{i}_{j}_wvalid(1'b0),
				.axi_{i}_{j}_wready({self.hbm_connections[hbm_idx]["w"]["ready"][-1].name}),
				//
				.axi_{i}_{j}_bid(),
				.axi_{i}_{j}_bresp({self.hbm_connections[hbm_idx]["b"]["data"][-1].name}),
				.axi_{i}_{j}_bvalid({self.hbm_connections[hbm_idx]["b"]["valid"][-1].name}),
				.axi_{i}_{j}_bready(1'b1),
				//
				.axi_{i}_{j}_arid(9'd0),
				.axi_{i}_{j}_araddr(28'd0),
				.axi_{i}_{j}_arlen(8'd0),
				.axi_{i}_{j}_arsize(3'b101),
				.axi_{i}_{j}_arburst(2'd1),
				.axi_{i}_{j}_arprot(3'd0),
				.axi_{i}_{j}_arqos(4'd0),
				.axi_{i}_{j}_aruser(1'b0),
				.axi_{i}_{j}_arvalid(1'b0),
				.axi_{i}_{j}_arready({self.hbm_connections[hbm_idx]["ar"]["ready"][-1].name}),
				//	
				.axi_{i}_{j}_rid(),
				.axi_{i}_{j}_rdata({self.hbm_connections[hbm_idx]["r"]["data"][-1].name}),
				.axi_{i}_{j}_rresp(),
				.axi_{i}_{j}_rlast(),
				.axi_{i}_{j}_rvalid({self.hbm_connections[hbm_idx]["r"]["valid"][-1].name}),
				.axi_{i}_{j}_rready(1'b1),	 
				"""	
		
				else:
					verilog += f"""
				.axi_{i}_{j}_awid(9'd0),
				.axi_{i}_{j}_awaddr({self.hbm_connections[hbm_idx]["aw"]["data"][-1].name}),
				.axi_{i}_{j}_awlen(8'd0),
				.axi_{i}_{j}_awsize(3'b101),
				.axi_{i}_{j}_awburst(2'd1),
				.axi_{i}_{j}_awprot(3'd0),
				.axi_{i}_{j}_awqos(4'd0),
				.axi_{i}_{j}_awuser(1'b0),
				.axi_{i}_{j}_awvalid({self.hbm_connections[hbm_idx]["aw"]["valid"][-1].name}),
				.axi_{i}_{j}_awready({self.hbm_connections[hbm_idx]["aw"]["ready"][-1].name}),
				//
				.axi_{i}_{j}_wdata({self.hbm_connections[hbm_idx]["w"]["data"][-1].name}),
				.axi_{i}_{j}_wstrb(32'hffff_ffff),
				.axi_{i}_{j}_wlast({self.hbm_connections[hbm_idx]["w"]["valid"][-1].name}),
				.axi_{i}_{j}_wvalid({self.hbm_connections[hbm_idx]["w"]["valid"][-1].name}),
				.axi_{i}_{j}_wready({self.hbm_connections[hbm_idx]["w"]["ready"][-1].name}),
				//
				.axi_{i}_{j}_bid(),
				.axi_{i}_{j}_bresp({self.hbm_connections[hbm_idx]["b"]["data"][-1].name}),
				.axi_{i}_{j}_bvalid({self.hbm_connections[hbm_idx]["b"]["valid"][-1].name}),
				.axi_{i}_{j}_bready({self.hbm_connections[hbm_idx]["b"]["ready"][-1].name}),
				//
				.axi_{i}_{j}_arid(9'd0),
				.axi_{i}_{j}_araddr({self.hbm_connections[hbm_idx]["ar"]["data"][-1].name}),
				.axi_{i}_{j}_arlen(8'd0),
				.axi_{i}_{j}_arsize(3'b101),
				.axi_{i}_{j}_arburst(2'd1),
				.axi_{i}_{j}_arprot(3'd0),
				.axi_{i}_{j}_arqos(4'd0),
				.axi_{i}_{j}_aruser(1'b0),
				.axi_{i}_{j}_arvalid({self.hbm_connections[hbm_idx]["ar"]["valid"][-1].name}),
				.axi_{i}_{j}_arready({self.hbm_connections[hbm_idx]["ar"]["ready"][-1].name}),
				//	
				.axi_{i}_{j}_rid(),
				.axi_{i}_{j}_rdata({self.hbm_connections[hbm_idx]["r"]["data"][-1].name}),
				.axi_{i}_{j}_rresp(),
				.axi_{i}_{j}_rlast(),
				.axi_{i}_{j}_rvalid({self.hbm_connections[hbm_idx]["r"]["valid"][-1].name}),
				.axi_{i}_{j}_rready({self.hbm_connections[hbm_idx]["r"]["ready"][-1].name}),	 
				"""	
		
		for i in range(CHANNELS):
			verilog += f"""		
				.apb_{i}_ur_paddr(16'd0),
				.apb_{i}_ur_psel(1'b0),
				.apb_{i}_ur_penable(1'b0),
				.apb_{i}_ur_pwrite(1'b0),
				.apb_{i}_ur_pwdata(16'd0),
				.apb_{i}_ur_pstrb(2'd0),
				.apb_{i}_ur_prready(),
				.apb_{i}_ur_prdata(),
			"""
		
		verilog += f"""  
				.cattrip({self.bridge_cattrip_bot.name}),
				.temp({self.bridge_temp_bot.name}),
				.wso({self.bridge_wso_bot.name}),
				.reset_n({self.bridge_reset_n_bot.name}),
				.wrst_n({self.bridge_wrst_n_bot.name}),
				.wrck({self.bridge_wrck_bot.name}),
				.shiftwr({self.bridge_shiftwr_bot.name}),
				.capturewr({self.bridge_capturewr_bot.name}),
				.updatewr({self.bridge_updatewr_bot.name}),
				.selectwir({self.bridge_selectwir_bot.name}),
				.wsi({self.bridge_wsi_bot.name})			
			);
				"""

		return verilog	


