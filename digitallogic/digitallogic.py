from copy import copy
from digitallogic.utils import clog2, v_hex_str, get_bit_mask, v_comment_string, print_stage_header, hex_str, BitArray
import digitallogic.digitallogic_hbm
import sys
import os
import warnings
import math
import inspect
import numpy as np
import hashlib
from string import Template
import hpipe.ArchLoader as arch

#For static Verilog files, key if filename (no extension), value is tuple of original module and corresponding simulation module
sim_module_replace = {"nx_dot6_int8": ("fourteennm_dsp_prime", "tensor_block_vector_fixed_point")} 
enable_sim_module_replace = False

final_dsp_count = 0
final_mult_count = 0 #for determining average dsp usage
scalar_tensor_count = 0

#since apparently Python won't let you modify it directly from another file
def modify_final_mult_count(value_to_add):
	global final_mult_count
	final_mult_count += value_to_add

m20k_count = 0 #Only counts extra M20Ks if we run out of MLABs
mlab_count = 0

mlab_limit = 0
mlab_limit_gx = 32000 
mlab_limit_nx = 16350 #This is calculate through trial and error so that 3400 tensor blocks can route at 300 Mhz
mlab_limit_nx_mv2 = 12800 #Mobilenet-v2 has this strange behaviour where it will use around 4,000 more MLABs than estimated by HPIPE (3k tensors) so we set this limit to roughly 4k lower 

#Constants for vector mode (not currently used in code as often)
NX_ICP_VECTOR_MODE = 6
NX_OUT_WIDTH_PARALLELISM_VECTOR_MODE = 1
NX_OUT_WIDTH_PARALLELISM_SCALAR_MODE = 3

#Number of tensor chains to share each weight ROM with
weight_ROM_duplication_factor = 3
#Number of weight ROMs to share each FSM with 
#e.g. if this is 2 and the above is 2, then one FSM will power 2 ROMs, which will each feed 2 chains (so 4 chains total powered by 1 FSM)
weight_FSM_duplication_factor = 2

#Constants for NX tensor mode
NX_ICP_PER_TENSOR_BLOCK = 10
NX_BFLOAT24_ADD_INPUTS = 4
NX_OUT_WIDTH_PARALLELISM_PER_TENSOR_BLOCK = 3
NX_INPUT_DATA_BITS = 8
NX_SHARED_EXP_SIZE = 8
NX_TENSOR_BFLOAT_OUTPUT_PRECISION = 24
NX_TENSOR_OUTPUT_PRECISION = 25 
NX_CASCADE_WEIGHT_SIZE = NX_ICP_PER_TENSOR_BLOCK*NX_INPUT_DATA_BITS+NX_SHARED_EXP_SIZE
NX_CASCADE_DATA_SIZE = NX_ICP_PER_TENSOR_BLOCK*NX_INPUT_DATA_BITS+2*NX_INPUT_DATA_BITS
NX_INPUT_TO_OUTPUT_DELAY_FXP = 3 #Fixed point
NX_INPUT_TO_OUTPUT_DELAY_BFP = 4 #block floating point
ENABLE_RAM_OUTPUT_REG_NX = 1 #enable ram output register for tensor

def initialize_mlab_limit():	
	global mlab_limit
	if arch.hpipe_device_arch.arch_name == "S10_NX": 
		if arch.hpipe_device_arch.network == "mobilenet_v2":
			mlab_limit = mlab_limit_nx_mv2
		else:
			mlab_limit = mlab_limit_nx
	else: mlab_limit = mlab_limit_gx

default_circuit = None
module_stack = []

multi_chip_enable = False 
multi_chip_flow = False
""" When enabled, the signals will have their names extended with 'multi_chip_piece'
	to enable simulation of multiple HPIPE pieces together.  I will modify it to be part of the Kwargs
"""

#read hpipe_piece from a file
"""ff = open("/media/mabdelfattah3929/Data/hpipe/HPIPE_GIT/hpipe/generated_files/circuits/multi_chip/hpipe_piece_num.txt")

hpipe_piece_num = ff.read()
multi_chip_piece = "__"+hpipe_piece_num#None  #can be the chip number and has to be in the form __x (e.g. __1)
multi_chip_piece = "__2"""
class ModuleScope(object):
	def __init__(self, m, overwrite_current_module=False):
		self.m = m
		self.previous = None
		self.overwrite_current_module = overwrite_current_module

	def __enter__(self):
		self.previous = Verilog.current_module
		Verilog.current_module = self
		if "name" in Verilog.current_params.kwargs:
			del Verilog.current_params.kwargs["name"]

	def __exit__(self, type, value, traceback):
		Verilog.current_module = self.previous

class ParamScope(object):
	def __init__(self, **kwargs):
		self.kwargs = kwargs
		if kwargs is None:
			self.kwargs = {}
		self.previous = None

	def __enter__(self):
		self.previous = Verilog.current_params
		self += self.previous
		Verilog.current_params = self

	def __exit__(self, type, value, traceback):
		Verilog.current_params = self.previous

	def __iadd__(self, a):
		for k,v in a.kwargs.items():
			if k not in self.kwargs:
				self.kwargs[k] = v
		to_delete = []
		for k,v in self.kwargs.items():
			if v is None:
				to_delete.append(k)
		for k in to_delete:
			del self.kwargs[k]
		return self

class Verilog(object):
	current_circuit = None
	current_module = None
	current_params = ParamScope()
	@classmethod
	def get_instantiating_module(cls):
		f = inspect.currentframe().f_back
		while not isinstance(f.f_locals['self'], Circuit):
			f = f.f_back
		return f.f_locals['self']

	@classmethod
	def get_instance_params(cls):
		f = inspect.currentframe().f_back
		while not isinstance(f.f_locals['self'], Circuit):
			f = f.f_back
		return (f.f_locals['self'], f.f_locals['instance_params'])

class Circuit(object):
	def __init__(self, propagate_unloaded_signals_to_top=False, **kwargs):
		if Verilog.current_circuit is None:
			Verilog.current_circuit = self
		self.modules = []
		self.combinational = []
		self.clocks = []
		self.flops = []
		self.constants = []
		self.anonymous_name_indices = {}
		self.qsf_lambdas = []
		self.sdc_lambdas = []
		self.auto_delays = []
		self.auto_delay_groups = []
		self.top = None
		self.propagate_unloaded_signals_to_top = propagate_unloaded_signals_to_top
		self.propagate_to_top = {}
		if not hasattr(self, "name"):
			self.name = "CIRCUIT"

	def m20k_estimates(self):
		total_m20ks = 0
		for m in self.modules:
			if not hasattr(m, "tile_estimate"):
				continue
			if m.is_lutram or (m.allow_lutram and m.depth <= 64):
				continue
			total_m20ks += m.tile_estimate

		total_m20ks += m20k_count
		if arch.hpipe_device_arch.network == "mobilenet_v2" and arch.hpipe_device_arch.arch_name == "S10_NX":
			print("Total MLABS: " + str(mlab_count) + " (result may be off by up to 4k for high tensor counts)")
		else:
			print("Total MLABS: " + str(mlab_count))
		return total_m20ks

	def get_memory_estimate(self, node):
		def append_modules(m):
			if not (hasattr(m,'modules')):
				return 0
			else:
				return m.modules

		all_modules = []
		to_explore_modules = self.modules
		while len(to_explore_modules) > 0 :
			current_module = to_explore_modules[0]
			all_modules.append(current_module)
			to_explore_modules = to_explore_modules[1:]
			x = append_modules(current_module)
			if(x == 0):
				continue
			else:
				for mx in x:
					to_explore_modules.append(mx)

		total_m20ks = 0
		for m in all_modules:
			if not hasattr(m, "tile_estimate"):
				continue
			if m.is_lutram or (m.allow_lutram and m.depth <= 64):
				continue
			total_m20ks += m.tile_estimate	
		node.m20k_estimate += total_m20ks

	def add_quartus_setting_lambda(self, qsf_lambda):
		self.qsf_lambdas.append(qsf_lambda)

	def add_sdc_lambda(self, sdc_lambda):
		self.sdc_lambdas.append(sdc_lambda)

	def add_element(self, element):
		if element.name == None:
			element.name = self.get_name(element)
# multi_chip_enable is used to give signals uniques names when there are more than
# one HPIPE instance on the same FPGA
		if multi_chip_enable == True:
			if (multi_chip_piece not in element.name):
				element.name = element.name+multi_chip_piece

		if isinstance(element, Clock):
			self.clocks.append(element)
		elif isinstance(element, Flop):
			self.flops.append(element)
		elif isinstance(element, Module):
			self.modules.append(element)
		elif isinstance(element, Constant):
			self.constants.append(element)
		elif isinstance(element, Logic):
			self.combinational.append(element)
		else:
			raise TypeError("Element must be a subclass of Flop, Module, or Logic")

	def get_inputs(self):
		inputs = []
		for l in self.combinational:
			if l.is_input:
				inputs.append(l)
		return inputs

	def get_outputs(self):
		logic = self.combinational + self.constants + self.flops
		outputs = []
		for l in logic:
			if l.is_output:
				outputs.append(l)
		return outputs

	def get_internal_signals(self):
		logic = self.combinational + self.constants + self.flops
		internal = []
		for l in logic:
			if not l.is_output and not l.is_input:
				internal.append(l)
		return internal

	def get_no_reset_flops(self):
		nr_flops = {}
		for f in self.flops:
			if f.get_reset_driver() is None:
				nr_flops.setdefault(f.get_clock().name, []).append(f)
		return nr_flops

	def get_reset_flops(self):
		reset_flops = {}
		for f in self.flops:
			if f.get_reset_driver() is not None:
				reset_flops.setdefault(f.get_clock().name, {})
				reset_flops[f.get_clock().name].setdefault(f.get_reset().name, []).append(f)
		return reset_flops

	def get_name(self, element):
		name = element.kind + "_" + str(self.anonymous_name_indices.get(element.kind, 0))
		self.anonymous_name_indices.setdefault(element.kind, 0)
		self.anonymous_name_indices[element.kind] += 1
		return name

	def make_names_unique(self, components):
		used_names = {}
		for c in components:
			if c.name is None or c.name == "":
				c.name = self.get_name(c)
			if c.name in used_names:
				tmp = c.name + "_" + str(used_names[c.name])
				while tmp in used_names:
					used_names[c.name] += 1
					tmp = c.name + "_" + str(used_names[c.name])
				used_names[c.name] += 1
				c.name = tmp
			else:
				used_names[c.name] = 0

	def set_top_level_module(self, m):
		assert(self.top is None)
		self.top = m

	def get_combinational_update_order(self, starting_point=None):
		to_compute = starting_point
		for c in self.combinational:
			c.visited = False
		if starting_point == None:
			driven_by_output = [c for c in self.combinational if c.driven_by_output]
			to_compute = self.constants + self.flops + self.inputs + driven_by_output
		for l in to_compute:
			l.visited = True
		compute_order = []
		while len(to_compute) > 0:
			current = to_compute.pop(0)
			current.to_compute = False
			if not current.visited:
				compute_order.append(current)
			current.visited = True
			for load in current.loads:
				if isinstance(load, Module):
					continue
				all_inputs_computed = True
				for driver in load.drivers:
					if driver is not None and not driver.visited:
						all_inputs_computed = False
						break
				if all_inputs_computed and not load.to_compute and not load.visited:
					load.to_compute = True
					to_compute.append(load)
		return compute_order

	def print_clock_crossings(self):
		visited = {}
		def get_driving_flops(f):
			to_explore = [f.get_driver()]
			reset_driver = f.get_reset_driver()
			if reset_driver is not None:
				to_explore.append(reset_driver)
				to_explore.append(f.get_reset())
			driving_flops = []
			while len(to_explore) > 0:
				d = to_explore.pop()
				if d is None:
					continue
				if isinstance(d, Flop):
					driving_flops.append(d)
				elif not isinstance(d, Clock):
					to_explore.extend(d.drivers)
			return driving_flops

		def get_ancestor(f):
			clock_ancestor = f.get_clock()
			clock_ancestor_name = ""
			while (not clock_ancestor is None):
				clock_ancestor_name = clock_ancestor.name
				if (clock_ancestor.has_drivers()):
					clock_ancestor = clock_ancestor.drivers[0]
				else:
					clock_ancestor = None
			return clock_ancestor_name

		for f in self.flops:
			if f.intended_clock_crossing:
				continue
			drivers = get_driving_flops(f)
			for d in drivers:
				if d.get_clock().name != f.get_clock().name:
					clock_ancestor_1_name = get_ancestor(d)
					clock_ancestor_2_name = get_ancestor(f)			

					# We ensure that two clocks with different names are not actually the same clock (issued from the same ancestor)
					if (clock_ancestor_1_name == clock_ancestor_2_name):
						continue

					parent_path = ".".join([o.name for o in reversed(f.get_owner_list()[:-1])])
					path = parent_path + "." + f.name
					parent_path = ".".join([o.name for o in reversed(d.get_owner_list()[:-1])])
					d_path = parent_path + "." + d.name
					print("CRITICAL WARNING:\n  Flop ", path, "\n  driven by a flop in another clock domain: ", d_path,
						"\n  Source domain: ", d.get_clock().name,
						"\n  Sink domain: ", f.get_clock().name,
						"\n  Source Reset: ", d.get_reset().name,
						"\n  Sink Reset: ", f.get_reset().name,
						"\n  You can silence this by setting the intended_clock_crossing property on the driven flop to True")


	def get_owners_up_to_common_owner(self, owners_list, other_list, reverse=False):
		ncol = []
		iteration = owners_list
		last_was_out = True
		for o in iteration:
			if o not in other_list:
				ncol.append(o)
			elif last_was_out:
				last_was_out = False
				ncol.append(o)
		if reverse:
			ncol = list(reversed(ncol))
		return ncol

	def generate_resets(self):
		resets = [s for s in self.combinational if isinstance(s, Reset)]
		for r in resets:
			cycles = r.cycles
			fanout_cycles = 5
			buffer_cycles = 10
			fanout_count = len(r.loads)
			spine_cycles = cycles - buffer_cycles - fanout_cycles
			els_per_spine_node = math.ceil(fanout_count / float(spine_cycles))
			inst = r.owner.inst
			with ModuleScope(r.owner):
				with ParamScope(circuit=self, clock=self.clocks[0], reset=r):
					with ParamScope(reset=r.drivers[0].delay(10)):
						reset_counter = inst(Counter, reset_value=0, increment_value=1, end_value=cycles-1, name="reset_counter")
						with ModuleScope(reset_counter):
							reset_counter.set_increment_condition(inst(NOT, reset_counter.is_done))
					spine_sr = inst(ShiftRegister, depth=spine_cycles, bits=1, name="reset_spine_shift_register")
					spine_sr.d.set_driver(inst(NOT, reset_counter.is_done))
					for i in range(spine_cycles):
						hi = min((i + 1) * els_per_spine_node, fanout_count)
						lo = i * els_per_spine_node
						ll = r.loads[lo:hi]
						fanout = [inst(Logic, bits=1, name="reset_fo") for _ in range(len(ll))]
						inst(PipelinedFanout, spine_sr.sr[i], *fanout, levels=fanout_cycles, name="spine_" + str(i) + "_fanout_pipe")
						for l,f in zip(ll, fanout):
							if hasattr(l, "drivers"):
								for i,d in enumerate(l.drivers):
									if d == r:
										l.drivers[i] = f
										f.add_load(l)
							elif hasattr(l, "reset"):
								with ModuleScope(l):
									reset = l.inst(Logic, bits=1, name="reset")
									reset.set_driver(f)
									l.reset = reset
							else:
								raise NotImplementedError("Element " + str(l) + " doesn't have either drivers or reset attributes.  Don't know how to handle this.  Exiting")


	def propagate_signals(self):
		"""
		Our RTL graph may have many signals that cross module boundaries without having the appropriate signal
		instances in all modules.  For example, if we have module A that instantiates modules B and C, and
		drives signal `sink` in C with signal `driver` in B, module A needs a wire to connect the two signals
		and module B needs `driver` as an output in its port list while module C needs `sink` as an input
		in its port list.
		"""
		for s, propagate_as in self.propagate_to_top.items():
			if propagate_as == "input" and s.owner != self.top:
				with ModuleScope(self.top):
					instance = self.top.inst(Logic, circuit=self, bits=s._bit_width, name=s.name, arithmetic_bit_spec=s.arithmetic_bit_spec)
					s.set_driver(instance)
					instance.is_input = True
		signals = self.combinational + self.flops + self.clocks + self.constants
		for s in signals:
			loads_to_propagate_to = []
			indices = []
			if isinstance(s, Clock):
				loads = s.sinks
			else:
				loads = s.loads
				if ((len(loads) == 0 and self.propagate_unloaded_signals_to_top) or (s in self.propagate_to_top)) and self.top != s.owner:
					with ModuleScope(self.top):
						instance = self.top.inst(Logic, circuit=self, bits=s._bit_width, name=s.name, arithmetic_bit_spec=s.arithmetic_bit_spec)
						if s not in self.propagate_to_top:
							instance.set_driver(s)
						else:
							if self.propagate_to_top[s] == "output":
								instance.set_driver(s)
								instance.is_output = True
						loads = s.loads
			for i,l in enumerate(loads):
				if l.owner != s.owner:
					loads_to_propagate_to.append(l)
					indices.append(i)
			if len(loads_to_propagate_to) == 0:
				continue
			owner_lists = [l.get_owner_list(True) for l in loads_to_propagate_to]
			s_owner_list = s.get_owner_list()
			s_parent_lists = [self.get_owners_up_to_common_owner(s_owner_list, o) for o in owner_lists]
			l_parent_lists = [self.get_owners_up_to_common_owner(o, s_owner_list, True) for o in owner_lists]
			s_parent_max_len_list = []
			for pl in s_parent_lists:
				if len(pl) > len(s_parent_max_len_list):
					s_parent_max_len_list = pl
			if len(s_parent_max_len_list) == 1:
				s_parent_max_len_list = []
			s_for_owner = {s.owner.name : s}
			previous = s
			for p in s_parent_max_len_list:
				previous.is_output = True
				if p == s.owner:
					continue
				with ModuleScope(p):
					new_name = s.owner.name + "_" + s.name
					if isinstance(s, Clock) or isinstance(s, Reset):
						new_name = s.name
					s_for_owner[p.name] = Logic(s._bit_width, previous, signed=s.signed, name=new_name, circuit=self, arithmetic_bit_spec=s.arithmetic_bit_spec)
					if isinstance(s, Clock):
						s_for_owner[p.name].special_properties["is_clock"] = True

				previous = s_for_owner[p.name]
				previous.driven_by_output = True

			for lpl in l_parent_lists:
				for lp in lpl:
					if lp.name in s_for_owner:
						continue
					with ModuleScope(lp):
						new_name = s.owner.name + "_" + s.name
						if isinstance(s, Clock) or isinstance(s, Reset):
							new_name = s.name
						s_for_owner[lp.name] = Logic(s._bit_width, s_for_owner[lp.owner.name], signed=s.signed, name=new_name, owner=lp, circuit=self, arithmetic_bit_spec=s.arithmetic_bit_spec)
						if isinstance(s, Clock):
							s_for_owner[lp.name].special_properties["is_clock"] = True
					s_for_owner[lp.name].is_input = True

			for j,l in reversed(list(enumerate(loads_to_propagate_to))):
				s_for_owner[l.owner.name].loads.append(l)
				if isinstance(s, Clock):
					del s.sinks[indices[j]]
				else:
					del s.loads[indices[j]]
				if isinstance(l, Module):
					continue
				for i,d in enumerate(l.drivers):
					if d == s:
						l.drivers[i] = s_for_owner[l.owner.name]

	def limit_ram_fanout(self):
		def get_driving_flops_for_control_signal(cs):
			driving_flops = []

			if len(cs.drivers) == 0 or cs.drivers[0] is None:
				return []

			d = cs.drivers[0]
			def skip_wires(d):
				while type(d) == Logic or isinstance(d, BitSelect):
					if len(d.drivers) == 0 or d.drivers[0] is None:
						return d
					d = d.drivers[0]
				return d
			d = skip_wires(d)
			while isinstance(d, Flop):
				driving_flops.append(d)
				d = skip_wires(d.get_driver())
			return driving_flops

		num_limited = 0
		for m in self.modules:
			if not isinstance(m, RAM):
				continue
			tile_estimate = m.tile_estimate
			if tile_estimate <= 4 or m.has_fanout_control:
				continue

			control_signal_names = "r_addr w_addr w_en r_en w_data".split(" ")
			limited = False
			for csn in control_signal_names:
				cs = getattr(m, csn)
				driving_flops = get_driving_flops_for_control_signal(cs)
				num_flops = len(driving_flops)
				desired_fanout_depth = math.ceil(math.log(tile_estimate, 4))
				fanout_depth = min(num_flops, desired_fanout_depth)
				if fanout_depth <= 1:
					if (len(cs.drivers) > 0 and cs.drivers[0] is not None and desired_fanout_depth > fanout_depth and
						type(cs.drivers[0]) != Constant):
						path = cs.get_path_string()
						warnings.warn("Could not limit fanout for " + path + " because there were not enough register to register paths to duplicate" +
							" (wanted "+ str(desired_fanout_depth) +", had, " + str(num_flops) + "). Driver type is " + str(type(cs.drivers[0])))
					continue
				fanout_per_stage = math.ceil(tile_estimate**(1/fanout_depth))
				remaining_fanout = tile_estimate
				for df in driving_flops[:fanout_depth]:
					remaining_fanout = math.ceil(remaining_fanout / fanout_per_stage)
					if remaining_fanout == 1:
						break

					get_sdc_filter_str = lambda df=df: "*|" + "|".join([o.name + "_i" for o in reversed(df.get_owner_list()[:-2])]) + "|" + df.name + "[*]"
					Verilog.current_circuit.add_quartus_setting_lambda(
						lambda fs=get_sdc_filter_str, rf=remaining_fanout, df=df: "set_instance_assignment -name DUPLICATE_REGISTER " + str(rf) + " -to " + '"' + fs(df) + '"')
				limited = True
			get_sdc_filter_str = lambda reg_name, m=m: "*|" + "|".join([o.name + "_i" for o in reversed(m.get_owner_list()[:-2])]) + "|" + m.name + "_i|*|" + reg_name + "[*]"
			Verilog.current_circuit.add_quartus_setting_lambda(
				lambda te=math.ceil(tile_estimate/4), fs=get_sdc_filter_str: "set_instance_assignment -name DUPLICATE_REGISTER " + str(te) + " -to " + '"' + fs("address_reg_a") + '"')
			Verilog.current_circuit.add_quartus_setting_lambda(
				lambda te=math.ceil(tile_estimate/4), fs=get_sdc_filter_str: "set_instance_assignment -name DUPLICATE_REGISTER " + str(te) + " -to " + '"' + fs("address_reg_b") + '"')
			Verilog.current_circuit.add_quartus_setting_lambda(
				lambda te=math.ceil(tile_estimate/4), fs=get_sdc_filter_str: "set_instance_assignment -name DUPLICATE_REGISTER " + str(te) + " -to " + '"' + fs("rdaddr_reg") + '"')
			if limited:
				num_limited += 1
		print("    Reduced fanout for ", num_limited, " RAMs")


	def get_verilog(self, t="  "):
		verilog = {}
		modules = {}
		module_count = len(self.modules)
		print("  Limiting RAM Fanouts")
		self.limit_ram_fanout()
		print("  Generating Reset Spine and Fanouts")
		#self.generate_resets()
		print("  Generating Unique Module Names")
		self.make_names_unique(self.modules)
		print("  Propagating Signals")
		self.propagate_signals()
		print("  Generating Unique Names For Signals in each Module")
		for module in self.modules:
			module.make_names_unique(module.combinational + module.flops + module.constants)
		self.print_clock_crossings()
		print("  Generating Verilog for each Module")
		generated_count = 0
		for i,module in enumerate(self.modules):
			if not module.should_create_verilog_definition:
				continue
			generated_count += 1
			v = module.get_verilog(t)
			if v is None:
				continue
			_hash = hashlib.md5(v.encode("utf8")).hexdigest()
			v = "// " + _hash + "\n" + "`timescale 1ns/1ns\n" + v
			verilog[module.name] = v
			modules[module.name] = module
			if sys.stdout.isatty():
				print("\r  Done " + str(i) + "/" + str(module_count) + " modules", end="")
		if sys.stdout.isatty():
			print("\r  Done " + str(module_count) + "/" + str(module_count) + " modules")
		

	
		print("  " + str(generated_count) + " Modules Needed to be Generated (some may have been pruned during generation)")
		return (verilog, modules)

	def add_architecture_specific_files(self, verilog, modules):
		if arch.hpipe_device_arch.arch_name == "S10_NX":
			print("Copying simulation modules and IPs for Stratix 10 NX")



			extra_verilog_rootdirs = []

			extra_verilog_rootdir = "./digitallogic/verilog_files"
			if not os.path.isdir(extra_verilog_rootdir): 
				extra_verilog_rootdir = "../digitallogic/verilog_files"
				if not os.path.isdir(extra_verilog_rootdir): 
					print("ERROR: verilog_files directory in digitallogic missing!")
					exit(1)
			extra_verilog_rootdirs.append(extra_verilog_rootdir)
			
			insert_hbm_sim_files = False
			for i,module in enumerate(self.modules):
				if (isinstance(module, digitallogic.digitallogic_hbm.HBM)):
					insert_hbm_sim_files = True

			if insert_hbm_sim_files:
				extra_verilog_rootdir = "./digitallogic/hbm_exclusive_verilog_files"
				if not os.path.isdir(extra_verilog_rootdir): 
					extra_verilog_rootdir = "../digitallogic/hbm_exclusive_verilog_files"
					if not os.path.isdir(extra_verilog_rootdir): 
						print("ERROR: hbm_exclusive_verilog_files directory in digitallogic missing!")
						exit(1)
				extra_verilog_rootdirs.append(extra_verilog_rootdir)

			for extra_verilog_rootdir in extra_verilog_rootdirs:			
				v = ""
				for subdir, dirs, files in os.walk(extra_verilog_rootdir):
					for file in files:	
						print(os.path.join(subdir, file))
						if ".sv" in file or ".v" in file:
							with open(os.path.join(subdir, file), 'r') as opened_file:
								v = opened_file.read()
								_hash = hashlib.md5(v.encode("utf8")).hexdigest()
								v = "// " + _hash + "\n" + "`timescale 1ns/1ns\n" + v
								
								split_filename = file.split(".")[0]
								
								#Replace Intel IPs with simulation modules to make things easier
								if enable_sim_module_replace and split_filename in sim_module_replace:
									v = v.replace(sim_module_replace[split_filename][0], sim_module_replace[split_filename][1])
								
								verilog[split_filename] = v
								
								filepath = subdir.replace(extra_verilog_rootdir, "")
								#remove extra slash to make things cleaner
								if len(filepath) > 0 and filepath[0] == "/": 
									if len(filepath) > 1: filepath = filepath[1:]
									else: filepath = ""
									
								modules[split_filename] = filepath
		return (verilog, modules)

	def write_verilog_to_dir(self, directory, t="  ", mif_additional_path=""):
		print_stage_header("Beginning Verilog Generation")
		verilog, modules = self.get_verilog(t)
		verilog, modules = self.add_architecture_specific_files(verilog, modules)
		v_paths = []
		mem_initialization_path = directory + mif_additional_path + "/memory_initialization_files"
		weight_hbm_binary_path = directory + mif_additional_path + "/weight_hbm_binary_files"
		print("  Generating Verilog Files")
		total_items = len(verilog)
		updated = 0
		sdc_file_string = ""
		for i, (n, v) in enumerate(verilog.items()):
			m = modules[n]
			if not isinstance(m, str):
				sdc_file_string += m.get_sdc_constraints_string()
				m.print_warnings()
				owner_list = [m.name for m in reversed(m.get_owner_list())]
				path = directory + "/" + "/".join(owner_list)
				filepath = path + "/" + n + ".v"
			else:
				path = directory + "/CIRCUIT/static_verilog_files/" + m
				filepath = path + "/" + n + ".v"
			
			v_paths.append(filepath)
			os.makedirs(path, exist_ok=True)
			needs_update = True

			if isinstance(m, digitallogic.digitallogic_hbm.HBM):
				weight_hbm_bin_data = m.format_weights_for_hardware()
				os.makedirs(weight_hbm_binary_path, exist_ok=True)
				extension = ".bin"
				for wbd in weight_hbm_bin_data:
					string_weight = "weight_" + str(len(weight_hbm_bin_data)) + "_images.bin"
					bin_file_path = weight_hbm_binary_path + "/" + string_weight
					with open(bin_file_path, "ab") as fh:
						fh.write(wbd)					
			if isinstance(m, ROM):
				os.makedirs(mem_initialization_path, exist_ok=True)
				hex_file_contents = m.get_mem_data_string()
				extension = ".mem"
				if m.implement_with_megafunction:
					extension = ".mif"
					new_contents = []
					data = hex_file_contents.split("\n")
					for ln, d in enumerate(data):
						if len(d) == 0:
							continue
						new_contents.append(str(ln) + " : " + d + ";\n")
						last_d_str = d
					if (len(new_contents) % 2) == 1:
						new_contents.append(str(len(new_contents)) + " : " + len(last_d_str) * "0" + "; -- padding\n")
					hex_file_contents = "".join(new_contents)
					data_radix = "HEX"
					if m._use_bit_str():
						data_radix = "BIN"
					hex_file_contents = "DEPTH=" + str(len(new_contents)) + ";\nWIDTH="+str(m.r_data._bit_width)+f";\n\nADDRESS_RADIX = UNS;\nDATA_RADIX = {data_radix};\n\nCONTENT BEGIN\n" + hex_file_contents + "\nEND;\n"
				hex_file_path = mem_initialization_path + "/" + n + extension
				with open(hex_file_path, "w") as fh:
					fh.write(hex_file_contents)
			if os.path.exists(filepath):
				first_line = v.split('\n', 1)[0]
				r_fh = open(filepath, "r")
				if first_line == r_fh.readline()[:-1]:
					needs_update = False
				r_fh.close()
			if needs_update:
				updated += 1
				w_fh = open(filepath, "w")
				w_fh.write(v)
				w_fh.close()
			if sys.stdout.isatty():
				print("\r  Done " + str(i) + "/" + str(total_items), end="")
		if sys.stdout.isatty():
			print("\r  Done " + str(total_items) + "/" + str(total_items))
		print("  " + str(updated) + " Files Needed to be Updated")
		
		#Print final dsp/tensor block count as it may be different from initial estimate (e.g. if we need a bigger multiplier for the mean layer on the NX, it will use multiple tensor blocks)
		global final_dsp_count
		global final_mult_count
		if arch.hpipe_device_arch.arch_name == "S10_NX":
			print("Final Tensor Block count = " + str(final_dsp_count))
			#This isn't exact but provides a good estimate
			if final_dsp_count != 0:
				if arch.hpipe_device_arch.tensor_block_mode == "tensor":
					if final_dsp_count != scalar_tensor_count: average_tensor_usage = ((final_mult_count-scalar_tensor_count*3)/30.0)/(final_dsp_count-scalar_tensor_count) * 100
					else: average_tensor_usage = ((final_mult_count)/3.0)/(final_dsp_count) * 100
				else:
					if final_dsp_count != scalar_tensor_count: average_tensor_usage = ((final_mult_count-scalar_tensor_count*3)/6.0)/(final_dsp_count-scalar_tensor_count) * 100
					else: average_tensor_usage = ((final_mult_count)/3.0)/(final_dsp_count) * 100

			print("Final multiplier count = " + str(final_mult_count) + " (avg tensor usage = " + str (average_tensor_usage) + "%)") 
			print("scalar_tensor_count", scalar_tensor_count)
		else:
			print("Final DSP count = " + str(final_dsp_count))
			if final_dsp_count != 0:  print("Final multiplier count = " + str(final_mult_count) + " (avg dsp usage = " + str ((final_mult_count/2)/final_dsp_count * 100) + "%)") 
		

		with open(directory + "/verilog_paths.txt", "w") as v_paths_fh:
			v_paths_fh.write("\n".join(v_paths))
		with open(directory + "/generated_constraints.sdc", "w") as sdc_fh:
			sdc_fh.write(sdc_file_string)
			for l in self.sdc_lambdas:
				sdc_fh.write(l() + "\n")
		with open(directory + "/generated_quartus_settings.qsf", "w") as gqsf:
			for l in self.qsf_lambdas:
				gqsf.write(l() + "\n")

class Component(object):
	def __init__(self, kind, circuit, **kwargs):
		self.kind = kind
		owner = Verilog.current_module.m
		if isinstance(owner, Module):
			owner.add_element(self)
		self.owner = owner
		self.kwargs = copy(kwargs)
		if isinstance(self, Module):
			self.kwargs["owner"] = self
		else:
			self.kwargs["owner"] = owner
		self.kwargs["circuit"] = circuit
		if "name" in self.kwargs:
			del self.kwargs["name"]
		circuit.add_element(self)
		if not isinstance(owner, Module) and isinstance(self, Module):
			circuit.set_top_level_module(self)

	def get_owner_list(self, include_self_if_module=False):
		owner_list = []
		owner = self.owner
		if include_self_if_module and isinstance(self, Module):
			owner = self
		while isinstance(owner, Module):
			owner_list.append(owner)
			owner = owner.owner
		owner_list.append(owner)
		return owner_list

	def get_instance_hierarchy_string(self):
		owner_list = self.get_owner_list()
		return ".".join([o.name for o in reversed(owner_list)] + [self.name])

class Module(Circuit, Component):
	def __init__(self, module_name, name=None, **kwargs):
		self.name=name
		self.disable_verilator_UNOPTFLAT = False #Gids rid of some false circular logic warnings if enabled. Do not sure unless Quartus synthesis passes on that module with major warnings
		self.module_name = module_name
		self.delay_module = None
		self.should_create_verilog_definition = True
		self.module_attribute_str = ""
		if Verilog.current_circuit is None:
			Verilog.current_circuit = Circuit()
			Verilog.current_module = ModuleScope(Verilog.current_circuit)
			kwargs["circuit"] = Verilog.current_circuit
			Verilog.current_params.kwargs["circuit"] = kwargs["circuit"]
		Circuit.__init__(self, **kwargs)
		Component.__init__(self, **kwargs)

	def get_sdc_constraints_string(self):
		return ""

	def set_module_attribute_str(self, string):
		self.module_attribute_str = string

	def get_instance_comment(self, t="  "):
		return ""

	def inst(self, t, *args, **kwargs):
		with ParamScope(**kwargs):
			instance = t(*args, **Verilog.current_params.kwargs)
			if not isinstance(instance, Module) and "propagate_to_top" in kwargs:
				Verilog.current_circuit.propagate_to_top[instance] = kwargs["propagate_to_top"]
			return instance


	def get_instance_verilog(self, t="  "):
		verilog = self.get_instance_comment(t)
		verilog += t + self.name + " " + self.name + "_i(\n"
		inputs = self.get_inputs()
		outputs = self.get_outputs()
		verilog += t * 2 + "// inputs\n"
		for j,i in enumerate(inputs):
			if j != 0:
				verilog += ",\n"
			verilog += t * 2 + "." + i.name + "(" + i.drivers[0].name + ")"
		add_first_comma = len(inputs) > 0
		for j,i in enumerate(outputs):
			if j != 0 or add_first_comma:
				verilog += ",\n"
			if j == 0:
				verilog += t * 2 + "// outputs\n"
			load = i.loads[0]
			for l in i.loads:
				if not isinstance(l, Logic):
					print(i.get_instance_hierarchy_string() + " drives something that is not logic:")
					print("   " + str(l))
					print("   " + l.name)
					print("   " + l.get_instance_hierarchy_string())
					sys.exit(-1)
				if l.driven_by_output:
					load = l
			verilog += t * 2 + "." + i.name + "(" + load.name + ")"
		verilog += "\n" + t + ");"
		return verilog

	def get_module_header(self, t="  "):
		verilog = ""
		attrs = self.get_module_attribute_str()
		if attrs != "":
			verilog += "(* " + attrs + " *) "
		verilog += "module " + self.name + "(\n"
		inputs = self.get_inputs()
		outputs = self.get_outputs()
		internal = self.get_internal_signals()
		for j,i in enumerate(inputs):
			if j != 0:
				verilog += ",\n"
			verilog += t + i.name
		add_first_comma = len(inputs) > 0
		for j,o in enumerate(outputs):
			if j != 0 or add_first_comma:
				verilog += ",\n"
			verilog += t + o.name
		verilog += self.get_custom_verilog_port_list(t)
		verilog += "\n);\n"
		verilog += self.get_instance_comment(t)
		return verilog

	def get_combinational_assignments(self, t="  "):
		verilog = ""

		inputs = self.get_inputs()
		outputs = self.get_outputs()
		flops = self.flops
		driven_by_output = [s for s in self.combinational if s.driven_by_output]
		simple_assignments = [s for s in self.combinational 
			if (s.assignment_type == 1 or (s.assignment_type == 0 and s.is_output)) 
			and not s.driven_by_output and not s.is_input]
		for s in simple_assignments:
			driver_string = s.get_driver_string(t="", n=0)
			if len(driver_string) > 0:
				# Override for if we want to instantiate an IP instead of using an assign
				if driver_string[0] == "#":
					verilog += t + driver_string[1:] + "\n"
				else:
					verilog += t + "assign " + driver_string + "\n"
		drivers = inputs + driven_by_output + flops + self.constants + simple_assignments
		# make sure they are all unique
		drivers = list({d.name : d for d in drivers}.values())

		comb_order = self.get_combinational_update_order(drivers)
		comb_str = ""
		for c in comb_order:
			if (c.is_input or
				c.driven_by_output or
				isinstance(c, Constant) or
				isinstance(c, Flop) or
				c.owner != self):
				continue
			comb_str += c.get_driver_string(t=t, n=2) + "\n"

		if len(comb_str) > 0:
			verilog += t + "always_comb begin\n"
			verilog += comb_str
			verilog += t + "end\n\n"
		return verilog

	def get_custom_verilog_str(self, t="  "):
		return ""

	def get_custom_verilog_port_list(self, t="  "):
		return ""

	def get_module_attribute_str(self):
		return self.module_attribute_str

	def get_parameter_declaration_string(self):
		return ""

	def get_verilog(self, t="  "):
		verilog = ""
		verilog += self.get_module_header(t)
		if self.__class__.__doc__:
			verilog += v_comment_string(self.__class__.__doc__, t=t)

		verilog += self.get_parameter_declaration_string() + "\n"

		inputs = self.get_inputs()
		outputs = self.get_outputs()

		if self.disable_verilator_UNOPTFLAT == True:
			verilog += "\n//verilator lint_off UNOPTFLAT\n"

		internal = self.get_internal_signals() + self.clocks
		driven_by_output = [s for s in internal if s.driven_by_output]

		signals = inputs + outputs + internal
		for s in signals:
			verilog += t + s.get_declaration_string() + "\n"

		verilog += "\n"

		verilog += self.get_custom_verilog_str(t=t)

		verilog += self.get_combinational_assignments(t=t)

		reset_flops = self.get_reset_flops()
		nr_flops = self.get_no_reset_flops()

		for clock_name, reset_flop_dict in reset_flops.items():
			verilog += t + "always @(posedge " + clock_name + ") begin\n"
			for reset_name, flop_list in reset_flop_dict.items():
				n = 2
				verilog += n * t + "if (" + reset_name + ") begin\n"
				n = 3
				for f in flop_list:
					verilog += n * t + f.name + " <= " + f.get_reset_driver().name + ";\n"
				n = 2
				verilog += n * t + "end else begin\n"
				n = 3
				for f in flop_list:
					if f and f.get_driver():
						verilog += n * t + f.name + " <= " + f.get_driver().name + ";\n"
				n = 2
				verilog += n * t + "end\n"
			verilog += t + "end\n\n"

		for clock_name, flop_list in nr_flops.items():
			verilog += t + "always @(posedge " + clock_name + ") begin\n"
			n = 2
			for f in flop_list:
				verilog += n * t + f.name + " <= " + f.get_driver().name + ";\n"
			verilog += t + "end\n\n"

		for i in internal:
			if not isinstance(i, ForcedSignal) and not isinstance(i, Clock):
				continue
			verilog += i.get_verilog(t=t, n=1)

		for m in self.modules:
			verilog += m.get_instance_verilog(t=t) + "\n\n"

		if self.disable_verilator_UNOPTFLAT == True:
			verilog += "\n//verilator lint_on UNOPTFLAT\n"

		verilog += "endmodule // " + self.name

		

		return verilog

	def print_warnings(self):
		if not hasattr(self, "requires_driver"):
			return
		for s in self.requires_driver:
			if not s.has_drivers() and not s.wave_needs_driver:
				parent_path = ".".join([o.name for o in reversed([self] + self.get_owner_list()[:-1])])
				path = parent_path + "." + s.name
				warnings.warn("Signal " + path + " has no driver, but is required to have one")

class ConnectionError(Exception):
	def __init__(self, value):
		self.value = value

	def __str__(self):
		return str(self.value)

class Logic(Component):
	def __init__(self, bits, *drivers, kind="logic", signed=None, name=None, assignment_type="simple", arithmetic_bit_spec=None, **kwargs):
		self.name = name
		super(Logic, self).__init__(kind=kind, **kwargs)
		self.visited = None
		self.to_compute = False
		self.wave_needs_driver = False
		self.loads = []
		self.arithmetic_bit_spec = arithmetic_bit_spec
		self.special_properties = {}
		if arithmetic_bit_spec is not None:
			# this is overly pessimistic and pretty expensive, but it gets it working for now
			self.arithmetic_bit_spec = copy(arithmetic_bit_spec)
			signed = arithmetic_bit_spec["sign"] == 1
		self.assignment_types = {
			"constant" : 0,
			"simple"   : 1,
			"complex"  : 2
		}
		self.assignment_type = self.assignment_types[assignment_type]
		self._signed = signed
		if bits is not None:
			self.set_bit_width(bits)
		else:
			self._bit_width = None
		if len(drivers) > 0:
			self.set_drivers(list(drivers))
		else:
			drivers = []
			self.drivers = drivers
		for driver in drivers:
			if hasattr(driver, "add_load") and callable(driver.add_load):
				driver.add_load(self)
		self.check_drivers()
		self._signed = signed
		self.is_output = False
		self.driven_by_output = False
		self.is_input = False
		self.current_value = None

	def get_delayed_driver(self, delay):
		new_d = self.copy()
		self.set_driver(new_d.delay(delay))
		return new_d

	def get_sdc_filter_string(self):
		parent_path = "*|" + "|".join([o.name + "_i" for o in reversed(self.get_owner_list()[0:-2])])
		return parent_path + "|" + self.name


	def get_signed(self):
		if self._signed or (self.arithmetic_bit_spec and self.arithmetic_bit_spec["sign"]):
			return True
		return False

	def set_signed(self, signed):
		self._signed = signed
		if self.arithmetic_bit_spec:
			self.arithmetic_bit_spec["sign"] = 1 if signed else 0

	signed = property(get_signed,set_signed)

	def copy(self):
		inst = Verilog.current_module.m.inst
		new_s = inst(Logic, bits=self._bit_width, signed=self.signed, name=self.name)
		new_s.set_driver(self)
		return new_s

	def get_extended(self, bits):
		if bits > self._bit_width:
			inst = self.owner.inst
			difference = bits - self._bit_width
			if self.signed:
				sign_bit = self[self._bit_width-1]
				return inst(Concat, signed=True, *([self] + difference * [sign_bit]), name=self.name + "_extended")
			else:
				return inst(Concat, *([self] + difference * [inst(Constant, bits=1, value=0, name="zero")]), name=self.name + "_extended")
		return self

	def delay(self, cycles, hyperpipe=False, max_fan=None, final_fan=None, final_fan_of_1=False, auto_shift_register_recognition=True, pack_to_memory=False):
		sr = self.get_delay(self, cycles, hyperpipe=hyperpipe, max_fan=max_fan, final_fan=final_fan, final_fan_of_1=final_fan_of_1, auto_shift_register_recognition=auto_shift_register_recognition, pack_to_memory=pack_to_memory)
		return sr

	def periodic_hold_valid(self, cycles=2):
		inst = Verilog.current_module.m.inst
		not_curr_eq_prev = inst(Logic, bits=1, name="not_curr_eq_prev")
		reset_signal = inst(OR, not_curr_eq_prev, Verilog.current_params.kwargs["reset"])
		with ParamScope(reset=reset_signal):
			counter = inst(Counter, reset_value=0, increment_value=1, end_value=cycles-1, name=f"{self.name}_periodic_hold_valid_counter")
		counter.set_increment_condition(inst(Constant, value=1, bits=1))
		previous_value = self.delay(1)
		reset_signal.set_driver(inst(NOT, inst(EQ, self, previous_value)))
		done_and_incrementing = counter.done_and_incrementing
		return done_and_incrementing



	def set_bit_width(self, bits):
		self._bit_width = bits
		self._bit_mask = get_bit_mask(self._bit_width)

	def get_path_string(self):
		parent_path = ".".join([o.name for o in reversed([self.owner] + self.owner.get_owner_list()[:-1])])
		path = parent_path + "." + self.name
		return path

	def cast(self, arithmetic_bit_spec):
		if self.arithmetic_bit_spec == None:
			self.arithmetic_bit_spec = arithmetic_bit_spec
			if np.sum(list(arithmetic_bit_spec.values())) != self._bit_width:
				warnings.warn("No existing arithmetic bit spec for %s, and cast bit spec does not match bit width" % self.name)
			return self

		from_spec = self.arithmetic_bit_spec
		to_spec = arithmetic_bit_spec
		inst = Verilog.current_module.m.inst

		signal = self
		lo = from_spec["frac"] - to_spec["frac"]
		hi = from_spec["frac"] + to_spec["int"] - 1
		sign_bit_index = signal._bit_width-1
		if lo < 0:
			hi -= lo
			signal = inst(Concat, inst(Constant, value=0, bits=-lo), signal)
			lo = 0
		if hi > sign_bit_index:
			sign_extend = signal[sign_bit]
			if not from_spec["sign"]:
				sign_extend = inst(Constant, bits=1, value=0)
			signal = inst(Concat, signal, inst(Concat, *([sign_extend]*(hi-sign_bit_index))))

		with ParamScope(name="slimmed_output"):
			slimmed_output = signal[hi:lo]
		positive_overflow = inst(OR,
			inst(Constant, bits=1, value=0),
			*[signal[k] for k in range(hi+1, sign_bit_index)],
			name="positive_overflow")
		positive_overflow_select = positive_overflow
		max_value = inst(Constant,
			name="max_value",
			bits=slimmed_output._bit_width,
			value=(1<<slimmed_output._bit_width)-1)

		if to_spec["sign"] == 1:
			with ParamScope(name="sign_bit"):
				sign_bit = signal[sign_bit_index]
			positive_overflow_and_not_sign = inst(AND, inst(NOT, sign_bit), positive_overflow)
			positive_overflow_select = positive_overflow_and_not_sign

		output_mux = inst(MuxV2,
			positive_overflow_select,
			slimmed_output,
			max_value,
			name="select_int_max_if_overflow")
		output = output_mux
		if to_spec["sign"]:
			output = inst(Concat, output_mux, sign_bit, name="signed_output")
		casted = inst(Logic, bits=output._bit_width, name=signal.name+"_cast")
		casted.set_driver(output)
		return casted




	def get_declaration_string(self):
		d_str = ""
		type_is_wire = self.assignment_type <= 1 or self.driven_by_output
		if self.is_input:
			d_str += "input "
		elif self.is_output:
			if type_is_wire and not self.assignment_type == 0:
				d_str += "output "
			else:
				d_str += "output reg "
		elif type_is_wire:
			d_str += "wire "
		else:
			d_str += "reg "

		if self.signed:
			d_str += "signed "

		if self._bit_width is None:
			self.set_bit_width(self.drivers[0]._bit_width)
		if self._bit_width <= 0:
			return ""
		synthesis_attributes_str = ""
		if hasattr(self, "synthesis_attributes") and self.synthesis_attributes is not None:
			synthesis_attributes_str = " /* " + self.synthesis_attributes + " */"
		name_string = self.name + synthesis_attributes_str + ";"
		if self.arithmetic_bit_spec:
			bs_string = ", ".join([k + ": " + str(v) for k,v in self.arithmetic_bit_spec.items()])
			name_string += " // " + bs_string
		if self.assignment_type == 0 and not self.is_output:
			name_string = self.get_driver_string(t="", n=0)
		d_str += "[" + str(self._bit_width - 1) + ":0] " + name_string
		return d_str

	def has_drivers(self):
		return len(self.drivers) > 0

	def check_drivers(self, *drivers):
		if not self.has_drivers() and drivers is ():
			return False
		if drivers is ():
			drivers = self.drivers
		for driver in drivers:
			if not isinstance(driver, Logic) and not isinstance(self, Clock):
				path = self.get_path_string()
				raise ConnectionError("Signal " + path + " is driven by something that is not a subclass of Logic: " + str(type(driver)))
		if len(drivers) == 1:
			d = drivers[0]
			if d is None:
				return True
			if isinstance(self, BitSelect):
				if self.start >= d._bit_width or self.stop >= d._bit_width:
					path = self.get_path_string()
					d_path = d.get_path_string()
					raise ConnectionError("Signal " + path + " trying to select [" + str(self.start) + ":" + str(self.stop) + "] but driver " + d_path + " only has a width of " + str(d._bit_width))

			elif d._bit_width is not None and self._bit_width is not None:
				if d._bit_width > self._bit_width:
					path = self.get_path_string()
					d_path = d.get_path_string()
					warnings.warn("Driver " + d_path + " has a width of " + str(d._bit_width) + " while " + path + " has a bit width of " + str(self._bit_width), stacklevel=5)
				elif d._bit_width < self._bit_width:
					path = self.get_path_string()
					d_path = d.get_path_string()
					raise ConnectionError("Signal " + path + " has a width of " + str(self._bit_width) + " but driver " + d_path + " only has a width of " + str(d._bit_width))
		return True

	def get_driver_string(self, t="  ", n=2):
		if not self.check_drivers():
			return ""
		return t * n + self.name + " = " + self.drivers[0].name + ";"

	def get_delay(self, driver, delay, hyperpipe=False, max_fan=None, final_fan=None, final_fan_of_1=False, auto_shift_register_recognition=True, pack_to_memory=False):
		driver_is_list = False
		driver_list = driver
		if delay is None or delay <= 0:
			return driver
		if Verilog.current_module.m.delay_module is None:
			Verilog.current_module.m.delay_module = Verilog.current_module.m.inst(DelayModule, name="delay")
		delay_module = Verilog.current_module.m.delay_module
		with ModuleScope(delay_module):
			if type(driver) is list:
				driver = self.owner.inst(Concat, *driver, name="concatenated_drivers_for_pipe")
				driver_is_list = True
			with ParamScope(arithmetic_bit_spec=driver.arithmetic_bit_spec, signed=driver.signed):
				if hyperpipe:
					sr = delay_module.inst(HyperPipe, bits=driver._bit_width, max_depth=delay, name=driver.name + "_hyperpipe_max_" + str(delay))
				#elif pack_to_memory and (delay > 3):
				#	sr = delay_module.inst(MemoryShiftRegister, bits=driver._bit_width, depth=delay, name=driver.name + "_delay_" + str(delay))
				#elif max_fan is None or final_fan is None:
				#	sr = delay_module.inst(IndividualDelay, bits=driver._bit_width, depth=delay, name=driver.name + "_delay_" + str(delay))
				else:
					sr = delay_module.inst(BasicShiftRegister, bits=driver._bit_width, depth=delay, name=driver.name + "_delay_" + str(delay))

		if not auto_shift_register_recognition:
			sr.set_module_attribute_str('altera_attribute = "-name AUTO_SHIFT_REGISTER_RECOGNITION OFF"')


		if max_fan is not None and final_fan is not None:
			stages = math.ceil(math.log(final_fan, max_fan))
			if final_fan_of_1:
				stages += 1
			stages = min(stages, delay)
			sr.set_module_attribute_str('altera_attribute = "-name AUTO_SHIFT_REGISTER_RECOGNITION OFF"')

			stage_duplicates = []
			if final_fan_of_1:
				stage_duplicates.append(final_fan)
				stages -= 1
				if stages == 1:
					stage_duplicates.insert(0, math.ceil(math.sqrt(final_fan)))
			if stages > 1:
				max_fan = math.ceil(math.log(final_fan, stages))
				remaining_fan = final_fan
				for i in range(stages):
					registers_in_stage = math.ceil(remaining_fan / max_fan)
					stage_duplicates.insert(0, registers_in_stage)
					remaining_fan = registers_in_stage

			start_stage = delay - len(stage_duplicates)
			for r,d in zip(sr.sr[start_stage:], stage_duplicates):
				if d > 1:
					Verilog.current_circuit.add_quartus_setting_lambda(
						lambda dup=d, reg=r: "set_instance_assignment -name DUPLICATE_REGISTER " + str(dup) + " -to " + '"' + reg.get_sdc_filter_string() + "[*]" + '"')

		sr.d.set_driver(driver)
		if driver_is_list:
			outputs = []
			with ModuleScope(Verilog.current_module.m.delay_module):
				accumulated_bit_width = 0
				for d in driver_list:
					with ParamScope(name=d.name):
						outputs.append(sr.q[accumulated_bit_width + d._bit_width-1:accumulated_bit_width])
					accumulated_bit_width += d._bit_width
			return outputs
		else:
			return sr.q

	def set_driver(self, driver, delay=None, hyperpipe=False):
		assert(driver is not None)
		driver = self.get_delay(driver, delay, hyperpipe=hyperpipe)
		if hasattr(driver, "add_load") and callable(driver.add_load):
			driver.add_load(self)
		self.drivers = [driver]
		if not self.arithmetic_bit_spec:
			self.arithmetic_bit_spec = copy(driver.arithmetic_bit_spec)
		self.check_drivers()

	def set_drivers(self, drivers):
		self.drivers = []
		for driver in drivers:
			if type(driver) is int:
				driver = Verilog.current_module.m.inst(Constant, value=driver, bits=self._bit_width, signed=self.signed, name=self.name+"_const_"+str(abs(driver)))
			if hasattr(driver, "add_load") and callable(driver.add_load):
				driver.add_load(self)
			self.drivers.append(driver)
			if driver and not self.arithmetic_bit_spec:
				self.arithmetic_bit_spec = copy(driver.arithmetic_bit_spec)
		self.check_drivers()

	def add_load(self, load):
		if not isinstance(load, Logic):
			raise ConnectionError("Trying to add " + str(load) + " as load to " + load.get_instance_hierarchy_string() + " but it is not an instance of Logic.")
		self.loads.append(load)

	def bit_width(self):
		return self._bit_width

	def update(self):
		self.current_value = self.drivers[0]() & self._bit_mask

	def __call__(self, update=False):
		if not update:
			return self.current_value
		for d in self.drivers:
			if d is None:
				self.current_value = None
				return None
		self.update()
		return self.current_value

	def __getitem__(self, i):
		#kwargs = Verilog.get_instantiating_module().kwargs
		module = Verilog.current_module.m
		if isinstance(i, slice):
			assert(i.step == None)
			start = i.start
			stop = i.stop
			if start is None:
				start = self._bit_width - 1
			if stop is None:
				stop = 0
			return module.inst(BitSelect, self, start, stop)
		else:
			return module.inst(BitSelect, self, i, i)

	def _get_binary_op_name(self, op, i1, i2):
		names = []
		for i in [i1,i2]:
			if type(i) is int:
				names.append(str(i))
			else:
				names.append(i.name)
		return "_".join([names[0], op, names[1]])

	def __lt__(self, other):
		return Verilog.current_module.m.inst(LT, self, other, name=self._get_binary_op_name("lt", self, other))

	def __gt__(self, other):
		return Verilog.current_module.m.inst(GT, self, other, name=self._get_binary_op_name("gt", self, other))

	def __le__(self, other):
		return Verilog.current_module.m.inst(LE, self, other, name=self._get_binary_op_name("le", self, other))

	def __ge__(self, other):
		return Verilog.current_module.m.inst(GE, self, other, name=self._get_binary_op_name("ge", self, other))

	def __add__(self, other):
		return Verilog.current_module.m.inst(ADD, self, other, name=self._get_binary_op_name("plus", self, other))

	def __sub__(self, other):
		return Verilog.current_module.m.inst(SUB, self, other, name=self._get_binary_op_name("minus", self, other))

class DelayModule(Module):
	def __init__(self, **kwargs):
		super(DelayModule, self).__init__("delay_module", kind="delay_module", **kwargs)

class IndividualDelay(Module):
	have_created_definition = False
	def __init__(self, bits, depth, **kwargs):
		super(IndividualDelay, self).__init__("individual_delay", kind="individual_delay", **kwargs)
		inst = self.inst
		self.bits = bits
		self.depth = depth
		with ModuleScope(self):
			self.d = inst(Flop, bits=bits, name="d")
			self.q = inst(Flop, bits=bits, name="q")
			self.q.set_driver(self.d)

	def get_instance_verilog(self, t="  "):
		if len(self.q.loads) == 0:
			return ""
		verilog = self.get_instance_comment(t)
		verilog += t + "individual_delay #(\n"
		verilog += 2*t + f".DEPTH({self.depth}),\n"
		verilog += 2*t + f".BITS({self.bits})\n"
		verilog += t + ") " + self.name + "_i(\n"
		inputs = self.get_inputs()
		outputs = self.get_outputs()
		verilog += t * 2 + "// inputs\n"
		for j,i in enumerate(inputs):
			if j != 0:
				verilog += ",\n"
			if "is_clock" in i.drivers[0].special_properties:
				verilog += t * 2 + ".clock(" + i.drivers[0].name + ")"
			else:
				verilog += t * 2 + ".d(" + i.drivers[0].name + ")"
		add_first_comma = len(inputs) > 0
		for j,i in enumerate(outputs):
			if j != 0 or add_first_comma:
				verilog += ",\n"
			if j == 0:
				verilog += t * 2 + "// outputs\n"
			load = i.loads[0]
			for l in i.loads:
				if not isinstance(l, Logic):
					print(i.get_instance_hierarchy_string() + " drives something that is not logic:")
					print("   " + str(l))
					print("   " + l.name)
					print("   " + l.get_instance_hierarchy_string())
					sys.exit(-1)
				if l.driven_by_output:
					load = l
			verilog += t * 2 + "." + i.name + "(" + load.name + ")"
		verilog += "\n" + t + ");"
		return verilog

	def get_verilog(self, t="  "):
		if (IndividualDelay.have_created_definition):
			return None

		ct = 1 * t
		l = lambda s: ct + s + "\n"
		IndividualDelay.have_created_definition = True
		verilog = ""
		attrs = self.get_module_attribute_str()
		if attrs != "":
			verilog += "(* " + attrs + " *) "
		verilog += "module individual_delay #(DEPTH, BITS)(\n"
		ct = 2 * t
		verilog += l("clock,")
		verilog += l("d,")
		verilog += l("q")
		verilog += ");\n"
		ct = 1 * t
		verilog += l("input clock;")
		verilog += l("input [BITS-1:0] d;")
		verilog += l("output [BITS-1:0] q;")
		verilog += l("logic [BITS-1:0] sr[DEPTH];")
		verilog += l("assign q = sr[DEPTH-1];")
		verilog += l("")
		verilog += l("if (DEPTH > 2) begin")
		ct = 2 * t
		verilog += l("always @(posedge clock) begin")
		ct = 3 * t
		verilog +=     l("sr[0] <= d;")
		verilog +=     l("sr[1:DEPTH-1] <= sr[0:DEPTH-2];")
		ct = 2 * t
		verilog += l("end")
		ct = 1 * t
		verilog += l("end else if (DEPTH > 1) begin")
		ct = 2 * t
		verilog += l("always @(posedge clock) begin")
		ct = 3 * t
		verilog +=     l("sr[0] <= d;")
		verilog +=     l("sr[1] <= sr[0];")
		ct = 2 * t
		verilog += l("end")
		ct = 1 * t
		verilog += l("end else begin")
		ct = 2 * t
		verilog += l("always @(posedge clock) begin")
		ct = 3 * t
		verilog +=     l("sr[0] <= d;")
		ct = 2 * t
		verilog += l("end")
		ct = 1 * t
		verilog += l("end")
		verilog += "endmodule\n"
		return verilog



class Concat(Logic):
	def __init__(self, *drivers, kind="concat", **kwargs):
		if "bits" in kwargs:
			del kwargs["bits"]
		bit_width = 0
		for d in drivers:
			bit_width += d._bit_width
		arithmetic_bit_spec = None
		frac_addition = 0
		int_addition = 0
		for d in drivers:
			if arithmetic_bit_spec is None:
				arithmetic_bit_spec = copy(d.arithmetic_bit_spec)
				if d.arithmetic_bit_spec is None:
					frac_addition += 1
			elif d.arithmetic_bit_spec and not (d.arithmetic_bit_spec["sign"] == 1 and
												d.arithmetic_bit_spec["int"] == 0 and
												d.arithmetic_bit_spec["frac"] == 0):
				arithmetic_bit_spec = None
				break
			else:
				int_addition += 1

		if arithmetic_bit_spec is not None:
			arithmetic_bit_spec["int"] += int_addition
			arithmetic_bit_spec["frac"] += frac_addition
			kwargs["arithmetic_bit_spec"] = arithmetic_bit_spec

		kwargs["kind"] = kind

		super(Concat, self).__init__(bit_width, *drivers, **kwargs)

	def update(self):
		bit = 0
		self.current_value = 0
		for s in self.drivers:
			self.current_value |= s() << bit
			bit += s._bit_width

	def get_driver_string(self, t="  ", n=2):
		string = t * n + self.name + " = {"
		for i,d in enumerate(reversed(self.drivers)):
			if i != 0:
				string += ", "
			string += d.name
		string += "};"
		return string


class BitSelect(Logic):
	"""
	Selects bits from a :class:`Logic` signal

	Usually you wouldn't instantiate this directly.  Instead, select bits with the [] operator.

	For example:
	::
		a = dl.Logic(bits=5, signed=True, name="a")
		a_sign = a[4]
		a_sign.name = "a_sign"
		a_without_sign = a[3:0]
		a_without_sign.name = "a_without_sign"
	"""
	def __init__(self, driver, start, stop, kind="bit_select", **kwargs):
		if "bits" in kwargs:
			del kwargs["bits"]
		self.start = start
		self.stop = stop
		bits = start - stop + 1
		if driver.arithmetic_bit_spec:
			arithmetic_bit_spec = {"sign" : 0}
			arithmetic_bit_spec["frac"] = driver.arithmetic_bit_spec["frac"] - stop
			arithmetic_bit_spec["int"] = start - stop + 1 - arithmetic_bit_spec["frac"]
			if "sign" in driver.arithmetic_bit_spec and driver.arithmetic_bit_spec["sign"] == 1 and start == (driver._bit_width - 1):
				arithmetic_bit_spec["sign"] = 1
				arithmetic_bit_spec["int"] -= 1
			for k,v in arithmetic_bit_spec.items():
				if v < 0:
					arithmetic_bit_spec[k] = 0
			if bits != np.sum(list(arithmetic_bit_spec.values())):
				arithmetic_bit_spec["frac"] = 0
				arithmetic_bit_spec["int"] = 0
			kwargs["arithmetic_bit_spec"] = arithmetic_bit_spec

		super(BitSelect, self).__init__(start - stop + 1, driver, kind=kind, **kwargs)
		if driver:
			self.check_connection_validity()

	def update(self):
		self.current_value = (self.drivers[0]() >> self.stop) & self._bit_mask

	def get_driver_string(self, t="  ", n=2):
		self.check_connection_validity()
		return t * n + self.name + " = " + self.drivers[0].name + "[" + str(self.start) + ":" + str(self.stop) + "]" + ";"

	def check_connection_validity(self):
		# Prints a warning if the bit selection is out of range
		if self.start > (self.drivers[0]._bit_width - 1) or self.stop < 0:
			path = self.get_path_string()
			d = self.drivers[0]
			d_path = d.get_path_string()

			warnings.warn("Driver " + d_path + " has a width of " +
				str(d._bit_width) + " while " + path +
				" tries to select bits [" + str(self.start) + ":" + str(self.stop) + "]",
				stacklevel=5)


class ReductionOp(Logic):
	def __init__(self, op_str, *drivers, bits=None, extend_to_match_bits=None, supports_unary_op=False, reduction_output_bits=1, **kwargs):
		self.supports_unary_op = supports_unary_op
		inst = Verilog.current_module.m.inst
		if "bits" in kwargs:
			del kwargs["bits"]
		drivers = list(drivers)
		max_bit_width = max([d._bit_width for d in drivers if hasattr(d, "_bit_width")])
		for i,d in enumerate(drivers):
			if type(d) is int:
				drivers[i] = inst(Constant, bits=max_bit_width, value=d, name="constant_value_" + str(d))
		if extend_to_match_bits is not None:
			drivers = [d.get_extended(max_bit_width) for d in drivers]
		super(ReductionOp, self).__init__(reduction_output_bits, *drivers, **kwargs)
		self.op_str = op_str

	def get_driver_string(self, t="  ", n=2):
		if self.supports_unary_op and len(self.drivers) == 1:
			string = t * n + self.name + " = " + self.op_str + self.drivers[0].name
		else:
			string = t * n + self.name + " = " + self.drivers[0].name
			for rd in self.drivers[1:]:
				string += " " + self.op_str + " " + rd.name
		string += ";"
		return string


class Flop(Logic):
	"""Describes a Flip-Flop.

	Every flip flop must have a clock and a reset associated with it when it is instantiated, but will
	only use the reset signal if reset_driver is not None.  After instantiation you can set the non-reset
	or reset drivers with ``set_driver`` and ``set_reset_driver``

	If you want any particular synthesis attributes to be associated with your flop you can
	set ``synthesis_attributes`` to something like ``"synthesis keep"``
	"""
	def __init__(self, clock, reset, bits=None, driver=None, reset_driver=None, name="", signed=None, kind="flop", assignment_type="complex", synthesis_attributes=None, intended_clock_crossing=False, **kwargs):
		super(Flop, self).__init__(name=name, kind=kind, bits=bits, assignment_type=assignment_type, **kwargs)

		self.intended_clock_crossing = intended_clock_crossing
		self.synthesis_attributes = synthesis_attributes
		self.drivers = [driver, reset, reset_driver, clock]
		if reset_driver is not None:
			self.set_reset_driver(reset_driver)
		self._bit_width = bits
		if driver is not None:
			self.set_driver(driver)
		if self.signed is None:
			self.signed = signed
		if signed is None and driver is not None:
			self.signed = driver.signed
		self.d = 0
		self.q = 0
		self.updated = False
		clock.add_sink(self)

	def get_clock(self):
		return self.drivers[3]

	def get_reset(self):
		return self.drivers[1]

	def get_driver(self):
		return self.drivers[0]

	def get_reset_driver(self):
		return self.drivers[2]

	def set_driver(self, driver, delay=None):
		driver = self.get_delay(driver, delay)
		if hasattr(driver, "add_load") and callable(driver.add_load):
			driver.add_load(self)
		self.drivers[0] = driver
		if self.signed is None:
			self.signed = driver.signed
		self.check_drivers(driver)
		if self._bit_width is None:
			self.set_bit_width(driver._bit_width)
		if not self.arithmetic_bit_spec:
			self.arithmetic_bit_spec = copy(driver.arithmetic_bit_spec)

	def set_reset_driver(self, reset_driver):
		if type(reset_driver) is int:
			reset_driver = Verilog.current_module.m.inst(Constant, value=reset_driver, bits=self._bit_width, signed=self.signed, name=self.name+"_reset_driver")
		if hasattr(reset_driver, "add_load") and callable(reset_driver.add_load):
			reset_driver.add_load(self)
		if hasattr(self.get_reset(), "add_load") and callable(self.get_reset().add_load):
			self.get_reset().add_load(self)
		self.drivers[2] = reset_driver
		self.check_drivers(reset_driver)

	def __call__(self, level=None):
		if level is None:
			return self.q
		if self.drivers[2] is not None and self.drivers[1]():
			if level == 1:
				self.updated = False
				self.q = self.d
			elif not self.updated:
				self.updated = True
				self.d = self.drivers[2]()
		else:
			if level == 1:
				self.updated = False
				self.q = self.d
			elif not self.updated:
				self.updated = True
				self.d = self.drivers[0]()
		return self.q

class Constant(Logic):
	"""This class builds constants that can have specific bit widths and optional signed decorators
	"""
	def __init__(self, value, signed=False, bits=None, kind="constant", assignment_type="constant", **kwargs):
		if bits is None:
			if value == 0:
				bits = 1
			else:
				bits = clog2(abs(value)+1)
			if signed:
				bits += 1
		super(Constant, self).__init__(bits, kind=kind, assignment_type=assignment_type, **kwargs)
		self.signed = signed
		self.current_value = value

	def get_driver_string(self, t="  ", n=2):
		signed_str = "s"
		if not self.signed:
			signed_str = ""
		d_str = t * n + self.name + " = " + v_hex_str(self.current_value, self._bit_width) + ";"
		if self.arithmetic_bit_spec:
			bs_string = ", ".join([k + ": " + str(v) for k,v in self.arithmetic_bit_spec.items()])
			d_str += " // " + bs_string
		else:
			d_str += " // " + str(self.current_value)
		return d_str

	def update(self):
		return

class ADD(Logic):
	"""Adds two :class:`Logic` signals together

	You can use the + operator instead of manually instantiating this class

	If the two signals have ``arithmetic_bit_spec`` properties, then the
	two signals will be aligned before they are added.

	If extend_to_match_bits is not None, it will automatically match the
	bit widths of the two inputs prior to adding them, by padding them with
	either a zero extension or a sign extension depending on if the signal is
	signed or unsigned.
	"""
	def __init__(self, a, b, kind="add", extend_to_match_bits=None, **kwargs):
		if "bits" in kwargs:
			del kwargs["bits"]
		arithmetic_bit_spec = None
		bits_to_extend = 1
		bits = lambda: max(a._bit_width, b._bit_width) + bits_to_extend
		if a.arithmetic_bit_spec and b.arithmetic_bit_spec:
			a,b,arithmetic_bit_spec = self._align(a, b)
			arithmetic_bit_spec["int"] += 1
			kwargs["arithmetic_bit_spec"] = arithmetic_bit_spec
			if arithmetic_bit_spec["sign"]:
				bits_to_extend = 0

		if extend_to_match_bits is not None:
			a = a.get_extended(b._bit_width)
			b = b.get_extended(a._bit_width)
		super(ADD, self).__init__(bits(), a, b, kind=kind, **kwargs)

	def _align(self, i1, i2):
		inst = Verilog.current_module.m.inst
		new_spec = {}
		i1_bit_spec = i1.arithmetic_bit_spec
		i2_bit_spec = i2.arithmetic_bit_spec
		for k,v in i1_bit_spec.items():
			new_spec[k] = v
		def get_extended(_input, bits_to_extend, signed, extend_integer=True):
			nonlocal inst
			if extend_integer:
				string = "integer"
			else:
				string = "fractional"
			if signed:
				to_repeat = _input[_input._bit_width-1]
			else:
				to_repeat = inst(Constant, bits=1, value=0, name=_input.name + "_" + string + "_extend")
			if extend_integer:
				to_concat = [_input] + [to_repeat] * bits_to_extend
			else:
				# extend fractional
				to_concat = [_input[0]] * bits_to_extend + [_input]

			return inst(Concat, *to_concat, signed=signed, name=_input.name + "_" + string + "_extended")
		def get_fractional_extend_amount(greater_spec, lesser_spec):
			nonlocal new_spec
			new_spec["frac"] = greater_spec["frac"]
			return greater_spec["frac"] - lesser_spec["frac"]
		def get_int_extend_amount(greater_spec, lesser_spec):
			nonlocal new_spec
			new_spec["int"] = greater_spec["int"]
			new_spec["sign"] = greater_spec["sign"]
			add_or_subtract_sign_bit = 0
			if greater_spec["sign"] and not lesser_spec["sign"]:
				add_or_subtract_sign_bit = 1
			elif not greater_spec["sign"] and lesser_spec["sign"]:
				add_or_subtract_sign_bit = -1
			return greater_spec["int"] - lesser_spec["int"] + add_or_subtract_sign_bit
		if i1_bit_spec["frac"] < i2_bit_spec["frac"]:
			e = get_fractional_extend_amount(i2_bit_spec, i1_bit_spec)
			i1 = get_extended(i1, e, i1_bit_spec["sign"], extend_integer=False)
		elif i1_bit_spec["frac"] > i2_bit_spec["frac"]:
			e = get_fractional_extend_amount(i1_bit_spec, i2_bit_spec)
			i2 = get_extended(i2, e, i2_bit_spec["sign"], extend_integer=False)

		if i1_bit_spec["int"] < i2_bit_spec["int"]:
			e = get_int_extend_amount(i2_bit_spec, i1_bit_spec)
			new_sign = i1_bit_spec["sign"]
			if not i1_bit_spec["sign"] and i2_bit_spec["sign"]:
				new_sign = True
				e += 1
			if e > 0:
				i1 = get_extended(i1, e, i1_bit_spec["sign"], extend_integer=True)
				i1_bit_spec["sign"] = new_sign
		elif i1_bit_spec["int"] > i2_bit_spec["int"]:
			e = get_int_extend_amount(i1_bit_spec, i2_bit_spec)
			new_sign = i2_bit_spec["sign"]
			if i1_bit_spec["sign"] and not i2_bit_spec["sign"]:
				new_sign = True
				e += 1
			if e > 0:
				i2 = get_extended(i2, e, i2_bit_spec["sign"], extend_integer=True)
				i2_bit_spec["sign"] = new_sign

		# extend one additional bit to allow two max integer quantities to overflow without
		# overflowing into the sign bit
		i1 = get_extended(i1, 1, i1_bit_spec["sign"], extend_integer=True)
		i2 = get_extended(i2, 1, i2_bit_spec["sign"], extend_integer=True)
		if not new_spec["sign"]:
			new_spec["int"] += 1
		return (i1, i2, new_spec)

	def update(self):
		self.current_value = self.drivers[0]() + self.drivers[1]()

	def get_driver_string(self, t="  ", n=2):
		return t * n + self.name + " = " + self.drivers[0].name + " + " + self.drivers[1].name + ";"

class SUB(ADD):
	def __init__(self, a, b, kind="sub", extend_to_match_bits=None, **kwargs):
		super(SUB, self).__init__(a, b, "sub", extend_to_match_bits, **kwargs)

	def get_driver_string(self, t="  ", n=2):
		return t * n + self.name + " = " + self.drivers[0].name + " - " + self.drivers[1].name + ";"


class ReduceAdd(Module):
	def __init__(self, *inputs, kind="reduce_add", **kwargs):
		super(ReduceAdd, self).__init__("reduce_add", kind=kind, **kwargs)
		inst = self.inst
		with ModuleScope(self):
			to_reduce = copy(list(inputs))
			while len(to_reduce) > 1:
				a = to_reduce.pop(0)
				b = to_reduce.pop(0)
				to_reduce.append(a + b)
			self.output = to_reduce[0]


class MUL(Logic):
	def __init__(self, a, b, kind="mul", extend_to_match_bits=None, **kwargs):
		if "name" not in kwargs or kwargs["name"] is None:
			kwargs["name"] = self._get_binary_op_name("times", a, b)
		def check_sign_and_extend(a, b):
			inst = Verilog.current_module.m.inst
			if (a.arithmetic_bit_spec and not a.arithmetic_bit_spec["sign"] and
				b.arithmetic_bit_spec and b.arithmetic_bit_spec["sign"]):
				new_a = inst(Concat, a, inst(Constant, bits=1, value=0, name="sign_padding"), name=a.name + "_sign_padded")
				new_a.arithmetic_bit_spec["sign"] = 1
				new_a.arithmetic_bit_spec["int"] -= 1
				return new_a
			return a
		a = check_sign_and_extend(a, b)
		b = check_sign_and_extend(b, a)
		if extend_to_match_bits is not None:
			a = a.get_extended(b._bit_width)
			b = b.get_extended(a._bit_width)
		if a.arithmetic_bit_spec and b.arithmetic_bit_spec:
			kwargs["arithmetic_bit_spec"] = {
				"sign" : a.arithmetic_bit_spec["sign"] | b.arithmetic_bit_spec["sign"],
				"int" : a.arithmetic_bit_spec["int"] + b.arithmetic_bit_spec["int"],
				"frac" : a.arithmetic_bit_spec["frac"] + b.arithmetic_bit_spec["frac"]
			}
		bits = a._bit_width + b._bit_width
		if a.signed and b.signed:
			bits -= 1

		super(MUL, self).__init__(bits, a, b, kind=kind, **kwargs)

	def update(self):
		self.current_value = self.drivers[0]() * self.drivers[1]()

	def get_driver_string(self, t="  ", n=2):
		return t * n + self.name + " = " + self.drivers[0].name + " * " + self.drivers[1].name + ";"

class DIV(Logic):
	def __init__(self, a, b, kind="div", extend_to_match_bits=None, **kwargs):
		if "name" not in kwargs or kwargs["name"] is None:
			kwargs["name"] = self._get_binary_op_name("div", a, b)
		if extend_to_match_bits is not None:
			a = a.get_extended(b._bit_width)
			b = b.get_extended(a._bit_width)
		if a.arithmetic_bit_spec or b.arithmetic_bit_spec:
			warnings.warn("DIV operation does not yet support arithmetic_bit_spec")
		super(DIV, self).__init__(max(a._bit_width, b._bit_width), a, b, kind=kind, **kwargs)

	def update(self):
		self.current_value = self.drivers[0]() * self.drivers[1]()

	def get_driver_string(self, t="  ", n=2):
		return t * n + self.name + " = " + self.drivers[0].name + " / " + self.drivers[1].name + ";"

# Instantiates a soft-logic fp24 add IP (used on the Stratix 10 NX)
# Can be used with or without pipelining
# Don't recommend using this for larger designs, use the TensorAdd module instead 
add_instance_num = 0
class FP24_ADD(Logic):
	def __init__(self, a, b, clock, reset=None, kind="fp24_add", extend_to_match_bits=None, **kwargs):
		self.clock = clock
		self.reset = reset

		self.has_reg = False
		if clock != None:
			self.has_reg = True

		assert(a._bit_width == 24)
		assert(a._bit_width == b._bit_width)
		super(FP24_ADD, self).__init__(a._bit_width, a, b, kind=kind, **kwargs)

	def get_driver_string(self, t="  ", n=2):
		global add_instance_num

		fp24_add_string = "#" #So hpipe knows not to use an assign

		if self.has_reg:
			#Create instance of fp24 add IP
			fp24_add_string += t * n + "fp24_add_hw_reg fp24_add_inst_" + str(add_instance_num) 
			#hook up inputs and outputs
			reset_name = "1'b0"
			if self.reset != None: reset_name = self.reset.name

			fp24_add_string += "(.clk(" + self.clock.name + "), .reset(" + reset_name + "),"
			fp24_add_string += " .fp24_input_a(" + self.drivers[0].name +"), .fp24_input_b(" + self.drivers[1].name + "), .fp24_output(" + self.name + "));"
		else:
			exit(1)
			#Create instance of fp24 add IP
			fp24_add_string += t * n + "fp24_add_hw fp24_add_inst_" + str(add_instance_num) 
			#hook up inputs and outputs
			fp24_add_string += "(.fp24_input_a(" + self.drivers[0].name +"), .fp24_input_b(" + self.drivers[1].name + "), .fp24_output(" + self.name + "));"


		add_instance_num += 1

		return fp24_add_string

class MOD(Logic):
	def __init__(self, a, b, kind="mod", extend_to_match_bits=None, **kwargs):
		if "name" not in kwargs or kwargs["name"] is None:
			kwargs["name"] = self._get_binary_op_name("mod", a, b)
		if extend_to_match_bits is not None:
			a = a.get_extended(b._bit_width)
			b = b.get_extended(a._bit_width)
		if a.arithmetic_bit_spec or b.arithmetic_bit_spec:
			warnings.warn("DIV operation does not yet support arithmetic_bit_spec")
		super(MOD, self).__init__(b._bit_width, a, b, kind=kind, **kwargs)

	def update(self):
		self.current_value = self.drivers[0]() * self.drivers[1]()

	def get_driver_string(self, t="  ", n=2):
		return t * n + self.name + " = " + self.drivers[0].name + " % " + self.drivers[1].name + ";"

class AND(ReductionOp):
	def __init__(self, *drivers, kind="and", **kwargs):
		if "name" not in kwargs or kwargs["name"] is None:
			kwargs["name"] = self._get_binary_op_name("and", drivers[0], drivers[1])
		super(AND, self).__init__("&&", *drivers, kind=kind, **kwargs)

	def update(self):
		self.current_value = True
		for d in self.drivers:
			self.current_value = self.current_value and d()

class OR(ReductionOp):
	def __init__(self, *drivers, kind="or", **kwargs):
		super(OR, self).__init__("||", *drivers, kind=kind, **kwargs)

	def update(self):
		self.current_value = False
		for d in self.drivers:
			self.current_value = self.current_value or d()

class BOR(ReductionOp):
	def __init__(self, *drivers, kind="bor", **kwargs):
		if len(drivers) == 1:
			bits=1
		else:
			bits = drivers[0]._bit_width
		super(BOR, self).__init__("|", *drivers, kind=kind, supports_unary_op=True, reduction_output_bits=bits, **kwargs)

class BAND(ReductionOp):
	def __init__(self, *drivers, kind="band", **kwargs):
		if len(drivers) == 1:
			bits=1
		else:
			bits = drivers[0]._bit_width
		super(BAND, self).__init__("&", *drivers, kind=kind, supports_unary_op=True, reduction_output_bits=bits, **kwargs)

# Right shift the first driver by an arbitrary value
class RightShift(ReductionOp):
	def __init__(self, *drivers, kind="right_shift", **kwargs):
		assert(len(drivers) == 2)

		bits = max(drivers[0]._bit_width, drivers[1]._bit_width)
		super(RightShift, self).__init__(">>", *drivers, kind=kind, supports_unary_op=True, reduction_output_bits=bits, **kwargs)

class XOR(ReductionOp):
	def __init__(self, *drivers, kind="xor", **kwargs):
		if len(drivers) == 1:
			bits=1
		else:
			bits = drivers[0]._bit_width
		super(XOR, self).__init__("^", *drivers, kind=kind, supports_unary_op=True, reduction_output_bits=bits, **kwargs)

class INV(ReductionOp):
	def __init__(self, driver, kind="inv", **kwargs):
		super(INV, self).__init__("~", driver, kind=kind, supports_unary_op=True,  **kwargs)

class EQ(ReductionOp):
	def __init__(self, a, b, kind="eq", **kwargs):
		super(EQ, self).__init__("==", a, b, kind=kind, **kwargs)

	def update(self):
		self.current_value = self.drivers[0]() == self.drivers[1]()

class LT(ReductionOp):
	def __init__(self, a, b, kind="lt", **kwargs):
		super(LT, self).__init__("<", a, b, kind=kind, **kwargs)

	def update(self):
		self.current_value = self.drivers[0]() < self.drivers[1]()

class LE(ReductionOp):
	def __init__(self, a, b, kind="le", **kwargs):
		super(LE, self).__init__("<=", a, b, kind=kind, **kwargs)

	def update(self):
		self.current_value = self.drivers[0]() <= self.drivers[1]()

class GT(ReductionOp):
	def __init__(self, a, b, kind="gt", **kwargs):
		super(GT, self).__init__(">", a, b, kind=kind, **kwargs)

	def update(self):
		self.current_value = self.drivers[0]() > self.drivers[1]()

class GE(ReductionOp):
	def __init__(self, a, b, kind="ge", **kwargs):
		super(GE, self).__init__(">=", a, b, kind=kind, **kwargs)

	def update(self):
		self.current_value = self.drivers[0]() >= self.drivers[1]()

class NOT(Logic):
	def __init__(self, a, kind="not", **kwargs):
		if "bits" in kwargs:
			del kwargs["bits"]
		super(NOT, self).__init__(1, a, kind=kind, **kwargs)

	def update(self):
		self.current_value = not self.drivers[0]()

	def get_driver_string(self, t="  ", n=2):
		return t * n + self.name + " = !" + self.drivers[0].name + ";"


class Mux(Logic):
	"""
	This is deprecated.  Please use dl.MuxV2 going forward.  It infers the number of bits from its drivers
	"""
	def __init__(self, bits, *inputs, kind="mux", extend_to_match_bits=None, **kwargs):
		if extend_to_match_bits is not None:
			partial_inputs = list(inputs)[1:]
			max_bit_width = max([d._bit_width for d in partial_inputs])
			partial_inputs = [d.get_extended(max_bit_width) for d in partial_inputs]
			inputs = [inputs[0]] + partial_inputs
		super(Mux, self).__init__(bits, *inputs, kind=kind, assignment_type="complex", **kwargs)
		self.options = lambda: self.drivers[1:]
		self.select = lambda: self.drivers[0]

	def set_inputs(select, *options):
		self.set_drivers(select, *options)

	def update(self):
		self.current_value = self.options()[self.select()()]()

	def get_driver_string(self, t="  ", n=2):
		string = t * n + "case (" + self.select().name + ")\n"
		n += 1
		for i,s in list(enumerate(self.options()))[1:]:
			string += t * n + str(self.select()._bit_width) + "'d" + str(i) + ": " + self.name + " = " + s.name + ";\n"
		string += t * n + "default: " + self.name + " = " + self.options()[0].name + ";\n"
		n -= 1
		string += t * n + "endcase"
		return string

class MuxV2(Logic):
	def __init__(self, *inputs, bits=None, kind="mux", extend_to_match_bits=None, **kwargs):
		if extend_to_match_bits is not None:
			partial_inputs = list(inputs)[1:]
			max_bit_width = max([d._bit_width for d in partial_inputs if hasattr(d, "_bit_width")])
			for i, d in enumerate(partial_inputs):
				if type(d) is int:
					partial_inputs[i] = Verilog.current_module.m.inst(Constant, bits=max_bit_width, value=d)
			partial_inputs = [d.get_extended(max_bit_width) for d in partial_inputs]
			inputs = [inputs[0]] + partial_inputs
		super(MuxV2, self).__init__(bits, *inputs, kind=kind, assignment_type="complex", **kwargs)
		self.options = lambda: self.drivers[1:]
		self.select = lambda: self.drivers[0]
		if len(inputs) > 1:
			self.set_bit_width(self.options()[0]._bit_width)

	def set_inputs(select, *options):
		self.set_drivers(select, *options)
		self.set_bit_width(self.options()[0]._bit_width)

	def update(self):
		self.current_value = self.options()[self.select()()]()

	def get_driver_string(self, t="  ", n=2):
		string = t * n + "case (" + self.select().name + ")\n"
		n += 1
		for i,s in list(enumerate(self.options()))[1:]:
			string += t * n + str(self.select()._bit_width) + "'d" + str(i) + ": " + self.name + " = " + s.name + ";\n"
		string += t * n + "default: " + self.name + " = " + self.options()[0].name + ";\n"
		n -= 1
		string += t * n + "endcase"
		return string

class OneHotMux(Logic):
	def __init__(self, selects, values, bits=None, kind="one_hot_mux", extend_to_match_bits=None, **kwargs):
		if extend_to_match_bits is not None:
			values = list(values)
			max_bit_width = max([d._bit_width for d in values])
			values = [d.get_extended(max_bit_width) for d in values]
		super(OneHotMux, self).__init__(bits, *values, *selects, kind=kind, assignment_type="complex", **kwargs)
		self.options = lambda: self.drivers[:len(values)]
		self.selects = lambda: self.drivers[len(values):len(values) + len(selects)]
		self.set_bit_width(self.options()[0]._bit_width)

	def get_driver_string(self, t="  ", n=2):
		string = t * n + "case (1'b1)\n"
		n += 1
		for i,s in list(enumerate(self.options()))[1:]:
			string += t * n + self.selects()[i].name + ": " + self.name + " = " + s.name + ";\n"
		string += t * n + "default: " + self.name + " = " + self.options()[0].name + ";\n"
		n -= 1
		string += t * n + "endcase"
		return string

# Selects a particular output for the mux depending on which position the leading one is in. Note that the options must be in reverse order
# This uses casez and ? as wildcards then lets Quartus optimize it (should create log2(bits) muxes in the critical path)
class LeadingOneMux(Logic):
	def __init__(self, *inputs, bits=None, kind="leading_one_mux", extend_to_match_bits=None, **kwargs):
		if extend_to_match_bits is not None:
			partial_inputs = list(inputs)[1:]
			max_bit_width = max([d._bit_width for d in partial_inputs if hasattr(d, "_bit_width")])
			for i, d in enumerate(partial_inputs):
				if type(d) is int:
					partial_inputs[i] = Verilog.current_module.m.inst(Constant, bits=max_bit_width, value=d)
			partial_inputs = [d.get_extended(max_bit_width) for d in partial_inputs]
			inputs = [inputs[0]] + partial_inputs
		super(LeadingOneMux, self).__init__(bits, *inputs, kind=kind, assignment_type="complex", **kwargs)
		self.options = lambda: self.drivers[1:]
		self.select = lambda: self.drivers[0]
		if len(inputs) > 1:
			self.set_bit_width(self.options()[0]._bit_width)
		self.bits = self.select()._bit_width

		assert(len(list(enumerate(self.options()))) == (bits+1))

	#Turns a particular option into a binary string
	#Note that the first index (0) in this case will be 1????... the second 01???... the third 001???... etc.
	def get_binary_string(self, int_num):

		bit_string = ""
		bit_encountered = False
		for i in range(self.bits):
			if i == int_num:
				bit_encountered = True
				bit_string += '1'
			elif bit_encountered:
				bit_string += '?'
			else:
				bit_string += '0'

		return bit_string

	def get_driver_string(self, t="  ", n=2):
		string = t * n + "casez (" + self.select().name + ")\n"

		#string = t * n + "casez (" + self.select().name + ") // synthesis parallel_case\n"
		n += 1
		for i,s in list(enumerate(self.options()))[:-1]:
			string += t * n + str(self.select()._bit_width) + "'b" + self.get_binary_string(i) + ": " + self.name + " = " + s.name + ";\n"
		string += t * n + "default: " + self.name + " = " + self.options()[-1].name + ";\n"
		n -= 1
		string += t * n + "endcase"		

		return string



class ContentAddressableROM(Logic):
	def __init__(self, content_list, kind="content_addressable_rom", **kwargs):
		super(ContentAddressableROM, self).__init__(1, kind=kind, assignment_type="complex", **kwargs)
		self.content_list = content_list

	def update(self):
		if self.drivers[0]() in self.content_list:
			self.current_value = 1
		else:
			self.current_value = 0

	def get_driver_string(self, t="  ", n=2):
		string = t * n + "case (" + self.drivers[0].name + ")\n"
		n += 1
		for c in self.content_list:
			string += t * n + str(self.drivers[0]._bit_width) + "'d" + str(c) + ": " + self.name + " = 1'b1;\n"
		string += t * n + "default: " + self.name + " = 1'b0;\n"
		n -= 1
		string += t * n + "endcase"
		return string

class StateMachine(Module):
	"""

	A state machine.

	NOTE: DO NOT USE NAMED ARGUMENTS TO INSTANTIATE THE STATE MACHINE
	For some reason if you instantiate one like this:
	::
		# WRONG, do not do
		state_machine = inst(dl.StateMachine,
			states=["STATE_1", "STATE_2"],
			edges=[
				["STATE_1", "STATE_2", go_to_state_2],
				["STATE_2", "STATE_1", go_to_state_1],
			],
			control=None)

	Python will spew errors.  Instead, instantiate it like this:
	::
		# CORRECT, do do
		state_machine = inst(dl.StateMachine,
			["STATE_1", "STATE_2"], # states
			[ # edges
				["STATE_1", "STATE_2", go_to_state_2],
				["STATE_2", "STATE_1", go_to_state_1],
			],
			None) # control


	Args:
		states (list):
			a list of state names e.g. ``["STATE_1", "STATE_2"]``.  The first state
			in the list will be the default state, and the state that is set during
			reset.

		edges (list):
			a list of lists e.g. ``[["FROM_STATE_NAME", "TO_STATE_NAME", condition]]``
			The first element in the inner list should be the name of the state
			from which the edge will leave, and the second is the name of the state
			to which the edge will take the state machine.  The final value
			is optional and it is the condition under which the state transition should
			occur.

		control (dict):
			``None``, or a dictionary of STATE_NAMES to dictionaries of control signal
			names that map to a value for that state. The default value for all control
			signals is zero, but you can set a different default value with 
			``state_machine_inst.set_default_for_control("name", value)``.
			Usually I just pass ``None`` instead of this control dict because
			the state machine will automatically create signals indicating whether
			the state machine is in a particular state.

	Attributes:
		c (dict):
			A dictionary of all the control signals driven by the state machine.
			These will include the ones you specified in the ``control`` argument.
			If you passed ``None`` it will just have some automatically created
			control signals that indicate which state it is	currently in.
			these are called ``is_<state_name_in_lowercase>``.  For example, if we made
			a state machine with states ``[IDLE, PROCESSING]``, the dictionary would
			contain:
			::
				{
					"is_idle" : <1 bit dl.Logic signal>,
					"is_processing" : <1 bit dl.Logic signal>
				}
			You can also access these as attributes of the instantiated state machine,
			e.g. state_machine.is_idle, state_machine.is_processing, etc.
	"""
	def __init__(self, states, edges, control, clock, reset, **kwargs):
		super(StateMachine, self).__init__("state_machine", kind="state_machine", **kwargs)
		inst = self.inst
		with ModuleScope(self):
			inst(PhantomFlop, clock=clock, reset=reset)
			clock.add_sink(self)
			self.clock = clock
			self.reset = reset
			self.reset_signal = inst(Logic, bits=1, name="reset")
			self.reset_signal.set_driver(self.reset)
			self.states = states
			self.edges = edges
			for e in self.edges:
				if len(e) >= 3:
					t = inst(Logic, bits=1, assignment_type="complex", name=e[2].name)
					t.set_driver(e[2])
					e[2] = t
			self.control = control
			if self.control is None:
				self.control = {}
			for s in self.states:
				self.control.setdefault(s, {})
				self.control[s]["is_" + s.lower()] = 1
				for c,v in self.control[s].items():
					if isinstance(v, Logic):
						signal = inst(Logic, 1, assignment_type="complex", name=v.name)
						signal.set_driver(v)
						self.control[s][c] = signal


			self.c = {c:None for s,cs in self.control.items() for c,v in cs.items()}
			self.c = {c:inst(Logic, bits=1, name=c, assignment_type="complex") for c in self.c.keys()}
			self.default_values = {c:0 for c in self.c.keys()}
			for c,v in self.c.items():
				setattr(self, c, v)

	def set_default_for_control(self, name, value):
		self.default_values[name] = value

	def get_verilog(self, t="  "):
		verilog = self.get_module_header(t)
		inputs = self.get_inputs()
		outputs = self.get_outputs()
		internal = self.get_internal_signals() + self.clocks
		to_declare = inputs + outputs + internal
		for s in to_declare:
			verilog += t + s.get_declaration_string() + "\n"

		verilog += self.get_combinational_assignments(t=t)

		verilog += t + "typedef enum int unsigned {\n"
		n = 2
		for i,s in enumerate(self.states):
			verilog += t * n + s
			if i != (len(self.states) - 1):
				verilog += ","
			verilog += "\n"
		n = 1
		verilog += t + "} state_t;\n"
		verilog += t + "state_t state, state_next;\n"

		verilog += t + "always @(posedge " + self.clock.name + ") begin\n"
		verilog += t * 2 + "if (" + self.reset_signal.name + ") begin\n"
		verilog += t * 3 +   "state <= " + self.states[0] + ";\n"
		verilog += t * 2 + "end else begin\n"
		verilog += t * 3 +   "state <= state_next;\n"
		verilog += t * 2 + "end\n"
		verilog += t + "end\n\n"


		verilog += t + "always_comb begin // control \n"
		for c,d in self.default_values.items():
			if len(self.c[c].loads) == 0:
				continue

			if multi_chip_enable == True: #To enable simulation of Multiple HPIPE Pieces
				c+=multi_chip_piece #Extend the name to avoid same names

			if multi_chip_enable == True: #To enable simulation of Multiple HPIPE Pieces
				c+=multi_chip_piece #Extend the name to avoid same names

			verilog += t * 2 + c + " = " + "1'b" + str(d) + ";\n"
		verilog += t * 2 + "case (state)\n"
		for s in self.states:
			verilog += t * 2 + s + ": begin\n"
			for c,v in self.control[s].items():
				if len(self.c[c].loads) == 0:
					continue

				if multi_chip_enable == True:
					c+=multi_chip_piece

				if isinstance(v, Logic):
					verilog += t * 3 + c + " = " + str(v.name) + ";\n"
				else:
					verilog += t * 3 + c + " = " + "1'b" + str(v) + ";\n"
			verilog += t * 2 + "end\n"
		verilog += t * 2 + "endcase\n"
		verilog += t + "end\n\n"

		verilog += t + "always_comb begin // edges \n"
		verilog += t * 2 + "state_next = state;\n"
		verilog += t * 2 + "case (state)\n"
		for s in self.states:
			verilog += t * 2 + s + ": begin\n"
			for e in self.edges:
				if e[0] == s:
					if len(e) == 3:
						verilog += t * 3 + "if (" + e[2].name + ") state_next = " + e[1] + ";\n"
					else:
						verilog += t * 3 + "state_next = " + e[1] + ";\n"
			verilog += t * 2 + "end\n"
		verilog += t * 2 + "endcase\n"
		verilog += t + "end\n\n"

		verilog += "endmodule // " + self.name
		return verilog

class FIFO(Module):
	def __init__(self, bits, depth, almost_empty=0, almost_full=0, **kwargs):
		super(FIFO, self).__init__("fifo", kind="fifo", **kwargs)
		inst = self.inst

		with ModuleScope(self):

			# Instantiate the main memory for our FIFO
			memory = inst(RAM, bits=bits, depth=depth)

			# Create a read pointer and a write pointer that
			# both start at 0 and count until the last address
			# of the FIFO (depth-1)
			with ParamScope(reset_value=0, increment_value=1, end_value=depth-1):
				read_ptr = inst(Counter, name="read_ptr")
				write_ptr = inst(Counter, name="write_ptr")
				self.read_ptr = read_ptr
				self.write_ptr = write_ptr

			# Connect the read and write pointers to the
			# addresses on the memory
			memory.r_addr.set_driver(read_ptr.current_value)
			memory.w_addr.set_driver(write_ptr.current_value)

			# Create references to the memory's read,
			# write, and data signals so that instantiating
			# modules can easily access them as the
			# FIFO's signals
			self.r_en = memory.r_en
			self.w_en = memory.w_en
			self.r_data = memory.r_data
			self.w_data = memory.w_data

			# Setting this will cause warnings to be
			# printed if any of these signals aren't driven
			self.requires_driver = [self.r_en, self.w_data, self.w_en]

			# Program the read and write pointers to increment
			# whenever the FIFO's read or write signals are
			# asserted
			read_ptr.set_increment_condition(self.r_en)
			write_ptr.set_increment_condition(self.w_en)

			# Create a counter that stores the number of
			# words currently stored in the FIFO.
			# Whenever the FIFO is read, we will
			# decrement this counter, whenever it is
			# written to we will increment it,
			# and if both are asserted in the same
			# cycle it will not change.
			lines_used_counter = inst(UpDownCounter,
				reset_value=0,
				increment_value=1,
				decrement_value=-1,
				end_value=depth,
				name="lines_used_counter")
			self.lines_used_counter = lines_used_counter
			lines_used_counter.set_decrement_condition(self.r_en)
			lines_used_counter.set_increment_condition(self.w_en)

			# Build some signals for the convenience of
			# instantiating modules.  These will assert signals
			# if the FIFO is empty, if it is full, if it is
			# almost empty, or if it is almost full.
			lines_used_bit_width = lines_used_counter.current_value._bit_width
			with ParamScope(bits=lines_used_bit_width):
				with ModuleScope(lines_used_counter):
					self.empty = inst(EQ,
						lines_used_counter.current_value,
						inst(Constant, 0, name="empty_value"))
					self.almost_empty = (lines_used_counter.current_value <=
						inst(Constant, almost_empty, name="almost_empty_value"))
					self.full = inst(EQ,
						lines_used_counter.current_value,
						inst(Constant, depth, name="full_value"))
					self.almost_full = (lines_used_counter.current_value >= 
						inst(Constant, depth-almost_full, name="almost_full_value"))

class PhantomFlop(Flop):
	"""
	In a few internal modules where much of the verilog is custom we use this
	PhantomFlop class to ensure reset and clock signals are propagated to the module.
	"""
	def __init__(self, bits=1, name="phantom_flop", preserve_reset=True, **kwargs):
		super(PhantomFlop, self).__init__(bits=1, **kwargs)
		inst = self.owner.inst
		if preserve_reset:
			self.set_reset_driver(inst(Constant, bits=1, value=0))

class PipelinedMux(Module):
	"""
	"""
	def __init__(self, bits, max_select_bits_per_stage, total_mux_size, **kwargs):
		super(PipelinedMux, self).__init__("pipelined_mux", kind="pipelined_mux", **kwargs)
		inst = self.inst
		with ModuleScope(self):
			self.stages = math.ceil(math.log(total_mux_size, 2**max_select_bits_per_stage))
			self.inputs = [inst(Logic, bits=bits, name="input_" + str(i)) for i in range(total_mux_size)]

			select_bits = clog2(total_mux_size)
			select_bits_per_stage = self._select_bits_per_stage(max_select_bits_per_stage, select_bits)
			self.select_signals = [inst(Logic, bits=b, name="select_for_stage_" + str(i)) for i,b in enumerate(select_bits_per_stage)]
			self.muxes_per_stage = self._muxes_per_stage(select_bits_per_stage, total_mux_size)
			stage_output_flops = [[inst(Flop, bits=bits, name="mux_stage_" + str(i)) for i in range(mps)] for mps in self.muxes_per_stage]
			self.params_to_print = {
				"bits" : bits,
				"max_select_bits_per_stage" : max_select_bits_per_stage,
				"total_mux_size" : total_mux_size,
				"stages" : self.stages,
				"select_bits" : select_bits,
				"select_bits_per_stage" : select_bits_per_stage
			}
			if total_mux_size <= 1:
				self.output = self.inputs[0]
				self.requires_driver = self.inputs
				return

			inputs_and_outputs = [self.inputs] + stage_output_flops
			inputs_and_outputs = [[inputs_and_outputs[i], inputs_and_outputs[i+1]] for i in range(len(inputs_and_outputs) - 1)]

			for io,select_bits in zip(inputs_and_outputs, self.select_signals):
				mux_size = 2 ** select_bits._bit_width
				for i,o in enumerate(io[1]):
					o.set_driver(inst(MuxV2, select_bits, *io[0][i * mux_size: (i + 1) * mux_size]))

			self.output = stage_output_flops[-1][0]

			self.requires_driver = self.inputs + self.select_signals

	def _select_bits_per_stage(self, max_select_bits_per_stage, select_bits):
		remaining_select_bits = select_bits
		select_bits_per_stage = []
		for i in range(self.stages):
			select_bits_per_stage.append(min(remaining_select_bits, max_select_bits_per_stage))
			remaining_select_bits -= select_bits_per_stage[-1]

		return select_bits_per_stage

	def _muxes_per_stage(self, select_bits_per_stage, total_mux_size):
		remaining_mux_size = total_mux_size
		muxes_per_stage = []
		for b in select_bits_per_stage:
			muxes_per_stage.append(math.ceil(remaining_mux_size / (2 ** b)))
			remaining_mux_size = muxes_per_stage[-1]

		return muxes_per_stage

	def get_instance_comment(self, t="  "):
		return v_comment_string("\n".join([k + ": " + str(v) for k,v in self.params_to_print.items()]), t=t)




class RAM(Module):
	"""A RAM
	
	It is ``bits`` wide, and ``depth`` deep.  You can specify if you would like it to be built out of LUTRAMs with ``use_lutram``,
	and specify if you would like to ensure it does not use LUTRAMs with ``allow_lutram``

	Data read by the RAM will be available 1 cycle after the read enable is asserted. For maximum performance we recommend
	you delay the ``r_data`` by at least 1 cycle (if the RAM is bigger or the data will have to travel a long distance, we recommend
	more).

	Atrributes:
		w_en (:class:`Logic`):
			Enable writing

		r_en (:class:`Logic`):
			Enable reading

		w_addr (:class:`Logic`):
			Controls the address to which ``w_data`` will be written while ``w_en`` is asserted

		r_addr (:class:`Logic`):
			Controls the address from which ``r_data`` will be read while ``r_en`` is asserted

		w_data (:class:`Logic`):
			``bits`` wide signal that controls the value written to the RAM while ``w_en`` is asserted

		r_data (:class:`Logic`):
			``bits`` wide signal that has the value read from address ``r_addr`` cycle after ``r_en`` is asserted
	"""
	@classmethod
	def physically_mapped_dims(cls, dim_x, dim_y):
		sizes = [[10, 2048], [20, 1024], [40, 512]]
		lutram_size = [20, 32]
		num_y = []
		remaining_y = dim_y
		num_x = []
		for i,size in enumerate(sizes):
			num_x.append(math.ceil(dim_x / size[0]))
			num_y.append(remaining_y // size[1])
			remaining_y -= size[1] * num_y[-1]
			if (i+1) >= len(sizes) and remaining_y > 64:
				num_y[-1] += 1
				remaining_y -= size[1]

		if remaining_y > 0:
			num_y.append(math.ceil(remaining_y / lutram_size[1]))
			num_x.append(math.ceil(dim_x / lutram_size[0]))
			sizes.append(lutram_size)

		return (num_x, num_y, sizes)

	@classmethod
	def specs_for_depth(cls, depth, is_lutram=False, allow_lutram=True):
		if mlab_limit == 0: initialize_mlab_limit()
		
		#This will work as long as we use it right before initialization 
		if (is_lutram or ((depth <= 64) and allow_lutram and mlab_count <= mlab_limit)):
			ram_specs = [[32, 20]]#, [64, 10]] # quartus doesn't seem to like to use 64 deep MLABs very often
		else:
			ram_specs = [[512, 40], [1024, 20], [2048, 10]]
		return ram_specs

	@classmethod
	def size_for_depth(cls, depth, **kwargs):
		ram_specs = RAM.specs_for_depth(depth, **kwargs)
		for rs in ram_specs:
			ram_spec = rs
			if rs[0] >= depth:
				break
		return ram_spec

	@classmethod
	def tile_size_for_dims(cls, depth, width, **kwargs):
		ram_spec = RAM.size_for_depth(depth, **kwargs)
		x_count = math.ceil(width / ram_spec[1])
		y_count = math.ceil(depth / ram_spec[0])
		return x_count * y_count

	@classmethod
	def tile_depth_for_specs(cls, depth, **kwargs):
		ram_spec = RAM.size_for_depth(depth, **kwargs)
		y_count = math.ceil(depth / ram_spec[0])
		return y_count

	def __init__(self,
		bits,
		depth,
		use_lutram=False,
		clock=None,
		read_clock=None,
		write_clock=None,
		allow_lutram=True,
		implement_with_megafunction=False,
		register_output=False,
		enable_force_to_zero=False,
		dont_allow_x_write=True,
		kind="ram",
		**kwargs):
		assert(clock is not None or (read_clock is not None and write_clock is not None))
		if read_clock is None or write_clock is None:
			read_clock = clock
			write_clock = clock
		super(RAM, self).__init__("ram", kind=kind, **kwargs)
		kwargs = self.kwargs
		inst = self.inst
		self.depth = depth
		self.has_fanout_control = False
		self.enable_force_to_zero = enable_force_to_zero
		self.dont_allow_x_write = dont_allow_x_write

		#temp remove
		self.flag = False

		global mlab_count, m20k_count
		if mlab_limit == 0: initialize_mlab_limit()

		self.implement_with_megafunction = implement_with_megafunction
		if (((depth > 2048) or (bits > 10 and depth > 1024) or (bits > 20 and depth > 512) or (bits > 40)) or (depth <= 64.0 and allow_lutram and mlab_count <= mlab_limit) or (use_lutram)):
			self.implement_with_megafunction = False

		self.register_output = register_output
		if register_output:
			self.model_delay = 2
		else:
			self.model_delay = 1

		self.is_mlab = False

		#In case we run out of MLABs (mainly occurs on NX) use M20ks instead
		if depth <= 64 and allow_lutram:
			if mlab_count <= mlab_limit:
				self.is_mlab = True
				if depth <= 32:
					num_labs = math.ceil(bits/20.0)
				else:
					num_labs = math.ceil(bits/10.0)
				mlab_count += num_labs
			else:
				m20k_count += 1 #count extra M20Ks to print the correct value at the end

		with ModuleScope(self):
			inst(PhantomFlop, clock=read_clock, preserve_reset=False)
			inst(PhantomFlop, clock=write_clock, preserve_reset=False)
			read_clock.add_sink(self)
			write_clock.add_sink(self)
			self.allow_lutram = allow_lutram
			self.read_clock = read_clock
			self.write_clock = write_clock
			self.addr_width = max(clog2(depth), 1)
			with ParamScope(bits=1):
				self.w_en = inst(Logic, name="w_en")
				self.r_en = inst(Logic, name="r_en")
			with ParamScope(bits=self.addr_width):
				self.w_addr = inst(Logic, name="w_addr")
				self.r_addr = inst(Logic, name="r_addr")
			with ParamScope(bits=bits):
				self.w_data = inst(Logic, name="w_data")
				self.r_data = inst(InternallyDrivenSignal, signed=False, name="r_data")

			self.r_addr_d1 = None
			self.is_lutram = use_lutram
			if self.is_lutram:
				warnings.warn("LUTRAM python model does not accurately model reading from and writing to the same address in the same cycle")
				warnings.warn("LUTRAM generated verilog works, but the python model adds in a cycle to read that doesn't exist in the verilog")
			else:
				warnings.warn("Python model does not accurately model read delays for BRAMs")
				if self.is_mlab:
					self.r_addr_d1 = inst(Flop, bits=self.addr_width, name="r_addr")

			self.requires_driver = [self.w_en, self.w_addr, self.r_addr, self.w_data]
			if not use_lutram and not self.is_mlab:
				self.requires_driver.append(self.r_en)
			self.requires_load = [self.r_data]

	@property
	def ram_specs(self):
		return RAM.specs_for_depth(self.depth, self.is_lutram, self.allow_lutram)

	@property
	def ram_size(self):
		return RAM.size_for_depth(self.depth, is_lutram=self.is_lutram, allow_lutram=self.allow_lutram)

	@property
	def tile_estimate(self):
		return RAM.tile_size_for_dims(self.depth, self.r_data._bit_width, is_lutram=self.is_lutram, allow_lutram=self.allow_lutram)

	@property
	def tile_depth_estimate(self):
		return RAM.tile_depth_for_specs(self.depth, is_lutram=self.is_lutram, allow_lutram=self.allow_lutram)

	def get_read_clock(self):
		return self.read_clock
	def get_write_clock(self):
		return self.write_clock
	def get_clock(self):
		return self.read_clock

	def get_verilog(self, t="  "):
		self.r_addr.signed = False
		self.w_addr.signed = False
		verilog = self.get_module_header(t)
		inputs = self.get_inputs()
		outputs = self.get_outputs()
		internal = self.get_internal_signals() + self.clocks
		to_declare = inputs + outputs + internal
		for s in to_declare:
			verilog += t + s.get_declaration_string() + "\n"

		if self.implement_with_megafunction:
			verilog += self._megafunction_string(t)
		else:
			verilog += self._functional_model_string(t)
		verilog += "endmodule // " + self.name
		return verilog

	def _functional_model_string(self, t="  "):
		verilog = ""
		if self.register_output:
			r_data_name = "internal_out_data_reg"
			verilog += t + "reg ["+str(self.r_data._bit_width-1)+":0] internal_out_data_reg;\n"
			if self.enable_force_to_zero:
				verilog += t + "reg sclear;\n"
		else:
			r_data_name = self.r_data.name


		prefix = '(* ramstyle = "M20K, no_rw_check" *) '
		if self.is_lutram or self.is_mlab:
			prefix = '(* ramstyle = "MLAB, no_rw_check" *) '

		verilog += t + prefix + "reg [" + str(self.r_data._bit_width - 1) + ":0] mem[" + str(self.depth) + "];\n"

		verilog += self.get_combinational_assignments(t=t)

		verilog += t + "always @(posedge " + self.get_write_clock().name + ") begin\n"
		if self.is_lutram:
			verilog += 2*t + "if (" + self.w_en.name + ")\n"
			verilog += 3*t + "mem[" + self.w_addr.name + "] = " + self.w_data.name + ";\n"
		else:
			verilog += 2*t + "if (" + self.w_en.name + ")\n"
			verilog += 3*t + "mem[" + self.w_addr.name + "] <= " + self.w_data.name + ";\n"

		if self.dont_allow_x_write:
			module_path = ".".join([o.name for o in reversed([self] + self.get_owner_list()[:-1])])
			verilog += "`ifdef DONT_ALLOW_X_WRITE_TO_RAM\n"
			verilog += t + "if (" + self.w_en.name + " === 1'b1 && ^" + self.w_data.name + " === 1'bX) begin\n"
			verilog += 2 * t + '$display("X written to RAM ' + module_path + " at time %0t.  Will exit because DONT_ALLOW_X_WRITE_TO_RAM is set." + '", $time);\n'
			verilog += 2 * t + "$finish;\n"
			verilog += t + "end\n"
			verilog += "`endif // DONT_ALLOW_X_WRITE_TO_RAM\n"
		verilog += t + "end\n"

		verilog += t + "always @(posedge " + self.get_read_clock().name + ") begin\n"
		if self.register_output:
			if self.enable_force_to_zero:
				verilog += 2 * t + "sclear <= !" + self.r_en.name + ";\n"
				verilog += 2 * t + "if (sclear) begin\n"
				verilog += 3 * t + self.r_data.name + " <= {" + str(self.r_data._bit_width) + "{1'b0}};\n"
				verilog += 2 * t + "end else begin\n"
				verilog += 3 * t + self.r_data.name + " <= " + r_data_name + ";\n"
				verilog += 2 * t + "end\n"
			else:
				verilog += 2 * t + self.r_data.name + " <= " + r_data_name + ";\n"
		read_address = self.r_addr
		if not self.is_lutram and self.is_mlab:
			verilog += 2*t + self.r_addr_d1.name + " <= " + self.r_addr.name + ";\n"
			read_address = self.r_addr_d1
		elif not self.is_lutram:
			have_r_en = len(self.r_en.drivers) > 0 and not (self.is_lutram or self.is_mlab)
			n = 2
			if have_r_en:
				verilog += 2*t + "if (" + self.r_en.name + ") begin\n"
				n = 3
			verilog += n*t + r_data_name + " <= mem[" + self.r_addr.name + "];\n"
			if have_r_en:
				verilog += 2*t + "end\n"
			if have_r_en and self.enable_force_to_zero:
				verilog += 2*t + "else begin\n"
				verilog += n*t + r_data_name + " <= {" + str(self.r_data._bit_width) + "{1'b0}};\n"
				verilog += 2*t + "end\n"

		verilog += t + "end\n"

		if self.is_lutram or self.is_mlab:
			verilog += t + "always @(*) begin\n"
			verilog += 2*t + r_data_name + " = mem[" + read_address.name + "];\n"
			verilog += t + "end\n"

		return verilog

	def _megafunction_string(self, t="  "):

		rd_width = self.r_data._bit_width
		ra_width = self.r_addr._bit_width
		depth = math.ceil(self.depth / 2) * 2
		ren_name = self.r_en.name
		raddr_name = self.r_addr.name
		rdata_name = self.r_data.name
		if self.is_lutram:
			rdclk = "CLOCK0"
			rdclk_name = "1'b1"
		elif isinstance(self, ROM) or (self.get_read_clock().name == self.get_write_clock().name):
			rdclk = "CLOCK0"
			rdclk_name = "1'b1"
		else:
			rdclk = "CLOCK1"
			rdclk_name = self.get_read_clock().name
		if self.is_lutram or not self.register_output:
			outdata_reg_a = "UNREGISTERED"
		else:
			outdata_reg_a = rdclk

		built_with_lutrams = self.is_lutram or self.is_mlab
		if built_with_lutrams:
			ram_block_type = "MLAB"
			enable_force_to_zero = "FALSE"
		else:
			ram_block_type = "M20K"
			enable_force_to_zero = "TRUE"

		if self.enable_force_to_zero and built_with_lutrams and not self.is_lutram:
			outdata_sclr_a = "SCLEAR"
			sclr_name = "sclr"
			build_sclr_string = f"""
	reg sclr;
	always @(posedge {rdclk_name}) begin
		sclr <= !{ren_name};
	end
"""
		else:
			sclr_name = "1'b0"
			outdata_sclr_a = "NONE"
			build_sclr_string = ""

		instance_name = self.name + "_i"
		if isinstance(self, ROM):
			write_params_string = ""
			write_ports_string = ""
			wdata_name = "1'b1"
			wen_name = "1'b0"
			wrclk_name = self.get_read_clock().name
			operation_mode = "ROM"
			init_file_string = (instance_name + '.init_file = "' +
				self.rom_path_modifier+'memory_initialization_files/' + self.name + '.mif",')
		else:
			wd_width = self.w_data._bit_width
			wa_width = self.w_addr._bit_width
			wdata_name = self.w_data.name
			wen_name = self.w_en.name
			waddr_name = self.w_addr.name
			wrclk_name = self.get_write_clock().name
			operation_mode = "DUAL_PORT"
			init_file_string = ""
			write_ports_string = f"""
                .address_a ({waddr_name}),"""
			write_params_string = f"""
        {instance_name}.widthad_a  = {wa_width},
        {instance_name}.width_a  = {wd_width},
        {instance_name}.numwords_a  = {depth},"""

		verilog = ""
		"""if self.register_output:
			rdata_name = "internal_out_data_reg"
			verilog += t + "reg ["+str(self.r_data._bit_width-1)+":0] internal_out_data_reg;\n"
			if self.enable_force_to_zero:
				verilog += t + "reg sclear;\n"
		
			register_output_string = f
	always @(posedge {rdclk_name}) begin
		{self.r_data.name} <= {rdata_name};
	end"""
		#else:
		register_output_string = ""
		rdata_name = self.r_data.name

		verilog += self.get_combinational_assignments(t=t)
		verilog += f"""
{register_output_string}
{build_sclr_string}
	altera_syncram  {instance_name} (
                .address_b ({raddr_name}),{write_ports_string}
                .clock0 ({wrclk_name}),
                .data_a ({wdata_name}),
                .rden_b ({ren_name}),
                .wren_a ({wen_name}),
                .q_b ({rdata_name}),
                .aclr0 (1'b0),
                .aclr1 (1'b0),
                .address2_a (1'b1),
                .address2_b (1'b1),
                .addressstall_a (1'b0),
                .addressstall_b (1'b0),
                .byteena_a (1'b1),
                .byteena_b (1'b1),
                .clock1 ({rdclk_name}),
                .clocken0 (1'b1),
                .clocken1 (1'b1),
                .clocken2 (1'b1),
                .clocken3 (1'b1),
                .data_b ({{{rd_width}{{1'b1}}}}),
                .eccencbypass (1'b0),
                .eccencparity (8'b0),
                .eccstatus (),
                .q_a (),
                .rden_a (1'b0),
                .sclr ({sclr_name}),
                .wren_b (1'b0));
    defparam
        {init_file_string}
        {instance_name}.address_aclr_b  = "NONE",
        {instance_name}.address_reg_b  = "{rdclk}",
        {instance_name}.clock_enable_input_a  = "BYPASS",
        {instance_name}.clock_enable_input_b  = "BYPASS",
        {instance_name}.clock_enable_output_b  = "BYPASS",
        {instance_name}.enable_ecc  = "FALSE",
        {instance_name}.enable_force_to_zero  = "{enable_force_to_zero}",
        {instance_name}.intended_device_family  = "Stratix 10",
        {instance_name}.lpm_type  = "altera_syncram",
        {instance_name}.numwords_b  = {depth},
        {instance_name}.operation_mode  = "{operation_mode}",
        {instance_name}.outdata_aclr_b  = "NONE",
        {instance_name}.outdata_sclr_b  = "{outdata_sclr_a}",
        {instance_name}.outdata_reg_b  = "{outdata_reg_a}",
        {instance_name}.power_up_uninitialized  = "FALSE",
        {instance_name}.ram_block_type  = "{ram_block_type}",
        {instance_name}.rdcontrol_reg_b  = "{rdclk}",
        {instance_name}.read_during_write_mode_mixed_ports  = "DONT_CARE",
        {instance_name}.widthad_b  = {ra_width},
        {instance_name}.width_b  = {rd_width},{write_params_string}
        {instance_name}.width_byteena_a  = 1;
"""
		return verilog


class ROM(RAM):
	"""A ROM

	content_list can either be a list of integers or a :class:`digitallogic.utils.BitArray` object.
	"""
	def __init__(self, clock, bits, content_list, use_lutram=False, kind="rom", rom_path_modifier="", enable_force_to_zero=False, implement_with_megafunction=False, **kwargs):
		super(ROM, self).__init__(bits=bits, depth=len(content_list), clock=clock, use_lutram=use_lutram, kind=kind, **kwargs)
		self.implement_with_megafunction = implement_with_megafunction
		self.enable_force_to_zero = enable_force_to_zero
		kwargs = self.kwargs
		inst = self.inst
		if type(content_list) is not BitArray:
			content_list = BitArray(bits=bits, value_vector=content_list)
		self.contents = content_list
		self.w_en.set_driver(inst(Constant, 0, bits=1))
		self.rom_path_modifier = rom_path_modifier

		if mlab_limit == 0: initialize_mlab_limit()

		#In case we run out of MLABs (mainly occurs on NX) use M20ks instead
		self.is_mlab = False
		global mlab_count, m20k_count
		if self.is_lutram or (self.allow_lutram and len(self.contents) <= 64 and not enable_force_to_zero):
			if mlab_count <= mlab_limit:
				self.is_mlab = True
				#For keeping track of how many MLABs we have used so we don't run out	
				if len(self.contents) <= 32:
					num_labs = math.ceil(self.r_data._bit_width/20.0)
				else:
					num_labs = math.ceil(self.r_data._bit_width/10.0)
				mlab_count += num_labs
			else:
				m20k_count += 1 #count extra M20Ks to print the correct value at the end

		for i in [self.w_en, self.w_addr, self.w_data]:
			del self.requires_driver[self.requires_driver.index(i)]

	def _use_bit_str(self):
		# using this removes a few linting warnings, but it pretty substantially increases the
		# build time for large RAMs
		return False
		#return (self.r_data._bit_width % 4) != 0

	def get_verilog(self, t="  "):
		if self.implement_with_megafunction:
			return super(ROM, self).get_verilog(t)
		verilog = self.get_module_header(t)
		inputs = self.get_inputs()
		outputs = self.get_outputs()
		internal = self.get_internal_signals() + self.clocks
		to_declare = inputs + outputs + internal
		for s in to_declare:
			verilog += t + s.get_declaration_string() + "\n"

		if self.register_output:
			r_data_name = "internal_out_data_reg"
			verilog += t + "reg ["+str(self.r_data._bit_width-1)+":0] internal_out_data_reg;\n"
			if self.enable_force_to_zero:
				verilog += t + "reg sclear;\n"
		else:
			r_data_name = self.r_data.name

		# changes for LUTRAM	
		prefix = '(* ramstyle = "M20K, no_rw_check" *) '
		sensitivity_list = "posedge " + self.get_clock().name
		assignement_str = " <= "
		if self.is_lutram or self.is_mlab:
			prefix = '(* ramstyle = "MLAB, no_rw_check" *) '
			build_with_lutram = True
		else:
			build_with_lutram = False
		if self.is_lutram:
			assignement_str = " = "
			sensitivity_list = "*"

		suffix = ""

		#Note: this filepath will not work as-is
		if self.implement_with_megafunction:
			suffix = '/* synthesis ram_init_file = "' + self.rom_path_modifier+'memory_initialization_files/' + self.name + '.mif" */'

		verilog += t + prefix + "reg [" + str(self.r_data._bit_width - 1) + ":0] rom[" + str(len(self.contents)) + "]" + suffix + ";\n"
		mem_file_type = "h"
		if self._use_bit_str():
			mem_file_type = "b"

		if not self.implement_with_megafunction:
			verilog += t + f'initial $readmem{mem_file_type}("'+self.rom_path_modifier+'memory_initialization_files/' + self.name + '.mem", rom);\n'

		verilog += self.get_combinational_assignments(t=t)

		verilog += t + "always @(" + sensitivity_list + ") begin\n"
		n = 2
		have_r_en = len(self.r_en.drivers) > 0 and not (self.is_lutram or self.is_mlab)
		if have_r_en:
			verilog += t * n + "if (" + self.r_en.name + ") begin\n"
			n += 1
		verilog += t * n + r_data_name + assignement_str + " rom[" + self.r_addr.name + "];\n"
		if have_r_en:
			verilog += t * 2 + "end\n"
		if have_r_en and self.enable_force_to_zero:
			verilog += 2 * t + "else begin\n"
			verilog += t * n + r_data_name + assignement_str + " {" + str(self.r_data._bit_width) + "{1'b0}};\n"
			verilog += 2 * t + "end\n"

		if self.register_output:
			n = 2
			if self.enable_force_to_zero:
				verilog += 2 * t + "sclear <= !" + self.r_en.name + ";\n"
				verilog += n * t + "if (sclear) begin\n"
				verilog += 3 * t + self.r_data.name + " <= {" + str(self.r_data._bit_width) + "{1'b0}};\n"
				verilog += 2 * t + "end\n"
				verilog += 2 * t + "else begin\n"
				n = 3
			verilog += n * t + self.r_data.name + " <= " + r_data_name + ";\n"
			if self.enable_force_to_zero:
				verilog += 2 * t + "end\n"


		"""
		verilog += t * n + "case (" + self.r_addr.name + ")\n"
		n += 1
		for a,c in enumerate(self.contents):
			verilog += t * n + str(self.addr_width) + "'d" + str(a) + ": " + self.r_data.name + assignement_str  + v_hex_str(c, self.r_data._bit_width) + ";\n"
		verilog += t * n + "default: " + self.r_data.name + assignement_str + str(self.r_data._bit_width) + "'d0;\n"
		n -= 1
		verilog += t * n + "endcase\n"
		"""
		verilog += t + "end\n"
		verilog += "endmodule // " + self.name
		return verilog

	def get_mem_data_string(self):
		if self._use_bit_str():
			strs = self.contents.get_bit_strs()
		else:
			strs = self.contents.get_hex_strs()
		str_out = ""
		for s in strs:
			str_out += s + "\n"
		return str_out

	def get_instance_comment(self, t="  "):
		params_to_print = {
			"depth" : len(self.contents),
			"bits" : self.r_data._bit_width,
			"r_addr_width" : self.r_addr._bit_width}
		return v_comment_string("\n".join([k + ": " + str(v) for k,v in params_to_print.items()]), t=t)

	def _megafunction_string(self, t="  "):

		rd_width = self.r_data._bit_width
		ra_width = self.r_addr._bit_width
		depth = math.ceil(self.depth / 2) * 2
		ren_name = self.r_en.name
		raddr_name = self.r_addr.name
		rdata_name = self.r_data.name
		if self.is_lutram:
			rdclk = "NONE"
			rdclk_name = "1'b1"
		elif isinstance(self, ROM) or (self.get_read_clock().name == self.get_write_clock().name):
			rdclk = "CLOCK0"
			rdclk_name = "1'b1"
		else:
			rdclk = "CLOCK1"
			rdclk_name = self.get_read_clock().name
		if self.is_lutram or not self.register_output:
			outdata_reg_a = "UNREGISTERED"
		else:
			outdata_reg_a = rdclk

		built_with_lutrams = self.is_lutram or self.is_mlab
		if built_with_lutrams:
			ram_block_type = "MLAB"
			enable_force_to_zero = "FALSE"
		else:
			ram_block_type = "M20K"
			enable_force_to_zero = "TRUE"

		if self.enable_force_to_zero and built_with_lutrams and not self.is_lutram:
			outdata_sclr_a = "SCLEAR"
			sclr_name = "sclr"
			build_sclr_string = f"""
	reg sclr;
	always @(posedge {rdclk_name}) begin
		sclr <= !{ren_name};
	end
"""
		else:
			sclr_name = "1'b0"
			outdata_sclr_a = "NONE"
			build_sclr_string = ""

		instance_name = self.name + "_i"
		if isinstance(self, ROM):
			write_params_string = ""
			write_ports_string = ""
			wdata_name = "1'b1"
			wen_name = "1'b0"
			wrclk_name = self.get_read_clock().name
			operation_mode = "ROM"
			init_file_string = (instance_name + '.init_file = "' +
				self.rom_path_modifier+'memory_initialization_files/' + self.name + '.mif",')
		else:
			wd_width = self.w_data._bit_width
			wa_width = self.w_addr._bit_width
			wdata_name = self.w_data.name
			wen_name = self.w_en.name
			waddr_name = self.w_addr.name
			wrclk_name = self.get_write_clock().name
			operation_mode = "DUAL_PORT"
			init_file_string = ""
			write_ports_string = f"""
                .address_b ({waddr_name}),"""
			write_params_string = f"""
        {instance_name}.widthad_b  = {wa_width},
        {instance_name}.width_b  = {wd_width},
        {instance_name}.numwords_b  = {depth},"""

		verilog = self.get_combinational_assignments(t=t)

		verilog += f"""
{build_sclr_string}
	altera_syncram  {instance_name} (
                .address_a ({raddr_name}),{write_ports_string}
                .clock0 ({wrclk_name}),
                .data_b ({wdata_name}),
                .rden_a ({ren_name}),
                .wren_b ({wen_name}),
                .q_a ({rdata_name}),
                .aclr0 (1'b0),
                .aclr1 (1'b0),
                .address2_a (1'b1),
                .address2_b (1'b1),
                .addressstall_a (1'b0),
                .addressstall_b (1'b0),
                .byteena_a (1'b1),
                .byteena_b (1'b1),
                .clock1 ({rdclk_name}),
                .clocken0 (1'b1),
                .clocken1 (1'b1),
                .clocken2 (1'b1),
                .clocken3 (1'b1),
                .data_a ({{{rd_width}{{1'b1}}}}),
                .eccencbypass (1'b0),
                .eccencparity (8'b0),
                .eccstatus (),
                .q_b (),
                .rden_b (1'b1),
                .sclr ({sclr_name}),
                .wren_a (1'b0));
    defparam
        {init_file_string}
        {instance_name}.address_aclr_a  = "NONE",
//        {instance_name}.address_reg_a  = "{rdclk}",
        {instance_name}.clock_enable_input_a  = "BYPASS",
        {instance_name}.clock_enable_input_b  = "BYPASS",
        {instance_name}.clock_enable_output_a  = "BYPASS",
        {instance_name}.enable_ecc  = "FALSE",
        {instance_name}.enable_force_to_zero  = "{enable_force_to_zero}",
        {instance_name}.intended_device_family  = "Stratix 10",
        {instance_name}.lpm_type  = "altera_syncram",
        {instance_name}.numwords_a  = {depth},
        {instance_name}.operation_mode  = "{operation_mode}",
        {instance_name}.outdata_aclr_a  = "NONE",
        {instance_name}.outdata_sclr_a  = "{outdata_sclr_a}",
        {instance_name}.outdata_reg_a  = "{outdata_reg_a}",
        {instance_name}.power_up_uninitialized  = "FALSE",
        {instance_name}.ram_block_type  = "{ram_block_type}",
//        {instance_name}.rdcontrol_reg_a  = "{rdclk}",
        {instance_name}.read_during_write_mode_mixed_ports  = "DONT_CARE",
        {instance_name}.widthad_a  = {ra_width},
        {instance_name}.width_a  = {rd_width},{write_params_string}
        {instance_name}.width_byteena_a  = 1;
"""
		return verilog

class ModuloROM(Module):
	"""docstring for ModuloROM"""
	def __init__(self, min_value, max_value, mod_value, kind="modulo_rom", **kwargs):
		super(ModuloROM, self).__init__("modulo_rom", kind=kind, **kwargs)
		inst = self.inst
		content_list = [i % mod_value for i in range(min_value, max_value+1)]
		with ModuleScope(self):
			self.input = inst(Logic, bits=clog2(len(content_list)), name="input_value")
			self.input_valid = inst(Logic, bits=1, name="input_valid")
			rom = inst(ROM, content_list=content_list, bits=clog2(mod_value))
			rom.r_addr.set_driver(self.input + inst(Constant, value=-min_value, signed=True, name="min_value"))
			rom.r_en.set_driver(self.input_valid)
			self.output = rom.r_data

			self.requires_driver = [self.input, self.input_valid]

		
#	def __call__(self, level=None):
#		if level is None:
#			return self.current_read_data
#		if level == 1:
#
#		else:
#			pass

class BigROM(Module):
	"""
	I wouldn't recommend using this.  It solves the fanout problem described below, 
	but it does a poor job of being efficient with memory bits.
	
	Quartus doesn't do a good job of fanning out signals that control big RAM blocks
	even if you give it enough pipeline stages (it just does the entire fanout
	in the last stage) to facilitate higher frequencies we do our own basic
	RAM mapping so that we can customize the fanout of the address and read enables
	"""
	def __init__(self, bits, content_list, control_delay, read_delay, kind="big_rom", **kwargs):
		super(BigROM, self).__init__("big_rom", kind=kind, **kwargs)
		inst = self.inst
		with ModuleScope(self):
			individual_rom_depth, individual_rom_width, n_rams_depth, n_rams_width = BigROM.get_mem_sizes(
				len(content_list), bits)
			input_address_bits = clog2(len(content_list))
			individual_rom_address_bits = clog2(individual_rom_depth)
			self.r_addr = inst(Logic, bits=input_address_bits, name="r_addr")
			self.r_en = inst(Logic, bits=1, name="r_en")
			if input_address_bits > individual_rom_address_bits:
				with ParamScope(name="chip_select_bits"):
					chip_select_bits = self.r_addr[input_address_bits-1:individual_rom_address_bits]
				chip_selects = [inst(AND, self.r_en,
					inst(EQ, chip_select_bits, inst(Constant, bits=chip_select_bits._bit_width, value=i), name="cs_"+str(i))) for i in range(n_rams_depth)]
			else:
				chip_select_bits = 0
				chip_selects = [self.r_en]
			with ParamScope(name="read_address"):
				read_address = self.r_addr[individual_rom_address_bits-1:0]

			roms = []
			for i in range(n_rams_depth):
				roms.append([])
				for j in range(n_rams_width):
					lines_in_ram = min(individual_rom_depth * (i+1), len(content_list)) - individual_rom_depth * i
					hi = min((j+1) * individual_rom_width, bits)
					lo = j * individual_rom_width
					min_line = individual_rom_depth * i
					max_line = min_line + lines_in_ram
					contents = content_list[min_line:max_line, hi-1:lo]
					roms[-1].append(inst(ROM, bits=hi-lo, content_list=contents))
				cs_fanout_pipe = inst(PipelinedFanout,
					chip_selects[i],
					*[r.r_en for r in roms[-1]],
					levels=control_delay,
					name="cs_" + str(i) + "_fanout_pipe")
			r_addr_fanout_pipe = inst(PipelinedFanout,
				read_address,
				*[r.r_addr for rl in roms for r in rl],
				levels=control_delay,
				explicitly_shrink_bitwidths=True,
				name="r_addr_fanout_pipe")

			r_data = [inst(Concat, *[r.r_data for r in rl], name="read_data").delay(read_delay-1) for rl in roms]
			if chip_select_bits != 0:
				self.r_data = inst(MuxV2, chip_select_bits.delay(control_delay + read_delay), *r_data)
			else:
				self.r_data = r_data[0]

	@staticmethod
	def get_mem_sizes(depth, width):
		individual_rom_depth = 2 ** min(11, max(clog2(depth), 9))
		individual_rom_width = 10 * 2**(11 - clog2(individual_rom_depth))
		if depth > 2048:
			individual_rom_depth = 2 ** min(14, clog2(depth))
			individual_rom_width = 2 ** (14 - clog2(individual_rom_depth))
		n_rams_depth = math.ceil(depth / individual_rom_depth)
		n_rams_width = math.ceil(width / individual_rom_width)
		return (individual_rom_depth, individual_rom_width, n_rams_depth, n_rams_width)


	@staticmethod
	def get_m20k_count(depth, width):
		_, _, n_rams_depth, n_rams_width = BigROM.get_mem_sizes(depth, width)
		return n_rams_depth * n_rams_width






class MultiControllerFIFO(Module):
	def __init__(self, bits, almost_empty=0, almost_full=0, depth=32, kind="multi_controller_fifo", **kwargs):
		super(MultiControllerFIFO, self).__init__("multi_controller_fifo", kind=kind, **kwargs)
		inst = self.inst
		with ModuleScope(self):
			self.r_en = inst(Logic, bits=1, name="r_en")
			self.w_en = inst(Logic, bits=1, name="w_en")
			self.w_ens = []
			self.r_data = inst(Logic, bits=bits, name="r_data")
			self.w_data = inst(Logic, bits=bits, name="w_data")
			self.requires_driver = [self.r_en, self.w_data, self.w_en]
			num_fifos = math.ceil(bits / 20.)
			fifos = [inst(FIFO, bits=20, depth=depth,
				almost_empty=almost_empty, almost_full=almost_full) for _ in range(num_fifos)]
			self.fifos = fifos
			self.empty = fifos[0].empty
			self.almost_empty = fifos[0].almost_empty
			self.full = fifos[0].full
			self.almost_full = fifos[0].almost_full
			in_data = self.w_data
			if (num_fifos * 20) > bits:
				padding_bits = (num_fifos * 20) - bits
				padding_zeros = inst(Constant, value=0, bits=padding_bits)
				in_data = inst(Concat, self.w_data, padding_zeros, name="padded_in_data")
			out_data = []
			for i,f in enumerate(fifos):
				lo = i * 20
				hi = (i + 1) * 20 - 1
				din = in_data[hi:lo]
				f.w_data.set_driver(din)
				f.w_en.set_driver(self.w_en)
				self.w_ens.append(f.w_en)
				f.r_en.set_driver(self.r_en)
				out_data.append(f.r_data)
			out_data_padded = inst(Concat, *out_data, name="out_data_padded")
			self.r_data.set_driver(out_data_padded[bits-1:0])

class SelectedRingROM(Module):
	def __init__(self, bits, content_list, kind="selected_ring_rom", **kwargs):
		super(SelectedRingROM, self).__init__("selected_ring_rom", kind=kind, **kwargs)
		inst = self.inst
		with ModuleScope(self):
			self.r_data = inst(Logic, bits=bits, name="r_data")
			self.r_en = inst(Logic, bits=1, name="r_en")
			self.ready = inst(Logic, bits=1, name="ready")

			if True:#len(content_list) > 1024:
				rom = inst(FIFORingROM, bits=bits, content_list=content_list)
				self.ready.set_driver(rom.ready)
			else:
				rom = inst(RingROM, bits=bits, content_list=content_list)
				self.ready = inst(Constant, bits=1, value=1)

			self.rom = rom
			rom.r_en.set_driver(self.r_en)
			self.r_data.set_driver(rom.r_data)


class RingROM(Module):
	def __init__(self, bits, content_list, kind="ring_rom", **kwargs):
		super(RingROM, self).__init__("ring_rom", kind=kind, **kwargs)
		inst = self.inst
		with ModuleScope(self):
			self.r_data = inst(Logic, bits=bits, name="r_data")
			self.r_en = inst(Logic, bits=1, name="r_en")
			rom = inst(ROM, bits=bits, content_list=content_list)
			r_addr = inst(Counter,
				reset_value=0,
				increment_value=1,
				end_value=len(content_list)-1,
				name="r_addr_counter")
			rom.r_en.set_driver(self.r_en)
			rom.r_addr.set_driver(r_addr.current_value)
			r_addr.set_increment_condition(self.r_en)
			self.r_data.set_driver(rom.r_data)


class FIFORingROM(Module):
	def __init__(self, bits, content_list, kind="fifo_ring_rom", **kwargs):
		super(FIFORingROM, self).__init__("fifo_ring_rom", kind=kind, **kwargs)
		inst = self.inst
		with ModuleScope(self):
			self.r_data = inst(Logic, bits=bits, name="r_data")
			self.r_en = inst(Logic, bits=1, name="r_en")
			self.ready = inst(Logic, bits=1, name="ready")
			_, _, bram_count_depth, bram_count_width = BigROM.get_mem_sizes(len(content_list), bits)
			bram_count = bram_count_depth * bram_count_width
			fanout_levels = math.ceil(math.log(bram_count, 4))
			read_delay = math.ceil(math.log(bram_count_depth, 2)) + 4

			r_addr = inst(Counter,
				reset_value=0,
				increment_value=1,
				end_value=len(content_list)-1,
				name="r_addr_counter")
			rom_r_en = inst(Logic, bits=1, name="rom_r_en")
			rom = inst(ROM, bits=bits, content_list=content_list)
			#rom.r_en.set_driver(rom_r_en)
			#rom.r_addr.set_driver(r_addr.current_value)

			fifo_depth = 32
			control_to_write_delay = fanout_levels * 2 + read_delay + 2
			if control_to_write_delay >= fifo_depth:
				fifo_depth = 64
				if control_to_write_delay >= fifo_depth:
					fifo_depth = 512
			fifo = inst(MultiControllerFIFO,
				bits=bits, almost_full=control_to_write_delay-1,
				almost_empty=control_to_write_delay)
			fifo.r_en.set_driver(self.r_en)
			self.fifo = fifo

			#with ParamScope(synthesis_attributes="synthesis maxfan=4"):
			fanned_out_r_addr = r_addr.current_value.delay(fanout_levels, final_fan=bram_count, max_fan=4)
			fanned_out_r_en = rom_r_en.delay(fanout_levels, final_fan=bram_count, max_fan=4)
			rom.r_en.set_driver(fanned_out_r_en)
			rom.r_addr.set_driver(fanned_out_r_addr)
			fifo.w_data.set_driver(rom.r_data.delay(read_delay-1))#rom.r_data.delay(fanout_levels))
			we_fanout_stages = math.ceil(math.log(len(fifo.w_ens), 8))
			inst(PipelinedFanout,
				rom_r_en.delay(fanout_levels+read_delay-we_fanout_stages),
				*fifo.w_ens,
				levels=we_fanout_stages,
				name="fifo_write_enable_fanout_pipe"
				)
			fifo.w_en.set_driver(inst(Constant, bits=1, value=0))
			self.r_data.set_driver(fifo.r_data)

			read_states = ["WAITING_FOR_SPACE_IN_FIFO", "READING"]
			edges = [["WAITING_FOR_SPACE_IN_FIFO", "READING", fifo.almost_empty],
				["READING", "WAITING_FOR_SPACE_IN_FIFO", fifo.almost_full]]
			read_state_machine = inst(StateMachine, read_states, edges, None)

			rom_r_en.set_driver(read_state_machine.c["is_reading"])
			r_addr.set_increment_condition(read_state_machine.c["is_reading"])
			bottom_fifo = fifo.fifos[0]
			with ModuleScope(bottom_fifo):
				lines_used_bits = bottom_fifo.lines_used_counter._bit_width
				elements_for_ready = inst(Constant, bits=lines_used_bits, value=5, name="elements_for_ready")
				self.ready.set_driver(bottom_fifo.lines_used_counter.current_value > elements_for_ready, delay=1)

			self.requires_driver = [self.r_en]


class HyperPipe(Module):
	def __init__(self, bits, max_depth, clock, kind="hyperpipe", **kwargs):
		super(HyperPipe, self).__init__("hyperpipe", kind=kind, **kwargs)
		inst = self.inst

		with ModuleScope(self):
			with ParamScope(bits=bits):
				self.d = inst(Logic, bits=bits, name="din")
				self.q = inst(Logic, bits=bits, name="dout")
				self.max_depth = max_depth
				self.bits = bits
				inst(PhantomFlop, clock=clock)
				self.clock = clock

				assert(max_depth <= 100)

	def get_parameter_declaration_string(self, t="  "):
		"""
		If I took the time I could probably figure out what on earth
		  MAX_PIPE_STR is actually computing, but it just seemed easier
		  to use their code.
		"""
		return """
  // Converting {} to string so it can be used as a string when setting altera_attribute
  localparam MAX_PIPE_STR = {{(({} / 100) % 10) + 8'd48, (({} / 10) % 10) + 8'd48, ({} % 10) + 8'd48}};
		""".format(*[str(self.max_depth) for _ in range(4)])

	def get_custom_verilog_str(self, t="  "):
		return """
  (* altera_attribute = {{"-name ADV_NETLIST_OPT_ALLOWED NEVER_ALLOW; -name HYPER_RETIMER_ADD_PIPELINING ", MAX_PIPE_STR}} *)
    reg [{}-1:0] vlat_r /* synthesis preserve */;
  always @ (posedge {}) begin
    vlat_r <= {};
  end
  assign {} = vlat_r;
		""".format(self.bits, self.clock.name, self.d.name, self.q.name)



class MemoryShiftRegister(Module):
	"""
	This is a shift register that is ``bits`` bits wide, and ``depth`` stages.

	Attributes:
		d:
			This is the first stage of the shift register.  Drive it to drive the rest of
			the shift register.

		q:
			This is the output of the shift register. Use it to drive things with the
			output of the shift register.

	"""
	def __init__(self, bits, depth, kind="memory_shift_register", **kwargs):
		module = self
		super(MemoryShiftRegister, self).__init__("memory_shift_register", kind=kind, **kwargs)
		inst = self.inst

		with ModuleScope(module):
			output_registers = 1
			#if depth > 32:
			#	output_registers = 1

			self.delay = depth
			ram_depth = depth
			self.ram_depth = ram_depth
			# - 1 because the last address is 1 less than the depth
			w_addr_start = ram_depth - 1
			r_addr_start = 0

			self.w_addr_start = w_addr_start
			self.r_addr_start = r_addr_start

			with ParamScope(bits=bits):
				r_addr_counter = inst(ContinuousCounter,
					reset_value=r_addr_start,
					increment_value=1,
					end_reset_value=0,
					end_value=ram_depth-1,
					name="r_addr_counter")

				w_addr_counter = inst(ContinuousCounter,
					reset_value=w_addr_start,
					increment_value=1,
					end_reset_value=0,
					end_value=ram_depth-1,
					name="w_addr_counter")

				ram = inst(RAM, depth=ram_depth, dont_allow_x_write=False)
				ram.w_en.set_driver(inst(Constant, bits=1, value=1))
				ram.r_en.set_driver(inst(Constant, bits=1, value=1))
				ram.w_addr.set_driver(w_addr_counter.current_value.delay(1))
				ram.r_addr.set_driver(r_addr_counter.current_value)
				self.addr_width = ram.w_addr._bit_width
				self.q = ram.r_data.delay(output_registers)
				self.d = ram.w_data

				self.requires_driver = [self.d]

	def get_instance_comment(self, t="  "):
		comment = "RAM Depth: " + str(self.ram_depth) + "\n"
		comment += "Delay: " + str(self.delay) + "\n"
		comment += "Address Width: " + str(self.addr_width) + "\n"
		comment += "Write Address Start: " + str(self.w_addr_start) + "\n"
		comment += "Read Address Start: " + str(self.r_addr_start) + "\n"
		return v_comment_string(comment, t)


class BasicShiftRegister(Module):
	"""
	This is a shift register that is ``bits`` bits wide, and ``depth`` stages.

	Attributes:
		d:
			This is the first stage of the shift register.  Drive it to drive the rest of
			the shift register.

		q:
			This is the output of the shift register. Use it to drive things with the
			output of the shift register.

	"""
	def __init__(self, bits, depth, kind="basic_shift_register", **kwargs):
		module = self
		if "override_parent" in kwargs:
			module = kwargs["override_parent"]
			inst = module.inst
		else:
			super(BasicShiftRegister, self).__init__("basic_shift_register", kind=kind, **kwargs)
			inst = self.inst

		with ModuleScope(module):
			with ParamScope(bits=None):
				previous = inst(Logic, bits)
				self.sr = []
				self.d = previous
				for i in range(depth):
					self.sr.append(inst(Flop, name="sr_" + str(i), bits=bits))
					self.sr[-1].set_driver(previous)
					previous = self.sr[-1]
				self.q = previous

class ShiftRegister(Module):
	"""
	This is a shift register that is ``bits`` bits wide, and ``depth`` stages and has
	``should_shift`` signals that control when the shift register shifts (essentially
	clock enables).  They have a default driver that is a constant 1, but you can set
	a new driver to control when it shifts.

	Attributes:
		d:
			This is the first stage of the shift register.  Drive it to drive the rest of
			the shift register.

		q:
			This is the output of the shift register. Use it to drive things with the
			output of the shift register.

		sr (list):
			This has all of the stages in the shift register, so you can access
			more than one stage if you want.

		should_shift:
			A single signal that controls whether the shift register should shift.
			if you want to manually control the fanout to these stages, use
			``should_shifts`` instead.

		should_shifts (list):
			Drive these signals to control when each stage of the shift register shifts
			independently (usually you want them to be all in sync, so this is mostly
			useful if the shift register has many stages and you want to control the
			fanout of the should_shift signal).

	"""
	def __init__(self, bits, depth, kind="shift_register", **kwargs):
		super(ShiftRegister, self).__init__("shift_register", kind=kind, **kwargs)
		inst = self.inst

		with ModuleScope(self):
			with ParamScope(bits=None):
				previous = inst(Logic, bits)
				self.sr = []
				self.should_shift = inst(Logic, 1)
				self.should_shift.set_driver(inst(Constant, 1, bits=1, signed=False))
				self.d = previous
				self.should_shifts = [inst(Logic, 1, name="should_shift") for _ in range(depth)]
				for i in range(depth):
					self.should_shifts[i].set_driver(self.should_shift)
					self.sr.append(inst(Flop, name="sr_" + str(i), bits=bits))
					mux = inst(Mux, bits, self.should_shifts[i], self.sr[-1], previous)
					self.sr[-1].set_driver(mux)
					previous = self.sr[-1]
				self.q = previous

class ParallelLoadShiftRegister(Module):
	"""
	This is a shift register that is ``bits`` bits wide, and ``depth`` stages and has
	``shifts`` signals that control when the shift register shifts (essentially
	clock enables).  It also has ``loads`` that allow you to specify when each
	register is loaded.  By default, the shift register shifts whenever it is
	not being loaded, but you can override this behaviour by driving ``shifts`` manually.

	Attributes:
		input_data (list):
			Drive these with whatever data should be loaded when ``loads`` are asserted.

		sr (list):
			This has all of the stages in the shift register, so you can access
			all of the stages to get the output data you want.

		shifts (list):
			Signals that control when the shift register should shift.  By default these
			are driven by a constant 1, so the shift register always shifts unless
			it is being loaded. (loading takes precedence over shifting)

		loads (list):
			Drive these high whenever you need to load new data into the registers in the shift register.

	"""
	def __init__(self, bits, depth, kind="parallel_load_shift_register", **kwargs):
		super(ParallelLoadShiftRegister, self).__init__("parallel_load_shift_register", kind=kind, **kwargs)
		inst = self.inst

		with ModuleScope(self):
			with ParamScope(bits=None):
				self.sr = []
				self.input_data = [inst(Logic, bits, name="input_data") for _ in range(depth)]
				self.loads = [inst(Logic, 1, name="load") for _ in range(depth)]
				self.shifts = [inst(Logic, 1, name="shift") for _ in range(depth)]
				default_shift = inst(Constant, bits=1, value=1, name="default_shift_always")
				for s in self.shifts:
					s.set_driver(default_shift)
				previous = inst(Constant, value=0, bits=bits, name="zeros")
				for i in range(depth):
					self.sr.append(inst(Flop, name="sr_" + str(i), bits=bits))
					select = inst(Concat, self.shifts[i], self.loads[i], name="select")
					mux = inst(MuxV2, select, self.sr[-1], previous, self.input_data[i], self.input_data[i])
					self.sr[-1].set_driver(mux)
					previous = self.sr[-1]

				self.requires_driver = self.input_data + self.loads

class FakePipelinedFanout(Module):
	"""
	dl.FakePipelinedFanout

	NOTE: It seems I made these statements prematurely.  I'd recommend we still use 
	:class:`PipelinedFanout`.  The quartus implementation isn't quite as good.

	Quartus 18.1 supported automatic duplication of registers driving high
	fanout nets, pushing some of the loading from the final stage into the prior
	stage of the pipeline
	::
		                               |-R
		        |-R                    |-R
		        |-R              |-Rd--|-R
		        |-R              |
		        |-R              |     |-R
		C---R1--|-R  Becomes C---|-R1--|-R
		        |-R              |     |-R
		        |-R              |
		        |-R              |-Rd--|-R
		        |-R                    |-R
		                               |-R

	Where Rd are duplicates of R1.  Unfortunately, for really high fanout
	nets this didn't go far enough, because it only distributed fanout across
	two pipeline stages.  To get around this limitation, I built a manual pipeline
	fanout generator (:class:`PipelinedFanout`) to manually instantiate registers that
	distribute the fanout across many stages.

	Quartus 19.3 now supports automatic duplication of registers in multiple pipeline stages,
	so this module is intended to replace :class:`PipelinedFanout` and allow Quartus to do its thang.

	Args:
		_from (:class:`Logic`): Pipeline Driver
		_to (:class:`Logic`): Logic to drive with the output of the pipeline
		levels (int): the number of flop stages
		max_fanout (int): the maximum fanout allowed
		explicitly_shrink_bitwidths (bool): explicitly selects fewer bits from instantiated
			driving registers when driving signals in ``_to``.  This is useful if some of the
			driven signals have different bitwidths than ``_from``.  For example, if you had
			an address being fanned out to a series of RAMs with different depths, then some
			of the addresses in ``_to`` might have fewer bits.  To prevent superfluous warnings
			you can set ``explicitly_shrink_bitwidths=True``
	"""
	def __init__(self, _from, *_to, levels=None, max_fanout=None, explicitly_shrink_bitwidths=False, **kwargs):
		super(FakePipelinedFanout, self).__init__("fake_pipelined_fanout", kind="fake_pipelined_fanout", **kwargs)
		inst = self.inst

		with ModuleScope(self):
			assert(levels is not None or max_fanout is not None)
			assert(levels is None or max_fanout is None)
			if levels is None:
				levels = math.ceil(math.log(len(_to), max_fanout))
			else:
				if levels == 0:
					max_fanout = 1
				else:
					max_fanout = math.ceil(len(_to) ** (1./levels))
			if max_fanout <= 1:
				max_fanout = 2

			self.levels = levels
			self.max_fanout = max_fanout

			sr_q = _from.delay(levels, max_fan=max_fanout, final_fan=len(_to), final_fan_of_1=True)

			for t in _to:
				if explicitly_shrink_bitwidths:
					t.set_driver(sr_q[t._bit_width-1:0])
				else:
					t.set_driver(sr_q)



	def get_levels(self):
		return self.levels

	def get_added_delay(self):
		#if self.levels == 0:
		#	return 0
		return self.levels

	def get_instance_comment(self, t="  "):
		comment = "Added delay: " + str(self.get_added_delay()) + "\n"
		comment += "Max fanout: " + str(self.max_fanout) + "\n"
		return v_comment_string(comment, t)

	def get_module_attribute_str(self):
		return 'altera_attribute = "-name AUTO_SHIFT_REGISTER_RECOGNITION OFF"'


class PipelinedFanout(Module):
	"""
	dl.PipelinedFanout

	Generates a tree of flops to limit the fanout of a signal from ``_from`` to
	``_to`` signals.

	You can either specify the number of flop stages in the tree
	and have the module compute the maximum fanout, or you can pass in the
	maximum fanout and have the module compute the number of stages

	Quartus 18.1 supported automatic duplication of registers driving high
	fanout nets, pushing some of the loading from the final stage into the prior
	stage of the pipeline
	::
		                               |-R
		        |-R                    |-R
		        |-R              |-Rd--|-R
		        |-R              |
		        |-R              |     |-R
		C---R1--|-R  Becomes C---|-R1--|-R
		        |-R              |     |-R
		        |-R              |
		        |-R              |-Rd--|-R
		        |-R                    |-R
		                               |-R

	Where Rd are duplicates of R1.  Unfortunately, for really high fanout
	nets this didn't go far enough, because it only distributed fanout across
	two pipeline stages.  To get around this limitation, I built a manual pipeline
	fanout generator (:class:`PipelinedFanout`) to manually instantiate registers that
	distribute the fanout across many stages.

	Args:
		_from (:class:`Logic`): Pipeline Driver
		_to (:class:`Logic`): Logic to drive with the output of the pipeline
		levels (int): the number of flop stages
		max_fanout (int): the maximum fanout allowed
		explicitly_shrink_bitwidths (bool): explicitly selects fewer bits from instantiated
			driving registers when driving signals in ``_to``.  This is useful if some of the
			driven signals have different bitwidths than ``_from``.  For example, if you had
			an address being fanned out to a series of RAMs with different depths, then some
			of the addresses in ``_to`` might have fewer bits.  To prevent superfluous warnings
			you can set ``explicitly_shrink_bitwidths=True``
	"""
	def __init__(self, _from, *_to, levels=None, max_fanout=None, explicitly_shrink_bitwidths=False, **kwargs):
		super(PipelinedFanout, self).__init__("pipelined_fanout", kind="pipelined_fanout", **kwargs)
		inst = self.inst

		with ModuleScope(self):
			assert(levels is not None or max_fanout is not None)
			assert(levels is None or max_fanout is None)
			if levels is None:
				levels = math.ceil(math.log(len(_to), max_fanout))
			else:
				if levels == 0:
					max_fanout = 1
				else:
					max_fanout = math.ceil(len(_to) ** (1./levels))
			self.levels = levels
			self.max_fanout = max_fanout

			flop_lists = []
			for i in range(levels):
				flop_lists.append([])
				for j in range(max_fanout ** (i+1)):
					flop_lists[i].append(inst(Flop,
						name="flop_%d_%d" % (i,j),
						bits=_from._bit_width,
						signed=_from.signed,
						synthesis_attributes="synthesis dont_merge"))

			if levels > 0:
				for i in range(max_fanout):
					flop_lists[0][i].set_driver(_from)

			for i in range(1,levels):
				for j in range(max_fanout ** (i + 1)):
					flop_lists[i][j].set_driver(flop_lists[i-1][j//max_fanout])
			for i,t in enumerate(_to):
				if levels > 0:
					driver = flop_lists[-1][i]
				else:
					driver = _from
				#if driver._bit_width > t._bit_width:
				#	t.set_driver(driver[t._bit_width-1:0])
				#else:
				l = inst(Logic, bits=t._bit_width, name=t.name)
				t.set_driver(l)
				if explicitly_shrink_bitwidths:
					l.set_driver(driver[t._bit_width-1:0])
				else:
					l.set_driver(driver)

	def get_levels(self):
		return self.levels

	def get_added_delay(self):
		#if self.levels == 0:
		#	return 0
		return self.levels

	def get_instance_comment(self, t="  "):
		comment = "Added delay: " + str(self.get_added_delay()) + "\n"
		comment += "Max fanout: " + str(self.max_fanout) + "\n"
		return v_comment_string(comment, t)

	def get_module_attribute_str(self):
		return 'altera_attribute = "-name AUTO_SHIFT_REGISTER_RECOGNITION OFF"'


class ShiftResyncFanout(Module):
	def __init__(self, _from, *_to, levels=None, max_fanout=None, explicitly_shrink_bitwidths=False, **kwargs):
		super(ShiftResyncFanout, self).__init__("pipelined_fanout", kind="pipelined_fanout", **kwargs)
		inst = self.inst

		with ModuleScope(self):
			assert(levels is not None or max_fanout is not None)
			assert(levels is None or max_fanout is None)
			if levels is None:
				assert(max_fanout > 0)
				levels = math.ceil(len(_to) / max_fanout)
			else:
				assert(levels >= 0)
				if levels == 0:
					max_fanout = len(_to)
				else:
					max_fanout = math.ceil((len(_to) / levels))
			self.levels = levels
			self.max_fanout = max_fanout

			n_shift_registers = math.ceil(len(_to) / float(max_fanout))
			input_sr_depth = n_shift_registers

			input_sr = inst(ShiftRegister, depth=input_sr_depth, bits=_from._bit_width, name="input_sr")
			input_sr.d.set_driver(_from)
			input_sr.set_module_attribute_str('altera_attribute = "-name AUTO_SHIFT_REGISTER_RECOGNITION OFF"')
			mid_fan = []
			for i,r in enumerate(input_sr.sr):
				for j in range(max_fanout):
					mid_fan.append(r.delay(self.levels - (i+1)))

			for i,t in enumerate(_to):
				if explicitly_shrink_bitwidths:
					t.set_driver(mid_fan[i//max_fanout][t._bit_width-1:0])
				else:
					t.set_driver(mid_fan[i//max_fanout])

	def get_levels(self):
		return self.levels

	def get_added_delay(self):
		#if self.levels == 0:
		#	return 0
		return self.levels

	def get_instance_comment(self, t="  "):
		comment = "Added delay: " + str(self.get_added_delay()) + "\n"
		comment += "Max fanout: " + str(self.max_fanout) + "\n"
		return v_comment_string(comment, t)


class LFSR(Module):
	"""
	This is a Fibonacci LFSR that is at most 64 bits long. We use a 65-bit long LFSR counter with minimum taps.
	It uses a primitive polynomial, which means it will cycle over the entire set of possible binary values, generating pseudo-random values.
		Note: Because this is not a Galois LFSR, there is only one new bit per cycle and the remaining bits are shifts from the previous bits.
	The pseudo-random sequences can be used to simulate applications that require non-deterministic behavior (e.g. simulating HBM efficiency)
	"""
	def __init__(self, bits, seed=5724, kind="lfsr", **kwargs):
		super(LFSR, self).__init__("lfsr", kind=kind, **kwargs)
		inst = self.inst
		assert(bits <= 64) #we're only doing 64 bits or less in this simple implementation
		with ModuleScope(self):

			self.lfsr = inst(Flop, bits=65, reset_driver=seed, name="lfsr_register")
			self.random_value = inst(Logic, bits=bits, name="random_value")
			self.lfsr.set_driver(inst(Concat, inst(XOR, self.lfsr[64], self.lfsr[46]), self.lfsr[63:0]))
			self.random_value.set_driver(self.lfsr[64:(64-(bits-1))])
	
class MultiplyAccumulate(Module):
	"""
	MultiplyAccumulate

	This module will either generate a behavioural model of a series of multipliers whose outputs are summed and accumulated while
	``accumulate`` is asserted, or it will use the DSPs/Tensor Blocks to implement a multiply accumulate. 

	If ``implement_with_megafunction`` is ``True`` it will instantiate the multiply accumulate as
	Stratix 10 IP if it supports the features required by the multiply accumulate specified. Otherwise it will either
	instantiate the static NX simulation files, or create a custom MAC on non-NX platforms.
	
	If ``stratix_10_nx`` is ``True`` it will make use of the Stratix 10 NX tensor block 

	For the Stratix 10 NX, the tensor block is used in one of 3 different modes:
	(1) Vector mode is used to implement a standard MAC (e.g. 3d Conv, pointwise conv). Vector mode has 6 8-bit multipliers and
	an accumulator for these multipliers. There is an additional accumulator at the output register that can support either
	a cascade input or an external accumulate signal (i.e. add new value to current value in register), not both. Therefore
	in the case were we need both, an external output register is instantiated.

	(2) Scalar Mode is used to implement layers that have only 1 independent multiplication per output (e.g. depthwise convolutions).
	In scalar mode there are 3 independent 8-bit multipliers. For example, this means that we group together 3 independent depthwise 
	convolutions (i.e. each tensor block produces 3 seperate outputs). Similar to vector mode, is an additional accumulator at each of 
	the 3 outputs that can support either a cascade input or an external accumulate signal (acc_en). Since the chain length is always 1 
	in our case, (no cascade) we just make use of the acc_en signal, although there is future-proof support for an external accumulate register. 

	(3) FP32 accumulate mode is used to pass values onto a new tensor chain when we have to break the chain (if it gets too long, 
	Quartus will fail placement). Note that despite this mode supporting the addition of 2 FP32 numbers, we don't actually make use of this.
	We just want to make use of the fact that we can pass a high precision value down the cascade chain. There is a fixed-point version of this,
	however it doesn;t seem to be supported as of Quartus 20.4 (or 21.2). The only caveat with using this mode is we need to add some exponent 
	to the fixed point number otherwise the tensor block will assume the fp32 value is 0. We also have to make sure exponent doesn't end up being
	all 1s, as that is NaN. The exponent will be discarded later down the chain, as we only make use of the first 25-bits for the final results,
	and we make sure to set bit 29 in the exponent (full fp32 exp is bits 23-30, sign is bit 31, mantissa is bits 0-22)
	
	The inputs are pipelined multiple stages depending on which method is used is an output register where the values are accumulated

	Attributes:
		inputs (list of :class:`Logic`):
			A list of inputs that must be driven.  They are in the order in which multipliers need
			inputs.  So if you have specified a ``parallel_reduction_count`` of 4, then there will
			be two multipliers with 4 inputs with the following order:
			::
				5 -----------|  # 5 is accumulate
				1 --\\        /|-- 0
				     x--   -| |
				2 --/   \\ /  \\|-\\
				3 --\\    + ->[]-|->
				     x--/
				4 --/

		output (:class:`Logic`):
			The accumulated value.

		carry_out (:class:`Logic`):
			This should be called chain_out, and it it is used to connect multiple DSP
			blocks together with dedicated routing in Stratix 10.  If you're not using
			this feature you can ignore it.

		carry_in (:class:`Logic`):
			If using ``carry_out``, drive this signal with the ``carry_out`` from another
			DSP block

	"""
	def __init__(self, input_widths, accumulate_width, parallel_reduction_count,
		kind="multiply_accumulate", implement_with_megafunction=False, stratix_10_nx=False, signed=True,
		external_carry_in=False, load_const=-1, independent_mults=False, **kwargs):
		super(MultiplyAccumulate, self).__init__("multiply_accumulate", kind=kind, **kwargs)
		inst = self.inst
		with ModuleScope(self):
			name_list = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l"]
			def i_width(i):
				nonlocal input_widths
				if type(input_widths) is list:
					return input_widths[i % len(input_widths)]
				return input_widths

			def i_signed(i):
				nonlocal signed
				if type(signed) is list:
					return signed[i % len(signed)]
				return signed
			any_signed = np.array(signed).any()

			self.independent_mults = independent_mults
			self.load_const = load_const
			self.has_external_carry_in = external_carry_in
			self.external_carry_in = inst(Logic, bits=36, name="external_carry_in", signed=any_signed)
			self.carry_in = inst(Logic, bits=64, name="carry_in")
			self.carry_in_default = inst(Constant, bits=64, value=0)
			self.carry_in.set_driver(self.carry_in_default)
			self.implement_with_megafunction = implement_with_megafunction
			self.inputs = [inst(Logic, bits=i_width(i), name=name_list[i], signed=i_signed(i))
			                   for i in range(parallel_reduction_count)]
			input_count = len(self.inputs) //2
			if self.has_external_carry_in:
				self.inputs += [self.external_carry_in]
			self.inputs += [inst(Logic, bits=1, name="accumulate", signed=False)]
			self.default_accumulate = inst(Constant, bits=1, value=0)
			self.inputs[-1].set_driver(self.default_accumulate)
			self.input_pipes = []
			self.input_pipe_depth = 3
			
			var_a_width = 0
			var_b_width = 0
			var_c_width = 0
			
			global final_mult_count
			old_mult_count = final_mult_count
			num = 0
			for i in self.inputs:
				final_mult_count += num % 2
				if var_a_width == 0: var_a_width = i._bit_width
				elif var_b_width == 0: var_b_width = i._bit_width
				elif var_c_width == 0: var_c_width = i._bit_width
					
				self.input_pipes.append(inst(ShiftRegister,
					bits=i._bit_width,
					signed=i.signed,
					depth=self.input_pipe_depth,
					name=i.name + "_input_registers"))
				self.input_pipes[-1].q.signed = i.signed
				self.input_pipes[-1].d.set_driver(i)
				num += 1

			self.multipliers = []
			for i in range(0,parallel_reduction_count,2):
				self.multipliers.append(
					inst(MUL, self.input_pipes[i].q, self.input_pipes[i+1].q, signed=i_signed(i)))
			if self.has_external_carry_in:
				self.multipliers.append(self.input_pipes[parallel_reduction_count].q)
			if load_const == -1:
				zero_in = inst(Constant, 0, bits=accumulate_width)
			else:
				zero_in = inst(Constant, 1 << load_const, bits=accumulate_width)
			self.output = inst(Flop, bits=accumulate_width, name="result", signed=any_signed)
			accumulate_mux = inst(Mux, accumulate_width, self.input_pipes[-1].q, zero_in, self.output)
			reduce_add = inst(ReduceAdd, *self.multipliers, accumulate_mux, self.carry_in, signed=any_signed)
			self.output.set_driver(reduce_add.output[accumulate_width-1:0])
			self.carry_out = inst(Logic, bits=64, name="carry_out")
			self.carry_out.set_driver(reduce_add.output[63:0], delay=1)

			#Can just use a placeholder here since we manually write the Verilofg later anyways
			if self.independent_mults:
				self.independent_outputs = [inst(Logic, bits=accumulate_width, name="mult_out") for i in range(input_count)]
			
			self.dsp_count = 1
			self.mul_stages = 4
			if arch.hpipe_device_arch.arch_name == "S10_NX":
				self.mul_stages = 3
				#For larger multiplications we need to use an Intel IP that instantriates multiple tensor blocks, these cycle estimates are from that IP 
				if not self.has_external_carry_in and (var_a_width > 8 or var_b_width > 8):
					if var_c_width > 1:
						print("ERROR: cannot implement multiply accumulate on Stratix 10 NX with inputs of size " + str(var_a_width) + ", " + str(var_b_width) + " and " + str(var_c_width))
						exit(1)
					
					num_a = (var_a_width - 2) // 7 + 1;
					num_b = (var_b_width - 2) // 7 + 1;
					
					if num_a <= num_b: 
						self.mul_stages = 4 + math.ceil(math.log(((num_a-1)/6+1), 2)) + 1
					else: 
						self.mul_stages = 4 + math.ceil(math.log(((num_b-1)/6+1), 2)) + 1			
			
					#Calculate actual dsp/tensor block usage on S10_NX based on formulas in Intel IP
					total_slices = num_a+num_b-1
					num_tensor = 0
					for i in range(total_slices):
						if i < num_b: min_b = 0
						else: min_b = i - num_b + 1
						
						if i < num_a: max_a = i
						else: max_a = num_a - 1

						local_num = max_a - min_b + 1
						
						num_dsp = (local_num - 1) // 6 + 1
						num_6 = num_dsp*6
						for j in range(num_dsp):
							if (j < num_dsp-1) or (num_6 - local_num != 5):
								num_tensor += 1
					
					self.dsp_count = num_tensor
					print("NOTE: cannot fit multiplier with inputs of sizes " + str(var_a_width) + " and " + str(var_b_width) + " in one tensor block. Using " + str(num_tensor) + " tensor blocks with a latency of " + str(self.mul_stages) + " cycles.")
			
			global final_dsp_count
			if self.has_external_carry_in: self.dsp_count += 1
			final_dsp_count += self.dsp_count
			

	def get_input_pipe_stages(self):
		return self.input_pipe_depth

	def get_verilog(self, t="  "):
		global enable_sim_module_replace
		
		if arch.hpipe_device_arch.arch_name != "S10_NX":
			if (not self.implement_with_megafunction or 
				len(self.inputs) > 5 or 
				(not self.has_external_carry_in and np.array([i._bit_width > 27 for i in self.inputs]).any())):
				return super(MultiplyAccumulate, self).get_verilog(t)
					
		for m in self.modules:
			m.should_create_verilog_definition = False
		self.output.driven_by_output = True
		verilog = self.get_module_header(t)
		inputs = self.get_inputs()
		outputs = self.get_outputs()
		to_declare = inputs + outputs
		for s in to_declare:
			verilog += t + s.get_declaration_string() + "\n"
			
		if arch.hpipe_device_arch.arch_name == "S10_NX":
			if not enable_sim_module_replace:  enable_sim_module_replace = (not self.implement_with_megafunction)
				
			#Make sure this module is compatible with the NX tensor block by checking input sizes
			use_custom_mult_ip = False
			for i in self.inputs[:-1]:
				if i._bit_width > 8 and not self.has_external_carry_in:
					print("\nWARNING: Tensor block can only be instantiated in scalar mode if the input is 8-bits or less. The port " + i.drivers[0].name + " is " + str(i._bit_width) + " bits, a custom multiplier IP is being instantated instead (area may be high).")
					use_custom_mult_ip = True
					break
					
			if use_custom_mult_ip:
					#use custom multiplier IP here
					verilog += self.get_megafunction_instance_nx_mult_ip()		
			else:
				assert(len(self.inputs) <= 13 or (len(self.inputs) <= 14 and self.has_external_carry_in))
				if self.independent_mults:
					verilog += self.get_megafunction_instance_nx_independent((not self.implement_with_megafunction))
				else: 
					verilog += self.get_megafunction_instance_nx((not self.implement_with_megafunction))
		else:
			verilog += self.get_megafunction_instance()

		verilog += "endmodule // " + self.name + "\n"
		return verilog
	
	def get_megafunction_instance(self):
		
		template_params = {}
		b_port_params = {}
		b_params = {}
		names = ["ax", "ay", "bx", "by"]
		port_indent = "\n                                        "
		for j,(n,i) in enumerate(zip(names, self.inputs[:-1])):
			port_dict = template_params
			param_dict = template_params
			if j >= 2:
				port_dict = b_port_params
				param_dict = b_params
			if i.signed:
				template_params["signed_" + n] = "true"
			else:
				template_params["signed_" + n] = "false"
			param_dict["bits_" + n] = str(i._bit_width)
			port_dict[n + "_s"] = i.drivers[0].name

		if self.has_external_carry_in:
			b_port_params["by_s"] = self.external_carry_in.drivers[0].name + "[17:0]"
			b_port_params["bx_s"] = self.external_carry_in.drivers[0].name + "[35:18]"
			b_params["bits_bx"] = 18
			b_params["bits_by"] = 18

		template_params["use_chainadder"] = "false"
		template_params["chainin_port"] = ""
		if self.carry_in.drivers[0] is not self.carry_in_default:
			template_params["use_chainadder"] = "true"
			template_params["chainin_port"] = port_indent + ".chainin (" + self.carry_in.drivers[0].name + "),"
		template_params["chainout_port"] = ""
		if len(self.carry_out.loads) > 0:
			template_params["chainout_port"] = port_indent + ".chainout (" + self.carry_out.name + "),"
		template_params["accum_clock"] = "none"
		template_params["accumulate_port"] = ""
		template_params["load_const_port"] = ""
		template_params["load_const_value"] = "0"
		if self.inputs[-1].drivers[0] != self.default_accumulate:
			template_params["accum_clock"] = "0"
			template_params["accumulate_port"] = ".accumulate(" + self.inputs[-1].drivers[0].name + "),"
			if self.load_const != -1:
				template_params["load_const_port"] = ".loadconst(1'b1),"
				template_params["load_const_value"] = str(self.load_const)



		template_params["ena_s"] = "3'b111"
		template_params["clk_s"] = self.input_pipes[0].sr[0].get_clock().name
		template_params["result_s"] = self.output.name
		template_params["operation_mode"] = "m18x18_sumof2"
		if len(self.inputs) == 3 and not self.has_external_carry_in:
			template_params["operation_mode"] = "m27x27"
			template_params["b_param_str"] = ""
			template_params["b_port_str"] = ""
			template_params["signed_bx"] = "false"
			template_params["signed_by"] = "false"
			template_params["b_clock"] = """
					fourteennm_mac_component.bx_clock="none",
					fourteennm_mac_component.bz_clock="none",
					fourteennm_mac_component.by_clock="none","""

		else:
			b_param_template = Template(r"""
					fourteennm_mac_component.bx_width = $bits_bx,
					fourteennm_mac_component.by_width = $bits_by,""")
			b_port_template = Template(r"""
                                        .bx ($bx_s),
                                        .by ($by_s),""")
			template_params["b_param_str"] = b_param_template.substitute(**b_params)
			template_params["b_port_str"] = b_port_template.substitute(**b_port_params)
			template_params["b_clock"] = """
					fourteennm_mac_component.bx_clock="0",
					fourteennm_mac_component.bz_clock="none",
					fourteennm_mac_component.by_clock="0","""
		if self.has_external_carry_in:
			template_params["operation_mode"] = "m18x18_plus36"
			template_params["signed_by"] = "false"

		template_params["result_width"] = self.output._bit_width

		template = Template(r"""
            fourteennm_mac        fourteennm_mac_component (
                                        $accumulate_port
                                        $load_const_port
                                        .ax ($ax_s),
                                        .ay ($ay_s),$b_port_str$chainin_port
                                        .ena ($ena_s),
                                        .clr (2'b00),
                                        .clk ({$clk_s,$clk_s,$clk_s}),$chainout_port
                                        .resulta ($result_s));
            defparam

					fourteennm_mac_component.ax_clock="0",
					fourteennm_mac_component.ay_scan_in_clock="0",
					fourteennm_mac_component.az_clock="none",
					fourteennm_mac_component.output_clock="0",
					fourteennm_mac_component.accumulate_clock="$accum_clock",
					fourteennm_mac_component.accum_pipeline_clock="$accum_clock",
					fourteennm_mac_component.accum_2nd_pipeline_clock="$accum_clock",$b_clock
					fourteennm_mac_component.coef_sel_a_clock="none",
					fourteennm_mac_component.coef_sel_b_clock="none",
					fourteennm_mac_component.sub_clock="none",
					fourteennm_mac_component.negate_clock="none",
					fourteennm_mac_component.load_const_clock="none",
					fourteennm_mac_component.load_const_pipeline_clock="none",
					fourteennm_mac_component.load_const_2nd_pipeline_clock="none",
					fourteennm_mac_component.input_pipeline_clock="0",
					fourteennm_mac_component.second_pipeline_clock="0",
					fourteennm_mac_component.input_systolic_clock="none",
					fourteennm_mac_component.preadder_subtract_a = "false", 
					fourteennm_mac_component.preadder_subtract_b = "false", 
					fourteennm_mac_component.delay_scan_out_ay = "false", 
					fourteennm_mac_component.delay_scan_out_by = "false", 
					fourteennm_mac_component.ay_use_scan_in = "false", 
					fourteennm_mac_component.by_use_scan_in = "false", 
					fourteennm_mac_component.operand_source_may = "input", 
					fourteennm_mac_component.operand_source_mby = "input", 
					fourteennm_mac_component.operand_source_max = "input", 
					fourteennm_mac_component.operand_source_mbx = "input", 
					fourteennm_mac_component.signed_max = "$signed_ax", 
					fourteennm_mac_component.signed_may = "$signed_ay", 
					fourteennm_mac_component.signed_mbx = "$signed_bx", 
					fourteennm_mac_component.signed_mby = "$signed_by", 
					fourteennm_mac_component.use_chainadder = "$use_chainadder", 
					fourteennm_mac_component.enable_double_accum = "false", 
					fourteennm_mac_component.operation_mode = "$operation_mode", 
					fourteennm_mac_component.clear_type = "none",
					fourteennm_mac_component.ax_width = $bits_ax,
					fourteennm_mac_component.ay_scan_in_width = $bits_ay,$b_param_str
					fourteennm_mac_component.result_a_width = $result_width,
					fourteennm_mac_component.load_const_value = $load_const_value,
					fourteennm_mac_component.coef_a_0 = 0,
					fourteennm_mac_component.coef_a_1 = 0,
					fourteennm_mac_component.coef_a_2 = 0,
					fourteennm_mac_component.coef_a_3 = 0,
					fourteennm_mac_component.coef_a_4 = 0,
					fourteennm_mac_component.coef_a_5 = 0,
					fourteennm_mac_component.coef_a_6 = 0,
					fourteennm_mac_component.coef_a_7 = 0,
					fourteennm_mac_component.coef_b_0 = 0,
					fourteennm_mac_component.coef_b_1 = 0,
					fourteennm_mac_component.coef_b_2 = 0,
					fourteennm_mac_component.coef_b_3 = 0,
					fourteennm_mac_component.coef_b_4 = 0,
					fourteennm_mac_component.coef_b_5 = 0,
					fourteennm_mac_component.coef_b_6 = 0,
					fourteennm_mac_component.coef_b_7 = 0;
""")
		return template.substitute(**template_params)


	#Same as above but uses the NX tensor block 
	def get_megafunction_instance_nx(self, sim=False):
		template_params = {}
		b_port_params = {}
		b_params = {}
		inst = self.inst
		tensor_block_cycles = self.mul_stages
		local_mult_count = 0
		#Step 1: combine input ports to form data_in
		names = ["din_a1", "din_b1", "din_a2", "din_b2", "din_a3", "din_b3", "din_a4", "din_b4", "din_a5", "din_b5", "din_a6", "din_b6"]
		if self.has_external_carry_in: names.append("temp_name")

		port_indent = "\n                                        "
		for i in range(len(names)):
			if i >= len(self.inputs[:-1]):
				template_params[names[i]+"_def"] = "8'd0;";
			else:
				local_mult_count += i % 2
				cur_input = self.inputs[i]
				cur_name = cur_input.drivers[0].name
				if cur_input._bit_width > 8:
					if not self.has_external_carry_in:
						print("ERROR: Tensor block can only be instantiated in scalar mode if the input is 8-bits or less. The port " + cur_name + " is " + str(cur_input._bit_width) + " bits!")
						for cur_input_i in self.inputs:
							print(cur_input_i.drivers[0].name)
						exit(1)
					else:
						template_params["external_carry_in"] = cur_name
						template_params["external_carry_in_width"] = cur_input._bit_width
						template_params[names[i]+"_def"] = "8'd0;";
				elif cur_input._bit_width == 8:
					template_params[names[i]+"_def"] = cur_name + ";"
				else:
					template_params[names[i]+"_def"] = "{ {" + str(8-cur_input._bit_width) + "{" + cur_name + "[" + str(cur_input._bit_width-1) + "]}}, " + cur_name + "}; //Sign extended"
				
		#Clock, enable and operation mode
		template_params["ena_s"] = "1'b1"
		template_params["clk_s"] = self.input_pipes[0].sr[0].get_clock().name

		template_params["acc_output_carry_chain"] = ""
		template_params["external_carry_in_tensor"] = ""

		#Create a tensor block to pass in the values into the new chain
		#This tensor block must operate in accumulate mode for full precision which means it cannot perform any multiplications
		if self.has_external_carry_in:
			assert(template_params["external_carry_in_width"] >= 32) #will have to change this limit later

			template_params["acc_output_carry_chain"] = "acc_output_carry_chain"
			template_params["module_name_acc"] = "fourteennm_dsp_prime"
			if sim:
				template_params["module_name_acc"] = "tensor_block_tensor_acc_sim"

			#global mlab_count
			#print("MLAB usage: ", mlab_count)
			
			external_carry_in_template = Template("""   	
				wire [31:0] acc_input;
				//Need to add some exponent otherwise tensor block will assume fp32 value is 0. Have to make sure exponent doesn't end up bein all 1s as that is NaN
				//This can be removed once support for fxp is working
				assign acc_input = {3'b001, $external_carry_in[28:0]}; 

				wire [95:0] $acc_output_carry_chain;

				//Currently unused 
				wire [37:0] result_l_acc;
 		 		wire [36:0] result_h_acc;

				$module_name_acc #(
					.dsp_mode("tensor_acc_fp32_one"),
					.dsp_sel_int4("select_int8"),
					.dsp_fp32_sub_en("float_sub_disabled"),
					.dsp_cascade("cascade_disabled")
					) tensorblock_acc_instance (        
					.ena($ena_s),
					.clk($clk_s),
					.clr({1'b0,1'b0}),
					.data_in({32'd0, acc_input}),
					.shared_exponent(8'd0),
					.load_buf_sel(1'b0),
					.mode_switch(1'b0),
					.load_bb_one(1'b0),
					.load_bb_two(1'b0),
					.feed_sel(2'b0),
					.zero_en(1'b0),
					.acc_en(1'b0),
					.cascade_weight_in(88'd0),
					.cascade_weight_out(),
					.result_h(result_h_acc),
					.result_l(result_l_acc),
					.cascade_data_in(96'd0),
					.cascade_data_out($acc_output_carry_chain)
					);	 """)

			template_params["external_carry_in_tensor"] = external_carry_in_template.substitute(**template_params)

		#Chain in and out (i.e. cascade_in and cascade_out)		
		#Tensorblock chain should be unconnected by default (or set to 0s if we are using a simulation module)
		template_params["chainin_port"] = ""
		if sim:
			template_params["chainin_port"] = "96'd0"
			template_params["cascade_enabled"] = 0
		else:
			template_params["cascade_enabled"] = '"cascade_disabled"'

			
		if self.carry_in.drivers[0] is not self.carry_in_default or self.has_external_carry_in:
			template_params["cascade_enabled"] = '"cascade_enabled"'
			if sim: template_params["cascade_enabled"] = 1
			
			if self.has_external_carry_in: template_params["chainin_port"] = template_params["acc_output_carry_chain"]
			else: template_params["chainin_port"] = "{32'd0, " + self.carry_in.drivers[0].name + "}" 
			
		template_params["chainout_port"] = ""
		template_params["result_wire"] = ""
		if len(self.carry_out.loads) > 0:
			template_params["chainout_port"] = "{cascade_tmp, " + self.carry_out.name + "}"
			template_params["result_wire"] = "wire [63:0] " + self.output.name + ";"

		template_params["result_s"] = self.output.name
		template_params["operation_mode"] = "vector_fxp"
		
		#Accumulate port
		template_params["accumulate_port"] = "1'b0"
		template_params["external_accumulate_reg"] = ""
		template_params["result_l"] = "result_l"
		
		if self.inputs[-1].drivers[0] != self.default_accumulate:
			
			#If there is a cascade input we cannot use the accumulate port at the same time on the NX
			#Therefore we have to create an extra output register outside of the tensor block to perform the accumulation
			if self.carry_in.drivers[0] is not self.carry_in_default:
				template_params["accumulate_input_name"] = self.inputs[-1].drivers[0].name
				
				template_params["signed"] = "$signed"
				template_params["accumulate_cycles"] = str(tensor_block_cycles-1)
				
				
				external_accumulate_reg_template = Template(r"""
					reg [18:0] external_accumulate_reg;
					reg [$accumulate_cycles:0] accumulate_delay; 
					integer i;
					always @(posedge $clk_s) begin
						for(i = 0; i <= $accumulate_cycles; i += 1) begin
							if(i == 0) accumulate_delay[0] <= $accumulate_input_name;
							else accumulate_delay[i] <= accumulate_delay[i-1];
						end
					
						if (accumulate_delay[$accumulate_cycles]) external_accumulate_reg <= $signed(external_accumulate_reg) + $signed(result_l);
						else external_accumulate_reg <= $signed(result_l);
					end
				
				""")
				
				template_params["result_l"] = "external_accumulate_reg"
				template_params["external_accumulate_reg"] = external_accumulate_reg_template.substitute(**template_params)
				
			else:
				template_params["accumulate_port"] = self.inputs[-1].drivers[0].name
			
			
			#Adding the the constant (which according to Mathew is for rounding the result) completely breaks the unit test and makes the csv files different compared to the s10 dsp
			#Therefore it is disabled for now...
			enable_load_const = False
			if enable_load_const and self.load_const != -1:
				#There is no load_const feature in this tensor block so we must use one of the inputs and multiply the const by 1
				if template_params["din_a6_def"] == "8'd0;":
					if self.load_const > 127 or self.load_const < -127:
						print("\nERROR: load_const value (" + str(self.load_const) + ") is not 8-bits and thus cannot be loaded into tensor block!")
						exit(1)

					template_params["din_a6_def"] = "8'd" + str(self.load_const) + ";"
					template_params["din_b6_def"] = "8'd1;" 

				else:
					print("\nERROR: load_const will not fit in current tensor block as all inputs are used! Cannot load " + str(self.load_const) + " into tensor block")
					exit(1)


			
		template_params["module_name"] = "fourteennm_dsp_prime"
		if sim:
			template_params["module_name"] = "tensor_block_vector_fixed_point"

		#Make sure result width is 32 for each value, otherwise it makes writing the rest a bit of a pain
		assert(self.output._bit_width == 64)
		template_params["result_width"] = str(self.output._bit_width)

		template = Template(r"""
		
		 wire signed  [7:0] din_a1, din_b1, din_a2, din_b2, din_a3, din_b3, din_a4, din_b4, din_a5, din_b5, din_a6, din_b6;
		
		assign din_a1 = $din_a1_def
		assign din_b1 = $din_b1_def
		assign din_a2 = $din_a2_def
		assign din_b2 = $din_b2_def
		assign din_a3 = $din_a3_def
		assign din_b3 = $din_b3_def
		assign din_a4 = $din_a4_def
		assign din_b4 = $din_b4_def
		assign din_a5 = $din_a5_def
		assign din_b5 = $din_b5_def
		assign din_a6 = $din_a6_def
		assign din_b6 = $din_b6_def
		
		wire [31:0] cascade_tmp;
		wire [18:0] res_tmp;
		
		wire [18:0] result_l;
		$result_wire

$external_carry_in_tensor
		
		$module_name #(
			.dsp_mode("$operation_mode"),
			.dsp_sel_int4("select_int8"),
			.dsp_fp32_sub_en("float_sub_disabled"),
			.dsp_cascade($cascade_enabled)
		)
		 tensorblock_instance (
			.ena($ena_s),
			.clk($clk_s),
			.data_in({din_b6,din_a6,din_b5,din_a5,din_b4,din_a4,din_b3,din_a3,din_b2,din_a2,din_b1,din_a1}),
			.clr({1'b0,1'b0}),
			.result_l( { res_tmp , result_l} ),
			.result_h(),
			.load_buf_sel(1'b0),
			.mode_switch(1'b0),
			.load_bb_one(1'b0),
			.load_bb_two(1'b0),
			.feed_sel(2'b0),
			.zero_en(1'b0),
			.shared_exponent(8'h0),
			.cascade_weight_in(88'h0),
			.cascade_data_in($chainin_port),
			.acc_en($accumulate_port),

			.cascade_weight_out(),
			.cascade_data_out($chainout_port)
);

$external_accumulate_reg

assign $result_s[63:0] = {{45{$result_l[18]}}, $result_l};

""")

		return template.substitute(**template_params)


	def get_megafunction_instance_nx_independent(self, sim=False):
		template_params = {}
		b_port_params = {}
		b_params = {}
		inst = self.inst
		tensor_block_cycles = self.mul_stages
		#Step 1: combine input ports to form data_in
		names = ["din_a1", "din_b1", "din_a2", "din_b2", "din_a3", "din_b3"]
		port_indent = "\n                                        "
		for i in range(len(names)):
			if i >= len(self.inputs[:-1]):
				template_params[names[i]+"_def"] = "8'd0;";
			else:
				cur_input = self.inputs[i]
				cur_name = cur_input.drivers[0].name
				if cur_input._bit_width > 8:
					print("ERROR: Tensor block can only be instantiated in scalar mode if the input is 8-bits or less. The port " + cur_name + " is " + str(cur_input._bit_width) + " bits!")
					for cur_input_i in self.inputs:
						print(cur_input_i.drivers[0].name)
					exit(1)
				elif cur_input._bit_width == 8:
					template_params[names[i]+"_def"] = cur_name + ";"
				else:
					template_params[names[i]+"_def"] = "{ {" + str(8-cur_input._bit_width) + "{" + cur_name + "[" + str(cur_input._bit_width-1) + "]}}, " + cur_name + "}; //Sign extended"
				


		if self.has_external_carry_in:
			print("ERROR: external carry in not yet supported for Stratix 10 NX")
			exit(1)

		assert(self.carry_in.drivers[0] is self.carry_in_default)
		assert(len(self.carry_out.loads) == 0)

		#Chain in and out (i.e. cascade_in and cascade_out)		
		#Tensorblock chain should be unconnected by default (or set to 0s if we are using a simulation module)
		template_params["chainin_port"] = ""
		template_params["cascade_enabled"] = "cascade_disabled"
		template_params["chainout_port"] = ""


		#Clock, enable and operation mode
		template_params["ena_s"] = "1'b1"
		template_params["clk_s"] = self.input_pipes[0].sr[0].get_clock().name

		#This version of the tensor block will have multiple output signals
		template_params["extra_res_wires"] = ""
		template_params["result_1"] = self.independent_outputs[0].name
		if len(self.independent_outputs) > 1: 
			template_params["result_2"] = self.independent_outputs[1].name
			if len(self.independent_outputs) > 2: template_params["result_3"] = self.independent_outputs[2].name
			else: 
				template_params["result_3"] = "res_3"
				template_params["extra_res_wires"] = "wire [63:0] res_3;"
		else: 
			template_params["result_2"] = "res_2"
			template_params["result_3"] = "res_3"
			template_params["extra_res_wires"] = "wire [63:0] res_2, res_3;"

		template_params["operation_mode"] = "scalar_8x8"

		global scalar_tensor_count
		scalar_tensor_count += 1
		
		#Accumulate port
		template_params["accumulate_port"] = "1'b0"
		template_params["external_accumulate_reg"] = ""
		template_params["result_l"] = "result_l"
		
		if self.inputs[-1].drivers[0] != self.default_accumulate:
			
			#If there is a cascade input we cannot use the accumulate port at the same time on the NX
			#Therefore we have to create an extra output register outside of the tensor block to perform the accumulation
			if self.carry_in.drivers[0] is not self.carry_in_default:
				template_params["accumulate_input_name"] = self.inputs[-1].drivers[0].name
				
				template_params["signed"] = "$signed"
				template_params["accumulate_cycles"] = str(tensor_block_cycles-1)
				
				
				external_accumulate_reg_template = Template(r"""
					reg [18:0] external_accumulate_reg;
					reg [$accumulate_cycles:0] accumulate_delay; 
					integer i;
					always @(posedge $clk_s) begin
						for(i = 0; i <= $accumulate_cycles; i += 1) begin
							if(i == 0) accumulate_delay[0] <= $accumulate_input_name;
							else accumulate_delay[i] <= accumulate_delay[i-1];
						end
					
						if (accumulate_delay[$accumulate_cycles]) external_accumulate_reg <= $signed(external_accumulate_reg) + $signed(result_l);
						else external_accumulate_reg <= $signed(result_l);
					end
				
				""")
				
				template_params["result_l"] = "external_accumulate_reg"
				template_params["external_accumulate_reg"] = external_accumulate_reg_template.substitute(**template_params)
				
			else:
				template_params["accumulate_port"] = self.inputs[-1].drivers[0].name
			
			
			#Adding the the constant (which according to Mathew is for rounding the result) completely breaks the unit test and makes the csv files different compared to the s10 dsp
			#Therefore it is disabled for now...
			enable_load_const = False
			if enable_load_const and self.load_const != -1:
				#There is no load_const feature in this tensor block so we must use one of the inputs and multiply the const by 1
				if template_params["din_a6_def"] == "8'd0;":
					if self.load_const > 127 or self.load_const < -127:
						print("\nERROR: load_const value (" + str(self.load_const) + ") is not 8-bits and thus cannot be loaded into tensor block!")
						exit(1)

					template_params["din_a6_def"] = "8'd" + str(self.load_const) + ";"
					template_params["din_b6_def"] = "8'd1;" 

				else:
					print("\nERROR: load_const will not fit in current tensor block as all inputs are used! Cannot load " + str(self.load_const) + " into tensor block")
					exit(1)

		template_params["module_name"] = "fourteennm_dsp_prime"
		template_params["dsp_cascade"] = '"cascade_disabled"'
		if sim:
			template_params["module_name"] = "tensor_block_8x8_scalar"
			template_params["dsp_cascade"] = 0

		#Make sure result width is 32 for each value, otherwise it makes writing the rest a bit of a pain
		assert(self.output._bit_width == 64)
		template_params["result_width"] = str(self.output._bit_width)

		template = Template(r"""
		
		 wire signed  [7:0] din_a1, din_b1, din_a2, din_b2, din_a3, din_b3;
		
		assign din_a1 = $din_a1_def
		assign din_b1 = $din_b1_def
		assign din_a2 = $din_a2_def
		assign din_b2 = $din_b2_def
		assign din_a3 = $din_a3_def
		assign din_b3 = $din_b3_def
		
		wire [37:0] result_l;
		wire [36:0] result_h;

		$extra_res_wires
				
		wire [95:0] tmp;
		
        $module_name #(
				.dsp_mode("$operation_mode"),
				.dsp_sel_int4("select_int8"),
				.dsp_fp32_sub_en("float_sub_disabled"),
				.dsp_cascade($dsp_cascade)
		) tensorblock_instance (        
				.ena($ena_s),
				.clk($clk_s),
				.clr(2'd0),
				.data_in({din_b3,din_a3,din_b2,din_a2,din_b1,din_a1}),
				.acc_en($accumulate_port),
				.result_h(result_h),
				.result_l(result_l),
				.cascade_data_in($chainin_port),
				.cascade_data_out($chainout_port),

				.load_buf_sel(1'b0), .mode_switch(1'b0), .load_bb_one(1'b0), .load_bb_two(1'b0), .zero_en(1'b0),
				.feed_sel(2'b0),
				.shared_exponent(8'b0),
				.cascade_weight_in(88'd0),
				.cascade_weight_out()


		);
				
		assign $result_1 = { {39{result_l[24]}}, result_l[24:0]};
		assign $result_2 = { {39{result_h[11]}}, result_h[11:0], result_l[37:25]};
		assign $result_3 = { {39{result_h[36]}}, result_h[36:12]};

$external_accumulate_reg

""")

		return template.substitute(**template_params)


	#Same as above but uses an IP to instantiate multiple NX tensor blocks for larger multiplications
	def get_megafunction_instance_nx_mult_ip(self):
		template_params = {}
	
		#Step 1: combine input ports to form data_in
		names = ["din_a", "din_b"]
		port_indent = "\n                                        "
		total_width = 0
		for i in range(len(names)):
			cur_input = self.inputs[i]
			cur_name = cur_input.drivers[0].name
			template_params[names[i]+"_def"] = cur_name
			template_params[names[i]+"_width"] = str(cur_input._bit_width)
			total_width += cur_input._bit_width
								
		template_params["result_s"] = self.output.name
		template_params["clk_s"] = self.input_pipes[0].sr[0].get_clock().name
		
		template_params["mult_output_bus"] = "[" + str(total_width-1) + ":0]"
		
		if total_width > self.output._bit_width:
			template_params["output_assign"] = "mult_output[" + str(self.output._bit_width-1) + ":0]"
		else:
			#Sign extend if needed
			template_params["output_assign"] = "{ {" + str(self.output._bit_width-total_width)  + " {mult_output[" + str(total_width-1) + "]}}, mult_output }"
		
		#This module does not support cascading, accumulation, or more than 2 inputs
		assert(self.inputs[-1].drivers[0] == self.default_accumulate)
		assert(self.carry_in.drivers[0] == self.carry_in_default)
		assert(len(self.inputs) <= 3) #Note, there is usually an extra accumulate input even if it isn't actually used
	
		template = Template(r""" 
		
		wire $mult_output_bus mult_output;
		
		nx_mult_int #(
				 .SIZE_A($din_a_width),
				.SIZE_B($din_b_width)
			) dut(
				.clk($clk_s),
				.din_a($din_a_def),
				.din_b($din_b_def),
				.dout(mult_output)
			);
			
			assign $result_s = $output_assign;
			
			""")
	
		return template.substitute(**template_params)


class TensorAdd(Module):
	'''
	 Stratix 10 NX tensor block addition module
	 This module uses the tensor block to perform 2 bfloat24 additions (1-bit sign, 8-bit exponent, 15-bit mantissa) 

	 No special control signals are needed for this mode, but the accumulate function can be used if needed

	 Attributes
		inputs (list of 24-bit :class:`Logic`):
		The 24-bit floating-point inputs. There are 2 adders per tensor block and thus 4 inputs total

		results (list of 24-bit :class:`Logic`):
		The 24-bit floating-point outputs. There are 2 adders per tensor block and thus 2 outputs total

	'''
	def __init__(self, clock, reset, kind="tensor_add", implement_with_megafunction=False, enable_reset=False, **kwargs):
		super(TensorAdd, self).__init__("tensor_add", kind=kind, **kwargs)
		inst = self.inst
		with ModuleScope(self):
		#Add phantomflop to propagate clock and reset since much of the verilog here is hand writtem
			inst(PhantomFlop, clock=clock, reset=reset)
			clock.add_sink(self)
			self.clock = clock
			self.reset = reset
			self.enable_reset = enable_reset

			self.implement_with_megafunction = implement_with_megafunction

			#Create 24-bit inputs and set default drivers
			# These must be 24-bits and cannot be "sign-extended" automatically as they are 24-bit float numbers
			import string
			name_list = list(string.hexdigits) 
			self.default_input = inst(Constant, bits=NX_TENSOR_BFLOAT_OUTPUT_PRECISION, value=0, name="default_input")
			self.inputs = [inst(Logic, bits=NX_TENSOR_BFLOAT_OUTPUT_PRECISION, name=("input_"+name_list[i]), signed=True) for i in range(NX_BFLOAT24_ADD_INPUTS)]
			for i in range(len(self.inputs)): self.inputs[i].set_driver(self.default_input)

			#Create 2 outputs
			self.results = [inst(Logic, bits=NX_TENSOR_BFLOAT_OUTPUT_PRECISION, name=("output_"+name_list[i]), signed=True) for i in range(NX_OUT_WIDTH_PARALLELISM_PER_TENSOR_BLOCK-1)]

			#Create any remaining optional signals
			self.default_acc_en = inst(Constant, bits=1, value=0, name="default_acc_en")
			self.acc_en = inst(Logic, bits=1, name="acc_en")
			self.acc_en.set_driver(self.default_acc_en)

			#Increment total DSP/tensor block count
			self.dsp_count = 1
			global final_dsp_count
			final_dsp_count += self.dsp_count
			


	def get_verilog(self, t="  "):
		#Tell HPIPE to add the static simulation modules to the output
		global enable_sim_module_replace
		if not enable_sim_module_replace: enable_sim_module_replace = (not self.implement_with_megafunction)
		for m in self.modules:
			m.should_create_verilog_definition = False

		#Generate the verilog for the module inputs/outputs
		verilog = self.get_module_header(t)
		inputs = self.get_inputs()
		outputs = self.get_outputs()
		to_declare = inputs + outputs
		for s in to_declare:
			verilog += t + s.get_declaration_string() + "\n"		

		#The majority of the module is generated in this function
		verilog += self.get_megafunction_instance()

		#Finish off the module
		verilog += "endmodule // " + self.name + "\n"
		return verilog

	def get_megafunction_instance(self):
		template_params = {}
		sim = (not self.implement_with_megafunction)

		OUTPUT_PRECISION = NX_TENSOR_BFLOAT_OUTPUT_PRECISION

		#Connect the input depending on if the bus method or the data method is used
		data_in_string = "{"
		for cur_input in self.inputs[::-1]:
			#If there are any unused inputs set them to 0. Note that input[0] will correspond to the 8 most least significant bits (or whatever NX_INPUT_DATA_BITS is)
			if cur_input.drivers[0] != self.default_input: data_in_string += cur_input.drivers[0].name + ", "
			else: data_in_string += str(NX_TENSOR_BFLOAT_OUTPUT_PRECISION) + "'d0,"
			template_params["data_in"] = data_in_string[:-2] + "}"

		#Connect the results, use a temp wire if they aren't hooked up
		template_params["tmp_res_wires"] = ""
		output_unconnected = False
		for i in range(len(self.results)):
			if len(self.results[i].loads) > 0:
				template_params["result_"+str(i)] = self.results[i].name
			else:
				if not output_unconnected:
					template_params["tmp_res_wires"] = "wire [" + str(OUTPUT_PRECISION-1) + ":0] temp_out_" + str(i)
					output_unconnected = True
				else:
					template_params["tmp_res_wires"] += ", temp_out_" + str(i)
				template_params["result_"+str(i)] = "temp_out_" + str(i)
		if output_unconnected: template_params["tmp_res_wires"] += ";"	

		#Set the tensor block module name (sim vs IP) and the operation modes
		template_params["module_name"] = "fourteennm_dsp_prime"
		if sim:
			template_params["module_name"] = "tensor_block_bfloat24_add"

		template_params["operation_mode"] = "scalar_bf24"

		#Define clock, enable and reset. We always want the tensor enabled as we don't stall in the middle of computing a line, only inbetween
		template_params["ena_s"] = "1'b1"
		template_params["clk_s"] = self.clock.name
		if self.enable_reset: template_params["clear"] = self.reset.name
		else: template_params["clear"] = "1'b0"

		#define default control signals
		template_params["tensor_mode"] = "2'b00" #Side-channel weight loading mode

		#Hook up data cascade in to default values
		template_params["cascade_enabled"] = '"cascade_disabled"'
		if sim: template_params["cascade_enabled"] = "0"
		template_params["cascade_data_in"] = str(NX_CASCADE_DATA_SIZE) + "'d0"	
		template_params["cascade_data_out"] = ""

		#Hook up weight cascade in
		if sim: template_params["cascade_weight_in"] = str(NX_CASCADE_WEIGHT_SIZE) + "'d0"	
		else: template_params["cascade_weight_in"] = "" #Quartus has an error if we try to set the weight cascade to 0s
		template_params["cascade_weight_out"] = ""

		#Hook up accumulate. We don't use this for out bfloat24 adder, but it's here for completeness
		if self.acc_en.drivers[0] != self.default_acc_en: template_params["acc_en"] =  self.acc_en.drivers[0].name
		else: template_params["acc_en"] = "1'b0"

		template_params["tensor_res_sel_1"] = "result_l[23:0]"
		template_params["tensor_res_sel_2"] = "result_h[23:0]"

		template = Template(r"""
		$tmp_res_wires
		wire [36:0] result_h;
		wire [37:0] result_l;
		
		$module_name #(
			.dsp_mode("$operation_mode"),
			.dsp_sel_int4("select_int8"),
			.dsp_fp32_sub_en("float_sub_disabled"),
			.dsp_cascade($cascade_enabled)
		)
		 tensorblock_instance (
			.ena($ena_s),
			.clk($clk_s),
			.data_in($data_in),
			.clr({$clear, $clear}),
			.result_l(result_l),
			.result_h(result_h),
			.load_buf_sel(1'b0),
			.mode_switch(1'b0),
			.load_bb_one(1'b0),
			.load_bb_two(1'b0),
			.feed_sel(2'b0),
			.zero_en(1'b0),
			.shared_exponent(8'd0),
			.acc_en($acc_en),
			.cascade_weight_in($cascade_weight_in),
			.cascade_weight_out($cascade_weight_out),
			.cascade_data_in($cascade_data_in),
			.cascade_data_out($cascade_data_out)
);

		//Assign results like in tensor block spec
		assign $result_0 = $tensor_res_sel_1;
		assign $result_1 = $tensor_res_sel_2;

		""")

		return template.substitute(**template_params)

class TensorBlock(Module):
	"""
	TensorBlock NX Module

	This module instantiates a tensor block on the Stratix 10 NX in tensor mode. Currently only the TensorConv module makes use of it.
	For any other multiplicastion related functions on the NX (e.g. Mean or BasicConv), the MultiplyAccumulate module is used instead and instantiates the tensor block in vector mode.

	If ``implement_with_megafunction`` is ``True`` it will instantiate the multiply accumulate as a Stratix 10 NX IP.
	Otherwise it uses the functional simulation version of the tensor block module under digitallogic/verilog_files/tensor_mode_fxp_sim.sv

	If ``cascade_loading`` is ``True`` then the weight cascade method is used to pre-load weights. Otherwise the side_inputs mode will be used
	If ``first_tensor`` is ``True` in addition to the above, then the inputs are passed down to cascade_weight_out so they can be sent down the chain to pre-load

	If ``enable_reset`` is set to ``True`` then each tensor block will also be hooked up to the main HPIPE reset. This is not reccomended as it's not functionally required
	assuming the control logic handles everything correctly and cpuld result in poorer timing if it is hooked up (MultiplyAccumulate doesn't use it either for the DSPs)

	If ``debug_mode`` is set to ``True`` then each tensor block will create some extra wires in order to make the waveform easier to read in the simulator.
	For example, if an input bis is used it will be split up into 10 seperate inputs before being fed into the tensor

	Note that this will only instantiate the tensor block shell itself and hook everything up, all the control logic & data must be generated elsewhere and provided.

	Attributes
		input_bus (80-bit :class:`Logic`):
		Either this or the list below must be hooked up. If this is hooked up, it will be assumed to be a bus containing all the inputs 
		(i.e. an 80-bit bus containing all 10 input values). Note that unlike vector mode or regular DSPs,
		this set of inputs is only one side of the multiplication, the other side must be pre-loaded using one of the pre-loading methods.

		inputs (list of 8-bit :class:`Logic` or one 80-bit :class:`Logic`):
		A list of inputs to be driven, either this or the input above must be hooked up. If this is hooked up,each list value will be treated as one 8-bit input. 
		The addtional note for the input_bus applies here too.

		shared_exp (8-bit :class:`Logic`):
		Optional variable for when we are using Bfloat

		side_inputs (list of 8-bit :class:`Logic`):
		Can contain up to 2 side-input values if we are pre-loading using the side input method. Make sure to refer to the user guide for the order of these values and control logic

		cascade_data_in (96 bit :class:`Logic`):
		If this isn't the first tensor block in the chain, then this should be the output of the previous chain. Note that unlike DSP blocks, tensor blocks have an extra register on this input
		
		cascade_data_out (96 bit :class:`Logic`):
		If this isn't the first tensor block in the chain, then this should be the output of the previous chain. Note that unlike DSP blocks, tensor blocks have an extra register on this input

		cascade_weight_in (88 bit :class:`Logic`):
		Optional values we want to cascade down the chain, depending on the pre-loading method. Note that in our case we are actually pre-loading activations so this name is a bit deceiving
		Appropriate ping pong register should also be selected

		cascade_weight_out (88 bit :class:`Logic`):
		Connect this to the next tensor block in the chain to pass on weights, depending on the pre-loading method. Appropriate ping pong register should also be selected

		results (list of :class:`Logic`):
		A list of 3 results from the tensor block. Note that one set of inputs is broadcast across all 3 results. The other set must be pre-loaded across multiple cycles (min 6, avg 15)

		load_ping_pong_reg (2-bit :class:`Logic`)
		Should set by control logic to 0 (don't load anything), 1 (load the first ping pong reg), or 2 (load the second ping pong reg)

		load_buf_sel (1-bit :class:`Logic`)
		Selects which ping pong register will be used for the actual computation (0=first ping pong, 1=second ping pong)

		acc_en (1-bit :class:`Logic`)
		Optional: will enable accumulation for the internal output register. Note that TensorConv does not make use of this and instead relies on external accumulation since it requires extra memory


	"""

	def __init__(self, clock, reset, kind="tensor_block", implement_with_megafunction=False, cascade_loading=True, first_tensor=False, enable_bfloat=False, external_carry_in=False, enable_reset=False, debug_mode=True, **kwargs):
		super(TensorBlock, self).__init__("tensor_block", kind=kind, **kwargs)
		inst = self.inst
		with ModuleScope(self):
			#Add phantomflop to propagate clock and reset since much of the verilog here is hand writtem
			inst(PhantomFlop, clock=clock, reset=reset)
			clock.add_sink(self)
			self.clock = clock
			self.reset = reset

			self.implement_with_megafunction = implement_with_megafunction
			self.cascade_loading = cascade_loading
			self.first_tensor = first_tensor
			self.debug_mode = debug_mode

			assert(not(first_tensor and not cascade_loading)) #There is no "first tensor" if we are using the side-input loading method

			self.external_carry_in = external_carry_in
			assert(not external_carry_in) #We don't currently support external carry-ins for tensor blocks in tensor mode as it would require at least 2 additional tensor blocks at the start of each chain

			self.enable_reset = enable_reset
			self.enable_bfloat = enable_bfloat

			#Create inputs and set default drivers
			import string
			name_list = list(string.hexdigits) #allows for up to 22 total inputs
			self.default_input = inst(Constant, bits=NX_INPUT_DATA_BITS, value=0, name="default_input")
			self.inputs = [inst(Logic, bits=NX_INPUT_DATA_BITS, name=("input_"+name_list[i]), signed=True) for i in range(NX_ICP_PER_TENSOR_BLOCK)]
			for i in range(len(self.inputs)): self.inputs[i].set_driver(self.default_input)
			self.default_input_bus = inst(Constant, bits=(NX_ICP_PER_TENSOR_BLOCK*NX_INPUT_DATA_BITS), value=0, name="default_input_bus")
			self.input_bus = inst(Logic, bits=(NX_ICP_PER_TENSOR_BLOCK*NX_INPUT_DATA_BITS), name=("input_bus"))
			self.input_bus.set_driver(self.default_input_bus)

			self.default_shared_exp = inst(Constant, bits=(NX_SHARED_EXP_SIZE), value=0, name="default_shared_exp")
			self.shared_exp = inst(Logic, bits=(NX_SHARED_EXP_SIZE), name="shared_exp")
			self.shared_exp.set_driver(self.default_shared_exp)

			#Create outputs. Driver will be hooked up during verilog generation stage
			if enable_bfloat:
				self.results = [inst(Logic, bits=NX_TENSOR_BFLOAT_OUTPUT_PRECISION, name=("output_"+name_list[i]), signed=True) for i in range(NX_OUT_WIDTH_PARALLELISM_PER_TENSOR_BLOCK)]
			else:
				self.results = [inst(Logic, bits=NX_TENSOR_OUTPUT_PRECISION, name=("output_"+name_list[i]), signed=True) for i in range(NX_OUT_WIDTH_PARALLELISM_PER_TENSOR_BLOCK)]

			#Create cascade inputs and set default drivers
			self.cascade_weight_in = inst(Logic, bits=(NX_CASCADE_WEIGHT_SIZE), name=("cascade_weight_in"))
			self.default_cascade_weight_in = inst(Constant, bits=(NX_CASCADE_WEIGHT_SIZE), value=0, name="default_cascade_weight_in")
			self.cascade_weight_in.set_driver(self.default_cascade_weight_in)
			self.cascade_data_in = inst(Logic, bits=(NX_CASCADE_DATA_SIZE), name=("cascade_data_in"))
			self.default_cascade_data_in= inst(Constant, bits=(NX_CASCADE_DATA_SIZE), value=0, name="default_cascade_data_in")
			self.cascade_data_in.set_driver(self.default_cascade_data_in)

			#Create cascade outputs. Driver will be hooked up during verilog generation stage
			self.cascade_weight_out = inst(Logic, bits=(NX_CASCADE_WEIGHT_SIZE), name="cascade_weight_out")
			self.cascade_data_out = inst(Logic, bits=(NX_CASCADE_DATA_SIZE), name="cascade_data_out")

			#Create enable signal and a potential 'constant 1' driver for it
			self.enable_clock = inst(Logic, bits=1, name="ena")
			self.default_enable = inst(Constant, bits=1, value=1, name="default_enable") #We'll connect this later if enable_clock is left unconnected

			#Create any remaining optional signals
			self.default_acc_en = inst(Constant, bits=1, value=0, name="default_acc_en")
			self.acc_en = inst(Logic, bits=1, name="acc_en")
			self.acc_en.set_driver(self.default_acc_en)

			#Create all the control signals, these must have a driver
			self.load_ping_pong_reg = inst(Logic, bits=2, name="load_ping_pong_reg")
			self.load_buf_sel = inst(Logic, bits=1, name="load_buf_sel")
			self.requires_driver = [self.load_ping_pong_reg, self.load_buf_sel]

			#Increment total DSP/tensor block count
			self.dsp_count = 1
			global final_dsp_count
			if self.external_carry_in: self.dsp_count += 2 #Future-proofing
			final_dsp_count += self.dsp_count

	def get_verilog(self, t="  "):
		#Tell HPIPE to add the static simulation modules to the output
		global enable_sim_module_replace
		if not enable_sim_module_replace: enable_sim_module_replace = (not self.implement_with_megafunction)
		for m in self.modules:
			m.should_create_verilog_definition = False

		#Generate the verilog for the module inputs/outputs
		verilog = self.get_module_header(t)
		inputs = self.get_inputs()
		outputs = self.get_outputs()
		to_declare = inputs + outputs
		for s in to_declare:
			verilog += t + s.get_declaration_string() + "\n"		

		#The majority of the module is generated in this function
		verilog += self.get_megafunction_instance()

		#Finish off the module
		verilog += "endmodule // " + self.name + "\n"
		return verilog

	#Get the IP or sim module instantiation
	def get_megafunction_instance(self):
		template_params = {}
		sim = (not self.implement_with_megafunction)
		assert(not(self.input_bus.drivers[0] is self.default_input_bus and self.inputs[0].drivers[0] is self.default_input)) # Make sure at least one of the inputs is defined
		assert(not(self.input_bus.drivers[0] is not self.default_input_bus and self.inputs[0].drivers[0] is not self.default_input)) # Make sure we didn't accidentaly define both types of input, must use one or the other

		template_params["debug_wires"] = ""

		#Connect the input depending on if the bus method or the data method is used
		if self.input_bus.drivers[0] is not self.default_input_bus:
			template_params["data_in"] = self.input_bus.drivers[0].name
			#Split up the input bus into seperate wires for debug mode
			if self.debug_mode:
				data_in_str = "{16'b0, "

				for i in range(NX_ICP_PER_TENSOR_BLOCK-1,-1,-1):
					in_str = "in_" + str(i)
					data_in_str += in_str + ","
					template_params["debug_wires"] += "wire [" + str(NX_INPUT_DATA_BITS-1) + ":0] " + in_str + ";\n"
					template_params["debug_wires"] += "assign " + in_str + " = " + self.input_bus.drivers[0].name + "[" + str((i+1)*NX_INPUT_DATA_BITS-1) + ":" + str(i*NX_INPUT_DATA_BITS) +  "];\n"

				template_params["data_in"] = data_in_str[:-1] + "}"

		else:
			data_in_string = "{"
			for cur_input in self.inputs[::-1]:
				#If there are any unused inputs set them to 0. Note that input[0] will correspond to the 8 most least significant bits (or whatever NX_INPUT_DATA_BITS is)
				if cur_input.drivers[0] != self.default_input: data_in_string += cur_input.drivers[0].name + ", "
				else: data_in_string += str(NX_INPUT_DATA_BITS) + "'d0,"
			template_params["data_in"] = data_in_string[:-2] + "}"

		# Hookup shared exponent if needed
		enable_bfloat = self.enable_bfloat
		
		if enable_bfloat:
			template_params["shared_exp"] = self.shared_exp.drivers[0].name
			OUTPUT_PRECISION = NX_TENSOR_BFLOAT_OUTPUT_PRECISION
		else:
			template_params["shared_exp"] = str(NX_SHARED_EXP_SIZE) + "'d0"
			OUTPUT_PRECISION = NX_TENSOR_OUTPUT_PRECISION

		#Connect the results, use a temp wire if they aren't hooked up
		template_params["tmp_res_wires"] = ""
		output_unconnected = False
		for i in range(len(self.results)):
			if len(self.results[i].loads) > 0:
				template_params["result_"+str(i)] = self.results[i].name
			else:
				if not output_unconnected:
					template_params["tmp_res_wires"] = "wire [" + str(OUTPUT_PRECISION-1) + ":0] temp_out_" + str(i)
					output_unconnected = True
				else:
					template_params["tmp_res_wires"] += ", temp_out_" + str(i)
				template_params["result_"+str(i)] = "temp_out_" + str(i)
		if output_unconnected: template_params["tmp_res_wires"] += ";"	

		#Setup the enable to a constant if it wasn't provided
		if not self.enable_clock.has_drivers():
			template_params["ena_s"] = "1'b1"
		else:
			template_params["ena_s"] = self.enable_clock.drivers[0].name

		#Set the tensor block module name (sim vs IP) and the operation modes
		template_params["module_name"] = "fourteennm_dsp_prime"
		if sim:
			if enable_bfloat: template_params["module_name"] = "tensor_block_tensor_floating_point"
			else: template_params["module_name"] = "tensor_block_tensor_fixed_point"
		template_params["operation_mode"] = "tensor_fxp"
		if enable_bfloat:
			template_params["operation_mode"] = "tensor_fp"

		#Define clock and reset. 

		template_params["clk_s"] = self.clock.name
		if self.enable_reset: template_params["clear"] = self.reset.name
		else: template_params["clear"] = "1'b0"


		#Select appropriate tensor block mode. Note that here the term "weight" refers to what we're loading in the ping pong registers (we actually load activations for convolutions)
		if self.cascade_loading:
			if self.first_tensor: template_params["tensor_mode"] = "2'b00" #Data-input weight loading mode for first block in cascade weight chain
			else: template_params["tensor_mode"] = "2'b01" #Cascade weight loading mode
		else: template_params["tensor_mode"] = "2'b10" #Side-channel weight loading mode

		#Hook up data cascade in
		if self.cascade_data_in.drivers[0] != self.default_cascade_data_in:
			template_params["cascade_enabled"] = '"cascade_enabled"'
			if sim: template_params["cascade_enabled"] = "1"
			template_params["cascade_data_in"] = self.cascade_data_in.drivers[0].name
		else:
			template_params["cascade_enabled"] = '"cascade_disabled"'
			if sim: template_params["cascade_enabled"] = "0"
			template_params["cascade_data_in"] = str(NX_CASCADE_DATA_SIZE) + "'d0"	

		#Hook up data cascade out
		template_params["cascade_data_out"] = ""
		if len(self.cascade_data_out.loads) > 0: template_params["cascade_data_out"] = self.cascade_data_out.name

		#Hook up weight cascade in
		if self.cascade_weight_in.drivers[0] != self.default_cascade_weight_in:
			template_params["cascade_weight_in"] = self.cascade_weight_in.drivers[0].name
		else:
			if sim: template_params["cascade_weight_in"] = str(NX_CASCADE_WEIGHT_SIZE) + "'d0"	
			else: template_params["cascade_weight_in"] = "" #Quartus has an error if we try to set the weight cascade to 0s

		#Hook up weight cascade out
		template_params["cascade_weight_out"] = ""
		if len(self.cascade_weight_out.loads) > 0: template_params["cascade_weight_out"] = self.cascade_weight_out.name

		#Hook up accumulate. In our TensorConv module we accumulate in an external memory so we don't use this signal, but it's here for completeness
		assert(not(self.acc_en.drivers[0] != self.default_acc_en and self.cascade_data_in.drivers[0] != self.default_cascade_data_in)) #Can't support both accumulates and cascade data in on NX tensor block as per spec
		if self.acc_en.drivers[0] != self.default_acc_en: template_params["acc_en"] =  self.acc_en.drivers[0].name
		else: template_params["acc_en"] = "1'b0"

		#Hook up any remaining control signals
		assert(len(self.load_ping_pong_reg.drivers) > 0 and len(self.load_buf_sel.drivers) > 0) #These control signals must have a driver in tensor mode as they control which values we are loading/multiplying against 
		template_params["load_buf_sel"] = self.load_buf_sel.drivers[0].name
		template_params["load_bb_one"] = self.load_ping_pong_reg.drivers[0].name + "[0]"
		template_params["load_bb_two"] = self.load_ping_pong_reg.drivers[0].name + "[1]"

		#Assign output based on spec and if we are producing 24-bit floating-point outputs or 25-bit fixed-point
		if enable_bfloat:
			template_params["tensor_res_sel_1"] = "result_l[23:0]"
			template_params["tensor_res_sel_2"] = "{result_l[37:24], result_h[9:0]}"
			template_params["tensor_res_sel_3"] = "result_h[33:10]"
		else:
			template_params["tensor_res_sel_1"] = "result_l[24:0]"
			template_params["tensor_res_sel_2"] = "{result_h[11:0], result_l[37:25]}"
			template_params["tensor_res_sel_3"] = "result_h[36:12]"

		#Insert all the variables set above into a string to instantiate the tensor block (or simulation module)
		template = Template(r"""
		$tmp_res_wires
		wire [36:0] result_h;
		wire [37:0] result_l;

		$debug_wires
		
		$module_name #(
			.dsp_mode("$operation_mode"),
			.dsp_sel_int4("select_int8"),
			.dsp_fp32_sub_en("float_sub_disabled"),
			.dsp_cascade($cascade_enabled)
		)
		 tensorblock_instance (
			.ena($ena_s),
			.clk($clk_s),
			.data_in($data_in),
			.clr({$clear, $clear}),
			.result_l(result_l),
			.result_h(result_h),
			.load_buf_sel($load_buf_sel),
			.mode_switch(1'b0),
			.load_bb_one($load_bb_one),
			.load_bb_two($load_bb_two),
			.feed_sel($tensor_mode),
			.zero_en(1'b0),
			.shared_exponent($shared_exp),
			.acc_en($acc_en),
			//Note: the term 'weight' refers to what we're loading in the ping pong registers. In our conv module we actually load activations
			.cascade_weight_in($cascade_weight_in),
			.cascade_weight_out($cascade_weight_out),
			.cascade_data_in($cascade_data_in),
			.cascade_data_out($cascade_data_out)
);

		//Assign results like in tensor block spec
		assign $result_0 = $tensor_res_sel_1;
		assign $result_1 = $tensor_res_sel_2;
		assign $result_2 = $tensor_res_sel_3;



		""")

		return template.substitute(**template_params)


class DCFIFO(Module):
	"""
		This is a wrapper around the Quartus DCFIFO IP
	"""
	def __init__(self, rdclk, wrclk, reset, bits, depth, almost_full = 0, showahead = "OFF", kind="dcfifo", **kwargs):
		super(DCFIFO, self).__init__("dcfifo", kind=kind, **kwargs)
		kwargs = self.kwargs
		inst = self.inst
		self.rdclk = rdclk
		self.wrclk = wrclk
		self.reset = reset
		self.width = bits
		self.depth = depth
		self.showahead = showahead
		
		if (clog2(depth) != math.log2(depth)):
			print("Error: DCFIFO only supports depths that are powers of 2")
			exit(1)
		assert(showahead == "ON" or showahead == "OFF")
		with ModuleScope(self):
			#Instantiate PhantomFlop to propagate the reset and clock signals
			with ParamScope(intended_clock_crossing=True):
				inst(PhantomFlop, clock=rdclk)
				inst(PhantomFlop, clock=wrclk)
			rdclk.add_sink(self)
			wrclk.add_sink(self)
			self.async_reset = inst(Logic, bits=1, signed=False, name="async_reset") #We do this for the name to show properly in the megafunction string
			self.async_reset.set_driver(self.reset)
			with ModuleScope(self):
				with ParamScope(bits=1):
					self.w_en = inst(Logic, name="w_en")
					self.r_en = inst(Logic, name="r_en")
					self.empty = inst(Logic, name="empty")
					self.full = inst(Logic, name="full")
					self.almost_full = inst(Logic, name="almost_full")
				with ParamScope(bits=self.width):
					self.w_data = inst(Logic, name="w_data")
					self.r_data = inst(Logic, name="r_data")
				with ParamScope(bits=clog2(self.depth)):
					self.wr_used = inst(Logic, signed=False, name="used_words")
					max_wr_almost_full = inst(Constant, signed = False, value=depth-almost_full, name="max_used_words")
				self.almost_full.set_driver(inst(GE, self.wr_used, max_wr_almost_full))
					

				self.requires_driver = [self.w_en, self.r_en, self.w_data]

	def get_verilog(self, t="  "):
		verilog = self.get_module_header(t)
		inputs = self.get_inputs()
		outputs = self.get_outputs()
		internal = self.get_internal_signals() + self.clocks
		to_declare = inputs + outputs + internal
		for s in to_declare:
			verilog += t + s.get_declaration_string() + "\n"


		verilog += self._megafunction_string(t)

		verilog += "endmodule // " + self.name
		return verilog

	def _megafunction_string(self, t="  "):
		instance_name = self.name + "_i"
		
		self.addr_width = clog2(self.depth)
		verilog = ""
		verilog += self.get_combinational_assignments(t=t)
		verilog += f"""
		dcfifo  {instance_name} (
					.aclr ({self.async_reset.name}),
					.data ({self.w_data.name}),
					.rdclk ({self.rdclk.name}),
					.rdreq ({self.r_en.name}),
					.wrclk ({self.wrclk.name}),
					.wrreq ({self.w_en.name}),
					.q ({self.r_data.name}),
					.rdempty ({self.empty.name}),
					.wrfull ({self.full.name}),
					.eccstatus (),
					.rdfull (),
					.rdusedw (),
					.wrempty (),
					.wrusedw ({self.wr_used.name}));
		defparam
			{instance_name}.enable_ecc  = "FALSE",
			{instance_name}.intended_device_family  = "Stratix 10",
			{instance_name}.lpm_hint  = "DISABLE_DCFIFO_EMBEDDED_TIMING_CONSTRAINT=TRUE",
			{instance_name}.lpm_numwords  = {self.depth},
			{instance_name}.lpm_showahead  = "{self.showahead}",
			{instance_name}.lpm_type  = "dcfifo",
			{instance_name}.lpm_width  = {self.width},
			{instance_name}.lpm_widthu  = {self.addr_width},
			{instance_name}.overflow_checking  = "ON",
			{instance_name}.rdsync_delaypipe  = 6,
			{instance_name}.read_aclr_synch  = "OFF",
			{instance_name}.underflow_checking  = "ON",
			{instance_name}.use_eab  = "ON",
			{instance_name}.write_aclr_synch  = "OFF",
			{instance_name}.wrsync_delaypipe  = 6;
		"""
		return verilog	



class PriorityArbiter(Module):
	def __init__(self, n_requesters, kind="priority_arbiter", **kwargs):
		super(PriorityArbiter, self).__init__("priority_arbiter", kind=kind, **kwargs)
		inst = self.inst

		with ModuleScope(self):
			self.inputs = [inst(Logic, 1) for i in range(n_requesters)]
			self.outputs = [inst(Logic, 1) for i in range(n_requesters)]

			for i in range(n_requesters):
				if i == 0:
					self.outputs[i].set_driver(self.inputs[i])
				elif i == 1:
					self.outputs[i].set_driver(inst(AND, self.inputs[i], inst(NOT, self.inputs[0])))
				else:
					self.outputs[i].set_driver(inst(AND, self.inputs[i], inst(NOT, inst(Concat, *self.inputs[:i]))))

class BufferedArbitratedShiftRegister(Module):
	def __init__(self, n_requesters, bits, kind="arbitrated_shift_register", **kwargs):
		super(ArbitratedShiftRegister, self).__init__("arbitrated_shift_register", kind=kind, **kwargs)
		inst = self.inst

		with ModuleScope(self):
			with ParamScope(bits=bits):
				self.d_in         = [inst(Logic, name="data_in_" + str(i)) for i in range(n_requesters)]
				self.d_shift_out  = inst(Logic, kwargs["bits"], name="d_shift_out")
				self.input_buffer = [inst(Flop, name="input_buffer_" + str(i)) for i in range(n_requesters)]
				self.sr_buffer    = [inst(Flop, name="sr_buffer_" + str(i)) for i in range(n_requesters)]

			with ParamScope(bits=1, signed=False):
				self.r_in         = [inst(Logic, 1, name="requeste_in_" + str(i)) for i in range(n_requesters)]

				self.gnt_shift_out= inst(Logic, 1, name="gnt_shift_out")
				self.r_shift_out  = inst(Logic, 1, name="r_shift_out")

				z = inst(Constant, 0)

				self.buffer_used  = [inst(Flop, reset_driver=z, name="buffer_used_" + str(i)) for i in range(n_requesters)]
				self.sr_used      = [inst(Flop, reset_driver=z, name="sr_used_" + str(i)) for i in range(n_requesters)]


			self.d_shift_out.set_driver(self.sr_buffer[-1])
			self.r_shift_out.set_driver(self.sr_used[-1])

			self.arbiters = []
			for i in range(n_requesters):
				buffer_keeper_mux = inst(Mux, bits, self.buffer_used[i], self.d_in[i], self.input_buffer[i])
				self.input_buffer[i].set_driver(buffer_keeper_mux)
				if i == 0:
					arb = inst(PriorityArbiter, 2)
					arb.inputs[0].set_driver(self.sr_used[0])
					arb.inputs[1].set_driver(self.buffer_used[0])
					sr_keeper_mux = inst(Mux, bits, self.sr_used[i], self.input_buffer[i],  self.sr_buffer[i], name="sr_keeper_mux_" + str(i))
				else:
					arb = inst(PriorityArbiter, 3)
					arb.inputs[0].set_driver(self.sr_used[i])
					arb.inputs[1].set_driver(self.buffer_used[i])
					arb.inputs[2].set_driver(self.sr_used[i-1])
					self.sr_used[i-1].set_driver(inst(OR, inst(AND, arbiters[-1].outputs[0], inst(NOT, arb.outputs[2])), *arbiters[-1].outputs[1:]))
					sr_input_select_mux = inst(Mux, bits, arb.outputs[1], self.sr_buffer[i-1], self.input_buffer[i], name="sr_input_select_mux_" + str(i-1))
					sr_keeper_mux = inst(Mux, bits, self.sr_used[i], sr_input_select_mux,  self.sr_buffer[i], name="sr_keeper_mux_" + str(i))
				self.buffer_used[i].set_driver(
					inst(OR,
						inst(AND,
							self.buffer_used,
							inst(NOT, arb.outputs[1])),
						inst(AND,
							inst(NOT, self.buffer_used),
							self.r_in[i])))
				self.sr_buffer[i].set_driver(sr_keeper_mux)
				self.arbiters.append(arb)
			self.sr_used[-1].set_driver(
				inst(OR,
					inst(AND,
						arbiters[-1].outputs[0],
						inst(NOT, self.gnt_shift_out)),
					*arbiters[-1].outputs[1:]))

class DownCounter(Module):
	"""
	DownCounter

	A counter that can be reset to a programmable value (``reset_value``)

	Args:
		max_possible_value (int):
			The largest value the counter can have.  This is used to determine its bit width.

	Attributes:
		reset_signal:
			When you assert this signal, the counter will be set to ``reset_value``

		reset_value:
			Drive this signal to whatever value you would like the counter to count down from.

		enable:
			Assert this when you would like the counter to count down.

		is_done:
			Asserted when the counter equals 0.

	"""
	def __init__(self, max_possible_value, **kwargs):
		super(DownCounter, self).__init__("down_counter", kind="down_counter", **kwargs)
		inst = self.inst
		with ModuleScope(self):
			bits = clog2(max_possible_value+1)
			if bits == 0:
				bits = 1
			with ParamScope(bits=1):
				self.reset_signal = inst(Logic, name="reset_signal")
				self.enable = inst(Logic, name="enable")
			with ParamScope(bits=bits, reset=self.reset_signal):
				self.reset_value = inst(Logic, name="reset_value")
				self.current_value = inst(Flop, reset_driver=self.reset_value, name=self.name)
				self.increment_value = inst(Constant, -1, name="increment_value")
				target = inst(Constant, 0, name="target")
			self.is_done = inst(EQ, self.current_value, target, name="is_done")

			to_add = inst(MuxV2, self.enable, target, self.increment_value)
			next_value = (to_add + self.current_value)[bits-1:0]
			#next_value = inst(MuxV2, kwargs["reset"], (to_add + self.current_value)[bits-1:0], target)
			self.current_value.set_driver(next_value)

			self.requires_driver = [self.reset_signal, self.enable, self.reset_value]

class ContinuousCounter(Module):
	def __init__(self, reset_value, end_reset_value, increment_value, end_value, reset, kind="continuous_counter", **kwargs):
		super(ContinuousCounter, self).__init__("counter", kind=kind, **kwargs)
		inst = self.inst
		with ModuleScope(self):
			assert(((end_value - reset_value) % increment_value) == 0)
			bit_width = clog2(end_value+1)
			if bit_width == 0:
				bit_width = 1
			self._bit_width = bit_width

			with ParamScope(bits=1):
				self.reset_signal           = inst(Logic, name="reset_signal")
			with ParamScope(bits=bit_width):
				self.end_value              = inst(Constant, end_value, name="end_value")
				self.reset_value            = inst(Constant, reset_value, name="reset_value")
				self.end_reset_value        = inst(Constant, end_reset_value, name="end_reset_value")
				self.increment_value        = inst(Constant, increment_value, name="increment_value")
				self.current_value          = inst(Flop, reset=self.reset_signal, reset_driver=self.reset_value, signed=False, name=self.name)
			self.change = inst(
				MuxV2, 
				self.is_done,
				self.increment_value,
				inst(Constant, end_reset_value - end_value, bits=bit_width),
				name="change")
			self.counter_plus_change    = inst(ADD, self.change, self.current_value, name="counter_plus_change")[self._bit_width-1:0]
			self.current_value.set_driver(self.counter_plus_change)
			self.is_reset_value         = inst(EQ, self.current_value, self.reset_value, name="is_reset_value")
			self.reset_signal.set_driver(reset)

	@property
	def is_done(self):
		with ModuleScope(self):
			return self.inst(EQ, self.current_value, self.end_value, name="is_done")
	

class Counter(Module):
	"""
	Counter

	A simple counter that counts from a constant value to a constant value and resets
	once the counter is at the end value and the increment condition is true

	Args:
		reset_value (int):
			The value of the counter after it resets or increments from end_value

		increment_value (int):
			The amount the counter should increment when ``increment_condition`` is asserted

		end_value (int):
			The value at which the counter should reset to reset_value

		load_value(bool):
			If this is enabled, you can load a specific value into the counter instead of incrementing
			You should set the drivers for the enable_load_value control signal and load_value data signal 

	Attributes:
		increment_condition:
			Whenever the driver asserts this signal, the counter will increment by increment_value

		current_value:
			The current value of the counter

		is_done:
			A signal that is asserted whenever the counter equals ``end_value``

		is_reset_value:
			A signal that is asserted whenever the counter equals ``reset_value``

		done_and_incrementing:
			A signal that is asserted whenever ``increment_condition`` and ``is_reset_value`` are both
			asserted.
	"""
	def __init__(self, reset, reset_value, increment_value, end_value, load_value=False, kind="counter", **kwargs):
		super(Counter, self).__init__("counter", kind=kind, **kwargs)
		inst = self.inst
		with ModuleScope(self):
			assert(((end_value - reset_value) % increment_value) == 0)
			bit_width = clog2(end_value + 1)
			if bit_width == 0:
				bit_width = 1
			self._bit_width = bit_width

			if reset_value == end_value:
				increment_value = 0

			with ParamScope(bits=1):
				self.reset_signal           = inst(Logic, name="reset_signal")
				self.increment_condition    = inst(Logic, name="increment_condition")
				if load_value: self.enable_load_value    = inst(Logic, name="enable_load_value")
			with ParamScope(bits=bit_width):
				self.end_value              = inst(Constant, end_value, name="end_value")
				self.reset_value            = inst(Constant, reset_value, name="reset_value")
				self.increment_value        = inst(Constant, increment_value, name="increment_value")
				if load_value: self.load_value    = inst(Logic, name="load_value")
				self.current_value          = inst(Flop, reset=self.reset_signal, reset_driver=self.reset_value, signed=False, name=self.name)
			self.change                 = inst(
				MuxV2, 
				self.increment_condition,
				inst(Constant, 0, bits=bit_width),
				self.increment_value, name="change")
			self.counter_plus_change    = inst(ADD, self.change, self.current_value, name="counter_plus_change")[self._bit_width-1:0]
			#If we want to load a specific value that should take priority over incrementing the counter
			if load_value: self.current_value.set_driver(inst(MuxV2, self.enable_load_value, self.counter_plus_change, self.load_value, name="select_current_value"))
			else: self.current_value.set_driver(self.counter_plus_change)
			self.is_reset_value         = inst(EQ, self.current_value, self.reset_value, name="is_reset_value")
			self.reset = reset
			self.reset_signal.set_driver(
				inst(OR, reset, self.done_and_incrementing))

			if load_value: self.requires_driver = [self.enable_load_value, self.load_value]


	@property
	def is_done(self):
		with ModuleScope(self):
			return self.inst(EQ, self.current_value, self.end_value, name="is_done")
	

	@property
	def done_and_incrementing(self):
		with ModuleScope(self):
			return self.inst(AND, self.is_done, self.increment_condition, name="done_and_incrementing")

	def get_bit_width(self):
		return self._bit_width

	def update_reset_condition(self, extra_reset_condition):
		self.reset_signal.set_driver(self.inst(OR, self.reset, self.done_and_incrementing, extra_reset_condition))

	def set_increment_condition(self, increment_condition):
		self.increment_condition.set_driver(increment_condition)
		# need to add this signal to the inputs as well

class ModuloCounter(Module):
	"""
	ModuloCounter
	
	A counter with an increment value greater than 1 that subtracts its end value once it exceeds that
	end value insead of simply reseting to the reset value.  The modulo operation takes an extra cycle,
	so the counter value is not valid for one cycle while it exceeds the modulo value.

	Args:
		increment_value (int):
			The amount the counter should increment when ``increment_condition`` is asserted

		modulo_value (int):
			If the counter exceeds this value it will decrement by this value.

	Attributes:
		increment_condition:
			Whenever the driver asserts this signal, the counter will increment by increment_value

		current_value:
			The current value of the counter
	"""
	def __init__(self, reset, reset_value, increment_value, modulo_value, kind="modulo_counter", **kwargs):
		super(ModuloCounter, self).__init__("modulo_counter", kind=kind, **kwargs)
		inst = self.inst
		with ModuleScope(self):
			bit_width = clog2(modulo_value + increment_value)
			if bit_width == 0:
				bit_width = 1
			self._bit_width = bit_width

			if reset_value == modulo_value:
				increment_value = 0

			self.reset_signal           = inst(Logic, 1, name="reset_signal")
			self.increment_condition    = inst(Logic, 1, name="increment_condition")
			with ParamScope(bits=bit_width):
				self.modulo_value           = inst(Constant, modulo_value, name="modulo_value")
				self.reset_value            = inst(Constant, reset_value, name="reset_value")
				self.increment_value        = inst(Constant, increment_value, name="increment_value")
				self.negative_modulo_value  = inst(Constant, -modulo_value, name="negative_modulo_value")
				self.increment_and_modulo   = inst(Constant, increment_value-modulo_value, name="increment_and_modulo")
				self.current_value          = inst(Flop, reset=self.reset_signal, reset_driver=self.reset_value, signed=False, name=self.name)
			self.greater_than_modulo_value  = self.current_value >= self.modulo_value
			self.change                 = inst(
				Mux, self._bit_width, 
				inst(Concat, self.increment_condition, self.greater_than_modulo_value, name="select_change"),
				inst(Constant, 0, bits=bit_width),
				self.increment_value, self.negative_modulo_value, self.increment_and_modulo, name="change")
			self.counter_plus_change    = inst(ADD, self.change, self.current_value, name="counter_plus_change")[self._bit_width-1:0]
			self.current_value.set_driver(self.counter_plus_change)
			self.reset_signal.set_driver(
				inst(OR, reset, self.done_and_incrementing))

	@property
	def is_done(self):
		with ModuleScope(self):
			return self.inst(EQ, self.current_value, self.modulo_value, name="is_done")
	

	@property
	def done_and_incrementing(self):
		with ModuleScope(self):
			return self.inst(AND, self.is_done, self.increment_condition, name="done_and_incrementing")


	def set_increment_condition(self, increment_condition):
		self.increment_condition.set_driver(increment_condition)
		# need to add this signal to the inputs as well


class UpDownCounter(Module):
	"""
	UpDownCounter

	A counter that can be both incremented and decremented by constant values.  This counter never resets
	itself, so the user must insure that the counter never exceeds the end value and is never less than
	the reset_value.
	"""
	def __init__(self, reset, reset_value, increment_value, decrement_value, end_value, kind="up_down_counter", **kwargs):
		super(UpDownCounter, self).__init__("up_down_counter", kind=kind, **kwargs)
		kwargs = self.kwargs
		inst = self.inst
		with ModuleScope(self):

			assert(((end_value - reset_value) % increment_value) == 0)
			bit_width = clog2(end_value + 1)
			if bit_width == 0:
				bit_width = 1
			self._bit_width = bit_width

			if reset_value == end_value:
				increment_value = 0

			self.reset_signal            = inst(Logic, bits=1, name="reset_signal")
			self.end_value               = inst(Constant, end_value, bits=bit_width, name="end_value")
			self.reset_value             = inst(Constant, reset_value, bits=bit_width, name="reset_value")
			self.increment_value         = inst(Constant, increment_value, bits=bit_width, name="increment_value")
			self.decrement_value         = inst(Constant, decrement_value, bits=bit_width, name="decrement_value")
			self.increment_and_decrement = inst(Constant, increment_value + decrement_value, bits=bit_width, name="increment_plus_decrement")
			self.current_value           = inst(Flop, reset=self.reset_signal, bits=bit_width, reset_driver=self.reset_value, signed=False, name=self.name)
			self.increment_condition     = inst(Logic, bits=1, name="increment_condition")
			self.decrement_condition     = inst(Logic, bits=1, name="decrement_condition")
			self.change                  = inst(MuxV2,
				inst(Concat, self.increment_condition, self.decrement_condition),
				inst(Constant, 0, bits=bit_width),
				self.increment_value,
				self.decrement_value,
				self.increment_and_decrement, name="change")
			self.next_value             = inst(ADD, self.current_value, self.change, name="next_value")[self._bit_width-1:0]
			self.current_value.set_driver(self.next_value)
			self.is_done                = inst(EQ, self.current_value, self.end_value, name="is_done")
			self.reset_signal.set_driver(reset)


	def set_increment_condition(self, increment_condition):
		self.increment_condition.set_driver(increment_condition)

	def set_decrement_condition(self, decrement_condition):
		self.decrement_condition.set_driver(decrement_condition)

class MultiChangeCounter(Module):
	"""
	dl.MultiChangeCounter:
	  A counter that can have multiple change_values
	"""
	def __init__(self, reset_value, change_values, end_value, kind="multi_change_counter", **kwargs):
		super(MultiChangeCounter, self).__init__("multi_change_counter", kind=kind, **kwargs)
		kwargs = self.kwargs
		inst = self.inst
		with ModuleScope(self):

			bit_width = clog2(end_value + 1)
			if bit_width == 0:
				bit_width = 1
			self._bit_width = bit_width

			if reset_value == end_value:
				increment_value = 0

			total_change_values = [0]
			for i,v in enumerate(change_values):
				num_total_change_values = len(total_change_values)
				for j in range(num_total_change_values):
					total_change_values.append(v + total_change_values[j])
			self.change_constants = [inst(Constant, value=v, bits=bit_width, name="v_" + str(i)) for i,v in enumerate(total_change_values)]
			self.change_signals = [inst(Logic, bits=1, name="change_control_" + str(i)) for i in range(len(change_values))]
			self.select = inst(Concat, *self.change_signals, name="select_change")
			self.change = inst(MuxV2, self.select, *self.change_constants, name="change")

			self.reset_value             = inst(Constant, reset_value, bits=bit_width, name="reset_value")
			self.current_value           = inst(Flop, bits=bit_width, reset_driver=self.reset_value, signed=False, name=self.name)
			self.next_value              = inst(ADD, self.current_value, self.change, name="next_value")[self._bit_width-1:0]
			self.current_value.set_driver(self.next_value)
			self.end_value               = inst(Constant, end_value, bits=bit_width, name="end_value")
			self.is_done                 = inst(EQ, self.current_value, self.end_value)
			self.is_reset_value          = inst(EQ, self.current_value, self.reset_value)

			self.requires_driver = self.change_signals

class ModuloMultiChangeCounter(Module):
	def __init__(self, reset_value, change_values, end_value, modulo_value, tolerable_delay=0, kind="modulo_counter_multi_change_counter", **kwargs):
		super(ModuloMultiChangeCounter, self).__init__("modulo_multi_change_counter", kind="modulo_multi_change_counter", **kwargs)
		inst = self.inst
		with ModuleScope(self):
			self.change_signals = [inst(Logic, bits=1, name="change_control_" + str(i)) for i in range(len(change_values))]
			change_values = [-modulo_value] + change_values
			multi_change_counter = inst(MultiChangeCounter,
				reset_value=reset_value,
				change_values=change_values,
				end_value=end_value)
			modulo_constant = inst(Constant, modulo_value, bits=multi_change_counter._bit_width, name="modulo_value")
			counter_gte_modulo = (multi_change_counter.current_value.delay(tolerable_delay) >= modulo_constant)
			for i in range(tolerable_delay):
				counter_gte_modulo = inst(AND, inst(NOT, counter_gte_modulo).delay(1), counter_gte_modulo)
			change_signals = [counter_gte_modulo] + self.change_signals
			self.current_value = multi_change_counter.current_value
			for s, t in zip(change_signals, multi_change_counter.change_signals):
				t.set_driver(s)
			self.requires_driver = self.change_signals

class FanoutRAM(Module):
	def __init__(self, bits, depth, clock=None, read_clock=None, write_clock=None, kind="fanout_ram", **kwargs):
		super(FanoutRAM, self).__init__("fanout_ram", kind=kind, **kwargs)
		if read_clock is None or write_clock is None:
			read_clock = clock
			write_clock = clock
		inst = self.inst
		with ModuleScope(self):
			self._build_physical_ram(depth, bits, read_clock, write_clock)
			self.requires_driver = [self.r_addr, self.r_en]

	def _build_physical_ram(self, depth, bits, read_clock, write_clock):
		inst = self.inst
		pmd = RAM.physically_mapped_dims(bits, depth)

		ram_lists = []
		y_base = 0
		depths = []

		for x_count, y_count, size in zip(*pmd):
			remaining_depth = depth
			for y in range(y_count):
				x_list = []
				x_base = 0
				if size[1] < remaining_depth:
					depths.append(size[1])
					remaining_depth -= size[1]
				else:
					depths.append(remaining_depth)
					remaining_depth = 0
				for x in range(x_count):
					x_end = min(x_base+size[0], bits)
					x_list.append(inst(RAM, read_clock=read_clock, write_clock=write_clock, bits=x_end-x_base, depth=depths[-1], name="ram_x_" + str(x) + "_y_" + str(y), enable_force_to_zero=True, register_output=True))
					x_base += size[0]
				y_base += size[1]
				ram_lists.append(x_list)

		def get_fanout_for_signal(sig, fanouts, name, levels=-1, last_stage_is_flop=True):
			nonlocal inst
			if levels == -1:
				kwargs = dict(max_fanout=8)
			else:
				kwargs = dict(levels=levels)
			if last_stage_is_flop:
				last_stage_type = Flop
				additional_delay = 1
			else:
				last_stage_type = Logic
				additional_delay = 0

			fanouts = [inst(last_stage_type, bits=sig._bit_width, name=sig.name + "_" + name + "_fanout_" + str(i)) for i in range(fanouts)]
			pipe = inst(PipelinedFanout,
				sig,
				*fanouts,
				**kwargs,
				name=sig.name + "_" + name + "_fanout_pipe")
			return fanouts, pipe.get_added_delay() + additional_delay

		addr_bits = clog2(depth)
		self.r_addr = inst(Logic, bits=addr_bits, signed=False, name="r_addr")
		self.r_en = inst(Logic, bits=1, signed=False, name="r_en")
		self.w_addr = inst(Logic, bits=addr_bits, signed=False, name="w_addr")
		self.w_en = inst(Logic, bits=1, signed=False, name="w_en")
		self.w_data = inst(Logic, bits=bits, name="w_data")

		levels = math.ceil(math.log(len(ram_lists[0]), 8))
		x_pipe_cycles = levels

		y_count = len(ram_lists)
		with ParamScope(clock=read_clock):
			r_addr_y_fans, y_pipe_cycles = get_fanout_for_signal(self.r_addr, y_count, "y")
			r_en_y_fans, _ = get_fanout_for_signal(self.r_en, y_count, "y")
		with ParamScope(clock=write_clock):
			w_addr_y_fans, _ = get_fanout_for_signal(self.w_addr, y_count, "y")
			w_en_y_fans, _ = get_fanout_for_signal(self.w_en, y_count, "y")
			w_data_y_fans, _ = get_fanout_for_signal(self.w_data.delay(x_pipe_cycles), y_count, "y")

		cumulative_base = 0
		maximum_depth = pmd[2][0][1]
		previous_iteration_depth = maximum_depth
		decoded_read_enables = []
		partial_r_addrs = []
		decoded_write_enables = []
		partial_w_addrs = []
		if len(depths) > 1:
			for r_en, r_addr, w_en, w_addr, _depth in zip(r_en_y_fans, r_addr_y_fans, w_en_y_fans, w_addr_y_fans, depths):
				if previous_iteration_depth != _depth:
					cumulative_base <<= clog2(previous_iteration_depth) - clog2(_depth)
				previous_iteration_depth = _depth
				ce_base = clog2(_depth)
				ce_bits = r_addr[r_addr._bit_width-1:ce_base]
				w_ce_bits = w_addr[w_addr._bit_width-1:ce_base]
				decoded_read_enables.append(inst(AND, r_en, inst(EQ, ce_bits, cumulative_base)))
				decoded_write_enables.append(inst(AND, w_en, inst(EQ, w_ce_bits, cumulative_base)))
				partial_r_addrs.append(r_addr[ce_base-1:0])
				partial_w_addrs.append(w_addr[ce_base-1:0])
				cumulative_base += 1
		else:
			decoded_read_enables = r_en_y_fans
			partial_r_addrs = r_addr_y_fans
			decoded_write_enables = w_en_y_fans
			partial_w_addrs = w_addr_y_fans

		concatenated_r_data = []
		for i, (r_en, r_addr, w_en, w_addr, w_data, ram_list) in enumerate(zip(decoded_read_enables, partial_r_addrs, decoded_write_enables, partial_w_addrs, w_data_y_fans, ram_lists)):
			with ParamScope(clock=read_clock):
				r_en_x_fanouts, _ = get_fanout_for_signal(r_en, len(ram_list), "x" + str(i), levels, last_stage_is_flop=False)
				r_addr_x_fanouts, _ = get_fanout_for_signal(r_addr, len(ram_list), "x" + str(i), levels, last_stage_is_flop=False)
			with ParamScope(clock=write_clock):
				w_en_x_fanouts, _ = get_fanout_for_signal(w_en, len(ram_list), "x" + str(i), levels, last_stage_is_flop=False)
				w_addr_x_fanouts, _ = get_fanout_for_signal(w_addr, len(ram_list), "x" + str(i), levels, last_stage_is_flop=False)
			r_data_to_concat = []
			x_base = 0
			for r in ram_list:
				r_data_to_concat.append(r.r_data)
				r.w_data.set_driver(w_data[r.w_data._bit_width + x_base - 1: x_base])
				x_base += r.w_data._bit_width
			concatenated_r_data.append(inst(Concat, *r_data_to_concat))
			for re, ra, raf, ref in zip([r.r_en for r in ram_list], [r.r_addr for r in ram_list], r_addr_x_fanouts, r_en_x_fanouts):
				ra.set_driver(raf)
				re.set_driver(ref)
			for we, wa, waf, wef in zip([r.w_en for r in ram_list], [r.w_addr for r in ram_list], w_addr_x_fanouts, w_en_x_fanouts):
				wa.set_driver(waf)
				we.set_driver(wef)

		read_output_stages = 0
		to_or = concatenated_r_data
		max_or_per_stage = 6
		with ParamScope(clock=read_clock):
			while len(to_or) > 1:
				read_output_stages += 1
				next_to_or = []
				for i in range(0, len(to_or), max_or_per_stage):
					if i == (len(to_or) - 1):
						next_to_or.append(to_or[-1].delay(1))
					else:
						next_to_or.append(inst(BOR, *to_or[i:i+max_or_per_stage]).delay(1))
				to_or = next_to_or

		mem_read_latency = 2
		self.read_delay = y_pipe_cycles + x_pipe_cycles + mem_read_latency + read_output_stages
		self.r_data = to_or[0]


class FanoutROM(Module):
	def __init__(self, bits, content_list, physically_mapped=False, kind="fanout_rom", **kwargs):
		super(FanoutROM, self).__init__("fanout_rom", kind=kind, **kwargs)
		inst = self.inst
		with ModuleScope(self):
			if physically_mapped:
				self._build_physical_ram(content_list, bits)
			else:
				rom_size = bits * len(content_list)
				m20k_count_estimate = math.ceil(rom_size / (10 * 2048))
				output_mux_input_estimate = math.ceil(len(content_list) / 2048)
				# BRAM read cycles + M20K selection muxing + a factor to account for how spread
				# out a large RAM will be
				read_cycles = 2 + math.ceil(math.log(output_mux_input_estimate, 4)) + math.floor(math.sqrt(m20k_count_estimate) * 0.1)
				control_fanout_depth = math.ceil(math.log(m20k_count_estimate*4, 4))

				self.rom = inst(ROM,
					bits=bits,
					content_list=content_list)
				self.rom.has_fanout_control = True

				self.r_addr = self.rom.r_addr.copy()
				self.r_en = self.rom.r_en.copy()
				self.r_addr.drivers = []
				self.r_en.drivers = []
				self.rom.r_addr.set_driver(self.r_addr.delay(control_fanout_depth, max_fan=4, final_fan=m20k_count_estimate, final_fan_of_1=True))
				self.rom.r_en.set_driver(self.r_en.delay(control_fanout_depth, max_fan=4, final_fan=m20k_count_estimate, final_fan_of_1=True))
				self.r_data = self.rom.r_data.delay(read_cycles - 1, auto_shift_register_recognition=False)
				self.read_delay = control_fanout_depth + read_cycles

				if m20k_count_estimate > 1:
					# Quartus removes the last stage of our address fanout and renames it to a register packed into the M20Ks, so we set
					# these settings to manually duplicate that register... which shockingly works :)
					get_sdc_filter_str = lambda ab: "*|" + "|".join([o.name + "_i" for o in reversed(self.get_owner_list()[:-2])]) + "|" + self.name + "_i|*|address_reg_" + ab + "[*]"
					Verilog.current_circuit.add_quartus_setting_lambda(
						lambda: "set_instance_assignment -name DUPLICATE_REGISTER " + str(m20k_count_estimate) + " -to " + '"' + get_sdc_filter_str("a") + '"')
					Verilog.current_circuit.add_quartus_setting_lambda(
						lambda: "set_instance_assignment -name DUPLICATE_REGISTER " + str(m20k_count_estimate) + " -to " + '"' + get_sdc_filter_str("b") + '"')

			self.requires_driver = [self.r_addr, self.r_en]

	def _build_physical_ram(self, content_list, bits):
		inst = self.inst
		pmd = RAM.physically_mapped_dims(bits, len(content_list))
		if not isinstance(content_list, BitArray):
			content_list = BitArray(bits, content_list)

		rom_lists = []
		y_base = 0
		depths = []

		#print(content_list)
		for x_count, y_count, size in zip(*pmd):
			for y in range(y_count):
				x_list = []
				x_base = 0
				depths.append(size[1])
				for x in range(x_count):
					x_end = min(x_base+size[0], bits)
					cl = content_list[y_base:y_base+size[1], x_end-1:x_base]
					#print(cl)
					x_list.append(inst(ROM, bits=x_end-x_base, content_list=cl, name="rom_x_" + str(x) + "_y_" + str(y), register_output=True, enable_force_to_zero=True))
					x_base += size[0]
				y_base += size[1]
				rom_lists.append(x_list)

		def get_fanout_for_signal(sig, fanouts, name, levels=-1, last_stage_is_flop=True):
			nonlocal inst
			if levels == -1:
				kwargs = dict(max_fanout=8)
			else:
				kwargs = dict(levels=levels)
			if last_stage_is_flop:
				last_stage_type = Flop
				additional_delay = 1
			else:
				last_stage_type = Logic
				additional_delay = 0

			fanouts = [inst(last_stage_type, bits=sig._bit_width, name=sig.name + "_" + name + "_fanout_" + str(i)) for i in range(fanouts)]
			pipe = inst(PipelinedFanout,
				sig,
				*fanouts,
				**kwargs,
				name=sig.name + "_" + name + "_fanout_pipe")
			return fanouts, pipe.get_added_delay() + additional_delay

		addr_bits = clog2(len(content_list))
		self.r_addr = inst(Logic, bits=addr_bits, signed=False, name="r_addr")
		self.r_en = inst(Logic, bits=1, signed=False, name="r_en")

		y_count = len(rom_lists)
		r_addr_y_fans, y_pipe_cycles = get_fanout_for_signal(self.r_addr, y_count, "y")
		r_en_y_fans, _ = get_fanout_for_signal(self.r_en, y_count, "y")

		cumulative_base = 0
		last_depth = pmd[2][0][1]
		decoded_read_enables = []
		partial_r_addrs = []
		if len(depths) > 1:
			for r_en, r_addr, depth in zip(r_en_y_fans, r_addr_y_fans, depths):
				if last_depth != depth:
					cumulative_base <<= clog2(last_depth) - clog2(depth)
				last_depth = depth
				ce_base = clog2(depth)
				ce_bits = r_addr[r_addr._bit_width-1:ce_base]
				decoded_read_enables.append(inst(AND, r_en, inst(EQ, ce_bits, cumulative_base)))
				partial_r_addrs.append(r_addr[ce_base-1:0])
				cumulative_base += 1
		else:
			decoded_read_enables = r_en_y_fans
			partial_r_addrs = r_addr_y_fans

		levels = math.ceil(math.log(len(rom_lists[0]), 8))
		concatenated_r_data = []
		for i, (r_en, r_addr, rom_list) in enumerate(zip(decoded_read_enables, partial_r_addrs, rom_lists)):
			r_en_x_fanouts, _ = get_fanout_for_signal(r_en, len(rom_list), "x" + str(i), levels, last_stage_is_flop=False)
			r_addr_x_fanouts, _ = get_fanout_for_signal(r_addr, len(rom_list), "x" + str(i), levels, last_stage_is_flop=False)
			r_data_to_concat = []
			for r in rom_list:
				r_data_to_concat.append(r.r_data)
			concatenated_r_data.append(inst(Concat, *r_data_to_concat))
			for re, ra, raf, ref in zip([r.r_en for r in rom_list], [r.r_addr for r in rom_list], r_addr_x_fanouts, r_en_x_fanouts):
				r_addr_fanout = raf[ra._bit_width-1:0]
				ra.set_driver(r_addr_fanout)
				re.set_driver(ref)

		read_output_stages = 0
		BOR_tree_input_delay = 1
		to_or = [c.delay(BOR_tree_input_delay) for c in concatenated_r_data]
		max_or_per_stage = 6
		while len(to_or) > 1:
			read_output_stages += 1
			next_to_or = []
			for i in range(0, len(to_or), max_or_per_stage):
				if i == (len(to_or) - 1):
					next_to_or.append(to_or[-1].delay(1))
				else:
					next_to_or.append(inst(BOR, *to_or[i:i+max_or_per_stage]).delay(1))
			to_or = next_to_or

		mem_read_latency = 2
		x_pipe_cycles = levels
		self.read_delay = y_pipe_cycles + x_pipe_cycles + mem_read_latency + BOR_tree_input_delay + read_output_stages
		self.r_data = to_or[0]



class ROMLatencyHider(Module):
	def __init__(self, rom, **kwargs):
		super(ROMLatencyHider, self).__init__("rom_latency_hider", kind="rom_latency_hider", **kwargs)
		inst = self.inst
		with ModuleScope(self):
			num_regs = 4
			self.next = inst(Logic, bits=1, name="next")
			internal_next = inst(Logic, bits=1, name="internal_next")
			self.read = inst(OR, self.next, internal_next, name="read")
			rom.r_en.set_driver(self.read)
			r_data_d1 = inst(Flop, driver=rom.r_data, name="r_data_d1")

			read_d2 = inst(ShiftRegister, bits=1, depth=2)
			read_d2.d.set_driver(self.read)

			output_select = inst(Counter, reset_value=0, end_value=num_regs-1, increment_value=1, name="output_select")
			output_select.set_increment_condition(self.next)
			write_select = inst(Counter, reset_value=0, end_value=num_regs-1, increment_value=1, name="write_select")
			write_select.set_increment_condition(read_d2.q)
			with ParamScope(bits=write_select.current_value._bit_width):
				wes = [inst(AND, read_d2.q, inst(EQ, write_select.current_value, inst(Constant, i)), name="we") for i in range(num_regs)]
			regs = [inst(Flop, bits=rom.r_data._bit_width) for _ in range(num_regs)]
			internal_muxes = [inst(MuxV2, we, r, r_data_d1) for r,we in zip(regs, wes)]
			for m,r in zip(internal_muxes, regs):
				r.set_driver(m)
			states = ["IDLE", "HAVE_NONE", "HAVE_ONE", "HAVE_TWO", "HAVE_THREE", "HAVE_FOUR"]
			edges = [
				["IDLE", "HAVE_NONE"],
				["HAVE_NONE", "HAVE_ONE"],
				["HAVE_ONE", "HAVE_TWO"],
				["HAVE_TWO", "HAVE_THREE"],
				["HAVE_THREE", "HAVE_FOUR"]
			]
			control = {
				"IDLE" : {"request_read" : 0, "valid" : 0},
				"HAVE_FOUR" : {"request_read" : 0, "valid" : 1}}
			state_machine = inst(StateMachine, states, edges, control)
			state_machine.set_default_for_control("request_read", 1)
			internal_next.set_driver(state_machine.c["request_read"])
			self.r_data = inst(MuxV2, output_select.current_value.delay(1)+self.next.delay(1), *[r.delay(1) for r in regs], name="r_data")
			self.valid = inst(Logic, bits=1, name="valid")
			self.valid.set_driver(state_machine.c["valid"])

			self.requires_driver = [self.next]

class SynchronizationChain(Module):
	def __init__(self, clock_a, clock_b, stages=6, **kwargs):
		super(SynchronizationChain, self).__init__("synchronization_chain", kind="synchronization_chain", **kwargs)
		inst = self.inst
		assert(stages > 1)
		with ModuleScope(self), ParamScope(synthesis_attributes="synthesis preserve"):
			with ParamScope(clock=clock_a):
				self.control_in = inst(Flop, bits=1, name="control_in")
			with ParamScope(clock=clock_b):
				self.cross_asserting = inst(Flop, driver=self.control_in, name="cross_asserting", intended_clock_crossing=True)
				self.control_out = self.cross_asserting.delay(stages-1)

	def get_sdc_constraints_string(self):
		asserting_filter_string = self.control_in.get_sdc_filter_string() + "[*]"
		cross_asserting_filter_string = self.cross_asserting.get_sdc_filter_string() + "[*]"
		string = ""
		"""
		string += "set_net_delay -from [get_registers {" + asserting_filter_string + "}]"
		string += " -to [get_registers {" + cross_asserting_filter_string + "}] "
		string += "-max -get_value_from_clock_period dst_clock_period -value_multiplier 3.0\n"
		string += "set_max_skew -from [get_keepers {" + asserting_filter_string + "}]"
		string += " -to [get_keepers {" + cross_asserting_filter_string + "}] "
		string += "-get_skew_value_from_clock_period min_clock_period -skew_value_multiplier 3.0\n"
		"""
		# Assuming we don't run at more than 1GHz we should be fine to set the max delay 
		# to 1ns * self.assert_cycles
		string += "set_max_delay -from [get_registers {" + asserting_filter_string + "}]"
		string += " -to [get_registers {" + cross_asserting_filter_string + "}] " + "2.000\n"
		# We hold the driving signal for multiple cycles, so there shouldn't be any hold
		# constraint
		string += "set_min_delay -from [get_registers {" + asserting_filter_string + "}]"
		string += " -to [get_registers {" + cross_asserting_filter_string + "}] -2.000\n"
		return string



class SingleCycleControlDomainCrossing(Module):
	def __init__(self, clock_a, clock_b, clock_a_reset, clock_b_reset, assert_cycles=10, **kwargs):
		super(SingleCycleControlDomainCrossing, self).__init__("single_cycle_control_domain_crossing", kind="single_cycle_control_domain_crossing", **kwargs)
		inst = self.inst
		self.assert_cycles = assert_cycles
		with ModuleScope(self), ParamScope(synthesis_attributes="synthesis preserve"):
			self.control_in = inst(Logic, bits=1, name="control_in")
			sync_chain = inst(SynchronizationChain, clock_a, clock_b)
			with ParamScope(clock=clock_a, reset=clock_a_reset):
				assert_counter = inst(Counter, reset_value=0, increment_value=1, end_value=assert_cycles)
				states = ["IDLE", "ASSERTED"]
				edges = [["IDLE", "ASSERTED", self.control_in],
					["ASSERTED", "IDLE", assert_counter.is_done]]
				in_state_machine = inst(StateMachine, states, edges, None, name="in_state_machine")
				assert_counter.set_increment_condition(in_state_machine.c["is_asserted"])
				with ParamScope(name="computation_delay"):
					asserting = in_state_machine.c["is_asserted"].delay(1)
				self.asserting = asserting
				sync_chain.control_in.set_driver(asserting)

			with ParamScope(clock=clock_b, reset=clock_b_reset):
				asserted = sync_chain.control_out
				states = ["IDLE", "MASTER_ASSERTING", "SLAVE_ASSERT"]
				edges = [["IDLE", "MASTER_ASSERTING", asserted],
					["MASTER_ASSERTING", "SLAVE_ASSERT", inst(NOT, asserted)],
					["SLAVE_ASSERT", "IDLE"]]
				out_state_machine = inst(StateMachine, states, edges, None, name="out_state_machine")
			self.control_out = out_state_machine.c["is_slave_assert"]




class Clock(Logic):
	def __init__(self, kind="clock", half_period=None, force_simple_assignment = False, **kwargs):
		super(Clock, self).__init__(1, None, kind=kind, assignment_type="complex", **kwargs)
		self.cycle = 0
		self.sinks = []
		self.special_properties["is_clock"] = True
		if half_period is None:
			self.half_period = 2
		else:
			warnings.warn("Clock frequency was overwritten. This will have no effects on Verilator simulations, but will affect VCS simulations.")
			self.half_period=half_period
		self.force_simple_assignment = force_simple_assignment

	def add_sink(self, sink):
		self.sinks.append(sink)

	def step(self):
		self.cycle += 1
		for sink in self.sinks:
			sink(0)
		for sink in self.sinks:
			sink(1)

	def get_verilog(self, t="  ", n=1):
		verilog = "\n"
                # When used with multi_chipe flow, the clock names get messed up
                # and thus, we disable verilator defs when using multi_chip
                # TODO: Fix the issue to enable verilator defs and multi_chip together
		if multi_chip_flow:
			return verilog
		verilog = "`ifndef VERILATOR\n"
		verilog += n * t + "initial begin\n"
		n += 1
		verilog += t * n + self.name + " = 1'b0;\n"
		verilog += t * n + "forever #" + str(self.half_period) + " " + self.name + " = !" + self.name + ";\n"
		n -= 1
		verilog += t * n + "end\n"
		verilog += "`endif\n"

		if self.force_simple_assignment == True:
			verilog = "\n  assign " + self.name + " = " + self.drivers[0].name + ";\n"
		return verilog

	def get_declaration_string(self):
		if not multi_chip_flow:
			d_str = "\n`ifndef VERILATOR\n"
			d_str += super(Clock, self).get_declaration_string() + "\n"
			d_str += "`endif\n"
		d_str += "\n"
		d_str += "`ifdef VERILATOR\n"
		d_str += "input "

		if self.signed:
			d_str += "signed "

		if self._bit_width is None:
			self.set_bit_width(self.drivers[0]._bit_width)
		d_str += "[" + str(self._bit_width - 1) + ":0] " + self.name + ";\n"
		d_str += "`endif"

		if self.force_simple_assignment == True:
			d_str = "\n  wire [0:0] " + self.name + ";"
		return d_str


class ForcedSignal(Constant):
	def __init__(self, bits, signed, kind="forced_signal", assignment_type="complex", **kwargs):
		super(ForcedSignal, self).__init__(None, signed=signed, bits=bits, kind=kind, assignment_type=assignment_type, **kwargs)
		self.current_value = 0
		self.values = []

	def set(self, v):
		self.current_value = v

	def add_value(self, v, t):
		self.values.append((v, t))

	def get_verilog(self, t="  ", n=1):
		verilog = "`ifndef VERILATOR\n"
		verilog += n * t + "initial begin\n"
		n += 1
		for v,time in self.values:
			verilog += t * n + "#" + str(time) + " " + self.name + " = " + str(v) + ";\n"
		n -= 1
		verilog += t * n + "end\n"
		verilog += "`endif\n"
		return verilog

	def get_declaration_string(self):
		d_str = "\n`ifndef VERILATOR\n"
		d_str += super(ForcedSignal, self).get_declaration_string() + "\n"
		d_str += "`endif\n"
		d_str += "`ifdef VERILATOR\n"
		d_str += "input "

		if self.signed:
			d_str += "signed "

		if self._bit_width is None:
			self.set_bit_width(self.drivers[0]._bit_width)
		d_str += "[" + str(self._bit_width - 1) + ":0] " + self.name + ";\n"
		d_str += "`endif"
		return d_str

class InternallyDrivenSignal(Constant):
	def __init__(self, bits, signed, kind="internally_driven_signal", assignment_type="complex", **kwargs):
		super(InternallyDrivenSignal, self).__init__(None, signed=signed, bits=bits, kind=kind, assignment_type=assignment_type, **kwargs)
		self.current_value = 0
		self.values = []


class Reset(Logic):
	def __init__(self, bits, signed, cycles=50, kind="reset", **kwargs):
		super(Reset, self).__init__(signed=signed, bits=bits, kind=kind, assignment_type="complex", **kwargs)
		self.current_value = 0
		self.values = []
		self.cycles = cycles


class Assertion(Module):
	def __init__(self, clock, condition_f_args, condition_f, error_string="", comment="", kind="assertion", **kwargs):
		super(Assertion, self).__init__("assertion", kind=kind, **kwargs)
		self.should_create_verilog_definition = False
		self.enable_ifdef = False
		self.condition_f = condition_f
		self.comment = comment
		self.error_string = error_string
		self.clock = clock
		self.condition_f_args = condition_f_args

		
	def get_instance_verilog(self, t="  "):
		verilog = ""
		if self.enable_ifdef:
			verilog += "`ifdef TESTING\n"
			
		verilog += v_comment_string(self.comment, t)
		verilog += t
		verilog += "always @(posedge " + self.clock.name + ") begin\n"
		n = 2
		_t = n * t
		#Assertion checks that the condition is not true so we need a NOT here before calling finish
		verilog += _t + "if (!(" + self.condition_f(self.condition_f_args) + ")) begin\n"
		n = 3
		_t = n * t
		verilog += _t + '$display("' + self.error_string + '");\n';
		verilog += _t + '$finish;\n';
		n = 2
		_t = n * t
		verilog += _t + "end\n"
		n = 1
		_t = n * t
		verilog += "end\n"
		
		if self.enable_ifdef:
			verilog += "`endif\n"

		return verilog
		
		#Old version, this does not seem to work in verilator
"""
	def get_instance_verilog(self, t="  "):
		verilog = ""
		if self.enable_ifdef:
			verilog += "`ifdef TESTING\n"
		verilog += v_comment_string(self.comment, t)
		verilog += t
		if self.name is not None:
			verilog += self.name + ": "
		verilog += "assert property (@(posedge " + self.clock.name + ")\n"
		n = 2
		_t = n * t
		verilog += _t + self.condition_f(self.condition_f_args) + ")\n"
		verilog += _t + "else begin\n"
		n = 3
		_t = n * t
		verilog += _t + '$display("' + self.error_string + '");\n';
		verilog += _t + '$finish;\n';
		n = 2
		_t = n * t
		verilog += _t + "end\n"
		
		if self.enable_ifdef:
			verilog += "`endif\n"

		return verilog
		"""



class TestModule(Module):
	def __init__(self, kind="test_module", **kwargs):
		super(TestModule, self).__init__("test_module", kind=kind, **kwargs)
		inst = self.inst
		with ModuleScope(self):
			clock = inst(Clock)
			reset = inst(ForcedSignal, 1, False, name="reset")
			with ParamScope(clock=clock, reset=reset):
				self.c = inst(Counter, reset_value=0, increment_value=1, end_value=5)
				self.c.set_increment_condition(inst(Constant, 1, bits=1))
				self.p = inst(ShiftRegister, self.c._bit_width, 10)
				self.p.d.set_driver(self.c.current_value)
				self.o = inst(Logic, self.c._bit_width, name="sr_d")
				self.o.set_driver(self.p.q)
				self.inst(ROM, bits=5, content_list=[1,2,3,4,5,6,7,-1])

class AutoDelayGroup():
	def __init__(self, delays, op):
		self.delay_a = delay_a
		self.delay_b = delay_b
		self.op = op
		Verilog.current_circuit.auto_delay_groups.append(self)

class AutoDelay():
	def __init__(self, max_async_delay=0):
		self.max_async_delay = max_async_delay
		self.computed_delay = None
		self.computed_max_async_delay = None
		Verilog.current_circuit.auto_delays.append(self)

	def max(self, others):
		return AutoDelayGroup([self] + others, "max")

	def __add__(self, other):
		return AutoDelayGroup([self,other], "add")

	def __sub__(self, other):
		return AutoDelayGroup([self,other], "sub")


def main(argv):
	warnings.simplefilter('once', UserWarning)
	circuit = Circuit()
	tlm = TestModule(circuit=circuit, owner=circuit)
	reset = tlm.kwargs["reset"]
	clock = tlm.kwargs["clock"]
	combinational_update_list = circuit.get_combinational_update_order()
	for i in range(300):
		for to_update in combinational_update_list:
			to_update(True)
		if i < 25:
			reset.set(1)
		else:
			reset.set(0)
		if tlm.p.q() == 5:
			print("TB finished with counter at value " + str(tlm.c.current_value()) + " in cycle " + str(clock.cycle))
			break
		clock.step()
	print(circuit.get_verilog())

if __name__ == "__main__":
	main(sys.argv)
