import os.path
import sys
import math
import numpy as np
from copy import copy
import digitallogic as dl

def cast_fixed(signal, from_spec, to_spec, max_bits_to_check_for_overflow=10, scale=1):

	inst = dl.Verilog.current_module.m.inst

	original_name = signal.name
	original_signal = signal

	#If there's a scale involved, it means that the to_spec should be shifted left (more ints, less fracs)
	if (scale > 1):
		assert(math.log2(scale) == clog2(scale)) #Make sure scale was properly set as a power of 2
		assert(to_spec["frac"] >= clog2(scale))
		scaled_to_spec = {
			"frac" : to_spec["frac"]-clog2(scale),
			"sign" : 1,
			"int" : to_spec["int"]+clog2(scale)
		}
		to_spec = scaled_to_spec

	# The following indices are relative to the from spec
	# so to_spec_lo_bit will be -3 if to spec has 3 more
	# fractional bits than from_spec.  If it has 2 less,
	# it will be +2.
	#
	to_spec_lo_bit = from_spec["frac"] - to_spec["frac"]
	#
	# This is the high bit in the to_spec relative to the
	# start of the from_spec. We will modify this and the
	# to_spec_lo_bit so that they are indices we can use
	# to select the correct bits from the extended input
	# signal
	to_spec_hi_bit = from_spec["frac"] + to_spec["int"] - 1
	# If the input signal is signed, this will be the index
	# of the sign bit
	sign_bit_index = signal._bit_width-1

	with dl.ParamScope(name=signal.name + "_sign_bit"):
		if to_spec["sign"]:
			sign_bit = signal[sign_bit_index]
		else:
			sign_bit = inst(dl.Constant, bits=1, value=0)

	# Zero pad the fractional bits if there are more
	# in the to_spec than the from_spec
	more_frac_in_to_spec = to_spec_lo_bit < 0
	if more_frac_in_to_spec:
		to_spec_hi_bit -= to_spec_lo_bit
		signal = inst(dl.Concat,
			inst(dl.Constant,
				value=0,
				bits=-to_spec_lo_bit,
				name="fractional_padding"),
			signal,
			name=signal.name + "_and_fractional_padding")
		to_spec_lo_bit = 0

	# Sign or zero extend the signal if necessary.
	# If there are more bits in the to_spec than the
	# from spec, then we need to ze or se it.
	more_int_in_to_spec = to_spec["int"] > from_spec["int"]
	fewer_int_in_to_spec = to_spec["int"] < from_spec["int"]
	if more_int_in_to_spec:
		if from_spec["sign"]:
			extended_name = "_sign_extend"
		else:
			extended_name = "_zero_extend"

		# sign extend or zero extend the signal 
		bits_to_extend = to_spec["sign"] + to_spec["int"] - from_spec["int"]
		signal = inst(dl.Concat,
			signal,
			inst(dl.Concat,
				*([sign_bit]*bits_to_extend),
				name=signal.name + extended_name),
			name=signal.name + extended_name + "ed")
	elif fewer_int_in_to_spec:
		# if we are cutting off bits we need to check to see if there is an
		# overflow.  If the sign bit is zero and any of the bits we cut off
		# are one, there is an overflow, so we or those non-sign bits together
		from_int_start_bit = from_spec["frac"]
		first_cut_bit = from_int_start_bit + to_spec["int"]
		last_cut_bit = min(from_int_start_bit + from_spec["int"],
			from_int_start_bit + to_spec["int"] + max_bits_to_check_for_overflow)
		positive_overflow = inst(dl.OR,
			*[original_signal[k] for k in range(first_cut_bit, last_cut_bit)],
			name=original_signal.name + "_is_positive_overflow")
		overflow_select = positive_overflow
		# we will need to select the maximum possible value
		# if we have a positive overflow
		output_non_sign_bits = to_spec_hi_bit - to_spec_lo_bit + 1
		max_value = inst(dl.Constant,
			name=original_name + "_cast_max_value",
			bits=output_non_sign_bits,
			value=(1<<output_non_sign_bits)-1)
		overflow_selections = [max_value]

		if to_spec["sign"] == 1 and from_spec["sign"] == 1:
			positive_overflow_and_not_sign = inst(dl.AND, 
				inst(dl.NOT, sign_bit), positive_overflow)
			negative_overflow = inst(dl.AND, sign_bit, inst(dl.OR,
				*[inst(dl.NOT, original_signal[k]) for k in range(first_cut_bit, last_cut_bit)],
				name=original_signal.name + "_is_negative_overflow"))
			overflow_select = inst(dl.Concat, positive_overflow_and_not_sign, negative_overflow, name=original_signal.name + "_overflow_select")
			min_value = inst(dl.Constant,
				name=original_name + "_cast_min_value_no_sign",
				bits=output_non_sign_bits,
				value=0)
			overflow_selections += [min_value]


	# since we have computed to_spec_hi_bit and to_spec_lo_bit,
	# we can now select the bits we need from the padded signal
	with dl.ParamScope(name=signal.name + "_slimmed_output"):
		slimmed_output = signal[to_spec_hi_bit:to_spec_lo_bit]

	if fewer_int_in_to_spec:
		output = inst(dl.MuxV2,
			overflow_select,
			slimmed_output,
			*overflow_selections,
			name=signal.name + "_select_int_max_if_overflow")
	else:
		output = slimmed_output

	if to_spec["sign"]:
		output = inst(dl.Concat, output, sign_bit, name=signal.name + "_signed_output")
	casted = inst(dl.Logic, bits=output._bit_width, name=signal.name+"_cast", signed=to_spec["sign"] == 1)
	casted.set_driver(output)
	return casted



class BitArray():
	def __init__(self, bits, value_vector=None, length=None, bit_array=None):
		if bit_array is not None:
			self.bits = bits
			self.bit_array = bit_array
			return
		assert(value_vector is not None or length is not None)
		if length is None:
			length = len(value_vector)
		self.bits = bits
		if bits == 0:
			return
		self.bit_array = np.zeros(shape=[length, math.ceil(bits/8.)], dtype=np.uint8)
		if value_vector is not None:
			for i in range(math.ceil(bits / 8.)):
				self.bit_array[:,i] = np.right_shift(value_vector, i * 8).astype(np.uint8)
		mask = get_bit_mask(bits % 8)
		if mask != 0:
			self.bit_array[:,-1] = np.bitwise_and(self.bit_array[:,-1], mask)


	def __add__(self, other):
		if self.bits == 0:
			return other
		elif other.bits == 0:
			return self
		assert(self.bit_array.shape[0] == other.bit_array.shape[0])
		new = BitArray(self.bits + other.bits, length=self.bit_array.shape[0])
		new.bit_array[:,:self.bit_array.shape[1]] = self.bit_array
		tmp = np.zeros(shape=[new.bit_array.shape[0],new.bit_array.shape[1]+1], dtype=np.uint8)
		to_shift = self.bits % 8
		second_shift = 8 - to_shift
		start_index = self.bits // 8
		next_index = start_index + 1
		tmp[:,start_index:start_index+other.bit_array.shape[1]] = np.left_shift(other.bit_array, to_shift)
		tmp_slice = tmp[:,next_index:next_index+other.bit_array.shape[1]]
		np.bitwise_or(tmp_slice, np.right_shift(other.bit_array, second_shift), out=tmp_slice)
		np.bitwise_or(new.bit_array, tmp[:,:-1], out=new.bit_array)
		return new

	def __getitem__(self, val):
		if type(val) is tuple:
			sel = val[0]
		else:
			sel = val

		vector_slice = self.bit_array[sel,:]
		if type(val) is not tuple or len(val) == 1:
			return BitArray(self.bits, bit_array=vector_slice)

		bit_sel = val[1]
		bit_sel_stop = min(bit_sel.stop, self.bits)
		bits = bit_sel.start - bit_sel_stop + 1
		to_shift = bit_sel_stop % 8
		vector = np.right_shift(vector_slice, to_shift)
		v_slice = vector[:,:-1]
		np.bitwise_or(v_slice, np.left_shift(vector_slice[:,1:], 8-to_shift), out=v_slice)
		mask = get_bit_mask(bits % 8)
		most_significant_byte = (bit_sel.start - to_shift) // 8
		least_significant_byte = (bit_sel_stop - to_shift) // 8
		if bits == 0 or mask != 0:
			np.bitwise_and(vector[:,most_significant_byte], mask, out=vector[:,most_significant_byte])
		vector = vector[:,least_significant_byte:most_significant_byte+1]
		return BitArray(bits, bit_array=vector)

	def __len__(self):
		return self.bit_array.shape[0]

	def get_hex_strs(self):
		hex_strs = []
		subtract_most_significant_char = (self.bits % 8) > 0 and (self.bits % 8) <= 4
		for l in range(self.bit_array.shape[0]):
			hex_str = "".join(["%02X" % v for v in reversed(self.bit_array[l,:])])
			if subtract_most_significant_char:
				hex_str = hex_str[1:]
			hex_strs.append(hex_str)

		return hex_strs

	def get_bit_strs(self):
		bit_strs = []
		bit_array = np.unpackbits(self.bit_array, -1, bitorder="little")
		for l in range(self.bit_array.shape[0]):
			bit_str = np.array2string(bit_array[l,:self.bits], separator="")[1:-1][::-1]
			bit_strs.append(bit_str)
		return bit_strs

	def get_byte_array(self):
		if (self.bits % 8) != 0:
			warnings.warn("gen_pure_verilog.utils.get_byte_array does not correctly output binary for cases where the number of bits per row is not a multiple of 8.  This has " + str(self.bits) + " bits.")
		b = self.bit_array.flatten().tobytes()
		return b


	def __str__(self):
		return str(self.get_hex_strs())



def print_stage_header(text):
	head = "=" * 80
	padding = (80 - len(text)) // 2
	string = head + "\n" + padding * " " + text + "\n" + head
	print(string)

def get_lines_in_file(filepath):
	with open(filepath) as f:
		lines = f.read().split("\n")
	to_delete = []
	for i,line in enumerate(lines):
		if line == "":
			to_delete.append(i)
	for i in reversed(to_delete):
		del lines[i]
	return lines

def get_nodes(graphdef, import_name="import"):
	import tensorflow as tf
	returned_elements = []
	for node in graphdef.node:
		returned_elements.append(node.name)
	nodes = tf.import_graph_def(graphdef, return_elements=returned_elements, name=import_name)
	return nodes

def get_tensor_to_producer_op_map(nodes):
	import tensorflow as tf
	tensor_to_producer_op = {}
	for node in nodes:
		if not isinstance(node, tf.Operation):
			continue
		for tensor in node.outputs:
			tensor_to_producer_op[tensor.name] = node
	return tensor_to_producer_op

def get_tensor_to_consumer_op_map(nodes):
	import tensorflow as tf
	tensor_to_consumer_ops = {}
	for node in nodes:
		if not isinstance(node, tf.Operation):
			continue
		for tensor in node.inputs:
			if tensor.name in tensor_to_consumer_ops.keys():
				tensor_to_consumer_ops[tensor.name].append(node)
				continue
			tensor_to_consumer_ops[tensor.name] = [node]
	return tensor_to_consumer_ops


def clog2(v):
	return math.ceil(math.log2(float(v)))

def get_bit_mask(bits):
	if bits <= 0:
		return 0
	mask = 1
	for _ in range(bits-1):
		mask = mask << 1
		mask |= 1
	return mask

def hex_str(v, bits):
	if type(v) is list:
		string = ""
		for i,b in reversed(list(enumerate(v))):
			bits_to_take = min(max(bits - (i * 8), 0), 8)
			mask = get_bit_mask(bits_to_take)
			string += format(b & mask, "02X")
		if (bits % 8) > 0 and (bits % 8) <= 4:
			string = string[1:]
		return string
	mask = get_bit_mask(bits)
	nibbles = math.ceil(bits / 4.)
	fmt = "0" + str(nibbles) + "X"
	return format(v & mask, fmt)

def v_hex_str(v, bits):
	hex_string = hex_str(v, bits)
	return str(bits) + "'h" + hex_string

def v_comment_string(string, t=""):
	out = t + "// "
	out += ("\n" + t + "// ").join(string.split("\n"))
	out += "\n"
	return out

def get_quant_op(tensor, integer_bits=4, fractional_bits=11):
	import tensorflow as tf
	max_value = 2**integer_bits
	smallest_fraction = 2**-fractional_bits
	min_value = -max_value
	max_value = max_value - smallest_fraction
	return tf.quantize(tensor, min_value, max_value, tf.qint16)

def get_quantized(tensor, sign_bits=1, integer_bits=4, fractional_bits=11):
	if np.issubdtype(tensor.dtype, np.integer):
		return tensor
	min_frac_value = 2**-fractional_bits
	if np.prod(tensor.shape) >= 1:
		# to round we add half the minimum value to positive values
		tensor[np.where(tensor > 0.)] += (min_frac_value / 2.)
		# to round we subtract half the minimum value from negative values
		tensor[np.where(tensor < 0.)] -= (min_frac_value / 2.)
		max_value = 2**integer_bits - min_frac_value
		min_value = -2**integer_bits
		tensor[np.where(tensor > max_value)] = max_value
		tensor[np.where(tensor < min_value)] = min_value
	integer_rep = tensor / min_frac_value
	return integer_rep.astype(int)


def concatenate_numbers(numbers, widths):
	cbytes = []
	current_byte = 0
	space_remaining = 8
	for i,(n,w) in enumerate(zip(numbers, widths)):
		remaining_in_n = w
		while remaining_in_n > 0:
			to_shift_n = w - remaining_in_n
			to_add = n >> to_shift_n
			bits_added = min(8, remaining_in_n, space_remaining)
			mask = get_bit_mask(bits_added)
			to_add &= mask
			current_byte |= to_add << (8-space_remaining)
			remaining_in_n -= bits_added
			space_remaining -= bits_added
			if space_remaining == 0:
				space_remaining = 8
				cbytes.append(current_byte)
				current_byte = 0
	if space_remaining != 8:
		space_remaining = 8
		cbytes.append(current_byte)
		current_byte = 0
	return cbytes

def get_padding(ia_shape, kernel_shape, stride, padding, explicit_padding):
	#if padding.lower() != "same":
	#	sys.exit(0)
	allow_negative_padding = False
	if explicit_padding is None:
		explicit_padding = [[0, 0], [0, 0], [0, 0], [0, 0]]
		#allow_negative_padding = True
	input_height = ia_shape[1]
	input_width = ia_shape[2]
	input_channels = ia_shape[3]

	kernel_height = kernel_shape[0]
	kernel_width = kernel_shape[1]
	if type(padding) is bytes:
		padding = padding.decode("ascii")

	input_spatial_shape = [s + np.sum(p) for s, p in zip(ia_shape[1:3], explicit_padding[1:3])]
	spatial_filter_shape = [s for s in kernel_shape[0:2]]
	if padding.lower() == "same":
		output_spatial_shape = [math.ceil(float(s) / stride) for s in input_spatial_shape]
	else:
		output_spatial_shape = [math.ceil((s - (k - 1)) / float(stride))
								for s, k in zip(input_spatial_shape, spatial_filter_shape)]
	output_spatial_shape = output_spatial_shape
	padded_input_spatial_shape = [(o - 1) * stride + k for o, k in zip(output_spatial_shape, spatial_filter_shape)]
	spatial_padding = [p - i for p, i in zip(padded_input_spatial_shape, input_spatial_shape)]
	divided_spatial_padding = [[p // 2, math.ceil(p / 2)] for p in spatial_padding]
	if(padding.lower() == "valid"):
		divided_spatial_padding[0].reverse()
		divided_spatial_padding[1].reverse()

	padding = [[0, 0]] + divided_spatial_padding + [[0, 0]]
	padding = [[p + e for p,e in zip(pad,list(reversed(exp)))] for pad,exp in zip(padding, explicit_padding)]
	if not allow_negative_padding:
		padding = [[max(p, 0) for p in pds] for pds in padding]


	return (padding, output_spatial_shape)

def is_power_of_2(value):
	l2 = math.log2(float(value))
	l2_floor = float(int(l2))
	if (l2 - l2_floor) < (2. ** -126):
		return True
	return False


if __name__ is "__main__":
	ba1 = BitArray(bits=8, value_vector=np.array([1,0,1,2,3]))
	ba2 = BitArray(bits=8, value_vector=np.array([6,1,12,14,32]))
	ba3 = ba1 + ba2
	print(ba3.bit_array)
	ba3[:,15:1]
	print(np.right_shift(ba2.bit_array, 1))
	print(ba3.bit_array.data.hex())
	print(ba3[:,12:0])

