//Functional model if the tensor block for simulation in programs that can't load Intel IPs
//This does 2 independent bfloat24 additions (1 sign bit, 8 exponent bits, and 15 mantissa bits)
module tensor_block_bfloat24_add #(
//These are not actually used, they are just to make the module instantiation easier 
	parameter dsp_mode = "scalar_bf24",
	parameter dsp_sel_int4 = "select_int8",
	parameter dsp_fp32_sub_en = "float_sub_disabled",
	//Should be 0 or 1
	parameter dsp_cascade = 0
) (    

input ena, clk, 
input acc_en,
input [1:0] clr, //Asynch clear

input [95:0] data_in,

input [95:0] cascade_data_in,
output [95:0] cascade_data_out,

output [36:0] result_h,
output [37:0] result_l,

//Not needed, but makes replacing instantiation easier
input load_buf_sel, mode_switch, load_bb_one, load_bb_two, zero_en,
input [1:0] feed_sel,
input [7:0] shared_exponent,
input [87:0] cascade_weight_in,
output [87:0] cascade_weight_out


);


wire reset;
assign reset =  clr[0] || clr[1];

logic acc_pipe_1, acc_pipe_2;

logic [23:0] bfloat24_num_a, bfloat24_num_b, bfloat24_num_c, bfloat24_num_d;
logic [31:0] fp32_num_a, fp32_num_b, fp32_num_c, fp32_num_d;

//Pipeline inputs
always_ff @(posedge clk) begin
	if(reset) begin
		bfloat24_num_a <= 24'd0;
		bfloat24_num_b <= 24'd0;
		bfloat24_num_c <= 24'd0;
		bfloat24_num_d <= 24'd0;
		acc_pipe_1 <= 1'd0;
	end else if (ena) begin
		bfloat24_num_a <= data_in[23:0];
		bfloat24_num_b <= data_in[47:24];
		bfloat24_num_c <= data_in[71:48];
		bfloat24_num_d <= data_in[95:72];	
		acc_pipe_1 <= acc_en;
	end
end

//Bypass multiplier and pipeline inputs again (including cascade)

always_ff @(posedge clk) begin
	if(reset) begin
		fp32_num_a <= 32'd0;
		fp32_num_b <= 32'd0;
		fp32_num_c <= 32'd0;
		fp32_num_d <= 32'd0;
		acc_pipe_2 <= 1'd0;
	end else if (ena) begin
		fp32_num_a <= {bfloat24_num_a, 8'd0};
		fp32_num_b <= (dsp_cascade == 1) ? cascade_data_in[31:0] : {bfloat24_num_b, 8'd0};
		fp32_num_c <= {bfloat24_num_c, 8'd0};
		fp32_num_d <= (dsp_cascade == 1) ? cascade_data_in[63:32] : {bfloat24_num_d, 8'd0};
		acc_pipe_2 <= acc_pipe_1;
	end
end

logic [31:0] addition_res_a_wire, addition_res_b_wire;
logic [31:0] addition_res_a, addition_res_b;

//Select accumulation if enabled
logic [31:0] fp32_num_b_final, fp32_num_d_final;
assign fp32_num_b_final = acc_pipe_2 ? addition_res_a : fp32_num_b;
assign fp32_num_d_final = acc_pipe_2 ? addition_res_b : fp32_num_d;

//perform the addition
fp32_add_sim_v2 sim_add_1(.num_a(fp32_num_a), .num_b(fp32_num_b_final), .res(addition_res_a_wire));
fp32_add_sim_v2 sim_add_2(.num_a(fp32_num_c), .num_b(fp32_num_d_final), .res(addition_res_b_wire));

//pipeline the results

always_ff @(posedge clk) begin
	if(reset) begin
		addition_res_a <= 32'd0;
		addition_res_b <= 32'd0;
	end else if (ena) begin
		addition_res_a <= addition_res_a_wire;
		addition_res_b <= addition_res_b_wire;
	end
end

//Convert back to 24-bit bloack floating point and output
//Technically we should implement proper rounding here
assign result_l[23:0] = addition_res_a[31:8];
assign result_h[23:0] = addition_res_b[31:8];

assign result_l[36:24] = 13'd0;
assign result_l[37:24] = 14'd0;

endmodule

//Same as the one in the tensor bfp simulation module but seperate so it isn't dependant on that file being used
module fp32_add_sim_v2 (
	input [31:0] num_a,
	input [31:0] num_b,
	output [31:0] res
);

//Annoyingly varilator does not support 32-bit FP (shortreal) only 64-bit so we have to convert
wire [63:0] num_a_fp64, num_b_fp64;
wire [63:0] res_fp64;

//Sign 
assign num_a_fp64[63] = num_a[31];
assign num_b_fp64[63] = num_b[31];


//Mantissa
assign num_a_fp64[51:0] = {num_a[22:0], 29'd0};
assign num_b_fp64[51:0] = {num_b[22:0], 29'd0};


wire [9:0] num_a_exp_no_bias, num_b_exp_no_bias;
assign num_a_exp_no_bias = num_a[30:23] - 8'd127;
assign num_b_exp_no_bias = num_b[30:23] - 8'd127;

wire [11:0] num_a_exp_no_bias_sign_ext, num_b_exp_no_bias_sign_ext;
assign num_a_exp_no_bias_sign_ext = $signed(num_a_exp_no_bias);
assign num_b_exp_no_bias_sign_ext = $signed(num_b_exp_no_bias);


//Exponent (need to fix bias)
// verilator lint_off WIDTH
assign num_a_fp64[62:52] = (num_a[30:23] == 8'd0) ? 11'd0 : $signed(num_a_exp_no_bias_sign_ext) + 11'd1023;
assign num_b_fp64[62:52] = (num_b[30:23] == 8'd0) ? 11'd0 : $signed(num_b_exp_no_bias_sign_ext) + 11'd1023;

//Cannot be synthesized, but good enough for the simulation model
assign res_fp64 = $realtobits($bitstoreal(num_a_fp64) + $bitstoreal(num_b_fp64));
wire [31:0] res_fp32;

assign res[31] = res_fp64[63];
assign res[22:0] = res_fp64[51:29];

assign res[30:23] = (res_fp64[62:52] == 11'd0) ? 8'd0 : $signed(res_fp64[62:52] - 11'd1023) + 8'd127;
// verilator lint_on WIDTH


endmodule