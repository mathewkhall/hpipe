//Functional model if the tensor block for simulation in programs that can't load Intel IPs
module tensor_block_8x8_scalar #(
//These are not actually used, they are just to make the module instantiation easier 
	parameter dsp_mode = "scalar_8x8",
	parameter dsp_sel_int4 = "select_int8",
	parameter dsp_fp32_sub_en = "float_sub_disabled",
	//This parameter is used to enable/disable the cascade in
	parameter dsp_cascade = 1'b0
) (    

input ena, clk, 
input acc_en,
input [1:0] clr, //Asynch clear

input [47:0] data_in,

//Double check this
input [95:0] cascade_data_in,
output [95:0] cascade_data_out,

output [36:0] result_h,
output [37:0] result_l,

//Not needed, but makes replacing instantiation easier
input load_buf_sel, mode_switch, load_bb_one, load_bb_two, zero_en,
input [1:0] feed_sel,
input [7:0] shared_exponent,
input [87:0] cascade_weight_in,
output [87:0] cascade_weight_out

);

//Multiplier is 8-bit but all internal tensor data paths after that have a width of 32-bits
localparam DATAW = 8;
localparam BUSW = 32;

//In reality different bits could clear different registers, but in HPIPE we should just assert both at the same time
wire reset;
assign reset =  clr[0] || clr[1];

//Pipeline inputs
logic acc_en_reg_1, acc_en_reg_2;
logic [47:0] data_in_reg;
always @(posedge clk or posedge reset) begin
	 if (reset) begin
		data_in_reg <= 48'd0;
        acc_en_reg_1 <= 1'b0;
        acc_en_reg_2 <= 1'b0;
	 end else begin
		data_in_reg <= ena ? data_in : data_in_reg;
        acc_en_reg_1 <= ena ? acc_en : acc_en_reg_1;
        acc_en_reg_2 <= ena ? acc_en_reg_1 : acc_en_reg_2;
	 end
 end
 
 integer j;
 //Create cascade register
 logic [2:0][31:0] cascade_reg;
 always @(posedge clk or posedge reset) begin
	for(j = 0; j < 3; j += 1) begin
		 if (reset) begin
			cascade_reg[j] <= 32'd0;
		 end else begin
			if (ena) cascade_reg[j] <= (dsp_cascade == 0) ?  32'd0 : cascade_data_in[j*BUSW +: BUSW]; /* verilator lint_off WIDTH */
			else cascade_reg[j] <= cascade_reg[j];
		 end
	 end
 end

 //Pipeline multiplication results and accumulate. 
 logic [2:0][31:0] mult_res;
 logic [2:0][31:0] mult_acc_res;
 integer i;
always @(posedge clk or posedge reset) begin
	for(i = 0; i < 3; i += 1) begin
		 if (reset) begin
			mult_res[i] <= 32'd0;
			mult_acc_res[i] <= 32'd0;
		 end else begin
			if (ena) begin
				mult_res[i] <= $signed(data_in_reg[(i*2)*DATAW +: DATAW]) * $signed(data_in_reg[(i*2+1)*DATAW +: DATAW]);
				//Check if cascade is disabled. If so, check if acc_en to see if we are accumulating with the last result
				mult_acc_res[i] <= (dsp_cascade == 1) ? $signed(mult_res[i]) + $signed(cascade_reg[i]) : (acc_en_reg_2 ? $signed(mult_res[i]) + $signed(mult_acc_res[i]) : mult_res[i]); /* verilator lint_off WIDTH */
			end else begin
				mult_res[i] <= mult_res[i];
				mult_acc_res[i] <= mult_acc_res[i];
			end
		 end
	 end
 end
 

 assign cascade_data_out = {mult_acc_res[2], mult_acc_res[1], mult_acc_res[0]};
 
 //Assign result_l and result_h based on spec
assign result_l[24:0] = mult_acc_res[0][24:0];
assign {result_h[11:0], result_l[37:25]} = mult_acc_res[1][24:0];
assign result_h[36:12] = mult_acc_res[2][24:0];
 
endmodule
