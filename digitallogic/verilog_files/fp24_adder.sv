//Fp24 adder, input and output should be registered outside the module
module fp24_add_hw (
	input [23:0] fp24_input_a, fp24_input_b,
	output [23:0] fp24_output
);

//Find larger exponent (and match the mantissa/signs)

wire [7:0] larger_exp, smaller_exp;
wire [14:0] larger_exp_mantissa, smaller_exp_mantissa;
wire larger_exp_sign, smaller_exp_sign;
wire exp_a_larger;

assign exp_a_larger = fp24_input_a[22:15] > fp24_input_b[22:15];
assign larger_exp = exp_a_larger ? fp24_input_a[22:15] : fp24_input_b[22:15]; 
assign smaller_exp = exp_a_larger ? fp24_input_b[22:15] : fp24_input_a[22:15]; 

assign larger_exp_mantissa = exp_a_larger ? fp24_input_a[14:0] : fp24_input_b[14:0]; 
assign smaller_exp_mantissa = exp_a_larger ? fp24_input_b[14:0] : fp24_input_a[14:0]; 

assign larger_exp_sign = exp_a_larger ? fp24_input_a[23] : fp24_input_b[23]; 
assign smaller_exp_sign = exp_a_larger ? fp24_input_b[23] : fp24_input_a[23]; 

//Shift smaller mantissa to align

wire [7:0] shift_amount;
assign shift_amount = larger_exp - smaller_exp;
wire [15:0] shifted_mantissa, non_shifted_mantissa; //Includes implicit 1
assign shifted_mantissa = {1'b1, smaller_exp_mantissa} >> shift_amount;
assign non_shifted_mantissa = {1'b1, larger_exp_mantissa};

//Convert mantissa to 2s complement
logic [16:0] shifted_mantissa_2s_complement, non_shifted_mantissa_2s_complement;
logic [7:0] shared_exponent;

//Could add registers here if needed
always_comb begin
	shifted_mantissa_2s_complement = smaller_exp_sign ? -{1'b0, shifted_mantissa} : {1'b0, shifted_mantissa};
	non_shifted_mantissa_2s_complement = larger_exp_sign ? -{1'b0, non_shifted_mantissa} : {1'b0, non_shifted_mantissa};
	shared_exponent = larger_exp;
end

//Perform integer addition
wire [17:0] added_mantissas;
assign added_mantissas = $signed(shifted_mantissa_2s_complement) + $signed(non_shifted_mantissa_2s_complement);

//Convert back to sign/magnitude
logic [16:0] magnitude_added_mantissas;
logic sign_added_mantissas;

//Should add another register here before barrel shifter (as we have just performed 2 additions)
always_comb begin
 	 sign_added_mantissas = added_mantissas[17];
	 magnitude_added_mantissas = sign_added_mantissas ? -added_mantissas : added_mantissas;
end

//Normalize the result
//Need to do this in 2 steps. First find the 
logic [14:0] result_mantissa;
logic [7:0] exponent_offset;
always_comb begin
	casez (magnitude_added_mantissas) // synthesis parallel_case 
		17'b1???????????????? : begin
			exponent_offset = 8'd1;
			result_mantissa = magnitude_added_mantissas[15:1];
		end
		17'b01??????????????? : begin
			exponent_offset = 8'd0; //Implicit 1 is already in the correct spot
			result_mantissa = magnitude_added_mantissas[14:0];
		end
		17'b001?????????????? : begin
			exponent_offset = -8'd1;
			result_mantissa = {magnitude_added_mantissas[13:0], 1'b0};
		end
		17'b0001????????????? : begin
			exponent_offset = -8'd2;
			result_mantissa = {magnitude_added_mantissas[12:0], 2'b0};
		end
		17'b00001???????????? : begin
			exponent_offset = -8'd3;
			result_mantissa = {magnitude_added_mantissas[11:0], 3'b0};
		end
		17'b000001??????????? : begin
			exponent_offset = -8'd4;
			result_mantissa = {magnitude_added_mantissas[10:0], 4'b0};
		end
		17'b0000001?????????? : begin
			exponent_offset = -8'd5; 
			result_mantissa = {magnitude_added_mantissas[9:0], 5'b0};
		end
		17'b00000001????????? : begin
			exponent_offset = -8'd6;
			result_mantissa = {magnitude_added_mantissas[8:0], 6'b0};
		end
		17'b000000001???????? : begin
			exponent_offset = -8'd7;
			result_mantissa = {magnitude_added_mantissas[7:0], 7'b0};
		end
		17'b0000000001??????? : begin
			exponent_offset = -8'd8;
			result_mantissa = {magnitude_added_mantissas[6:0], 8'b0};
		end
		17'b00000000001?????? : begin
			exponent_offset = -8'd9;
			result_mantissa = {magnitude_added_mantissas[5:0], 9'b0};
		end
		17'b000000000001????? : begin
			exponent_offset = -8'd10;
			result_mantissa = {magnitude_added_mantissas[4:0], 10'b0};
		end
		17'b0000000000001???? : begin
			exponent_offset = -8'd11;
			result_mantissa = {magnitude_added_mantissas[3:0], 11'b0};
		end
		17'b00000000000001??? : begin
			exponent_offset = -8'd12;
			result_mantissa = {magnitude_added_mantissas[2:0], 12'b0};
		end
		17'b000000000000001?? : begin
			exponent_offset = -8'd13;
			result_mantissa = {magnitude_added_mantissas[1:0], 13'b0};
		end
		17'b0000000000000001? : begin
			exponent_offset = -8'd14;
			result_mantissa = {magnitude_added_mantissas[0], 14'b0};
		end
		17'b00000000000000001 : begin
			exponent_offset = -8'd15;
			result_mantissa = {15'd0};
		end
		default: begin
			exponent_offset = -shared_exponent; //This means our result is zero, so we should make sure the shared exponent is 0
			result_mantissa = 15'd0;
		end
	endcase
end



wire [7:0] normalized_exp;
assign normalized_exp = shared_exponent + $signed(exponent_offset);

//Output result (should bre registered outside the module)
assign fp24_output = {sign_added_mantissas, normalized_exp, result_mantissa};

endmodule


//Same as the above, but with a pipeline register (reaches 400 MHz on it's own, may need to add some extra registers to go higher)
module fp24_add_hw_reg (
	input clk, reset,
	input [23:0] fp24_input_a, fp24_input_b,
	output [23:0] fp24_output
);

//Find larger exponent (and match the mantissa/signs)

wire [7:0] larger_exp, smaller_exp;
wire [14:0] larger_exp_mantissa, smaller_exp_mantissa;
wire larger_exp_sign, smaller_exp_sign;
wire exp_a_larger;

assign exp_a_larger = fp24_input_a[22:15] > fp24_input_b[22:15];
assign larger_exp = exp_a_larger ? fp24_input_a[22:15] : fp24_input_b[22:15]; 
assign smaller_exp = exp_a_larger ? fp24_input_b[22:15] : fp24_input_a[22:15]; 

assign larger_exp_mantissa = exp_a_larger ? fp24_input_a[14:0] : fp24_input_b[14:0]; 
assign smaller_exp_mantissa = exp_a_larger ? fp24_input_b[14:0] : fp24_input_a[14:0]; 

assign larger_exp_sign = exp_a_larger ? fp24_input_a[23] : fp24_input_b[23]; 
assign smaller_exp_sign = exp_a_larger ? fp24_input_b[23] : fp24_input_a[23]; 

//Shift smaller mantissa to align

wire [7:0] shift_amount;
assign shift_amount = larger_exp - smaller_exp;
wire [15:0] shifted_mantissa, non_shifted_mantissa; //Includes implicit 1
assign shifted_mantissa = {1'b1, smaller_exp_mantissa} >> shift_amount;
assign non_shifted_mantissa = {1'b1, larger_exp_mantissa};

//Convert mantissa to 2s complement
logic [16:0] shifted_mantissa_2s_complement, non_shifted_mantissa_2s_complement;
logic [7:0] shared_exponent;

//Could add registers here if needed
always_ff @(posedge clk) begin
	if (reset) begin
		shifted_mantissa_2s_complement <= 17'd0;
		non_shifted_mantissa_2s_complement <= 17'd0;
		shared_exponent <= 8'd0;
	end else begin
		shifted_mantissa_2s_complement <= smaller_exp_sign ? -{1'b0, shifted_mantissa} : {1'b0, shifted_mantissa};
		non_shifted_mantissa_2s_complement <= larger_exp_sign ? -{1'b0, non_shifted_mantissa} : {1'b0, non_shifted_mantissa};
		shared_exponent <= larger_exp;
	end
end


//Perform integer addition
wire [17:0] added_mantissas;
assign added_mantissas = $signed(shifted_mantissa_2s_complement) + $signed(non_shifted_mantissa_2s_complement);

//Convert back to sign/magnitude
logic [17:0] magnitude_added_mantissas;
logic sign_added_mantissas;
logic [7:0] shared_exp_reg;

//Should add another register here before barrel shifter (as we have just performed 2 additions)
always_ff @(posedge clk) begin
	if (reset) begin
 		sign_added_mantissas <= 1'b0;
	 	magnitude_added_mantissas <= 18'd0;
	 	shared_exp_reg <= 8'd0;
	end else begin
 		sign_added_mantissas <= added_mantissas[17];
	 	magnitude_added_mantissas <= added_mantissas[17] ? -added_mantissas : added_mantissas;
	 	shared_exp_reg <= shared_exponent;
	end
end

//Normalize the result
//Need to do this in 2 steps. First find the 
logic [14:0] result_mantissa;
logic [7:0] exponent_offset;
always_comb begin
	casez (magnitude_added_mantissas[16:0]) // synthesis parallel_case 
		17'b1???????????????? : begin
			exponent_offset = 8'd1;
			result_mantissa = magnitude_added_mantissas[15:1];
		end
		17'b01??????????????? : begin
			exponent_offset = 8'd0; //Implicit 1 is already in the correct spot
			result_mantissa = magnitude_added_mantissas[14:0];
		end
		17'b001?????????????? : begin
			exponent_offset = -8'd1;
			result_mantissa = {magnitude_added_mantissas[13:0], 1'b0};
		end
		17'b0001????????????? : begin
			exponent_offset = -8'd2;
			result_mantissa = {magnitude_added_mantissas[12:0], 2'b0};
		end
		17'b00001???????????? : begin
			exponent_offset = -8'd3;
			result_mantissa = {magnitude_added_mantissas[11:0], 3'b0};
		end
		17'b000001??????????? : begin
			exponent_offset = -8'd4;
			result_mantissa = {magnitude_added_mantissas[10:0], 4'b0};
		end
		17'b0000001?????????? : begin
			exponent_offset = -8'd5; 
			result_mantissa = {magnitude_added_mantissas[9:0], 5'b0};
		end
		17'b00000001????????? : begin
			exponent_offset = -8'd6;
			result_mantissa = {magnitude_added_mantissas[8:0], 6'b0};
		end
		17'b000000001???????? : begin
			exponent_offset = -8'd7;
			result_mantissa = {magnitude_added_mantissas[7:0], 7'b0};
		end
		17'b0000000001??????? : begin
			exponent_offset = -8'd8;
			result_mantissa = {magnitude_added_mantissas[6:0], 8'b0};
		end
		17'b00000000001?????? : begin
			exponent_offset = -8'd9;
			result_mantissa = {magnitude_added_mantissas[5:0], 9'b0};
		end
		17'b000000000001????? : begin
			exponent_offset = -8'd10;
			result_mantissa = {magnitude_added_mantissas[4:0], 10'b0};
		end
		17'b0000000000001???? : begin
			exponent_offset = -8'd11;
			result_mantissa = {magnitude_added_mantissas[3:0], 11'b0};
		end
		17'b00000000000001??? : begin
			exponent_offset = -8'd12;
			result_mantissa = {magnitude_added_mantissas[2:0], 12'b0};
		end
		17'b000000000000001?? : begin
			exponent_offset = -8'd13;
			result_mantissa = {magnitude_added_mantissas[1:0], 13'b0};
		end
		17'b0000000000000001? : begin
			exponent_offset = -8'd14;
			result_mantissa = {magnitude_added_mantissas[0], 14'b0};
		end
		17'b00000000000000001 : begin
			exponent_offset = -8'd15;
			result_mantissa = {15'd0};
		end
		default: begin
			exponent_offset = -shared_exp_reg; //This means our result is zero, so we should make sure the shared exponent is 0
			result_mantissa = 15'd0;
		end
	endcase
end

wire [7:0] normalized_exp;
assign normalized_exp = shared_exp_reg + $signed(exponent_offset);

//Output result (should bre registered outside the module)
assign fp24_output = {sign_added_mantissas, normalized_exp, result_mantissa};

endmodule


//Below is for verifying that the custom fp24 adder matches the actual fp24 adder with Inf, denormalized exponents, and NaN
/*
module fp24_add_hw_reg (
	input clk, reset,
	input [23:0] fp24_input_a, fp24_input_b,
	output logic [23:0] fp24_output
);

logic [31:0] num_a, num_b, res, res_pipelined;

assign num_a = {fp24_input_a, 8'd0};
assign num_b = {fp24_input_b, 8'd0};

//Annoyingly varilator does not support 32-bit FP (shortreal) only 64-bit so we have to convert
wire [63:0] num_a_fp64, num_b_fp64;
wire [63:0] res_fp64;

//Sign 
assign num_a_fp64[63] = num_a[31];
assign num_b_fp64[63] = num_b[31];


//Mantissa
assign num_a_fp64[51:0] = {num_a[22:0], 29'd0};
assign num_b_fp64[51:0] = {num_b[22:0], 29'd0};


wire [9:0] num_a_exp_no_bias, num_b_exp_no_bias;
assign num_a_exp_no_bias = num_a[30:23] - 8'd127;
assign num_b_exp_no_bias = num_b[30:23] - 8'd127;

wire [11:0] num_a_exp_no_bias_sign_ext, num_b_exp_no_bias_sign_ext;
assign num_a_exp_no_bias_sign_ext = $signed(num_a_exp_no_bias);
assign num_b_exp_no_bias_sign_ext = $signed(num_b_exp_no_bias);


//Exponent (need to fix bias)
// verilator lint_off WIDTH
assign num_a_fp64[62:52] = (num_a[30:23] == 8'd0) ? 11'd0 : $signed(num_a_exp_no_bias_sign_ext) + 11'd1023;
assign num_b_fp64[62:52] = (num_b[30:23] == 8'd0) ? 11'd0 : $signed(num_b_exp_no_bias_sign_ext) + 11'd1023;

//Cannot be synthesized, but good enough for the simulation model
assign res_fp64 = $realtobits($bitstoreal(num_a_fp64) + $bitstoreal(num_b_fp64));
wire [31:0] res_fp32;


assign res[31] = res_fp64[63];
assign res[22:0] = res_fp64[51:29];

assign res[30:23] = (res_fp64[62:52] == 11'd0) ? 8'd0 : $signed(res_fp64[62:52] - 11'd1023) + 8'd127;
// verilator lint_on WIDTH

always @(posedge clk) begin
	if (reset) begin
		res_pipelined <= 32'd0;
		fp24_output <= 24'd0;
	end else begin
		res_pipelined <= res;
		fp24_output <= res_pipelined[31:8];
	end
end

endmodule
*/