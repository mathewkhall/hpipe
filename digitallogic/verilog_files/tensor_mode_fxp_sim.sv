//Functional model if the tensor block for simulation in programs that can't load Intel IPs
//This does 6 8-bit multiplications then accumulates the results
module tensor_block_tensor_fixed_point #(
//These are not actually used, they are just to make the module instantiation easier 
	parameter dsp_mode = "tensor_fxp",
	parameter dsp_sel_int4 = "select_int8",
	parameter dsp_fp32_sub_en = "float_sub_disabled",
	//This parameter is used to enable/disable the cascade in
	parameter dsp_cascade = "cascade_disabled"
) (    

input ena, clk, 
input acc_en,
input [1:0] clr, //Asynch clear

input [95:0] data_in,

input [95:0] cascade_data_in,
output [95:0] cascade_data_out,

output [36:0] result_h,
output [37:0] result_l,

//The following two signals are not implemented in this sim module as we don't use them in HPIPE
//Zero_en disables the ALU in the tensor block and mode_switch is only used in scalar switching mode
input mode_switch, zero_en,

input load_buf_sel,load_bb_one, load_bb_two, 
input [1:0] feed_sel, 
input [7:0] shared_exponent,
input [87:0] cascade_weight_in,
output logic [87:0] cascade_weight_out


);

//Multiplier is 8-bit but all internal tensor data paths after that have a width of 32-bits
localparam DATAW = 8;
localparam BUSW = 32;

//In reality different bits could clear different registers, but in HPIPE we should just assert both at the same time
wire reset;
assign reset =  clr[0] || clr[1];

//Pipeline inputs (including cascade)
 logic [95:0] cascade_reg;
logic acc_en_reg_1, acc_en_reg_2, load_bb_one_reg, load_bb_two_reg, load_buf_sel_reg;
logic [95:0] data_in_reg;
always @(posedge clk or posedge reset) begin
	 if (reset) begin
		data_in_reg <= 96'd0;
		cascade_reg <= 96'd0;
        acc_en_reg_1 <= 1'b0;
        acc_en_reg_2 <= 1'b0;
		  load_bb_one_reg <= 1'b0;
		  load_bb_two_reg <= 1'b0;
		  load_buf_sel_reg <= 1'b0;
	 end else begin
		data_in_reg <= ena ? data_in : data_in_reg;
        acc_en_reg_1 <= ena ? acc_en : acc_en_reg_1;
        acc_en_reg_2 <= ena ? acc_en_reg_1 : acc_en_reg_2;
		  load_bb_one_reg <= ena ? load_bb_one : load_bb_one_reg;
		  load_bb_two_reg <= ena ? load_bb_two : load_bb_two_reg;
		  load_buf_sel_reg <= ena ? load_buf_sel : load_buf_sel_reg;
		if (dsp_cascade == 1) cascade_reg <= ena ? cascade_data_in : cascade_reg;
		else cascade_reg <= 96'd0;
	 end
 end
 
 integer w;
 
 //Create weight registers
 //Note that these are labeled as "weights" but can actually be activations in the HPIPE design depending on the implementation (i.e. which vaslues we are pre-loading into ping-pong registers
 logic [2:0][9:0][7:0] weights_bank_1, weights_bank_2;
 always_ff @(posedge clk or posedge reset) begin
	 if (reset) begin
		for(w= 0; w < 10; w += 1) begin
			weights_bank_1[0][w] <= 8'd0;
			weights_bank_1[1][w] <= 8'd0;
			weights_bank_1[2][w] <= 8'd0;
			weights_bank_2[0][w] <= 8'd0;
			weights_bank_2[1][w] <= 8'd0;
			weights_bank_2[2][w] <= 8'd0;
		end
	 end
	 else if (ena) begin
		//Casecade mode
		if(feed_sel == 2'b01) begin
			for(w= 0; w < 10; w += 1) begin
				if (load_bb_one_reg) begin
					weights_bank_1[0][w] <= cascade_weight_in[w*DATAW +: DATAW];
					weights_bank_1[1][w] <= weights_bank_1[0][w];
					weights_bank_1[2][w] <= weights_bank_1[1][w];
				end else if (load_bb_two_reg) begin
					weights_bank_2[0][w] <= cascade_weight_in[w*DATAW +: DATAW];
					weights_bank_2[1][w] <= weights_bank_2[0][w];
					weights_bank_2[2][w] <= weights_bank_2[1][w];				
				end
			end
		end
		
		//Insert side-channel mode here
		else if (feed_sel == 2'b10) begin
			for(w= 0; w < 10; w += 1) begin
				if (load_bb_one_reg) begin
					weights_bank_1[0][w] <= (w == 0) ?  data_in[87:80] : ((w == 5) ?  data_in[95:88] : weights_bank_1[2][w-1]); //Side-channel mode has a weird data flow, see section 3.1.2 in user guide for more info
					weights_bank_1[1][w] <= weights_bank_1[0][w];
					weights_bank_1[2][w] <= weights_bank_1[1][w];
				end else if (load_bb_two_reg) begin
					weights_bank_2[0][w] <= (w == 0) ?  data_in[87:80] : ((w == 5) ?  data_in[95:88] : weights_bank_2[2][w-1]);
					weights_bank_2[1][w] <= weights_bank_2[0][w];
					weights_bank_2[2][w] <= weights_bank_2[1][w];				
				end	
			end
		end

		//Data-input mode here 
		else if (feed_sel == 2'b00) begin
			for(w= 0; w < 10; w += 1) begin
				if (load_bb_one_reg) begin
					weights_bank_1[0][w] <= data_in[w*DATAW +: DATAW];
					weights_bank_1[1][w] <= weights_bank_1[0][w];
					weights_bank_1[2][w] <= weights_bank_1[1][w];
				end else if (load_bb_two_reg) begin
					weights_bank_2[0][w] <= data_in[w*DATAW +: DATAW];
					weights_bank_2[1][w] <= weights_bank_2[0][w];
					weights_bank_2[2][w] <= weights_bank_2[1][w];				
				end
			end
		end
		
	 end
 end
 
 integer wo;
//Output weight cascade logic
always_comb begin
	for(wo= 0; wo < 10; wo += 1) begin
		if(load_bb_one_reg) begin
			cascade_weight_out[wo*DATAW +: DATAW] = weights_bank_1[2][wo];
		end else if (load_bb_two_reg) begin
			cascade_weight_out[wo*DATAW +: DATAW] = weights_bank_2[2][wo];
		end else begin
			cascade_weight_out[wo*DATAW +: DATAW] = 8'd0;
		end
	end
	
	cascade_weight_out[87:80] = 8'd0;
end
 

 //Pipeline multiplication results 
 logic signed [2:0][9:0][15:0] mult_res;
 integer j;
always @(posedge clk or posedge reset) begin
	for (j = 0; j < 10; j += 1) begin
		if (reset) begin
			mult_res[j] <= 16'd0;
		end else if (ena) begin
			//Select weights from appropriate ping pong reg. Note that 3 different sets of 10 weights are multiplied against just 1 set of 10 activations 
			if (load_buf_sel_reg ==0) begin
				mult_res[0][j] <= $signed(data_in_reg[j*DATAW +: DATAW]) * $signed(weights_bank_1[0][j]);
				mult_res[1][j] <= $signed(data_in_reg[j*DATAW +: DATAW]) * $signed(weights_bank_1[1][j]);
				mult_res[2][j] <= $signed(data_in_reg[j*DATAW +: DATAW]) * $signed(weights_bank_1[2][j]);
			end else begin
				mult_res[0][j] <= $signed(data_in_reg[j*DATAW +: DATAW]) * $signed(weights_bank_2[0][j]);
				mult_res[1][j] <= $signed(data_in_reg[j*DATAW +: DATAW]) * $signed(weights_bank_2[1][j]);
				mult_res[2][j] <= $signed(data_in_reg[j*DATAW +: DATAW]) * $signed(weights_bank_2[2][j]);
			end
		end
	end
 end
 
logic signed [2:0][31:0] accumulated_res_p1, accumulated_res_p2;
logic signed [2:0][31:0] accumulated_res;
logic [2:0][31:0] cascade_reg_or_accumulate;


//Calculate partial accumulated results here, split it up so it looks a bit less messy
 integer i;
always_comb begin
for (i = 0; i < 3; i += 1) begin
	accumulated_res_p1[i] = { {16{mult_res[i][0][15]}}, $signed(mult_res[i][0])} + { {16{mult_res[i][1][15]}}, $signed(mult_res[i][1])} +  { {16{mult_res[i][2][15]}}, $signed(mult_res[i][2])} +  { {16{mult_res[i][3][15]}}, $signed(mult_res[i][3])} +  { {16{mult_res[i][4][15]}}, $signed(mult_res[i][4])};
	accumulated_res_p2[i] = { {16{mult_res[i][5][15]}}, $signed(mult_res[i][5])} + { {16{mult_res[i][6][15]}}, $signed(mult_res[i][6])} +  { {16{mult_res[i][7][15]}}, $signed(mult_res[i][7])} +  { {16{mult_res[i][8][15]}}, $signed(mult_res[i][8])} +  { {16{mult_res[i][9][15]}}, $signed(mult_res[i][9])};
	cascade_reg_or_accumulate[i] = acc_en_reg_2 ? accumulated_res[i] : cascade_reg[i*32 +: 32];
	end
end

 //Pipeline accumulated result
 always @(posedge clk or posedge reset) begin
	 if (reset) begin
		accumulated_res <= 32'd0;
	 end else begin
		 //Kind of messy because we need to explicitly sign extend in order to avoid Verilator warnings/errors
		 if (ena) begin
            accumulated_res[0] <= accumulated_res_p1[0] + accumulated_res_p2[0] + $signed(cascade_reg_or_accumulate[0]);
				accumulated_res[1] <= accumulated_res_p1[1] + accumulated_res_p2[1] + $signed(cascade_reg_or_accumulate[1]);
				accumulated_res[2] <= accumulated_res_p1[2] + accumulated_res_p2[2] + $signed(cascade_reg_or_accumulate[2]);
		end 
	 end
 end

 assign cascade_data_out = {accumulated_res[2], accumulated_res[1], accumulated_res[0]};
 
 //Assign result_l and result_h based on spec (in this case it's taken from an Intel reference design)
assign result_l[24:0] = accumulated_res[0][24:0];
assign {result_h[11:0], result_l[37:25]} = accumulated_res[1][24:0];
assign result_h[36:12] = accumulated_res[2][24:0];
 
endmodule
