//Functional model if the tensor block for simulation in programs that can't load Intel IPs
//This uses block floating point with a shared 8-bit exponent (and the exponent is offset by -6)
// The mantissa must be 2s complement and should include the implicit 1
module tensor_block_tensor_floating_point #(
//These are not actually used, they are just to make the module instantiation easier 
	parameter dsp_mode = "tensor_fp",
	parameter dsp_sel_int4 = "select_int8",
	parameter dsp_fp32_sub_en = "float_sub_disabled",
	//This parameter is used to enable/disable the cascade in
	parameter dsp_cascade = 1
) (    

input ena, clk, 
input acc_en,
input [1:0] clr, //Asynch clear

input [95:0] data_in,

//Double check this
input [95:0] cascade_data_in,
output [95:0] cascade_data_out,

output [36:0] result_h,
output [37:0] result_l,

//Not needed, but makes replacing instantiation easier
input load_buf_sel, mode_switch, load_bb_one, load_bb_two, zero_en,
input [1:0] feed_sel, 
input [7:0] shared_exponent,
input [87:0] cascade_weight_in,
output logic [87:0] cascade_weight_out


);

//Multiplier is 8-bit but all internal tensor data paths after that have a width of 32-bits
localparam DATAW = 8;
localparam BUSW = 32;

//In reality different bits could clear different registers, but in HPIPE we should just assert both at the same time
wire reset;
assign reset =  clr[0] || clr[1];

//Pipeline inputs (including cascade)
 logic [95:0] cascade_reg;
logic acc_en_reg_1, acc_en_reg_2, load_bb_one_reg, load_bb_two_reg, load_buf_sel_reg;
logic [95:0] data_in_reg;
logic [7:0] shared_exponent_reg;
always @(posedge clk or posedge reset) begin
	 if (reset) begin
		data_in_reg <= 96'd0;
		cascade_reg <= 96'd0;
        acc_en_reg_1 <= 1'b0;
        acc_en_reg_2 <= 1'b0;
		  load_bb_one_reg <= 1'b0;
		  load_bb_two_reg <= 1'b0;
		  load_buf_sel_reg <= 1'b0;
		  shared_exponent_reg <= 8'd0;
	 end else begin
		data_in_reg <= ena ? data_in : data_in_reg;
        acc_en_reg_1 <= ena ? acc_en : acc_en_reg_1;
        acc_en_reg_2 <= ena ? acc_en_reg_1 : acc_en_reg_2;
		  load_bb_one_reg <= ena ? load_bb_one : load_bb_one_reg;
		  load_bb_two_reg <= ena ? load_bb_two : load_bb_two_reg;
		  load_buf_sel_reg <= ena ? load_buf_sel : load_buf_sel_reg;
		  shared_exponent_reg <= ena ? shared_exponent : shared_exponent_reg;
		if (dsp_cascade == 1) cascade_reg <= ena ? cascade_data_in : cascade_reg;
		else cascade_reg <= 96'd0;
	 end
 end
 
 integer w;
 
 //Create weight registers
 //Note that these are labeled as "weights" but can actually be activations in the HPIPE design depending on the implementation (i.e. which vaslues we are pre-loading into ping-pong registers
 logic [2:0][9:0][7:0] weights_bank_1, weights_bank_2;
 logic [2:0][7:0] weight_shared_exp_bank_1, weight_shared_exp_bank_2;
 always_ff @(posedge clk or posedge reset) begin
	 if (reset) begin
		for(w= 0; w < 10; w += 1) begin
			weights_bank_1[0][w] <= 8'd0;
			weights_bank_1[1][w] <= 8'd0;
			weights_bank_1[2][w] <= 8'd0;
			weights_bank_2[0][w] <= 8'd0;
			weights_bank_2[1][w] <= 8'd0;
			weights_bank_2[2][w] <= 8'd0;
		end
		weight_shared_exp_bank_1[0] <= 8'd0;
		weight_shared_exp_bank_1[1] <= 8'd0;
		weight_shared_exp_bank_1[2] <= 8'd0;
		weight_shared_exp_bank_2[0] <= 8'd0;
		weight_shared_exp_bank_2[1] <= 8'd0;
		weight_shared_exp_bank_2[2] <= 8'd0;	
	 end
	 else if (ena) begin
		//Casecade mode
		if(feed_sel == 2'b01) begin
			for(w= 0; w < 10; w += 1) begin
				if (load_bb_one_reg) begin
					weights_bank_1[0][w] <= cascade_weight_in[w*DATAW +: DATAW];
					weights_bank_1[1][w] <= weights_bank_1[0][w];
					weights_bank_1[2][w] <= weights_bank_1[1][w];
				end else if (load_bb_two_reg) begin
					weights_bank_2[0][w] <= cascade_weight_in[w*DATAW +: DATAW];
					weights_bank_2[1][w] <= weights_bank_2[0][w];
					weights_bank_2[2][w] <= weights_bank_2[1][w];				
				end
			end
			// Load in shared exponent
			if (load_bb_one_reg) begin
				weight_shared_exp_bank_1[0] <= cascade_weight_in[87:80];
				weight_shared_exp_bank_1[1] <= weight_shared_exp_bank_1[0];
				weight_shared_exp_bank_1[2] <= weight_shared_exp_bank_1[1];
			end else if (load_bb_two_reg) begin
				weight_shared_exp_bank_2[0] <= cascade_weight_in[87:80];
				weight_shared_exp_bank_2[1] <= weight_shared_exp_bank_2[0];
				weight_shared_exp_bank_2[2] <= weight_shared_exp_bank_2[1];	
			end
			
		end
		
		//Insert side-channel mode here
		else if (feed_sel == 2'b10) begin
			for(w= 0; w < 10; w += 1) begin
				if (load_bb_one_reg) begin
					weights_bank_1[0][w] <= (w == 0) ?  data_in[87:80] : ((w == 5) ?  data_in[95:88] : weights_bank_1[2][w-1]); //Side-channel mode has a weird data flow, see section 3.1.2 in user guide for more info
					weights_bank_1[1][w] <= weights_bank_1[0][w];
					weights_bank_1[2][w] <= weights_bank_1[1][w];
				end else if (load_bb_two_reg) begin
					weights_bank_2[0][w] <= (w == 0) ?  data_in[87:80] : ((w == 5) ?  data_in[95:88] : weights_bank_2[2][w-1]);
					weights_bank_2[1][w] <= weights_bank_2[0][w];
					weights_bank_2[2][w] <= weights_bank_2[1][w];				
				end	
			end
			// Load in shared exponent (this will require 3 extra cycles of loading where we load the 3rd shared exp, followed by the second, then the first)
			if (load_bb_one_reg) begin
				weight_shared_exp_bank_1[0] <= weights_bank_2[2][9];
				weight_shared_exp_bank_1[1] <= weight_shared_exp_bank_1[0];
				weight_shared_exp_bank_1[2] <= weight_shared_exp_bank_1[1];
			end else if (load_bb_two_reg) begin
				weight_shared_exp_bank_2[0] <= weights_bank_2[2][9];
				weight_shared_exp_bank_2[1] <= weight_shared_exp_bank_2[0];
				weight_shared_exp_bank_2[2] <= weight_shared_exp_bank_2[1];	
			end		
		end

		//Data-input mode here 
		else if (feed_sel == 2'b00) begin
			for(w= 0; w < 10; w += 1) begin
				if (load_bb_one_reg) begin
					weights_bank_1[0][w] <= data_in[w*DATAW +: DATAW];
					weights_bank_1[1][w] <= weights_bank_1[0][w];
					weights_bank_1[2][w] <= weights_bank_1[1][w];
				end else if (load_bb_two_reg) begin
					weights_bank_2[0][w] <= data_in[w*DATAW +: DATAW];
					weights_bank_2[1][w] <= weights_bank_2[0][w];
					weights_bank_2[2][w] <= weights_bank_2[1][w];				
				end
			end
			// Load in shared exponent
			if (load_bb_one_reg) begin
				weight_shared_exp_bank_1[0] <= shared_exponent;
				weight_shared_exp_bank_1[1] <= weight_shared_exp_bank_1[0];
				weight_shared_exp_bank_1[2] <= weight_shared_exp_bank_1[1];
			end else if (load_bb_two_reg) begin
				weight_shared_exp_bank_2[0] <= shared_exponent;
				weight_shared_exp_bank_2[1] <= weight_shared_exp_bank_2[0];
				weight_shared_exp_bank_2[2] <= weight_shared_exp_bank_2[1];	
			end
			
		end
		
	 end
 end
 
 integer wo;
//Output weight cascade logic
always_comb begin
	for(wo= 0; wo < 10; wo += 1) begin
		if(load_bb_one_reg) begin
			cascade_weight_out[wo*DATAW +: DATAW] = weights_bank_1[2][wo];
		end else if (load_bb_two_reg) begin
			cascade_weight_out[wo*DATAW +: DATAW] = weights_bank_2[2][wo];
		end else begin
			cascade_weight_out[wo*DATAW +: DATAW] = 8'd0;
		end
	end
	
	//Shared exponent
	if(load_bb_one_reg) begin
		cascade_weight_out[87:80] = weight_shared_exp_bank_1[2];
	end else if (load_bb_two_reg) begin
		cascade_weight_out[87:80] = weight_shared_exp_bank_2[2];;
	end else begin
		cascade_weight_out[87:80] = 8'd0;
	end
end

//Remove bias from shared exponents
logic [8:0] shared_exp_no_bias;
logic [2:0][8:0] weight_shared_exp_bank_1_no_bias, weight_shared_exp_bank_2_no_bias;
integer sxp;
always_comb begin
	shared_exp_no_bias = (shared_exponent_reg + 8'd6) - 8'd127 ; //Nx has an extra offset of 6 for some reason
	for (sxp=0; sxp < 3; sxp += 1) begin
		weight_shared_exp_bank_1_no_bias[sxp] = (weight_shared_exp_bank_1[sxp] + 8'd6) - 8'd127 ;
		weight_shared_exp_bank_2_no_bias[sxp] = (weight_shared_exp_bank_2[sxp] + 8'd6) - 8'd127 ;
	end
end

 

 //Pipeline multiplication results 
 logic signed [2:0][9:0][15:0] mult_res;
 logic signed [2:0][8:0] added_shared_exp;
 integer j;
always @(posedge clk or posedge reset) begin
	for (j = 0; j < 10; j += 1) begin
		if (reset) begin
			mult_res[0][j] <= 16'd0;
			mult_res[1][j] <= 16'd0;
			mult_res[2][j] <= 16'd0;
		end else if (ena) begin
			//Select weights from appropriate ping pong reg. Note that 3 different sets of 10 weights are multiplied against just 1 set of 10 activations 
			if (load_buf_sel_reg ==0) begin
				mult_res[0][j] <= $signed(data_in_reg[j*DATAW +: DATAW]) * $signed(weights_bank_1[0][j]);
				mult_res[1][j] <= $signed(data_in_reg[j*DATAW +: DATAW]) * $signed(weights_bank_1[1][j]);
				mult_res[2][j] <= $signed(data_in_reg[j*DATAW +: DATAW]) * $signed(weights_bank_1[2][j]);
			end else begin
				mult_res[0][j] <= $signed(data_in_reg[j*DATAW +: DATAW]) * $signed(weights_bank_2[0][j]);
				mult_res[1][j] <= $signed(data_in_reg[j*DATAW +: DATAW]) * $signed(weights_bank_2[1][j]);
				mult_res[2][j] <= $signed(data_in_reg[j*DATAW +: DATAW]) * $signed(weights_bank_2[2][j]);
			end
		end
	end
	
	// Add shared exponents together
	if (reset) begin
		added_shared_exp[0] <= 9'd0;
		added_shared_exp[1] <= 9'd0;
		added_shared_exp[2] <= 9'd0;
	end else if (ena) begin
		if (load_buf_sel_reg ==0) begin
			added_shared_exp[0] <= ($signed(shared_exp_no_bias) + $signed(weight_shared_exp_bank_1_no_bias[0])) + 8'd127;
			added_shared_exp[1] <= ($signed(shared_exp_no_bias) + $signed(weight_shared_exp_bank_1_no_bias[1])) + 8'd127;
			added_shared_exp[2] <= ($signed(shared_exp_no_bias) + $signed(weight_shared_exp_bank_1_no_bias[2])) + 8'd127;
		end else begin
			added_shared_exp[0] <= ($signed(shared_exp_no_bias) + $signed(weight_shared_exp_bank_2_no_bias[0])) + 8'd127;
			added_shared_exp[1] <= ($signed(shared_exp_no_bias) + $signed(weight_shared_exp_bank_2_no_bias[1])) + 8'd127;
			added_shared_exp[2] <= ($signed(shared_exp_no_bias) + $signed(weight_shared_exp_bank_2_no_bias[2])) + 8'd127;		
		end
	
	end
	
 end
 
logic signed [2:0][19:0] accumulated_res_p1, accumulated_res_p2;
logic signed [2:0][19:0] accumulated_res;

logic signed [2:0][31:0] fp32_accumulated_res, fp32_accumulated_res_reg, final_result;
logic [2:0][31:0] cascade_reg_or_accumulate;


//Calculate partial accumulated results here, split it up so it looks a bit less messy
 integer i;
always_comb begin
for (i = 0; i < 3; i += 1) begin
	accumulated_res_p1[i] = { {4{mult_res[i][0][15]}}, $signed(mult_res[i][0])} + { {4{mult_res[i][1][15]}}, $signed(mult_res[i][1])} +  { {4{mult_res[i][2][15]}}, $signed(mult_res[i][2])} +  { {4{mult_res[i][3][15]}}, $signed(mult_res[i][3])} +  { {4{mult_res[i][4][15]}}, $signed(mult_res[i][4])};
	accumulated_res_p2[i] = { {4{mult_res[i][5][15]}}, $signed(mult_res[i][5])} + { {4{mult_res[i][6][15]}}, $signed(mult_res[i][6])} +  { {4{mult_res[i][7][15]}}, $signed(mult_res[i][7])} +  { {4{mult_res[i][8][15]}}, $signed(mult_res[i][8])} +  { {4{mult_res[i][9][15]}}, $signed(mult_res[i][9])};
	accumulated_res[i] = accumulated_res_p1[i] + accumulated_res_p2[i];
	cascade_reg_or_accumulate[i] = acc_en_reg_2 ? fp32_accumulated_res[i] : cascade_reg[i*32 +: 32];
	end
end

//Convert from fxp with shared exp to fp32
fxp_with_exp_to_fp32 convert1(.fxp_mantissa(accumulated_res[0]), .shared_exp(added_shared_exp[0][7:0]), .fp32_number(fp32_accumulated_res[0]));
fxp_with_exp_to_fp32 convert2(.fxp_mantissa(accumulated_res[1]), .shared_exp(added_shared_exp[1][7:0]), .fp32_number(fp32_accumulated_res[1]));
fxp_with_exp_to_fp32 convert3(.fxp_mantissa(accumulated_res[2]), .shared_exp(added_shared_exp[2][7:0]), .fp32_number(fp32_accumulated_res[2]));


//Tensor fp mode needs an extra pipeline register
always @(posedge clk or posedge reset) begin
	if (reset) begin
		fp32_accumulated_res_reg[0] = 32'd0;
		fp32_accumulated_res_reg[1] = 32'd0;
		fp32_accumulated_res_reg[2] = 32'd0;
	end else if (ena) begin
		fp32_accumulated_res_reg[0] = fp32_accumulated_res[0];
		fp32_accumulated_res_reg[1] = fp32_accumulated_res[1];
		fp32_accumulated_res_reg[2] = fp32_accumulated_res[2];
	end

end



logic [2:0][31:0] addr_results;

fp32_add_sim fp_add1(.num_a(fp32_accumulated_res_reg[0]), .num_b(cascade_reg_or_accumulate[0]), .res(addr_results[0]));
fp32_add_sim fp_add2(.num_a(fp32_accumulated_res_reg[1]), .num_b(cascade_reg_or_accumulate[1]), .res(addr_results[1]));
fp32_add_sim fp_add3(.num_a(fp32_accumulated_res_reg[2]), .num_b(cascade_reg_or_accumulate[2]), .res(addr_results[2]));



 //Pipeline accumulated result
 always @(posedge clk or posedge reset) begin
	 if (reset) begin
		final_result[0] <= 32'd0;
		final_result[1] <= 32'd0;
		final_result[2] <= 32'd0;
	 end else begin
		 
		 if (ena) begin
			//Perform fp32 addition
			final_result[0] <= addr_results[0];
			final_result[1] <= addr_results[1];
			final_result[2] <= addr_results[2];
			
		end 
	 end
 end

 assign cascade_data_out = {final_result[2], final_result[1], final_result[0]};
 
 //Assign result_l and result_h based on spec (in this case it's taken from an Intel reference design)
assign result_l[23:0] = final_result[0][31:8];
assign {result_l[37:24], result_h[9:0] } = final_result[1][31:8];
assign result_h[33:10] = final_result[2][31:8];
assign result_h[36:34] = 3'd0;
 
endmodule

module fp32_add_sim (
	input [31:0] num_a,
	input [31:0] num_b,
	output [31:0] res
);

//Annoyingly varilator does not support 32-bit FP (shortreal) only 64-bit so we have to convert
wire [63:0] num_a_fp64, num_b_fp64;
wire [63:0] res_fp64;

//Sign 
assign num_a_fp64[63] = num_a[31];
assign num_b_fp64[63] = num_b[31];


//Mantissa
assign num_a_fp64[51:0] = {num_a[22:0], 29'd0};
assign num_b_fp64[51:0] = {num_b[22:0], 29'd0};


wire [9:0] num_a_exp_no_bias, num_b_exp_no_bias;
assign num_a_exp_no_bias = num_a[30:23] - 8'd127;
assign num_b_exp_no_bias = num_b[30:23] - 8'd127;

wire [11:0] num_a_exp_no_bias_sign_ext, num_b_exp_no_bias_sign_ext;
assign num_a_exp_no_bias_sign_ext = $signed(num_a_exp_no_bias);
assign num_b_exp_no_bias_sign_ext = $signed(num_b_exp_no_bias);


//Exponent (need to fix bias)
// verilator lint_off WIDTH
assign num_a_fp64[62:52] = (num_a[30:23] == 8'd0) ? 11'd0 : $signed(num_a_exp_no_bias_sign_ext) + 11'd1023;
assign num_b_fp64[62:52] = (num_b[30:23] == 8'd0) ? 11'd0 : $signed(num_b_exp_no_bias_sign_ext) + 11'd1023;

//Cannot be synthesized, but good enough for the simulation model
assign res_fp64 = $realtobits($bitstoreal(num_a_fp64) + $bitstoreal(num_b_fp64));
wire [31:0] res_fp32;

assign res[31] = res_fp64[63];
assign res[22:0] = res_fp64[51:29];

assign res[30:23] = (res_fp64[62:52] == 11'd0) ? 8'd0 : $signed(res_fp64[62:52] - 11'd1023) + 8'd127;
// verilator lint_on WIDTH


endmodule


module fxp_with_exp_to_fp32(
	input [19:0] fxp_mantissa,
	input [7:0] shared_exp,
	
	output [31:0] fp32_number
);

//First convert from 2s complement to sign and mangnitude
logic sign;
logic [18:0] magnitude;
assign sign = fxp_mantissa[19];
assign magnitude = sign ? (~fxp_mantissa[18:0]) + 1'b1 : fxp_mantissa[18:0];

//Next determine offset needed for exponent 
logic [7:0] exponent_offset;
always_comb begin
	casez (magnitude) 
		19'b1?????????????????? : exponent_offset = 8'd6;
		19'b01????????????????? : exponent_offset = 8'd5;
		19'b001???????????????? : exponent_offset = 8'd4;
		19'b0001??????????????? : exponent_offset = 8'd3;
		19'b00001?????????????? : exponent_offset = 8'd2;
		19'b000001????????????? : exponent_offset = 8'd1;
		19'b0000001???????????? : exponent_offset = 8'd0; //We are multiplying 2 6-bit numbers (sign = 1-bit, implicit 1 = 1-bit), so the decimal point starts here (after 12-bits)
		19'b00000001??????????? : exponent_offset = -8'd1;
		19'b000000001?????????? : exponent_offset = -8'd2;
		19'b0000000001????????? : exponent_offset = -8'd3;
		19'b00000000001???????? : exponent_offset = -8'd4;
		19'b000000000001??????? : exponent_offset = -8'd5;
		19'b0000000000001?????? : exponent_offset = -8'd6;
		19'b00000000000001????? : exponent_offset = -8'd7;
		19'b000000000000001???? : exponent_offset = -8'd8;
		19'b0000000000000001??? : exponent_offset = -8'd9;
		19'b00000000000000001?? : exponent_offset = -8'd10;
		19'b000000000000000001? : exponent_offset = -8'd11;
		19'b0000000000000000001 : exponent_offset = -8'd12;
		default: exponent_offset = -shared_exp; //This means our result is zero, so we should make sure the shared exponent is 0
	endcase
end

logic [7:0] new_exponent;
assign new_exponent = $signed(shared_exp) + $signed(exponent_offset);

//Next extract fp32 mantissa based on position of leading 1
logic [18:0] shifted_magnitude;
always_comb begin
	casez (magnitude) 
		19'b1?????????????????? : shifted_magnitude = {magnitude[17:0], 1'd0};
		19'b01????????????????? : shifted_magnitude = {magnitude[16:0], 2'd0};
		19'b001???????????????? : shifted_magnitude = {magnitude[15:0], 3'd0};
		19'b0001??????????????? : shifted_magnitude = {magnitude[14:0], 4'd0};
		19'b00001?????????????? : shifted_magnitude = {magnitude[13:0], 5'd0};
		19'b000001????????????? : shifted_magnitude = {magnitude[12:0], 6'd0};
		19'b0000001???????????? : shifted_magnitude = {magnitude[11:0], 7'd0}; //Decimal point starts here, so we need to extract everything after implicit 1 and pad
		19'b00000001??????????? : shifted_magnitude = {magnitude[10:0], 8'd0};
		19'b000000001?????????? : shifted_magnitude = {magnitude[9:0], 9'd0};
		19'b0000000001????????? : shifted_magnitude = {magnitude[8:0], 10'd0};
		19'b00000000001???????? : shifted_magnitude = {magnitude[7:0], 11'd0};
		19'b000000000001??????? : shifted_magnitude = {magnitude[6:0], 12'd0};
		19'b0000000000001?????? : shifted_magnitude = {magnitude[5:0], 13'd0};
		19'b00000000000001????? : shifted_magnitude = {magnitude[4:0], 14'd0};
		19'b000000000000001???? : shifted_magnitude = {magnitude[3:0], 15'd0};
		19'b0000000000000001??? : shifted_magnitude = {magnitude[2:0], 16'd0};
		19'b00000000000000001?? : shifted_magnitude = {magnitude[1:0], 17'd0};
		19'b000000000000000001? : shifted_magnitude = {magnitude[0], 18'd0};
		19'b0000000000000000001 : shifted_magnitude = 19'd0;
		default: shifted_magnitude = 19'd0; //This means our result is zero
	endcase
end

assign fp32_number = {sign, new_exponent, shifted_magnitude, 4'b0};

endmodule