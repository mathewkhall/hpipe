//Functional model if the tensor block for simulation in programs that can't load Intel IPs
//Note that this is only used to start new tensor chains and will only model the first fp32 number (this mode can support 2)
//This literally just passes a 32-bit input value through the cascade chains, we only use it to start new chains
module tensor_block_tensor_acc_sim #(
	//None of these are used for anything
	parameter dsp_mode = "tensor_acc_fp32_one", //Use fp32 cause for some reason fxp doesn't work in Modelsim (doesn't matter since we aren't actually adding anything)
	parameter dsp_sel_int4 = "select_int8",
	parameter dsp_fp32_sub_en = "float_sub_disabled",
	parameter dsp_cascade = "cascade_disabled"
)    ( 

input ena, clk, 
input acc_en,
input [1:0] clr, //Asynch clear

input [63:0] data_in,

//Double check this
input [95:0] cascade_data_in,
output [95:0] cascade_data_out,

output [36:0] result_h,
output [37:0] result_l,

//Not needed, but makes replacing instantiation easier
input load_buf_sel, mode_switch, load_bb_one, load_bb_two, zero_en,
input [1:0] feed_sel,
input [7:0] shared_exponent,
input [87:0] cascade_weight_in,
output [87:0] cascade_weight_out


);

//In reality different bits could clear different registers, but in HPIPE we should just assert both at the same time
wire reset;
assign reset =  clr[0] || clr[1];

//Just pipeline 3 times before outputting/cascading
reg [2:0][63:0] input_vals;

always @(posedge clk) begin
	if (reset) begin
		input_vals[0] <= 64'd0;
		input_vals[1] <= 64'd0;
		input_vals[2] <= 64'd0;
	end else begin
		input_vals[0] <= (ena) ? data_in[63:0] : input_vals[0];
		input_vals[1] <= (ena) ? (input_vals[0][30:23] == 8'hff ? {data_in[63:32], 32'h7fc00000} : input_vals[0]): input_vals[1]; //check for NaN
		input_vals[2] <= (ena) ? (input_vals[1][30:23] == 8'd0 ? {data_in[63:32], 0} : input_vals[1]) : input_vals[2];	//add a check to ensure input is a valid fp32 number since fxp mode doesn't work
	end
end


assign result_l = {6'd0, input_vals[2][31:0]};
assign result_h = {5'd0, input_vals[2][63:32]};

assign cascade_data_out = {32'd0, input_vals[2]};
 
endmodule
