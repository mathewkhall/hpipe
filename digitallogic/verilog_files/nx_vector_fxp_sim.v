//Functional model if the tensor block for simulation in programs that can't load Intel IPs
//This does 6 8-bit multiplications then accumulates the results
module tensor_block_vector_fixed_point #(
//These are not actually used, they are just to make the module instantiation easier 
	parameter dsp_mode = "vector_fxp",
	parameter dsp_sel_int4 = "select_int8",
	parameter dsp_fp32_sub_en = "float_sub_disabled",
	//This parameter is used to enable/disable the cascade in
	parameter dsp_cascade = 0
) (    

input ena, clk, 
input acc_en,
input [1:0] clr, //Asynch clear

input [95:0] data_in,

//Double check this
input [95:0] cascade_data_in,
output [95:0] cascade_data_out,

output [36:0] result_h,
output [37:0] result_l,

//Not needed, but makes replacing instantiation easier
input load_buf_sel, mode_switch, load_bb_one, load_bb_two, zero_en,
input [1:0] feed_sel,
input [7:0] shared_exponent,
input [87:0] cascade_weight_in,
output [87:0] cascade_weight_out


);

//Multiplier is 8-bit but all internal tensor data paths after that have a width of 32-bits
localparam DATAW = 8;
localparam BUSW = 32;

//In reality different bits could clear different registers, but in HPIPE we should just assert both at the same time
wire reset;
assign reset =  clr[0] || clr[1];

//Pipeline inputs (including cascade)
 logic [31:0] cascade_reg;
logic acc_en_reg_1, acc_en_reg_2;
logic [95:0] data_in_reg;
always @(posedge clk or posedge reset) begin
	 if (reset) begin
		data_in_reg <= 96'd0;
		cascade_reg <= 32'd0;
        acc_en_reg_1 <= 1'b0;
        acc_en_reg_2 <= 1'b0;
	 end else begin
		data_in_reg <= ena ? data_in : data_in_reg;
        acc_en_reg_1 <= ena ? acc_en : acc_en_reg_1;
        acc_en_reg_2 <= ena ? acc_en_reg_1 : acc_en_reg_2;
		if (dsp_cascade == 1) cascade_reg <= ena ? cascade_data_in[31:0] : cascade_reg;
		else cascade_reg <= 32'd0;

	 end
 end
 

 //Pipeline multiplication results 
 logic signed [5:0][15:0] mult_res;
 integer i;
always @(posedge clk or posedge reset) begin
	for (i = 0; i < 6; i += 1) begin
		if (reset) begin
			mult_res[i] <= 16'd0;
		end else begin
			if (ena) mult_res[i] <= $signed(data_in_reg[(i*2)*DATAW +: DATAW]) * $signed(data_in_reg[(i*2+1)*DATAW +: DATAW]);
			else  mult_res[i] <= mult_res[i];
		end
	end
 end
 
logic signed [31:0] accumulated_res;
wire [31:0] cascade_reg_or_accumulate;
assign cascade_reg_or_accumulate = acc_en_reg_2 ? accumulated_res : cascade_reg;

 //Pipeline accumulated result
 always @(posedge clk or posedge reset) begin
	 if (reset) begin
		accumulated_res <= 32'd0;
	 end else begin
		 //Kind of messy because we need to explicitly sign extend in order to avoid Verilator warnings/errors
		 if (ena) begin
			 accumulated_res <= $signed({{16{mult_res[0][15]}}, mult_res[0]}) + $signed({{16{mult_res[1][15]}}, mult_res[1]}) + $signed({{16{mult_res[2][15]}}, mult_res[2]}) + $signed({{16{mult_res[3][15]}}, mult_res[3]}) + $signed({{16{mult_res[4][15]}}, mult_res[4]}) + $signed({{16{mult_res[5][15]}}, mult_res[5]}) + $signed(cascade_reg_or_accumulate);
		end else accumulated_res <= accumulated_res;
	 end
 end

 //assign cascade_data_out = {64'd0, accumulated_res};
	//Not functionally correct but makes debuging sim easier
	assign cascade_data_out = {{64{accumulated_res[31]}}, accumulated_res};
 
 //Assign result_l and result_h based on spec (in this case it's taken from an Intel reference design)
assign result_l[18:0] = accumulated_res[18:0];
assign result_l[37:19] = 19'd0;
assign result_h[36:0] = 37'd0;

assign cascade_weight_out = 88'd0;
 
endmodule
