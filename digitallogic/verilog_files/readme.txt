This folder is for any modules that are not generated automatically by HPIPE. For example, simulation modules for the tensor blocks and code for IPs provided by Intel (e.g. Tensor block multiplier for larger numbers). HPIPE will go through this directory (and all subdirectories) and copy the files to the output folder.

Note: do not include Timescale for any of these modules, HPIPE will add it automatically when copying them to the output
